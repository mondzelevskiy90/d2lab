<?php
namespace App\Services;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class Content{
	public static function getLangContent($arr = array()) {
		$data = [];

		$languages = config('app.languages');
		$default_language = config('app.voyager_language_default');

		foreach ($languages as $code => $lang_name) {
			$data[$code] = [];
			$data[$code]['code'] = $code;
			$data[$code]['language_name'] = $lang_name;
			$data[$code]['language_default'] = ($code == $default_language ? 1 : 0);
			$data[$code]['name'] = '';
			$data[$code]['description'] = '';
			$data[$code]['meta_title'] = '';
			$data[$code]['meta_description'] = '';
		}

		if (count($arr)) {
			foreach ($arr as $item) {
				$item = $item->toArray();

				$data[$item['lang']]['name'] = $item['name'];
				$data[$item['lang']]['description'] = $item['description'];
				$data[$item['lang']]['meta_title'] = $item['meta_title'];
				$data[$item['lang']]['meta_description'] = $item['meta_description'];
			}
		}

		return $data;
	}

	public static function createSlug($str, $model, $num = 0) {
		$num++;

		if (!is_null($model::where('slug', $str)->first())) {
			$str = self::createSlug($str.$num, $model, $num);
		} else {
			$str = Str::slug($str);
		}

		return $str;
	}

	public static function getSeo($item) {
		$data = false;

		if ($item) {
			foreach ($item->content as $content) {
				if ($content->lang == config('app.voyager_language_default')) $data = $content;
			}
		}

		return $data;
	}

	public static function checkBranch() {
		$data = false;

		if (\App\Models\Branch::where('owner_id', Auth::user()->id)->where('status', '!=', 4)->first()) $data = true;

		return $data;
	}

	public static function getDelivery() {
		return \App\Models\Delivery::where('status', 1)->where('id', '!=', 12)->orderBy('sort_order', 'ASC')->get();
	}

	public static function getIndustriesByParent($parent_id) {
		$industries = \App\Models\Industry::where('parent_id', $parent_id)
            ->where('status', 1)
            ->orderBy('sort_order', 'ASC')
            ->get();

		return $industries;
	}

	public static function getServicesByIndustry($id, $default = false, $not_prepare = false) {
		$services = \App\Models\Industry_service::where('industry_id', $id)
			->where('status', 1);

		if ($default) {
			$services->where('default_list', 1);
		}

		$services =	$services->get();

		if ($not_prepare) {
			return self::prepareNotSortedServicesList($services);
		} else {
			return self::prepareServicesList($services);
		}
	} 

	public static function getServiceChilrdensByFilter($id) {
		$arr = array();
		
		$services = \App\Models\Industry_service::select('id')->where('parent_id', $id)->get();

		foreach ($services as $service) {
			
			$arr[] = $service->id;

			$childrens = self::getServiceChilrdensByFilter($service->id);

			if (count($childrens)) $arr = array_merge($arr, $childrens);
		}

		return $arr;
	}

	public static function getOptionsByIndustry($id) {
		return \App\Models\Option::where('industry_id', $id)
			->where('status', 1)
			->get();
	}

	public static function getOptionsByService($id) {
		return \App\Models\Option_to_service::with('option_info')->where('industry_service_id', $id)->get();
	}

	public static function getPriceServices($id, $parent = false) {
		$services = \App\Models\Price_service::where('price_id', $id);

		if ($parent) {
			$services->with('service');
		}

		$branches = [0];

		if (Auth::check()) {
			switch (Auth::user()->role_id) {
				case config('roles.clinic_head'):
					$branches_list = \App\Models\Branch::where('owner_id', Auth::user()->id)->get();

					foreach ($branches_list as $branch) {
						$branches[] = $branch->id;
					}

					break;
				case config('roles.clinic_staff'):
				case config('roles.doctor'):
					$staff = \App\Models\Branch_staff::where('user_id', Auth::user()->id)->first();

					if ($staff) {
						$branches = [$staff->branch_id];
					}

					break;
			}
		}
			
		$services->with([
			'customs' => function($q) use ($branches) {
				$q->whereIn('branch_id', $branches);
			}
		]);

		$services = $services->get();

		return self::preparePriceServices($services, $parent);
	}

	public static function getPriceDelivery($id) {
		$data = array();
		
		$deliveries = \App\Models\Price_delivery::where('price_id', $id)->get()->toArray();

		foreach ($deliveries as $delivery) {
			$data[$delivery['delivery_id']] = $delivery;
		}

		return $data;
	}

	public static function getOptionsToPriceService($price_id, $industry_service_id) {
		return \App\Models\Option_to_price_service::where('price_id', $price_id)
			->where('industry_service_id', $industry_service_id)
			->get();
	}

	public static function getFieldsByServiceAndSystem($service_id, $system_id) {
		$data = array();

		$fields = \App\Models\Field::with([
				'service' => function ($q) use ($service_id) {
					$q->where('industry_service_id', $service_id);
				},
				'system' => function ($q) use ($system_id) {
					$q->where('numbering_system_id', $system_id);
				},
				'values'
			])
			->where('status', 1)
			->orderBy('sort_order', 'ASC')
			->get();

		foreach ($fields as $field) {
			if ($field->service && $field->system) {
				$data[] = $field;
			}
		}

		return $data;
	}

	public static function getSelectedToothsText($str) {
		$selecteds_arr = explode(',', $str);

        $data = array();
        $pre_selecteds = array();
        $lined = false;
        $lined2 = false;
        $need_more = false;

        foreach($selecteds_arr as $key => $selected) {                       
            if($selected) {                        	
                $pre_selecteds[] = explode('_', $selected);
            }
        }

        foreach($pre_selecteds as $key => $selected) { 
            $index = $selected[2];

            if ($selected[1] == 2) {
            	if ($selected[3] && $selected[3] == 3) {
					//show all selected with '-' for Full denture
					if (!isset($pre_selecteds[$key - 1][1])) {
						$lined2 = $index.'-';
					} else if (!isset($pre_selecteds[$key + 1][1])) {
						$lined2 .= $index;
	                    $data[] = $lined2;
	                    $lined2 = false;						
					}
				} else if ($selected[3] && $selected[3] == 4) {
					$prev_key = $selected[0] - 1;
					$next_key = $selected[0] + 1;

					//Partial denture can be single or group selected. Show group selected with '-'
					if ((!isset($pre_selecteds[$key - 1][0]) || (isset($pre_selecteds[$key - 1][0]) && $pre_selecteds[$key - 1][0] != $prev_key)) && isset($pre_selecteds[$key + 1][0]) && $pre_selecteds[$key + 1][0] == $next_key) {						
						$lined2 = $index.'-';
					} else if (isset($pre_selecteds[$key - 1][0]) && $pre_selecteds[$key - 1][0] == $prev_key  && (!isset($pre_selecteds[$key + 1][0]) || (isset($pre_selecteds[$key + 1][0]) && $pre_selecteds[$key + 1][0] != $next_key))) {
						$lined2 .= $index;
						$data[] = $lined2;
						$lined2 = false;
					} else if ($lined || $lined2){
						continue;
					} else {
						$data[] = $index;
						$lined2 = false;
					}
				} else {
					$data[] = $index;
				}            	
            }

            if ($selected[1] == 5) {
                if (isset($pre_selecteds[$key - 1][1]) && $pre_selecteds[$key - 1][1] == 5) {
                    
                } else {
                    $lined = $index.'-';
                    $need_more = true;
                }
            }
            
            if($selected[1] == 3) {
            	if (isset($pre_selecteds[$key - 1][1]) && $pre_selecteds[$key - 1][1] == 5) $index = $pre_selecteds[$key - 1][2];

                if (!$need_more) $lined = $index.'-';
            }

            if($selected[1] == 4) {
            	if (isset($pre_selecteds[$key + 1][1]) && $pre_selecteds[$key + 1][1] == 6) {
                    $need_more = true;
                } else {
                    $need_more = false;
                }

                if (!$need_more) {
                    $lined .= $index;
                    $data[] = $lined;
                    $lined = false;
                }
                
            }

            if ($selected[1] == 6) {
                if (isset($pre_selecteds[$key + 1][1]) && $pre_selecteds[$key + 1][1] == 6) {
                    $need_more = true;
                } else {
                    $lined .= $index;
                    $data[] = $lined;
                    $lined = false;
                    $need_more = false;
                }
            }
           
        }

       return $data = implode(', ', $data);
	}

	public static function calcLabRating($branch_id) {
		$rating = 0;
		$reviews_rating = 0.0;
		$total_orders = 0;
		$success_points = 0;
		$a = 0;
		$b = 0;		

		$branch = \App\Models\Branch::where('id', (int)$branch_id)->first();

		$orders = \App\Models\Order::where('to_branch_id', (int)$branch_id)
			->whereNotIn('status', [1,2])
			->get();

		if (count($orders)) {
			$total_orders = count($orders);
			$total_success_orders = 0;

			foreach ($orders as $order) {
				if ($order->status == 17) $total_success_orders++;
			}

			if ($total_success_orders > 0) {
				$success_points = $total_success_orders / 10 * 0.01;

				$a = $total_success_orders / $total_orders + $success_points;
			}
		}
		

		$reviews = \App\Models\Order_review::where('branch_id', (int)$branch_id)
			->where('status', 1)
			->get();

		if (count($reviews)) {
			$reviews_total_rating = 0.0;
			$reviews_count = count($reviews);

			foreach ($reviews as $review) {
				$reviews_total_rating = (float)$reviews_total_rating + (float)$review->rating;
			}

			if ($reviews_total_rating != 0.0) {
				$reviews_rating = round($reviews_total_rating / $reviews_count, 1);

				$b = $reviews_rating / 2;
			}
		}

		$rating = $a * 100 + $b;

		$branch->old_rating = $branch->rating;
		$branch->rating = $rating;
		$branch->reviews_rating = $reviews_rating;

		$branch->save();
	}

	public static function getReviewAccess($order_id, $user_data) {
		$data = (object)[];
        $data->order_id = $order_id;
        $data->can_review = false;
        $data->cant_comment = false;
        $data->can_edit = true;

        if ($user_data->type == config('types.clinic')) {
            $user_order_review =  \App\Models\Order_review::where('order_id', $order_id)
                ->where('user_id', $user_data->id)
                ->where('status', 1)
                ->first();

            if (!$user_order_review) $data->can_review = true;
        }

        if ($user_data->type == config('types.lab')) {
            $data->cant_comment = [];

            $user_reviews_comments = \App\Models\Order_review_comment::where('order_id', $order_id)
                ->where('user_id', $user_data->id)
                ->where('status', 1)
                ->get();

            foreach ($user_reviews_comments as $user_reviews_comments) {
                $data->cant_comment[] = $user_reviews_comments->review_id;
            }
        } 

        return $data;
	}

	protected static function prepareNotSortedServicesList($arr = array()) {
		$data = array();

		foreach ($arr as $item) {			
			$data[$item->id] = array( 
				'id' => $item->id,
				'name' => $item->name
			);			
		}

		return $data;
	}

	protected static function prepareServicesList($arr = array(), $parent_id = 0) {
		$data = array();

		foreach ($arr as $item) {
			if ($item->parent_id == $parent_id) {
				$data[] = array( 
					'id' => $item->id,
					'name' => $item->name,
					'parent_id' => $item->parent_id,
					'price_for' => $item->price_for,
					'options' => self::getOptionsByService($item->id),
					'childrens' => self::prepareServicesList($arr, $item->id)
				);
			}
		}

		return $data;
	}

	protected static function preparePriceServices($arr, $parent) {
		$data = array();

		foreach ($arr as $item) {
			if (!$item->service) continue; 
			
			$parent_id = 0;

			if ($parent) {
				$parent_id = $item->service->parent_id;
			}

			$price = $item->price;
			$show_prices_message = false;

			if (isset($item->customs) && count($item->customs)) {
				if (count($item->customs) > 1) {
					$show_prices_message = true;
				} else {
					$price = $item->customs[0]->price;
				}
			}

			$data[$item->industry_service_id] = array(
				'industry_service_id' => $item->industry_service_id,
				'parent_id' => $parent_id,
				'price' => $price,
				'currency_id' => $item->currency_id,
				'min_days' => $item->min_days,
				'price_for' => $item->service->price_for,
				'options' => self::preparePriceServicesOptionsList(self::getOptionsToPriceService($item->price_id, $item->industry_service_id)),
				'show_prices_message' => $show_prices_message
			);
		}

		return $data;
	}

	protected static function preparePriceServicesOptionsList($arr) {
		$data = array();

		foreach ($arr as $item) {
			$data[$item->option_id] = $item->option_id;
		}

		return $data;
	}
}