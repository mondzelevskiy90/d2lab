<?php
namespace App\Services;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use App\Models\Invoice;
use App\Models\Order;
use App\Models\Order_history;
use App\Models\Order_prescription;
use App\Models\Order_prescription_field;
use App\Models\Order_total;
use App\Models\Notification_message;

class OrderService{
	public static function createOrderRemake($invoice_id, $order_id, $prescription_id, $action = 0) {
		$order_prescriptions = Order_prescription::with('service')
	        ->where('order_id', $order_id)
	        ->where('paid', 0)
	        ->get();

	    $order_prescription = Order_prescription::with('service', 'prescription_fields', 'order_info')
            ->where('id', (int)$prescription_id)
            ->first();

	    if ($action == 1) {
	        $new_order = new Order;

	        $new_order->order_type = 2;
	        $new_order->name = 'Remake #'.$order_prescription->order_id;
	        $new_order->user_id = $order_prescription->order_info->user_id;
	        $new_order->user_name = $order_prescription->order_info->user_name;
	        $new_order->branch_id = $order_prescription->order_info->branch_id;
	        $new_order->branch_name = $order_prescription->order_info->branch_name;
	        $new_order->to_branch_id = $order_prescription->order_info->to_branch_id;
	        $new_order->to_branch_name = $order_prescription->order_info->to_branch_name;
	        $new_order->user_email = $order_prescription->order_info->user_email;
	        $new_order->user_phone = $order_prescription->order_info->user_phone;
	        $new_order->patient_name = $order_prescription->order_info->patient_name;
	        $new_order->patient_lastname = $order_prescription->order_info->patient_lastname;
	        $new_order->patient_age = $order_prescription->order_info->patient_age;
	        $new_order->patient_sex = $order_prescription->order_info->patient_sex;
	        $new_order->ship_date = $order_prescription->order_info->ship_date;
	        $new_order->ship_address = $order_prescription->order_info->ship_address;        
	        $new_order->attached = $order_prescription->order_info->attached;    
	        $new_order->numbering_system_id = $order_prescription->order_info->numbering_system_id; 
	        $new_order->comment = $order_prescription->order_info->comment; 
	        $new_order->payment_status = 3;   
	        $new_order->status = 12;

	        $new_order->save();

	        $history = new Order_history;

	        $history_comment = 'Order has been created!';

	        if ($order_prescription->order_info->comment) $history_comment .= ' Comment: '.$order_prescription->order_info->comment.' ';

	        $history->order_id = $new_order->id;
	        $history->user = $order_prescription->order_info->user_name;
	        $history->comment = $history_comment;

	        $history->save();
	        
	        $new_order_total = new Order_total;

	        $new_order_total->order_id = $new_order->id;                           
	        $new_order_total->advance_price = $order_prescription->price;
	        $new_order_total->price = $order_prescription->price;
	        $new_order_total->currency_id = 1;
	        $new_order_total->save();

	        $new_order_prescription = new Order_prescription;

	        $new_order_prescription->order_id = $new_order->id;
	        $new_order_prescription->service_id = $order_prescription->service_id;
	        $new_order_prescription->numbering_system_id = $order_prescription->numbering_system_id;
	        $new_order_prescription->selecteds = $order_prescription->selecteds;
	        $new_order_prescription->price_order_date = $order_prescription->price_order_date;
	        $new_order_prescription->selected_option = $order_prescription->selected_option;
	        $new_order_prescription->price_for = $order_prescription->price_for;
	        $new_order_prescription->advance_price = $order_prescription->price;
	        $new_order_prescription->price = $order_prescription->price;
	        $new_order_prescription->currency_id = 1;
	        $new_order_prescription->comment = $order_prescription->comment;
	        $new_order_prescription->save();
	            
	        foreach ($order_prescription->prescription_fields as $field) {
	            $new_prescription_field = new Order_prescription_field;

	            $new_prescription_field->prescription_id = $new_order_prescription->id;
	            $new_prescription_field->field_id = $field->field_id;
	            $new_prescription_field->value = $field->value;
	            $new_prescription_field->save();
	        }

	        $old_invoice = Invoice::where('id', $invoice_id)->first();

	        if ($old_invoice) {
	        	$invoice = Invoice::firstOrNew(['order_id' => (int)$new_order->id]);

	        	$invoice->branch_id = $old_invoice->branch_id;
	            $invoice->to_branch_id = $old_invoice->to_branch_id;
	            $invoice->order_user_id = $old_invoice->order_user_id;
	        	$invoice->name = 'INV #'.(int)$new_order->id.'(D2L)';
	        	$invoice->status = 1;
	        	$invoice->save();
	        }  

	        $order_url = route('orders.edit', $new_order->id);

	        $notificate_message = sprintf(__('translations.order_new_notificate'), $order_url, $new_order->id);

            $notificate = new Notification_message;

            $notificate->branch_id = $new_order->to_branch_id;
            $notificate->text = $notificate_message;
            $notificate->save();          
	    }

	    if (count($order_prescriptions)) {
	        if (count($order_prescriptions) == 1) {
	            $comment = 'Order was closed by lab with doctor aggrement!';

	            if ($action == 1) $comment .= ' New order has been created!';

	            //Invoice::where('id', $invoice_id)->update(['status' => 2]);
	            Order::where('id', $order_id)->update(['status' => 7, 'comment' => $comment]);

	            $history = new Order_history;

	            $history->order_id = $order_id;
	            $history->user = Auth::user()->name.' '.Auth::user()->surname;
	            $history->comment = $comment;

	            $history->save();

	            $count_prescriptions = false;
	        } else if ($action != 1) {
	            Order_prescription::where('id', (int)$prescription_id)->delete();
	            Order_prescription_field::where('prescription_id', (int)$prescription_id)->delete();

	            $order_total = Order_total::where('order_id', $order_id)->first();

	            if ($order_total) {
	                $new_price = (float)$order_total->price - (float)$order_prescription->price;
	                $price_changed_to = 0.00;

	                if ($order_prescription->price_changed_to != '0.00') $price_changed_to = (float)$order_total->price_changed_to - (float)$order_prescription->price_changed_to;


	                $order_total->price = number_format($new_price, 2);
	                $order_total->price_changed_to = number_format($price_changed_to, 2);
	                $order_total->save();
	            }

	            $count_prescriptions = 1; 
	        }
	    }

	    return $action == 1 ? $new_order->id : $count_prescriptions; 
	}
}