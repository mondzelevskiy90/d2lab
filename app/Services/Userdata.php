<?php
namespace App\Services;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Country;
use App\Models\Region;
use App\Models\Locality;
use App\Models\User_status;
use App\Models\User_type;
use App\Models\Role;
use Session;

class Userdata{
	public static function getSubordination($role_id) {
		$data = array();

		foreach (self::getRoles() as $role) {
			$data[$role['id']] = explode(',', $role['not_childrens']);
		}
		
		return $data[$role_id];
	}

	public static function getStatuses() {
		return User_status::get()->toArray();
	}

	public static function getTypes() {
		return User_type::get()->toArray();
	}

	public static function getRoles() {
		return Role::where('id', '!=', 2)->get()->toArray();
	}

	public static function getRole($id) {
		return Role::where('id', $id)->first();
	}

	public static function getUserAddress($user) {
		return $user->zip_code .', '.$user->address.', '.self::getLocality($user->locality);
	}

	public static function getLocality($locality_id) {
		$str = '';

		$locality = Locality::with('country')->where('id', (int)$locality_id)->first();

		if ($locality) $str = $locality->name.(isset($locality->country) ? ', '.$locality->country->code : '');		

		return $str;
	}

	public static function detectLocation($ip = NULL) {
	    if (!Session::has('location')) {
	    	$location_id = false;

		    if (filter_var($ip, FILTER_VALIDATE_IP) === FALSE) {
		    	$ip = $_SERVER["REMOTE_ADDR"];
		     
	        	if (filter_var($_SERVER['HTTP_X_FORWARDED_FOR'], FILTER_VALIDATE_IP)) $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
	        	if (filter_var($_SERVER['HTTP_CLIENT_IP'], FILTER_VALIDATE_IP)) $ip = $_SERVER['HTTP_CLIENT_IP'];	     
		    }
		  
		    if (filter_var($ip, FILTER_VALIDATE_IP)) {
		      	$ipdat = json_decode(file_get_contents('http://www.geoplugin.net/json.gp?ip='.$ip));

		      	if (strlen(trim($ipdat->geoplugin_countryCode)) == 2 && $ipdat->geoplugin_regionName && $ipdat->geoplugin_regionName != '' && $ipdat->geoplugin_city && $ipdat->geoplugin_city != '') {
		      		$country = Country::where('short_code', $ipdat->geoplugin_countryCode)->first();

		      		if (!$country) {
		      			$country = new Country;

		      			$country->short_code = strtolower($ipdat->geoplugin_countryCode);
		      			$country->code = $ipdat->geoplugin_countryName;
		      			$country->name = $ipdat->geoplugin_countryName;
		      			$country->save();
		      		}

		      		$region = Region::where('country_id', $country->id)
		      			->where('code', $ipdat->geoplugin_regionCode)
		      			->first();

		      		if (!$region) {
		      			$region = new Region;

		      			$region->country_id = $country->id;
		      			$region->code = $ipdat->geoplugin_regionCode;
		      			$region->name = $ipdat->geoplugin_regionName;
		      			$region->save();
		      		}	

		      		//New York districts returned as city???? ---> fix it: 
		      		$block_cites = [
		      			'Brooklyn',
		      			'Glen Cove',
		      			'The Bronx',
		      			'Garden City',
		      			'Seaford',
		      			'Shirley',
		      			'Queens',
		      			'Manhattan',
		      			'Staten Island',
		      			'Harrison',
		      			'Carmel',
		      			'Rhinebeck',
		      			'Orchard Park',
		      			'Buffalo',
		      			'Irondequoit',
		      			'Camillus',
		      			'Corning',
		      			'Utica',
		      			'Schuylerville',
		      			'Amsterdam'
		      		];

		      		if ($country->id == 1 && $region->id == 33 && in_array($ipdat->geoplugin_city, $block_cites)) {
						$location_id = 1;
		      		} else {
						$locality = Locality::where('country_id', $country->id)
			      			->where('region_id', $region->id)
			      			->where('name', 'LIKE', $ipdat->geoplugin_city.'%')
			      			->first();

			      		if (!$locality) {		      			
			      			$locality = new Locality;

			      			$locality->country_id = $country->id;
			      			$locality->region_id = $region->id;
			      			$locality->code = '';
			      			$locality->name = $ipdat->geoplugin_city.', '.$ipdat->geoplugin_regionCode;
			      			$locality->save();
			      		}

			      		$location_id = $locality->id;
		      		}		      		
		    	}
			}

			Session::put('location', $location_id);
		}

		return session('location');
	}
}