<?php
namespace App\Services;

class Google {
	private static $api_key = 'AIzaSyAiOhNh7bGlZpAbuUYvdDrY7XpXz8xrQ8A';
	private static $api_url = 'https://maps.googleapis.com/maps/api/';	

	public static function getLocalitiesByRegion($region_name, $country_code) {
		$data = self::sendRequest('place/autocomplete/json?input='.$region_name.'&region='.$country_code.'&language=en&types=(cities)&sessiontoken='.self::$api_key);

		return $data;
	}

	public static function getLocationDataByName($query, $country) {
		$data = self::sendRequest('place/textsearch/json?query='.urlencode($query).'&region='.$country->code.'&sessiontoken='.self::$api_key);

		return $data;
	}

	protected static function sendRequest($add_url) {
		$url = self::$api_url.$add_url.'&key='.self::$api_key;

		$c = curl_init();

		curl_setopt($c, CURLOPT_URL, $url);
		curl_setopt($c, CURLOPT_RETURNTRANSFER, true);

		$data = curl_exec($c);
		curl_close($c);

		return json_decode($data);
	}
}