<?php
namespace App\Services;

use App\Models\Invoice;

class Nmi {
    private static $service_key = '5fPMA4Vy22Cj8gj8u39vYvpBDkP846B2';
  
    public static function createPayment($user, $invoices, $card_data) {
        $return = array(
            'status' => 3,
            'transaction_id' => null,
            'charge_transaction_id' => null
        );

        if (!empty($card_data) && !empty($invoices)){
            $amount = 0;
            $service_amount = 0;
            $invoice_data = false;
            $private_key = false;
            $invoice_names = array();
            $service_charge = setting('admin.service_charge');

            foreach ($invoices as $invoice) {
                if (!$invoice) continue;

                $invoice_data = Invoice::with([
                        'total',
                        'branch',
                        'to_branch',
                        'order_info',
                        'nmi_info'
                    ])
                    ->where('id', (int)$invoice)
                    ->where('payment_status', '!=', 4)
                    ->where('status', 1)
                    ->first();

                if ($invoice_data && $invoice_data->total && $invoice_data->nmi_info) {
                    $amount = $amount + (float)$invoice_data->total->price;
                    $private_key = $invoice_data->nmi_info->private_key;
                    $invoice_names[] = $invoice_data->name;

                    $branch = \App\Models\Branch::where('id', $invoice_data->to_branch_id)->first();

                    if ($branch && $branch->charge_value != '0.00') {
                        $service_charge = $branch->charge_value;
                    }
                }

                break;
            }

            if (!$invoice_data || !$private_key || $amount == 0) return $return;

            $service_amount = $amount * $service_charge;

            $data  = '';
            $data .= 'ccnumber='.urlencode($card_data['ccnumber']).'&';
            $data .= 'ccexp='.urlencode(str_replace('/', '', $card_data['ccexp'])).'&';
            $data .= 'cvv='.urlencode($card_data['cvv']).'&';
            $data .= 'ipaddress='.urlencode(!empty($_SERVER['HTTP_X_FORWARDED_FOR']) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR']).'&';
            $data .= 'orderid='.urlencode('Invoice #'.$invoice_data->id).'&';
            $data .= 'firstname='.urlencode($user->name).'&';
            $data .= 'lastname='.urlencode($user->surname).'&';
            $data .= 'phone='.urlencode($user->phone).'&';
            $data .= 'email='.urlencode($user->email).'&';
            $data .= 'currency=USD&';
            $data .= 'type=sale&';

            $data_service = $data;

            $data .= 'security_key='.urlencode($private_key).'&';
            $data .= 'amount='.urlencode(number_format($amount - $service_amount, 2, '.', '')).'&';
            $data .= 'orderdescription='.urlencode('Order from '.$invoice_data->branch->name.' for '.(implode(', ', $invoice_names)));

            $data_service .= 'security_key='.urlencode(self::$service_key).'&';
            $data_service .= 'amount='.urlencode(number_format($service_amount, 2, '.', '')).'&';
            $data_service .= 'orderdescription='.urlencode('D2L service charge from '.$invoice_data->branch->name.' for '.(implode(', ', $invoice_names)));

            $result = self::createRequest($data);

            if ($result && isset($result[0]) && $result[0] == 1) {
                $return['status'] = 1;
                $return['transaction_id'] = $result[3];

                $result_service = self::createRequest($data_service);

                if ($result_service && isset($result_service[0]) && $result_service[0] == 1) {
                    $return['charge_transaction_id'] = $result_service[3];
                } else {
                    $return['status'] = 3;
                    $return['transaction_id'] = null;

                    self::createRefund($result[3], number_format($amount, 2, '.', ''), $private_key);
                }
            }
        }

        return $return;
    }

    protected static function createVoid($transactionid, $private_key) {
        $data  = '';
        $data .= 'security_key='.urlencode($private_key).'&';
        $data .= 'transactionid='.urlencode($transactionid).'&';
        $data .= 'type=void';

        return self::createRequest($data);
    }

    protected static function createRefund($transactionid, $amount = 0, $private_key) {
        $data  = '';
        $data .= 'security_key='.urlencode($private_key).'&';
        $data .= 'transactionid='.urlencode($transactionid).'&';
        
        if ($amount > 0) $data .= 'amount='.urlencode(number_format($amount,2, '.', '')).'&';

        $data .= 'type=refund';

        return self::createRequest($data);
    }

    protected static function createRequest($data) {
        if ($data == '') return false;

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, 'https://secure.networkmerchants.com/api/transact.php');
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_POST, 1);

        if (!($result = curl_exec($ch))) return false;

        curl_close($ch);
        unset($ch);
       
        $result = explode('&',$result);

        $result_data = array();

        foreach ($result as $item) {
            $item_data = explode('=', $item);

            if (isset($item_data[1]))$result_data[] = $item_data[1];
        }
        
        return $result_data;
    }
}