<?php
namespace App\Services;
use App\Models\Currency;

class Currencies {	

	public static function getPriceStr($value, $currency_id) {
		$str = $value;

		$currency = self::getCurrencyById($currency_id);

		if ($currency) {
			$str = $currency->symbol.round($value * $currency->value, $currency->after_point); 
		}

		return $str;
	}

	public static function getCurrencyById($id) {
		$data = false;

		$currency = Currency::where('id', $id)->first();

		if ($currency) $data = $currency;

		return $data;
	}
}