<?php
namespace App\Services;

use App\Models\Notification_message;

class Notificate {
        public static function createNotification($branch_id, $order_id, $message) {
        	$notificate = new Notification_message;

                $notificate->branch_id = $branch_id;
                $notificate->order_id = $order_id;
                $notificate->text = $message;
                $notificate->save(); 

                $ch = curl_init();

                curl_setopt($ch, CURLOPT_URL, 'https://app.dentist2lab.com/api/sendnotification.php?id='.$notificate->id); 
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        		curl_setopt($ch, CURLOPT_HEADER, 0);
        		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');

                $result_data = curl_exec($ch);

                curl_close($ch);
                unset($ch);
        }
}