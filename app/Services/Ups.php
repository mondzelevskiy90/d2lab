<?php
namespace App\Services;
use App\Models\Branch;
use App\Models\Order;

class Ups {

	private static $service_key = '9D830D8DBE1AE992';
	private static $service_user = 'DentistLab';
	private static $service_pass = 'M7248tSP';	
	private static $service_shipper_number = 'Y7946X';
	private static $service_url = 'https://wwwcie.ups.com/ups.app/xml/';  //<-------- test
	//private static $service_url = 'https://onlinetools.ups.com/ups.app/xml/';    //<-------- production

	public static function create($order_id = 0, $card_data, $from = 'branch_id', $to = 'to_branch_id') {
		$data = Order::with([
                'status_info',
                'delivery_info',
                'delivery_service'
            ])
            ->where('id', (int)$order_id)
            ->first();

		if (!$data || empty($data)) return false;

		$shipFromBranch = Branch::with([
				'locality_info' => function ($q) {
					$q->with(['country', 'region']);
				}
			])
			->where('id', (int)$data->{$from})
			->first();

		$shipToBranch = Branch::with([
				'locality_info' => function ($q) {
					$q->with(['country', 'region']);
				}
			])
			->where('id', (int)$data->{$to})
			->first();

		$accessRequestXML = new \SimpleXMLElement('<AccessRequest></AccessRequest>');

		$accessRequestXML->addChild('AccessLicenseNumber', self::$service_key);
		$accessRequestXML->addChild('UserId', self::$service_user);
		$accessRequestXML->addChild('Password', self::$service_pass);

		$shipmentConfirmRequestXML = new \SimpleXMLElement('<ShipmentConfirmRequest ></ShipmentConfirmRequest>');

		$request = $shipmentConfirmRequestXML->addChild('Request');
		$request->addChild('RequestAction', 'ShipConfirm');
		$request->addChild('RequestOption', 'nonvalidate');
		
		$labelSpecification = $shipmentConfirmRequestXML->addChild('LabelSpecification');
		$labelSpecification->addChild('HTTPUserAgent', '');
		$labelPrintMethod = $labelSpecification->addChild('LabelPrintMethod');
		$labelPrintMethod->addChild('Code', 'GIF');
		$labelImageFormat = $labelSpecification->addChild('LabelImageFormat');
		$labelImageFormat->addChild('Code', 'GIF');
		
		$shipment = $shipmentConfirmRequestXML->addChild('Shipment');
		$shipment->addChild('Description', '');
		$rateInformation = $shipment->addChild('RateInformation');
		$rateInformation->addChild('NegotiatedRatesIndicator', '');
		
		$shipper = $shipment->addChild('Shipper');
		$shipper->addChild('Name', $shipFromBranch->name);
		$shipper->addChild('PhoneNumber', self::clearStr($shipFromBranch->phone));
		$shipper->addChild('TaxIdentificationNumber', self::clearStr($shipFromBranch->tin));
		$shipper->addChild('ShipperNumber', self::$service_shipper_number);
		$shipperAddress = $shipper->addChild('Address');
		$shipperAddress->addChild('AddressLine1', $shipFromBranch->address);
		$shipperAddress->addChild('City', self::clearAddr($shipFromBranch->locality_info->name));
		$shipperAddress->addChild('StateProvinceCode', $shipFromBranch->locality_info->region->code);
		$shipperAddress->addChild('PostalCode', $shipFromBranch->zip_code);
		$shipperAddress->addChild('CountryCode', strtoupper($shipFromBranch->locality_info->country->short_code));

		$shipTo = $shipment->addChild('ShipTo');
		$shipTo->addChild('CompanyName', $shipToBranch->name);
		$shipTo->addChild('PhoneNumber', self::clearStr($shipToBranch->phone));
		$shipTo->addChild('TaxIdentificationNumber', self::clearStr($shipToBranch->tin));
		$shipToAddress = $shipTo->addChild('Address');
		$shipToAddress->addChild('AddressLine1', $shipToBranch->address);
		$shipToAddress->addChild('City', self::clearAddr($shipToBranch->locality_info->name));
		$shipToAddress->addChild('StateProvinceCode', $shipToBranch->locality_info->region->code);
		$shipToAddress->addChild('PostalCode', $shipToBranch->zip_code);
		$shipToAddress->addChild('CountryCode', strtoupper($shipToBranch->locality_info->country->short_code));
		
		$shipFrom = $shipment->addChild('ShipFrom');
		$shipFrom->addChild('CompanyName', $shipFromBranch->name);
		$shipFrom->addChild('PhoneNumber', self::clearStr($shipFromBranch->phone));
		$shipFrom->addChild('TaxIdentificationNumber', self::clearStr($shipFromBranch->tin));
		$shipFromAddress = $shipFrom->addChild('Address');
		$shipFromAddress->addChild('AddressLine1', $shipFromBranch->address);
		$shipFromAddress->addChild('City', self::clearAddr($shipFromBranch->locality_info->name));
		$shipFromAddress->addChild('StateProvinceCode', $shipFromBranch->locality_info->region->code);
		$shipFromAddress->addChild('PostalCode', $shipFromBranch->zip_code);
		$shipFromAddress->addChild('CountryCode', strtoupper($shipFromBranch->locality_info->country->short_code));
		
		$paymentInformation = $shipment->addChild('PaymentInformation');
		$shipmentCharge = $paymentInformation->addChild ('ShipmentCharge');
		$shipmentCharge->addChild ('Type', '01');
		$prepaid = $paymentInformation->addChild ( 'Prepaid' );

		
		$billShipper = $prepaid->addChild ( 'BillShipper' );
		/*
		$billShipperCredit = $billShipper->addChild ('CreditCard');
		$billShipperCredit->addChild ('Number', $card_data['ccnumber']);
		$billShipperCredit->addChild ('ExpirationDate', $card_data['ccexp'].'20');
		$billShipperCredit->addChild ('SecurityCode', $card_data['cvv']);
		$billShipperCredit->addChild ('Type', 04);
		$billShipperCreditAddr = $billShipperCredit->addChild ('Address');
		$billShipperCreditAddr->addChild('AddressLine1', $shipToBranch->address);
		$billShipperCreditAddr->addChild('City', self::clearAddr($shipToBranch->locality_info->name));
		$billShipperCreditAddr->addChild('StateProvinceCode', $shipToBranch->locality_info->region->code);
		$billShipperCreditAddr->addChild('PostalCode', $shipToBranch->zip_code);
		$billShipperCreditAddr->addChild('CountryCode', strtoupper($shipToBranch->locality_info->country->short_code));
		*/
		$billShipper->addChild ( 'AccountNumber', 'Y7846X' );

		$service = $shipment->addChild('Service');
		$service->addChild('Code', '02');
		$service->addChild('Description', '');
		
		$package = $shipment->addChild('Package');
		$package->addChild('Description', '');
		$packagingType = $package->addChild('PackagingType');
		$packagingType->addChild('Code', '02');
		$packagingType->addChild('Description', '');
		$packageWeight = $package->addChild( 'PackageWeight');
		$packageWeight->addChild('Weight', $data->delivery_service->weight);
		$packageWeight->addChild('UnitOfMeasurement');
		$packageWeight->addChild('Code', 'LBS');
		$dimensions = $package->addChild( 'Dimensions');		
		$dimensions->addChild('UnitOfMeasurement');
		$dimensions->addChild('Code', 'IN');
		$dimensions->addChild('Length', $data->delivery_service->length);
		$dimensions->addChild('Width', $data->delivery_service->width);
		$dimensions->addChild('Height', $data->delivery_service->height);
		
		$requestXML = $accessRequestXML->asXML().$shipmentConfirmRequestXML->asXML();
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, self::$service_url.'ShipConfirm');
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $requestXML);
		$response = curl_exec($ch);
		curl_close($ch);
			
		if ($response == false) return false;
			
	
		$resp = new \SimpleXMLElement($response);
		dd($response);
	}  

	public static function acceptRequest($responce_code) {
		$accessRequestXML = new \SimpleXMLElement('<AccessRequest></AccessRequest>');

		$accessRequestXML->addChild('AccessLicenseNumber', self::$service_key);
		$accessRequestXML->addChild('UserId', self::$service_user);
		$accessRequestXML->addChild('Password', self::$service_pass);
	
		$shipmentAcceptRequestXML = new \SimpleXMLElement('<ShipmentAcceptRequest ></ShipmentAcceptRequest >');

		$request = $shipmentAcceptRequestXML->addChild('Request');
		$request->addChild('RequestAction', '01');
		$shipmentAcceptRequestXML->addChild('ShipmentDigest', $responce_code);
	
		$requestXML = $accessRequestXML->asXML().$shipmentAcceptRequestXML->asXML();
	
		$ch = curl_init();
		curl_setopt( $ch, CURLOPT_URL, self::$service_url.'ShipAccept');
		curl_setopt( $ch, CURLOPT_POST, true);
		curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt( $ch, CURLOPT_POSTFIELDS, $requestXML);
		$response = curl_exec($ch);
		curl_close($ch);
	
		if ($response == false) return false;

		$resp = new \SimpleXMLElement($response);
		dd($resp);
		return $resp;
	}

	public static function cancel($number) {	
		$accessRequestXML = new \SimpleXMLElement('<AccessRequest></AccessRequest>');

		$accessRequestXML->addChild('AccessLicenseNumber', self::$service_key);
		$accessRequestXML->addChild('UserId', self::$service_user);
		$accessRequestXML->addChild('Password', self::$service_pass);
		
		$voidShipmentRequestXML = new \SimpleXMLElement('<VoidShipmentRequest ></VoidShipmentRequest >');
		
		$request = $voidShipmentRequestXML->addChild('Request');
		$request->addChild('RequestAction', '1');
		
		$voidShipmentRequestXML->addChild('ShipmentIdentificationNumber', $number);
		
		$requestXML = $accessRequestXML->asXML().$voidShipmentRequestXML->asXML();
		
		$form = array(
			'http' => array(
				'method' => 'POST',
				'header' => 'Content-type: application/x-www-form-urlencoded',
				'content' => $requestXML 
			) 
		);

		$request = stream_context_create($form);
		$browser = fopen(self::$service_url.'Void', 'rb', false, $request);
		
		if (!$browser) return false;
		
		$response = stream_get_contents($browser);
		fclose ($browser);
	} 

	protected static function clearStr($str) {
		return str_replace([' ', '+', '(', ')', '-'], '', $str);
	}

	protected static function clearAddr($str) {
		$pos = strpos($str, ',');

		if ($pos !== false) {
			$str = substr($str, 0, $pos);
		}

		return $str;
	}
}