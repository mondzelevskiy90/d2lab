<?php
namespace App\Services;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Image;
use QrCode;

class Img{
	private static $quality = 80;
	private static $icon_width = 50;
	private static $icon_height = 50;
	private static $thumb_width = 150;
	private static $thumb_height = 150;	

	public static function getThumb($image) {
		return self::squareImage($image, self::$thumb_width, self::$thumb_height);		
	}

	public static function getIcon($image) {
		return self::squareImage($image, self::$icon_width, self::$icon_height);
	}

	public static function saveImageVersions($image, $path) {
		$data = array();

        $data['icon'] = self::fitImage($image, $path, self::$icon_width, self::$icon_height, self::$quality);
        $data['thumb'] = self::fitImage($image, $path, self::$thumb_width, self::$thumb_height, self::$quality);
        $data['image'] = self::saveImage($image, $path);

        return $data;          
	}

	/*!!!Use only for uploaded images!!!*/
	public static function fitImage($image, $path, $width, $height, $quality = 100) {
		$name = time().'_'.$width.'x'.$height.'.'.$image->getClientOriginalExtension();

		self::checkPath(public_path().'/storage/'.$path);

		$new_image = Image::make($image->getRealPath())->fit($width,$height)->save(public_path().'/storage/'.$path.$name, $quality);

        return $path.$name;
	}

	public static function resizeImage($image, $width, $height, $quality = 100) {		
		$new_image = self::getNewImageName($image, $width, $height);

		if (!file_exists(public_path().'/storage/'.$new_image)) {
			$image = Image::make(public_path().'/storage/'.$image);
	        $image->resize($width, $height, function($constraint) {
			    $constraint->aspectRatio();
			});

			$canvas = Image::canvas($width, $height);
			$canvas->insert($image, 'center');
	        $canvas->save(public_path().'/storage/'.$new_image, $quality);
		}

		return url('/storage/'.$new_image);
	}
	
	/*!!!Use only for uploaded images!!!*/
	public static function saveImage($image, $path) {
		$name = time().'.'.$image->getClientOriginalExtension();

		self::checkPath(public_path().'/storage/'.$path);

		$image->move(public_path().'/storage/'.$path, $name);

		return $path.$name;
	}

	public static function squareImage($image, $width, $height) {
		if (!$image) return false;

		$thumb = self::getNewImageName($image, $width, $height);

		if (!file_exists(public_path().'/storage/'.$thumb)) {
			Image::make(public_path().'/storage/'.$image)->fit($width, $height)->save(public_path().'/storage/'.$thumb, self::$quality);
		}

		return url('/storage/'.$thumb);
	}

	/*!!!Use only for uploaded images!!!*/
	public static function cropUploadedImage($image, $path, $width = 50, $height = 50, $data = array()) {
		$file = self::saveImage($image, $path);

		$name = self::getNewImageName($file, $width, $height);

		if (!isset($data['width']) || !$data['width']) $data['width'] = 50;
		if (!isset($data['height']) || !$data['height']) $data['height'] = 50;
		if (!isset($data['x']) || !$data['x']) $data['x'] = 0;
		if (!isset($data['y']) || !$data['y']) $data['y'] = 0;

		Image::make(public_path().'/storage/'.$file)->crop((int)$data['width'], (int)$data['height'], (int)$data['x'], (int)$data['y'])->save(public_path().'/storage/'.$name, self::$quality);

		return $name;
	}

	public static function cropImage($image, $width = 50, $height = 50, $x = 0, $y = 0) {
		$thumb = self::getNewImageName($image, $width, $height);

		if (!file_exists(public_path().'/storage/'.$thumb)) {
			Image::make(public_path().'/storage/'.$image)->crop($width, $height, $x, $y)->save(public_path().'/storage/'.$thumb, self::$quality);
		}

		return url('/storage/'.$thumb);
	}

	public static function getQrCode($content, $name) {
		$file = '/storage/qrcodes/'.$name;

		if (!file_exists(public_path().$file)) {
 			QrCode::size(100)->format('png')->generate($content, public_path().$file);
		}

		return url($file);
	}

	protected static function getNewImageName($image, $width, $height) {
		$str = '_'.$width.'x'.$height;

		return str_replace(['.jpg', '.png', '.JPEG', '.jpeg'], [$str.'.jpg', $str.'.png', $str.'.JPEG', $str.'.jpeg'], $image);
	}

	protected static function checkPath($path) {
		$path = explode('/', $path);

		$path_str = '/';

		foreach ($path as $dir) {
			$path_str .= '/'.$dir;

			if (!file_exists($path_str)) {
				mkdir($path_str, 0777);
			}
		}
	}
}