<?php

namespace App\Http\Middleware;

use Closure;

class AccountRoleCheck {
    public function handle($request, Closure $next) {
        if ($request->user()) {
        	$pages_access = config('account_access.'.$request->user()->role_id);
        	
        	foreach ($pages_access as $page) {
        		if(strrpos($request->route()->uri, $page) !== false) return $next($request);
        	}
        } 
        
        return redirect()->route('account');              
    }
}
