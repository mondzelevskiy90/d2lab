<?php

namespace App\Http\Middleware;

use Closure;

class GuestCheck {
    public function handle($request, Closure $next) {
        if ($request->user() &&  $request->user()->role_id != config('roles.guest')) {
            return $next($request);
        } else {
            return redirect()->route('account');
        }        
    }
}
