<?php

namespace App\Http\Middleware;

use Closure;

class PostCheck {
    public function handle($request, Closure $next) {
        if ($request->method() == 'POST') {
            return $next($request);
        } else {
            return response()->json('Method is not supported');
        }        
    }
}
