<?php

namespace App\Http\Controllers\Backend;

use TCG\Voyager\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use TCG\Voyager\Facades\Voyager;
use App\Models\Banner;

class VoyagerBannerController extends Controller
{
    private $model;
    private $type;   

    public function __construct() {        
        $this->model = app('App\Models\Banner');
        $this->type = 'banners'; 
    }

    public function index(Request $request) {       
        $this->authorize('browse', $this->model);

        $type = $this->type;
        $model = $this->model;       
        $requests = $request->getQueryString();       

        $filter = [
            'filter_status' => $request->get('status'),
            'filter_search' => $request->get('search'),
        ];

        $many = Banner::orderBy('id', 'DESC');

        if ($filter['filter_status']) $many->where('status', $filter['filter_status']);       
        if ($filter['filter_search']) $many->where('name', 'LIKE', $filter['filter_search'].'%');

        $many = $many->paginate(20);

        return view('Backend.'.$type.'.browse', compact(
            'many',
            'type',
            'model',
            'filter',
            'requests'
        ));

    }    

    public function create(Request $request) { 
    //not used for this project    	
        $this->authorize('add', $this->model);        
    }

    public function store(Request $request) { 
    //not used for this project   	
        $this->authorize('add', $this->model);        
    }

    public function show(Request $request, $id)
    {
        //not used for this project
    }

    public function edit(Request $request, $id)
    {
        $this->authorize('edit', $this->model); 

        $data = Banner::where('id', (int)$id)->first();        
        $type = $this->type;
        $model = $this->model; 

        $requests = $request->getQueryString();       

        return view('Backend.'.$type.'.edit-add', compact(
            'data',
            'model',
            'type',
            'requests'
        ));
    }
   
    public function update(Request $request, $id)
    {
        $this->authorize('edit', $this->model);

        $data = Banner::where('id', (int)$id)->first();

        $messages = array(
            'name.required' => 'Name field is required!',                
            'name.max' => 'Name field contains more then 255 symbols!',            
        );

        $rules = array(
            'name' => 'required|max:255'
        );

        $request->validate($rules, $messages);            

        if (!$request->ajax()) { 
            $data->name = $request->input('name');
            $data->link = $request->input('link') ? $request->input('link') : null;
            $data->position = $request->input('position');
            $data->sort_order = $request->input('sort_order') ? (int)$request->input('sort_order') : 0;
            $data->status = $request->input('status');

            if ($request->file('image')) {
                $image_name = time().'.'.$request->file('image')->getClientOriginalExtension();                        
                $image = 'banners/'.$image_name;
                
                $request->file('image')->move(public_path().'/storage/banners/', $image_name); 

                $data->image = $image;
            }
                        
            $data->save();
        }

        return redirect()
            ->route('voyager.'.$this->type.'.index', $request->getQueryString())
            ->with([
                'message'    => __('voyager.generic.successfully_updated'),
                'alert-type' => 'success',
            ]);
    }

    public function destroy(Request $request, $id) {       
        $this->authorize('delete', $this->model);

        if (empty($id)) {           
            $ids = explode(',', $request->ids);
        } else {           
            $ids = array($id);
        }

        foreach ($ids as $id) {
            Banner::where('id', $id)->delete();            
        }

        return redirect()
            ->route('voyager.'.$this->type.'.index', $request->getQueryString())
            ->with([
                'message'    => __('voyager.generic.successfully_deleted'),
                'alert-type' => 'success'
            ]);
    }
}
