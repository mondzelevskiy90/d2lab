<?php

namespace App\Http\Controllers\Backend;

use TCG\Voyager\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use TCG\Voyager\Facades\Voyager;
use App\Models\Industry;
use App\Models\Option;
use App\Models\Option_to_service;
use App\Models\Option_to_price_service;

class VoyagerOptionController extends Controller
{
    private $model;
    private $type;   

    public function __construct() {        
        $this->model = app('App\Models\Option');
        $this->type = 'options'; 
    }

    public function index(Request $request) {       
        $this->authorize('browse', $this->model);

        $type = $this->type;
        $model = $this->model;       
        $requests = $request->getQueryString();       

        $filter = [
            'filter_status' => $request->get('status'),
            'filter_search' => $request->get('search')                        
        ];

        $many = Option::orderBy('id', 'DESC');

        if ($filter['filter_status']) $many->where('status', $filter['filter_status']);            
        if ($filter['filter_search']) $many->where('name', 'LIKE', $filter['filter_search'].'%');

        $many = $many->paginate(20);

        return view('Backend.'.$type.'.browse', compact(
            'many',
            'type',
            'model',
            'filter',
            'requests'
        ));

    }    

    public function create(Request $request) {  	
        $this->authorize('add', $this->model);

        $data = array();
        $data = (object)$data;

        $data->id = null;    
        $data->name = ''; 

        $industries = $this->getIndustries();   

        $type = $this->type;
        $model = $this->model; 
        $requests = $request->getQueryString();       

        return view('Backend.'.$type.'.edit-add', compact(
            'data',
            'type',
            'industries',
            'model', 
            'requests'
        ));       
    }

    public function store(Request $request) {    
        $this->authorize('add', $this->model);        

        $messages = array(
            'name.required' => 'Name field is required!',                
            'name.max' => 'Name field contains more then 255 symbols!',            
        );

        $rules = array(
            'name' => 'required|max:255'
        );   

        $request->validate($rules, $messages);            

        if (!$request->ajax()) { 
            $data = new Option;
            
            $data->name = $request->input('name');
            $data->industry_id = $request->input('industry_id');            
            $data->sort_order = $request->input('sort_order');            
            $data->status = $request->input('status');            
            $data->save();
        }

        return redirect()
            ->route('voyager.'.$this->type.'.index', $request->getQueryString())
            ->with([
                'message'    => __('voyager.generic.successfully_added_new'),
                'alert-type' => 'success',
            ]);     
    }

    public function show(Request $request, $id)
    {
        //not used for this project
    }

    public function edit(Request $request, $id)
    {
        $this->authorize('edit', $this->model); 

        $data = Option::with('industry')->where('id', (int)$id)->first();
        $industries = $this->getIndustries();             
        $type = $this->type;
        $model = $this->model; 
        $requests = $request->getQueryString();       

        return view('Backend.'.$type.'.edit-add', compact(
            'data',
            'type',
            'industries',
            'model', 
            'requests'
        ));
    }
   
    public function update(Request $request, $id)
    {
        $this->authorize('edit', $this->model);

        $data = Option::where('id', (int)$id)->first();

        $messages = array(
            'name.required' => 'Name field is required!',                
            'name.max' => 'Name field contains more then 255 symbols!',            
        );

        $rules = array(
            'name' => 'required|max:255'
        );   

        $request->validate($rules, $messages);            

        if (!$request->ajax()) { 
            $data->name = $request->input('name');
            $data->industry_id = $request->input('industry_id');         
            $data->sort_order = $request->input('sort_order');          
            $data->status = $request->input('status');            
            $data->save();
        }

        return redirect()
            ->route('voyager.'.$this->type.'.index', $request->getQueryString())
            ->with([
                'message'    => __('voyager.generic.successfully_updated'),
                'alert-type' => 'success',
            ]);
    }

    public function destroy(Request $request, $id) {       
        $this->authorize('delete', $this->model);

        if (empty($id)) {           
            $ids = explode(',', $request->ids);
        } else {           
            $ids = array($id);
        }

        foreach ($ids as $id) {            
            Option::where('id', $id)->delete();
            Option_to_service::where('option_id', $id)->delete();
            Option_to_price_service::where('option_id', $id)->delete();        
        }

        return redirect()
            ->route('voyager.'.$this->type.'.index', $request->getQueryString())
            ->with([
                'message'    => __('voyager.generic.successfully_deleted'),
                'alert-type' => 'success'
            ]);
    }

    

    public function getIndustries($parent_id = 0) {
        $data = array();

        $industries = Industry::where('parent_id', $parent_id)->get();

        foreach($industries as $industry) {
            $data[] = $industry;

            $childrens = $this->getIndustries($industry->id);

            if (count($childrens)) $data = array_merge($data, $childrens);
        }


        return $data;
    }
}