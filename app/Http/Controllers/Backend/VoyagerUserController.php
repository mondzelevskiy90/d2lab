<?php

namespace App\Http\Controllers\Backend;

use TCG\Voyager\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use TCG\Voyager\Facades\Voyager;
use App\Services\Userdata;
use App\Models\User;
use App\Models\Branch;
use App\Models\Portfolio;
use App\Models\Portfolio_image;
use App\Models\Price;
use App\Models\Price_service;
use App\Models\Price_service_custom;
use App\Models\Option_to_price_service;
use App\Models\Branch_staff;
use App\Models\Branch_list;
use App\Models\Branch_order_field;

class VoyagerUserController extends Controller
{
    private $model;
    private $type;   
    private $statuses;
    private $types;
    private $roles;

    public function __construct() {        
        $this->model = app('App\Models\User');
        $this->type = 'users';       
        $this->statuses = Userdata::getStatuses();
        $this->types = Userdata::getTypes();
        $this->roles = Userdata::getRoles();
    }

    public function index(Request $request) {       
        $this->authorize('browse', $this->model);

        $type = $this->type;
        $model = $this->model;
        $statuses = $this->statuses;
        $types = $this->types;
        $roles = $this->roles;
        $requests = $request->getQueryString();

        $subordination = Userdata::getSubordination(Auth::user()->role_id);

        $roles = array();

        foreach ($this->roles as $role) {
        	if (!in_array($role['id'], $subordination)) $roles[] = $role;
        }

        $branch = false;

        $filter = [
            'filter_status' => $request->get('status'),
            'filter_search' => $request->get('search'),
            'filter_type' => $request->get('type'),
            'filter_role' => $request->get('role_id'),
            'filter_branch' => $request->get('branch_id')        
        ];

        $many = User::with([
        	'role_info',
        	'type_info',
        	'status_info'
        ]);

        if ($filter['filter_status']) $many->where('status', $filter['filter_status']);
        if ($filter['filter_type']) $many->where('type', $filter['filter_type']);
        if ($filter['filter_role']) $many->where('role_id', $filter['filter_role']);
        if ($filter['filter_search']) {
            $many->where(function ($many) use ($filter) {
                $many->orWhere('email', 'LIKE', $filter['filter_search'].'%')                    
                    ->orWhere('name', 'LIKE', $filter['filter_search'].'%')
                    ->orWhere('surname', 'LIKE', $filter['filter_search'].'%');
            });
        }

        if ($filter['filter_branch']) {
            $many->whereIn('id', function($query) use ($filter) {
                $query->select('user_id')
                    ->from(with(new Branch_staff)->getTable())
                    ->where('branch_id', (int)$filter['filter_branch']);
            })
            ->orWhere('id', function($query) use ($filter) {
                $query->select('owner_id')
                    ->from(with(new Branch)->getTable())
                    ->where('id', (int)$filter['filter_branch']);
            });

            $branch = Branch::where('id', (int)$filter['filter_branch'])->first();
        }

        $many->whereNotIn('role_id', $subordination);

        $many = $many->orderBy('id', 'DESC')->paginate(20);

        return view('Backend.'.$type.'.browse', compact(
            'many',
            'type',
            'model',
            'filter',
            'statuses',
            'types',
            'roles',
            'branch',
            'requests'
        ));

    }    

    public function create(Request $request) { 
    //not used for this project    	
        $this->authorize('add', $this->model);

        $subordination = Userdata::getSubordination(Auth::user()->role_id); 
        $type = $this->type;
        $model = $this->model; 
        $statuses = $this->statuses;
        $types = $this->types;
        $requests = $request->getQueryString(); 

        $roles = array();

        foreach ($this->roles as $role) {
        	if (!in_array($role['id'], $subordination)) $roles[] = $role;
        }
        
        $data = array();
        $data = (object)$data;

        $data->id = null;    
        $data->name = ''; 

        return view('Backend.'.$type.'.edit-add', compact(
            'data',
            'type',
            'model',
            'statuses',
            'types',
            'roles',
            'requests'
        ));
    }

    public function store(Request $request) { 
    //not used for this project   	
        $this->authorize('add', $this->model);

        $messages = [
            //validation messages
        ];

        $request->validate([
            //validdation rules
        ], $messages);

        if (!$request->ajax()) {           

        }

        return redirect()
            ->route('voyager.'.$this->type.'.index', $request->getQueryString())
            ->with([
                'message'    => __('voyager.generic.successfully_added_new'),
                'alert-type' => 'success',
            ]);
    }

    public function show(Request $request, $id)
    {
        //not used for this project
    }

    public function edit(Request $request, $id)
    {
        $this->authorize('edit', $this->model); 

        $data = User::where('id', (int)$id)->first();
        $subordination = Userdata::getSubordination(Auth::user()->role_id); 
        $type = $this->type;
        $model = $this->model; 
        $statuses = $this->statuses;
        $types = $this->types;        
        $address = Userdata::getUserAddress($data);
        $requests = $request->getQueryString(); 

        $roles = array();

        foreach ($this->roles as $role) {
        	if (!in_array($role['id'], $subordination)) $roles[] = $role;
        }

        return view('Backend.'.$type.'.edit-add', compact(
            'data',
            'type',
            'model',
            'statuses',
            'types',
            'roles',
            'address',
            'requests'
        ));
    }
   
    public function update(Request $request, $id)
    {
        $this->authorize('edit', $this->model);
        $user = User::where('id', (int)$id)->first();

        $messages = array(
            'name.required' => 'Name field is required!',                
            'name.max' => 'Name field contains more then 255 symbols!',
            'surname.required' => 'Surname field is required!',               
            'surname.max' => 'Surname field contains more then 255 symbols!',
        );

        $rules = array(
            'name' => 'required|max:255',
            'surname' => 'required|max:255'
        );

        if ($user->email != $request->input('email')) {
        	$messages['email.required'] = 'Email field is required!';
            $messages['email.email'] = 'Email field content is not an e-mail';
            $messages['email.unique'] = 'E-mail is not unique!';

        	$rules['email'] = 'required|email|unique:users';
        }

        if ($request->input('type') != 1) {
            $messages['company.required'] = 'Company field is required!';
            $messages['company.max'] = 'Company field contains more then 255 symbols!'; 
            $messages['phone.required'] = 'Phone field is required!';
            $messages['avatar.image'] = 'Uploaded file is not an image!';
            $messages['avatar.mimes'] = 'Uploaded file format is not correct!';
            $messages['avatar.max'] = 'Uploaded file under 10Mb!';

            $rules['company'] = 'required|max:255'; 
            $rules['phone'] = 'required';
            $rules['avatar'] = 'image|mimes:jpeg,jpg,png|max:10240';
        }

        $request->validate($rules, $messages);            

        if (!$request->ajax()) { 
            $timestamp = time();

            $user->role_id = (int)$request->input('role_id');
            $user->type = (int)$request->input('type');
            $user->name = $request->input('name');
            $user->surname = $request->input('surname');
            $user->email = $request->input('email'); 
            $user->updated_at = $timestamp;
            $user->status = (int)$request->input('status');            

            if ($request->file('avatar')) {
            	$image = 'logos/default_logo.jpg';

                $image_name = time().'.'.$request->file('avatar')->getClientOriginalExtension();
                $image = 'logos/'.date('FY').'/'.$image_name;
                $request->file('avatar')->move(public_path().'/storage/logos/'.date('FY').'/', $image_name);

                $user->company = $request->input('company');                
                $user->phone = $request->input('phone'); 
                $user->avatar = $image;               
            }
            
            $user->save();
        }

        return redirect()
            ->route('voyager.'.$this->type.'.index', $request->getQueryString())
            ->with([
                'message'    => __('voyager.generic.successfully_updated'),
                'alert-type' => 'success',
            ]);
    }

    public function destroy(Request $request, $id) {       
        $this->authorize('delete', $this->model);

        if (empty($id)) {           
            $ids = explode(',', $request->ids);
        } else {           
            $ids = array($id);
        }

        foreach ($ids as $id) {
            User::where('id', $id)->delete(); 
            Branch_staff::where('user_id', $id)->delete();
            Branch_list::where('user_id', $id)->delete();

            $branches = Branch::where('owner_id', $id)->get(); 

            foreach ($branches as $branch) {
                $price = Price::where('branch_id', $branch->id)->first();

                if ($price) {
                    Price_service::where('price_id', $price->id)->delete();
                    Price_service_custom::where('price_id', $price->id)->delete();
                    Option_to_price_service::where('price_id', $price->id)->delete();
                }

                $staff =  Branch_staff::where('branch_id', $branch->id)->get(); 

                foreach ($staff as $user) {
                    User::where('id', $user->user_id)->delete();
                }     

                Branch_staff::where('branch_id', $branch->id)->delete();  
                Branch_order_field::where('branch_id', $branch->id)->delete();     

                $portfolio = Portfolio::where('branch_id', $branch->id)->first();

                if ($portfolio) {
                   $images = Portfolio_image::where('portfolio_id', $portfolio->id)->get();

                   foreach ($images as $image) {
                      unlink(public_path().'/storage/'.$image->image);
                   }

                   Portfolio_image::where('portfolio_id', $portfolio->id)->delete();
                } 

                Portfolio::where('branch_id', $branch->id)->delete();
            }  

            Branch::where('owner_id', $id)->delete(); 

            \App\Models\Order_chat_view::where('user_id', $id)->delete(); 
            \App\Models\Order_chat::where('user_id', $id)->delete();  
            \App\Models\Order_review::where('user_id', $id)->delete(); 
            \App\Models\Order_review_comment::where('user_id', $id)->delete();   
        }

        return redirect()
            ->route('voyager.'.$this->type.'.index', $request->getQueryString())
            ->with([
                'message'    => __('voyager.generic.successfully_deleted'),
                'alert-type' => 'success'
            ]);
    }
}
