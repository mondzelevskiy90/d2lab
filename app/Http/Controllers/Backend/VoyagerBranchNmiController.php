<?php

namespace App\Http\Controllers\Backend;

use TCG\Voyager\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use TCG\Voyager\Facades\Voyager;
use App\Services\Content;
use App\Models\Branch;
use App\Models\Branch_nmi_data;

class VoyagerBranchNmiController extends Controller
{
    private $model;
    private $type;

    public function __construct() {
        $this->model = app('App\Models\Branch_nmi_data');
        $this->type = 'branch-nmi-data';
    }

    public function index(Request $request) {       
        $this->authorize('browse', $this->model);

        $type = $this->type;
        $model = $this->model;
        $requests = $request->getQueryString();

        $filter = [
            'filter_branch' => $request->get('branch')    
        ];

        $many = Branch_nmi_data::with('branch');        

        if ($filter['filter_branch']) {
            $many->whereIn('branch_id', function($q) use ($filter) {
                $q->select('id')
                    ->from(with(new Branch)->getTable())
                    ->where('name', 'LIKE', $filter['filter_branch'].'%');
            });
        }

        $many = $many->orderBy('id', 'DESC')
            ->paginate(20);

        $branches = Branch::with('locality')->get();

        return view('Backend.'.$type.'.browse', compact(
            'many',
            'type',
            'branches',
            'model',
            'filter', 
            'requests'
        ));

    }

    public function create(Request $request) {  
    	$this->authorize('add', $this->model);
       
        $type = $this->type;
        $model = $this->model;        
        $requests = $request->getQueryString();
        
        $data = array();
        $data = (object)$data;

        $data->id = null;    
        $data->name = ''; 

        $branches = Branch::with('locality')
            ->whereNotIn('id', function ($q) {
                $q->select('branch_id')->from(with(new Branch_nmi_data)->getTable());
            })
            ->get();

        return view('Backend.'.$type.'.edit-add', compact(
            'data',
            'type',
            'model',  
            'branches',          
            'requests'
        ));

        
    }

    public function store(Request $request) {
    	$this->authorize('add', $this->model);

    	$messages = [
            'branch_id.required' => 'Branch field is required!',
            'branch_id.not_in' => 'Branch field is required!',
            'private_key.required' => 'Private key field is required!',
            'public_key.required' => 'Public key field is required!',
        ];

        $request->validate([
        	'branch_id' => 'required|not_in:0',
            'private_key' => 'required',
            'public_key' => 'required',
        ], $messages);

        if (!$request->ajax()) { 
            $data = new Branch_nmi_data;
           
            $data->branch_id = $request->input('branch_id');
            $data->private_key = $request->input('private_key');
            $data->public_key = $request->input('public_key');

            $data->save();
        }

        return redirect()
            ->route('voyager.'.$this->type.'.index', $request->getQueryString())
            ->with([
                'message'    => __('voyager.generic.success'),
                'alert-type' => 'success',
            ]);
    }

    public function show(Request $request, $id)
    {
        return redirect()
            ->route('voyager.'.$this->type.'.index', $request->getQueryString())
            ->with([
                'message'    => __('voyager.generic.success'),
                'alert-type' => 'success',
            ]);
    }

    public function edit(Request $request, $id)
    {
        $this->authorize('edit', $this->model); 

        $data = Branch_nmi_data::with('branch')->where('id', (int)$id)->first();

        $type = $this->type;
        $model = $this->model;  
        $requests = $request->getQueryString(); 

        return view('Backend.'.$type.'.edit-add', compact(
            'data',
            'content',
            'type',
            'model',           
            'requests'
        ));
    }
   
    public function update(Request $request, $id)
    {
        $this->authorize('edit', $this->model);
       
        $messages = [            
            'private_key.required' => 'Private key field is required!',
            'public_key.required' => 'Public key field is required!',
        ];

        $request->validate([        	
            'private_key' => 'required',
            'public_key' => 'required',
        ], $messages);

        if (!$request->ajax()) { 
            $data = Branch_nmi_data::where('id', (int)$id)->first();           
           
            $data->private_key = $request->input('private_key');
            $data->public_key = $request->input('public_key');

            $data->save();
        }

        return redirect()
            ->route('voyager.'.$this->type.'.index', $request->getQueryString())
            ->with([
                'message'    => __('voyager.generic.successfully_updated'),
                'alert-type' => 'success',
            ]);
    }

    public function destroy(Request $request, $id) { 
    	$this->authorize('delete', $this->model);

        if (empty($id)) {           
            $ids = explode(',', $request->ids);
        } else {           
            $ids = array($id);
        }

        foreach ($ids as $id) {
        	Branch_nmi_data::where('id', $id)->delete();
        }
        return redirect()
            ->route('voyager.'.$this->type.'.index', $request->getQueryString())
            ->with([
                'message'    => __('voyager.generic.successfully_deleted'),
                'alert-type' => 'success'
            ]);
    }

}