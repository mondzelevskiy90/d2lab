<?php

namespace App\Http\Controllers\Backend;

use TCG\Voyager\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use TCG\Voyager\Facades\Voyager;
use App\Models\Order;
use App\Models\Order_chat;
use App\Models\Order_chat_view;
use App\Models\Order_delivery;
use App\Models\Order_file;
use App\Models\Order_history;
use App\Models\Order_prescription;
use App\Models\Order_prescription_field; 
use App\Models\Order_total;
use App\Models\Order_review;
use App\Models\Order_review_comment;

class VoyagerOrdersController extends Controller {
    private $model;
    private $type;   

    public function __construct() {        
        $this->model = app('App\Models\Order');
        $this->type = 'orders'; 
    }

    public function index(Request $request) {       
        $this->authorize('browse', $this->model);

        $type = $this->type;
        $model = $this->model;       
        $requests = $request->getQueryString();       

        $filter = [            
            'filter_search' => $request->get('search')                   
        ];

        $many = Order::orderBy('id', 'DESC');
         
        if ($filter['filter_search']) {
            $many->where('id', 'LIKE', $filter['filter_search'].'%');
        }

        $many = $many->paginate(20);

        return view('Backend.'.$type.'.browse', compact(
            'many',
            'type',
            'model',
            'filter',
            'requests'
        ));

    }    

    public function create(Request $request) { 
        
    }

    public function store(Request $request) { 
       
    }

    public function show(Request $request, $id)
    {
        //not used for this project
    }

    public function edit(Request $request, $id)
    {
      //not used for this project 
    }
   
    public function update(Request $request, $id)
    {        
        //not used for this project 
    }

    public function destroy(Request $request, $id) {       
        $this->authorize('delete', $this->model);

        if (empty($id)) {           
            $ids = explode(',', $request->ids);
        } else {           
            $ids = array($id);
        }

        foreach ($ids as $id) {
            Order_chat::where('order_id', $id)->delete();
            Order_chat_view::where('order_id', $id)->delete();
            Order_delivery::where('order_id', $id)->delete();
            Order_file::where('order_id', $id)->delete();
            Order_history::where('order_id', $id)->delete();

            $order_prescriptions = Order_prescription::where('order_id', $id)->get();

            foreach($order_prescriptions as $order_prescription) {
                Order_prescription_field::where('prescription_id', $order_prescription->id)->delete();
            }

            Order_prescription::where('order_id', $id)->delete();
            Order_total::where('order_id', $id)->delete();
            Order_review::where('order_id', $id)->delete();
            Order_review_comment::where('order_id', $id)->delete();
            Order::where('id', $id)->delete();
        }

        return redirect()
            ->route('voyager.'.$this->type.'.index', $request->getQueryString())
            ->with([
                'message'    => __('voyager.generic.successfully_deleted'),
                'alert-type' => 'success'
            ]);
    }
}