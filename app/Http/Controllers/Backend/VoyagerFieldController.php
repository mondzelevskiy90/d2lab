<?php

namespace App\Http\Controllers\Backend;

use TCG\Voyager\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use TCG\Voyager\Facades\Voyager;
use App\Services\Content;
use App\Models\Industry_service;
use App\Models\Numbering_system;
use App\Models\Field;
use App\Models\Field_to_service;
use App\Models\Field_to_num_system;
use App\Models\Field_value;

class VoyagerFieldController extends Controller
{
    private $model;
    private $type;   

    public function __construct() {        
        $this->model = app('App\Models\Field');
        $this->type = 'fields'; 
    }

    public function index(Request $request) {       
        $this->authorize('browse', $this->model);

        $type = $this->type;
        $model = $this->model;       
        $requests = $request->getQueryString();       

        $filter = [
            'filter_status' => $request->get('status'),
            'filter_search' => $request->get('search')                        
        ];

        $many = Field::orderBy('id', 'DESC');

        if ($filter['filter_status']) $many->where('status', $filter['filter_status']);            
        if ($filter['filter_search']) $many->where('name', 'LIKE', $filter['filter_search'].'%');

        $many = $many->paginate(20);

        return view('Backend.'.$type.'.browse', compact(
            'many',
            'type',
            'model',
            'filter',
            'requests'
        ));

    }    

    public function create(Request $request) {  	
        $this->authorize('add', $this->model);

        $data = array();
        $data = (object)$data;

        $data->id = null;    
        $data->name = ''; 

        $services = Content::getServicesByIndustry(2);

        $numbering_systems = Numbering_system::where('status', 1)
            ->orderBy('sort_order', 'ASC')
            ->get();

        $selected_service_data = array();
        $selected_systems_data = array();
        $field_values = array();

        $type = $this->type;
        $model = $this->model; 
        $requests = $request->getQueryString();       

        return view('Backend.'.$type.'.edit-add', compact(
            'data',
            'services',
            'selected_service_data',
            'numbering_systems',
            'selected_systems_data',
            'field_values',
            'type',
            'model', 
            'requests'
        ));       
    }

    public function store(Request $request) {    
        $this->authorize('add', $this->model);        

        $messages = array(
            'name.required' => 'Name field is required!',                
            'name.max' => 'Name field contains more then 255 symbols!',            
        );

        $rules = array(
            'name' => 'required|max:255'
        );  

        if ($request->input('description')) {
            $messages['description.max'] = 'Description field contains more then 255 symbols!';
            $rules['description'] = 'max:255';
        } 

        if ($request->input('help')) {
            $messages['help.max'] = 'Help field contains more then 1000 symbols!';
            $rules['help'] = 'max:1000';
        } 

        if($request->file('image')) {
            $messages['image.image'] = 'Uploaded file is not an image!';
            $messages['image.mimes'] = 'Uploaded file format is not correct!';
            $messages['image.max'] = 'Uploaded file under 10Mb!';

            $rules['image'] = 'image|mimes:jpeg,jpg,png|max:10240';
        }

        $request->validate($rules, $messages);            

        if (!$request->ajax()) { 
            $data = new Field;
            
            $data->name = $request->input('name');            
            $data->field_type = $request->input('field_type');
            $data->required = $request->input('required');
            $data->default_prescription = $request->input('default_prescription');
            $data->sort_order = $request->input('sort_order');            
            $data->status = $request->input('status'); 

            if ($request->input('description')) $data->description = $request->input('description');
            if ($request->input('help')) $data->help = $request->input('help');

            if ($request->file('image')) {
                $image = '';

                $image_name = time().'.'.$request->file('image')->getClientOriginalExtension();
                $image = 'fields/'.date('FY').'/'.$image_name;
                $request->file('image')->move(public_path().'/storage/fields/'.date('FY').'/', $image_name);
                
                $data->image = $image;               
            }

            $data->save();

            if ($request->input('services')) {
                foreach ($request->input('services') as $key => $value) {
                    $row = new Field_to_service;

                    $row->field_id = $data->id;
                    $row->industry_service_id = $key;
                    $row->save();
                }
            }

            if ($request->input('systems')) {
                foreach ($request->input('systems') as $key => $value) {
                    $row = new Field_to_num_system;

                    $row->field_id = $data->id;
                    $row->numbering_system_id = $key;
                    $row->save();
                }
            }

            if ($request->input('field_values') && $request->input('use_values') && $request->input('use_values') == 1) {
                foreach ($request->input('field_values') as $key => $field_value) {
                    if ($field_value['value'] && $field_value['value'] != '') {
                        $row = new Field_value;

                        $image = null;

                        if ($request->file('field_values')) {
	                        foreach($request->file('field_values') as $key_file => $field_file) {
	                        	if ($key == $key_file) {
		                        	$image_name = time().'_value_'.$key_file.'.'.$field_file['image']->getClientOriginalExtension();
					                $image = 'fields/'.date('FY').'/'.$image_name;

					                $field_file['image']->move(public_path().'/storage/fields/'.date('FY').'/', $image_name);
	                   
	                        	}
	                        }
	                    }

                        $row->field_id = $data->id;
                        $row->value = $field_value['value'];
                        $row->image = $image;
                        $row->selected = isset($field_value['selected']) && $field_value['selected'] ? 1 : 0;
                        $row->sort_order = $field_value['sort_order'];
                        $row->status = $field_value['status'];
                        $row->save();
                    }
                }
            }
        }

        return redirect()
            ->route('voyager.'.$this->type.'.index', $request->getQueryString())
            ->with([
                'message'    => __('voyager.generic.successfully_added_new'),
                'alert-type' => 'success',
            ]);     
    }

    public function show(Request $request, $id)
    {
        //not used for this project
    }

    public function edit(Request $request, $id)
    {
        $this->authorize('edit', $this->model); 

        $data = Field::where('id', (int)$id)->first();
               
        $type = $this->type;
        $model = $this->model; 

        $services = Content::getServicesByIndustry(2);

        $numbering_systems = Numbering_system::where('status', 1)
            ->orderBy('sort_order', 'ASC')
            ->get();

        $selected_service_data = array();

        $selected_services = Field_to_service::where('field_id', $id)->get();

        foreach($selected_services as $selected_service) {
            $selected_service_data[$selected_service->industry_service_id] = $selected_service->industry_service_id;
        } 

        $selected_systems_data = array();

        $selected_systems = Field_to_num_system::where('field_id', $id)->get();

        foreach($selected_systems as $selected_system) {
            $selected_systems_data[$selected_system->numbering_system_id] = $selected_system->numbering_system_id;
        } 

        $field_values = Field_value::where('field_id', $id)->orderBy('sort_order', 'ASC')->get();
        
        $requests = $request->getQueryString();       

        return view('Backend.'.$type.'.edit-add', compact(
            'data',
            'services',
            'selected_service_data',
            'numbering_systems',
            'selected_systems_data',
            'field_values',
            'type',
            'model', 
            'requests'
        ));
    }
   
    public function update(Request $request, $id) {    	
        $this->authorize('edit', $this->model);

        $data = Field::where('id', (int)$id)->first();

        $messages = array(
            'name.required' => 'Name field is required!',                
            'name.max' => 'Name field contains more then 255 symbols!',            
        );

        $rules = array(
            'name' => 'required|max:255'
        ); 

        if ($request->input('description')) {
            $messages['description.max'] = 'Description field contains more then 255 symbols!';
            $rules['description'] = 'max:255';
        } 

        if ($request->input('help')) {
            $messages['help.max'] = 'Help field contains more then 1000 symbols!';
            $rules['help'] = 'max:1000';
        } 

        if($request->file('image')) {
            $messages['image.image'] = 'Uploaded file is not an image!';
            $messages['image.mimes'] = 'Uploaded file format is not correct!';
            $messages['image.max'] = 'Uploaded file under 10Mb!';

            $rules['image'] = 'image|mimes:jpeg,jpg,png|max:10240';
        }  

        $request->validate($rules, $messages);            

        if (!$request->ajax()) { 
            $data->name = $request->input('name');            
            $data->field_type = $request->input('field_type');
            $data->required = $request->input('required');
            $data->default_prescription = $request->input('default_prescription');
            $data->sort_order = $request->input('sort_order');            
            $data->status = $request->input('status'); 

            if ($request->input('description')) $data->description = $request->input('description');
            if ($request->input('help')) $data->help = $request->input('help');

            if ($request->file('image')) {
                $image = '';

                $image_name = time().'.'.$request->file('image')->getClientOriginalExtension();
                $image = 'fields/'.date('FY').'/'.$image_name;
                $request->file('image')->move(public_path().'/storage/fields/'.date('FY').'/', $image_name);
                
                $data->image = $image;               
            }

            $data->save();

            Field_to_service::where('field_id', $id)->delete();

            if ($request->input('services')) {
                foreach ($request->input('services') as $key => $value) {
                    $row = new Field_to_service;

                    $row->field_id = $id;
                    $row->industry_service_id = $key;
                    $row->save();
                }
            }

            Field_to_num_system::where('field_id', $id)->delete();

            if ($request->input('systems')) {
                foreach ($request->input('systems') as $key => $value) {
                    $row = new Field_to_num_system;

                    $row->field_id = $id;
                    $row->numbering_system_id = $key;
                    $row->save();
                }
            }

            Field_value::where('field_id', $id)->delete();

            if ($request->input('field_values')) {
                if ($request->input('use_values') && $request->input('use_values') == 1) {

                    foreach ($request->input('field_values') as $key => $field_value) {
                        if ($field_value['value'] && $field_value['value'] != '') {
                            $row = new Field_value;

							$image = null;

							if ($request->file('field_values')) {
	                            foreach($request->file('field_values') as $key_file => $field_file) {
	                            	if ($key == $key_file) {
			                        	$image_name = time().'_value_'.$key_file.'.'.$field_file['image']->getClientOriginalExtension();
						                $image = 'fields/'.date('FY').'/'.$image_name;

						                $field_file['image']->move(public_path().'/storage/fields/'.date('FY').'/', $image_name);
	                       
	                            	}
	                            }
	                        }

                            if (isset($field_value['image']) && $field_value['image']) {
                                $image = $field_value['image'];
                            }

                            $row->field_id = $id;
                            $row->value = $field_value['value'];
                            $row->image = $image;
                            $row->selected = isset($field_value['selected']) && $field_value['selected'] ? 1 : 0;
                            $row->sort_order = $field_value['sort_order'];
                            $row->status = $field_value['status'];
                            $row->save();
                        }
                        
                    }
                }
            }
        }

        return redirect()
            ->route('voyager.'.$this->type.'.index', $request->getQueryString())
            ->with([
                'message'    => __('voyager.generic.successfully_updated'),
                'alert-type' => 'success',
            ]);
    }

    public function destroy(Request $request, $id) {       
        $this->authorize('delete', $this->model);

        if (empty($id)) {           
            $ids = explode(',', $request->ids);
        } else {           
            $ids = array($id);
        }

        foreach ($ids as $id) {            
            Field::where('id', $id)->delete();
            Field_to_service::where('field_id', $id)->delete();
            Field_value::where('field_id', $id)->delete();  
            Field_to_num_system::where('field_id', $id)->delete();   
        }

        return redirect()
            ->route('voyager.'.$this->type.'.index', $request->getQueryString())
            ->with([
                'message'    => __('voyager.generic.successfully_deleted'),
                'alert-type' => 'success'
            ]);
    }
}