<?php

namespace App\Http\Controllers\Backend;

use TCG\Voyager\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use TCG\Voyager\Facades\Voyager;
use App\Models\Dashboard;
use App\Models\Dashboard_default;
use App\Models\Role;

class VoyagerDashboardController extends Controller
{
    private $model;
    private $type;

    public function __construct() {
        $this->model = app('App\Models\Dashboard');
        $this->type = 'dashboard';
    }

    public function index(Request $request) {       
        $this->authorize('browse', $this->model);

        $type = $this->type;
        $model = $this->model;
        $requests = $request->getQueryString();

        $filter = [
            'filter_status' => $request->get('status'),
            'filter_search' => $request->get('search')                        
        ];

        $many = Dashboard::orderBy('sort_order', 'ASC')
            ->orderBy('id', 'ASC');

        if ($filter['filter_status']) $many->where('status', $filter['filter_status']);            
        if ($filter['filter_search']) $many->where('name', 'LIKE', $filter['filter_search'].'%');

        $many = $many->paginate(20);

        return view('Backend.'.$type.'.browse', compact(
            'many',
            'type',
            'model',
            'filter', 
            'requests'
        ));

    }

    public function create(Request $request) {  
        $this->authorize('add', $this->model);

        $data = (object)[];

        $data->id = null;    
        $data->name = '';   

        $type = $this->type;
        $model = $this->model; 
        $requests = $request->getQueryString(); 

        $roles = Role::where('frontend', 1)->get();  
        $checked_roles = [];    

        return view('Backend.'.$type.'.edit-add', compact(
            'data',
            'type',
            'roles',
            'checked_roles',
            'model', 
            'requests'
        ));    
    }

    public function store(Request $request) {
        $this->authorize('add', $this->model);        

        $messages = array(
            'name.required' => 'Name field is required!',                
            'name.max' => 'Name field contains more then 255 symbols!',            
        );

        $rules = array(
            'name' => 'required|max:255'
        );   

        $request->validate($rules, $messages);            

        if (!$request->ajax()) { 
            $data = new Dashboard;
            
            $data->name = $request->input('name'); 
            $data->code = $request->input('code');
            $data->content = $request->input('content') ? $request->input('content') : null;                 
            $data->sort_order = $request->input('sort_order');            
            $data->status = $request->input('status');            
            $data->save();

            if ($request->input('roles') && is_array($request->input('roles'))) {
                foreach ($request->input('roles') as $key => $role) {
                    $dashboard_default = new Dashboard_default;

                    $dashboard_default->dashboard_id = $data->id;
                    $dashboard_default->role_id = $key;
                    $dashboard_default->save();
                }
            }
        }

        return redirect()
            ->route('voyager.'.$this->type.'.index', $request->getQueryString())
            ->with([
                'message'    => __('voyager.generic.successfully_added_new'),
                'alert-type' => 'success',
            ]);
    }

    public function show(Request $request, $id)
    {
        return redirect()
            ->route('voyager.'.$this->type.'.index', $request->getQueryString())
            ->with([
                'message'    => __('voyager.generic.success'),
                'alert-type' => 'success',
            ]);
    }

    public function edit(Request $request, $id)
    {
        $this->authorize('edit', $this->model); 

        $data = Dashboard::where('id', (int)$id)->first();

        $type = $this->type;
        $model = $this->model;  
        $requests = $request->getQueryString();  

        $roles = Role::where('frontend', 1)->get(); 
        $checked_roles = Dashboard_default::where('dashboard_id', $id)->pluck('role_id')->toArray();   

        return view('Backend.'.$type.'.edit-add', compact(
            'data',
            'content',
            'type',
            'model',
            'roles',
            'checked_roles',
            'requests'
        ));
    }
   
    public function update(Request $request, $id)
    {
        $this->authorize('edit', $this->model);

        $data = Dashboard::where('id', (int)$id)->first();

        $messages = array(
            'name.required' => 'Name field is required!',                
            'name.max' => 'Name field contains more then 255 symbols!',            
        );

        $rules = array(
            'name' => 'required|max:255'
        );   

        $request->validate($rules, $messages);            

        if (!$request->ajax()) { 
            $data->name = $request->input('name'); 
            $data->code = $request->input('code');
            $data->content = $request->input('content') ? $request->input('content') : null;           
            $data->sort_order = $request->input('sort_order');          
            $data->status = $request->input('status');            
            $data->save();

            Dashboard_default::where('dashboard_id', $id)->delete();

            if ($request->input('roles') && is_array($request->input('roles'))) {
                foreach ($request->input('roles') as $key => $role) {
                    $dashboard_default = new Dashboard_default;

                    $dashboard_default->dashboard_id = $id;
                    $dashboard_default->role_id = $key;
                    $dashboard_default->save();
                }
            }
        }

        return redirect()
            ->route('voyager.'.$this->type.'.index', $request->getQueryString())
            ->with([
                'message'    => __('voyager.generic.successfully_updated'),
                'alert-type' => 'success',
            ]);
    }

    public function destroy(Request $request, $id) { 
        $this->authorize('delete', $this->model);

        if (empty($id)) {           
            $ids = explode(',', $request->ids);
        } else {           
            $ids = array($id);
        }

        foreach ($ids as $id) {            
            Dashboard::where('id', $id)->delete();    
        }

        return redirect()
            ->route('voyager.'.$this->type.'.index', $request->getQueryString())
            ->with([
                'message'    => __('voyager.generic.successfully_deleted'),
                'alert-type' => 'success'
            ]);
    }
}