<?php

namespace App\Http\Controllers\Backend;

use TCG\Voyager\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use TCG\Voyager\Facades\Voyager;
use App\Services\Content;
use App\Models\Branch;
use App\Models\Order_review;
use App\Models\Order_review_comment;


class VoyagerReviewsController extends Controller
{
    private $model;
    private $type;

    public function __construct() {
        $this->model = app('App\Models\Order_review');
        $this->type = 'order-reviews';
    }

    public function index(Request $request) {       
        $this->authorize('browse', $this->model);

        $type = $this->type;
        $model = $this->model;
        $requests = $request->getQueryString();

        $filter = [
            'filter_status' => $request->get('status'),
            'filter_branch' => $request->get('branch'),
            'filter_moderation' => $request->get('moderation')         
        ];

        $many = Order_review::with(['branch', 'user'])
            ->select('id', 'order_id', 'user_id', 'branch_id', 'review', 'moderation', 'status', 'type_str');
        $many_comments = Order_review_comment::with(['branch', 'user'])
            ->select('id', 'order_id', 'user_id', 'branch_id', 'review', 'moderation', 'status', 'type_str');

        if ($filter['filter_status']) {
            $many->where('status', $filter['filter_status']);
            $many_comments->where('status', $filter['filter_status']);
        }

        $branch = false;

        if ($filter['filter_branch']) {
            $many->where('branch_id', $filter['filter_branch']);
            $many_comments->where('branch_id', $filter['filter_branch']);

            $branch = Branch::where('id', $filter['filter_branch'])->first();
        }

        if ($filter['filter_moderation']) {
            $many->where('moderation', $filter['filter_moderation']);
            $many_comments->where('moderation', $filter['filter_moderation']);
        }

        $many = $many->union($many_comments)
            ->orderBy('moderation', 'DESC')
            ->orderBy('id', 'DESC')
            ->paginate(20);

        return view('Backend.'.$type.'.browse', compact(
            'many',
            'type',
            'branch',
            'model',
            'filter', 
            'requests'
        ));

    }

    public function create(Request $request) {  
        return redirect()
            ->route('voyager.'.$this->type.'.index', $request->getQueryString())
            ->with([
                'message'    => __('voyager.generic.success'),
                'alert-type' => 'success',
            ]);
    }

    public function store(Request $request) {

        return redirect()
            ->route('voyager.'.$this->type.'.index', $request->getQueryString())
            ->with([
                'message'    => __('voyager.generic.success'),
                'alert-type' => 'success',
            ]);
    }

    public function show(Request $request, $id)
    {
        return redirect()
            ->route('voyager.'.$this->type.'.index', $request->getQueryString())
            ->with([
                'message'    => __('voyager.generic.success'),
                'alert-type' => 'success',
            ]);
    }

    public function edit(Request $request, $id)
    {
        $this->authorize('edit', $this->model); 

        if ($request->get('type_str') && $request->get('type_str') == 'order_review_comment') {
            $data = Order_review_comment::with(['branch', 'user'])
                ->where('id', (int)$id)
                ->first();
        } else {
            $data = Order_review::with(['branch', 'user'])
                ->where('id', (int)$id)
                ->first();
        }

        $type = $this->type;
        $model = $this->model;  
        $requests = $request->getQueryString();    

        return view('Backend.'.$type.'.edit-add', compact(
            'data',
            'content',
            'type',
            'model',
            'requests'
        ));
    }
   
    public function update(Request $request, $id)
    {
        $this->authorize('edit', $this->model);
       
        $messages = [
            //validation messages
        ];

        $request->validate([
            //validdation rules
        ], $messages);

        if (!$request->ajax()) { 
            if ($request->input('type_str') && $request->input('type_str') == 'order_review_comment') {
                $data = Order_review_comment::with(['branch', 'user'])
                ->where('id', (int)$id)
                ->first();
            } else {
                $data = Order_review::with(['branch', 'user'])
                    ->where('id', (int)$id)
                    ->first();
            }


            $data->moderation = (int)$request->input('moderation');

            if ($request->input('moderation') == 1) {
                $data->moderation_text = null;
            }

            $data->status = (int)$request->input('status');
            $data->save();
        }

        return redirect()
            ->route('voyager.'.$this->type.'.index', $request->getQueryString())
            ->with([
                'message'    => __('voyager.generic.successfully_updated'),
                'alert-type' => 'success',
            ]);
    }

    public function destroy(Request $request, $id) { 
        return redirect()
            ->route('voyager.'.$this->type.'.index', $request->getQueryString())
            ->with([
                'message'    => __('voyager.generic.successfully_deleted'),
                'alert-type' => 'success'
            ]);
    }

}