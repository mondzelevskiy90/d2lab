<?php

namespace App\Http\Controllers\Backend;

use TCG\Voyager\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use TCG\Voyager\Facades\Voyager;
use App\Services\Userdata;
use App\Models\Branch;
use App\Models\Portfolio;
use App\Models\Portfolio_image;

class VoyagerPortfolioController extends Controller
{
    private $model;
    private $type;   

    public function __construct() {        
        $this->model = app('App\Models\Portfolio');
        $this->type = 'portfolios'; 
    }

    public function index(Request $request) {       
        $this->authorize('browse', $this->model);

        $type = $this->type;
        $model = $this->model;       
        $requests = $request->getQueryString();       

        $filter = [            
            'filter_search' => $request->get('search')                   
        ];

        $many = Portfolio_image::with([
            'branch',
            'service'
        ]);
         
        if ($filter['filter_search']) {
            $many->whereIn('portfolio_id', function($query) use ($filter) {
                $query->select('id')
                    ->from(with(new Portfolio)->getTable())
                    ->whereIn('branch_id', function($query) use ($filter) {
                        $query->select('id')
                            ->from(with(new Branch)->getTable())                            
                            ->where('name', 'LIKE', $filter['filter_search'].'%');
                    });
            });
        }

        $many = $many->orderBy('id', 'DESC')->paginate(20);

        return view('Backend.'.$type.'.browse', compact(
            'many',
            'type',
            'model',
            'filter',
            'requests'
        ));

    }    

    public function create(Request $request) { 
    //not used for this project    	
        $this->authorize('add', $this->model);        
    }

    public function store(Request $request) { 
    //not used for this project   	
        $this->authorize('add', $this->model);        
    }

    public function show(Request $request, $id)
    {
        //not used for this project
    }

    public function edit(Request $request, $id)
    {
      //not used for this project 
    }
   
    public function update(Request $request, $id)
    {
        $this->authorize('edit', $this->model);
        //not used for this project 
    }

    public function destroy(Request $request, $id) {       
        $this->authorize('delete', $this->model);

        if (empty($id)) {           
            $ids = explode(',', $request->ids);
        } else {           
            $ids = array($id);
        }

        foreach ($ids as $id) {
            Portfolio_image::where('id', $id)->delete();          
        }

        return redirect()
            ->route('voyager.'.$this->type.'.index', $request->getQueryString())
            ->with([
                'message'    => __('voyager.generic.successfully_deleted'),
                'alert-type' => 'success'
            ]);
    }
}
