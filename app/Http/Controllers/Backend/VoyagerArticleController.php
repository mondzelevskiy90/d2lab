<?php

namespace App\Http\Controllers\Backend;

use TCG\Voyager\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use TCG\Voyager\Facades\Voyager;
use App\Services\Content;
use App\Models\Article;
use App\Models\Article_content;


class VoyagerArticleController extends Controller
{
    private $model;
    private $type;

    public function __construct() {
        $this->model = app('App\Models\Article');
        $this->type = 'articles';
    }

    public function index(Request $request) {       
        $this->authorize('browse', $this->model);

        $type = $this->type;
        $model = $this->model;
        $requests = $request->getQueryString();

        $filter = [
            'filter_status' => $request->get('status'),
            'filter_name' => $request->get('name')         
        ];

        $many = Article::orderBy('id', 'DESC');

        if ($filter['filter_status']) $many->where('status', $filter['filter_status']);

        if ($filter['filter_name']) {
            $many->whereHas('content', function($query) use ($filter) {
                $query->whereName($filter['filter_name']);
            });
        } else {
            $many->with('content');
        }

        $many = $many->paginate(20);

        return view('Backend.'.$type.'.browse', compact(
            'many',
            'type',
            'model',
            'filter', 
            'requests'
        ));

    }

    public function create(Request $request) {  
        $this->authorize('add', $this->model);

        $type = $this->type;
        $model = $this->model;
        
        $data = array();
        $data = (object)$data;

        $data->id = null;    
        $data->name = '';
        $content = Content::getLangContent();  
        $requests = $request->getQueryString();

        return view('Backend.'.$type.'.edit-add', compact(
            'data',
            'content',
            'type',
            'model',
            'requests'
        ));
    }

    public function store(Request $request) {
        $this->authorize('add', $this->model);

        $messages = [
            'image.image' => 'Uploaded file is not an image!',
            'image.mimes' => 'Uploaded file mime type is not correct!',
            'image.max' => 'Uploaded file under 10Mb!'
        ];

        $request->validate([
            'image' => 'image|mimes:jpeg,jpg,png|max:10240'
        ], $messages);

        if (!$request->ajax()) {  
            $image = '';

            if ($request->file('image')) {
                $image_name = time().'.'.$request->file('image')->getClientOriginalExtension();                        
                $image = 'articles/'.date('FY').'/'.$image_name;
                
                $request->file('image')->move(public_path().'/storage/articles/'.date('FY').'/', $image_name); 
            }

            $article = new Article;

            $article->slug = Content::createSlug($request->input('slug') ? $request->input('slug') : $request->input('content')[config('app.voyager_language_default')]['name'], $this->model);
            $article->image = $image;
            $article->sort_order = $request->input('sort_order');
            $article->status = $request->input('status');
            $article->created_at = time();
            $article->updated_at = time();
            $article->save();

            foreach ($request->input('content') as $lang => $value) {
                $article_content = new Article_content;

                $article_content->id = $article->id;
                $article_content->lang = $lang;
                $article_content->name = $value['name'];
                $article_content->meta_title = !empty($value['meta_title']) ? $value['meta_title'] : $value['name'];
                $article_content->meta_description = !empty($value['meta_description']) ? $value['meta_description'] : $value['name'];
                $article_content->description = !empty($value['description']) ? $value['description'] : '';
                $article_content->save();
            }

        }

        return redirect()
            ->route('voyager.'.$this->type.'.index', $request->getQueryString())
            ->with([
                'message'    => __('voyager.generic.successfully_added_new'),
                'alert-type' => 'success',
            ]);
    }

    public function show(Request $request, $id)
    {
        //not used for this project
    }

    public function edit(Request $request, $id)
    {
        $this->authorize('edit', $this->model); 

        $data = Article::with('content')
            ->where('id', (int)$id)
            ->first();

        $data->name = $data->content[0]->name;        
        $content = Content::getLangContent($data->content);

        $type = $this->type;
        $model = $this->model;  
        $requests = $request->getQueryString();    

        return view('Backend.'.$type.'.edit-add', compact(
            'data',
            'content',
            'type',
            'model',
            'requests'
        ));
    }
   
    public function update(Request $request, $id)
    {
        $this->authorize('edit', $this->model);
       
        $messages = [
            //validation messages
        ];

        $request->validate([
            //validdation rules
        ], $messages);

        if (!$request->ajax()) {           
            $article = Article::where('id', $id)->first();

            if ($article->slug != $request->input('slug')) {
                $article->slug = Content::createSlug($request->input('slug') ? $request->input('slug') : $request->input('content')[config('app.voyager_language_default')]['name'], $this->model);
            }
            

            if ($request->file('image')) {
                $image_name = time().'.'.$request->file('image')->getClientOriginalExtension();                        
                $image = 'articles/'.date('FY').'/'.$image_name;
                
                $request->file('image')->move(public_path().'/storage/articles/'.date('FY').'/', $image_name); 

                $article->image = $image;
            }
            
            $article->sort_order = $request->input('sort_order');
            $article->status = $request->input('status');
            $article->updated_at = time();
            $article->save();

            Article_content::where('id', $article->id)->delete();

            foreach ($request->input('content') as $lang => $value) {
                $article_content = new Article_content;

                $article_content->id = $article->id;
                $article_content->lang = $lang;
                $article_content->name = $value['name'];
                $article_content->meta_title = !empty($value['meta_title']) ? $value['meta_title'] : $value['name'];
                $article_content->meta_description = !empty($value['meta_description']) ? $value['meta_description'] : $value['name'];
                $article_content->description = !empty($value['description']) ? $value['description'] : '';
                $article_content->save();
            }
        }

        return redirect()
            ->route('voyager.'.$this->type.'.index', $request->getQueryString())
            ->with([
                'message'    => __('voyager.generic.successfully_updated'),
                'alert-type' => 'success',
            ]);
    }

    public function destroy(Request $request, $id) {       
        $this->authorize('delete', $this->model);

        if (empty($id)) {           
            $ids = explode(',', $request->ids);
        } else {           
            $ids = array($id);
        }

        foreach ($ids as $id) {
            Article::where('id', $id)->delete();
            Article_content::where('id', $id)->delete();
        }

        return redirect()
            ->route('voyager.'.$this->type.'.index', $request->getQueryString())
            ->with([
                'message'    => __('voyager.generic.successfully_deleted'),
                'alert-type' => 'success'
            ]);
    }

}