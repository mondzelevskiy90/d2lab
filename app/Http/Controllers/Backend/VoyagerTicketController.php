<?php

namespace App\Http\Controllers\Backend;

use TCG\Voyager\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use TCG\Voyager\Facades\Voyager;
use App\Services\Content;
use App\Models\Ticket;

class VoyagerTicketController extends Controller
{
    private $model;
    private $type;

    public function __construct() {
        $this->model = app('App\Models\Ticket');
        $this->type = 'tickets';
    }

    public function index(Request $request) {       
        $this->authorize('browse', $this->model);

        $type = $this->type;
        $model = $this->model;
        $requests = $request->getQueryString();

        $filter = [
            'filter_id' => $request->get('id'),
            'filter_user' => $request->get('user_id'),
            'filter_answer' => $request->get('has_answer'),    
        ];

        $many = Ticket::with('user');
        
        if ($filter['filter_id']) {
            $many->where('id', 'LIKE', $filter['filter_id'].'%');
        }

        if ($filter['filter_user']) {
            $many->where('user_id', $filter['filter_user']);
        }

        if ($filter['filter_answer'] && $filter['filter_answer'] == 2) {
            $many->where('answer', null);
        } 

        $many = $many->orderBy('id', 'DESC')
            ->paginate(20);

        $users = Ticket::with('user')->select('user_id')->distinct()->get();

        return view('Backend.'.$type.'.browse', compact(
            'many',
            'type',
            'users',
            'model',
            'filter', 
            'requests'
        ));

    }

    public function create(Request $request) {  
        return redirect()
            ->route('voyager.'.$this->type.'.index', $request->getQueryString())
            ->with([
                'message'    => __('voyager.generic.success'),
                'alert-type' => 'success',
            ]);
    }

    public function store(Request $request) {

        return redirect()
            ->route('voyager.'.$this->type.'.index', $request->getQueryString())
            ->with([
                'message'    => __('voyager.generic.success'),
                'alert-type' => 'success',
            ]);
    }

    public function show(Request $request, $id)
    {
        return redirect()
            ->route('voyager.'.$this->type.'.index', $request->getQueryString())
            ->with([
                'message'    => __('voyager.generic.success'),
                'alert-type' => 'success',
            ]);
    }

    public function edit(Request $request, $id)
    {
        $this->authorize('edit', $this->model); 

        $data = Ticket::with('user')->where('id', (int)$id)->first();

        $type = $this->type;
        $model = $this->model;  
        $requests = $request->getQueryString();    

        return view('Backend.'.$type.'.edit-add', compact(
            'data',
            'content',
            'type',
            'model',
            'requests'
        ));
    }
   
    public function update(Request $request, $id)
    {
        $this->authorize('edit', $this->model);
       
        $messages = [
            //validation messages
        ];

        $request->validate([
            //validdation rules
        ], $messages);

        if (!$request->ajax()) { 
            $data = Ticket::where('id', (int)$id)->first();
           
            $data->answer = $request->input('answer');
            $data->save();
        }

        return redirect()
            ->route('voyager.'.$this->type.'.index', $request->getQueryString())
            ->with([
                'message'    => __('voyager.generic.successfully_updated'),
                'alert-type' => 'success',
            ]);
    }

    public function destroy(Request $request, $id) { 
        return redirect()
            ->route('voyager.'.$this->type.'.index', $request->getQueryString())
            ->with([
                'message'    => __('voyager.generic.successfully_deleted'),
                'alert-type' => 'success'
            ]);
    }

}