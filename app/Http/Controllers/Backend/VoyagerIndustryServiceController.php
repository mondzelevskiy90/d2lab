<?php

namespace App\Http\Controllers\Backend;

use TCG\Voyager\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use TCG\Voyager\Facades\Voyager;
use App\Models\Industry;
use App\Models\Industry_service;
use App\Models\Option;
use App\Models\Option_to_service;

class VoyagerIndustryServiceController extends Controller
{
    private $model;
    private $type;   

    public function __construct() {        
        $this->model = app('App\Models\Industry_service');
        $this->type = 'industry-services'; 
    }

    public function index(Request $request) {       
        $this->authorize('browse', $this->model);

        $type = $this->type;
        $model = $this->model;       
        $requests = $request->getQueryString();       

        $filter = [
            'filter_status' => $request->get('status'),
            'filter_parent' => $request->get('parent'),
            'filter_search' => $request->get('search')                        
        ];

        $parents = Industry_service::where('status', 1)->get();

        $many = Industry_service::with([
            'parent',
            'industry'
        ]);

        if ($filter['filter_status']) $many->where('status', $filter['filter_status']);            
        if ($filter['filter_search']) $many->where('name', 'LIKE', $filter['filter_search'].'%');
        if ($filter['filter_parent']) $many->where('parent_id', $filter['filter_parent']);

        $many = $many->orderBy('id', 'DESC')->paginate(20);

        return view('Backend.'.$type.'.browse', compact(
            'many',
            'parents',
            'type',
            'model',
            'filter',
            'requests'
        ));

    }    

    public function create(Request $request) {  	
        $this->authorize('add', $this->model);

        $data = array();
        $data = (object)$data;

        $data->id = null;    
        $data->name = ''; 

        $industries = $this->getIndustries();
        $parents = $this->getIndustryServices(2); 

        $options = Option::where('industry_id', 2)
            ->where('status', 1)
            ->get();

        $type = $this->type;
        $model = $this->model; 
        $requests = $request->getQueryString();       

        return view('Backend.'.$type.'.edit-add', compact(
            'data',
            'type',
            'industries',
            'parents',
            'options',
            'model', 
            'requests'
        ));       
    }

    public function store(Request $request) {    
        $this->authorize('add', $this->model);        

        $messages = array(
            'name.required' => 'Name field is required!',                
            'name.max' => 'Name field contains more then 255 symbols!',            
        );

        $rules = array(
            'name' => 'required|max:255'
        );   

        $request->validate($rules, $messages);            

        if (!$request->ajax()) { 
            $data = new Industry_service;
            
            $data->name = $request->input('name');
            $data->industry_id = $request->input('industry_id');
            $data->parent_id = $request->input('parent_id');
            $data->sort_order = $request->input('sort_order');
            $data->default_list = $request->input('default_list');
            $data->price_for = $request->input('price_for') ? $request->input('price_for') : 1; //deprecated
            $data->available_statuses = $request->input('available_statuses') ? $request->input('available_statuses') : '1,2,3,4,5,6,7'; //deprecated
            $data->available_options = $request->input('available_options') ? $request->input('available_options') : 0; //deprecated
            $data->status = $request->input('status');

            if ($request->input('available_options2')) {
                $data->available_options2 = implode(',', $request->input('available_options2'));
            } else {
                $data->available_options2 = null;
            }

            $data->prescription_description = $request->input('prescription_description') ? $request->input('prescription_description') : null;  
            
                        
            $data->save();

            if ($request->input('options')) {
                foreach ($request->input('options') as $key => $value) {
                    $option = new Option_to_service;

                    $option->option_id = $key;
                    $option->industry_service_id = $data->id;
                    $option->save();
                }
            }
        }

        return redirect()
            ->route('voyager.'.$this->type.'.index', $request->getQueryString())
            ->with([
                'message'    => __('voyager.generic.successfully_added_new'),
                'alert-type' => 'success',
            ]);     
    }

    public function show(Request $request, $id)
    {
        //not used for this project
    }

    public function edit(Request $request, $id)
    {
        $this->authorize('edit', $this->model); 

        $data = Industry_service::with('parent', 'industry')->where('id', (int)$id)->first();
        $industries = $this->getIndustries();
        $parents = $this->getIndustryServices($data->industry_id);

        $options = Option::with([
                'selected' => function ($q) use ($id) {
                    $q->where('industry_service_id', $id);
                }
            ])
            ->where('industry_id', $data->industry_id)
            ->where('status', 1)
            ->get();
           
        $selected_options = Option_to_service::where('industry_service_id', $id)->get();    
        $type = $this->type;
        $model = $this->model; 
        $requests = $request->getQueryString();       

        return view('Backend.'.$type.'.edit-add', compact(
            'data',
            'type',
            'industries',
            'parents', 
            'options',           
            'selected_options',
            'model', 
            'requests'
        ));
    }
   
    public function update(Request $request, $id)
    {
        $this->authorize('edit', $this->model);

        $data = Industry_service::where('id', (int)$id)->first();

        $messages = array(
            'name.required' => 'Name field is required!',                
            'name.max' => 'Name field contains more then 255 symbols!',            
        );

        $rules = array(
            'name' => 'required|max:255'
        );   

        $request->validate($rules, $messages);            

        if (!$request->ajax()) { 
            $data->name = $request->input('name');
            $data->industry_id = $request->input('industry_id');
            $data->parent_id = $request->input('parent_id');
            $data->sort_order = $request->input('sort_order');
            $data->default_list = $request->input('default_list');
            $data->price_for = $request->input('price_for') ? $request->input('price_for') : 1; //deprecated
            $data->available_statuses = $request->input('available_statuses') ? $request->input('available_statuses') : '1,2,3,4,5,6,7'; //deprecated
            $data->available_options = $request->input('available_options') ? $request->input('available_options') : 0; //deprecated
            $data->status = $request->input('status');

            if ($request->input('available_options2')) {
                $data->available_options2 = implode(',', $request->input('available_options2'));
            } else {
                $data->available_options2 = null;
            }

            $data->prescription_description = $request->input('prescription_description') ? $request->input('prescription_description') : null;  
            
                        
            $data->save();

            Option_to_service::where('industry_service_id', $id)->delete();

            if ($request->input('options')) {
                foreach ($request->input('options') as $key => $value) {
                    $option = new Option_to_service;

                    $option->option_id = $key;
                    $option->industry_service_id = $id;
                    $option->save();
                }
            }
        }

        return redirect()
            ->route('voyager.'.$this->type.'.index', $request->getQueryString())
            ->with([
                'message'    => __('voyager.generic.successfully_updated'),
                'alert-type' => 'success',
            ]);
    }

    public function destroy(Request $request, $id) {       
        $this->authorize('delete', $this->model);

        if (empty($id)) {           
            $ids = explode(',', $request->ids);
        } else {           
            $ids = array($id);
        }

        foreach ($ids as $id) {            
            Industry_service::where('id', $id)->delete(); 
            Option_to_service::where('industry_service_id', $id)->delete();       
        }

        return redirect()
            ->route('voyager.'.$this->type.'.index', $request->getQueryString())
            ->with([
                'message'    => __('voyager.generic.successfully_deleted'),
                'alert-type' => 'success'
            ]);
    }

    

    public function getIndustries($parent_id = 0) {
        $data = array();

        $industries = Industry::where('parent_id', $parent_id)->get();

        foreach($industries as $industry) {
            $data[] = $industry;

            $childrens = $this->getIndustries($industry->id);

            if (count($childrens)) $data = array_merge($data, $childrens);
        }


        return $data;
    }

    public function getIndustryServices($industry_id, $parent_id = 0) {
        $data = array();

        $services = Industry_service::where('parent_id', $parent_id)
            ->where('industry_id', $industry_id)
            ->get();

        foreach($services as $service) {
            $data[] = $service;

            $childrens = $this->getIndustryServices($industry_id, $service->id);

            if (count($childrens)) $data = array_merge($data, $childrens);
        }

        return $data;
    }

    public function getservices(Request $request) {
        $data = false;

        if ($request->input('industry')) {
            $data = $this->getIndustryServices((int)$request->input('industry'));
        }

        return response()->json($data);
    }

    public function getoptions(Request $request) {
        $data = false;

        if ($request->input('industry')) {
            $data = Option::where('industry_id', (int)$request->input('industry'))
                ->where('status', 1)
                ->get();
        }

        return response()->json($data);
    }
}
