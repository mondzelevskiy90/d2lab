<?php

namespace App\Http\Controllers\Backend;

use TCG\Voyager\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use TCG\Voyager\Facades\Voyager;
use App\Services\Userdata;
use App\Models\Branch;
use App\Models\Branch_staff;
use App\Models\Price;
use App\Models\Portfolio;
use App\Models\Portfolio_image;
use App\Models\Branch_list;
use App\Models\Branch_order_field;
use App\Models\Price_service;
use App\Models\Price_service_custom;
use App\Models\Option_to_price_service;
use App\Models\User;

class VoyagerBranchController extends Controller
{
    private $model;
    private $type;   

    public function __construct() {        
        $this->model = app('App\Models\Branch');
        $this->type = 'branches'; 
    }

    public function index(Request $request) {       
        $this->authorize('browse', $this->model);

        $type = $this->type;
        $model = $this->model;       
        $requests = $request->getQueryString();       

        $filter = [
            'filter_status' => $request->get('status'),
            'filter_search' => $request->get('search'),
            'filter_verified' => $request->get('verified')            
        ];

        $many = Branch::with(['owner']);

        if ($filter['filter_status']) $many->where('status', $filter['filter_status']);
        if ($filter['filter_verified']) $many->where('verified', $filter['filter_verified']);        
        if ($filter['filter_search']) $many->where('name', 'LIKE', $filter['filter_search'].'%');

        $many = $many->orderBy('verified', 'DESC')->orderBy('id', 'DESC')->paginate(20);

        return view('Backend.'.$type.'.browse', compact(
            'many',
            'type',
            'model',
            'filter',
            'requests'
        ));

    }    

    public function create(Request $request) { 
    //not used for this project    	
        $this->authorize('add', $this->model);        
    }

    public function store(Request $request) { 
    //not used for this project   	
        $this->authorize('add', $this->model);        
    }

    public function show(Request $request, $id)
    {
        //not used for this project
    }

    public function edit(Request $request, $id)
    {
        $this->authorize('edit', $this->model); 

        $data = Branch::with('owner')->where('id', (int)$id)->first();        
        $type = $this->type;
        $model = $this->model;              
        $address = Userdata::getUserAddress($data);
        $price = Price::where('branch_id', $id)->first();

        $portfolio = Portfolio_image::where('portfolio_id', function($query) use ($id) {
                $query->select('id')
                    ->from(with(new Portfolio)->getTable())
                    ->where('branch_id', $id);
            })
            ->get();

        $staff = Branch_staff::with('user')->where('branch_id', $id)->get();

        $requests = $request->getQueryString(); 
        $latitude = false;
        $longitude = false;

        if ($data->geolocation) {
            $data->geolocation = json_decode($data->geolocation);

            $latitude = $data->geolocation->lat;
            $longitude = $data->geolocation->lon;
        }

        return view('Backend.'.$type.'.edit-add', compact(
            'data',
            'type',
            'model',            
            'address',
            'price',
            'staff',
            'portfolio',
            'latitude',
            'longitude',
            'requests'
        ));
    }
   
    public function update(Request $request, $id)
    {
        $this->authorize('edit', $this->model);

        $data = Branch::where('id', (int)$id)->first();

        $messages = array(
            'name.required' => 'Name field is required!',                
            'name.max' => 'Name field contains more then 255 symbols!',            
        );

        $rules = array(
            'name' => 'required|max:255'
        );

    	$messages['email.required'] = 'Email field is required!';
        $messages['email.email'] = 'Email field content is not an e-mail';
        $messages['phone.required'] = 'Phone field is required!';
        $messages['avatar.image'] = 'Uploaded file is not an image!';
        $messages['avatar.mimes'] = 'Uploaded file format is not correct!';
        $messages['avatar.max'] = 'Uploaded file under 10Mb!';
        $messages['latitude.min'] = 'Latitude field contains less then 10 symbols!';
        $messages['longitude.min'] = 'longitude field contains less then 10 symbols!';
        $messages['latitude.required_if'] = 'You can verified only if latitude selected!';
        $messages['longitude.required_if'] = 'You can verified only if longitude selected!';
        $messages['price.required_if'] = 'There are no any available price for this department!!!';

        $rules['email'] = 'required|email';
        $rules['phone'] = 'required';
        $rules['avatar'] = 'image|mimes:jpeg,jpg,png|max:10240';
        $rules['latitude'] = 'required_if:verified,==,1';
        $rules['longitude'] = 'required_if:verified,==,1';
        $rules['price'] = 'required_if:verified,==,1';      

        $request->validate($rules, $messages);            

        if (!$request->ajax()) { 
            $timestamp = time();
            
            $data->name = $request->input('name');
            $data->description = $request->input('description');
            $data->verified = (int)$request->input('verified');
            $data->status = (int)$request->input('status'); 
            $data->phone = $request->input('phone');
            $data->email = $request->input('email'); 
            $data->charge_value = (float)$request->input('charge_value'); 
            $data->updated_at = $timestamp;

            if ($request->input('latitude') && $request->input('longitude')) {
                $data->geolocation = '{"lat":"'.$request->input('latitude').'", "lon":"'.$request->input('longitude').'"}';
            }         

            if ($request->file('avatar')) {
            	$image = 'logos/default_logo.jpg';

                $image_name = time().'.'.$request->file('avatar')->getClientOriginalExtension();
                $image = 'logos/'.date('FY').'/'.$image_name;
                $request->file('avatar')->move(public_path().'/storage/logos/'.date('FY').'/', $image_name);

                $data->avatar = $image;               
            }
            
            $data->save();
        }

        return redirect()
            ->route('voyager.'.$this->type.'.index', $request->getQueryString())
            ->with([
                'message'    => __('voyager.generic.successfully_updated'),
                'alert-type' => 'success',
            ]);
    }

    public function destroy(Request $request, $id) {       
        $this->authorize('delete', $this->model);

        if (empty($id)) {           
            $ids = explode(',', $request->ids);
        } else {           
            $ids = array($id);
        }

        foreach ($ids as $id) {
            Branch::where('id', $id)->delete(); 
            Branch_list::where('branch_id', $id)->delete();
            Branch_order_field::where('branch_id', $id)->delete();

            $price = Price::where('branch_id', $id)->first();

            if ($price) {
                Price_service::where('price_id', $price->id)->delete();
                Price_service_custom::where('price_id', $price->id)->delete();
                Option_to_price_service::where('price_id', $price->id)->delete();
            }

            $staff =  Branch_staff::where('branch_id', $id)->get(); 

            foreach ($staff as $user) {
                User::where('id', $user->user_id)->delete();
            }     

            Branch_staff::where('branch_id', $id)->delete();          

            $portfolio = Portfolio::where('branch_id', $id)->first();

            if ($portfolio) {
               $images = Portfolio_image::where('portfolio_id', $portfolio->id)->get();

               foreach ($images as $image) {
                  unlink(public_path().'/storage/'.$image->image);
               }

               Portfolio_image::where('portfolio_id', $portfolio->id)->delete();
            } 

            Portfolio::where('branch_id', $id)->delete();         
        }

        return redirect()
            ->route('voyager.'.$this->type.'.index', $request->getQueryString())
            ->with([
                'message'    => __('voyager.generic.successfully_deleted'),
                'alert-type' => 'success'
            ]);
    }

    public function getbranches(Request $request) {
        $data = false;

        if ($request->input('search')) {
            $branches = Branch::with('owner')->where('name', 'LIKE', $request->input('search').'%')->get();

            foreach ($branches as $branch) {
                $data[] = array(
                    'id' => $branch->id, 
                    'text' => $branch->name.(isset($branch->owner->company) ? ' ('.$branch->owner->company.')' : '')
                );
            }
        }

        return response()->json($data);
    }
}
