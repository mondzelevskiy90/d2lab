<?php

namespace App\Http\Controllers\Backend;

use TCG\Voyager\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use TCG\Voyager\Facades\Voyager;
use App\Models\Delivery;


class VoyagerDeliveryController extends Controller
{
    private $model;
    private $type;   

    public function __construct() {        
        $this->model = app('App\Models\Delivery');
        $this->type = 'delivery'; 
    }

    public function index(Request $request) {       
        $this->authorize('browse', $this->model);

        $type = $this->type;
        $model = $this->model;       
        $requests = $request->getQueryString();       

        $filter = [
            'filter_status' => $request->get('status'),
            'filter_search' => $request->get('search')                        
        ];

        $many = Delivery::orderBy('sort_order', 'ASC')
            ->orderBy('id', 'DESC');

        if ($filter['filter_status']) $many->where('status', $filter['filter_status']);            
        if ($filter['filter_search']) $many->where('name', 'LIKE', $filter['filter_search'].'%');

        $many = $many->paginate(20);

        return view('Backend.'.$type.'.browse', compact(
            'many',
            'type',
            'model',
            'filter',
            'requests'
        ));

    }    

    public function create(Request $request) {  	
        $this->authorize('add', $this->model);

        $data = array();
        $data = (object)$data;

        $data->id = null;    
        $data->name = '';   

        $type = $this->type;
        $model = $this->model; 
        $requests = $request->getQueryString();       

        return view('Backend.'.$type.'.edit-add', compact(
            'data',
            'type',
            'model', 
            'requests'
        ));       
    }

    public function store(Request $request) {    
        $this->authorize('add', $this->model);        

        $messages = array(
            'name.required' => 'Name field is required!',                
            'name.max' => 'Name field contains more then 255 symbols!',            
        );

        $rules = array(
            'name' => 'required|max:255'
        );   

        $request->validate($rules, $messages);            

        if (!$request->ajax()) { 
            $data = new Delivery;
            
            $data->name = $request->input('name');                 
            $data->sort_order = $request->input('sort_order'); 
            $data->code = $request->input('code');  
            $data->tracking_link = $request->input('tracking_link') ? $request->input('tracking_link') : null;
            $data->status = $request->input('status');            
            $data->save();
        }

        return redirect()
            ->route('voyager.'.$this->type.'.index', $request->getQueryString())
            ->with([
                'message'    => __('voyager.generic.successfully_added_new'),
                'alert-type' => 'success',
            ]);     
    }

    public function show(Request $request, $id)
    {
        //not used for this project
    }

    public function edit(Request $request, $id)
    {
        $this->authorize('edit', $this->model); 

        $data = Delivery::where('id', (int)$id)->first();
              
        $type = $this->type;
        $model = $this->model; 
        $requests = $request->getQueryString();       

        return view('Backend.'.$type.'.edit-add', compact(
            'data',
            'type',
            'model', 
            'requests'
        ));
    }
   
    public function update(Request $request, $id)
    {
        $this->authorize('edit', $this->model);

        $data = Delivery::where('id', (int)$id)->first();

        $messages = array(
            'name.required' => 'Name field is required!',                
            'name.max' => 'Name field contains more then 255 symbols!',            
        );

        $rules = array(
            'name' => 'required|max:255'
        );   

        $request->validate($rules, $messages);            

        if (!$request->ajax()) { 
            $data->name = $request->input('name');    
            $data->code = $request->input('code'); 
            $data->tracking_link = $request->input('tracking_link') ? $request->input('tracking_link') : null;
            $data->sort_order = $request->input('sort_order');          
            $data->status = $request->input('status');            
            $data->save();
        }

        return redirect()
            ->route('voyager.'.$this->type.'.index', $request->getQueryString())
            ->with([
                'message'    => __('voyager.generic.successfully_updated'),
                'alert-type' => 'success',
            ]);
    }

    public function destroy(Request $request, $id) {       
        $this->authorize('delete', $this->model);

        if (empty($id)) {           
            $ids = explode(',', $request->ids);
        } else {           
            $ids = array($id);
        }

        foreach ($ids as $id) {            
            Delivery::where('id', $id)->delete();    
        }

        return redirect()
            ->route('voyager.'.$this->type.'.index', $request->getQueryString())
            ->with([
                'message'    => __('voyager.generic.successfully_deleted'),
                'alert-type' => 'success'
            ]);
    }

}