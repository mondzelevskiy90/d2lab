<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\Content;
use App\Models\Branch;
use App\Models\Branch_staff;
use App\Models\Order;
use App\Models\Order_review;
use Auth;

class ReviewsController extends Controller {
    public function index(Request $request) {
        $filters = array();
        $branches_list = array();

        $data = Order_review::with('branch')
            ->select('order_id', 'branch_id')
            ->where('status', 1)
            ->orderBy('order_id', 'DESC');

        switch (Auth::user()->role_id) {
            case config('roles.clinic_head'):
                $branches_arr = array();
                $branches = Branch::where('owner_id', Auth::user()->id)->get();                

                foreach ($branches as $branch) {
                    $branches_arr[] = $branch->id;
                }

                $branches_list = Order::with('to_branch')
                    ->select('to_branch_id')
                    ->whereIN('branch_id', $branches_arr)
                    ->whereIn('status', [17,18])
                    ->distinct()
                    ->get();

                $branches_list_arr = array();

                foreach ($branches_list as $order) {
                    $branches_list_arr[] = $order->to_branch_id;
                }

                $data->whereIn('branch_id', $branches_list_arr);

                if ($request->input('branch')) {
                    $filters['branch'] = $request->input('branch');

                    $data->where('branch_id', (int)$filters['branch']);
                } 

                break;

            case config('roles.clinic_staff'):
                $staff = Branch_staff::where('user_id', Auth::user()->id)->first();

                if ($staff) {  
                    $branches_list = Order::with('to_branch')
                        ->select('to_branch_id')
                        ->where('branch_id', $staff->branch_id)
                        ->whereIn('status', [17,18])
                        ->distinct()
                        ->get();

                    $branches_list_arr = array();

                    foreach ($branches_list as $order) {
                        $branches_list_arr[] = $order->to_branch_id;
                    }

                    $data->whereIn('branch_id', $branches_list_arr);

                    if ($request->input('branch')) {
                        $filters['branch'] = $request->input('branch');

                        $data->where('branch_id', (int)$filters['branch']);
                    } 
                } else {                   
                    $data->where('status', '-1'); 
                }
                
                break;

            case config('roles.doctor'):
                $staff = Branch_staff::where('user_id', Auth::user()->id)->first();

                if ($staff) {  
                    $branches_list = Order::with('to_branch')
                        ->select('to_branch_id')
                        ->where('user_id', Auth::user()->id)
                        ->where('branch_id', $staff->branch_id)
                        ->whereIn('status', [17,18])
                        ->distinct()
                        ->get();

                    $branches_list_arr = array();

                    foreach ($branches_list as $order) {
                        $branches_list_arr[] = $order->to_branch_id;
                    }

                    $data->whereIn('branch_id', $branches_list_arr);

                    if ($request->input('branch')) {
                        $filters['branch'] = $request->input('branch');

                        $data->where('branch_id', (int)$filters['branch']);
                    }  
                } else {
                    $data->where('status', '-1');
                }
                
                break;

            case config('roles.lab_head'):
                $branches_arr = array();
                $branches = Branch::where('owner_id', Auth::user()->id)->get(); 

                $branches_list = $branches;               

                foreach ($branches as $branch) {
                    $branches_arr[] = $branch->id;
                }

                if ($request->input('branch')) {
                    $filters['branch'] = $request->input('branch');

                    $data->where('branch_id', (int)$filters['branch']);
                } else {
                    $data->whereIn('branch_id', $branches_arr);
                }
                
                break;

            case config('roles.lab_staff'):
                $staff = Branch_staff::where('user_id', Auth::user()->id)->first();

                if ($staff) { 
                    $data->where('branch_id', $staff->branch_id); 
                } else {
                    $data->where('status', '-1');
                }       

                break;

            default:
                $data->where('status', '-1'); 

                break;
        }    

        $data = $data->distinct('order_id')->paginate(20);

    	$breadcrumbs = array();

    	$breadcrumbs[] = array(
			'text' => __('translations.home'),
			'href' => route('home')
		);

        $breadcrumbs[] = array(
            'text' => __('translations.account'),
            'href' => route('account')
        );

		$breadcrumbs[] = array(
			'text' => __('translations.reviews'),
			'href' => route('reviews')
		);

        $seo = (object)[
            'name' => __('translations.reviews'),
            'meta_title' => __('translations.reviews'),
            'meta_description' => 'Dentist2Lab service reviews page',
        ];

        $requests = $request->getQueryString();

    	return view('Frontend.pages.account.reviews.index', compact(
    		'data', 
            'branches_list',
    		'seo',
    		'breadcrumbs',
            'requests'
    	));
    }

    public function edit(Request $request, $id) {
        $data = Order::with([
                'to_branch',                
                'reviews'
            ])
            ->where('id', (int)$id)
            ->firstOrFail();

        $data->review_access = Content::getReviewAccess($id, Auth::user());

        $breadcrumbs = array();

        $breadcrumbs[] = array(
            'text' => __('translations.home'),
            'href' => route('home')
        );

        $breadcrumbs[] = array(
            'text' => __('translations.account'),
            'href' => route('account')
        );

        $breadcrumbs[] = array(
            'text' => __('translations.reviews'),
            'href' => route('reviews')
        );

        $breadcrumbs[] = array(
            'text' => __('translations.order'). ' ' .__('translations.reviews'),
            'href' => route('reviews')
        );

        $seo = (object)[
            'name' => __('translations.review'),
            'meta_title' => __('translations.review'),
            'meta_description' => 'Dentist2Lab service viewing order review page',
        ];

        $requests = $request->getQueryString();

        return view('Frontend.pages.account.reviews.edit', compact(
            'data', 
            'seo',
            'breadcrumbs',
            'requests'
        ));
    }
}