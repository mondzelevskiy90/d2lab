<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Services\Img;
use App\Services\Google;
use App\Models\User;
use App\Models\Branch;
use App\Models\Country;
use Auth;
use Mail;

class RegistrationController extends Controller {  

    public function index(Request $request) {       
        if (Auth::check()) redirect(route('account'));

        $type = false;

        if ($request->input('type')) $type = $request->input('type');

        if ($request->method() == 'POST') {
            $messages = array(
                'name.required' => 'Field is required!',                
                'name.max' => 'Field contains more then 255 symbols!',
                'surname.required' => 'Field is required!',               
                'surname.max' => 'Field contains more then 255 symbols!',
                'email.required' => 'Field is required!',
                'email.email' => 'Field content is not an e-mail',
                'email.unique' => 'E-mail is not unique!',
                'password.required' => 'Field is required!',
                'password.min' => 'Field contains less then 6 symbols!',
                'password.max' => 'Field contains more then 255 symbols!',
                'password.required_with' => 'Field Password confirmation is not defined!',
                'password.same' => 'Field not same to field Password confirmation!',
                'password_confirmation.required' => 'Field is required!'
            );

            $rules = array(
                'name' => 'required|max:255',
                'surname' => 'required|max:255',
                'email' => 'required|email|unique:users',
                'password' => 'required|min:6|max:255|required_with:password_confirmation|same:password_confirmation',
                'password_confirmation' => 'required'
            );

            if ($type) {
                $messages['company.required'] = 'Field is required!';
                $messages['company.max'] = 'Field contains more then 255 symbols!';
                $messages['zip_code.required'] = 'Field is required!';
                $messages['country.required'] = 'Field is required!';
                $messages['country.not_in'] = 'Field is not selected!';
                $messages['region.required'] = 'Field is required!';
                $messages['region.not_in'] = 'Field is not selected!';
                $messages['locality.required'] = 'Field is required!';
                $messages['locality.not_in'] = 'Field is not selected!';
                $messages['address.required'] = 'Field is required!';
                $messages['phone.required'] = 'Field is required!';
                $messages['logo.image'] = 'Uploaded file is not an image!';
                $messages['logo.mimes'] = 'Uploaded file format is not correct!';
                $messages['logo.max'] = 'Uploaded file under 10Mb!';

                $rules['company'] = 'required|max:255'; 
                $rules['zip_code'] = 'required';
                $rules['country'] = 'required|not_in:0';
                $rules['region'] = 'required|not_in:0';
                $rules['locality'] = 'required|not_in:0';
                $rules['address'] = 'required';
                $rules['phone'] = 'required';
                $rules['logo'] = 'image|mimes:jpeg,jpg,png|max:10240';
            }

            $request->validate($rules, $messages);

            $user = new User;

            $timestamp = time();
            $name = $request->input('name');
            $surname = $request->input('surname');
            $email = $request->input('email');
            $password = $request->input('password');

            $role_id = config('roles.guest');

            if ($type && $type == config('types.clinic')) $role_id = config('roles.clinic_head');
            if ($type && $type == config('types.lab')) $role_id = config('roles.lab_head');

            $user->role_id = $role_id;
            $user->type = $type ? $type : config('types.guest');
            $user->name = $name;
            $user->surname = $surname;
            $user->email = $email;
            $user->password = Hash::make($password);
            $user->email_verified_at = null;
            $user->settings = '{"locale":"en"}';
            $user->created_at = $timestamp;
            $user->updated_at = $timestamp;
            $user->status = (int)$request->input('social');

            $image = 'logos/default_logo.png';

            if ($type) {
                if ($request->file('logo')) { 
                    if ($request->input('logo')['status'] == 1) {
                        $data->avatar = Img::cropUploadedImage($request->file('logo'), '/logos/'.date('FY').'/', 50, 50, $request->input('data_logo'));
                    } else {
                        $image = Img::fitImage($request->file('logo'), '/logos/'.date('FY').'/', 275, 275, 80);
                    }   
                }

                $user->company = $request->input('company');
                $user->zip_code = $request->input('zip_code');
                $user->locality = $request->input('locality');
                $user->address = $request->input('address');
                $user->phone = $request->input('phone'); 
            }

            $user->logo = $image;  

            $image = 'users/default.png'; 

            if ($request->input('social_img')) {
                $file_opened = fopen($request->input('social_img'), 'rb');

                $file_path = public_path().'/storage/users/'.date('FY').'/'.$timestamp.'.jpg';

                if (!file_exists(public_path().'/storage/users/'.date('FY'))) {
                    mkdir(public_path().'/storage/users/'.date('FY'), 0777, true);
                } 
        
                if ($file_opened && file_put_contents($file_path, $file_opened)) {
                    $image = 'users/'.date('FY').'/'.$timestamp.'.jpg';
                }
            }

            $user->avatar = $image;            
            
            $user->save();

            if ($request->input('social') == config('statuses.email')) {
                $content = [
                    'type' => $type,
                    'name' => $name.' '.$surname,
                    'email' => $email,
                    'password' => $password,
                    'url' => route('login', ['param' => \Illuminate\Support\Facades\Crypt::encrypt($email)])              
                ];

                Mail::send('Frontend.emails.registration', $content, function ($message) use ($request, $email) {
                    $message->to($email)->subject('Registration info');
                });

                if ($role_id == config('roles.clinic_head') || $role_id == config('roles.lab_head')) {
                    $emails = setting('admin.admin_emails');

                    Mail::send('Frontend.emails.adm_registration', $content, function ($message) use ($request, $emails) {
                        $message->to($emails)->subject('New user create account!');
                    });
                }                
            }

            if ($type && ($type == config('types.clinic') || $type == config('types.lab'))) {
                $branch = new Branch;

                $timestamp = time();

                $branch->type = $user->type;
                $branch->owner_id = $user->id;
                $branch->industry_id = 2;
                $branch->name = $user->company;
                $branch->description = 'Description...';
                $branch->email = $user->email;
                $branch->phone = $user->phone;
                $branch->zip_code = $user->zip_code;
                $branch->locality = $user->locality;
                $branch->address = $user->address;
                $branch->verified = 2;         
                $branch->sort_order = 0;
                $branch->status = 1;
                $branch->created_at = $timestamp;
                $branch->updated_at = $timestamp;
                $branch->logo = $user->logo; 

                $branch->save();

                if ($request->input('country') && $request->input('region') && $request->input('locality') && $request->input('address') && $user->type == config('types.lab')) {            
                    $country = Country::where('id', $request->input('country'))->first();

                    if ($country) {
                        $place = Google::getLocationDataByName($request->input('address'), $country);

                        $lat = '40.762705';
                        $lng = '-74.010930';

                        if (isset($place->results[0]->geometry->location) && $place->results[0]->geometry->location) {
                            $lat = $place->results[0]->geometry->location->lat;
                            $lng = $place->results[0]->geometry->location->lng;
                        } 

                        $branch->geolocation = '{"lat":"'.$lat.'", "lon":"'.$lng.'"}';
                        $branch->save();                
                    }
                }
            }

            if ($type || $request->input('social') == config('statuses.email')) {
                return redirect()
                    ->route('home')
                    ->with([
                        'message'    => __('translations.need_moderation'). '<a href="mailto:'.$email.'">'.$email.'</a>',
                        'alert-type' => 'success',
                    ]);
            } else {
                if (Auth::attempt(['email' => $email, 'password' => $password])) {
                    return redirect()
                        ->route('catalog.industry.type', ['dentistry', 'labs'])
                        ->with([
                            'message'    => __('translations.reg_success_guest'),
                            'alert-type' => 'success',
                        ]);
                } else {
                    return redirect()
                        ->route('home');
                }                
            }
        }

        $breadcrumbs = array();

        $breadcrumbs[] = array(
            'text' => __('translations.home'),
            'href' => route('home')
        );

        $breadcrumbs[] = array(
            'text' => __('translations.registration'),
            'href' => route('registration')
        );    

        $seo = (object)[
            'name' => __('translations.registration'),
            'meta_title' => __('translations.registration'),
            'meta_description' => 'Dentist2Lab service registration page',
        ];

        return view('Frontend.pages.registration', compact(            
            'breadcrumbs',
            'seo',
            'type'
        ));
    }
}
