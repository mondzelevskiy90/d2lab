<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\Content;
use App\Models\Article;

class ArticleController extends Controller {
    public function index($slug, Request $request) {
    	$data = Article::with('content')
    		->where('slug', trim($slug))
    		->where('status', 1)
    		->firstOrFail();

    	$seo = Content::getSeo($data);

    	$breadcrumbs = array();

    	$breadcrumbs[] = array(
			'text' => __('translations.home'),
			'href' => route('home')
		);

		$breadcrumbs[] = array(
			'text' => $seo->name,
			'href' => '/article/'.$data->slug
		);

    	return view('Frontend.pages.article', compact(
    		'data', 
    		'seo',
    		'breadcrumbs'
    	));
    }
}
