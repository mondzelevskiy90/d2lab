<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\Notificate;
use App\Services\Img;
use App\Services\Google;
use App\Services\Content;
use App\Services\Chatdata;
use App\Services\OrderService;
use App\Models\Banner;
use App\Models\Branch;
use App\Models\Branch_list;
use App\Models\Branch_staff;
use App\Models\Country;
use App\Models\Region;
use App\Models\Locality;
use App\Models\Field;
use App\Models\Industry;
use App\Models\Industry_service;
use App\Models\Invoice;
use App\Models\Numbering_system;
use App\Models\Order;
use App\Models\Order_chat;
use App\Models\Order_chat_view;
use App\Models\Order_delivery;
use App\Models\Order_file;
use App\Models\Order_history;
use App\Models\Order_prescription;
use App\Models\Order_prescription_field;
use App\Models\Order_prescription_version;
use App\Models\Order_review;
use App\Models\Order_review_comment;
use App\Models\Order_status;
use App\Models\Order_total;
use App\Models\Option;
use App\Models\Option_to_service;
use App\Models\Payment_type;
use App\Models\Price;
use App\Models\Price_service;
use Illuminate\Support\Facades\Hash;
use Session;
use Auth;
use Illuminate\Support\Str;

class AjaxController extends Controller {
    public function getData(Request $request) {
        $data = false;

        if ($request->input('action')) {
            $func = $request->input('action');

            $data = $this->{$func}($request);
        }

        return response()->json($data);
    }

    protected function getCountries($request) {
        $data = array();

        if($request->input('search')) {
            $search = $request->input('search');

            $countries = Country::where(function ($countries) use ($search) {
                $countries->where('name', 'LIKE', $search.'%')                   
                    ->orWhere('code', 'LIKE', $search.'%');
            })->get();
        } else {
            $countries = Country::all();
        }

        foreach ($countries as $country) {
            $data[] = array(
                'id' => $country->id, 
                'text' => $country->name.($country->code ? ' ('.$country->code.')' : '')
            );
        }

        return $data;
    } 

    protected function getRegions($request) {
        $data = array();

        $regions = Region::where('country_id', (int)$request->input('country_id'));

        if($request->input('search')) {
            $search = $request->input('search');

            $regions->where(function ($regions) use ($search) {
                $regions->where('name', 'LIKE', $search.'%')                   
                    ->orWhere('code', 'LIKE', $search.'%');
            });
        }

        $regions = $regions->get();

        foreach ($regions as $region) {
            $data[] = array(
                'id' => $region->id, 
                'text' => $region->name.($region->code ? ' ('.$region->code.')' : '')
            );
        }

        return $data;
    }

    protected function getLocalities($request) {
        $data = array();

        $localities = Locality::where('country_id', (int)$request->input('country_id'))
            ->where('region_id', (int)$request->input('region_id'));

        if($request->input('search')) $localities->where('name', 'LIKE', $request->input('search').'%');

        $localities = $localities->get();

        if (!count($localities) && $request->input('region_id') != 0) {
            $localities = array();

            $region = Region::with('country')
                ->where('id', (int)$request->input('region_id'))
                ->first();

            $google_localities = Google::getLocalitiesByRegion($request->input('search'), $region->country->short_code);

            foreach ($google_localities->predictions as $google_locality) {
                $locality = Locality::firstOrNew(array('name' => $google_locality->description));                

               
                $location_region = Region::where('code', $google_locality->structured_formatting->secondary_text)
                    ->where('country_id', (int)$request->input('country_id'))
                    ->first();
               
                if (!$location_region) {
                    $location_region = Region::where('name', $google_locality->structured_formatting->secondary_text)
                        ->where('country_id', (int)$request->input('country_id'))
                        ->first();
                }
                

                if ($location_region) {
                    $locality->country_id = $region->country->id;
                    $locality->region_id = $location_region->id;
                    $locality->code = '';
                    $locality->name = $google_locality->description;
                    $locality->save(); 

                    if ($locality->region_id == (int)$request->input('region_id')) {
                        $localities[] = $locality;
                    }
                }
            }
        }

        foreach ($localities as $locality) {
            $data[] = array(
                'id' => $locality->id, 
                'text' => $locality->name
            );
        }

        return count($data) ? $data : false;
    }

    protected function getLocalitiesToSelect($request) {
        $data = array();

        $localities = Locality::with('region');

        if($request->input('search')) $localities->where('name', 'LIKE', $request->input('search').'%');

        $localities = $localities->get();

        $data[] = array(
            'id' => 0, 
            'text' => 'Show all'
        );

        foreach ($localities as $locality) {
            $data[] = array(
                'id' => $locality->id, 
                'text' => $locality->name.' ('.$locality->region->name.')'
            );
        }

        return count($data) ? $data : false;
    }

    protected function setSession($request) {
        if ($request->input('method') && $request->input('name')) {
            if ($request->input('method') == 'add') {
                Session::put($request->input('name'), $request->input('value'));
            }

            if ($request->input('method') == 'remove') {
                Session::forget($request->input('name'));
            }
            
        }
    }

    protected function getIndustries($request) {  
        $data = Industry::where('parent_id', $request->input('parent_id'))
            ->where('status', 1)
            ->get();

        return $data;
    }

    protected function getIndustriesToSelect($request) {
        $data = array();

        $industries = Industry::where('parent_id', '!=', 0)
            ->where('status', 1);

        if ($request->input('search')) {
            $industries->where('name', 'LIKE', $request->input('search').'%');
        }

        $industries = $industries->orderBy('sort_order', 'ASC')->get();

        foreach ($industries as $industry) {            
            $data[] = array(
                'id' => $industry->slug, 
                'text' => $industry->name
            );
        }

        return $data;
    }

    protected function getServicesByIndustryToSelect($request) {
        $data = array();

        $services = Industry_service::where('industry_id', $request->input('industry'))
            ->where('status', 1);

        if ($request->input('search')) {
            $services->where('name', 'LIKE', $request->input('search').'%');
        }

        $services = $services->orderBy('sort_order', 'ASC')->get();

        $data[] = array(
            'id' => 0, 
            'text' => 'Show all'
        );

        foreach ($services as $service) {            
            $data[] = array(
                'id' => $service->id, 
                'text' => $service->name
            );
        }

        return $data;
    }

    protected function getOptionsByIndustryToSelect($request) {
        $data = array();

        $options = Option::where('industry_id', $request->input('industry'))
            ->where('status', 1);

        if ($request->input('search')) {
            $options->where('name', 'LIKE', $request->input('search').'%');
        }

        $options = $options->orderBy('sort_order', 'ASC')->get();

        $data[] = array(
            'id' => 0, 
            'text' => 'Show all'
        );

        foreach ($options as $option) {            
            $data[] = array(
                'id' => $option->id, 
                'text' => $option->name
            );
        }

        return $data;
    }

    protected function saveImage($request) {
        $data = false;
        
        if ($request->image) {
            $prefix = $request->input('prefix');

            if ($prefix) {
                if ($request->input($prefix) && $request->input($prefix)['status'] == 1) {
                    $data = Img::cropUploadedImage($request->image, $request->folder.'/'.(Auth::check() ? Auth::user()->id : 'regs').'/', 50, 50, $request->input($prefix));
                } else {
                    $data = Img::fitImage($request->image, $request->folder.'/'.(Auth::check() ? Auth::user()->id : 'regs').'/', 50, 50, 80);
                }
            } else {
                $data = Img::saveImageVersions($request->image, 'portfolios/'.Auth::user()->id.'/');
            }
        }
        
        return $data;
    }

    protected function saveCheckoutFile($request) {
        $data = false;

        if ($request->order_file && $request->file_index) {
        	$data = array();

        	$data['extension'] = $request->order_file->getClientOriginalExtension();

            $name = time().'_'.$request->file_index.'.'.$data['extension'];

            $request->order_file->move(public_path().'/storage/checkout_files/'.date('FY').'/', $name);

            $data['file'] = 'checkout_files/'.date('FY').'/'.$name; 

            if ($request->order_id && $request->to_branch_id) {
                $file = new Order_file;

                $file->order_id = $request->order_id;
                $file->file = $data['file'];
                $file->save();

                $order_url = route('orders.edit', $request->order_id);

                $notificate_message = sprintf(__('translations.order_file_notificate'), $order_url, $request->order_id);

                Notificate::createNotification($request->to_branch_id, $request->order_id, $notificate_message);
            }         
        }

        return $data;
    }

    protected function removeCheckoutFile($request) {
        $data = false;

        if ($request->order_id && $request->file_id) {            
            Order_file::where('order_id', $request->order_id)->where('id', $request->file_id)->delete();

            $data = true;
        }

        return $data;
    }

    protected function getGeoLocationsById($request) {
        $data = false;

        if ($request->input('ids')) {
            $ids = explode(',', $request->input('ids'));

            $branches = Branch::select('geolocation')->whereIn('id', $ids)->get()->toArray();
            
            foreach($branches as $branch) {
                $data[] = json_decode($branch['geolocation']);
            }
        }
        
        return $data;
    }

    protected function addToMyLabs($request) {
        $data = false;

        if ($request->input('branch_id') && $request->input('user_id')) {
            Branch_list::where('branch_id', (int)$request->input('branch_id'))
                ->where('user_id', (int)$request->input('user_id'))
                ->delete();

            if ($request->input('method') && $request->input('method') == 'add') {
                $item = new Branch_list;

                $item->branch_id = (int)$request->input('branch_id');            
                $item->user_id = (int)$request->input('user_id');            
                $item->save();
            }
            
            $data = true;
        }

        return $data;
    }

    protected function getBranchAddreess($request) {
        $data = false;

        if ($request->input('branch_id')) {
            $branch = Branch::where('id', (int)$request->input('branch_id'))->first();

            if ($branch) $data = $branch->zip_code .', '. $branch->address .', '. \App\Services\Userdata::getLocality($branch->locality);
        }

        return $data;
    }

    protected function getNumberingSystemSvg($request) {
        $data = false;

        if ($request->input('system')) {
            $numbering_system = Numbering_system::where('id', (int)$request->input('system'))->first();

            if ($numbering_system) {               
                $data = $numbering_system->svg;
            }
        }

        return $data;
    }

    protected function getOrderPrescription($request) {
        $data = false;

        if ($request->input('service') && $request->input('system') && $request->input('branch_id')) {
            $data = array();

            $data['service_id'] = (int)$request->input('service');
            $data['numbering_system_id'] = (int)$request->input('system');


            $service = Industry_service::with('parent')
                ->where('id', (int)$request->input('service'))
                ->first();

            if ($service) {
                $data['service_name'] = $service->name;
                $data['parent_name'] = '';
                $data['prescription_description'] = $service->prescription_description;
              //  $data['available_statuses'] = $service->available_statuses;
              //  $data['available_options'] = $service->available_options;
                $data['available_options'] = explode(',', $service->available_options2);

                if ($service->parent) {
                    $data['service_name'] = $service->name . ' ('.$service->parent->name.')';
                    $data['parent_name'] = $service->parent->name;
                }
            }

            $numbering_system = Numbering_system::where('id', (int)$request->input('system'))->first();

            if ($numbering_system) {               
                $data['numbering_system_name'] = $numbering_system->name;
            }

            $data['price'] = 0;
            $data['price_for'] = 1;
            $data['min_days'] = false;

            $price_service = Price_service::with([
            		'service',
            		'custom' => function($q) use ($request) {
            			$q->where('branch_id', (int)$request->input('from_branch_id'));
            		}
            	])
                ->where('price_id', function ($q) use ($request){
                    $q->select('id')
                        ->from(with(new Price)->getTable())
                        ->where('branch_id', (int)$request->input('branch_id'));
                })
                ->where('industry_service_id', (int)$request->input('service'))
                ->first();

            if ($price_service) {
                $data['price'] = isset($price_service->custom->price) ? $price_service->custom->price : $price_service->price;
                $data['min_days'] = $price_service->min_days;

                if ($price_service->service) $data['price_for'] = $price_service->service->price_for;
            }

            $data['fields'] = Content::getFieldsByServiceAndSystem((int)$request->input('service'), (int)$request->input('system'));
        }

        return $data;
    }   

    protected function getOrderPrescriptionSelecteds($request) {
        $data = false;

        if ($request->input('order_id')) {
            $data = array();

            $prescriptions = Order_prescription::where('order_id', (int)$request->input('order_id'))->get();

            if ($prescriptions) {
                $data[] = false;
                foreach ($prescriptions as $prescription) {
                    if ($prescription->selecteds) {
                        $selecteds = explode(',', $prescription->selecteds);

                        $data[] = $selecteds;
                    } else {
                        $data[] = false;
                    }
                }
            }
        }

        return $data;
    } 

    protected function getOrderPrescriptionData($request) {
        $data = false;

        if ($request->input('id')) {
            $data = array();

            $prescription = Order_prescription::select('order_id', 'service_id', 'numbering_system_id', 'comment')->where('id', (int)$request->input('id'))->first();

            if ($prescription) {
                $data = array();

                $service = Industry_service::where('id', $prescription->service_id)->first();
                $fields = Order_prescription_field::where('prescription_id', $request->input('id'))->get();

                $data['id'] = $request->input('id');
                $data['order_id'] = $prescription->order_id;
                $data['service_name'] = $service && $service->name ? $service->name : false;
                $data['comment'] = $prescription->comment;
                //$data['selecteds'] = $fields && count($fields) ? $fields->toArray() : false;
                $data['fields'] = Content::getFieldsByServiceAndSystem((int)$prescription->service_id, (int)$prescription->numbering_system_id);
            }
        }

        return $data;
    }

    protected function showPrescriptionVersion($request) {
        $data = false;

        if ($request->input('id')) {
            $data = array();

            $prescription = Order_prescription::select('order_id', 'service_id', 'numbering_system_id')->where('id', (int)$request->input('id'))->first();

            if ($prescription) {
                $data = array();

                $service = Industry_service::where('id', $prescription->service_id)->first();
                $fields = Order_prescription_field::where('prescription_id', $request->input('id'))->get();

                $data['id'] = $request->input('id');
                $data['order_id'] = $prescription->order_id;
                $data['service_name'] = $service && $service->name ? $service->name : false;
                $data['fields'] = Content::getFieldsByServiceAndSystem((int)$prescription->service_id, (int)$prescription->numbering_system_id);
                $data['selecteds'] = array();
                $data['comment'] = null;  

                $prev_fields = Order_prescription_version::where('version', $request->input('version'))->where('prescription_id', $request->input('id'))->first();

                if ($prev_fields) {
                    $data['selecteds'] = json_decode($prev_fields->content);
                    $data['comment'] = $prev_fields->comment;
                }              
            }
        }

        return $data;
    }

    protected function saveOrderPrescriptionData($request) {
        $data = false;

        if ($request->input('order_id') && $request->input('prescription_id') && $request->input('prescriptions')) {
        	$new_version = false;
            $old_data = array();
        	$order = Order::where('id', $request->input('order_id'))->first();
        	$prescription = Order_prescription::where('id', $request->input('prescription_id'))->first();

        	$history_comment = 'Order prescription has been changed!';

        	if ($request->input('prescriptions')) {               
        		foreach ($request->input('prescriptions') as $key => $prescription_data) {                    
                    $prescription_fields = Order_prescription_field::where('prescription_id', $key)->get();

                    foreach ($prescription_fields as $field) {
                        $old_data[$field->field_id] = $field->value;
                    }

        			if (isset($prescription_data['fields'])) {
        				Order_prescription_field::where('prescription_id', $key)->delete();

        				foreach ($prescription_data['fields'] as $field_id => $field_value) {
        					if ($field_value) {
	                            $prescription_field = new Order_prescription_field;

	                            $prescription_field->prescription_id = $key;
	                            $prescription_field->field_id = $field_id;
	                            $prescription_field->value = is_array($field_value) ? implode(';', $field_value): $field_value;
	                            $prescription_field->save();
	                            
	                        }
        				}

                        $new_version = true;
        			}

                    $old_prescription = new Order_prescription_version;

                    $old_prescription->prescription_id = $key;
                    $old_prescription->version = $prescription->version;
                    $old_prescription->comment = $prescription->comment ? $prescription->comment : null;
                    $old_prescription->content = json_encode($old_data);
                    $old_prescription->save();                    

        			if ($prescription_data['comment']) {
		        		$new_version = true;    

		        		$prescription->comment = $prescription_data['comment'];
		        	} else {
		        		$prescription->comment = null;
		        	}

                    $prescription->version = $prescription->version + 1;
		        	$prescription->save();
        		}

        		if ($new_version) $history_comment .= ' New prescription version is defined!';
        	}

        	if ($new_version) {
        		$order->version = $order->version + 1;
        		$order->save();

        		$history = new Order_history;

		        $history->order_id = $order->id;
		        $history->user = Auth::user()->name.' '.Auth::user()->surname;
		        $history->comment = $history_comment;

		        $history->save();

		        $order_url = route('orders.edit', $order->id);

		        $notificate_message = sprintf(__('translations.order_new_version'), $order_url, $order->id);

		        Notificate::createNotification($order->branch_id, $order->id, $notificate_message);
        	}

            $data = true;
        }

        return $data;
    }

    protected function getOrderStatusActions($request) {
        $data = false;

        if ($request->input('status')) {
            $data = Order_status::where('id', (int)$request->input('status'))->first();
        }

        return $data;
    }

    protected function closeInvoices($request) {
        $data = false;

        if ($request->input('invoices') && is_array($request->input('invoices'))) {
            foreach ($request->input('invoices') as $invoice) {
                $invoice_data = Invoice::where('id', (int)$invoice)->first();

                if ($invoice_data) {
                    $invoice_data->payment_type = 1;

                    if ($request->input('status')) {
                        $invoice_data->payment_status = (int)$request->input('status');

                        Order::where('id', $invoice_data->order_id)->update(['payment_status' => 4]);

                        $prescriptions = Order_prescription::where('order_id', $invoice_data->order_id)->get();

                        foreach($prescriptions as $prescription) {
                            $prescription->price_paid = $prescription->price;
                            $prescription->paid = 1;
                            $prescription->save(); 
                        }
                    }

                    $invoice_data->save();
                }

                $data = true;
            }
        }

        return $data;
    }

    protected function getSelectedInvoicesData($request) {
        $data = false;

        if ($request->input('invoices') && is_array($request->input('invoices'))) {
            $total = 0;
            $debt = 0;
            $pay = 0;
            $service_charge = setting('admin.service_charge');

            $can_online = true;

            foreach ($request->input('invoices') as $invoice) {
                $invoice_data = Invoice::with([
                        'total',
                        'nmi_info'
                    ])
                    ->where('id', (int)$invoice)
                    ->first();

                if (!$invoice_data) continue;

                if ($invoice_data->total) {
                    $pay = $pay + $invoice_data->total->price;
                }

                if (!$invoice_data->nmi_info) {
                    $can_online = false;
                }

                $branch = Branch::where('id', $invoice_data->to_branch_id)->first();

                if ($branch && $branch->charge_value != '0.00') {
                    $service_charge = $branch->charge_value;
                }
            }
/*
            $invoices = Invoice::with(['total'])
                ->where('payment_status', '!=', 4)
                ->where('status', 1);

            switch (Auth::user()->role_id) {
                case config('roles.clinic_head'):
                    $branches_arr = array();
                    $branches = Branch::where('owner_id', Auth::user()->id)->get();                

                    foreach ($branches as $branch) {
                        $branches_arr[] = $branch->id;
                    }
                    
                    $invoices->whereIn('branch_id', $branches_arr); 
                   
                    break;

                case config('roles.clinic_staff'):
                    $staff = Branch_staff::where('user_id', Auth::user()->id)->first();

                    if ($staff) {                 
                        $invoices->where('branch_id', $staff->branch_id);
                    } else {
                        //do not get invoices if user removed from branch
                        $invoices->where('payment_status', '-1');
                    }
                    
                    break;

                default:
                    // do not get invoices if any other roles posts this method
                    $invoices->where('payment_status', '-1');

                    break;
            }

            $invoices = $invoices->get();

            foreach ($invoices as $invoice) {
                if ($invoice->total) $total = $total + $invoice->total->price;
            }

            $debt = $total - $pay;
            $service_charge = $service_charge * 100;

            $data['total'] = round($total, 2);
            $data['debt'] = round($debt, 2);
*/
            $data['pay'] = round($pay, 2);
            $data['service_charge'] = $service_charge * 100;

            $payment_systems = Payment_type::where('status', 1)
                ->orderBy('sort_order', 'ASC');

            if (!$can_online) $payment_systems->where('id', '!=', 2);

            $payment_systems = $payment_systems->get()->toArray();

            $data['payment_systems'] = $payment_systems;
            $data['can_online'] = $can_online;
        }

        return $data;
    }

    protected function confirmPriceChanging($request) {
        $data = false;

        if ($request->input('id') && $request->input('status')) {
            $prescription = Order_prescription::with('service')
                ->where('id', (int)$request->input('id'))
                ->first();

            if ($prescription) {
                $history = new Order_history;

                if ($request->input('status') == 1) {
                    $prescription->price = $prescription->price_changed_to;
                    $history_comment = $prescription->service->name.' prescription price changing is confirmed!'; 
                } else {
                    $history_comment = $prescription->service->name.' prescription price changing is not confirmed!';  
                }

                $prescription->price_changed_to = '0.00';
                $prescription->save();

                $history->order_id = $prescription->order_id;
                $history->user = Auth::user()->name.' '.Auth::user()->surname;
                $history->comment = $history_comment;

                $history->save();

                            
                $order_total = Order_total::where('order_id', $prescription->order_id)->first();

                $total = (float)$order_total->price;
                $price_changed_to = 0;

                $prescriptions = Order_prescription::where('order_id', $prescription->order_id)->get();
                
                $all_confirmed = true;

                foreach ($prescriptions as $item) {
                    if ($item->price_changed_to != '0.00') {
                        $price_changed_to = $price_changed_to + (float)$item->price_changed_to;

                        $all_confirmed = false;
                    } else {
                        $price_changed_to = $price_changed_to + (float)$item->price;
                    }
                }

                $price_changed_to = round($price_changed_to, 2);

                if (!$all_confirmed) {
                    $order_total->price_changed_to = $price_changed_to;
                } else {
                    $order_total->price_changed_to = '0.00';
                    $total = $price_changed_to;
                }

                $order_total->price = $total;  
                  
                $order_total->save();             
              

                $data = true;
            }
        }

        return $data;
    }

    protected function saveReview($request) {
        $data = false;

        if ($request->input('type') && $request->input('order')) {
            $review = false;
            $calc_rating = false;

            $order = Order::where('id', (int)$request->input('order'))->first();

            if ($order) {
                if ($request->input('type') == 'order_review') {
                    $review = new Order_review;
                    $calc_rating = true;

                    if ($request->input('rating') && $request->input('rating') > 0) $review->rating = (int)$request->input('rating');
                }

                if ($request->input('type') == 'order_review_comment') {
                    $review = new Order_review_comment; 
                    if ($request->input('review_id') && $request->input('review_id') > 0) $review->review_id = (int)$request->input('review_id');
                }

                $review->order_id = (int)$request->input('order');
                $review->user_id = Auth::user()->id;
                $review->branch_id = $order->to_branch_id;                
                
                if ($request->input('review')) $review->review = $request->input('review');

                $review->moderation = 1;
                $review->status = 1;
                $review->created_at = date('Y-m-d H:i:s');
                $review->updated_at = date('Y-m-d H:i:s');
                
                $review->save();

                if ($calc_rating) Content::calcLabRating($review->branch_id);

                $data = true;
            }            
        }

        return $data;
    }

    protected function editReview($request) {
        $data = false;

        if ($request->input('id') && $request->input('type')) {
            $review = false;
            $calc_rating = false;

            if ($request->input('type') == 'order_review') {
                $review = Order_review::where('id', (int)$request->input('id'))->first();
            }

            if ($request->input('type') == 'order_review_comment') {
                $review = Order_review_comment::where('id', (int)$request->input('id'))->first(); 
            }

            if ($review) {
                if ($request->input('moderation') && $request->input('moderation') == 2) {
                    $review->moderation = 2;
                    $review->moderation_text = $request->input('text');
                }

                if ($request->input('rating') && $review->rating != (int)$request->input('rating')) {
                    $review->rating = (int)$request->input('rating'); 

                    $calc_rating = true;
                }

                if ($request->input('review')) $review->review = $request->input('review');
                
                $review->save();

                if ($calc_rating) {
                    Content::calcLabRating($review->branch_id);
                }
            }

            $data = true;
        }

        return $data;
    }

    protected function getChat($request) {
        $data = false;

        if ($request->input('order_id')) {
            $data = Order_chat::with('user')
                ->where('order_id', (int)$request->input('order_id'))
                ->where('status', 1)
                ->orderBy('created_at', 'ASC')
                ->get()
                ->toArray();
        }

        return $data;
    }

    protected function saveChat($request) {
        $data = false;

        if ($request->input('order_id') && $request->input('message')) {
            $message = new Order_chat;

            $message->order_id = (int)$request->input('order_id');
            $message->user_id = Auth::user()->id;
            $message->message = $request->input('message');

            if ($request->file && $request->file !== 'false') {               
                $message->file = Img::saveImage($request->file, 'chat/'.$request->input('order_id').'/');
            }

            $message->status = 1;
            $message->save();

            Order_chat_view::where('order_id', (int)$request->input('order_id'))
                ->where('user_id', '!=', Auth::user()->id)
                ->delete();

            $order = Order::where('id', (int)$request->input('order_id'))->first();

            if ($order) {
                $order_url = route('orders.edit', $order->id);
                $notificate_message = sprintf(__('translations.order_new_chat_message'), $order_url, $order->id);

                if (Auth::user()->type == config('types.lab')) {
                    $branch_id = $order->branch_id;
                } else {
                    $branch_id = $order->to_branch_id;
                }

                Notificate::createNotification($branch_id, $order->id, $notificate_message);
            }

            $data = true;
        }

        return $data;
    }

    protected function addChatView($request) {
        $data = false;

        if ($request->input('order_id')) {
            $add_view = new Order_chat_view;

            $add_view->order_id = (int)$request->input('order_id');
            $add_view->user_id = Auth::user()->id;
            $add_view->save();

            $data = true;
        }

        return $data;
    }

    protected function notificationViewed($request) {
        $data = false;

        if ($request->input('id')) {
            $add_view = new \App\Models\Notification_view;

            $add_view->notification_id = (int)$request->input('id');
            $add_view->user_id = Auth::user()->id;
            $add_view->save();

            $data = true;
        }

        return $data;
    }

    protected function showFieldHelp($request) {
         $data = false;

        if ($request->input('id')) {
            $field = Field::where('id', (int)$request->input('id'))->first();

            if ($field) $data = $field->help;
        }

        return $data;
    }

    protected function changeShipDate($request) {
         $data = false;

        if ($request->input('id') && $request->input('date') && $request->input('date')) {
            $order = Order::where('id', (int)$request->input('id'))->first();

            if ($order) {
                $order->ship_date_changed = $request->input('date');
                $order->save();

                $data = $request->input('date');

                $order_url = route('orders.edit', $order->id);
                $notificate_message = sprintf(__('translations.order_status_ship_dateCh'), $order_url, $order->id);

                if (Auth::user()->type == config('types.lab')) {
                    $branch_id = $order->branch_id;
                } else {
                    $branch_id = $order->to_branch_id;
                }

                Notificate::createNotification($branch_id, $order->id, $notificate_message);
            }
        }

        return $data;
    }

    protected function confirmShipDate($request) {
         $data = false;

        if ($request->input('id')) {
            $order = Order::where('id', (int)$request->input('id'))->first();

            if ($order) {
                if ($request->input('change') && $request->input('change') == 2) {

                    $order->ship_date = $order->ship_date_changed;
                }
                
                $order->ship_date_changed = null;
                $order->save();

                $data = $order->ship_date;
            }
        }

        return $data;
    }

    protected function payOnlineInvoices($request) {
        $data = __('translations.online_payment_error');

        if ($request->input('invoices') && $request->input('card') && $request->input('exp') && $request->input('cvv')) {
            $invoices = $request->input('invoices');

            $card_data = array(
                'ccnumber' => $request->input('card'),
                'ccexp' => $request->input('exp'),
                'cvv' => $request->input('cvv')
            );

            $payment = \App\Services\Nmi::createPayment(Auth::user(), $invoices, $card_data);

            if ($payment) {
                if ($payment['status'] && $payment['status'] == 1) {
                    foreach ($invoices as $invoice) {
                        $invoice_data = Invoice::where('id', (int)$invoice)->first();

                        if ($invoice_data) {
                            $invoice_data->payment_type = 2;
                            $invoice_data->payment_status = 4;
                            $invoice_data->transaction_id = $payment['transaction_id'];
                            $invoice_data->charge_transaction_id = $payment['charge_transaction_id'];

                            Order::where('id', $invoice_data->order_id)->update(['payment_status' => 4]);

                            $prescriptions = Order_prescription::where('order_id', $invoice_data->order_id)->get();

                            foreach($prescriptions as $prescription) {
                                $prescription->price_paid = $prescription->price;
                                $prescription->paid = 1;
                                $prescription->save(); 
                            }

                            $invoice_data->save();
                        }
                    }

                    $data = __('translations.online_payment_approved');

                } else if ($payment['status'] && $payment['status'] == 2) {
                    $data = __('translations.online_payment_declined');
                }
            }
        }

        return $data;
    }

    protected function proccessDeliveryService($request) {
        $data = false;

        if ($request->input('id') && $request->input('method')) {
            if ($request->input('method') == 'add' && $request->input('delivery_id')) {
                $order = Order::where('id', (int)$request->input('id'))->first();

                if ($order) {
                    $delivery = new Order_delivery;

                    $from = $order->branch_id;
                    $to = $order->to_branch_id;

                    if ($request->input('type') && $request->input('type') == config('types.lab')) {
                        $from = $order->to_branch_id;
                        $to = $order->branch_id;
                    }

                    $delivery->order_id = $order->id;
                    $delivery->bill_to = config('types.lab');
                    $delivery->from_branch = $from;
                    $delivery->to_branch = $to; 
                    $delivery->delivery_id = $request->input('delivery_id');
                    $delivery->created_at = date('m-d-Y');

                    if ($request->input('delivery_invoice')) {
                        $delivery->delivery_invoice = $request->input('delivery_invoice');                   
                    }                    

                    $delivery->save(); 

                    return Order_delivery::with('delivery_info')->where('id', $delivery->id)->first()->toArray();
                }
            }

            if ($request->input('method') == 'delete' && $request->input('delivery_service_id')) {
                Order_delivery::where('id', $request->input('delivery_service_id'))->delete();

                $data = true;  
            } 

            if ($request->input('method') == 'received' && $request->input('delivery_service_id')) {
                Order_delivery::where('id', $request->input('delivery_service_id'))->update(['received' => 1]);

                $data = true;  
            }      
        }

        return $data;
    }

    protected function markOrderAsDeleted($request) {
        //funcion has fake name! All order data wiil be deleted by request!
        $data = false;

        if ($request->input('order')) {
            $id = \Illuminate\Support\Facades\Crypt::decrypt($request->input('order'));

            Order_chat::where('order_id', $id)->delete();
            Order_chat_view::where('order_id', $id)->delete();
            Order_delivery::where('order_id', $id)->delete();
            Order_file::where('order_id', $id)->delete();
            Order_history::where('order_id', $id)->delete();

            $order_prescriptions = Order_prescription::where('order_id', $id)->get();

            foreach($order_prescriptions as $order_prescription) {
                Order_prescription_field::where('prescription_id', $order_prescription->id)->delete();
            }

            Order_prescription::where('order_id', $id)->delete();
            Order_total::where('order_id', $id)->delete();
            Order_review::where('order_id', $id)->delete();
            Order_review_comment::where('order_id', $id)->delete();
            Order::where('id', $id)->delete();

            $data = true;
        }

        return $data;
    }

    protected function saveOrderReview($request) {
        $data = false;

        if ($request->input('id') && $request->input('review') && $request->input('rating')) {
            $review = new \App\Models\Order_review;

            $review->order_id = (int)$request->input('id');
            $review->user_id = Auth::user()->id;
            $review->branch_id = $request->input('to_branch_id');
            $review->rating = (int)$request->input('rating');
            $review->review = $request->input('review');
            $review->moderation = 1;
            $review->status = 1;
            $review->created_at = date('Y-m-d H:i:s');
            $review->updated_at = date('Y-m-d H:i:s');

            $review->save();

            $history = new \App\Models\Order_history;

            $history_comment = 'Added review to the order!';

            $history->order_id = (int)$request->input('id');
            $history->user = Auth::user()->name.' '.Auth::user()->surname;
            $history->comment = $history_comment;

            $history->save();

            $data = true;
        }

        return $data;
    }

    protected function createPrescriptionRemake($request) {
    	$data = false;

    	if ($request->input('invoice_id') && $request->input('order_id') && $request->input('prescription_id')) {
            $order_id = OrderService::createOrderRemake($request->input('invoice_id'), $request->input('order_id'), $request->input('prescription_id'), 1);

            $data = $order_id;
    	}

    	return $data;
    }

    protected function addBannerVisit($request) {
        $data = false;

        if ($request->input('id')) {
            $banner = Banner::where('id', $request->input('id'))->first();
            
            $banner->views = (int)$banner->views + 1;
            $banner->save();

            $data = true;
        }

        return $data;
    }

    protected function changeServiceCharge($request) {
        $data = false;

        if ($request->input('key') && $request->input('value')) {
            \App\Models\Setting::where('key', $request->input('key'))->update(['value' => $request->input('value')]);

            $data = true;
        }

        return $data;
    }

    protected function appleUser($request) {
        $data = false;

        if ($request->input('apple')) {
        	$apple_data = $request->input('apple');

        	if (isset($apple_data['authorization']['id_token'])) {
        		$result = explode('.', $apple_data['authorization']['id_token']);
        		
        		$email = false;

        		foreach ($result as $item) {
        			$item = base64_decode($item);
	        		$item = json_decode($item);

        			if (isset($item->email)) {
        				$email = $item->email;
        			}
        		}
				if ($email) {
					$user_apple = \App\Models\User::where('email', $email)
		                ->orderBy('id', 'DESC')
		                ->first();

		            if ($user_apple) {
		                $email = $user_apple->email;
		                $password = $user_apple->apple;
		            } else {
		                $user = new \App\Models\User;

		                $timestamp = time();
		                
		                $password = $email;

		                $user->role_id = config('roles.guest');
		                $user->type = config('types.guest');
		                $user->name = 'New';
		                $user->surname = 'User';
		                $user->email = $email;
		                $user->password = Hash::make($password);
		                $user->email_verified_at = null;
		                $user->avatar = 'users/default.png';
		                $user->logo = 'logos/default_logo.png';
		                $user->settings = '{"locale":"en"}';
		                $user->created_at = $timestamp;
		                $user->updated_at = $timestamp;
		                $user->status = config('statuses.new');
		                $user->apple = $password;

		                $user->save();
		            }

		            if (Auth::attempt(['email' => $email, 'password' => $password])) {
		                $data = true;
		            }  
				}
			}        
        }

        return $data;
    }

    protected function getPriceServicesDataForCustom($request) {
        $data = false;

        if ($request->input('price_id') && $request->input('branch_id')) {
            $data = [];

            $price_services = Price_service::with([
                    'custom' => function($q) use ($request) {
                        $q->where('branch_id', (int)$request->input('branch_id'));
                    },
                    'service' => function($q) {
                        $q->with('parent');
                    }
                ])
                ->where('price_id', (int)$request->input('price_id'))
                ->get();

            foreach ($price_services as $item) {
                if ($item->service) {                    
                    if (!isset($data[$item->service->parent_id])) {
                        $data[$item->service->parent_id] = [
                            'name' => $item->service->parent && $item->service->parent_id != 0 ? $item->service->parent->name : '',
                            'services' => []
                       ];
                    }

                    $data[$item->service->parent_id]['services'][] = [
                        'price_id' => (int)$request->input('price_id'),
                        'branch_id' => (int)$request->input('branch_id'),
                        'industry_service_name' => $item->service->name,
                        'industry_service_id' => $item->industry_service_id,
                        'price_service_id' => $item->id,
                        'price' => $item->price,
                        'changed_price' => $item->custom ? $item->custom->price : false,
                        'currency_id' => $item->currency_id,
                        'price_for' => $item->service ? $item->service->price_for : false, 
                    ];
                }
            }
        }

        return $data;
    }
}