<?php
namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Models\User;
use Auth;

class ApiController extends Controller {
	private $user;
	private $api_key = 'kglp5n36luj0gerd9wjl';

	public function method(Request $request, $function) {
		if (!$request->input('api_token') || ($request->input('api_token') && !$this->checkApiToken($request->input('api_token')))) {
			return response()->json(['error' => 'Api token is not defined or invalid!'], 401);
		}

		if (!$request->input('api_key') || ($request->input('api_key') != $this->api_key)) {
			return response()->json(['error' => 'Api key has wrong value or not defined!'], 401);
		}

		if (method_exists($this, $function)) {
			$data = $this->{$function}($request);

			return response()->json($data, 200);
		} else {
			return response()->json(['error' => 'Method is not exist!'], 404);
		}		
	}

	public function gettoken(Request $request) {
		Auth::logout();

		if (!$request->input('email') || !$request->input('password')) {
			return response()->json(['error' => 'Email or password is not defined!'], 401);
		}

		if (!$request->input('api_key') || ($request->input('api_key') != $this->api_key)) {
			return response()->json(['error' => 'Api key has wrong value or not defined!'], 401);
		}
		
		if (Auth::attempt(['email' => $request->input('email'), 'password' => $request->input('password')])) {
			return response()->json(['api_token' => $this->getApiToken(Auth::user()->id)]);
		} else {
			return response()->json(['error' => 'Login is failed!'], 401);
		}
	}

	protected function getApiToken($user_id) {
		$api_token = Str::random(32);

		User::where('id', $user_id)->update(['api_token' => $api_token]);

		return $api_token;
	}

	protected function checkApiToken($api_token) {
		$this->user = User::where('api_token', $api_token)->first();

		if ($this->user) {
			return true;
		} else {
			return false;
		}
	}

	protected function getnotifications($request) {
		return $this->user;//test data
	}
}