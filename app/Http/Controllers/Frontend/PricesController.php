<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\Content;
use App\Services\Currencies;
use App\Services\Notificate;
use App\Models\Branch;
use App\Models\Branch_staff;
use App\Models\Price;
use App\Models\Price_service;
use App\Models\Price_service_custom;
use App\Models\Option_to_price_service;
use App\Models\Price_delivery;
use App\Models\Industry_service;
use Auth;
use Mail;

class PricesController extends Controller {
    public function index() {
        $branches = Branch::where('owner_id', Auth::user()->id)
        	->where('status', '!=', 4)
        	->get();

        $data = Price::with([
                'branch' => function($q) {
                    $q->where('status', '!=', 4);
                }
            ])
        	->where('owner_id', Auth::user()->id)
        	->get();

    	$breadcrumbs = array();

    	$breadcrumbs[] = array(
			'text' => __('translations.home'),
			'href' => route('home')
		);

        $breadcrumbs[] = array(
            'text' => __('translations.account'),
            'href' => route('account')
        );

		$breadcrumbs[] = array(
			'text' => __('translations.prices'),
			'href' => route('prices')
		);

        $seo = (object)[
            'name' => __('translations.prices'),
            'meta_title' => __('translations.prices'),
            'meta_description' => 'Dentist2Lab service prices page',
        ];

    	return view('Frontend.pages.account.prices.index', compact(
    		'data', 
            'branches',
    		'seo',
    		'breadcrumbs'
    	));
    }

    public function create(Request $request) {
        if (!Content::checkBranch()) return redirect()->route('account');

        $data = array();        

        if ($request->method() == 'POST') {
            $data = new Price;

            $branch = Branch::where('id', $request->input('branch_id'))->first();

            $data->owner_id = Auth::user()->id;
            $data->branch_id = $request->input('branch_id');
            $data->name = 'Price for '.$branch->name;             
            $data->sort_order = 0;
            $data->status = 1;            
            $data->save(); 
           
            return redirect()->route('prices.edit', $data->id); 
        }

        $branches = Branch::where('owner_id', Auth::user()->id)
            ->where('status', '!=', 4)
            ->whereNotIn('id', function($query){
                $query->select('branch_id')
                    ->from(with(new Price)->getTable())
                    ->where('owner_id', Auth::user()->id);
            })
            ->get();

        $breadcrumbs = array();

        $breadcrumbs[] = array(
            'text' => __('translations.home'),
            'href' => route('home')
        );

        $breadcrumbs[] = array(
            'text' => __('translations.account'),
            'href' => route('account')
        );

        $breadcrumbs[] = array(
            'text' => __('translations.prices'),
            'href' => route('prices')
        );

        $breadcrumbs[] = array(
            'text' => __('translations.creating'). ' ' .__('translations.price'),
            'href' => route('prices.create')
        );

        $seo = (object)[
            'name' => __('translations.creating'). ' ' .__('translations.price'),
            'meta_title' => __('translations.creating'). ' ' .__('translations.price'),
            'meta_description' => 'Dentist2Lab service prices creating page',
        ];

        return view('Frontend.pages.account.prices.edit', compact(
            'data',
            'branches',
            'seo',
            'breadcrumbs'
        ));
    }

    public function edit(Request $request, $id) {
        if (!Content::checkBranch()) return redirect()->route('account');

        $data = Price::where('id', (int)$id)->firstOrFail();

        $branch = Branch::where('id', $data->branch_id)->first();

        if ($request->method() == 'POST') {
            $messages = array(
                'name.required' => 'Field is required!',                
                'name.max' => 'Field contains more then 255 symbols!'
            );

            $rules = array(
                'name' => 'required|max:255'
            );

            $request->validate($rules, $messages);

            $data->name = $request->input('name');
            $data->save(); 

            Option_to_price_service::where('price_id', $id)->delete();

            foreach ($request->input('services') as $key => $value) {     
                       	
				if (!is_null($value['price'])) {
					$price_service = Price_service::firstOrNew(['price_id' => $id, 'industry_service_id' => $key]);

					$price_service->price_id = $id;
					$price_service->industry_service_id = $key;
					$price_service->price = is_numeric($value['price']) ? $value['price'] : 1;
					$price_service->currency_id = $value['currency_id'];
                    $price_service->min_days = $value['min_days'] && $value['min_days'] != '' ? (int)$value['min_days'] : '';
					$price_service->save();

					if (isset($value['options']) && count($value['options'])) {
						foreach ($value['options'] as $option_key => $option_value) {
							$option_to_service = new Option_to_price_service;
							$option_to_service->price_id = $id;
							$option_to_service->option_id = $option_key;
							$option_to_service->industry_service_id = $key;
							$option_to_service->save();
						}
					}
 				} else {
 					Price_service::where('price_id', $id)
 						->where('industry_service_id',$key)
 						->delete();
 				}
            }

            foreach ($request->input('delivery') as $key => $value) {
                if (!is_null($value['value_from'])) {
                    $price_delivery = Price_delivery::firstOrNew(['price_id' => $id, 'delivery_id' => $key]);

                    $price_delivery->price_id = $id;
                    $price_delivery->delivery_id = $key;
                    $price_delivery->value_from = $value['value_from'];
                    $price_delivery->currency_id = $value['currency_id'];
                    $price_delivery->save();

                } else {
                    Price_delivery::where('price_id', $id)
                        ->where('delivery_id',$key)
                        ->delete();
                }
            }
           
            return redirect()
                ->route('prices')
                ->with([
                    'message'    => __('translations.success'),
                    'alert-type' => 'success',
                ]); 
        }

        $services = Content::getServicesByIndustry($branch->industry_id, true);
        $price_services = Content::getPriceServices($id); 
        $delivery = Content::getDelivery();
        $price_delivery = Content::getPriceDelivery($id);      
        $currency = Currencies::getCurrencyById(1);

        $breadcrumbs = array();

        $breadcrumbs[] = array(
            'text' => __('translations.home'),
            'href' => route('home')
        );

        $breadcrumbs[] = array(
            'text' => __('translations.account'),
            'href' => route('account')
        );

        $breadcrumbs[] = array(
            'text' => __('translations.prices'),
            'href' => route('prices')
        );

        $breadcrumbs[] = array(
            'text' => __('translations.editing'). ' ' .__('translations.price'),
            'href' => route('prices.edit', $id)
        );

        $seo = (object)[
            'name' => __('translations.editing'). ' ' .__('translations.price'),
            'meta_title' => __('translations.editing'). ' ' .__('translations.price'),
            'meta_description' => 'Dentist2Lab service prices editing page',
        ];

        return view('Frontend.pages.account.prices.edit', compact(
            'data',
            'branch',
            'services',
            'price_services',
            'delivery',
            'price_delivery',
            'currency',
            'seo',
            'breadcrumbs'
        ));
    }

    public function editcustom(Request $request, $id) {
        if (!Content::checkBranch()) return redirect()->route('account');

        $data = Price::where('id', (int)$id)->firstOrFail();

        if ($request->method() == 'POST') {
            $services = [];
            $branch_id = false;

            foreach ($request->input('services') as $key => $value) {
				if (!is_null($value['price'])) {
					$price_service = Price_service_custom::firstOrNew(['price_id' => $id, 'branch_id' => $value['branch_id'], 'industry_service_id' => $key, 'price_service_id' => $value['price_service_id']]);

					$price_service->price_id = $id;
					$price_service->branch_id = $value['branch_id'];
					$price_service->price_service_id = $value['price_service_id'];
					$price_service->industry_service_id = $key;
					$price_service->price = is_numeric($value['price']) ? $value['price'] : 1;
					$price_service->currency_id = $value['currency_id'];
					$price_service->save();

                    $services[] = [
                        'name' => Industry_service::where('id', $key)->first()->name,
                        'price' => is_numeric($value['price']) ? $value['price'] : 1
                    ];

                    $branch_id = $value['branch_id'];
                    $send = true;
 				} else {
 					Price_service_custom::where('price_id', $id)
                        ->where('price_service_id', $value['price_service_id'])
                        ->where('branch_id', $value['branch_id'])
 						->where('industry_service_id',$key)
 						->delete();
 				}
            }

            $emails = [];

            if ($branch_id) {
                $branch_data = Branch::with('owner')->where('id', $branch_id)->first();

                if ($branch_data && $branch_data->owner) {
                    $emails[] = $branch_data->owner->email;
                }

                $branch_staff = Branch_staff::with('user')->where('branch_id', $branch_id)->get();

                foreach ($branch_staff as $staff) {
                    if ($staff->user) {
                        $emails[] = $staff->user->email;
                    }
                }
            }
 
            if ($send && count($emails)) {
                $content = [
                    'services' => $services
                ];

                foreach ($emails as $email) {
                    Mail::send('Frontend.emails.dentist_price_changed', $content, function ($message) use ($email) {
                        $message->to($email)->subject('New price for services!');
                    });
                }
            }
           
            return redirect()
                ->route('prices.edit', $id)
                ->with([
                    'message'    => __('translations.success'),
                    'alert-type' => 'success',
                ]); 
        }

        $branches = Branch::select('id', 'name')
        	->where('type', config('types.clinic'))
        	->where('status', 1)
        	->orderBy('name', 'asc')
        	->get();

        $breadcrumbs = array();

        $breadcrumbs[] = array(
            'text' => __('translations.home'),
            'href' => route('home')
        );

        $breadcrumbs[] = array(
            'text' => __('translations.account'),
            'href' => route('account')
        );

        $breadcrumbs[] = array(
            'text' => __('translations.prices'),
            'href' => route('prices')
        );

        $breadcrumbs[] = array(
            'text' => __('translations.editing'). ' ' .__('translations.price'),
            'href' => route('prices.edit', $id)
        );

        $breadcrumbs[] = array(
            'text' => __('translations.editing'). ' ' .__('translations.price_dentist_custom'),
            'href' => route('prices.editcustom', $id)
        );

        $seo = (object)[
            'name' => __('translations.editing'). ' ' .__('translations.price_dentist_custom'),
            'meta_title' => __('translations.editing'). ' ' .__('translations.price_dentist_custom'),
            'meta_description' => 'Dentist2Lab service prices editing page',
        ];

        return view('Frontend.pages.account.prices.editcustom', compact(
            'data',
            'branches',
            'seo',
            'breadcrumbs'
        ));
    }

    public function delete(Request $request, $id) {
        Price::where('id', (int)$id)->delete();
        Price_service::where('price_id', (int)$id)->delete();
        Price_delivery::where('price_id', (int)$id)->delete();
        Price_service_custom::where('price_id', (int)$id)->delete();
        Option_to_price_service::where('price_id', (int)$id)->delete();
        
        return redirect()
	        ->route('prices')
	        ->with([
	            'message'    => __('translations.success'),
	            'alert-type' => 'success',
	        ]);
    }
}