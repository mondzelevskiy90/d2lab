<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\Notificate;
use App\Services\Content;
use App\Services\Currencies;
use App\Models\Branch;
use App\Models\Branch_staff;
use App\Models\Industry_service;
use App\Models\Invoice;
use App\Models\Numbering_system;
use App\Models\Order;
use App\Models\Order_delivery;
use App\Models\Order_file;
use App\Models\Order_history;
use App\Models\Order_prescription;
use App\Models\Order_prescription_field;
use App\Models\Order_review;
use App\Models\Order_review_comment;
use App\Models\Order_status;
use App\Models\Order_tech_status;
use App\Models\Order_total;
use App\Models\Order_type;
use App\Models\Payment_status;
use App\Models\Price;
use App\Models\Price_delivery;
use App\Models\Price_service;
use App\Models\User;
use Auth;
use Mail;

class OrdersController extends Controller {
    private $tech_statuses = [12];
    private $hide_to_lab = [1];
    private $hightlight_ship_statuses = [11,12,13,14,20];

    public function index(Request $request) {
        $data = array();
        $order_statuses = array();
        $order_types = array();
        $payment_statuses = array();
        $filters = array();
        $branches = array();
        $branch_staff = array();
        $hightlight_ship_statuses = $this->hightlight_ship_statuses;

        switch (Auth::user()->role_id) {
            case config('roles.lab_head'):
                $order_statuses = Order_status::whereNotIn('id', $this->hide_to_lab)->get();
                break;
            
            case config('roles.lab_staff'):
                $order_statuses = Order_status::whereNotIn('id', $this->hide_to_lab)->get();
                break;

            case config('roles.tech'):
                $order_statuses = Order_status::whereIn('id', $this->tech_statuses)->get();
                break;

            default:
                $order_statuses = Order_status::all();
                break;
        }

        $order_types = Order_type::where('status', 1)->get();
        $payment_statuses = Payment_status::where('status', 1)->get();

        $data = Order::with([
            'status_info',
            'type_info',
            'payment_info',
            'delivery_service' => function($q) {
                $q->with('delivery_info')->orderBy('id', 'DESC');
            },
            'chat',
            'chat_view' => function ($q) {
                $q->where('user_id', Auth::user()->id);
            }
        ]);

        if ($request->input('search')) {
            $filters['search'] = $request->input('search');
            $data->where('id', 'LIKE', $filters['search'].'%');
        }

        if ($request->input('status')) {
            $filters['status'] = $request->input('status');
            $data->where('status', (int)$filters['status']);
        }

        if ($request->input('order_type')) {
            $filters['order_type'] = $request->input('order_type');
            $data->where('order_type', (int)$filters['order_type']);
        }

        if ($request->input('payment_status')) {
            $filters['payment_status'] = $request->input('payment_status');
            $data->where('payment_status', (int)$filters['payment_status']);
        }

        if ($request->input('ship_date')) {
            $filters['ship_date'] = $request->input('ship_date');
            $data->where('ship_date', $filters['ship_date']);
        }

        if ($request->input('created_at')) {
            $filters['created_at'] = $request->input('created_at');
            $data->where('created_at', 'LIKE', $filters['created_at'].'%');
        }

        if ($request->input('patient')) {
            $filters['patient'] = $request->input('patient');
            $data->where('patient_name', 'LIKE', $filters['patient'].'%');
        }

        switch (Auth::user()->role_id) {
            case config('roles.clinic_head'):
                $branches_arr = array();
                $branches = Branch::where('owner_id', Auth::user()->id)->get();                

                foreach ($branches as $branch) {
                    $branches_arr[] = $branch->id;
                }

                if ($request->input('branch')) {
                    $filters['branch'] = $request->input('branch');
                    $data->where('branch_id', (int)$filters['branch']);

                    $branch_staff = Branch_staff::with('user')
                        ->where('branch_id', (int)$filters['branch'])
                        ->get();
                } else {
                    $data->whereIn('branch_id', $branches_arr);

                    $branch_staff = Branch_staff::with('user')
                        ->whereIn('branch_id', $branches_arr)
                        ->get();
                }

                if ($request->input('branch_staff')) {
                    $filters['branch_staff'] = $request->input('branch_staff');
                    $data->where('user_id', (int)$filters['branch_staff']);
                }
                
                break;

            case config('roles.clinic_staff'):

                $staff = Branch_staff::where('user_id', Auth::user()->id)->first();

                if ($staff) {                 
                    $data->where('branch_id', $staff->branch_id); 

                    $branch_staff = Branch_staff::with('user')
                        ->where('branch_id', $staff->branch_id)
                        ->where('user_id', '!=', Auth::user()->id)
                        ->get();

                    if ($request->input('branch_staff')) {
                        $filters['branch_staff'] = $request->input('branch_staff');
                        $data->where('user_id', (int)$filters['branch_staff']);
                    }
                } else {
                    //do not show orders if user removed from branch
                    $data->where('status', '-1'); 
                }
                
                break;

            case config('roles.doctor'):
                $staff = Branch_staff::where('user_id', Auth::user()->id)->first();

                if ($staff) {                 
                    $data->where('branch_id', $staff->branch_id)
                        ->where('user_id', Auth::user()->id); 
                } else {
                    //do not show orders if user removed from branch
                    $data->where('status', '-1'); 
                }
                
                break;

            case config('roles.lab_head'):
                $branches_arr = array();
                $branches = Branch::where('owner_id', Auth::user()->id)->get();

                foreach ($branches as $branch) {
                    $branches_arr[] = $branch->id;
                }

                if ($request->input('branch')) {
                    $filters['branch'] = $request->input('branch');
                    //do not show orders in editing and new status   
                    $data->where('to_branch_id', (int)$filters['branch'])
                        ->whereNotIn('status', $this->hide_to_lab); 

                    $branch_staff = Branch_staff::with('user')
                        ->where('branch_id', (int)$filters['branch'])
                        ->get();
                } else {
                    //do not show orders in editing and new status                
                    $data->whereIn('to_branch_id', $branches_arr)
                        ->whereNotIn('status', $this->hide_to_lab);

                    $branch_staff = Branch_staff::with('user')
                        ->whereIn('branch_id', $branches_arr)
                        ->get(); 
                }

                if ($request->input('branch_staff')) {
                    $filters['branch_staff'] = $request->input('branch_staff');
                    $data->where('user_id', function ($q) use ($filters) {
                        $q->where('executor_id', (int)$filters['branch_staff'])
                            ->orWhere('acceptor_id', (int)$filters['branch_staff']);
                    });
                 }
                
                break;

            case config('roles.lab_staff'):
                $staff = Branch_staff::where('user_id', Auth::user()->id)->first();

                if ($staff) {    
                    //do not show orders in editing and new status                
                    $data->where('to_branch_id', $staff->branch_id)
                        ->whereNotIn('status', $this->hide_to_lab); 

                    $branch_staff = Branch_staff::with('user')
                        ->where('branch_id', $staff->branch_id)
                        ->where('user_id', '!=', Auth::user()->id)
                        ->get();

                     if ($request->input('branch_staff')) {
                        $filters['branch_staff'] = $request->input('branch_staff');
                        $data->where('user_id', function ($q) use ($filters) {
                            $q->where('executor_id', (int)$filters['branch_staff'])
                                ->orWhere('acceptor_id', (int)$filters['branch_staff']);
                        });
                     }
                } else {
                    //do not show orders if user removed from branch
                    $data->where('status', '-1'); 
                }                

                break;

            case config('roles.tech'):
                $staff = Branch_staff::where('user_id', Auth::user()->id)->first(); 

                if ($staff) {
                    // Tech can view only Ready to work, In work, Ready to ship statuses
                    $data->where('to_branch_id', $staff->branch_id)
                        //->where('executor_id', Auth::user()->id)
                        ->whereIn('status', $this->tech_statuses); 
                } else {
                    //do not show orders if user removed from branch
                    $data->where('status', '-1'); 
                }
                
                break;
            
            default:
                // do not show orders if any other roles views this page
                $data->where('status', '-1'); 

                break;
        }

        $data = $data->orderBy('id', 'DESC')->paginate(20);

    	$breadcrumbs = array();

    	$breadcrumbs[] = array(
			'text' => __('translations.home'),
			'href' => route('home')
		);

        $breadcrumbs[] = array(
            'text' => __('translations.account'),
            'href' => route('account')
        );

		$breadcrumbs[] = array(
			'text' => __('translations.orders'),
			'href' => route('orders')
		);

        $seo = (object)[
            'name' => __('translations.orders'),
            'meta_title' => __('translations.orders'),
            'meta_description' => 'Dentist2Lab service orders page',
        ];

        $requests = $request->getQueryString();

    	return view('Frontend.pages.account.orders.index', compact(
    		'data', 
            'filters',
            'branches',
            'branch_staff',
            'order_statuses',
            'order_types',
            'payment_statuses',
            'hightlight_ship_statuses',
            'requests',
    		'seo',
    		'breadcrumbs'
    	));
    }

    public function edit(Request $request, $id) {
        $data = Order::with([
                'to_branch',
                'type_info',
                'status_info',
                'payment_info',
                'delivery_info',
                'delivery_service' =>function ($q) {
                    $q->with([
                            'delivery_info',
                            'from_info',
                            'to_info'
                        ])
                        ->orderBy('id', 'DESC');
                },
                'invoice',
                'total',
                'files',
                'history',
                'prescriptions',
                'reviews'
            ])
            ->where('id', (int)$id)
            ->firstOrFail();

        $data->review_access = Content::getReviewAccess($id, Auth::user());

        $prescription_index = 1;
        $file_index = 1;
        $branch_staff = false;
        $branches = false;
        $user_branch = false;
        $services = array();
        $numbering_systems = array();
        $deliveries = array();
        $order_types = array();
        $order_statuses = array();
        $prescriptions = array();
        $hightlight_ship_statuses = $this->hightlight_ship_statuses;

        $branch = Branch::with('owner')
        	->where('id', $data->to_branch_id)
        	->first();

        if (Auth::user()->role_id == config('roles.lab_head') || Auth::user()->role_id == config('roles.lab_staff')) {
        	$branch_staff = array();

        	$branch_staff[] = array(
        		'id' => $branch->owner->id,
        		'name' => $branch->owner->name.' '.$branch->owner->surname
        	);

        	$branch_staff_query = Branch_staff::with('user')
        		->where('branch_id', $branch->id)
        		->get();

        	foreach ($branch_staff_query as $brach_user) {
        		if ($brach_user->user->status != config('statuses.blocked') && $brach_user->user->status != config('statuses.removed')) {
        			$branch_staff[] = array(
		        		'id' => $brach_user->user->id,
		        		'name' => $brach_user->user->name.' '.$brach_user->user->surname
		        	);
        		}
        	}
        }

        if ($data->status == 1) {
            if (Auth::user()->role_id == config('roles.clinic_head')) {
                $branches = Branch::where('owner_id', Auth::user()->id)
                    ->where('type', config('types.clinic'))
                    ->where('status', 1)                   
                    ->get();
            } else {
                $user_branch = Branch_staff::with('branch')
                    ->where('user_id', Auth::user()->id)
                    ->first();
            }
        }

        $currency = Currencies::getCurrencyById(1);
        
        $services = Price_service::with([
                'service' => function($q) {
                    $q->with('parent');
                }
            ])
            ->where('price_id', function ($q) use ($branch){
                $q->select('id')
                    ->from(with(new Price)->getTable())
                    ->where('branch_id', $branch->id);
            })
            ->get();

        foreach ($services as $service) {
           if ($service->service && $service->service->parent) {
                $service->parent_name = $service->service->parent->name;
           } else {
                $service->parent_name = 'Uncategoriesed';
           }
        }

        function array_orderby()
        {
            $args = func_get_args();
            $data = array_shift($args);
            foreach ($args as $n => $field) {
                if (is_string($field)) {
                    $tmp = array();
                    foreach ($data as $key => $row)
                        $tmp[$key] = $row[$field];
                    $args[$n] = $tmp;
                    }
            }
            $args[] = &$data;
            call_user_func_array('array_multisort', $args);
            return array_pop($args);
        }

        $services = $services->toArray();
        $services = array_orderby($services, 'parent_name', SORT_ASC);

        $deliveries = Price_delivery::with('delivery')
            ->where('price_id', function ($q) use ($data){
                $q->select('id')
                    ->from(with(new Price)->getTable())
                    ->where('branch_id', $data->to_branch_id);
            })
            ->get();

        $order_types = Order_type::where('status', 1)->get();
        $order_statuses = Order_status::all();

        $numbering_systems = Numbering_system::where('status', 1)
            ->orderBy('sort_order', 'ASC')
            ->get();

        if ($data->prescriptions && count($data->prescriptions)) {
            foreach ($data->prescriptions as $prescription) {
                $service_name = '';
                $available_statuses = '1,2,3,4,5,6,7';
                $available_options = 0;
                $parent_name = false;
                $numbering_system_name = '';
                $prescription_description = false;

                $service = Industry_service::with('parent')
                    ->where('id', $prescription->service_id)
                    ->first();

                if ($service) {
                    $service_name = $service->name;
                    $available_statuses = $service->available_statuses;
                    $available_options = $service->available_options2;

                    if($service->prescription_description) $prescription_description = $service->prescription_description;

                    if (isset($service->parent) && $service->parent) {
                        $parent_name = $service->parent->name;
                        $service_name = $service->name . ' ('.$parent_name.')';
                    }
                }

                $numbering_system = Numbering_system::where('id', $prescription->numbering_system_id)->first();

                if ($numbering_system) {
                    $numbering_system_name = $numbering_system->name;
                    $numbering_system_svg = $numbering_system->svg;
                }

                $fields = Content::getFieldsByServiceAndSystem($prescription->service_id, $prescription->numbering_system_id);

                $user_fields = array();

                $user_fields_query = Order_prescription_field::where('prescription_id', $prescription->id)->get()->toArray();

                if ($user_fields_query) {
                    foreach ($user_fields_query as $field) {
                        $user_fields[$field['field_id']] = $field;
                    }
                }

                $selecteds = false;

                if ($prescription->selecteds) {
                    $selecteds = Content::getSelectedToothsText($prescription->selecteds);
                }

                $prescriptions[] = array(
                    'id' => $prescription->id,
                    'service_id' => $prescription->service_id,
                    'service_name' => $service_name,
                    'available_statuses' => $available_statuses,
                    'available_options' => $available_options,
                    'parent_name' => $parent_name,
                    'prescription_description' => $prescription_description,
                    'numbering_system_id' => $prescription->numbering_system_id,
                    'numbering_system_name' => $numbering_system_name,
                    'numbering_system_svg' => $numbering_system_svg,
                    'comment' => $prescription->comment,
                    'fields' => $fields,
                    'user_fields' => $user_fields,
                    'selecteds_data' => $prescription->selecteds, 
                    'selected_option' => $prescription->selected_option,
                    'selecteds' => $selecteds,
                    'price_for' => $prescription->price_for,
                    'price_order_date' => $prescription->price_order_date,
                    'advance_price' => $prescription->advance_price,
                    'price' => $prescription->price,
                    'price_changed_to' => $prescription->price_changed_to,
                    'paid' => $prescription->paid,
                    'version' => $prescription->version,
                );
            }
        }

        $url_qrcode = \App\Services\Img::getQrCode(route('orders.edit', $id), $id.'qr_url.png');

        $tech_status = false;

        if (Auth::user()->role_id == config('roles.tech')) {
            $tech_status = Order_tech_status::where('order_id', $id)
                ->where('user_id', Auth::user()->id)
                ->orderBy('id', 'DESC')
                ->first();
        }

        $breadcrumbs = array();

        $breadcrumbs[] = array(
            'text' => __('translations.home'),
            'href' => route('home')
        );

        $breadcrumbs[] = array(
            'text' => __('translations.account'),
            'href' => route('account')
        );

        $breadcrumbs[] = array(
            'text' => __('translations.orders'),
            'href' => route('orders')
        );

        $breadcrumbs[] = array(
            'text' => __('translations.editing'). ' ' .__('translations.order'),
            'href' => route('orders')
        );

        $seo = (object)[
            'name' => __('translations.order'),
            'meta_title' => __('translations.editing'). ' ' .__('translations.order'),
            'meta_description' => 'Dentist2Lab service editing order page',
        ];

        $requests = $request->getQueryString();

        return view('Frontend.pages.account.orders.edit', compact(
            'data', 
            'seo',
            'branch',
            'branch_staff',
            'branches',
            'user_branch',
            'services',
            'deliveries',
            'currency',
            'numbering_systems',
            'prescriptions',            
            'order_types',
            'order_statuses',
            'prescription_index',
            'file_index',
            'url_qrcode',
            'hightlight_ship_statuses',
            'breadcrumbs',
            'tech_status',
            'requests'
        ));
    }

    public function update(Request $request, $id) {
        $data = Order::with([
                'status_info',
                'delivery_info',
                'delivery_service',
                'prescriptions',
                'total'
            ])
            ->where('id', (int)$id)
            ->firstOrFail();

        $status = $data->status;
        $new_status = $status;

        if ($request->input('status') && $status != (int)$request->input('status')) {
            $new_status = (int)$request->input('status');

            $data->status = $new_status;
        }

        $status_info = Order_status::where('id', $new_status)->first();

        if ($status == 1) {            
            $branch = Branch::where('id', (int)$request->input('branch_id'))->first();
            $service = Industry_service::where('id', (int)$request->input('service_id'))->first();

            $attached = null;

            if ($request->input('attached')) {
                $attached = implode(';', $request->input('attached'));
            }

            $data->order_type = (int)$request->input('order_type');
            $data->user_id = (int)$request->input('user_id');
            $data->user_name = $request->input('user_name');
            $data->branch_id = (int)$request->input('branch_id');
            $data->branch_name = $branch->name;
            $data->to_branch_id = $request->input('to_branch_id');
            $data->to_branch_name = $request->input('to_branch_name');
            $data->user_email = $request->input('user_email');
            $data->user_phone = $request->input('user_phone');
            $data->patient_name = $request->input('patient_name');
            $data->patient_lastname = $request->input('patient_lastname');
            $data->patient_age = $request->input('patient_age') ? (int)$request->input('patient_age') : null;
            $data->patient_sex = $request->input('patient_sex') ? $request->input('patient_sex') : null;
            $data->ship_date = $request->input('ship_date');
            $data->ship_address = $request->input('ship_address');        
            $data->attached = $attached;    
            $data->numbering_system_id = $request->input('numbering_system_id');
            $data->acceptor_id = 0;
            $data->acceptor_name = null; 
            $data->executor_id = 0;
            $data->executor_name = null; 

            Order_file::where('order_id', (int)$id)->delete();

            if ($request->input('order_files')) { 
                foreach ($request->input('order_files') as $file_path) {
                    if ($file_path) {
                        $file = new Order_file;

                        $file->order_id = $data->id;
                        $file->file = $file_path;
                        $file->save();
                    }
                }
            }

            $old_prescriptions = Order_prescription::where('order_id', (int)$id)->get();

            foreach ($old_prescriptions as $old_prescription) {
                Order_prescription_field::where('prescription_id', (int)$old_prescription->id)->delete();
            }

            Order_prescription::where('order_id', (int)$id)->delete();

            if ($request->input('prescriptions')) {
                foreach ($request->input('prescriptions') as $key => $prescription) {
                    $order_prescription = new Order_prescription;

                    $order_prescription->order_id = $id;
                    $order_prescription->service_id = $prescription['service_id'];
                    $order_prescription->numbering_system_id = $prescription['numbering_system_id'];
                    $order_prescription->selecteds = $prescription['selecteds'] ? $prescription['selecteds'] : null;
                    $order_prescription->price_order_date = $prescription['price_order_date'];
                    $order_prescription->price_for = $prescription['price_for'];
                    $order_prescription->selected_option = isset($prescription['selected_option']) ? $prescription['selected_option'] : 0;
                    $order_prescription->advance_price = $prescription['price'];
                    $order_prescription->price = '0.00';
                    $order_prescription->currency_id = 1;
                    $order_prescription->comment = isset($prescription['comment']) && $prescription['comment'] ? $prescription['comment'] : null;
                    $order_prescription->save();

                    if (isset($prescription['fields']) && $prescription['fields']) {
                        foreach ($prescription['fields'] as $key => $field) {
                            if ($field) {
                                $prescription_field = new Order_prescription_field;

                                $prescription_field->prescription_id = $order_prescription->id;
                                $prescription_field->field_id = $key;
                                $prescription_field->value = is_array($field) ? implode(';', $field): $field;
                                $prescription_field->save();
                            }
                        }
                    }
                }
            } 

            if ($request->input('advance_price') || $request->input('total')) {
                Order_total::where('order_id', (int)$id)->delete();

                $order_total = new Order_total;

                $order_total->order_id = $id;

                if ($request->input('advance_price')) {
                    $order_total->advance_price = $request->input('advance_price');
                }

                if ($request->input('total')) {
                    $order_total->price = $request->input('total');
                }

                $order_total->currency_id = 1;
                $order_total->save();
            }              
        }

        if ($request->input('comment')) {
            $data->comment = $request->input('comment'); 
        }

        if ($data->need_confirmation == 1 && $data->status_info->confirm_by != 0 && Auth::user()->type == $data->status_info->confirm_by) $data->need_confirmation = 0;

        if ($data->need_confirmation == 1 && $status != $new_status && Auth::user()->type != $data->status_info->confirm_by) $data->need_confirmation = 0;

        if ($status != $new_status && $data->need_confirmation == 0 && $status_info->confirm_by != 0) $data->need_confirmation = 1;

        $data->executor_id = 0;
        $data->executor_name = null;

        if ($request->input('executor_id')) {
        	$data->executor_id = 0;
        	$data->executor_name = null;
        
			$executor = User::where('id', (int)$request->input('executor_id'))->first();

        	if ($executor) {
        		$data->executor_id = $executor->id;
        		$data->executor_name = $executor->name.' '.$executor->surname;
        	}
        }

        if ($status == 12 && $request->input('tech_status')) {             
            $order_tech = new Order_tech_status;

            $order_tech->order_id = $id;
            $order_tech->user_id = Auth::user()->id;
            $order_tech->status = $request->input('tech_status');
            $order_tech->save();

            $history = new Order_history;

            $history_comment = 'Technician '.Auth::user()->name.' '.Auth::user()->surname.' marked order as '.__('translations.select_tech_status'.$request->input('tech_status')).'!';

            $history->order_id = $id;
            $history->user = Auth::user()->name.' '.Auth::user()->surname;
            $history->comment = $history_comment;

            $history->save();
                    
        }

        if ($data->payment_status != 4) $data->payment_status = $status_info->payment_status;

        if ($status_info->create_invoice == 1 && $status != 16) {
        	$invoice = Invoice::firstOrNew(['order_id' => (int)$id]);

        	$invoice->branch_id = $data->branch_id;
            $invoice->to_branch_id = $data->to_branch_id;
            $invoice->order_user_id = $data->user_id;
        	$invoice->name = 'INV #'.(int)$id.'(D2L)';
        	$invoice->status = 1;
        	$invoice->save();



            $data->payment_status = 3;
        }

        $data->save();
/*
        if ($data->status_info->close_order == 1 && $data->need_confirmation == 0) {
        	Invoice::where('order_id', (int)$id)->update(['status' => 2]);
        }
*/
        if ($new_status != $status) {
            $history = new Order_history;

            $history_comment = 'Status changed to '.$status_info->name.'!';

            if ($request->input('comment')) $history_comment .= ' Comment: '.$request->input('comment').' ';

            $history->order_id = $id;
            $history->user = Auth::user()->name.' '.Auth::user()->surname;
            $history->comment = $history_comment;

            $history->save();

            $notificate_message = false;
            $notificate_branch = $data->branch_id;
            $order_url = route('orders.edit', $id);

            switch ($new_status) {
                case 2:
                    $notificate_message = sprintf(__('translations.order_status_ready'), $order_url, $id);
                    $notificate_branch = $data->to_branch_id;
/*
                    if ($request->input('delivery_id')) {
                        $data->delivery_id = $request->input('delivery_id');
                        $data->save();

                        $delivery_data = array(
                        	'order' => $data,
                        	'delivery_params' => $request->input('delivery_params') ? $request->input('delivery_params') : false
                        );

                        $this->proccessDelivery($new_status, $delivery_data);
                    }
*/
                    $from_branch = Branch::where('id', $data->branch_id)->first();
                    $to_branch = Branch::where('id', $data->to_branch_id)->first();

                    $content = [
                        'from_branch' => $from_branch,
                        'to_branch' => $to_branch,
                        'url' => route('orders.edit', $id)              
                    ];

                    Mail::send('Frontend.emails.new_order', $content, function ($message) use ($to_branch, $from_branch) {
                        $message->to($to_branch->email)->subject('New order from '.$from_branch->name);
                    });

                    break;

                case 12:
                    $notificate_message = sprintf(__('translations.order_status_work'), $order_url, $id);
                    $notificate_branch = $data->branch_id;

                    if ($status == 2) {                        
                        foreach ($data->prescriptions as $prescription) {
                            if ($prescription->price == '0.00' && $prescription->price != $prescription->advance_price) {
                                Order_prescription::where('id', $prescription->id)->update(['price' => $prescription->advance_price]);
                            }
                        }
                    }

                	if ($data->acceptor_id == 0) {
			        	$data->acceptor_id = Auth::user()->id;
			        	$data->acceptor_name = Auth::user()->name.' '.Auth::user()->surname;
			        	$data->save();
                        /*
			            if ($data->delivery_id != 1 && $data->delivery_id != 2) {
			            	$delivery_data = array(
			                	'order' => $data,
			                	'delivery_params' => $request->input('delivery_params') ? $request->input('delivery_params') : false
			                );

			                $this->proccessDelivery($new_status, $delivery_data);
			            }
                        */
			        }

                	break;

                case 6:
                    $notificate_message = sprintf(__('translations.order_status_proposition'), $order_url, $id);

                    break;

                case 7:
                    $notificate_message = sprintf(__('translations.order_status_cancelled'), $order_url, $id);

                case 10:
                    $notificate_message = sprintf(__('translations.order_status_paused'), $order_url, $id);

                    break;

                case 15:
/*                  $notificate_message = sprintf(__('translations.order_status_sent'), $order_url, $id);

                    if ($request->input('delivery_id')) {
                        $data->delivery_id = $request->input('delivery_id');
                        $data->save();

                        $delivery_data = array(
                            'order' => $data,
                            'delivery_params' => $request->input('delivery_params') ? $request->input('delivery_params') : false
                        );

                        $this->proccessDelivery($new_status, $delivery_data);
                    }
*/
                    break;

                case 16:
                    $notificate_message = sprintf(__('translations.order_status_work_ready'), $order_url, $id);
/*
                    $delivery = new Order_delivery;

                    $delivery->order_id = $id;
                    $delivery->bill_to = config('types.lab');
                    $delivery->from_branch = $data->to_branch_id;
                    $delivery->to_branch = $data->branch_id; 
                    $delivery->delivery_id = 12;
                    $delivery->created_at = date('m-d-Y');
                    $delivery->save();
*/           
                    break;

                case 18:
                    $notificate_message = sprintf(__('translations.order_status_not_accepted'), $order_url, $id);
                    $notificate_branch = $data->to_branch_id;

                    break;                

                default:
                    break;
            }

            if ($data->need_confirmation == 1 && $data->status_info->confirm_by != 0) {

                $notificate_message = sprintf(__('translations.order_status_confirmation'), $order_url, $id);
                if (Auth::user()->type == config('types.clinic')) $notificate_branch = $data->to_branch_id;
                    
            }

            if ($notificate_message) {
                Notificate::createNotification($notificate_branch, $data->id, $notificate_message);     
            }
        }  

        if ($status == 2 && $request->input('order_total')) {
            $order_total = Order_total::where('order_id', (int)$id)->first();
            $order_total->price = $request->input('order_total');
            $order_total->save();
        }    

        if ($status == 2 && $request->input('prescriptions')) {
            foreach ($request->input('prescriptions') as $key => $prescription) {
                if (isset($prescription['price']) && $prescription['price']) {
                    $order_prescription = Order_prescription::where('id', (int)$key)->first();

                    if ($order_prescription) {
                        $order_prescription->price = $prescription['price'];
                        $order_prescription->save();
                    }
                }
            }
        }

        if ($request->input('review')) {
            $review_data = $request->input('review');

            if($review_data['review']) {
                $review = new Order_review;
                $review->order_id = $id;
                $review->user_id = Auth::user()->id;
                $review->branch_id = $data->to_branch_id;
                $review->rating = $review_data['rating'];
                $review->review = $review_data['review'];
                $review->moderation = 1;
                $review->status = 1;
                $review->created_at = date('Y-m-d H:i:s');
                $review->updated_at = date('Y-m-d H:i:s');

                $review->save();

                $history = new Order_history;

                $history_comment = 'Added review to the order!';

                $history->order_id = $id;
                $history->user = Auth::user()->name.' '.Auth::user()->surname;
                $history->comment = $history_comment;

                $history->save();
            }
        }

        

        $requests = $request->getQueryString();

        if ($new_status != $status && $new_status == 1 && Auth::user()->type == config('types.clinic')) {
            return redirect()->route('orders.edit', [$id, $requests]);
        } else{
            Content::calcLabRating($data->to_branch_id);
            
            return redirect()
                ->route('orders', $requests)
                ->with([
                    'message'    => __('translations.success'),
                    'alert-type' => 'success',
                ]);
        }
    }

    protected function proccessDelivery($status, $data) {
    	$order = $data['order'];
    	$params = isset($data['delivery_params']) ? $data['delivery_params'] : false;

    	switch ($status) {
    		case 3:
    			$delivery = new Order_delivery;

                $delivery->order_id = $order->id;
                $delivery->bill_to = isset($params['bill_to']) && $params['bill_to'] ? $params['bill_to'] : config('types.lab');
                $delivery->from_branch = $order->branch_id;
                $delivery->to_branch = $order->to_branch_id; 
                $delivery->delivery_id = $order->delivery_id;
                $delivery->delivery_invoice = isset($params['delivery_invoice']) && $params['delivery_invoice'] ? $params['delivery_invoice'] : null;
                $delivery->delivery_service_type = isset($params['delivery_service_type']) && $params['delivery_service_type'] ? $params['delivery_service_type'] : null;
                $delivery->length = isset($params['length']) && $params['length'] ? $params['length'] : '0.0000';
                $delivery->width = isset($params['width']) && $params['width'] ? $params['width'] : '0.0000';
                $delivery->height = isset($params['height']) && $params['height'] ? $params['height'] : '0.0000';
                $delivery->weight = isset($params['weight']) && $params['weight'] ? $params['weight'] : '0.0000';

                $delivery->save(); 
/*
                if (isset($params['bill_to']) && $params['bill_to'] == config('types.clinic')) {
                	$card_data = array(
		                'ccnumber' => $params['card'],
		                'ccexp' => $params['exp'],
		                'cvv' => $params['cvv']
		            );

		            $delivery_responce = $delivery_service::create($order->id, $card_data);

		            if ($delivery_responce) {
		            	$delivery->delivery_responce = $delivery_responce;
		            	$delivery->save();
		            }
                }
*/
    			break;
    		/*case 4:
    			$delivery = Order_delivery::with('delivery_info')
    				->where('order_id', $order->id)
                    ->where('from_branch', $order->branch_id)
                    ->where('to_branch', $order->to_branch_id)
                    ->first();

                if ($delivery && !$delivery->delivery_invoice) {
                	$delivery_service = '\\App\Services\\'.ucfirst($delivery->delivery_info->code);

                	$delivery_request = false;

                	if ($delivery->delivery_responce) {		                
		                $delivery_request = $delivery_service::acceptRequest($delivery->delivery_responce);
		            } else {
		            	$card_data = array(
			                'ccnumber' => $params['card'],
			                'ccexp' => $params['exp'],
			                'cvv' => $params['cvv']
			            );

			            $delivery_responce = $delivery_service::create($order->id, $card_data);

			            if ($delivery_responce) {
			            	$delivery->delivery_responce = $delivery_responce;
			            	$delivery->save();

			            	$delivery_request = $delivery_service::acceptRequest($delivery_responce);
			            }
		            }

	                if ($delivery_request) {		                   
                        $delivery->delivery_invoice = $delivery_request['delivery_invoice'];
                        $delivery->delivery_label = $delivery_request['delivery_label'];
                        $delivery->price = $delivery_request['price'];
                        $delivery->currency = $delivery_request['currency'];

                        $delivery->save();
	                }
                }
    						
    			break;*/
    		case 15:
                $delivery = new Order_delivery;

                $delivery->order_id = $order->id;
                $delivery->bill_to = isset($params['bill_to']) && $params['bill_to'] ? $params['bill_to'] : config('types.lab');
                $delivery->from_branch = $order->to_branch_id;
                $delivery->to_branch = $order->branch_id; 
                $delivery->delivery_id = $order->delivery_id;
                $delivery->delivery_invoice = isset($params['delivery_invoice']) && $params['delivery_invoice'] ? $params['delivery_invoice'] : null;
                $delivery->delivery_service_type = isset($params['delivery_service_type']) && $params['delivery_service_type'] ? $params['delivery_service_type'] : null;
                $delivery->length = isset($params['length']) && $params['length'] ? $params['length'] : '0.0000';
                $delivery->width = isset($params['width']) && $params['width'] ? $params['width'] : '0.0000';
                $delivery->height = isset($params['height']) && $params['height'] ? $params['height'] : '0.0000';
                $delivery->weight = isset($params['weight']) && $params['weight'] ? $params['weight'] : '0.0000';

                $delivery->save(); 

    			break;
    		/*case 16:
    		
    			break;*/
    		default:
    			break;
    	}
    }
}