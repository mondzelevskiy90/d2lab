<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\Content;
use App\Services\Userdata;
use App\Services\Currencies;
use App\Models\Branch;
use App\Models\User_type;
use App\Models\Industry;
use App\Models\Industry_service;
use App\Models\Locality;
use App\Models\Price;
use App\Models\Price_service;
use App\Models\Option;
use App\Models\Option_to_price_service;
use App\Models\Order_review;

class CatalogController extends Controller {	
    public function index() {
        $data = array();

        $types = Userdata::getTypes();

        $industries = Content::getIndustriesByParent(0);

        foreach ($industries as $industry) {
            $industry->childrens = Content::getIndustriesByParent($industry->id);

            $data[] = $industry;
        }

    	$breadcrumbs = array();

    	$breadcrumbs[] = array(
			'text' => __('translations.home'),
			'href' => route('home')
		);

        $breadcrumbs[] = array(
            'text' => __('translations.catalog'),
            'href' => route('catalog')
        );

        $seo = (object)[
            'name' => __('translations.catalog'),
            'meta_title' => __('translations.catalog'),
            'meta_description' => 'Dentist2Lab service catalog page',
        ];

    	return view('Frontend.pages.catalog.index', compact(
    		'data', 
            'types',
    		'seo',
    		'breadcrumbs'
    	));
    }

    public function type($slug1, $slug2, Request $request) {
        $industry = Industry::where('slug', trim($slug1))->where('status', 1)->firstOrFail();
        $type = User_type::where('slug', trim($slug2))
            ->where('catalog', 1)
            ->firstOrFail();

        if ($request->input('locality_id')) {
            $locality_id = (int)$request->input('locality_id');
        } else {
            $locality_id = (int)Userdata::detectLocation($_SERVER['REMOTE_ADDR']);
        }

        $services = Industry_service::where('industry_id', $industry->id)
            ->where('status', 1)
            ->orderBy('sort_order', 'ASC')
            ->get();

        $options = Option::where('industry_id', $industry->id)
            ->where('status', 1)
            ->orderBy('sort_order', 'ASC')
            ->get();

        $filter = true;
        $currency = Currencies::getCurrencyById(1); //TO DO -> change to user selected currency!!!
        $service_id = $request->input('service_id');
        $search = $request->input('search');
        $sort = $request->input('sort');
        $rating_from = $request->input('rating_from');
        $rating_to = $request->input('rating_to');
        $price_from = $request->input('price_from');
        $price_to = $request->input('price_to');
        $options_selected = $request->input('options'); //TO DO -> change this filter to multiselect!!!
        $zip_code = $request->input('zip_code');

        $selected_filters = array(
            'search' => $search,
            'service' => $service_id,
            'sort' => $sort,
            'options' => $options_selected,
            'rating_from' => $rating_from,
            'rating_to' => $rating_to,
            'price_from' => $price_from,
            'price_to' => $price_to,
            'zip_code' => $zip_code,
        );

        $sort_by = 'rating';
        $order = 'DESC';

        if ($sort) {
            switch ($sort) {
                case 'rating_asc':
                    $sort_by = 'rating';
                    $order = 'ASC';
                    break;

                case 'name_asc':
                    $sort_by = 'name';
                    $order = 'ASC';
                    break;

                case 'name_desc':
                    $sort_by = 'name';
                    $order = 'DESC';
                    break;
                
                default:
                    $sort_by = 'rating';
                    $order = 'DESC';
                    break;
            }
        }

        $data = Branch::where('industry_id', $industry->id)
            ->where('type', $type->id)
            ->where('status', 1);
           // ->where('verified', 1);
        
        if ($search) {
            $data->where('name', 'LIKE', $search.'%');
        }

        if ($rating_from) {
        	$data->where('reviews_rating', '>=', (int)$rating_from);
        }

        if ($rating_to) {
        	$data->where('reviews_rating', '<=', (int)$rating_to);
        }

        $locality = false;

        if ($zip_code) {
            $data->where('zip_code', (int)$zip_code);

            $locality_id = false;
        } else if ($locality_id) {
            if ($locality_id == 1) {
                $localities_arr = [1];

                $sublocalities = Locality::where('parent_id', 1)->get();

                foreach ($sublocalities as $sublocality) {
                    $localities_arr[] = $sublocality->id;
                }

                $data->whereIn('locality', $localities_arr);
            } else {
               $data->where('locality', $locality_id);
            }  

            $locality = Locality::where('id', $locality_id)->first();          
        }

        $services_id = array();

        if ($service_id) {          
            $services_id = Content::getServiceChilrdensByFilter($service_id);
            $services_id[] = (int)$service_id;
        }

        if ($service_id || $price_from || $price_to) {
            $data->whereIn('id', function($q) use ($services_id, $price_from, $price_to, $currency) {
                $q->select('branch_id')
                    ->from(with(new Price)->getTable())
                    ->whereIn('id', function($q) use ($services_id, $price_from, $price_to, $currency) {
                        $q->select('price_id')
                            ->from(with(new Price_service)->getTable());

                            if (count($services_id)) {
                                $q->whereIn('industry_service_id', $services_id);
                            }

                            if ($price_from && $price_from != 0) {
                                $q->where('price', '>=', number_format($price_from / $currency->value, 2, '.', ''));
                            }

                            if ($price_to && $price_to != 0) {
                                $q->where('price', '<=', number_format($price_to / $currency->value, 2, '.', ''));
                            }
                    });
            });
        }

        if ($options_selected) {
            $options_selected = explode(',', $options_selected);
            
            $data->whereIn('id', function($q) use ($options_selected) {
                $q->select('branch_id')
                    ->from(with(new Price)->getTable())
                    ->whereIn('id', function($q) use ($options_selected) {
                        $q->select('price_id')
                            ->from(with(new Option_to_price_service)->getTable())
                            ->whereIn('option_id', $options_selected);
                    });
            });
        }

        $total = $data->count();

        $all_geolocations = $data;
        $all_geolocations = $all_geolocations->get('id')->toArray(); 

        if ($services_id || $price_from || $price_to) {
            $data = $data->with([
                'prices' => function($q) use ($services_id, $price_from, $price_to, $currency) {
                    if ($price_from && $price_from != 0) {
                        $q->where('price', '>=', number_format($price_from / $currency->value, 2, '.', ''));
                    }

                    if ($price_to && $price_to != 0) {
                        $q->where('price', '<=', number_format($price_to / $currency->value, 2, '.', ''));
                    }

                    if (count($services_id)) {
                        $q->whereIn('industry_service_id', $services_id);
                    }
                }
            ]);
        } else {
            $data = $data->with([
                'prices'
            ]);
        }

        $data = $data->orderBy($sort_by, $order)
            ->paginate(4);

        $geolocations = array();
       
        foreach ($all_geolocations as $value) {
            $geolocations[] = $value['id'];
        }

        $geolocations = implode(',', $geolocations);

        foreach ($data as $d) {
            $geolocation = json_decode($d->geolocation);

            $d->lat = isset($geolocation->lat) ? $geolocation->lat : false;
            $d->lon = isset($geolocation->lon) ? $geolocation->lon : false;;

            if ($d->prices) {
                foreach($d->prices as $price) {
                    $price_service = Industry_service::where('id', $price->industry_service_id)->first();

                    if ($price_service) {
                        $price->name = $price_service->name;
                        $price->value = Currencies::getPriceStr($price->price, $price->currency_id);
                    } else {
                        continue;
                    }
                    
                } 
            }

            $d->reviews = Order_review::where('branch_id', (int)$d->id)
                ->where('status', 1)
                ->orderBy('created_at', 'ASC')
                ->get();
        }

        $name = $industry->name.' '.$type->catalog_name;

        if ($service_id) {
            foreach($services as $service) {
                if ($service->id == $service_id) $name .= ' '.$service->name;
            }
        }

        if ($locality) {
            $name .= ' '.$locality->name;
        }

        $name .= ' catalog'.($request->input('page') && $request->input('page') != 1 ? ' page #'.(int)$request->input('page') : '');

        $breadcrumbs = array();

        $breadcrumbs[] = array(
            'text' => __('translations.home'),
            'href' => route('home')
        );

       /* $breadcrumbs[] = array(
            'text' => __('translations.catalog'),
            'href' => route('catalog')
        ); */

        $breadcrumbs[] = array(
            'text' => $name,
            'href' => route('catalog')
        );

        $seo = (object)[
            'name' => $name,
            'meta_title' => $name,
            'meta_description' => $name
        ];

        $requests = $request->getQueryString();

        return view('Frontend.pages.catalog.type', compact(
            'data',
            'filter', 
            'selected_filters',
            'industry',
            'type',
            'locality',
            'services',
            'options',
            'geolocations',
            'total',
            'currency',
            'requests',
            'seo',
            'breadcrumbs'
        ));
    }
}