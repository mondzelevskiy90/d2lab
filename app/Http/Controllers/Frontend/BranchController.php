<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\Content;
use App\Services\Userdata;
use App\Services\Currencies;
use App\Models\Branch;
use App\Models\Branch_list;
use App\Models\User_type;
use App\Models\Industry;
use App\Models\Order;
use App\Models\Order_prescription;
use App\Models\Order_review;
use App\Models\Price;
use App\Models\Price_service;
use App\Models\Portfolio;
use App\Models\Portfolio_image;
use DB;
use Auth;

class BranchController extends Controller {
    public function index($slug, $id, Request $request) {        
        $type = User_type::where('slug', trim($slug))
            ->where('catalog', 1)
            ->firstOrFail();

        $data = Branch::where('id', (int)$id)
            ->where('type', $type->id)
            ->where('status', 1)
           // ->where('verified', 1)
            ->firstOrFail();
        
        $currency = Currencies::getCurrencyById(1);
        $services = Content::getServicesByIndustry($data->industry_id, false, true);
        $options = Content::getOptionsByIndustry($data->industry_id)->toArray();
        $price = Price::where('branch_id', (int)$id)->first();

        $prices = array();
        $data->apl = false;

        if ($price) {
            $price_id = $price->id;
            $prices_list = Content::getPriceServices($price_id, true);

            $branch_total_price = 0;
            $avarage_total_price = 0;    
            
            foreach ($prices_list as $price) {               
                $parent_id = $price['parent_id'];
                $parent_service = isset($services[$parent_id]) ? $services[$parent_id] : ['id' => 0, 'name' => 'Common'];

                if (!isset($prices[$parent_id])) {
                    $prices[$parent_id] = $parent_service;
                    $prices[$parent_id]['prices'] = array();
                }

                $price_options = array();

                foreach($price['options'] as $id) {
                    foreach ($options as $option) {
                        if ($option['id'] == $id) {
                            $price_options[] = $option['name'];
                        }
                    }
                }

                $branch_total_price = $branch_total_price + (float)$price['price'];

                $all_service_price = Price_service::where('price_id', '!=', $price_id)
                    ->where('industry_service_id', (int)$price['industry_service_id'])
                    ->pluck('price');

                $avarage_service_price = 0;

                foreach ($all_service_price as $service_price) {
                    $avarage_service_price = $avarage_service_price + (float)$service_price;
                }

                if ($avarage_service_price > 0) {
                    $avarage_service_price = $avarage_service_price / count($all_service_price);
                }

                $avarage_total_price = $avarage_total_price + $avarage_service_price;

                $prices[$parent_id]['prices'][] = array(
                    'name' => $services[$price['industry_service_id']]['name'], 
                    'value' => Currencies::getPriceStr($price['price'], $price['currency_id']),
                    'options' => implode(', ', $price_options),
                    'show_prices_message' => $price['show_prices_message']
                );
            }

            if ($avarage_total_price > 0 && $branch_total_price > 0) {
                $apl = $branch_total_price / $avarage_total_price;

                $apl_data = array();

                if ($apl > 1.35) {
                    $apl_data['value'] = '$';
                    $apl_data['title'] = __('translations.avarage_low_prices');
                } else if ($apl >= 0.65 && $apl <= 1.35) {
                    $apl_data['value'] = '$$';
                    $apl_data['title'] = __('translations.avarage_middle_prices');
                } else {
                    $apl_data['value'] = '$$$';
                    $apl_data['title'] = __('translations.avarage_hight_prices');
                } 

                $data->apl = $apl_data;
            }
        }

        $data->prices = $prices;        

        $data->portfolio = Portfolio_image::with('service')
        	->select('industry_service_id', DB::raw('group_concat(DISTINCT portfolio_images.image) as images')) 
        	->where('portfolio_id', function($q) use ($data) {
                $q->select('id')
                    ->from(with(new Portfolio)->getTable())
                    ->where('branch_id', $data->id);
            }) 
        	->groupBy('industry_service_id')
        	->get();

        $data->reviews = Order_review::with([
                'user',
                'branch',
                'comments'
            ])
            ->where('branch_id', (int)$data->id)
            ->where('status', 1)
            ->orderBy('created_at', 'ASC')
            ->get();

        $data->location_name = Userdata::getLocality($data->locality);

        $industry = Industry::where('id', $data->industry_id)->first();

        $name = $data->name;

        $mybranches = null;

        if (Auth::check()) {
            $mybranches = Branch_list::where('branch_id', $data->id)
                ->where('user_id', Auth::user()->id)
                ->first();
        }

        $branch_focus = array();

        $branch_services = Order_prescription::with('service')
            ->whereIn('order_id', function($q) use ($data) {
                $q->select('id')
                    ->from(with(new Order)->getTable())
                    ->where('to_branch_id', $data->id);
            });

        $total_services = $branch_services->count('id');

        $branch_services = $branch_services->groupBy('service_id')
            ->selectRaw('count(*) as total, service_id')
            ->orderBy('total', 'DESC')
            ->limit(4)
            ->get();

        $cnt = 1; 
        $sum_percent = 100; 

        foreach ($branch_services as $branch_service) {
            $percent = round($branch_service->total / $total_services * 100, 2) - 0.01;

            $sum_percent = $sum_percent - $percent;

            $branch_focus[] = array(
                'name' => $branch_service->service ? $branch_service->service->name : '',
                'color' => '#5e'.$cnt.$cnt.'1d',
                'percent' => $percent 
            );

            $cnt++;
            $cnt++;
        }

        if ($sum_percent != 100 && $sum_percent > 0) {
            $branch_focus[] = array(
                'name' => 'Others',
                'color' => '#f3dc3e',
                'percent' => $sum_percent 
            );
        }

        $breadcrumbs = array();

        $breadcrumbs[] = array(
            'text' => __('translations.home'),
            'href' => route('home')
        );

      /*  $breadcrumbs[] = array(
            'text' => __('translations.catalog'),
            'href' => route('catalog')
        ); */
        
        $breadcrumbs[] = array(
            'text' => $industry->name.' '.$type->catalog_name.' catalog',
            'href' => route('catalog.industry.type', [$industry->slug, $type->slug])
        );

        $breadcrumbs[] = array(
            'text' => $name,
            'href' => route('catalog')
        );

        $seo = (object)[
            'name' => $name,
            'meta_title' => $name,
            'meta_description' => $name.','
        ];

        return view('Frontend.pages.catalog.branch', compact(
            'data', 
            'mybranches',
            'branch_focus',
            'seo',
            'breadcrumbs'
        ));
    }
}