<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Services\Img;
use App\Services\Userdata;
use App\Models\Branch;
use App\Models\Branch_staff;
use App\Models\Dashboard;
use App\Models\Dashboard_custom;
use App\Models\Dashboard_default;
use App\Models\Locality;
use App\Models\Invoice;
use App\Models\Notification_message;
use App\Models\Order;
use App\Models\Order_chat;
use App\Models\Order_review;
use App\Models\Order_review_comment;
use App\Models\Order_status;
use App\Models\User;
use Auth;

class AccountController extends Controller {
    private $tech_statuses = [12];
    private $hide_to_lab = [1,2];
    private $complited = [17];
    private $procces = [3,4,6,10,11,12,13,14,15,16,20];
    private $cancelled = [7,9,18];

    public function index() {
        $data = array();

        $dashboards = Dashboard::with([
                'dashboard_default' => function ($q) {
                    $q->where('role_id', Auth::user()->role_id);
                },
                'dashboard_custom' => function ($q) {
                    $q->where('user_id', Auth::user()->id);
                }
            ])
            ->where('status', 1)
            ->orderBy('sort_order', 'ASC')
            ->orderBy('id', 'ASC')
            ->get();

        $defaults = Dashboard_default::with([
                'dashboard' => function ($q) {
                    $q->where('status', 1);
                }
            ])
            ->where('role_id', Auth::user()->role_id)
            ->get();

        $preset = 'dashboard_default';

        $customs = Dashboard_custom::where('user_id', Auth::user()->id)->pluck('dashboard_id')->toArray();

        if (count($customs)) $preset = 'dashboard_custom';
        
        $orders = array();
        $branches = array();
        $reviews = array();
        $chats = array();

        $order_statuses = Order_status::whereNotIn('id', $this->hide_to_lab)
            ->orderBy('sort_order', 'ASC');

        switch (Auth::user()->role_id) {
            case config('roles.clinic_head'):
                $branches = Branch::where('owner_id', Auth::user()->id)->pluck('id')->toArray();

                $orders = Order::whereIN('branch_id', $branches);
                break;
            case config('roles.clinic_staff'):
                $staff = Branch_staff::where('user_id', Auth::user()->id)->first();

                if ($staff) $branches[] = $staff->branch_id;

                $orders = Order::whereIN('branch_id', $branches);                
                break;
            case config('roles.doctor'):
                $staff = Branch_staff::where('user_id', Auth::user()->id)->first();

                if ($staff) $branches[] = $staff->branch_id;
                
                $orders = Order::where('user_id', Auth::user()->id);
                break;
            case config('roles.lab_head'):
                $branches = Branch::where('owner_id', Auth::user()->id)->pluck('id')->toArray();

                $orders = Order::whereIN('to_branch_id', $branches)
                    ->whereNotIn('status', $this->hide_to_lab);
                break;
            case config('roles.lab_staff'):
                $staff = Branch_staff::where('user_id', Auth::user()->id)->first();

                if ($staff) $branches[] = $staff->branch_id;

                $orders = Order::whereIN('to_branch_id', $branches)
                    ->whereNotIn('status', $this->hide_to_lab);                
                break;
            case config('roles.tech'):
                $staff = Branch_staff::where('user_id', Auth::user()->id)->first();

                if ($staff) $branches[] = $staff->branch_id;
                
                $orders = Order::whereIn('to_branch_id', $branches)
                    ->whereIn('status', $this->tech_statuses);

                $order_statuses->where('id', $this->tech_statuses);
                break;
            default:
                $orders = Order::whereIN('branch_id', $branches);
                break;
        }

        $orders_ids = $orders->pluck('id')->toArray();

        $reviews = Order_review::with('user')
            ->select('id', 'order_id', 'user_id', 'review', 'status', 'created_at')
            ->whereIN('order_id', $orders_ids)
            ->where('status', 1);

        $comments = Order_review_comment::with('user')
            ->select('id', 'order_id', 'user_id', 'review', 'status', 'created_at')
            ->whereIN('order_id', $orders_ids)
            ->where('status', 1);

        $reviews = $reviews->union($comments)
            ->orderBy('id', 'DESC')
            ->limit(5)
            ->get();

        $chats = Order_chat::with('user')
            ->whereIN('order_id', $orders_ids)
            ->where('status', 1)
            ->orderBy('id', 'DESC')
            ->limit(5)
            ->get();

        $orders = $orders->with([
                'status_info',
                'payment_info',
                'total',
                'invoice'
            ])
            ->get();

        $order_statuses = $order_statuses->get();

        foreach ($dashboards as $dashboard) {
            if (is_null($dashboard->{$preset})) continue;

            $code = $dashboard->code;

            switch ($code) {
                case 'text':
                    $content = $dashboard->content;
                    break;
                case 'reviews':
                    $content = array(
                        'text' => $dashboard->content,
                        'data' => $reviews
                    );

                    break;
                case 'chats':
                    $content = array(
                        'text' => $dashboard->content,
                        'data' => $chats
                    );

                    break;
                case 'orders':
                    $content = array(
                        'text' => $dashboard->content,
                        'data' => $orders->reverse()
                    );

                    break;
                case 'statuses':
                    $orders_statuses_data = array();

                    foreach ($order_statuses as $status) {
                        $orders_statuses_data[$status->id] = array(
                            'id' => $status->id,
                            'name' => $status->name,
                            'icon' => $status->icon,
                            'count' => 0
                        );
                    }

                    foreach ($orders as $order) {
                        if (!in_array($order->status, $this->hide_to_lab)) $orders_statuses_data[$order->status]['count'] = $orders_statuses_data[$order->status]['count'] + 1;
                    }

                    $content = array(
                        'text' => $dashboard->content,
                        'data' => $orders_statuses_data
                    );

                    break;
                case 'locations':
                    $locations_data = array();

                    $branches_data = Branch::whereIN('id', $branches)
                        ->where('status', '!=', 4)
                        ->orderBy('name', 'ASC')
                        ->get();

                    foreach ($branches_data as $branch) {
                        $total_orders = 0;
                        $total_payments = 0;
                        $сomplited_orders = 0;
                        $сomplited_payments = 0;
                        $procces_orders = 0;
                        $procces_payments = 0;
                        $cancelled_orders = 0;
                        $cancelled_payments = 0;
                        $unpaid = 0;
                        $paid = 0;

                        foreach ($orders as $order) {
                            if (Auth::user()->type == config('types.lab') && $order->to_branch_id != $branch->id) continue;
                            if (Auth::user()->type == config('types.clinic') && $order->branch_id != $branch->id) continue;

                            if (in_array($order->status, $this->hide_to_lab) || !$order->total) continue;

                            if ($order->total->price != '0.00') {
                                $price = (float)$order->total->price;
                            } else {
                                $price = (float)$order->total->advance_price;
                            }

                            if (in_array($order->status, $this->cancelled)) {
                                $cancelled_orders++;
                                $cancelled_payments = (float)$cancelled_payments + $price;
                            }

                            if (in_array($order->status, $this->complited)) {
                                $сomplited_orders++;
                                $сomplited_payments = (float)$сomplited_payments + $price;
                            }

                            if (in_array($order->status, $this->procces)) {
                                $procces_orders++;
                                $procces_payments = (float)$procces_payments + $price;
                            }

                            if ($order->invoice) {
                                if ($order->payment_status == 4) {
                                    $paid = (float)$paid + $price;
                                } else {
                                    $unpaid = (float)$unpaid + $price;
                                }
                            }

                            
                        }

                        $locations_data[] = array(
                            'id' => $branch->id,
                            'name' => $branch->name,
                            'logo' => $branch->logo,
                            'total_orders' => $total_orders,
                            'total_payments' => $total_payments,
                            'сomplited_orders' => $сomplited_orders,
                            'сomplited_payments' => round($сomplited_payments),
                            'procces_orders' => $procces_orders,
                            'procces_payments' => round($procces_payments),
                            'cancelled_orders' => $cancelled_orders,
                            'cancelled_payments' => round($cancelled_payments),
                            'unpaid' => round($unpaid),
                            'paid' => round($paid)
                        );
                    }

                    $content = array(
                        'text' => $dashboard->content,
                        'data' => $locations_data
                    );

                    break;
                case 'graph':
                    $labels = array();
                    $total = array();
                    $paid = array();
                    $unpaid = array();

                    $invoices = Invoice::with('total')
                        ->whereIN('order_id', $orders_ids)
                        ->where('status', 1)
                        ->where('created_at', '>', date('Y-m-d', strtotime('-6 month')).' 00:00:00')
                        ->get();

                    for ($i = 0; $i < date('n'); $i++) {
                        $labels[$i] = date('F', strtotime('-'.$i.' month'));
                        $month_num = date('n', strtotime('-'.$i.' month'));
                        $total[$i] = 0;
                        $paid[$i] = 0;
                        $unpaid[$i] = 0;

                        foreach ($invoices as $invoice) {
                            if ($invoice->total && $month_num == date('n', strtotime($invoice->created_at))) {
                                $total[$i] = round($total[$i] + $invoice->total->price);

                                if ($invoice->payment_status == 4) {
                                   $paid[$i] = round($paid[$i] + $invoice->total->price);
                                } else {
                                   $unpaid[$i] = round($unpaid[$i] + $invoice->total->price); 
                                }
                            }
                        }
                    }

                    $content = array(
                        'text' => $dashboard->content,
                        'labels' => json_encode(array_reverse($labels)),
                        'total' => json_encode(array_reverse($total)),
                        'paid' => json_encode(array_reverse($paid)),
                        'unpaid' => json_encode(array_reverse($unpaid))
                    );

                    break;                
                case 'diagram':
                    $labels = array();
                    $pies = array();

                    foreach ($order_statuses as $status) {
                        if (in_array($status->id, $this->hide_to_lab)) continue;
                        if (Auth::user()->role_id == config('roles.tech') && !in_array($status->id, $this->tech_statuses)) continue;

                        $labels[$status->id] = $status->name;
                        $pies[$status->id] = 0;
                    }

                    foreach ($orders as $order) {
                        if (in_array($order->status, $this->hide_to_lab)) continue;
                        if (Auth::user()->role_id == config('roles.tech') && !in_array($order->status, $this->tech_statuses)) continue;

                        $pies[$order->status] = $pies[$order->status] + 1;
                    }

                    $content = array(
                        'text' => $dashboard->content,
                        'labels' => json_encode($labels),
                        'pies' => json_encode($pies)
                    );

                    break;
                case 'notifications':
                    $notifications = Notification_message::whereIN('branch_id', $branches)
                            ->where('created_at', '<=', date('Y-m-d').' 23:59:59')
                            ->orderBy('created_at', 'DESC')
                            ->get();

                    $content = array(
                        'text' => $dashboard->content,
                        'data' => $notifications 
                    );
                    break;
                case 'update':
                    $content = $dashboard->content;
                    break;
                case 'catalog':
                    $content = $dashboard->content;
                    break;                
                default:
                    $code = 'text';
                    $content = $dashboard->content;
                    break;
            }
           
            $data[] = view('Frontend.templates.dashboard.'.$code, compact('content', 'code'));
        }

        $mylabs = false;

        if (Auth::user()->type == config('types.clinic')) {
            $mylabs = \App\Models\Branch_list::where('user_id', Auth::user()->id)->first();
        }
        
    	$breadcrumbs = array();

    	$breadcrumbs[] = array(
			'text' => __('translations.home'),
			'href' => route('home')
		);

		$breadcrumbs[] = array(
			'text' => __('translations.account'),
			'href' => route('account')
		);

        $seo = (object)[
            'name' => __('translations.account'),
            'meta_title' => __('translations.account'),
            'meta_description' => 'Dentist2Lab service account page',
        ];

    	return view('Frontend.pages.account.dashboard', compact(
    		'data',
            'defaults', 
            'customs',
            'mylabs',
    		'seo',
    		'breadcrumbs'
    	));
    }

    public function setup(Request $request) {
        Dashboard_custom::where('user_id', Auth::user()->id)->delete();

        if ($request->input('dashboards') && is_array($request->input('dashboards'))) {
           foreach ($request->input('dashboards') as $key => $value) {
               $data = new Dashboard_custom;

               $data->user_id = Auth::user()->id;
               $data->dashboard_id = $key;
               $data->save();
            } 
        }

        return redirect()
            ->route('account')
            ->with([
                'message'    => __('translations.success'),
                'alert-type' => 'success',
            ]); 
    }

    public function settings(Request $request) {
        $data = User::with([
                'role_info',
                'type_info'
            ])
            ->where('id', Auth::user()->id)
            ->whereIn('status', [config('statuses.new'),config('statuses.approved')])
            ->firstOrFail();

        $locality = Locality::with([
                'region',
                'country'
            ])
            ->where('id', (int)$data->locality)
            ->first();

        $types = Userdata::getTypes();

        if ($request->method() == 'POST') {
            $name = $request->input('name');
            $surname = $request->input('surname');
            $phone = $request->input('phone');
            $company = $request->input('company');
            $locality = $request->input('locality');
            $zip_code = $request->input('zip_code');
            $address = $request->input('address');
            $logo = $request->file('logo');   
            $avatar = $request->file('avatar');         
            $password = $request->input('password');
            $type = $request->input('type');
            $role_id = false;

            if ($type && $type == config('types.clinic')) $role_id = config('roles.clinic_head');
            if ($type && $type == config('types.lab')) $role_id = config('roles.lab_head');
            
            $messages = array(
                'name.required' => 'Field is required!',                
                'name.max' => 'Field contains more then 255 symbols!',
                'surname.required' => 'Field is required!',               
                'surname.max' => 'Field contains more then 255 symbols!'
            );           

            $rules = array(
                'name' => 'required|max:255',
                'surname' => 'required|max:255'
            );

            if ($password) {                
                $messages['password.min'] = 'Field contains less then 6 symbols!';
                $messages['password.max'] = 'Field contains more then 255 symbols!';

                $rules['password'] = 'min:6|max:255';
            } 

            if ($company) {
                $messages['company.max'] = 'Field contains more then 255 symbols!';
                $rules['company'] = 'max:255';
            }

            if ($logo) {
                $messages['logo.image'] = 'Uploaded file is not an image!';
                $messages['logo.mimes'] = 'Uploaded file format is not correct!';
                $messages['logo.max'] = 'Uploaded file under 10Mb!';

                $rules['logo'] = 'image|mimes:jpeg,jpg,png|max:10240';
            }

            if ($avatar) {
                $messages['avatar.image'] = 'Uploaded file is not an image!';
                $messages['avatar.mimes'] = 'Uploaded file format is not correct!';
                $messages['avatar.max'] = 'Uploaded file under 10Mb!';

                $rules['avatar'] = 'image|mimes:jpeg,jpg,png|max:10240';
            }

            if ($type && $data->role_id != config('roles.doctor') && $data->role_id != config('roles.tech')) {
                $messages['company.required'] = 'Field is required!';
                $messages['company.max'] = 'Field contains more then 255 symbols!';
                $messages['zip_code.required'] = 'Field is required!';
                $messages['country.required'] = 'Field is required!';
                $messages['country.not_in'] = 'Field is not selected!';
                $messages['region.required'] = 'Field is required!';
                $messages['region.not_in'] = 'Field is not selected!';
                $messages['locality.required'] = 'Field is required!';
                $messages['locality.not_in'] = 'Field is not selected!';
                $messages['address.required'] = 'Field is required!';
                $messages['phone.required'] = 'Field is required!';

                $rules['zip_code'] = 'required';
                $rules['country'] = 'required|not_in:0';
                $rules['region'] = 'required|not_in:0';
                $rules['locality'] = 'required|not_in:0';
                $rules['address'] = 'required';
                $rules['phone'] = 'required';
            }

            $request->validate($rules, $messages);

            $timestamp = time();

            $data->name = $name;
            $data->surname = $surname;
            $data->phone = $phone ? $phone : null; 
            $data->company = $company ? $company : null;
            $data->zip_code = $zip_code ? $zip_code : null;
            $data->locality = $locality ? $locality : 0;
            $data->address = $address ? $address : null;
            $data->updated_at = $timestamp;           

            if ($role_id) $data->role_id = $role_id; 
            if ($password && $password != '') {
                $data->password = Hash::make($password);

                if ($data->apple && !is_null($data->apple)) $data->apple = $password;
            }

            if ($type) {
                $data->type = $type;
                $data->status = config('statuses.new');
            }

            if ($avatar) {
                if ($request->input('data_avatar') && $request->input('data_avatar')['status'] == 1) {
                    $data->avatar = Img::cropUploadedImage($avatar, '/users/'.date('FY').'/', 50, 50, $request->input('data_avatar'));
                } else {
                    $data->avatar = Img::fitImage($avatar, '/users/'.date('FY').'/', 50, 50, 80);
                }

            }
            if ($logo) {
                if ($request->input('data_logo') && $request->input('data_logo')['status'] == 1) {
                    $data->logo = Img::cropUploadedImage($logo, '/logos/'.date('FY').'/', 275, 275, $request->input('data_logo'));
                } else {
                    $data->logo = Img::fitImage($logo, '/logos/'.date('FY').'/', 275, 275, 80);
                }
            }

            $data->save(); 
           
            return redirect()
                ->route('account')
                ->with([
                    'message'    => __('translations.success'),
                    'alert-type' => 'success',
                ]);            
        }

        $breadcrumbs = array();

        $breadcrumbs[] = array(
            'text' => __('translations.home'),
            'href' => route('home')
        );

        $breadcrumbs[] = array(
            'text' => __('translations.account'),
            'href' => route('account')
        );

        $breadcrumbs[] = array(
            'text' => __('translations.settings'),
            'href' => route('settings')
        );

        $seo = (object)[
            'name' => __('translations.settings'),
            'meta_title' => __('translations.settings'),
            'meta_description' => 'Dentist2Lab service account settings page',
        ];

        return view('Frontend.pages.account.settings', compact(
            'data', 
            'seo',
            'breadcrumbs',
            'types',
            'locality'
        ));
    }
}