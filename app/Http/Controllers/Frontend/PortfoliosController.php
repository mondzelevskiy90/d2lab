<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\Content;
use App\Services\Currencies;
use App\Models\Branch;
use App\Models\Price;
use App\Models\Price_service;
use App\Models\Portfolio;
use App\Models\Portfolio_image;
use Auth;

class PortfoliosController extends Controller {
    public function index() {
        $branches = Branch::where('owner_id', Auth::user()->id)
            ->where('status', '!=', 4)
            ->get();

        $prices = Price::where('owner_id', Auth::user()->id)
            ->get();

        $data = Portfolio::with([
                'branch' => function($q) {
                    $q->where('status', '!=', 4);
                }
            ])
            ->where('owner_id', Auth::user()->id)
            ->get();

        $breadcrumbs = array();

        $breadcrumbs[] = array(
            'text' => __('translations.home'),
            'href' => route('home')
        );

        $breadcrumbs[] = array(
            'text' => __('translations.account'),
            'href' => route('account')
        );

        $breadcrumbs[] = array(
            'text' => __('translations.portfolios'),
            'href' => route('portfolios')
        );

        $seo = (object)[
            'name' => __('translations.portfolios'),
            'meta_title' => __('translations.portfolios'),
            'meta_description' => 'Dentist2Lab service portfolios page',
        ];

        return view('Frontend.pages.account.portfolio.index', compact(
            'data', 
            'branches',
            'prices',
            'seo',
            'breadcrumbs'
        ));
    }

    public function create(Request $request) {
        if (!Content::checkBranch()) return redirect()->route('account');

        $data = array();        

        if ($request->method() == 'POST') {
            $data = new Portfolio;

            $branch = Branch::where('id', $request->input('branch_id'))->first();

            $data->owner_id = Auth::user()->id;
            $data->branch_id = $request->input('branch_id');
            $data->name = 'Portfolio for '.$branch->name;             
            $data->sort_order = 0;
            $data->status = 1;            
            $data->save(); 
           
            return redirect()->route('portfolio.edit', $data->id); 
        }

        $branches = Branch::where('owner_id', Auth::user()->id)
            ->where('status', '!=', 4)
            ->whereNotIn('id', function($query){
                $query->select('branch_id')
                    ->from(with(new Portfolio)->getTable())
                    ->where('owner_id', Auth::user()->id);
            })
            ->get();

        $breadcrumbs = array();

        $breadcrumbs[] = array(
            'text' => __('translations.home'),
            'href' => route('home')
        );

        $breadcrumbs[] = array(
            'text' => __('translations.account'),
            'href' => route('account')
        );

        $breadcrumbs[] = array(
            'text' => __('translations.portfolios'),
            'href' => route('portfolios')
        );

        $breadcrumbs[] = array(
            'text' => __('translations.creating'). ' ' .__('translations.portfolio'),
            'href' => route('portfolio.create')
        );

        $seo = (object)[
            'name' => __('translations.creating'). ' ' .__('translations.portfolio'),
            'meta_title' => __('translations.creating'). ' ' .__('translations.portfolio'),
            'meta_description' => 'Dentist2Lab service portfolio creating page',
        ];

        $requests = $request->getQueryString();

        return view('Frontend.pages.account.portfolio.edit', compact(
            'data',
            'branches',
            'requests',
            'seo',
            'breadcrumbs'
        ));
    }

    public function edit(Request $request, $id) {
        if (!Content::checkBranch()) return redirect()->route('account');

        $data = Portfolio::where('id', (int)$id)->firstOrFail();

        $branch = Branch::where('id', $data->branch_id)->first();

        if ($request->method() == 'POST') {
            $messages = array(
                'name.required' => 'Field is required!',                
                'name.max' => 'Field contains more then 255 symbols!'
            );

            $rules = array(
                'name' => 'required|max:255'
            );

            $request->validate($rules, $messages);

            $data->name = $request->input('name');
            $data->save(); 

            Portfolio_image::where('portfolio_id', $id)->delete();

            if ($request->input('images')) {
            	foreach($request->input('images') as $key => $images) {
            		if ($images && count($images)) {
            			foreach($images as $image) {
            				$portfolio_image = new Portfolio_image;

            				$portfolio_image->portfolio_id = $id;
            				$portfolio_image->industry_service_id = $key;
            				$portfolio_image->image = $image;
            				$portfolio_image->save();
            			}
            		}
            	}
            }
           
            return redirect()
                ->route('portfolios')
                ->with([
                    'message'    => __('translations.success'),
                    'alert-type' => 'success',
                ]); 
        }

        $services = Content::getServicesByIndustry($branch->industry_id);

        $items = array();

        foreach ($services as $service) {
            $images = Portfolio_image::where('portfolio_id', $id)
                ->where('industry_service_id', $service['id'])
                ->get();

            $items[] = array(
                'id' => $service['id'],
                'name' => $service['name'],
                'images' => $images
            ); 
        }

        $breadcrumbs = array();

        $breadcrumbs[] = array(
            'text' => __('translations.home'),
            'href' => route('home')
        );

        $breadcrumbs[] = array(
            'text' => __('translations.account'),
            'href' => route('account')
        );

        $breadcrumbs[] = array(
            'text' => __('translations.portfolios'),
            'href' => route('portfolios')
        );

        $breadcrumbs[] = array(
            'text' => __('translations.editing'). ' ' .__('translations.portfolio'),
            'href' => route('portfolio.edit', $id)
        );

        $seo = (object)[
            'name' => __('translations.editing'). ' ' .__('translations.portfolio'),
            'meta_title' => __('translations.editing'). ' ' .__('translations.portfolio'),
            'meta_description' => 'Dentist2Lab service portfolio editing page',
        ];

        $requests = $request->getQueryString();

        return view('Frontend.pages.account.portfolio.edit', compact(
            'data',
            'branch',
            'requests',
            'items',           
            'seo',
            'breadcrumbs'
        ));
    }

    public function delete(Request $request, $id) {
        Portfolio::where('id', (int)$id)->delete();
        Portfolio_image::where('portfolio_id', (int)$id)->delete();       
        
        return redirect()
            ->route('portfolios')
            ->with([
                'message'    => __('translations.success'),
                'alert-type' => 'success',
            ]);
    }
}