<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\Content;
use App\Models\Branch;
use App\Models\Branch_order_custom;
use App\Models\Branch_order_field;
use Auth;

class BranchPrescriptionController extends Controller {
    public function index() {
        $branches = Branch::where('owner_id', Auth::user()->id)
            ->where('status', '!=', 4)
            ->get();

        $data = Branch_order_custom::with([
                'branch' => function($q) {
                    $q->where('status', '!=', 4);
                }
            ])
            ->where('owner_id', Auth::user()->id)
            ->get();

        $breadcrumbs = array();

        $breadcrumbs[] = array(
            'text' => __('translations.home'),
            'href' => route('home')
        );

        $breadcrumbs[] = array(
            'text' => __('translations.account'),
            'href' => route('account')
        );

        $breadcrumbs[] = array(
            'text' => __('translations.branch_prescription'),
            'href' => route('prices')
        );

        $seo = (object)[
            'name' => __('translations.branch_prescription'),
            'meta_title' => __('translations.branch_prescription'),
            'meta_description' => 'Dentist2Lab custom order settings page',
        ];

        return view('Frontend.pages.account.branch_prescription.index', compact(
            'data',
            'branches',
            'seo',
            'breadcrumbs'
        ));
    }

    public function create(Request $request) {
        if (!Content::checkBranch()) return redirect()->route('account');

        $data = array();        

        if ($request->method() == 'POST') {
            $data = new Branch_order_custom;

            $branch = Branch::where('id', $request->input('branch_id'))->first();

            $data->owner_id = Auth::user()->id;
            $data->branch_id = $request->input('branch_id');
            $data->name = 'Custom order settings for '.$branch->name;             
            $data->sort_order = 0;
            $data->status = 1;            
            $data->save(); 
           
            return redirect()->route('branch_prescription.edit', $data->id); 
        }

        $branches = Branch::where('owner_id', Auth::user()->id)
            ->where('status', '!=', 4)
            ->whereNotIn('id', function($query){
                $query->select('branch_id')
                    ->from(with(new Branch_order_custom)->getTable())
                    ->where('owner_id', Auth::user()->id);
            })
            ->get();

        $breadcrumbs = array();

        $breadcrumbs[] = array(
            'text' => __('translations.home'),
            'href' => route('home')
        );

        $breadcrumbs[] = array(
            'text' => __('translations.account'),
            'href' => route('account')
        );

        $breadcrumbs[] = array(
            'text' => __('translations.branch_prescription'),
            'href' => route('branch_prescription')
        );

        $breadcrumbs[] = array(
            'text' => __('translations.creating'). ' ' .__('translations.branch_prescription'),
            'href' => route('branch_prescription.create')
        );

        $seo = (object)[
            'name' => __('translations.creating'). ' ' .__('translations.branch_prescription'),
            'meta_title' => __('translations.creating'). ' ' .__('translations.branch_prescription'),
            'meta_description' => 'Dentist2Lab service custom order settings creating page',
        ];

        return view('Frontend.pages.account.branch_prescription.edit', compact(
            'data',
            'branches',
            'seo',
            'breadcrumbs'
        ));
    }

    public function edit(Request $request, $id) {
        if (!Content::checkBranch()) return redirect()->route('account');

        $data = Branch_order_custom::where('id', (int)$id)->firstOrFail();

        $branch = Branch::where('id', $data->branch_id)->first();

        if ($request->method() == 'POST') {
            $messages = array(
                'name.required' => 'Field is required!',                
                'name.max' => 'Field contains more then 255 symbols!'
            );

            $rules = array(
                'name' => 'required|max:255'
            );

            $request->validate($rules, $messages);

            $data->name = $request->input('name');
            $data->status = $request->input('status');
            $data->save();             
           
            return redirect()
                ->route('branch_prescription')
                ->with([
                    'message'    => __('translations.success'),
                    'alert-type' => 'success',
                ]); 
        }        

        $breadcrumbs = array();

        $breadcrumbs[] = array(
            'text' => __('translations.home'),
            'href' => route('home')
        );

        $breadcrumbs[] = array(
            'text' => __('translations.account'),
            'href' => route('account')
        );

        $breadcrumbs[] = array(
            'text' => __('translations.branch_prescription'),
            'href' => route('branch_prescription')
        );

        $breadcrumbs[] = array(
            'text' => __('translations.editing'). ' ' .__('translations.branch_prescription'),
            'href' => route('branch_prescription.edit', $id)
        );

        $seo = (object)[
            'name' => __('translations.editing'). ' ' .__('translations.branch_prescription'),
            'meta_title' => __('translations.editing'). ' ' .__('translations.branch_prescription'),
            'meta_description' => 'Dentist2Lab service custom order settings editing page',
        ];

        return view('Frontend.pages.account.branch_prescription.edit', compact(
            'data',
            'branch',            
            'seo',
            'breadcrumbs'
        ));
    }

    public function delete(Request $request, $id) {
        Branch_order_custom::where('id', (int)$id)->delete();
        Branch_order_field::where('branch_order_custom_id', (int)$id)->delete();
       
        return redirect()
            ->route('branch_prescription')
            ->with([
                'message'    => __('translations.success'),
                'alert-type' => 'success',
            ]);
    }
}