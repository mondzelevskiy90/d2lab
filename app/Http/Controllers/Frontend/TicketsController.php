<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Ticket;
use Auth;

class TicketsController extends Controller {
    public function index(Request $request) {
        $data = Ticket::with('user')
        	->where('user_id', Auth::user()->id)
        	->orderBy('id', 'DESC')
        	->get();

        if ($request->method() == 'POST') {
            $ticket = new Ticket;

            $ticket->user_id = Auth::user()->id;
            $ticket->question = $request->input('question');
            $ticket->save();

            return redirect()
                ->route('tickets')
                ->with([
                    'message'    => __('translations.success'),
                    'alert-type' => 'success',
                ]); 
        }

        foreach($data as $item) {
        	if ($item->answer && $item->user_viewed == 0) {
        		$item->user_viewed = 1;
        		$item->save();
        	}
        }

    	$breadcrumbs = array();

    	$breadcrumbs[] = array(
			'text' => __('translations.home'),
			'href' => route('home')
		);

        $breadcrumbs[] = array(
            'text' => __('translations.account'),
            'href' => route('account')
        );

		$breadcrumbs[] = array(
			'text' => __('translations.tickets'),
			'href' => route('tickets')
		);

        $seo = (object)[
            'name' => __('translations.tickets'),
            'meta_title' => __('translations.tickets'),
            'meta_description' => 'Dentist2Lab service tickets page',
        ];

    	return view('Frontend.pages.account.tickets', compact(
    		'data',
    		'seo',
    		'breadcrumbs'
    	));
    }
}