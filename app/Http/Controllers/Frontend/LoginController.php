<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Auth;
use Mail;

class LoginController extends Controller {

    public function index(Request $request) {
    	if (Auth::check()) redirect(route('account'));

        if ($request->method() == 'POST') {
            $messages = array(                
                'email.required' => 'Field is required!',
                'email.email' => 'Field content is not an e-mail', 
                'password.required' => 'Field is required!'               
            );

            $rules = array(                
                'email' => 'required|email',
                'password' => 'required'
            );           

            $request->validate($rules, $messages);

            $email = $request->input('email');
            $password = $request->input('password');
            $status = false;
            $role_id = false;

            $user = User::where('email', $email)
                ->whereIn('status', [config('statuses.new'),config('statuses.approved')])
                ->whereIn('role_id', config('roles'))
                ->first(); 

            if (!$user && $request->input('social') == config('statuses.new')) {
                return redirect()->route('registration', ['social' => $request->input('social_type')]);
            }

            if ($user) {
                $role_id = $user->role_id;
                $status = $user->status;
            }

            $remember_me = $request->has('remember_me') ? true : false;

            if (($status && $role_id && Auth::attempt(['email' => $email, 'password' => $password, 'status' => $status, 'role_id' => $role_id], $remember_me) || Auth::viaRemember())) {
                $user->ip = request()->ip();
                $user->save();

                if ($role_id == config('roles.guest')) {
                    return redirect()->to('catalog/dentistry/labs');
                } else {
                    return redirect()->route('account');
                }
               
            } else {
                return redirect()
                    ->route('login')
                    ->with([
                        'message'    => __('translations.login_error'),
                        'alert-type' => 'error',
                    ]);
            }
        }

        $email = false;

        if ($request->method() == 'GET' && $request->input('param')) {
            $email = \Illuminate\Support\Facades\Crypt::decrypt($request->input('param'));
            
            $user = User::where('email', $email)
                ->where('status', config('statuses.email'))
                ->whereIn('role_id', config('roles'))
                ->first();

            if ($user) {
                $user->status = config('statuses.new');
                $user->recovery_code = '';
                $user->save();            
            }
        } 

		$breadcrumbs = array();

        $breadcrumbs[] = array(
            'text' => __('translations.home'),
            'href' => route('home')
        );

        $breadcrumbs[] = array(
            'text' => __('translations.login'),
            'href' => route('login')
        );

        $seo = (object)[
            'name' => __('translations.login'),
            'meta_title' => __('translations.login'),
            'meta_description' => 'Dentist2Lab service login page',
        ];

        return view('Frontend.pages.login', compact(            
            'breadcrumbs',
            'seo',
            'email'
        ));
    }

    public function logout() {
        Auth::logout();

        return redirect(route('home'));
    }

    public function forgot(Request $request) {
    	if (Auth::check()) redirect(route('account'));

        if ($request->method() == 'POST') {
            $messages = array(                
                'email.required' => 'Field is required!',
                'email.email' => 'Field content is not an e-mail'      
            );

            $rules = array(                
                'email' => 'required|email'
            );           

            $request->validate($rules, $messages);

            $email = $request->input('email');           

            $user = User::where('email', $email)
                ->where('status', '<', config('statuses.blocked'))
                ->whereIn('role_id', config('roles'))
                ->first(); 

            if ($user) {
                $code = str_shuffle('eRg1CvbTt345BsWN');

                $user->recovery_code = $code;
                $user->save();

                $content = array(
                    'url' => route('forgot', ['param' => \Illuminate\Support\Facades\Crypt::encrypt($email), 'code' => $code])
                );

                Mail::send('Frontend.emails.forgot', $content, function ($message) use ($request, $email) {
                    $message->to($email)->subject('Password recovery request');
                });

                return redirect()
                    ->route('login')
                    ->with([
                        'message'    => __('translations.forgot_check_email'),
                        'alert-type' => 'error',
                    ]);
            } else {
                return redirect()
                    ->route('forgot')
                    ->with([
                        'message'    => __('translations.forgot_no_email'),
                        'alert-type' => 'error',
                    ]);
            }
        }

        if ($request->method() == 'GET' && $request->input('param') && $request->input('code')) {
            $email = \Illuminate\Support\Facades\Crypt::decrypt($request->input('param'));
            
            $user = User::where('email', $email)
                ->where('recovery_code', $request->input('code'))
                ->where('status', '<', config('statuses.blocked'))
                ->whereIn('role_id', config('roles'))
                ->first();

            if ($user) {
                $password = str_shuffle('eRg1CvbTt345BsWN');

                $user->password = \Illuminate\Support\Facades\Hash::make($password);
                $user->recovery_code = '';
                $user->save();

                $content = array(
                    'password' => $password
                );

                Mail::send('Frontend.emails.new_password', $content, function ($message) use ($request, $email) {
                    $message->to($email)->subject('New password');
                });

                return redirect()
                    ->route('login')
                    ->with([
                        'message'    => __('translations.forgot_new_password'),
                        'alert-type' => 'success',
                    ]);
            }
        } 

		$breadcrumbs = array();

        $breadcrumbs[] = array(
            'text' => __('translations.home'),
            'href' => route('home')
        );

        $breadcrumbs[] = array(
            'text' => __('translations.forgot'),
            'href' => route('forgot')
        );

        $seo = (object)[
            'name' => __('translations.forgot'),
            'meta_title' => __('translations.forgot'),
            'meta_description' => 'Dentist2Lab service password recovery page',
        ];

        return view('Frontend.pages.forgot', compact(            
            'breadcrumbs',
            'seo'
        ));
    }
}
