<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\Notificate;
use App\Models\Branch;
use App\Models\Branch_staff;
use App\Services\Currencies;
use App\Services\OrderService;
use App\Models\Invoice;
use App\Models\Order;
use App\Models\Order_history;
use App\Models\Order_prescription;
use App\Models\Order_prescription_field;
use App\Models\Order_total;
use App\Models\Payment_type;
use App\Models\Payment_status;
use Auth;
use Mail;

class InvoicesController extends Controller {
    public function index(Request $request) {
        $can_pay = false;
        $filters = array();
        $branches = array();
        $invoice_branches = array();
        $changed_prices = array();
        $currency = Currencies::getCurrencyById(1);

        $payment_types = Payment_type::where('status', 1)
            ->orderBy('sort_order', 'ASC')
            ->get();

        $payment_statuses = Payment_status::where('status', 1)
            ->where('id', '>', 2)
            ->orderBy('sort_order', 'ASC')
            ->get();

        $invoice_branches = Invoice::where('status', 1);

        $data = Invoice::with([
                'branch',
                'to_branch',
                'order_info',
                'payment_type_info',
                'payment_status_info',
                'total'
            ])
            ->where('status', 1);



        if ($request->input('payment_type')) {
            $filters['payment_type'] = (int)$request->input('payment_type');

            $data->where('payment_type', $filters['payment_type']);
        }

        if ($request->input('payment_status')) {
            $filters['payment_status'] = (int)$request->input('payment_status');

            $data->where('payment_status', $filters['payment_status']);
        }

        if ($request->input('search')) {
            $filters['search'] = $request->input('search');

            $data->where(function($q) use ($filters) {
                $q->orWhere('name', 'LIKE', $filters['search'].'%')
                    ->orWhere('order_id', $filters['search']);
            });
        }

        if ($request->input('from_date')) {
            $filters['from_date'] = $request->input('from_date');

            $data->where('created_at', '>=', $filters['from_date'].' 00:00:00');
        }

        if ($request->input('to_date')) {
            $filters['to_date'] = $request->input('to_date');

            $data->where('created_at', '<=', $filters['to_date'].' 23:59:59');
        }

        if ($request->input('invoice_branch')) {
        	$filters['invoice_branch'] = (int)$request->input('invoice_branch');

            if (Auth::user()->type == config('types.clinic')) {
                $data->where('to_branch_id', $filters['invoice_branch']);
            }

            if (Auth::user()->type == config('types.lab')) {
                $data->where('branch_id', $filters['invoice_branch']);
            }
        }

        switch (Auth::user()->role_id) {
            case config('roles.clinic_head'):
                $branches_arr = array();
                $branches = Branch::where('owner_id', Auth::user()->id)->get();                

                foreach ($branches as $branch) {
                    $branches_arr[] = $branch->id;
                }

                if ($request->input('branch')) {
                    $filters['branch'] = $request->input('branch');
                    $data->where('branch_id', (int)$filters['branch']);
                } else {
                    $data->whereIn('branch_id', $branches_arr);
                }

                $invoice_branches->with('to_branch')
                	->select('to_branch_id')
                	->whereIn('branch_id', $branches_arr);

                $can_pay = true;
                
                break;

            case config('roles.clinic_staff'):
                $staff = Branch_staff::where('user_id', Auth::user()->id)->first();

                $invoice_branches->with('to_branch')
                	->select('to_branch_id');

                if ($staff) {                 
                    $data->where('branch_id', $staff->branch_id);
                    $invoice_branches->where('branch_id', $staff->branch_id); 

                    if ($staff->can_pay == 1) $can_pay = true;
                } else {
                    //do not show invoices if user removed from branch
                    $data->where('status', '-1'); 
                    $invoice_branches->where('payment_status', '-1');
                }
                
                break;

            case config('roles.doctor'):
                $staff = Branch_staff::where('user_id', Auth::user()->id)->first();

                $invoice_branches->with('to_branch')
                	->select('to_branch_id');

                if ($staff) {                 
                    $data->where('branch_id', $staff->branch_id)
                        ->where('order_user_id', Auth::user()->id);

                    $invoice_branches->where('branch_id', $staff->branch_id)
                        ->where('order_user_id', Auth::user()->id);

                    if ($staff->can_pay == 1) $can_pay = true;
                } else {
                    //do not show invoice if user removed from branch
                    $data->where('status', '-1');
                    $invoice_branches->where('payment_status', '-1');
                }
                
                break;

            case config('roles.lab_head'):
                $branches_arr = array();
                $branches = Branch::where('owner_id', Auth::user()->id)->get();                

                foreach ($branches as $branch) {
                    $branches_arr[] = $branch->id;
                }

                if ($request->input('branch')) {
                    $filters['branch'] = $request->input('branch');
                    $data->where('to_branch_id', (int)$filters['branch']);
                } else {
                    $data->whereIn('to_branch_id', $branches_arr);
                }

                $invoice_branches->with('branch')
                	->select('branch_id')
                	->whereIn('to_branch_id', $branches_arr);

                $can_pay = true;
                
                break;

            case config('roles.lab_staff'):
                $staff = Branch_staff::where('user_id', Auth::user()->id)->first();

                $invoice_branches->with('branch')
                	->select('branch_id');

                if ($staff) {                 
                    $data->where('to_branch_id', $staff->branch_id); 

                    $invoice_branches->where('to_branch_id', $staff->branch_id);

                    if ($staff->can_pay == 1) $can_pay = true;
                } else {
                    //do not show invoices if user removed from branch
                    $data->where('status', '-1'); 
                    $invoice_branches->where('payment_status', '-1');
                }       

                break;

            default:
                // do not show invoices if any other roles views this page
                $data->where('status', '-1'); 
                $invoice_branches->where('payment_status', '-1');

                break;
        }
        
        $invoice_branches = $invoice_branches->distinct()->get();
        $data = $data->orderBy('payment_status', 'ASC')->orderBy('id', 'DESC')->paginate(50);

        $changed_prices_query = Order_prescription::select('order_id')
            ->where('price_changed_to', '!=', '0.00')
            ->where('paid', 0)
            ->distinct()            
            ->groupBy('order_id')
            ->get();

        foreach ($changed_prices_query as $item) {
            $changed_prices[] = $item['order_id'];
        }

    	$breadcrumbs = array();

    	$breadcrumbs[] = array(
			'text' => __('translations.home'),
			'href' => route('home')
		);

        $breadcrumbs[] = array(
            'text' => __('translations.account'),
            'href' => route('account')
        );

		$breadcrumbs[] = array(
			'text' => __('translations.invoices'),
			'href' => route('invoices')
		);

        $seo = (object)[
            'name' => __('translations.invoices'),
            'meta_title' => __('translations.invoices'),
            'meta_description' => 'Dentist2Lab service invoices page',
        ];

        $requests = $request->getQueryString();

    	return view('Frontend.pages.account.invoices.index', compact(
    		'data', 
            'can_pay',
            'currency',
            'filters',
            'branches',
            'invoice_branches',
            'payment_statuses',
            'payment_types',
            'changed_prices',
    		'seo',
    		'breadcrumbs',
            'requests'
    	));
    }

    public function edit(Request $request, $id) {
        $data = Invoice::with([
                'branch',
                'to_branch',
                'order_info',
                'payment_type_info',
                'payment_status_info',
                'total'
            ])
            ->where('id', (int)$id)
            ->firstOrFail();

        $currency = Currencies::getCurrencyById(1);
        $prescriptions = Order_prescription::with('service')
        	->where('order_id', $data->order_id)
        	->get();

        $can_pay = false;

        switch (Auth::user()->role_id) {
            case config('roles.clinic_head'):
                $can_pay = true;
                
                break;

            case config('roles.clinic_staff'):
                $staff = Branch_staff::where('user_id', Auth::user()->id)->first();

                if ($staff && $staff->can_pay == 1) $can_pay = true;
                
                break;

            case config('roles.doctor'):
                $staff = Branch_staff::where('user_id', Auth::user()->id)->first();

                if ($staff && $staff->can_pay == 1) $can_pay = true;
                
                break;

            case config('roles.lab_head'):              
                $can_pay = true;
                
                break;

            case config('roles.lab_staff'):
                $staff = Branch_staff::where('user_id', Auth::user()->id)->first();

                if ($staff && $staff->can_pay == 1) $can_pay = true;

                break;

            default:
                $can_pay = false;

                break;
        }

        $breadcrumbs = array();

        $breadcrumbs[] = array(
            'text' => __('translations.home'),
            'href' => route('home')
        );

        $breadcrumbs[] = array(
            'text' => __('translations.account'),
            'href' => route('account')
        );

        $breadcrumbs[] = array(
            'text' => __('translations.invoices'),
            'href' => route('invoices')
        );

        $breadcrumbs[] = array(
            'text' => __('translations.editing'). ' ' .__('translations.invoice'),
            'href' => route('invoices')
        );

        $seo = (object)[
            'name' => __('translations.invoice'),
            'meta_title' => __('translations.editing'). ' ' .__('translations.invoice'),
            'meta_description' => 'Dentist2Lab service invoice page',
        ];

        $requests = $request->getQueryString();

        return view('Frontend.pages.account.invoices.edit', compact(
            'data', 
            'seo', 
            'can_pay',           
            'currency',            
            'prescriptions',   
            'breadcrumbs',
            'requests'
        ));
    }
    
    public function update(Request $request, $id) {
        $data = Invoice::with('order_info')
            ->where('id', (int)$id)
            ->firstOrFail();  

        $requests = $request->getQueryString();

        $currency = Currencies::getCurrencyById(1);  

        if (Auth::user()->type == config('types.lab') && $data->payment_status != 4 && $request->input('prescriptions')) {
            foreach ($request->input('prescriptions') as $key => $prescription) {
                $order_prescription = Order_prescription::with('service', 'prescription_fields', 'order_info')
                        ->where('id', (int)$key)
                        ->first();

                if ((float)$prescription['price'] != (float)$prescription['price_changed_to']) {
                    if ($order_prescription) {                       
                        if ($prescription['price_changed_to'] == '0.00') {
                            $order_prescription->price = '0.00';
                        } else {
                            $order_prescription->price_changed_to = $prescription['price_changed_to'];
                        }

                        $history = new Order_history;

                        $history_comment = $order_prescription->service->name.' prescription price changed to '.$currency->symbol.$prescription['price_changed_to'].'!';

                        $history->order_id = $order_prescription->order_id;
                        $history->user = Auth::user()->name.' '.Auth::user()->surname;
                        $history->comment = $history_comment;

                        $history->save();
                    }
                } else {
                    $order_prescription->price_changed_to = '0.00';
                }

                $order_prescription->save();

                if (isset($prescription['prescription_action']) && $prescription['prescription_action'] != 0) {
                    $count_prescriptions = OrderService::createOrderRemake($id, $data->order_id, $order_prescription->id, $prescription['prescription_action']);

                    if ($count_prescriptions && $prescription['prescription_action'] != 1) {
                        return redirect()
                        ->route('invoices.edit', [$id,$requests])
                        ->with([
                            'message'    => __('translations.success'),
                            'alert-type' => 'success',
                        ]); 
                    } else {
                        return redirect()
                            ->route('orders')
                            ->with([
                                'message'    => __('translations.success'),
                                'alert-type' => 'success',
                            ]);
                    }

                                      
                }
            }
        }

        if (Auth::user()->type == config('types.lab') && $data->payment_status != 4 && $request->input('order_total')) {
            $order_total = Order_total::where('order_id', (int)$data->order_id)->first();

            if ($order_total) {
                if ((float)$order_total->price != (float)$request->input('order_total')) {
                    if ($request->input('order_total') == '0.00') {
                        $order_total->price = '0.00';
                    } else {
                        $order_total->price_changed_to = $request->input('order_total');
                    }

                    $email = $data->order_info->user_email;

                    $content = [
                        'order_id' => $data->order_id,
                        'order_url' => route('orders.edit', $data->order_id),
                        'url' => route('invoices.edit', $id)              
                    ];

                    Mail::send('Frontend.emails.invoice_price_changed', $content, function ($message) use ($email) {
                        $message->to($email)->subject('Invoice has been changed by lab!');
                    }); 

                    $order_url = route('orders.edit', $data->order_id);

                    $notificate_message = sprintf(__('translations.order_change_price_notificate'), $order_url, $data->order_id);

                    Notificate::createNotification($data->branch_id, $data->order_id, $notificate_message);
                } else {
                    $order_total->price_changed_to = '0.00';
                }

                $order_total->save();
            }
        } 
        
        return redirect()
            ->route('invoices', $requests)
            ->with([
                'message'    => __('translations.success'),
                'alert-type' => 'success',
            ]);
    }    
}