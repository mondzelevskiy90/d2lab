<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\Content;
use App\Models\Article;
use App\Models\Branch;
use App\Models\User_type;
use App\Models\Industrty;
use App\Models\Industry_service;
use App\Models\Price;
use App\Models\Price_service;
use App\Models\Locality;

class SitemapController extends Controller {
    public function index(Request $request) {
    	$data = array();

    	$breadcrumbs = array();

    	$breadcrumbs[] = array(
			'text' => __('translations.home'),
			'href' => route('home')
		);

		$breadcrumbs[] = array(
			'text' => __('translations.sitemap'),
			'href' => route('sitemap')
		);

        $items = $this->getSitemapItems($request);

        $paginator = false;
        $total = count($items);
        $perPage = 50;

        if ($request->page) {
            $currentPage = $request->page;
        } else {
            $currentPage = 1;
        }

        $requests = $request->getQueryString();
        $requests = explode('&', $requests);

        foreach($requests as $key => $value) {
            if (strpos($value, 'page='.$currentPage) !== false) unset($requests[$key]); 
        }

        $requests = implode('', $requests);

        $paginator = new \Illuminate\Pagination\LengthAwarePaginator($items, $total, $perPage, $currentPage, ['path' => $request->url().'?'.$requests] );

        $start = ($currentPage - 1) * $perPage;
        $end = $start + $perPage;

        foreach ($items as $key => $value) {
            if ($key >= $start && $key < $end) {
                $data[] = $value;
            }
        }

    	return view('Frontend.pages.sitemap', compact(
    		'data',
    		'breadcrumbs',
            'paginator'
    	));
    }

    public function sitemapxml(Request $request) {
        $lastmod = date('Y-m-d');
        $limit = 100;

        $items = $this->getSitemapItems($request);

        if (!file_exists(public_path().'/sitemaps')) {
            mkdir(public_path().'/sitemaps', 0775);
        }

        $files = glob(public_path().'/sitemaps/*');

        foreach ($files as $file) { 
            if(is_file($file)) unlink($file); 
        }

        if (!file_exists(public_path().'/sitemap.xml')) {
            $file = fopen(public_path().'/sitemap.xml', 'w');
            fclose($file);
        }

        $total = intval(ceil(count($items) / $limit));

        $main_sitemap = '';

        $main_sitemap .= '<?xml version="1.0" encoding="UTF-8"?>';
        $main_sitemap .= '<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';

        for($i = 1; $i <= $total; $i++) {
            $main_sitemap .= '<sitemap>';
            $main_sitemap .= '<loc>'.url('/').'/sitemaps/sitemap-'.$i.'.xml</loc>';
            $main_sitemap .= '<lastmod>'.$lastmod.'</lastmod>';
            $main_sitemap .= '</sitemap>';

            if (!file_exists(public_path().'/sitemaps/sitemap-'.$i.'.xml')) {
                $file = fopen(public_path().'/sitemaps/sitemap-'.$i.'.xml', 'w');
                fclose($file);
            }

            $sitemap = '';

            $sitemap .= '<?xml version="1.0" encoding="UTF-8"?>';
            $sitemap .= '<urlset xmlns="http://www.google.com/schemas/sitemap/0.9">';

            foreach ($items as $key => $value) {
                if ($key >= ($i * $limit - $limit) && $key <= ($i * $limit - 1)) {
                    $sitemap .= '<url>';      
                    $sitemap .= '<loc>'.str_replace('&', '&amp;', $value['href']).'</loc>';
                    $sitemap .= '<lastmod>'.$lastmod.'</lastmod>';
                    $sitemap .= '<priority>'.$value['priority'].'</priority>';
                    $sitemap .= '<changefreq>'.$value['changefreq'].'</changefreq>';
                    $sitemap .= '</url>';
                }
            }

            $sitemap .= '</urlset>';

            file_put_contents(public_path().'/sitemaps/sitemap-'.$i.'.xml', $sitemap);
        }

        $main_sitemap .= '</sitemapindex>';

        file_put_contents(public_path().'/sitemap.xml', $main_sitemap);
    }

    protected function getSitemapItems($request) {
        $data = array();

        $data[] = array(
            'name' => 'Home',
            'href' => route('home'),
            'priority' => 1,
            'changefreq' => 'weekly'
        );

        $articles = Article::with('content')
            ->where('status', 1)
            ->get();

        foreach ($articles as $article) {
            $seo = Content::getSeo($article);

            $data[] = array(
                'name' => $seo->name,
                'href' => route('home').'/article/'.$article->slug,
                'priority' => 0.7,
                'changefreq' => 'monthly'
            );
        }

        //<--- For dentists
        /*$data[] = array(
            'name' => 'Catalog',
            'href' => route('catalog'),
            'priority' => 0.9,
            'changefreq' => 'weekly'
        );*/
          
        $types = User_type::where('catalog', 1)->get();
        $industries = Content::getIndustriesByParent(0);

        foreach ($industries as $industry) {           
            $industry->childrens = Content::getIndustriesByParent($industry->id);

            foreach ($types as $type) {
                if ($type->id != config('types.lab')) continue; //<--- For dentists

                $locations = $this->getLocationByIndustryAndType($industry->id, $type->id);
                $services = $this->getServicesByIndustryAndType($industry->id, $type->id);

                if ($industry->childrens) {
                    foreach ($industry->childrens as $children) {
                        $localities = $this->getLocationByIndustryAndType($children->id, $type->id);
                        $children_services = $this->getServicesByIndustryAndType($children->id, $type->id);

                        $data[] = array(
                            'name' => $children->name.' '.$type->catalog_name,
                            'href' => route('catalog.industry.type', [$children->slug, $type->slug]),
                            'priority' => 0.9,
                            'changefreq' => 'weekly'
                        ); 

                        foreach ($children_services as $children_service) {
                            $data[] = array(
                                'name' => $children->name.' '.$type->catalog_name.' '.$children_service->name,
                                'href' => route('catalog.industry.type', [$children->slug, $type->slug, 'service_id' => $children_service->id]),
                                'priority' => 0.9,
                                'changefreq' => 'weekly'
                            ); 
                        }

                        foreach ($localities as $locality) {
                            $data[] = array(
                                'name' => $children->name.' '.$type->catalog_name.' '.$locality->name. ' ('.$locality->country->code.')',
                                'href' => route('catalog.industry.type', [$children->slug, $type->slug, 'locality_id' => $locality->id]),
                                'priority' => 0.9,
                                'changefreq' => 'weekly'
                            ); 

                            foreach ($children_services as $children_service) {
                                $data[] = array(
                                    'name' => $children->name.' '.$type->catalog_name.' '.$locality->name. ' ('.$locality->country->code.')' .' '.$children_service->name,
                                    'href' => route('catalog.industry.type', [$children->slug, $type->slug, 'locality_id' => $locality->id, 'service_id' => $children_service->id]),
                                    'priority' => 0.9,
                                    'changefreq' => 'weekly'
                                );
                            }
                        }
                    }
                } else {
                    $data[] = array(
                        'name' => $industry->name.' '.$type->catalog_name,
                        'href' => route('catalog.industry.type', [$industry->slug, $type->slug]),
                        'priority' => 0.9,
                        'changefreq' => 'weekly'
                    );

                    foreach ($services as $service) {
                            $data[] = array(
                                'name' => $industry->name.' '.$type->catalog_name.' '.$service->name,
                                'href' => route('catalog.industry.type', [$industry->slug, $type->slug, 'service_id' => $service->id]),
                                'priority' => 0.9,
                                'changefreq' => 'weekly'
                            ); 
                        }

                    foreach ($locations as $locality) {
                        $data[] = array(
                            'name' => $industry->name.' '.$type->catalog_name.' '.$industry->name. ' ('.$industry->country->code.')',
                            'href' => route('catalog.industry.type', [$industry->slug, $type->slug, 'locality_id' => $locality->id]),
                            'priority' => 0.9,
                            'changefreq' => 'weekly'
                        ); 

                        foreach ($services as $service) {
                            $data[] = array(
                                'name' => $industry->name.' '.$type->catalog_name.' '.$locality->name. ' ('.$locality->country->code.')' .' '.$service->name,
                                'href' => route('catalog.industry.type', [$industry->slug, $type->slug, 'locality_id' => $locality->id, 'service_id' => $service->id]),
                                'priority' => 0.9,
                                'changefreq' => 'weekly'
                            );
                        }
                    }
                }
            }
        }

        $branches = Branch::with('type_info')
            ->where('status', 1)
           // ->where('verified', 1)
            ->get();

        foreach ($branches as $branch) {
            if ($branch->type != config('types.lab')) continue; //<--- For dentists

            $data[] = array(
                'name' => $branch->name,
                'href' => route('branch', [$branch->type_info->slug, $branch->id]),
                'priority' => 0.9,
                'changefreq' => 'weekly'
            );
        }

        return $data;
    }

    protected function getLocationByIndustryAndType($industry_id = 0, $type_id = 0) {
        return Locality::with('country')
            ->whereIn('id', function ($q) use ($industry_id, $type_id){
                $q->select('locality')
                ->from(with(new Branch)->getTable())
                ->where('industry_id', $industry_id)
                ->where('type', $type_id)
                ->distinct();
            })
            ->get();
    }

    protected function getServicesByIndustryAndType($industry_id = 0, $type_id = 0) {
        return Industry_service::where('status', 1)
            ->whereIn('id', function ($q) use ($industry_id, $type_id) {
                $q->select('industry_service_id')
                    ->from(with(new Price_service)->getTable())
                    ->whereIn('price_id', function($q2) use ($industry_id, $type_id) {
                        $q2->select('id')
                            ->from(with(new Price)->getTable())
                            ->whereIn('branch_id', function($q3) use ($industry_id, $type_id) {
                                $q3->select('id')
                                    ->from(with(new Branch)->getTable())
                                    ->where('industry_id', $industry_id)
                                    ->where('type', $type_id)
                                    ->distinct();
                            });
                    })
                    ->distinct();
            })
            ->get();
    }
}
