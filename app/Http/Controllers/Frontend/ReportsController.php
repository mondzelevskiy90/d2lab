<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\Currencies;
use App\Services\Userdata;
use App\Models\Branch;
use App\Models\Branch_staff;
use App\Models\Invoice;
use App\Models\Order;
use App\Models\Order_prescription;
use App\Models\Report;
use Auth;
use Excel;

class ReportsController extends Controller {
    public function index() {
        $data = Report::where('status', 1)
            ->orderBy('sort_order', 'ASC')
            ->get();

    	$breadcrumbs = array();

    	$breadcrumbs[] = array(
			'text' => __('translations.home'),
			'href' => route('home')
		);

        $breadcrumbs[] = array(
            'text' => __('translations.account'),
            'href' => route('account')
        );

		$breadcrumbs[] = array(
			'text' => __('translations.reports'),
			'href' => route('reports')
		);

        $seo = (object)[
            'name' => __('translations.reports'),
            'meta_title' => __('translations.reports'),
            'meta_description' => 'Dentist2Lab service reports page',
        ];

    	return view('Frontend.pages.account.reports.index', compact(
    		'data', 
    		'seo',
    		'breadcrumbs'
    	));
    }

    public function report(Request $request, $slug) {
        $data = Report::where('slug', $slug)
            ->where('status', 1)
            ->firstOrFail();

        $branches = array();
        $order_branches = array();
        $filters = array();
        $currency = Currencies::getCurrencyById(1);
        $can_pay = false;

        $order_branches = Order::whereNotIn('status', [1,2]);

        if ($request->input('branch')) {
            $filters['branch'] = (int)$request->input('branch');
        } else {
            $filters['branch'] = false;
        }

        if ($request->input('order_branche')) {
            $filters['order_branche'] = (int)$request->input('order_branche');
        } else {
            $filters['order_branche'] = false;
        }

        if ($request->input('date')) {
            $filters['date'] = $request->input('date');
        } else {
            $filters['date'] = date('Y-m-').'01';
        }

        switch (Auth::user()->role_id) {
            case config('roles.clinic_head'):
                $branches_arr = array();
                $branches = Branch::where('owner_id', Auth::user()->id)
                    ->where('status', '!=', 4)
                    ->get()
                    ->toArray();

                foreach ($branches as $branch) {
                    $branches_arr[] = $branch['id'];
                }

                $order_branches->with('to_branch')
                    ->select('to_branch_id')
                    ->whereIn('branch_id', $branches_arr);

                $can_pay = true;

                break;

            case config('roles.clinic_staff'):
                $staff = Branch_staff::where('user_id', Auth::user()->id)->first();

                $order_branches->with('to_branch')
                    ->select('to_branch_id');

                if ($staff) { 
                    $order_branches->where('branch_id', $staff->branch_id); 

                    $branches = Branch::where('id', $staff->branch_id)->get()->toArray();

                    if ($staff->can_pay == 1) $can_pay = true;
                } else {
                    $order_branches->where('status', '-1');
                }
                
                break;

            case config('roles.lab_head'):
                $branches_arr = array();
                $branches = Branch::where('owner_id', Auth::user()->id)
                    ->where('status', '!=', 4)
                    ->get()
                    ->toArray();

                foreach ($branches as $branch) {
                    $branches_arr[] = $branch['id'];
                }

                $order_branches->with('branch')
                    ->select('branch_id')
                    ->whereIn('to_branch_id', $branches_arr);

                break;

            case config('roles.lab_staff'):
                $staff = Branch_staff::where('user_id', Auth::user()->id)->first();

                $order_branches->with('branch')
                    ->select('branch_id');

                if ($staff) { 
                    $order_branches->where('to_branch_id', $staff->branch_id);

                    $branches = Branch::where('id', $staff->branch_id)->get()->toArray();
                } else {
                    $order_branches->where('status', '-1');
                }       

                break;

            default:
                $order_branches->where('status', '-1'); 

                break;
        }
  
        $order_branches = $order_branches->distinct()->get()->toArray();

        $content = [];

        foreach ($branches as $branch) {
        	if ($filters['branch'] && $filters['branch'] != $branch['id']) continue;

        	$order_branches_data = array();

            foreach ($order_branches as $order_branche){
            	$invoice_data = false;  
            	$order_branch_id = 0;

            	if (Auth::user()->type == config('types.clinic')) $order_branch_id = $order_branche['to_branch_id'];
            	if (Auth::user()->type == config('types.lab')) $order_branch_id = $order_branche['branch_id'];
            	if ($filters['order_branche'] && $filters['order_branche'] != $order_branch_id) continue;

            	$order_branches_data[] = array(
        			'branch' => Branch::where('id', $order_branch_id)->first()->toArray(),
            		'report_data'=> $this->{$slug}($branch['id'], $order_branch_id, $filters)
            	);
            }

            $branch['order_branches'] = $order_branches_data;
            $content[] = $branch;
        } 

        $breadcrumbs = array();

        $breadcrumbs[] = array(
            'text' => __('translations.home'),
            'href' => route('home')
        );

        $breadcrumbs[] = array(
            'text' => __('translations.account'),
            'href' => route('account')
        );

        $breadcrumbs[] = array(
            'text' => __('translations.reports'),
            'href' => route('reports')
        );

        $breadcrumbs[] = array(
            'text' => $data->name,
            'href' => route('reports')
        );

        $seo = (object)[
            'name' => $data->name,
            'meta_title' => $data->name,
            'meta_description' => 'Dentist2Lab service '.$data->name.' page',
        ];

        $requests = $request->getQueryString();

        return view('Frontend.pages.account.reports.'.$slug, compact(
            'data', 
            'content',
            'filters',
            'branches',
            'order_branches',
            'currency',
            'can_pay',
            'seo',
            'breadcrumbs',
            'requests'
        ));
    }

    protected function statement($branch_id, $to_branch_id, $filters) {
        $data = array();

        $data['balance'] = 0.00;
        $data['amount_due'] = 0.00;
        $data['current'] = 0.00;
        $data['1_days_due'] = 0.00;
        $data['31_days_due'] = 0.00;
        $data['61_days_due'] = 0.00;
        $data['91_days_due'] = 0.00;
        $data['first_date'] = date('m-d-Y', strtotime('-1 day', time()));

        $invoices = Invoice::with([
                'order_info',
                'prescriptions'
            ])
        	->where('status', 1);

        if (Auth::user()->type == config('types.clinic')) {
            $invoices->where('branch_id', $branch_id)
            	->where('to_branch_id', $to_branch_id);
        }

        if (Auth::user()->type == config('types.lab')) {
            $invoices->where('branch_id', $to_branch_id)
            	->where('to_branch_id', $branch_id);
        }

        $from_date = false;
        $to_date = false;
        $date_month = date('Y-m');

        if ($filters['date']) {
            $from_date = $filters['date'].' 00:00:00';
            $data['first_date'] = date('m-d-Y', strtotime('-1 day', strtotime($from_date)));
            $to_date = date('Y-m-t', strtotime($from_date)).' 23:59:59';
            $date_month = date('Y-m', strtotime($from_date));
        }

        $date_1_days_due = $date_month;
        $date_31_days_due = date('Y-m', strtotime('-1 month', strtotime($date_month)));
        $date_61_days_due = date('Y-m', strtotime('-2 month', strtotime($date_month)));;
        $date_91_days_due = date('Y-m', strtotime('-3 month', strtotime($date_month)));;

        $invoices = $invoices->orderBy('created_at', 'ASC')->get();

        $data['invoices'] = [];

        foreach ($invoices as $key => $invoice) {
            if (!$from_date && $key == 0) $data['first_date'] = date('m-d-Y', strtotime('-1 day', strtotime($invoice->created_at)));

            foreach ($invoice->prescriptions as $prescription) {
                if ($prescription->paid == 0) {
                    if (date('Y-m', strtotime($invoice->created_at)) <= $date_91_days_due) {
                        $data['91_days_due'] = (float)$data['91_days_due'] + (float)$prescription->price;
                    }

                    if (date('Y-m', strtotime($invoice->created_at)) == $date_61_days_due) {
                        $data['61_days_due'] = (float)$data['61_days_due'] + (float)$prescription->price;
                    }

                    if (date('Y-m', strtotime($invoice->created_at)) == $date_31_days_due) {
                        $data['31_days_due'] = (float)$data['31_days_due'] + (float)$prescription->price;
                    }

                    if (date('Y-m', strtotime($invoice->created_at)) == $date_1_days_due) {
                        $data['1_days_due'] = (float)$data['1_days_due'] + (float)$prescription->price;
                    }

                    if ($from_date && $invoice->created_at < $from_date) {
                        $data['balance'] = (float)$data['balance'] + (float)$prescription->price;
                    }

                    if (!$to_date || ($to_date && $invoice->created_at <= $to_date)) {
                        $data['amount_due'] = (float)$data['amount_due'] + (float)$prescription->price;
                    }

                    
                }
            }

            if ($from_date && $invoice->created_at < $from_date) continue;
            if ($to_date && $invoice->created_at > $to_date) continue;

            $data['invoices'][] = $invoice;
        }

        $data['balance'] = number_format($data['balance'], 2);
        $data['amount_due'] = number_format($data['amount_due'], 2);
        $data['current'] = $data['amount_due'];
        $data['1_days_due'] = number_format($data['1_days_due'], 2);
        $data['31_days_due'] = number_format($data['31_days_due'], 2);
        $data['61_days_due'] = number_format($data['61_days_due'], 2);
        $data['91_days_due'] = number_format($data['91_days_due'], 2);

        return $data;
    }

    protected function orders_list($branch_id, $to_branch_id, $filters) {
        $data = array();

        $orders = Order::with([
                'status_info',
                'type_info',
                'payment_info',
                'prescriptions',
                'total'
            ])
            ->whereNotIn('status', [1,2]);

        if (Auth::user()->type == config('types.clinic')) {
            $orders->where('branch_id', $branch_id)
            	->where('to_branch_id', $to_branch_id);
        }

        if (Auth::user()->type == config('types.lab')) {
            $orders->where('branch_id', $to_branch_id)
            	->where('to_branch_id', $branch_id);
        }

        if ($filters['date']) {
            $from_date = $filters['date'].' 00:00:00';            
            $to_date = date('Y-m-t', strtotime($from_date)).' 23:59:59';

            $orders->whereBetween('created_at', [$from_date, $to_date]);   
        }

        $data['orders'] = $orders->orderBy('id', 'ASC')->get();

        return $data;
    }
    
    public function excel(Request $request) {
        $output = array();
        $type = false;
        $currency = Currencies::getCurrencyById(1);

        if ($request->input('type') && $request->input('branch_id') && $request->input('to_branch_id')) {
            $type = $request->input('type');

            switch ($type) {
                case 'orders_list':
                	$orders = Order::with([
                		    'branch',
                		    'to_branch',
			                'status_info',
			                'type_info',
			                'payment_info',
			                'prescriptions',
			                'total'
			            ]);
			            

			        if (Auth::user()->type == config('types.clinic')) {
			        	$orders->where('status', '!=', 1);
			        	$orders->where('branch_id', (int)$request->input('branch_id'));
			        	$orders->where('to_branch_id', (int)$request->input('to_branch_id'));
			        }

			        if (Auth::user()->type == config('types.lab')) {
			        	$orders->whereNotIn('status', [1,2]);
			            $orders->where('branch_id', (int)$request->input('to_branch_id'));
			        	$orders->where('to_branch_id', (int)$request->input('branch_id'));
			        }

			        if ($request->input('date')) {
			            $date = $request->input('date');

			            $from_date = $date.' 00:00:00';            
			            $to_date = date('Y-m-t', strtotime($from_date)).' 23:59:59';

			            $orders->whereBetween('created_at', [$from_date, $to_date]); 
			        } 

			        $orders = $orders->orderBy('id', 'ASC')->get();

			        $branch = Branch::where('id', (int)$request->input('branch_id'))->first();
			        $to_branch = Branch::where('id', (int)$request->input('to_branch_id'))->first();

			        $cnt = 0;
			        $total = 0.00;

			        foreach ($orders as $order) {
			        	if ($cnt == 0) {
			        		if (Auth::user()->type == config('types.clinic')) {
					        	$user_word = __('translations.created_by');
					        	$clinic_info = $branch->name.', '.$branch->zip_code.', '.$branch->address.', '.Userdata::getLocality($branch->locality);
					        	$lab_info = $to_branch->name.', '.$to_branch->zip_code.', '.$to_branch->address.', '.Userdata::getLocality($to_branch->locality);
					        }

					        if (Auth::user()->type == config('types.lab')) {
					            $user_word = __('translations.executed_by');
					            $clinic_info = $to_branch->name.', '.$to_branch->zip_code.', '.$to_branch->address.', '.Userdata::getLocality($to_branch->locality);
					            $lab_info = $branch->name.', '.$branch->zip_code.', '.$branch->address.', '.Userdata::getLocality($branch->locality);
					        }

			        		$output[$cnt] = array(
			        			__('translations.clinic').' '.__('translations.branch'),
			        			$clinic_info,
			        			'',
			        			'',
			        			'',
			        			'',
			        			''
			        		);

			        		$cnt++;

			        		$output[$cnt] = array(
			        			__('translations.lab'),
			        			$lab_info,
			        			'',
			        			'',
			        			'',
			        			'',
			        			''
			        		);

			        		$cnt++;

			        		$date = date('Y-m').'-01';

			        		if ($request->input('date')) $date = $request->input('date');

			        		$from_date = date('m-d-Y', strtotime($date));            
					        $to_date = date('m-t-Y', strtotime($date));

			        		$output[$cnt] = array(
			        			__('translations.date'),
			        			'from '.$from_date.' to '.$to_date,
			        			'',
			        			'',
			        			'',
			        			'',
			        			''
			        		);

			        		$cnt++;

			        		$output[$cnt] = array(
			        			'ID',
			        			__('translations.date'),
			        			$user_word,
			        			__('translations.status'),
			        			__('translations.payment_status'),
			        			__('translations.amount'),
			        			'Currency'
			        		);

			        		$cnt++;
			        	}

			        	if (Auth::user()->type == config('types.clinic')) {
							$user_name = $order->user_name;
			        	}
						
						if (Auth::user()->type == config('types.lab')) {
							if ($order->executor_id != 0) {
								$user_name = $order->executor_name;
							} else {
								$user_name = __('translations.not_assigned');
							}
						}

						if ($order->total && $order->total->price && $order->total->price != '0.00') {
							$total = (float)$total + (float)$order->total->price;
							$order_price = $order->total->price;
						} else {
							$order_price = '---';
						} 							

			        	$output[$cnt] = array(
		        			'#'.$order->id,
		        			date('m-d-Y', strtotime($order->created_at)),
		        			$user_name,
		        			$order->status_info->name,
		        			$order->payment_info->name,		        			
		        			$order_price,
		        			$currency->symbol,
		        		);

						$cnt++;
			        }

			        $total = number_format($total, 2);

			        $output[$cnt] = array(
	        			'',
	        			'',
	        			'',
	        			'',
	        			'',
	        			$total,
	        			$currency->symbol
	        		);

                    break;
                
                default:
                   
                    break;
            }
        }

        $filename = $type.'_'.date('d.m.Y.H-i').'.xlsx';

        Excel::store(new InvoicesExport($output), $filename, 'xls');

        return response()->json(url('/').'/storage/xls_reports/'.$filename);
    }

}

use Maatwebsite\Excel\Concerns\FromArray;

class InvoicesExport implements FromArray
{
	protected $data;

    public function __construct(array $data)
    {
        $this->data = $data;
    }

    public function array(): array
    {
        return $this->data;
    }
}