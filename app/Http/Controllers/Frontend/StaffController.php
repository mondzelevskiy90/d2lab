<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\Content;
use App\Services\Userdata;
use App\Models\Branch;
use App\Models\Branch_staff;
use App\Models\User;
use Auth;
use Mail;

class StaffController extends Controller {
    public function index() {
        $branch = Content::checkBranch();

        $data = Branch_staff::with([
                'branch',
                'user',
                'role'
            ])
            ->whereIn('branch_id', function($query){
                $query->select('id')
                    ->from(with(new Branch)->getTable())
                    ->where('owner_id', Auth::user()->id);
            })
            ->whereNotIn('user_id', function($query){
                $query->select('id')
                    ->from(with(new User)->getTable())
                    ->where('status', config('statuses.removed'));
            })
            ->get();

    	$breadcrumbs = array();

    	$breadcrumbs[] = array(
			'text' => __('translations.home'),
			'href' => route('home')
		);

        $breadcrumbs[] = array(
            'text' => __('translations.account'),
            'href' => route('account')
        );

		$breadcrumbs[] = array(
			'text' => __('translations.staff'),
			'href' => route('staff')
		);

        $seo = (object)[
            'name' => __('translations.staff'),
            'meta_title' => __('translations.staff'),
            'meta_description' => 'Dentist2Lab service staff page',
        ];

    	return view('Frontend.pages.account.staff.index', compact(
    		'data', 
            'branch',
    		'seo',
    		'breadcrumbs'
    	));
    }

    public function create(Request $request) {
        if (!Content::checkBranch()) return redirect()->route('account');

        $data = array();        

        if ($request->method() == 'POST') {
            $messages = array(
                'branch_id.required' => 'Field is required!',
                'branch_id.not_in' => 'Field is not selected!',
                'name.required' => 'Field is required!',                
                'name.max' => 'Field contains more then 255 symbols!',
                'surname.required' => 'Field is required!',               
                'surname.max' => 'Field contains more then 255 symbols!',
                'email.required' => 'Field is required!',
                'email.email' => 'Field content is not an e-mail',
                'email.unique' => 'E-mail is not unique!'
            );

            $rules = array(
                'branch_id' => 'required|not_in:0',
                'name' => 'required|max:255',
                'surname' => 'required|max:255',
                'email' => 'required|email|unique:users'
            );           

            $request->validate($rules, $messages);

            $user = new User;

            $timestamp = time();
            $name = $request->input('name');
            $surname = $request->input('surname');
            $email = $request->input('email');
            $password = str_shuffle('eRg1CvbTt345BsWN');
            $role_id = $request->input('role_id');

            $user->role_id = $role_id;
            $user->type = Auth::user()->type;
            $user->name = $name;
            $user->surname = $surname;
            $user->email = $email;
            $user->password = \Illuminate\Support\Facades\Hash::make($password);
            $user->avatar = 'logos/default_logo.png';
            $user->email_verified_at = null;
            $user->settings = '{"locale":"en"}';
            $user->created_at = $timestamp;
            $user->updated_at = $timestamp;
            $user->status = 2;
            $user->save();

            $branch_staff = new Branch_staff;

            $branch_staff->branch_id = $request->branch_id;
            $branch_staff->user_id = $user->id;  
            $branch_staff->can_pay = $request->input('can_pay') && $request->input('can_pay') == 1 ? 1 : 0;                   
            $branch_staff->save();

            $content = [
                'role' => Userdata::getRole($role_id)->display_name,
                'owner' => Auth::user()->name. ' ' .Auth::user()->surname,
                'name' => $name.' '.$surname,
                'email' => $email,
                'password' => $password,
                'url' => route('login')              
            ];

            Mail::send('Frontend.emails.staff', $content, function ($message) use ($request, $email) {
                $message->to($email)->subject('Registration info');
            }); 
           
            return redirect()
                ->route('staff')
                ->with([
                    'message'    => __('translations.success'),
                    'alert-type' => 'success',
                ]);  
        }

        $branches = Branch::where('owner_id', Auth::user()->id)
            ->where('status', '!=', 4)            
            ->get();

        $subordinations = Userdata::getSubordination(Auth::user()->role_id);
        
        $roles = array();
        
        foreach (Userdata::getRoles() as $role) {
            if ($role['id'] == 10) continue;
            
            if (!in_array($role['id'], $subordinations)) $roles[] = $role;
        }

        $breadcrumbs = array();

        $breadcrumbs[] = array(
            'text' => __('translations.home'),
            'href' => route('home')
        );

        $breadcrumbs[] = array(
            'text' => __('translations.account'),
            'href' => route('account')
        );

        $breadcrumbs[] = array(
            'text' => __('translations.staff'),
            'href' => route('staff')
        );

        $breadcrumbs[] = array(
            'text' => __('translations.creating'). ' ' .__('translations.staff'),
            'href' => route('staff.create')
        );

        $seo = (object)[
            'name' => __('translations.creating'). ' ' .__('translations.staff'),
            'meta_title' => __('translations.creating'). ' ' .__('translations.staff'),
            'meta_description' => 'Dentist2Lab service staff creating page',
        ];

        $requests = $request->getQueryString();

        return view('Frontend.pages.account.staff.edit', compact(
            'data',
            'branches',
            'requests',
            'roles',
            'seo',
            'breadcrumbs'
        ));
    }

    public function edit(Request $request, $id) {
        if (!Content::checkBranch()) return redirect()->route('account');

        $data = Branch_staff::with('user')
            ->where('id', (int)$id)
            ->firstOrFail();

        if ($request->method() == 'POST') {
            $user = User::where('id', $data->user_id)->first();

            $user->role_id = $request->input('role_id');
            $user->status = $request->input('status') ? $request->input('status') : 1;
            $user->updated_at = time();
            $user->save();            

            $data->branch_id = $request->input('branch_id');
            $data->can_pay = $request->input('can_pay') && $request->input('can_pay') == 1 ? 1 : 0;
            $data->save();
           
            return redirect()
                ->route('staff')
                ->with([
                    'message'    => __('translations.success'),
                    'alert-type' => 'success',
                ]); 
        }    

        $branches = Branch::where('owner_id', Auth::user()->id)
            ->where('status', '!=', 4)            
            ->get(); 

        $subordinations = Userdata::getSubordination(Auth::user()->role_id);
        
        $roles = array();
        
        foreach (Userdata::getRoles() as $role) {
            if (!in_array($role['id'], $subordinations)) $roles[] = $role;
        }   

        $breadcrumbs = array();

        $breadcrumbs[] = array(
            'text' => __('translations.home'),
            'href' => route('home')
        );

        $breadcrumbs[] = array(
            'text' => __('translations.account'),
            'href' => route('account')
        );

        $breadcrumbs[] = array(
            'text' => __('translations.staff'),
            'href' => route('staff')
        );

        $breadcrumbs[] = array(
            'text' => __('translations.editing'). ' ' .__('translations.staff'),
            'href' => route('staff.edit', $id)
        );

        $seo = (object)[
            'name' => __('translations.editing'). ' ' .__('translations.staff'),
            'meta_title' => __('translations.editing'). ' ' .__('translations.staff'),
            'meta_description' => 'Dentist2Lab service staff editing page',
        ];

        $requests = $request->getQueryString();

        return view('Frontend.pages.account.staff.edit', compact(
            'data',
            'branches',
            'requests',
            'roles',             
            'seo',
            'breadcrumbs'
        ));
    }

    public function delete(Request $request, $id) {
        User::where('id', (int)$id)->delete(); 

        Branch_staff::where('user_id', (int)$id)
            ->delete();       
        
        return redirect()
            ->route('staff')
            ->with([
                'message'    => __('translations.success'),
                'alert-type' => 'success',
            ]);
    }
}