<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\Notificate;
use App\Services\Currencies;
use App\Services\Content;
use App\Models\Branch;
use App\Models\Branch_list;
use App\Models\Branch_staff;
use App\Models\Industry_service;
use App\Models\Numbering_system;
use App\Models\Price;
use App\Models\Price_service;
use App\Models\Price_delivery;
use App\Models\Order;
use App\Models\Order_history;
use App\Models\Order_file;
use App\Models\Order_prescription;
use App\Models\Order_prescription_field;
use App\Models\Order_total;
use App\Models\Order_type;
use Auth;

class CheckoutController extends Controller {
    public function index(Request $request) {
        if (Auth::user()->type != config('types.clinic')) {
            return redirect()->route('orders');
        }

        $prescription_index = 1;
        $file_index = 1;
        $mylabs = false;
        $branch = false;
        $branches = false;
        $user_branch = false;
        $services = array();
        $numbering_systems = array();
        $deliveries = array();
        $order_types = array();        

        if ($request->input('branch_id')) {
            $branch = Branch::where('id', (int)$request->input('branch_id'))
                ->where('type', config('types.lab'))
                ->where('status', 1)
               // ->where('verified', 1)
                ->first();

            if (!$branch) return redirect()->route('checkout');

            if (Auth::user()->role_id == config('roles.clinic_head')) {
                $branches = Branch::where('owner_id', Auth::user()->id)
                    ->where('type', config('types.clinic'))
                    ->where('status', 1)                   
                    ->get();

                if(!count($branches)) {
                    return redirect()
                        ->route('branches')
                        ->with([
                            'message'    => __('translations.must_create_branch'),
                            'alert-type' => 'error',
                        ]); 
                }
            } else {
                $user_branch = Branch_staff::with('branch')
                    ->where('user_id', Auth::user()->id)
                    ->first();
            }

            $services = Price_service::with([
                    'service' => function($q) {
                        $q->with('parent');
                    }
                ])
                ->where('price_id', function ($q) use ($branch){
                    $q->select('id')
                        ->from(with(new Price)->getTable())
                        ->where('branch_id', $branch->id);
                })
                ->get();

            if (!$services || ($services && count($services) == 0)) {
                return redirect()->route('orders');
            }

            foreach ($services as $service) {
               if ($service->service && $service->service->parent) {
                    $service->parent_name = $service->service->parent->name;
               } else {
                    $service->parent_name = 'Uncategoriesed';
               }
            }

            function array_orderby()
            {
                $args = func_get_args();
                $data = array_shift($args);
                foreach ($args as $n => $field) {
                    if (is_string($field)) {
                        $tmp = array();
                        foreach ($data as $key => $row)
                            $tmp[$key] = $row[$field];
                        $args[$n] = $tmp;
                        }
                }
                $args[] = &$data;
                call_user_func_array('array_multisort', $args);
                return array_pop($args);
            }

            $services = $services->toArray();
            $services = array_orderby($services, 'parent_name', SORT_ASC);
           
            $deliveries = Price_delivery::with('delivery')
                ->where('price_id', function ($q) use ($branch){
                    $q->select('id')
                        ->from(with(new Price)->getTable())
                        ->where('branch_id', $branch->id);
                })
                ->get();

            $order_types = Order_type::where('status', 1)->get();

            $numbering_systems = Numbering_system::where('status', 1)
                ->orderBy('sort_order', 'ASC')
                ->get();            
        } else {            
            $mylabs = Branch_list::with('branch')
                ->where('user_id', Auth::user()->id)
                ->whereIn('branch_id', function($q) {
                    $q->select('id')
                        ->from(with(new Branch)->getTable())
                        ->where('type', config('types.lab'))
                        ->where('status', 1);
                        //->where('verified', 1);
                })
                ->get();
        }

        $currency = Currencies::getCurrencyById(1);        

    	$breadcrumbs = array();

    	$breadcrumbs[] = array(
			'text' => __('translations.home'),
			'href' => route('home')
		);

		$breadcrumbs[] = array(
			'text' => __('translations.checkout'),
			'href' => route('checkout')
		);

        $seo = (object)[
            'name' => __('translations.checkout'),
            'meta_title' => __('translations.checkout'),
            'meta_description' => 'Dentist2Lab service checkout page',
        ];

    	return view('Frontend.pages.checkout', compact(    		
    		'seo',
            'branch',
            'branches',
            'user_branch',
            'mylabs',
            'services',
            'deliveries',
            'currency',
            'numbering_systems',            
            'order_types',
            'prescription_index',
            'file_index',
    		'breadcrumbs'
    	));
    }

    public function create(Request $request) {
     
		$messages = array(
            'order_type.required' => 'Field is required!', 
            'user_id.required' => 'Field is required!',
            'user_name.required' => 'Field is required!',
            'user_name.max' => 'Field contains more then 255 symbols!',
            'branch_id.required' => 'Field is required!',	       
	        'to_branch_id.required' => 'Field is required!',
	        'to_branch_name.required' => 'Field is required!',
	        'to_branch_name.max' => 'Field contains more then 255 symbols!',
	        'user_email.required' => 'Field is required!',
	        'user_email.email' => 'Field contains not email value!',
	        'user_phone.required' => 'Field is required!',
	        'user_phone.max' => 'Field contains more then 32 symbols!',
	        'patient_name.required' => 'Field is required!',
	        'patient_lastname.required' => 'Field is required!',
	        'ship_date.required' => 'Field is required!',
	        'ship_date.max' => 'Field contains more then 255 symbols!',
			'ship_address.required' => 'Field is required!',
			'ship_address.max' => 'Field contains more then 255 symbols!' 
        );

        $rules = array(
            'order_type' => 'required',
            'user_id' => 'required',
            'user_name' => 'required|max:255',
            'branch_id' => 'required',
	        'to_branch_id' => 'required',
	        'to_branch_name' => 'required|max:255',
	        'user_email' => 'required|email',
	        'user_phone' => 'required|max:32',
	        'patient_name' => 'required',
	        'patient_lastname' => 'required',
	        'ship_date' => 'required|max:255',
			'ship_address' => 'required|max:255'
        );

        if (!$request->input('branch_name')) {
			$branch_name = Branch::where('id', (int)$request->input('branch_id'))->first()->name;
        } else {
        	$branch_name = $request->input('branch_name');
        }

        $request->validate($rules, $messages);

        $service = \App\Models\Industry_service::where('id', (int)$request->input('service_id'))->first();

        $attached = null;

        if ($request->input('attached')) {
        	$attached = implode(';', $request->input('attached'));
        }

        $data = new Order;

        $data->order_type = (int)$request->input('order_type');
        $data->user_id = (int)$request->input('user_id');
        $data->user_name = $request->input('user_name');
        $data->branch_id = (int)$request->input('branch_id');
        $data->branch_name = $branch_name;
        $data->to_branch_id = $request->input('to_branch_id');
        $data->to_branch_name = $request->input('to_branch_name');
        $data->user_email = $request->input('user_email');
        $data->user_phone = $request->input('user_phone') ? $request->input('user_phone') : '+1 (XXX) XXX-XX-XX';
        $data->patient_name = $request->input('patient_name');
        $data->patient_lastname = $request->input('patient_lastname');
        $data->patient_age = $request->input('patient_age') ? (int)$request->input('patient_age') : null;
        $data->patient_sex = $request->input('patient_sex') ? $request->input('patient_sex') : null;
        $data->ship_date = $request->input('ship_date');
		$data->ship_address = $request->input('ship_address');        
        $data->attached = $attached;    
        $data->numbering_system_id = $request->input('numbering_system_id'); 
        $data->comment = $request->input('comment') ? $request->input('comment') : null;    
        $data->status = 2; // <-- create as NEW order!

        $data->save();

        $history = new Order_history;

        $history_comment = 'Order has been created!';

        if ($request->input('comment')) $history_comment .= ' Comment: '.$request->input('comment').' ';

        $history->order_id = $data->id;
        $history->user = $request->input('user_name');
        $history->comment = $history_comment;

        $history->save();

        if ($request->input('advance_price') || $request->input('total')) {
        	$order_total = new Order_total;

        	$order_total->order_id = $data->id;

        	if ($request->input('advance_price')) {
        		$order_total->advance_price = $request->input('advance_price');
        	}

        	if ($request->input('total')) {
        		$order_total->price = $request->input('total');
        	}

        	$order_total->currency_id = 1;
        	$order_total->save();
        }

		if ($request->input('order_files')) {
			foreach ($request->input('order_files') as $key => $order_file) {
                if ($order_file) {
                    $file = new Order_file;

                    $file->order_id = $data->id;
                    $file->file = $order_file;
                    $file->save();
                }				
			}
		}

        if ($request->input('prescriptions')) {
            foreach ($request->input('prescriptions') as $key => $prescription) {
                $order_prescription = new Order_prescription;

                $order_prescription->order_id = $data->id;
                $order_prescription->service_id = $prescription['service_id'];
                $order_prescription->numbering_system_id = $prescription['numbering_system_id'];
                $order_prescription->selecteds = $prescription['selecteds'] ? $prescription['selecteds'] : null;
                $order_prescription->price_order_date = $prescription['price_order_date'];
                $order_prescription->price_for = $prescription['price_for'];
                $order_prescription->selected_option = isset($prescription['selected_option']) ? $prescription['selected_option'] : 0;
                $order_prescription->advance_price = $prescription['price'];
                $order_prescription->price = '0.00';
                $order_prescription->currency_id = 1;
                $order_prescription->comment = isset($prescription['comment']) && $prescription['comment'] ? $prescription['comment'] : null;
                $order_prescription->save();

                if (isset($prescription['fields']) && $prescription['fields']) {
                    foreach ($prescription['fields'] as $key => $field) {
                        if ($field) {
                            $prescription_field = new Order_prescription_field;

                            $prescription_field->prescription_id = $order_prescription->id;
                            $prescription_field->field_id = $key;
                            $prescription_field->value = is_array($field) ? implode(';', $field): $field;
                            $prescription_field->save();
                        }
                    }
                }
                
            }
        }
/*
        $delivery = new \App\Models\Order_delivery;

        $delivery->order_id = $data->id;
        $delivery->bill_to = config('types.lab');
        $delivery->from_branch = $data->branch_id;
        $delivery->to_branch = $data->to_branch_id; 
        $delivery->delivery_id = 12;
        $delivery->created_at = date('m-d-Y');
        $delivery->save(); 
*/       
        $notificate_message = sprintf(__('translations.order_status_ready'), route('orders.edit', $data->id), $data->id);

        Notificate::createNotification($data->to_branch_id, $data->id, $notificate_message);

        return redirect()
            ->route('orders')
            ->with([
                'message'    => __('translations.success'),
                'alert-type' => 'success',
            ]); 
    }
}