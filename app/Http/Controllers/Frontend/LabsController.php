<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Branch_list;
use Auth;

class LabsController extends Controller {
    public function index() {
        $data = Branch_list::with('branch')
            ->where('user_id', Auth::user()->id)
            ->orderBy('id', 'ASC')
            ->paginate(20);

    	$breadcrumbs = array();

    	$breadcrumbs[] = array(
			'text' => __('translations.home'),
			'href' => route('home')
		);

        $breadcrumbs[] = array(
            'text' => __('translations.account'),
            'href' => route('account')
        );

		$breadcrumbs[] = array(
			'text' => __('translations.labs'),
			'href' => route('mylabs')
		);

        $seo = (object)[
            'name' => __('translations.labs'),
            'meta_title' => __('translations.labs'),
            'meta_description' => 'Dentist2Lab service My laboratories page',
        ];

    	return view('Frontend.pages.account.labs.index', compact(
    		'data', 
    		'seo',
    		'breadcrumbs'
    	));
    }
}