<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;

class HomeController extends Controller {
    public function index() {   
    	$users = User::select('id', 'role_id')
    		->whereNotIn('status', [config('statuses.blocked'), config('statuses.removed')])
    		->whereIn('role_id', config('roles'))
    		->get();

    	$registered = 0;
    	$clinics = 0;
    	$labs = 0;

    	foreach ($users as $user) {
    		$registered++;

    		if ($user->role_id == config('roles.clinic_head')) $clinics++;
    		if ($user->role_id == config('roles.lab_head')) $labs++;
    	}

    	return view('Frontend.pages.home', compact(
    		'registered',
    		'clinics',
    		'labs'
    	));
    }
}
