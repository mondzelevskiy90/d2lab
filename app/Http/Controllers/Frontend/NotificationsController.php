<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\Notificate;
use App\Models\Branch;
use App\Models\Branch_staff;
use App\Models\Notification_message;
use App\Models\Order;
use Auth;

class NotificationsController extends Controller {
    public function index() {
        $branches = array();
        
        switch (Auth::user()->role_id) {
            case config('roles.clinic_head'):
            case config('roles.lab_head'):
                $branches = Branch::where('owner_id', Auth::user()->id)->pluck('id')->toArray();
                break;
            case config('roles.clinic_staff'):
            case config('roles.doctor'):
            case config('roles.lab_staff'):
            case config('roles.tech'):
                $staff = Branch_staff::where('user_id', Auth::user()->id)->first();

                if ($staff) $branches[] = $staff->branch_id;                            
                break;
        }

        $viewed = \App\Models\Notification_view::select('notification_id')
            ->where('user_id', Auth::user()->id)
            ->pluck('notification_id')
            ->toArray();

        $data = Notification_message::whereIn('branch_id', $branches)
            ->whereNotIn('id', $viewed)
            ->where('created_at', '<=', date('Y-m-d').' 23:59:59')
            ->orderBy('created_at', 'DESC')
            ->paginate(100);

    	$breadcrumbs = array();

    	$breadcrumbs[] = array(
			'text' => __('translations.home'),
			'href' => route('home')
		);

        $breadcrumbs[] = array(
            'text' => __('translations.account'),
            'href' => route('account')
        );

		$breadcrumbs[] = array(
			'text' => __('translations.notifications'),
			'href' => route('notifications')
		);

        $seo = (object)[
            'name' => __('translations.notifications'),
            'meta_title' => __('translations.notifications'),
            'meta_description' => 'Dentist2Lab service notifications page',
        ];

    	return view('Frontend.pages.account.notifications.index', compact(
    		'data', 
    		'seo',
    		'breadcrumbs'
    	));
    }

    public function createnotifications() {
        //notificate users for come expired ship date
        $notificate_statuses = [11,12,13,14,20];

        $orders = Order::whereIn('status', $notificate_statuses)->get();

        foreach ($orders as $order) {
            $order_url = route('orders.edit', $order->id);
            $order_ship_date = date('U', strtotime(str_replace('-', '/', $order->ship_date.' 00:00:00')));
            $lab_notificate_date = date('U', strtotime('+2 day '.date('m/d/Y').' 00:00:00'));
            $clinic_notificate_date = date('U', strtotime('+1 day '.date('m/d/Y').' 00:00:00'));

            $notificate_message = sprintf(__('translations.order_status_ship_date'), $order_url, $order->id);

            if ($order_ship_date == $clinic_notificate_date) {
                Notificate::createNotification($order->branch_id, $order->id, $notificate_message);                
            }

            if ($order_ship_date == $lab_notificate_date) {
                Notificate::createNotification($order->to_branch_id, $order->id, $notificate_message); 
            }
        }
        //end notificate users for come expired ship date
    }
}