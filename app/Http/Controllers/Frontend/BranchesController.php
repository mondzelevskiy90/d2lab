<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\Img;
use App\Services\Google;
use App\Services\Userdata;
use App\Models\Branch;
use App\Models\Industry;
use App\Models\Locality;
use App\Models\Country;
use Auth;

class BranchesController extends Controller {
    public function index(Request $request) {
        $data = Branch::where('owner_id', Auth::user()->id)
            ->where('status', '!=', 4);

        $search = false;

        if ($request->input('search')) {
            $search = $request->input('search');
            $data->where('name', 'LIKE', $search.'%');
        }

        $data = $data->orderBy('created_at', 'DESC')->get();

    	$breadcrumbs = array();

    	$breadcrumbs[] = array(
			'text' => __('translations.home'),
			'href' => route('home')
		);

        $breadcrumbs[] = array(
            'text' => __('translations.account'),
            'href' => route('account')
        );

		$breadcrumbs[] = array(
			'text' => __('translations.branches'),
			'href' => route('branches')
		);

        $seo = (object)[
            'name' => __('translations.branches'),
            'meta_title' => __('translations.branches'),
            'meta_description' => 'Dentist2Lab service Departments page',
        ];

        $requests = $request->getQueryString();

    	return view('Frontend.pages.account.branches.index', compact(
    		'data', 
    		'seo',
    		'breadcrumbs',
            'requests',
            'search'
    	));
    }

    public function create(Request $request) {
        $data = array();

        $locality = Locality::with([
                'region',
                'country'
            ])
            ->where('id', (int)Auth::user()->locality)
            ->first();

        $industries = Industry::where('parent_id', 0)
            ->where('status', 1)
            ->get();

        $requests = $request->getQueryString();        

        $breadcrumbs = array();

        $breadcrumbs[] = array(
            'text' => __('translations.home'),
            'href' => route('home')
        );

        $breadcrumbs[] = array(
            'text' => __('translations.account'),
            'href' => route('account')
        );

        $breadcrumbs[] = array(
            'text' => __('translations.branches'),
            'href' => route('branches')
        );

        $breadcrumbs[] = array(
            'text' => __('translations.creating'). ' ' .__('translations.branch'),
            'href' => route('branches.create')
        );

        $seo = (object)[
            'name' => __('translations.creating'). ' ' .__('translations.branch'),
            'meta_title' => __('translations.creating'). ' ' .__('translations.branch'),
            'meta_description' => 'Dentist2Lab service Departments creating page',
        ];

        return view('Frontend.pages.account.branches.edit', compact(
            'data', 
            'locality',
            'industries',
            'seo',
            'breadcrumbs',
            'requests'
        ));
    }

    public function store(Request $request) {
        $messages = array(
            'name.required' => 'Field is required!',                
            'name.max' => 'Field contains more then 255 symbols!',
            'description.required' => 'Field is required!',                
            'description.max' => 'Field contains more then 1000 symbols!',                
            'email.required' => 'Field is required!',
            'email.email' => 'Field content is not an e-mail',
            'industry_id.required' => 'Field is required!',
            'industry_id.not_in' => 'Field is not selected!',
            'zip_code.required' => 'Field is required!',
            'region.required' => 'Field is required!',
            'region.not_in' => 'Field is not selected!',
            'locality.required' => 'Field is required!',
            'locality.not_in' => 'Field is not selected!',
            'address.required' => 'Field is required!',
            'phone.required' => 'Field is required!',
           // 'tin.required' => 'Field is required!',
            'logo.image' => 'Uploaded file is not an image!',
            'logo.mimes' => 'Uploaded file format is not correct!',
            'logo.max' => 'Uploaded file under 10Mb!'
        );

        $rules = array(
            'name' => 'required|max:255', 
            'description' => 'required|max:1000',                
            'email' => 'required|email',
            'industry_id' => 'required|not_in:0',
            'zip_code' => 'required',
            'region' => 'required|not_in:0',
            'locality' => 'required|not_in:0',
            'address' => 'required',
            'phone' => 'required',
           // 'tin' => 'required',
            'logo' => 'image|mimes:jpeg,jpg,png|max:10240'                
        );

        $request->validate($rules, $messages);

        $data = new Branch;

        $timestamp = time();

        $data->type = Auth::user()->type;
        $data->owner_id = Auth::user()->id;
        $data->industry_id = (int)$request->input('industry_id');
        $data->name = $request->input('name');
        $data->description = $request->input('description');
        $data->email = $request->input('email');
        $data->phone = $request->input('phone');
        $data->zip_code = $request->input('zip_code');
        $data->locality = (int)$request->input('locality');
        $data->address = $request->input('address'); 
        $data->tin = $request->input('tin') ? $request->input('tin') : null;   
        $data->verified = 2;         
        $data->sort_order = 0;
        $data->status = (int)$request->input('status');
        $data->created_at = $timestamp;
        $data->updated_at = $timestamp;

        if ($request->file('logo')) { 
            if ($request->input('data_logo')['status'] == 1) {
                $data->logo = Img::cropUploadedImage($request->file('logo'), '/logos/'.date('FY').'/', 275, 275, $request->input('data_logo'));
            } else {
                $data->logo = Img::fitImage($request->file('logo'), '/logos/'.date('FY').'/', 275, 275, 80); 
            }               
                         
        } else {
            $data->logo = Auth::user()->logo;
        }

        $data->save(); 

        if ($request->input('country') && $request->input('region') && $request->input('locality') && $request->input('address') && $data->type == config('types.lab')) {            
            $country = Country::where('id', $request->input('country'))->first();

            if ($country) {
                $place = Google::getLocationDataByName($request->input('address'), $country);

                $lat = '40.762705';
                $lng = '-74.010930';

                if (isset($place->results[0]->geometry->location) && $place->results[0]->geometry->location) {
                    $lat = $place->results[0]->geometry->location->lat;
                    $lng = $place->results[0]->geometry->location->lng;
                } 

                $data->geolocation = '{"lat":"'.$lat.'", "lon":"'.$lng.'"}';
                $data->save();                
            }
        }
       
        return redirect()
            ->route('branches', $request->getQueryString())
            ->with([
                'message'    => __('translations.success'),
                'alert-type' => 'success',
            ]); 
    }

    public function edit(Request $request, $id) {
        $data = Branch::with('nmi_info')
            ->where('id', (int)$id)
            ->firstOrFail();

        $locality = Locality::with([
                'region',
                'country'
            ])
            ->where('id', (int)$data->locality)
            ->first();

        $industry = Industry::where('id', $data->industry_id)->first();

        $requests = $request->getQueryString();

        $breadcrumbs = array();

        $breadcrumbs[] = array(
            'text' => __('translations.home'),
            'href' => route('home')
        );

        $breadcrumbs[] = array(
            'text' => __('translations.account'),
            'href' => route('account')
        );

        $breadcrumbs[] = array(
            'text' => __('translations.branches'),
            'href' => route('branches')
        );

        $breadcrumbs[] = array(
            'text' => __('translations.editing'). ' ' .__('translations.branch'),
            'href' => route('branches.edit', $id)
        );

        $seo = (object)[
            'name' => __('translations.editing'). ' ' .__('translations.branch'),
            'meta_title' => __('translations.editing'). ' ' .__('translations.branch'),
            'meta_description' => 'Dentist2Lab service Departments editing page',
        ];

        return view('Frontend.pages.account.branches.edit', compact(
            'data', 
            'locality',
            'industry',
            'seo',
            'breadcrumbs',
            'requests'
        ));
    }

    public function update(Request $request, $id) {

        $data = Branch::where('id', (int)$id)->first();

        $locality = $data->locality;
        $address = $data->address;
//dd($request->input());
        $messages = array(
            'name.required' => 'Field is required!',                
            'name.max' => 'Field contains more then 255 symbols!',
            'description.required' => 'Field is required!',                
            'description.max' => 'Field contains more then 1000 symbols!',                
            'email.required' => 'Field is required!',
            'email.email' => 'Field content is not an e-mail',
            'zip_code.required' => 'Field is required!',
            'region.required' => 'Field is required!',
            'region.not_in' => 'Field is not selected!',
            'locality.required' => 'Field is required!',
            'locality.not_in' => 'Field is not selected!',
            'address.required' => 'Field is required!',
            'phone.required' => 'Field is required!',
           // 'tin.required' => 'Field is required!',
            'logo.image' => 'Uploaded file is not an image!',
            'logo.mimes' => 'Uploaded file format is not correct!',
            'logo.max' => 'Uploaded file under 10Mb!'
        );

        $rules = array(
            'name' => 'required|max:255',  
            'description' => 'required|max:1000',                
            'email' => 'required|email',
            'zip_code' => 'required',
            'region' => 'required|not_in:0',
            'locality' => 'required|not_in:0',
            'address' => 'required',
            'phone' => 'required',
           // 'tin' => 'required',
            'logo' => 'image|mimes:jpeg,jpg,png|max:10240'                
        );

        $request->validate($rules, $messages);

        $timestamp = time();

        $data->name = $request->input('name');
        $data->description = $request->input('description');
        $data->email = $request->input('email');
        $data->phone = $request->input('phone');
        $data->zip_code = $request->input('zip_code');
        $data->locality = $request->input('locality');
        $data->address = $request->input('address'); 
        $data->tin = $request->input('tin') ? $request->input('tin') : null;            
        $data->sort_order = 0;
        $data->updated_at = $timestamp;

        if ($request->input('status')) $data->status = $request->input('status');

        if ($request->file('logo')) { 
            if ($request->input('data_logo')['status'] == 1) {
                $data->logo = Img::cropUploadedImage($request->file('logo'), '/logos/'.date('FY').'/', 275, 275, $request->input('data_logo'));
            } else {
                $data->logo = Img::fitImage($request->file('logo'), '/logos/'.date('FY').'/', 275, 275, 80); 
            }          
        } 

        $data->save(); 

        if ($request->input('country') && $request->input('region') && $request->input('locality') && $request->input('address') && $data->type == config('types.lab')) {

            if ($request->input('locality') != $locality || $request->input('address') != $address) {
                $country = Country::where('id', $request->input('country'))->first();

                if ($country) {
                    $place = Google::getLocationDataByName($request->input('address'), $country);
                    
                    $lat = '40.762705';
                    $lng = '-74.010930';

                    if (isset($place->results[0]->geometry->location) && $place->results[0]->geometry->location) {
                        $lat = $place->results[0]->geometry->location->lat;
                        $lng = $place->results[0]->geometry->location->lng;
                    } 

                    $data->geolocation = '{"lat":"'.$lat.'", "lon":"'.$lng.'"}';
                    $data->save();
                }
                
            }
        }
       
        return redirect()
            ->route('branches', $request->getQueryString())
            ->with([
                'message'    => __('translations.success'),
                'alert-type' => 'success',
            ]); 
    }

    public function delete(Request $request, $id) {
        $branch = Branch::where('id', (int)$id)
            ->where('owner_id', Auth::user()->id)
            ->first();

        if ($branch) {
            $branch->status = 4;
            $branch->save();

            $users = \App\Models\User::whereIn('id', function($query) use ($id) {
                    $query->select('user_id')
                        ->from(with(new \App\Models\Branch_staff)->getTable())
                        ->where('branch_id', $id);
                })
                ->get();

            foreach ($users as $user) {
                if ($user->status != config('statuses.removed')) {
                    $user->status = config('statuses.blocked');
                    $user->notice = 'Blocked while Department deletion';
                    $user->save();
                }
            }

            return redirect()
            ->route('branches')
            ->with([
                'message'    => __('translations.success'),
                'alert-type' => 'success',
            ]);

        } else {
            return redirect()
            ->route('branches')
            ->with([
                'message'    => __('translations.fail'),
                'alert-type' => 'error',
            ]);
        }
    }
}