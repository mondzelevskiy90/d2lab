<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Industry_service extends Model
{
	public $timestamps = false;
	protected $table = 'industry_services';	

	public function parent() {
		return $this->hasOne('App\Models\Industry_service', 'id', 'parent_id');
	}

	public function industry() {
		return $this->hasOne('App\Models\Industry', 'id', 'industry_id');
	}
}
