<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class User_apple extends Model
{
	public $timestamps = false;
	protected $table = 'user_apple';

	public function user() {
		return $this->hasOne('App\Models\User', 'id', 'user_id');
	}	
}
