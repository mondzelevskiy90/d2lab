<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Portfolio_image extends Model
{
    public $timestamps = false;
	protected $table = 'portfolio_images';
	protected $fillable = ['portfolio_id'];

	public function service() {
		return $this->hasOne('App\Models\Industry_service', 'id', 'industry_service_id');
	}

	public function branch() {
		return $this->hasOneThrough(
    		'App\Models\Branch',
    		'App\Models\Portfolio',     		
    		'id', 
    		'id',
    		'portfolio_id',
    		'branch_id'
    	);
	}


}
