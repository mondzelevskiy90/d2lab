<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order_history extends Model
{
    public $timestamps = false;
	protected $table = 'order_history';	
}
