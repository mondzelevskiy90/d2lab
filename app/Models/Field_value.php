<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Field_value extends Model
{
    public $timestamps = false;
	protected $table = 'field_values';	
}
