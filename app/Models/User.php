<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function type_info() {
    	return $this->hasOne('App\Models\User_type', 'id', 'type');
    }

    public function status_info() {
    	return $this->hasOne('App\Models\User_status', 'id', 'status');
    }

    public function role_info() {
    	return $this->hasOne('App\Models\Role', 'id', 'role_id');
    }
}
