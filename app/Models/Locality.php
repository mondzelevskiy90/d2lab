<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Locality extends Model
{
	public $timestamps = false;
    protected $table = 'localities';
    protected $fillable = ['name'];

    public function region() {
    	return $this->hasOne('App\Models\Region', 'id', 'region_id');
    }

    public function country() {
    	return $this->hasOne('App\Models\Country', 'id', 'country_id');
    }
}
