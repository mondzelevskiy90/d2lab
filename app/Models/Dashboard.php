<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Dashboard extends Model {
    public $timestamps = false;
	protected $table = 'dashboard';

	public function dashboard_default() {
		return $this->hasOne('App\Models\Dashboard_default', 'dashboard_id', 'id');
	}

	public function dashboard_custom() {
		return $this->hasOne('App\Models\Dashboard_custom', 'dashboard_id', 'id');
	}
}
