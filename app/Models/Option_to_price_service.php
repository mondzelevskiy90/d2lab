<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Option_to_price_service extends Model
{
	public $timestamps = false;	
	protected $table = 'option_to_price_service';
}