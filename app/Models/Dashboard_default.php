<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Dashboard_default extends Model
{
    public $timestamps = false;
	protected $table = 'dashboard_default_roles';

	public function dashboard() {
		return $this->hasOne('App\Models\Dashboard', 'id', 'dashboard_id');
	}
}
