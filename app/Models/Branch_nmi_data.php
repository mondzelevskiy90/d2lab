<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Branch_nmi_data extends Model
{
	public $timestamps = false;
	protected $table = 'branch_nmi_data';

	public function branch() {
		return $this->hasOne('App\Models\Branch', 'id', 'branch_id');
	}	
}
