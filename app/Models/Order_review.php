<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order_review extends Model
{
	protected $table = 'order_reviews';	

	public function user() {
		return $this->hasOne('App\Models\User', 'id', 'user_id');
	}

	public function branch() {
		return $this->hasOne('App\Models\Branch', 'id', 'branch_id');
	}

	public function comments() {
		return $this->hasMany('App\Models\Order_review_comment', 'review_id', 'id');
	}

}
