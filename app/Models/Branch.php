<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Branch extends Model
{
    protected $fillable = ['name'];

    public function owner() {
    	return $this->hasOne('App\Models\User', 'id', 'owner_id');
    }

    public function type_info() {
        return $this->hasOne('App\Models\User_type', 'id', 'type');
    }

    public function price() {
        return $this->hasOne('App\Models\Price', 'branch_id', 'id');
    }

    public function nmi_info() {
        return $this->hasOne('App\Models\Branch_nmi_data', 'branch_id', 'id');
    }

    public function prices() {    	
		return $this->hasManyThrough(
    		'App\Models\Price_service',
    		'App\Models\Price',     		
    		'branch_id', 
    		'price_id',
    		'id',
    		'id'
    	);	
    }

    public function portfolio() {
    	return $this->hasOne('App\Models\Portfolio', 'branch_id', 'id');
    }

    public function staff() {
        return $this->hasMany('App\Models\Branch_staff', 'branch_id', 'id');
    }

    public function locality() {
        return $this->hasOne('App\Models\Locality', 'id', 'locality');
    }

    public function locality_info() {
        return $this->hasOne('App\Models\Locality', 'id', 'locality');
    }
}