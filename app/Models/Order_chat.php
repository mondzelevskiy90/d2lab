<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order_chat extends Model
{
    public $timestamps = false;
	protected $table = 'order_chat';

	public function user() {
		return $this->hasOne('App\Models\User', 'id', 'user_id')->select('id', 'name', 'surname', 'avatar');
	}	
}
