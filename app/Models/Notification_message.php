<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Notification_message extends Model
{
    public $timestamps = false;
	protected $table = 'notifications';	

	public function viewed() {
        return $this->hasMany('App\Models\Notification_view', 'notification_id', 'id');
    }

}
