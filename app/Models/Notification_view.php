<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Notification_view extends Model
{
    public $timestamps = false;
	protected $table = 'notifications_view';	
}
