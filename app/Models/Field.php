<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Field extends Model
{
    public $timestamps = false;	

    public function service() {
		return $this->hasOne('App\Models\Field_to_service', 'field_id', 'id');
	}

	public function system() {
		return $this->hasOne('App\Models\Field_to_num_system', 'field_id', 'id');
	}

    public function services() {
		return $this->hasMany('App\Models\Field_to_service', 'field_id', 'id');
	}

	public function systems() {
		return $this->hasMany('App\Models\Field_to_num_system', 'field_id', 'id');
	}

	public function values() {
		return $this->hasMany('App\Models\Field_value', 'field_id', 'id');
	}
}
