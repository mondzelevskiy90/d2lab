<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    public function content() {
    	return $this->hasMany('App\Models\Article_content', 'id', 'id');
    }
}
