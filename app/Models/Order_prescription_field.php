<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order_prescription_field extends Model
{
    public $timestamps = false;
	protected $table = 'order_prescription_fields';	
}