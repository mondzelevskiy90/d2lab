<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Price_delivery extends Model
{
	public $timestamps = false;
    protected $table = 'price_delivery';
    protected $fillable = ['price_id'];
    
    public function delivery() {
		return $this->hasOne('App\Models\Delivery', 'id', 'delivery_id');
	}
}
