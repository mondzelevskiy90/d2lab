<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Payment_status extends Model
{
    public $timestamps = false;
	protected $table = 'payment_statuses';	
}
