<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class User_status extends Model
{
	public $timestamps = false;
	protected $table = 'user_status';	
}
