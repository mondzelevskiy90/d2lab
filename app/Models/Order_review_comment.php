<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order_review_comment extends Model
{
	protected $table = 'order_review_comments';	

	public function user() {
		return $this->hasOne('App\Models\User', 'id', 'user_id');
	}

	public function branch() {
		return $this->hasOne('App\Models\Branch', 'id', 'branch_id');
	}
}
