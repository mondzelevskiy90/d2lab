<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Branch_staff extends Model
{
	public $timestamps = false;
	protected $table = 'branch_staff';	

	public function user() {
		return $this->hasOne('App\Models\User', 'id', 'user_id');
	}

	public function branch() {
		return $this->hasOne('App\Models\Branch', 'id', 'branch_id');
	}

	public function role() {
		return $this->hasOneThrough(
    		'App\Models\Role',
    		'App\Models\User',     		
    		'id', 
    		'id',
    		'user_id',
    		'role_id'
    	);
	}
}
