<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    protected $fillable = ['order_id'];

    public function branch() {
        return $this->hasOne('App\Models\Branch', 'id', 'branch_id');
    }

    public function to_branch() {
        return $this->hasOne('App\Models\Branch', 'id', 'to_branch_id');
    }
    
    public function order_info() {
    	return $this->hasOne('App\Models\Order', 'id', 'order_id');
    }

    public function payment_type_info() {
    	return $this->hasOne('App\Models\Payment_type', 'id', 'payment_type');
    }

    public function payment_status_info() {
    	return $this->hasOne('App\Models\Payment_status', 'id', 'payment_status');
    }

    public function total() {
        return $this->hasOne('App\Models\Order_total', 'order_id', 'order_id');
    }

    public function nmi_info() {
        return $this->hasOne('App\Models\Branch_nmi_data', 'branch_id', 'to_branch_id');
    }

    public function prescriptions() {
        return $this->hasMany('App\Models\Order_prescription', 'order_id', 'order_id')
            ->with([
                'service',
                'numbering_system'
            ]);
    }
}
