<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    public function status_info() {
    	return $this->hasOne('App\Models\Order_status', 'id', 'status');
    }

    public function type_info() {
    	return $this->hasOne('App\Models\Order_type', 'id', 'order_type');
    }

    public function payment_info() {
    	return $this->hasOne('App\Models\Payment_status', 'id', 'payment_status');
    }

    public function delivery_info() {
        return $this->hasOne('App\Models\Delivery', 'id', 'delivery_id');
    }

    public function delivery_service() {
        return $this->hasMany('App\Models\Order_delivery', 'order_id', 'id');
    }

    public function total() {
    	return $this->hasOne('App\Models\Order_total', 'order_id', 'id');
    }

    public function invoice() {
        return $this->hasOne('App\Models\Invoice', 'order_id', 'id');
    }

    public function branch() {
        return $this->hasOne('App\Models\Branch', 'id', 'branch_id');
    }

    public function to_branch() {
        return $this->hasOne('App\Models\Branch', 'id', 'to_branch_id');
    }

    public function chat() {
        return $this->hasOne('App\Models\Order_chat', 'order_id', 'id');
    }

    public function chat_view() {
        return $this->hasOne('App\Models\Order_chat_view', 'order_id', 'id');
    }

    public function files() {
    	return $this->hasMany('App\Models\Order_file', 'order_id', 'id');
    }

    public function history() {
    	return $this->hasMany('App\Models\Order_history', 'order_id', 'id')->orderBy('id', 'DESC');
    }    

    public function prescriptions() {
    	return $this->hasMany('App\Models\Order_prescription', 'order_id', 'id');
    }

    public function reviews() {
        return $this->hasMany('App\Models\Order_review', 'order_id', 'id')->with(['user', 'branch', 'comments']);
    }
}
