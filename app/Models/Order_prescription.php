<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order_prescription extends Model
{
    public $timestamps = false;
	protected $table = 'order_prescription';

	public function service() {
		return $this->hasOne('App\Models\Industry_service', 'id', 'service_id');
	}	

	public function numbering_system() {
		return $this->hasOne('App\Models\Numbering_system', 'id', 'numbering_system_id');
	}

	public function prescription_fields() {
		return $this->hasMany('App\Models\Order_prescription_field', 'prescription_id', 'id');
	}

	public function order_info() {
		return $this->hasOne('App\Models\Order', 'id', 'order_id');
	}
}
