<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Option extends Model
{
	public $timestamps = false;	

	public function industry() {
		return $this->hasOne('App\Models\Industry', 'id', 'industry_id');
	}

	public function selected() {
		return $this->hasOne('App\Models\Option_to_service', 'option_id', 'id');
	}
}
