<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Article_content extends Model
{
    protected $table = 'articles_content';

    public $timestamps = false;
}
