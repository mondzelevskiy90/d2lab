<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Branch_order_field extends Model
{
	public $timestamps = false;
	protected $table = 'branch_order_fields';	

	public function branch() {
		return $this->hasOne('App\Models\Branch', 'id', 'branch_id');
	}

	public function industry_service() {
		return $this->hasOne('App\Models\Industry_service', 'id', 'industry_service_id');
	}

	public function field() {
		return $this->hasOne('App\Models\Field', 'id', 'field_id');
	}
}
