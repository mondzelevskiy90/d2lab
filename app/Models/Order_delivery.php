<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order_delivery extends Model
{
    public $timestamps = false;
	protected $table = 'order_delivery';

	public function delivery_info() {
        return $this->hasOne('App\Models\Delivery', 'id', 'delivery_id');
    }

    public function from_info() {
        return $this->hasOne('App\Models\Branch', 'id', 'from_branch');
    }

    public function to_info() {
        return $this->hasOne('App\Models\Branch', 'id', 'to_branch');
    }	
}
