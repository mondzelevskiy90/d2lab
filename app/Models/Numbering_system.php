<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Numbering_system extends Model
{
    public $timestamps = false;
	protected $table = 'numbering_systems';	
}
