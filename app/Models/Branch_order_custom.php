<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Branch_order_custom extends Model
{
    public $timestamps = false;
	protected $table = 'branch_order_custom';	

	public function branch() {
		return $this->hasOne('App\Models\Branch', 'id', 'branch_id');
	}
}
