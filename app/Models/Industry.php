<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Industry extends Model
{
	public $timestamps = false;
	protected $table = 'industries';

	public function parent() {
		return $this->hasOne('App\Models\Industry', 'id', 'parent_id');
	}	
}
