<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Price_service_custom extends Model
{
    public $timestamps = false;
	protected $table = 'price_services_custom';
	protected $fillable = ['price_id', 'industry_service_id', 'price_service_id'];

	public function service() {
		return $this->hasOne('App\Models\Industry_service', 'id', 'industry_service_id');
	}
}
