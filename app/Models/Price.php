<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Price extends Model
{
	public $timestamps = false;
	
    public function branch() {
    	return $this->hasOne('App\Models\Branch', 'id', 'branch_id');
    }
}
