<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Branch_list extends Model
{
	public $timestamps = false;
	protected $table = 'branches_list';	

	public function branch() {
		return $this->hasOne('App\Models\Branch', 'id', 'branch_id');
	}
}
