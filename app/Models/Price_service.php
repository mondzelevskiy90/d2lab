<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Price_service extends Model
{
    public $timestamps = false;
	protected $table = 'price_services';
	protected $fillable = ['price_id'];

	public function service() {
		return $this->hasOne('App\Models\Industry_service', 'id', 'industry_service_id');
	}

	public function custom() {
		return $this->hasOne('App\Models\Price_service_custom', 'price_service_id', 'id');
	}

	public function customs() {
		return $this->hasMany('App\Models\Price_service_custom', 'price_service_id', 'id');
	}
}
