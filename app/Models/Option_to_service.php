<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Option_to_service extends Model
{
	public $timestamps = false;	
	protected $table = 'option_to_service';

	public function option_info() {
		return $this->hasOne('App\Models\Option', 'id', 'option_id');
	}
}