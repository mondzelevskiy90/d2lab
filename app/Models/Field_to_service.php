<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Field_to_service extends Model
{
    public $timestamps = false;
	protected $table = 'field_to_service';
}
