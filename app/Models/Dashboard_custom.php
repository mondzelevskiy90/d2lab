<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Dashboard_custom extends Model
{
    public $timestamps = false;
	protected $table = 'dashboard_custom';

	public function dashboard() {
		return $this->hasOne('App\Models\Dashboard', 'id', 'dashboard_id');
	}
}
