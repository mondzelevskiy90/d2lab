window.verifyAppleToken = require('verify-apple-id-token').default;

window.appleSignIn = () => {	
	document.addEventListener('AppleIDSignInOnSuccess', async function (data) {
		console.log(data);

		if (data.detail && data.detail.authorization) {	

			const id_token = data.detail.authorization.id_token;

			try {
			 /* const userAppleId  = await apple.verifyIdToken(
			    id_token,
			    {
			      audience: 'dentist2lab.com2',
			      ignoreExpiration: true
			    }
			  );*/

				const userAppleId = await verifyAppleToken({
					idToken: id_token,
					clientId: 'dentist2lab.com2',
					nonce: 'nonce'
				});

			  console.log(userAppleId);

			  if (userAppleId && 1 < 0) {
			  	$.ajax({
					url: '/ajax/getData',
					type: 'post',
				    dataType: 'json',
				    headers: {
				        'X-CSRF-TOKEN': '{{ csrf_token() }}'
				    },
				    data: {action:'appleUser', apple_token: userAppleId},	
				    success: function(data) {
				    	if (data) {
				    	 	window.location.href = '/account';
				    	}
				    },
				    error: function () {
				    	showPopup(('Social network error!'));
				    }
				});		
			  }
			} catch (err) {
			  console.error(err);
			}
		} else {
			showPopup(('Social network error!'));

			return false;
		}
	}); 

	document.addEventListener('AppleIDSignInOnFailure', function (error) {
		showPopup(('Social network error!'));
	    console.log(error);
	});	
}