import $ from 'jquery';
window.$ = window.jQuery = $;

window.onload = () => {
	hideLoader();	
}

function showLoader() {
	$('#loader').css('display', 'flex');
}

function hideLoader() {
	$('#loader').css('display', 'none');
}

function showPopup(data) {
	$.colorbox({
		html: '<div class="popup-content">'+data+'</div>',
		onComplete : function() {
			$(this).colorbox.resize();
		}
	});
}