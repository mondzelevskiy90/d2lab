@extends('voyager::master')

@section('page_title', __('voyager::generic.'.(isset($data->id) ? 'edit' : 'add')).' '.$data->name)

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('page_header')
    <h1 class="page-title">
        <i class="voyager-edit"></i>
        {{ __('voyager::generic.'.(isset($data->id) ? 'edit' : 'add')).' '.$data->name }}
    </h1>
@stop

@php
function create_services_list($items, $selected, $parents = array(), $padding = 10) {    
    foreach ($items as $item) {      
        $line = '<tr>';
        $line .= '<td>';
        $line .= '<input type="checkbox" id="services'.$item['id'].'" name="services['.$item['id'].']" '.(in_array($item['id'], $selected) ? 'checked="checked"' : '').' data-parents="'.(count($parents) ? implode(',', $parents) : '').'" data-id="'.$item['id'].'" class="service-item-input">';
        $line .= '</td>';
        $line .= '<td style="padding-left:'.$padding.'px;'.($item['childrens'] ? 'font-weight:bold;' : '').($padding != 0 ? 'font-sise:'.(14 - ($padding / 30)).'px;' : '').'">'.$item['name'].'</td>';        
        $line .= '</tr>';

        echo $line;

        $parents_arr = $parents;
        $parents_arr[] = $item['id'];

        if (!empty($item['childrens'])) create_services_list($item['childrens'], $selected, $parents_arr, $padding + 30);
    }
}
@endphp

@section('content')
    <div class="page-content container-fluid">
        <form class="form-edit-add" role="form" action="@if(!is_null($data->id)){{ route('voyager.'.$type.'.update', [$data->id, $requests]) }}@else{{ route('voyager.'.$type.'.store', [$requests]) }}@endif" method="POST" enctype="multipart/form-data" autocomplete="off">            
            @if(isset($data->id))
                {{ method_field("PUT") }}
            @endif
            {{ csrf_field() }}

            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-bordered">  
                        <div class="panel-body">                 
                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif                                                    
                            <div class="form-group">
                                <label for="status">{{ __('adm.status') }}</label>
                                <select class="form-control" name="status">        
                                    <option value="1" @if(isset($data->status) && $data->status == 1) selected @endif>Active</option>       
                                    <option value="2" @if(isset($data->status) && $data->status == 2) selected @endif>Disactive</option>                                      
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="name">{{ __('adm.name') }}</label>
                                <input type="text" class="form-control" id="name" name="name" placeholder="{{ __('adm.name') }}" value="{{ old('name', $data->name ?? '') }}">
                            </div> 
                            <div class="form-group">
                                <label for="description">{{ __('adm.description') }}</label>
                                <input type="text" class="form-control" id="description" name="description" placeholder="{{ __('adm.description') }}" value="{{ old('description', $data->description ?? '') }}">
                            </div>
                            <div class="form-group">
                                <label for="help">Help</label>
                                <input type="text" class="form-control" id="help" name="help" placeholder="Help" value="{{ old('help', $data->help ?? '') }}">
                            </div> 
                            <div class="form-group">
                                <label for="default_prescription">Use for default prescription</label>
                                <select class="form-control" name="default_prescription">        
                                    <option value="1" @if(isset($data->default_prescription) && $data->default_prescription == 1) selected @endif>Active</option>       
                                    <option value="2" @if(isset($data->default_prescription) && $data->default_prescription == 2) selected @endif>Disactive</option>                                      
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="required">Required</label>
                                <select class="form-control" name="required">        
                                    <option value="1" @if(isset($data->required) && $data->required == 1) selected @endif>Yes</option>       
                                    <option value="2" @if(isset($data->required) && $data->required == 2) selected @endif>No</option>                                      
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="field_type">Field type</label>
                                <select class="form-control" name="field_type" id="field_type">        
                                    <option value="text" @if(isset($data->field_type) && $data->field_type == 'text') selected @endif>Input type=text</option>       
                                    <option value="checkbox" @if(isset($data->field_type) && $data->field_type == 'checkbox') selected @endif>Input type=checkbox (multiple selection)</option>
                                    <option value="radio" @if(isset($data->field_type) && $data->field_type == 'radio') selected @endif>Input type=radio (single selection)</option>
                                    <option value="select" @if(isset($data->field_type) && $data->field_type == 'select') selected @endif>Select</option> 
                                    <option value="file" @if(isset($data->field_type) && $data->field_type == 'file') selected @endif>Input type=file</option>                                    
                                </select>
                            </div>   
                            <div id="field_values_block" class="form-group" style="@if(isset($data->field_type) && ($data->field_type == 'select' || $data->field_type == 'checkbox' || $data->field_type == 'radio')){{ 'display: block;' }}@else{{ 'display:none;' }}@endif">
                                @php
                                    $cnt = 0;
                                @endphp

                                <table id="dataTable" class="table table-hover">
                                    <thead>
                                        <tr>                                
                                            <th>Value</th>
                                            <th>Image (only for checkbox type!)</th>
                                            <th>Default</th>
                                            <th>Sort order</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody id="field_values_tbody">
                                        @foreach($field_values as $field_value)
                                            <tr id="field_line{{ $cnt }}">
                                                <td>
                                                    <input type="text" name="field_values[{{ $cnt }}][value]" class="form-control" value="{{ $field_value->value }}" placeholder="Value">
                                                </td>
                                                <td>
                                                    @if($field_value->image)                            
                                                    <img src="{{ filter_var($field_value->image, FILTER_VALIDATE_URL) ? $field_value->image : Voyager::image( $field_value->image ) }}" style="width:30px; height:auto; clear:both; display:block; padding:2px; border:1px solid #ddd;margin-bottom:10px;" />
                                                        <input type="hidden" name="field_values[{{ $cnt }}][image]" class="form-control" value="{{ $field_value->image }}">
                                                    @else
                                                        <input type="file" name="field_values[{{ $cnt }}][image]" class="form-control" value="">
                                                    @endif
                                                </td>
                                                <td>
                                                    <input type="checkbox" name="field_values[{{ $cnt }}][selected]" @if($field_value->selected && $field_value->selected == 1){{ 'checked="checked"' }}@endif>
                                                </td>
                                                <td>
                                                    <input type="text" name="field_values[{{ $cnt }}][sort_order]" class="form-control" value="{{ $field_value->sort_order }}">
                                                </td>
                                                <td>
                                                    <select class="form-control" name="field_values[{{ $cnt }}][status]">
                                                        <option value="1" @if($field_value->status == 1) selected="selected" @endif>Active</option>
                                                        <option value="2" @if($field_value->status == 2) selected="selected" @endif>Disabled</option>
                                                    </select>
                                                </td>
                                                <td><div class="btn btn-danger" onclick="document.getElementById('field_line{{ $cnt }}').remove();">Remove</div></td>
                                            </tr>
                                            @php $cnt++; @endphp
                                        @endforeach                                    
                                    </tbody>
                                    <tfoot>
                                        <td colspan="4"></td>
                                        <td>
                                            <div class="btn btn-primary" onclick="addFieldValueLine()">Add</div>
                                        </td>
                                    </tfoot>
                                </table>


                                <input type="hidden" id="use_values" name="use_values" value="@if(count($field_values)){{ 1 }}@else{{ 0 }}@endif">
                            </div>             
                            <div class="form-group">
                                <label for="sort_order">{{ __('adm.sort_order') }}</label>
                                <input type="text" class="form-control" id="sort_order" name="sort_order" placeholder="{{ __('voyager::generic.sort_order') }}" value="{{ old('sort_order', $data->sort_order ?? 0) }}">
                            </div> 
                            <div class="form-group">
                                <label for="image">{{ __('adm.image') }}</label>
                                @if(isset($data->image) && $data->image != '')
                                    <img src="{{ filter_var($data->image, FILTER_VALIDATE_URL) ? $data->image : Voyager::image( $data->image ) }}" style="width:50px; height:auto; clear:both; display:block; padding:2px; border:1px solid #ddd;margin-bottom:10px;" />
                                @endif
                                <input type="file" data-name="image" name="image" id="image">
                            </div> 
                            @if ($numbering_systems)
                                <div class="form-group">
                                    <br>
                                    <h3>Available for Tooth numbering systems:</h3>
                                    @foreach($numbering_systems as $numbering_system) 
                                        <label>                                        
                                            <input type="checkbox" id="systems{{ $numbering_system->id }}" name="systems[{{ $numbering_system->id }}]" @if(in_array($numbering_system->id, $selected_systems_data)) checked="checked" @endif>
                                            {{ $numbering_system->name }}
                                        </label>
                                        <br>
                                    @endforeach
                                </div>
                            @endif
                            @if ($services)
                                <div class="form-group">
                                    <br>
                                    <h3>Available for services:</h3>
                                    <table id="dataTable" class="table table-hover">
                                        <thead>
                                            <tr>                                
                                                <th></th>
                                                <th>Service name</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php 
                                                create_services_list($services, $selected_service_data); 
                                            @endphp
                                        </tbody>
                                    </table>
                                </div>
                            @endif                                                     
                        </div>
                    </div>
                </div>                
            </div>

            <button type="submit" class="btn btn-primary pull-right save">
                {{ __('voyager::generic.save') }}
            </button>
        </form>

       
    </div>
@stop

@section('javascript')
    <script>
        $('document').ready(function () {
            $('.toggleswitch').bootstrapToggle();

            $('#field_type').on('change', function(){
                var val = $(this).val();

                if (val == 'select' || val == 'checkbox' || val == 'radio') {
                    $('#field_values_block').css('display', 'block');
                    $('#use_values').val(1);
                } else {
                    $('#field_values_block').css('display', 'none');
                    $('#use_values').val(0);
                }
            });

            $('.service-item-input').on('change', function(){
                var index = $(this).data('id');
                var checked = $(this).prop('checked');
                var inputs = $('.service-item-input');

                inputs.each(function(i){
                    var parents = inputs[i].getAttribute('data-parents');

                    if (parents) {
                        parents = parents.split(',');

                        for(j = 0; j < parents.length; j++) {

                            if (parents[j] == index) {
                                 console.log(checked);
                                inputs[i].removeAttribute('checked');

                                if (checked) {
                                    inputs[i].setAttribute('checked', 'checked');
                                }                                 
                            }
                        };
                    }
                });
            });
        });

        var cnt = {{ $cnt }};

        function addFieldValueLine() {
            var html = '';

            html += '<tr id="field_line'+cnt+'">';
            html += '<td>';
            html += '<input type="text" name="field_values['+cnt+'][value]" class="form-control" value="" placeholder="Value">';
            html += '</td>';
            html += '<td>';
            html += '<input type="file" name="field_values['+cnt+'][image]" class="form-control" value="">';
            html += '</td>';
            html += '<td>';
            html += '<input type="checkbox" name="field_values['+cnt+'][selected]">';
            html += '</td>';
            html += '<td>';
            html += '<input type="text" name="field_values['+cnt+'][sort_order]" class="form-control" value="0">';
            html += '</td>';
            html += '<td>';
            html += '<select class="form-control" name="field_values['+cnt+'][status]">';
            html += '<option value="1">Active</option>';
            html += '<option value="2">Disabled</option>';
            html += '</select>';
            html += '</td>';
            html += '<td><div class="btn btn-danger" onclick="document.getElementById(\'field_line'+cnt+'\').remove();">Remove</div></td>';
            html += '</tr>';

            $('#field_values_tbody').append(html);

            cnt++;
        }
    </script>
@stop
