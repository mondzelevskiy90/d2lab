@extends('voyager::master')

@section('page_title', __('voyager::generic.viewing').' Locations')

@section('page_header')
    <h1 class="page-title">
        <i class="voyager-list-add"></i> Locations        
    </h1>
@stop
 
@section('content') 
    <div class="page-content container-fluid">
        @include('voyager::alerts')
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-body">
                        <div class="col-xs-12">
                                <div class="col-sm-12">
                                    General service charge value (0.01 = 1% ... 0.99 = 99%)
                                </div>
                                <div class="col-sm-4">
                                    <input type="text" id="service_charge" class="form-control" value="{{ setting('admin.service_charge') }}" placeholder="General service_charge value">
                                </div>
                                <div class="col-sm-8">
                                    <div class="btn btn-danger" onclick="saveServiceCharge()" style="position: relative;top: -5px;">Save service charge value</div>
                                </div>
                           
                        </div>
                        <br>
                        <br>
                        <form method="get" action="{{ route('voyager.'.$type.'.index', $requests) }}">
                            <div class="col-sm-10">
                                <div class="row">                      
                                    <div class="col-sm-12">                                        
                                        <div class="col-md-3">
                                            <select onchange="$(this).parents('form').submit()" class="form-control" name="verified">
                                                <option value="0" default>Select verified status</option>                                                
                                                <option value="1" @if(isset($filter['filter_verified']) && $filter['filter_verified'] == 1) selected @endif>Verified</option>       
                                                <option value="2" @if(isset($filter['filter_verified']) && $filter['filter_verified'] == 2) selected @endif>Not verified</option>
                                            </select>
                                        </div>
                                        <div class="col-md-3">
                                            <select onchange="$(this).parents('form').submit()" class="form-control" name="status">
                                                <option value="0" default>{{ __('adm.select_status') }}</option>                                                
                                                <option value="1" @if(isset($filter['filter_status']) && $filter['filter_status'] == 1) selected @endif>Active</option>       
                                                <option value="2" @if(isset($filter['filter_status']) && $filter['filter_status'] == 2) selected @endif>Suspended</option>
                                                <option value="3" @if(isset($filter['filter_status']) && $filter['filter_status'] == 3) selected @endif>Blocked</option>
                                                <option value="4" @if(isset($filter['filter_status']) && $filter['filter_status'] == 4) selected @endif>Removed</option>  
                                            </select>
                                        </div>                                                                       
                                        <div class="col-md-3">
                                            <input type="text" class="form-control" name="search" value="@if(isset($filter['filter_search']) && $filter['filter_search'] != ''){{ $filter['filter_search'] }}@endif" placeholder="{{ __('voyager::generic.search') }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                                        
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <input type="submit" style="margin:0;" class="btn btn-success" value="OK">
                                <a class="btn btn-danger" style="margin:0;" href="{{ route('voyager.'.$type.'.index') }}">Clear</a>
                            </div>
                        </form>
                        <table id="dataTable" class="table table-hover">
                            <thead>
                            <tr>                                
                                <th>ID</th> 
                                <th>Name</th> 
                                <th>Owner</th>
                                <th>Address</th>
                                <th>Logo</th>
                                <th>Verified</th>
                                <th>Status</th>                             
                                <th class="actions text-right">{{ __('voyager::generic.actions') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($many as $one)
                                @php
                                    if ($one->verified == 1) {
                                        $color = 'rgba(33, 152, 31, 0.35)';
                                        $verified = 'Verified';
                                    }

                                    if ($one->verified == 2) {
                                        $color = 'rgba(255, 0, 0, 0.35)';
                                        $verified = 'Not verified';
                                    }
                                @endphp
                                <tr style="font-weight:400;color:#000;background-color:{{ $color }}">
                                    <td>{{ $one->id }}</td>                                    
                                    <td>{{ $one->name }}</td>
                                    <td>
                                        @if ($one->owner)
                                        <a href="{{ route('voyager.users.edit', $one->owner->id) }}">{{ $one->owner->company }}</a>
                                        @endif
                                    </td>
                                    <td>{{ app('App\Services\Userdata')::getUserAddress($one) }}</td>
                                    <td>
                                        <img src="{{ App\Services\Img::getIcon($one->logo) }}" style="width:50px; height:auto; clear:both; display:block; padding:2px;border-radius: 50%;" />
                                    </td>
                                    <td><strong>{{ $verified }}</strong></td> 
                                    <td>
                                        @php
                                            if ($one->status == 1) $status = 'Active';
                                            if ($one->status == 2) $status = 'Suspended';
                                            if ($one->status == 3) $status = 'Blocked';
                                            if ($one->status == 4) $status = 'Removed';
                                        @endphp
                                        <strong>{{ $status }}</strong>
                                    </td>
                                    <td class="no-sort no-click bread-actions">
                                        @can('delete', $model)
                                            <div class="btn btn-sm btn-danger pull-right delete" data-id="{{ $one->id }}">
                                                <i class="voyager-trash"></i> {{ __('voyager::generic.delete') }}
                                            </div>
                                        @endcan
                                        @can('edit', $model)
                                            <a href="{{ route('voyager.'.$type.'.edit', [$one->id, $requests]) }}" class="btn btn-sm btn-primary pull-right edit">
                                                <i class="voyager-edit"></i> {{ __('voyager::generic.edit') }}
                                            </a>
                                        @endcan                                        
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="panel-body">                        
                        <div class="pull-right">{{ $many->appends(\Request::except('page'))->links() }}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal modal-danger fade" tabindex="-1" id="delete_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="{{ __('voyager::generic.close') }}">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">
                        <i class="voyager-trash"></i> {{ __('voyager::generic.delete_question') }}?
                    </h4>
                </div>
                <div class="modal-footer">
                    <form action="#" id="delete_form" method="POST">
                        {{ method_field("DELETE") }}
                        {{ csrf_field() }}
                        <input type="submit" class="btn btn-danger pull-right delete-confirm" value="{{ __('voyager::generic.delete_this_confirm') }}">
                    </form>
                    <button type="button" class="btn btn-default pull-right" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                </div>
            </div>
        </div>
    </div>
@stop

@section('javascript')   
    <script> 
        $('td').on('click', '.delete', function (e) {
            $('#delete_form')[0].action = '{{ route('voyager.'.$type.'.destroy', ['id' => '__item', $requests]) }}'.replace('__item', $(this).data('id'));

            $('#delete_modal').modal('show');
        });

        function saveServiceCharge() {
            var value = $('#service_charge').val();

            if (value && value != '') {
                $.ajax({
                    url: '/ajax/getData',
                    type: 'post',
                    dataType: 'json',
                    headers: {
                        'X-CSRF-TOKEN': '{{ csrf_token() }}'
                    },
                    data: {action:'changeServiceCharge', value: value, key: 'admin.service_charge'}
                });
            }
        }
    </script>
@stop
