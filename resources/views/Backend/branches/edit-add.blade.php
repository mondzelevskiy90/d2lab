@extends('voyager::master')

@section('page_title', __('voyager::generic.'.(isset($data->id) ? 'edit' : 'add')).' '.$data->name)

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('page_header')
    <h1 class="page-title">
        <i class="voyager-edit"></i>
        {{ __('voyager::generic.'.(isset($data->id) ? 'edit' : 'add')).' '.$data->name }}
    </h1>
@stop

@section('content')
    <div class="page-content container-fluid">
        <form class="form-edit-add" role="form" action="@if(!is_null($data->id)){{ route('voyager.'.$type.'.update', [$data->id, $requests]) }}@else{{ route('voyager.'.$type.'.store', [$requests]) }}@endif" method="POST" enctype="multipart/form-data" autocomplete="off">            
            @if(isset($data->id))
                {{ method_field("PUT") }}
            @endif
            {{ csrf_field() }}

            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-bordered">  
                        <div class="panel-body">                 
                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif                                                    
                            <div class="form-group">
                                <label for="status">{{ __('adm.status') }}</label>
                                <select class="form-control" name="status">        
                                    <option value="1" @if(isset($data->status) && $data->status == 1) selected @endif>Active</option>       
                                    <option value="2" @if(isset($data->status) && $data->status == 2) selected @endif>Suspended</option>
                                    <option value="3" @if(isset($data->status) && $data->status == 3) selected @endif>Blocked</option>
                                    <option value="4" @if(isset($data->status) && $data->status == 4) selected @endif>Removed</option>     
                                </select>
                            </div> 
                            <div class="form-group">
                                <label for="verified">Verified status</label>                                
                                <select class="form-control" name="verified">
                                    <option value="1" @if(isset($data->verified) && $data->verified == 1) selected @endif>Verified</option>       
                                    <option value="2" @if(isset($data->verified) && $data->verified == 2) selected @endif>Not verified</option>
                                </select>                                
                            </div>
                            
                            <div class="form-group">
                                <label for="name">{{ __('adm.name') }}</label>
                                <input type="text" class="form-control" id="name" name="name" placeholder="{{ __('adm.name') }}" value="{{ old('name', $data->name ?? '') }}">
                            </div> 
                            <div class="form-group">
                                <label for="description">{{ __('adm.description') }}</label>
                                <textarea class="form-control" id="description" name="description" placeholder="{{ __('adm.description') }}">{{ old('description', $data->description ?? '') }}</textarea>
                            </div>  
                            <div class="form-group">
                                <label for="email">{{ __('adm.email') }}</label>
                                <input type="email" class="form-control" id="email" name="email" placeholder="{{ __('adm.email') }}" value="{{ old('email', $data->email ?? '') }}">
                            </div>                          
                            <div class="form-group">
                                <label for="phone">{{ __('adm.phone') }}</label>
                                <input type="text" class="form-control" id="phone" name="phone" placeholder="{{ __('adm.phone') }}" value="{{ old('phone', $data->phone ?? '') }}">
                            </div>
                            @if(!is_null($data->id))
                                <div class="form-group">
                                    <label for="address">{{ __('adm.address') }}</label>
                                    <input type="text" class="form-control" id="address" value="{{ $address }}" disabled="disabled">
                                </div>  
                            @endif 
                            <div class="form-group">
                                <label for="charge_value">Custom service charge value (0,01 = 1% ... 0.99 = 99%)</label>
                                <input type="text" class="form-control" id="charge_value" name="charge_value" placeholder="Custom service charge value" value="{{ old('charge_value', $data->charge_value ?? '') }}">
                            </div>
                            <p>Manual coordinate detection service - <strong><a target="_blank" href="https://3planeta.com/gps-tools/ru/index.html">---Click---</a></strong></p>
                            <div class="form-group">
                                <label for="latitude">Latitude</label>
                                <input type="text" class="form-control" name="latitude" id="latitude" placeholder="Latitude" value="{{ old('latitude', $latitude ?? '') }}">
                            </div>
                            <div class="form-group">
                                <label for="longitude">Longitude</label>
                                <input type="text" class="form-control" name="longitude" id="longitude" placeholder="Longitude" value="{{ old('longitude', $longitude ?? '') }}">
                            </div>
                            @if(isset($data->logo) && $data->logo != '')
                            <div class="form-group">
                                <label for="logo">{{ __('adm.image') }}</label>
                                
                                    <img src="{{ filter_var($data->logo, FILTER_VALIDATE_URL) ? $data->logo : Voyager::image( $data->logo ) }}" style="width:200px; height:auto; clear:both; display:block; padding:2px; border:1px solid #ddd; margin-bottom:10px;" />
                               
                            </div>
                            @endif
                            <input type="hidden" name="price" value="@if($price){{ $price->id }}@endif">
                        </div>
                    </div>
                </div>                
            </div>

            <button type="submit" class="btn btn-primary pull-right save">
                {{ __('voyager::generic.save') }}
            </button>
            <div class="clearfix"></div>
        </form>
        @if (count($staff))
        <br>
        <br>
        <div class="panel panel-bordered"> 
            <div class="panel-body">
                <h3>Department staff</h3>
                <br>                
                <ul>
                @foreach ($staff as $user)
                	@if ($user->user) 
                   	 <li><a href="{{ route('voyager.users.edit', [$user->user_id]) }}">{{ $user->user->name.' '.$user->user->surname.' ('.$user->role->display_name.')' }}</a></li>
                    @endif
                @endforeach
                </ul>
            </div> 
        </div>
        @endif
        @if (count($portfolio))
        <br>
        <br>
        <div class="panel panel-bordered"> 
            <div class="panel-body">
                <h3>Department portfolio</h3>
                <br>                
                @foreach ($portfolio as $image) 
                    <img src="{{ App\Services\Img::getThumb($image->image) }}" style="width:125px; height:auto; clear:both; display:block; padding:2px; border:1px solid #ddd; margin-bottom:20px;margin-right: 15px; display: inline-block; vertical-align:top" />
                @endforeach
                <br>

                <p><a class="btn btn-primary pull-right" href="{{ route('voyager.portfolios.index', ['search' => $data->name ]) }}">Manage location portfolio</a></p>
                <div class="clearfix"></div>
            </div> 
        </div>
        @endif

        <iframe id="form_target" name="form_target" style="display:none"></iframe>
        <form id="my_form" action="{{ route('voyager.upload') }}" target="form_target" method="post" enctype="multipart/form-data" style="width:0px;height:0;overflow:hidden">
            {{ csrf_field() }}
            <input name="image" id="upload_file" type="file" onchange="$('#my_form').submit();this.value='';">
            <input type="hidden" name="type_slug" id="type_slug" value="{{ $type }}">
        </form>
    </div>
@stop

@section('javascript')
    <script>
        $('document').ready(function () {
            $('.toggleswitch').bootstrapToggle();
        });
    </script>
@stop
