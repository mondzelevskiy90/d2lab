@extends('voyager::master')

@section('page_title', __('voyager::generic.'.(isset($data->id) ? 'edit' : 'add')).' '.$data->name)

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('page_header')
    <h1 class="page-title">
        <i class="voyager-edit"></i>
        {{ __('voyager::generic.'.(isset($data->id) ? 'edit' : 'add')).' '.$data->name }}
    </h1>
@stop

@section('content')
    <div class="page-content container-fluid">
        <form class="form-edit-add" role="form" action="@if(!is_null($data->id)){{ route('voyager.'.$type.'.update', [$data->id, $requests]) }}@else{{ route('voyager.'.$type.'.store', [$requests]) }}@endif" method="POST" enctype="multipart/form-data" autocomplete="off">            
            @if(isset($data->id))
                {{ method_field("PUT") }}
            @endif
            {{ csrf_field() }}

            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-bordered">  
                        <div class="panel-body">                 
                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif                                                    
                            <div class="form-group">
                                <label for="branch_id">Location</label>
                                @if (is_null($data->id))
                                    <select class="form-control" name="branch_id" id="branch_id" required="required">
                                        <option value="0">--- Select Location ---</option>
                                        @foreach ($branches as $branch)
                                            <option value="{{ $branch->id }}">{{ $branch->name }}({{ \App\Services\Userdata::getUserAddress($branch) }})</option> 
                                        @endforeach
                                    </select>
                                @else 
                                    <p>{{ $data->branch->name }} ({{ \App\Services\Userdata::getUserAddress($data->branch) }})</p>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="public_key">Public key</label>
                                <input type="text" class="form-control" id="public_key" name="public_key" placeholder="Public key" value="{{ old('public_key', $data->public_key ?? '') }}" maxlength="255" required="required">
                            </div> 
                            <div class="form-group">
                                <label for="private_key">Private key</label>
                                <input type="text" class="form-control" id="private_key" name="private_key" placeholder="Private key" value="{{ old('private_key', $data->private_key ?? '') }}" maxlength="255" required="required">                            
                            </div>                         
                        </div>
                    </div>
                </div>                
            </div>

            <button type="submit" class="btn btn-primary pull-right save">
                {{ __('voyager::generic.save') }}
            </button>
        </form>

       
    </div>
@stop

@section('javascript')
    <script src="/js/select.js"></script>  
    <script> 
        $('#branch_id').select2({
            minimumInputLength: -2,            
        });
    </script>
@stop
