@extends('voyager::master')

@section('page_title', 'Location NMI data')

@section('page_header')
    <h1 class="page-title">
        <i class="voyager-list-add"></i> Location NMI data
        @can('add', $model)
            <a href="{{ route('voyager.'.$type.'.create', $requests) }}" class="btn btn-success">
                <i class="voyager-plus"></i> {{ __('voyager::generic.add_new') }}
            </a>
        @endcan
    </h1>
@stop
 
@section('content') 
    <div class="page-content container-fluid">
        @include('voyager::alerts')
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-body">
                        <form method="get" action="{{ route('voyager.'.$type.'.index', $requests) }}">
                            <div class="col-sm-10">                              
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="col-md-3">
                                            <input type="text" class="form-control" name="branch" value="@if(isset($filter['filter_branch']) && $filter['filter_branch'] != ''){{ $filter['filter_branch'] }}@endif" placeholder="Location name...">
                                        </div>              
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <input type="submit" style="margin:0;" class="btn btn-success" value="OK">
                                <a class="btn btn-danger" style="margin:0;" href="{{ route('voyager.'.$type.'.index') }}">Clear</a>
                            </div>
                        </form>
                        <table id="dataTable" class="table table-hover">
                            <thead>
                            <tr>                                
                                <th>ID</th> 
                                <th>Name</th> 
                                <th>Address</th>           
                                <th class="actions text-right">{{ __('voyager::generic.actions') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($many as $one)
                                <tr>
                                    <td>{{ $one->id }}</td>
                                    <td>{{ $one->branch->name }}</td>
                                    <td>{{ \App\Services\Userdata::getUserAddress($one->branch) }}</td>
                                    <td class="no-sort no-click bread-actions">
                                        @can('delete', $model)
                                            <div class="btn btn-sm btn-danger pull-right delete" data-id="{{ $one->id }}">
                                                <i class="voyager-trash"></i> {{ __('voyager::generic.delete') }}
                                            </div>
                                        @endcan
                                        @can('edit', $model)
                                            <a href="{{ route('voyager.'.$type.'.edit', [$one->id, $requests]) }}" class="btn btn-sm btn-primary pull-right edit">
                                                <i class="voyager-edit"></i> {{ __('voyager::generic.edit') }}
                                            </a>
                                        @endcan                                        
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="panel-body">                        
                        <div class="pull-right">{{ $many->appends(\Request::except('page'))->links() }}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal modal-danger fade" tabindex="-1" id="delete_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="{{ __('voyager::generic.close') }}">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">
                        <i class="voyager-trash"></i> {{ __('voyager::generic.delete_question') }}?
                    </h4>
                </div>
                <div class="modal-footer">
                    <form action="#" id="delete_form" method="POST">
                        {{ method_field("DELETE") }}
                        {{ csrf_field() }}                        
                        <div class="form-group" style="text-align: left;">
                            <label for="delete_text">Type 'DELETE' to the field under this text!</label>
                            <input type="text" class="form-control" id="delete_text" value="">                            
                        </div>
                        <input type="submit" class="btn btn-danger pull-right delete-confirm" value="{{ __('voyager::generic.delete_this_confirm') }}" onclick="return document.getElementById('delete_text').value == 'DELETE' ? true : false;">
                    </form>
                    <button type="button" class="btn btn-default pull-right" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                </div>
            </div>
        </div>
    </div>
@stop

@section('javascript')
    <script> 
        $('td').on('click', '.delete', function (e) {
            $('#delete_form')[0].action = '{{ route('voyager.'.$type.'.destroy', ['id' => '__item', $requests]) }}'.replace('__item', $(this).data('id'));

            $('#delete_modal').modal('show');
        });
    </script>
@stop
