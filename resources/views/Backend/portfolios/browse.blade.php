@extends('voyager::master')

@section('page_title', __('voyager::generic.viewing').' '.ucfirst($type))

@section('page_header')
    <h1 class="page-title">
        <i class="voyager-list-add"></i> {{ ucfirst($type) }}        
    </h1>
@stop
 
@section('content') 
    <div class="page-content container-fluid">
        @include('voyager::alerts')
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-body">
                        <form method="get" action="{{ route('voyager.'.$type.'.index', $requests) }}">
                            <div class="col-sm-10">
                                <div class="row">                      
                                    <div class="col-sm-12">                                              
                                        <div class="col-md-3">
                                            <input type="text" class="form-control" name="search" value="@if(isset($filter['filter_search']) && $filter['filter_search'] != ''){{ $filter['filter_search'] }}@endif" placeholder="Search by Branch name">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                                        
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <input type="submit" style="margin:0;" class="btn btn-success" value="OK">
                                <a class="btn btn-danger" style="margin:0;" href="{{ route('voyager.'.$type.'.index') }}">Clear</a>
                            </div>
                        </form>
                        <table id="dataTable" class="table table-hover">
                            <thead>
                            <tr>                                
                                <th>ID</th> 
                                <th>Image</th>
                                <th>Service group</th> 
                                <th>Branch</th>                            
                                <th class="actions text-right">{{ __('voyager::generic.actions') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($many as $one)
                                <tr>
                                    <td>{{ $one->id }}</td>                                    
                                    <td><img src="{{ App\Services\Img::getIcon($one->image) }}" style="width:50px; height:auto; clear:both; display:block; padding:2px;" /></td>
                                    <td>{{ $one->service->name }}</td>
                                    <td><a href="{{ route('voyager.branches.edit', $one->branch->id) }}">{{ $one->branch->name }}</a></td>                                    
                                    <td class="no-sort no-click bread-actions">
                                        @can('delete', $model)
                                            <div class="btn btn-sm btn-danger pull-right delete" data-id="{{ $one->id }}">
                                                <i class="voyager-trash"></i> {{ __('voyager::generic.delete') }}
                                            </div>
                                        @endcan                                                                            
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="panel-body">                        
                        <div class="pull-right">{{ $many->appends(\Request::except('page'))->links() }}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal modal-danger fade" tabindex="-1" id="delete_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="{{ __('voyager::generic.close') }}">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">
                        <i class="voyager-trash"></i> {{ __('voyager::generic.delete_question') }}?
                    </h4>
                </div>
                <div class="modal-footer">
                    <form action="#" id="delete_form" method="POST">
                        {{ method_field("DELETE") }}
                        {{ csrf_field() }}
                        <input type="submit" class="btn btn-danger pull-right delete-confirm" value="{{ __('voyager::generic.delete_this_confirm') }}">
                    </form>
                    <button type="button" class="btn btn-default pull-right" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                </div>
            </div>
        </div>
    </div>
@stop

@section('javascript')   
    <script> 
        $('td').on('click', '.delete', function (e) {
            $('#delete_form')[0].action = '{{ route('voyager.'.$type.'.destroy', ['id' => '__item', $requests]) }}'.replace('__item', $(this).data('id'));

            $('#delete_modal').modal('show');
        });
    </script>
@stop
