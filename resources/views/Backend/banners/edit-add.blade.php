@extends('voyager::master')

@section('page_title', __('voyager::generic.'.(isset($data->id) ? 'edit' : 'add')).' '.$data->name)

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('page_header')
    <h1 class="page-title">
        <i class="voyager-edit"></i>
        {{ __('voyager::generic.'.(isset($data->id) ? 'edit' : 'add')).' '.$data->name }}
    </h1>
@stop

@section('content')
    <div class="page-content container-fluid">
        <form class="form-edit-add" role="form" action="@if(!is_null($data->id)){{ route('voyager.'.$type.'.update', [$data->id, $requests]) }}@else{{ route('voyager.'.$type.'.store', [$requests]) }}@endif" method="POST" enctype="multipart/form-data" autocomplete="off">            
            @if(isset($data->id))
                {{ method_field("PUT") }}
            @endif
            {{ csrf_field() }}

            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-bordered">  
                        <div class="panel-body">                 
                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif       
                            <div class="form-group">
                                <p>Views: {{ $data->views ?? 0 }}</p>
                            </div>                                             
                            <div class="form-group">
                                <label for="status">{{ __('adm.status') }}</label>
                                <select class="form-control" name="status">        
                                    <option value="1" @if(isset($data->status) && $data->status == 1) selected @endif>Active</option>       
                                    <option value="2" @if(isset($data->status) && $data->status == 2) selected @endif>Disabled</option>
                                </select>
                            </div>                             
                            <div class="form-group">
                                <label for="name">{{ __('adm.name') }}</label>
                                <input type="text" class="form-control" id="name" name="name" placeholder="{{ __('adm.name') }}" value="{{ old('name', $data->name ?? '') }}" required="required">
                            </div>
                            <div class="form-group">
                                <label for="link">Banner link</label>
                                <input type="text" class="form-control" id="link" name="link" placeholder="Link" value="{{ old('link', $data->link ?? '') }}">
                            </div>
                            <div class="form-group">
                                <label for="position">Position</label>
                                <select name="position" class="form-control" id="position">
                                    <option value="1" @if(isset($data->position) && $data->position == 1) selected @endif>Footer</option>
                                    <option value="2" @if(isset($data->position) && $data->position == 2) selected @endif>Left</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="sort_order">Sort order</label>
                                <input type="text" class="form-control" id="sort_order" name="sort_order" placeholder="Sort order" value="{{ old('sort_order', $data->sort_order ?? '') }}">
                            </div>
                            <div class="form-group">
                                <label for="image">{{ __('adm.image') }}</label>
                                @if(isset($data->image) && $data->image != '')
                                    <img src="{{ filter_var($data->image, FILTER_VALIDATE_URL) ? $data->image : Voyager::image( $data->image ) }}" style="width:200px; height:auto; clear:both; display:block; padding:2px; border:1px solid #ddd; margin-bottom:10px;" />
                                @endif
                            </div>
                            <input type="file" data-name="image" name="image" id="image">
                        </div>
                    </div>
                </div>                
            </div>

            <button type="submit" class="btn btn-primary pull-right save">
                {{ __('voyager::generic.save') }}
            </button>
            <div class="clearfix"></div>
        </form> 
        <iframe id="form_target" name="form_target" style="display:none"></iframe>
        <form id="my_form" action="{{ route('voyager.upload') }}" target="form_target" method="post" enctype="multipart/form-data" style="width:0px;height:0;overflow:hidden">
            {{ csrf_field() }}
            <input name="image" id="upload_file" type="file" onchange="$('#my_form').submit();this.value='';">
            <input type="hidden" name="type_slug" id="type_slug" value="{{ $type }}">
        </form>
    </div>
@stop

@section('javascript')
    <script>
        $('document').ready(function () {
            $('.toggleswitch').bootstrapToggle();
        });
    </script>
@stop
