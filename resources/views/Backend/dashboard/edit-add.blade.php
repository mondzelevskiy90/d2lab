@extends('voyager::master')

@section('page_title', __('voyager::generic.'.(isset($data->id) ? 'edit' : 'add')).' '.$data->name)

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('page_header')
    <h1 class="page-title">
        <i class="voyager-edit"></i>
        {{ __('voyager::generic.'.(isset($data->id) ? 'edit' : 'add')).' '.$data->name }}
    </h1>
@stop

@section('content')
    <div class="page-content container-fluid">
        <form class="form-edit-add" role="form" action="@if(!is_null($data->id)){{ route('voyager.'.$type.'.update', [$data->id, $requests]) }}@else{{ route('voyager.'.$type.'.store', [$requests]) }}@endif" method="POST" enctype="multipart/form-data" autocomplete="off">            
            @if(isset($data->id))
                {{ method_field("PUT") }}
            @endif
            {{ csrf_field() }}

            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-bordered">  
                        <div class="panel-body">                 
                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif                                                    
                            <div class="form-group">
                                <label for="status">{{ __('adm.status') }}</label>
                                <select class="form-control" name="status">        
                                    <option value="1" @if(isset($data->status) && $data->status == 1) selected @endif>Active</option>       
                                    <option value="2" @if(isset($data->status) && $data->status == 2) selected @endif>Disactive</option>                                      
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="name">{{ __('adm.name') }}</label>
                                <input type="text" class="form-control" id="name" name="name" placeholder="{{ __('adm.name') }}" value="{{ old('name', $data->name ?? '') }}" required="required">
                            </div>
                            <div class="form-group">
                                <label for="code">Block Type</label>
                                <select name="code" id="code" class="form-control">
                                    <option value="text" @if(isset($data->code) && $data->code == 'text'){{ 'selected="selected"' }}@endif>Text block</option>
                                    <option value="reviews" @if(isset($data->code) && $data->code == 'reviews'){{ 'selected="selected"' }}@endif>Latest reviews</option>
                                    <option value="notifications" @if(isset($data->code) && $data->code == 'notifications'){{ 'selected="selected"' }}@endif>Notifications</option>
                                    <option value="orders" @if(isset($data->code) && $data->code == 'orders'){{ 'selected="selected"' }}@endif>Orders data</option>
                                    <option value="statuses" @if(isset($data->code) && $data->code == 'statuses'){{ 'selected="selected"' }}@endif>Orders statuses</option>
                                    <option value="diagram" @if(isset($data->code) && $data->code == 'diagram'){{ 'selected="selected"' }}@endif>Orders statuses diagram</option>
                                    <option value="locations" @if(isset($data->code) && $data->code == 'locations'){{ 'selected="selected"' }}@endif>Locations data</option>
                                    <option value="graph" @if(isset($data->code) && $data->code == 'graph'){{ 'selected="selected"' }}@endif>Invoices graph</option>
                                    <option value="catalog" @if(isset($data->code) && $data->code == 'catalog'){{ 'selected="selected"' }}@endif>Catalog button</option>
                                    <option value="update" @if(isset($data->code) && $data->code == 'update'){{ 'selected="selected"' }}@endif>Update button</option>
                                </select>
                            </div> 
                            <div class="form-group">
                                <label for="content">Text content</label>
                                <textarea class="richTextBox form-control" id="content" name="content" placeholder="Text content">{{ old('content', $data->content ?? '') }}</textarea>
                            </div>
                            <div class="form-group">
                                <label for="sort_order">{{ __('adm.sort_order') }}</label>
                                <input type="text" class="form-control" id="sort_order" name="sort_order" placeholder="{{ __('adm.sort_order') }}" value="{{ old('sort_order', $data->sort_order ?? 0) }}">
                            </div> 
                            <div class="form-group">
                                <label>Available for roles</label>
                                @foreach ($roles as $role)
                                    <label for="roles_{{ $role->id }}" style="width:100%;margin-bottom: 5px;">
                                        <input type="checkbox" name="roles[{{ $role->id }}]" id="roles_{{ $role->id }}" value="{{ $role->id }}" @if(in_array($role->id, $checked_roles)){{ 'checked="checked"' }}@endif>
                                        {{ $role->display_name }}
                                    </label>
                                @endforeach 
                            </div>
                        </div>
                    </div>
                </div>                
            </div>

            <button type="submit" class="btn btn-primary pull-right save">
                {{ __('voyager::generic.save') }}
            </button>
        </form>

        <iframe id="form_target" name="form_target" style="display:none"></iframe>
        <form id="my_form" action="{{ route('voyager.upload') }}" target="form_target" method="post" enctype="multipart/form-data" style="width:0px;height:0;overflow:hidden">
            {{ csrf_field() }}
            <input name="image" id="upload_file" type="file" onchange="$('#my_form').submit();this.value='';">
            <input type="hidden" name="type_slug" id="type_slug" value="{{ $type }}">
        </form>
       
    </div>
@stop

@section('javascript')
    <script>
        $('document').ready(function () {
            $('.toggleswitch').bootstrapToggle();
        });
    </script>
@stop
