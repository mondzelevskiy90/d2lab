@extends('voyager::master')

@section('page_title', __('voyager::generic.'.(isset($data->id) ? 'edit' : 'add')).' '.$data->name)

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('page_header')
    <h1 class="page-title">
        <i class="voyager-edit"></i>
        {{ __('voyager::generic.'.(isset($data->id) ? 'edit' : 'add')).' '.$data->name }}
    </h1>
@stop

@section('content')
    <div class="page-content container-fluid">
        <form class="form-edit-add" role="form" action="@if(!is_null($data->id)){{ route('voyager.'.$type.'.update', [$data->id, $requests]) }}@else{{ route('voyager.'.$type.'.store', [$requests]) }}@endif" method="POST" enctype="multipart/form-data" autocomplete="off">            
            @if(isset($data->id))
                {{ method_field("PUT") }}
            @endif
            {{ csrf_field() }}

            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-bordered">  
                        <div class="panel-body">                 
                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif                                                    
                            <div class="form-group">
                                <label for="status">{{ __('adm.status') }}</label>
                                <select class="form-control" name="status">        
                                    <option value="1" @if(isset($data->status) && $data->status == 1) selected @endif>Active</option>       
                                    <option value="2" @if(isset($data->status) && $data->status == 2) selected @endif>Disactive</option>                                      
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="name">{{ __('adm.name') }}</label>
                                <input type="text" class="form-control" id="name" name="name" placeholder="{{ __('adm.name') }}" value="{{ old('name', $data->name ?? '') }}">
                            </div> 
                            <div class="form-group">
                                <label for="prescription_description">{{ __('adm.description') }}</label>
                                <textarea class="richTextBox form-control" id="prescription_description" name="prescription_description" placeholder="{{ __('adm.description') }}">{{ old('prescription_description', $data->prescription_description ?? '') }}</textarea>
                            </div>
                            <div class="form-group">
                                <label for="sort_order">{{ __('adm.sort_order') }}</label>
                                <input type="text" class="form-control" id="sort_order" name="sort_order" placeholder="{{ __('adm.sort_order') }}" value="{{ old('sort_order', $data->sort_order ?? 0) }}">
                            </div> 
                            <div class="form-group">
                                <label for="default_list">Show for default price list?</label>
                                <select class="form-control" name="default_list">        
                                    <option value="1" @if(isset($data->id) && $data->default_list == 1) selected @endif>Yes</option>       
                                    <option value="2" @if(isset($data->id) && $data->default_list == 2) selected @endif>No</option>                                      
                                </select>
                            </div>
                            <div class="form-group" style="display:none;">
                                <label for="price_for">Price for</label>
                                <select class="form-control" name="price_for">        
                                    <option value="1" @if(isset($data->id) && $data->price_for == 1) selected @endif>For 1 item (e.g. 1 tooth)</option>       
                                    <option value="2" @if(isset($data->id) && $data->price_for == 2) selected @endif>For service</option>
                                    <option value="4" @if(isset($data->id) && $data->price_for == 4) selected @endif>For service (ignore tooth select)</option>
                                </select>
                            </div>
                           <div class="form-group" style="display:none;">
                                <label for="available_statuses">Selected statuses for SVG</label>
                                <select class="form-control" name="available_statuses">        
                                    <option value="1,2,3,4,5,6,7" @if(isset($data->id) && $data->available_statuses == '1,2,3,4,5,6,7') selected @endif>All elements</option>       
                                    <option value="1,2,7" @if(isset($data->id) && $data->available_statuses == '1,2,7') selected @endif>Only active and empty</option> 
                                    <option value="1,7" @if(isset($data->id) && $data->available_statuses == '1,7') selected @endif>Only empty</option>
                                </select>
                            </div>
                            <div class="form-group" style="display:none;">
                                <label for="available_options">Selected options</label>
                                <select class="form-control" name="available_options">        
                                    <option value="0" @if(isset($data->id) && $data->available_options == 0) selected @endif>None</option>       
                                    <option value="1" @if(isset($data->id) && $data->available_options == 1) selected @endif>Lower</option>
                                    <option value="2" @if(isset($data->id) && $data->available_options == 2) selected @endif>Upper</option>
                                    <option value="3" @if(isset($data->id) && $data->available_options == 3) selected @endif>Both</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="available_options2">Selected options</label>
                                @php 
                                    $selected_available_options2 = array();

                                    if (isset($data->available_options2)) {
                                        $selected_available_options2 = explode(',', $data->available_options2);
                                    }
                                @endphp
                                <div>
                                    <label>
                                        <input type="checkbox" name="available_options2[1]" id="available_options2_1" value="1" @if(isset($data->id) && in_array(1, $selected_available_options2)) checked="checked" @endif> Crown (Price for 1 tooth)
                                    </label>
                                </div>
                                <div>
                                    <label>
                                        <input type="checkbox" name="available_options2[2]" id="available_options2_2" value="2" @if(isset($data->id) && in_array(2, $selected_available_options2)) checked="checked" @endif> Bridge (Price for 1 tooth)
                                    </label>
                                </div>                               
                                <div>
                                    <label>
                                        <input type="checkbox" name="available_options2[3]" id="available_options2_3" value="3" @if(isset($data->id) && in_array(3, $selected_available_options2)) checked="checked" @endif> Full denture (Price for all selected)
                                    </label>
                                </div>
                                <div>
                                    <label>
                                        <input type="checkbox" name="available_options2[4]" id="available_options2_4" value="4" @if(isset($data->id) && in_array(4, $selected_available_options2)) checked="checked" @endif> Partial denture (Flipper, Partial prosthetics - Price for all selected)
                                    </label>
                                </div> 
                                <div>
                                    <label>
                                        <input type="checkbox" name="available_options2[5]" id="available_options2_5" value="5" @if(isset($data->id) && in_array(5, $selected_available_options2)) checked="checked" @endif> Partial denture (Upper or Lower - Price for upper or lower selected)
                                    </label>
                                </div> 
                            </div>
                            <div class="form-group" style="display:none;">
                                <label for="industry_id">Industry</label>
                                <!--<select class="form-control" id="industry_id" name="industry_id">
                                    @foreach($industries as $industry) 
                                        <option value="{{ $industry->id }}" @if(isset($data->id) && $data->industry_id == $industry->id) selected @endif>{{ $industry->name }}</option>
                                    @endforeach                 
                                </select> -->
                                <input type="hidden" name="industry_id" value="2"/>
                            </div>
                            <div class="form-group">
                                <label for="parent_id">Parent (leave empty if main)</label>
                                <select class="form-control" id="parent_id" name="parent_id">        
                                    <option value="0" @if(isset($data->parent_id) && $data->parent_id == 0) selected @endif>---- </option>
                                    @foreach($parents as $parent) 
                                        @php if (isset($data->id) && $data->id == $parent->id) continue; @endphp
                                        <option value="{{ $parent->id }}" @if(isset($data->id) && $data->parent_id == $parent->id) selected @endif>{{ $parent->name }}</option>
                                    @endforeach                 
                                </select>
                            </div>  
                            <div class="form-group">
                                <label for="parent_id">Service options</label>
                                <div id="options_list">
                                    @if (count($options))
                                        @foreach($options as $option)
                                            <div>
                                                <label>
                                                    <input type="checkbox" name="options[{{ $option->id }}]" id="option{{ $option->id }}" @if(isset($data->id) && isset($option->selected->id)) checked="checked" @endif> {{ $option->name }}
                                                </label>
                                            </div>
                                        @endforeach
                                    @else
                                        <p>There are no any available options for selected industry!</p>
                                    @endif
                                </div>
                            </div>                         
                        </div>
                    </div>
                </div>                
            </div>

            <button type="submit" class="btn btn-primary pull-right save">
                {{ __('voyager::generic.save') }}
            </button>
        </form>

       
    </div>
@stop

@section('javascript')
    <script>
        $('document').ready(function () {
            $('.toggleswitch').bootstrapToggle();

            $('#industry_id').on('change', function(){
                $.ajax({
                    url: '{{ route('voyager.getservices') }}',
                    type: 'post',
                    dataType: 'json',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    data:{industry: $(this).val()},
                    success: function(data) {
                        html = '<option value="0">---- </option>';

                        if (data) {
                            for (index in data) {
                                html += '<option value="'+data[index]['id']+'">'+data[index]['name']+'</option>';
                            }

                            $('#parent_id').html(html);                           
                        }                            
                    },
                    error: function(){
                        console.log("ajax_error");
                    }
                });

                $.ajax({
                    url: '{{ route('voyager.getoptions') }}',
                    type: 'post',
                    dataType: 'json',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    data:{industry: $(this).val()},
                    success: function(data) {
                        html = '<p>There are no any available options for selected industry!</p>';

                        if (data && data[0]) {
                            html = '';

                            for (index in data) {
                                html += '<div>';
                                html += '<label>';
                                html += '<input type="checkbox" name="options['+data[index]['id']+']" id="option'+data[index]['id']+'"> '+data[index]['name']+'';
                                html += '</label>';
                                html += '</div>';
                            }                        
                        } 

                        $('#options_list').html(html);                            
                    },
                    error: function(){
                        console.log("ajax_error");
                    }
                });
            });
        });
    </script>
@stop
