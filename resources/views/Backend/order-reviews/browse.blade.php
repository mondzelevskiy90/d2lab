@extends('voyager::master')

@section('page_title', __('voyager::generic.viewing').' '.ucfirst($type))

@section('page_header')
    <h1 class="page-title">
        <i class="voyager-list-add"></i> {{ ucfirst($type) }}        
    </h1>
@stop
 
@section('content') 
    <div class="page-content container-fluid">
        @include('voyager::alerts')
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-body">
                        <form method="get" action="{{ route('voyager.'.$type.'.index', $requests) }}">
                            <div class="col-sm-10">
                                <div class="row">                      
                                    <div class="col-sm-12">  
                                        <div class="col-md-3">
                                            <select onchange="$(this).parents('form').submit()" class="form-control" name="moderation">
                                                <option value="0" default>Moderation</option>
                                                <option value="1" @if(isset($filter['filter_moderation']) && $filter['filter_moderation'] == 1) selected @endif>Not need</option>
                                                <option value="2" @if(isset($filter['filter_moderation']) && $filter['filter_moderation'] == 2) selected @endif>Need</option>           
                                            </select>
                                        </div>                                      
                                        <div class="col-md-3">
                                            <select onchange="$(this).parents('form').submit()" class="form-control" name="status">
                                                <option value="0" default>Select status</option>
                                                <option value="1" @if(isset($filter['filter_status']) && $filter['filter_status'] == 1) selected @endif>Actice</option>
                                                <option value="2" @if(isset($filter['filter_status']) && $filter['filter_status'] == 2) selected @endif>Disactice</option>           
                                            </select>
                                        </div>
                                        <div class="col-md-3">
                                            <select onchange="$(this).parents('form').submit()" class="form-control" id="branch" name="branch">                                                
                                                @if(isset($filter['filter_branch']) && $filter['filter_branch'] && $branch) 
                                                    <option value="{{ $branch->id }}" selected>{{ $branch->name }}</option>
                                                @else
                                                    <option value="0">Location</option>
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                                        
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <input type="submit" style="margin:0;" class="btn btn-success" value="OK">
                                <a class="btn btn-danger" style="margin:0;" href="{{ route('voyager.'.$type.'.index') }}">Clear</a>
                            </div>
                        </form>
                        <table id="dataTable" class="table table-hover">
                            <thead>
                            <tr>                                
                                <th>ID</th>     
                                <th>Location</th>
                                <th>Moderation</th>                               
                                <th>Status</th>                          
                                <th class="actions text-right">{{ __('voyager::generic.actions') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($many as $one)
                                <tr style="@if($one->moderation == 2){{ 'color:#000;background-color:rgba(255, 0, 0, 0.35);' }}@endif">
                                    <td>Review for Order #{{ $one->order_id }} ({{ $one->user->name.' '.$one->user->surname }})</td>
                                    <td>
                                        @if (isset($one->branch))
                                            {{ $one->branch->name }}
                                        @endif
                                    </td>
                                    <td>
                                        @php
                                            if ($one->moderation == 1) echo 'No';
                                            if ($one->moderation == 2) echo 'Yes';
                                        @endphp
                                    </td>
                                    <td>
                                    	@php
                                    		if ($one->status == 1) echo 'Actice';
                                    		if ($one->status == 2) echo 'Disactice';
                                    	@endphp
                                    </td>
                                    <td class="no-sort no-click bread-actions">
                                        @can('edit', $model)
                                            <a href="{{ route('voyager.'.$type.'.edit', [$one->id, 'type' => $one->type_str, $requests]) }}" class="btn btn-sm btn-primary pull-right edit">
                                                <i class="voyager-edit"></i> {{ __('voyager::generic.edit') }}
                                            </a>
                                        @endcan                                        
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="panel-body">	                    
                        <div class="pull-right">{{ $many->appends(\Request::except('page'))->links() }}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('javascript') 
    <script src="/js/select.js"></script>  
    <script> 
        $('#branch').select2({
            minimumInputLength: 3,
            ajax: {
                url: '{{ route("voyager.getbranches") }}',
                type: 'post',
                dataType: 'json',
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                },
                data:function(params) {
                  return {
                    search: params.term,
                  };
                },
                processResults: function (data) {
                    if (data) {
                        return {
                            results: data
                        }; 
                    }
                }
            }
        });
    </script>
@stop