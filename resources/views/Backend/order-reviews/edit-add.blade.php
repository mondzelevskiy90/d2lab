@extends('voyager::master')

@section('page_title', __('voyager::generic.'.(isset($data->id) ? 'edit' : 'add')).' review')

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('page_header')
    <h1 class="page-title">
        <i class="voyager-edit"></i>
        {{ __('voyager::generic.'.(isset($data->id) ? 'edit' : 'add')).' review' }}
    </h1>
@stop

@section('content')
    <div class="page-content container-fluid">
        <form class="form-edit-add" role="form" action="@if(!is_null($data->id)){{ route('voyager.'.$type.'.update', [$data->id, $requests]) }}@else{{ route('voyager.'.$type.'.store', [$requests]) }}@endif" method="POST" enctype="multipart/form-data" autocomplete="off">            
            @if(isset($data->id))
                {{ method_field("PUT") }}
            @endif
            {{ csrf_field() }}

            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-bordered">                   
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <div class="panel-body">
                            <input type="hidden" name="type_str" id="type_str" value="{{ $data->type_str }}">
                            <div class="form-group">
                                <label for="status">Status</label>
                                <select class="form-control" name="status">                                    
                                    <option value="1" @if(isset($data->status) && $data->status == 1) selected @endif>Active</option>
                                    <option value="2" @if(!isset($data->status) || $data->status == 2) selected @endif>disactive</option>   
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="moderation">Moderation</label>
                                <select class="form-control" name="moderation">                                    
                                    <option value="1" @if(isset($data->moderation) && $data->moderation == 1) selected @endif>Not need</option>
                                    <option value="2" @if(!isset($data->moderation) || $data->moderation == 2) selected @endif>Need</option>   
                                </select>
                            </div>
                            @if (isset($data->moderation_text) && $data->moderation_text)
                                <div class="form-group">
                                    <label for="review">Moderation reason</label>
                                    <p>{{ $data->moderation_text }}</p>
                                </div>
                            @endif
                            <div class="form-group">
                                <label for="review">User</label>
                                <p><a href="{{ route('voyager.users.edit', $data->user_id) }}">{{ $data->user->name.' '.$data->user->surname }}</a></p>
                            </div>
                            <div class="form-group">
                                <label for="review">Review</label>
                                <p><strong>{{ $data->review }}</strong></p>
                            </div>
                        </div>
                    </div>
                </div>                
            </div>

            <button type="submit" class="btn btn-primary pull-right save">
                {{ __('voyager::generic.save') }}
            </button>
        </form>
    </div>
@stop

@section('javascript')
    <script>
        $('document').ready(function () {
            $('.toggleswitch').bootstrapToggle();
        });
    </script>
@stop
