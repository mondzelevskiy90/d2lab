@extends('voyager::master')

@section('page_title', __('voyager::generic.'.(isset($data->id) ? 'edit' : 'add')).' '.$data->name)

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('page_header')
    <h1 class="page-title">
        <i class="voyager-edit"></i>
        {{ __('voyager::generic.'.(isset($data->id) ? 'edit' : 'add')).' '.$data->name }}
    </h1>
@stop

@section('content')
    <div class="page-content container-fluid">
        <form class="form-edit-add" role="form" action="@if(!is_null($data->id)){{ route('voyager.'.$type.'.update', [$data->id, $requests]) }}@else{{ route('voyager.'.$type.'.store', [$requests]) }}@endif" method="POST" enctype="multipart/form-data" autocomplete="off">            
            @if(isset($data->id))
                {{ method_field("PUT") }}
            @endif
            {{ csrf_field() }}

            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-bordered">  
                        <div class="panel-body">                 
                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif                                                    
                            <div class="form-group">
                                <label for="status">{{ __('adm.status') }}</label>
                                <select class="form-control" name="status">                                    
                                    @foreach ($statuses as $status)
                                        <option value="{{ $status['id'] }}" @if(!is_null($data->id) && $status['id'] == $data->status) selected @endif>{{ $status['name'] }}</option>
                                    @endforeach    
                                </select>
                            </div> 
                            <div class="form-group">
                                <label for="type">{{ __('voyager::generic.type') }}</label>
                                @if(!is_null($data->id))
                                    @foreach ($types as $type_item)
                                        @if ($type_item['id'] == $data->type)
                                            <input type="text" class="form-control" id="type" value="{{ $type_item['name'] }}" disabled="disabled">
                                            <input type="hidden" name="type" value="{{ $data->type }}">
                                        @endif
                                    @endforeach
                                @else
                                    <select class="form-control" name="type">
                                        @foreach ($types as $type_item)
                                            <option value="{{ $type_item['id'] }}">{{ $type_item['name'] }}</option>
                                        @endforeach
                                    </select>
                                @endif   
                            </div>
                             <div class="form-group">
                                <label for="role_id">{{ __('adm.role') }}</label>
                                <select class="form-control" name="role_id">                                    
                                    @foreach ($roles as $role)
                                        <option value="{{ $role['id'] }}" @if(!is_null($data->id) && $role['id'] == $data->role_id) selected @endif>{{ $role['display_name'] }}</option>
                                    @endforeach    
                                </select>
                            </div> 
                            <div class="form-group">
                                <label for="name">{{ __('adm.name') }}</label>
                                <input type="text" class="form-control" id="name" name="name" placeholder="{{ __('adm.name') }}" value="{{ old('name', $data->name ?? '') }}">
                            </div> 
                            <div class="form-group">
                                <label for="surname">{{ __('adm.surname') }}</label>
                                <input type="text" class="form-control" id="surname" name="surname" placeholder="{{ __('adm.surname') }}" value="{{ old('surname', $data->surname ?? '') }}">
                            </div>
                            <div class="form-group">
                                <label for="email">{{ __('adm.email') }}</label>
                                @if(!is_null($data->id))
                                    <input type="text" class="form-control" id="email" placeholder="{{ __('adm.email') }}" value="{{ $data->email ?? '' }}" disabled="disabled">
                                    <input type="hidden" name="email" value="{{ $data->email }}">
                                @else
                                    <input type="text" class="form-control" name="email" id="email" placeholder="{{ __('adm.email') }}" value="{{ $data->email ?? '' }}">
                                @endif
                            </div> 
                            <div class="form-group">
                                <label for="phone">{{ __('adm.phone') }}</label>
                                <input type="text" class="form-control" id="phone" name="phone" placeholder="{{ __('adm.phone') }}" value="{{ old('phone', $data->phone ?? '') }}">
                            </div>
                            @if(!is_null($data->id))
                                <div class="form-group">
                                    <label for="address">{{ __('adm.address') }}</label>
                                    <input type="text" class="form-control" id="address" value="{{ $address }}" disabled="disabled">
                                </div>  
                            @endif                            
                            <div class="form-group">
                                <label for="company">{{ __('adm.company_name') }}</label>
                                <input type="text" class="form-control" name="company" id="company" placeholder="{{ __('adm.company_name') }}" value="{{ old('company', $data->company ?? '') }}">
                            </div>
                            <div class="form-group">
                                <label for="ip">Last visit from IP</label>
                                <input type="text" class="form-control" id="ip" value="{{ $data->ip ?? '' }}" disabled="disabled">
                            </div>
                            <div class="form-group">
                                <label for="avatar">{{ __('adm.image') }}</label>
                                @if(isset($data->avatar) && $data->avatar != '')
                                    <img src="{{ filter_var($data->avatar, FILTER_VALIDATE_URL) ? $data->avatar : Voyager::image( $data->avatar ) }}" style="width:50px; height:auto; clear:both; display:block; padding:2px; border:1px solid #ddd; border-radius:50%;margin-bottom:10px;" />
                                @endif
                                <input type="file" data-name="avatar" name="avatar" id="avatar">
                            </div>
                        </div>
                    </div>
                </div>                
            </div>

            <button type="submit" class="btn btn-primary pull-right save">
                {{ __('voyager::generic.save') }}
            </button>
        </form>

        <iframe id="form_target" name="form_target" style="display:none"></iframe>
        <form id="my_form" action="{{ route('voyager.upload') }}" target="form_target" method="post" enctype="multipart/form-data" style="width:0px;height:0;overflow:hidden">
            {{ csrf_field() }}
            <input name="image" id="upload_file" type="file" onchange="$('#my_form').submit();this.value='';">
            <input type="hidden" name="type_slug" id="type_slug" value="{{ $type }}">
        </form>
    </div>
@stop

@section('javascript')
    <script>
        $('document').ready(function () {
            $('.toggleswitch').bootstrapToggle();
        });
    </script>
@stop
