@extends('voyager::master')

@section('page_title', __('voyager::generic.viewing').' '.ucfirst($type))

@section('page_header')
    <h1 class="page-title">
        <i class="voyager-list-add"></i> {{ ucfirst($type) }}        
    </h1>
@stop
 
@section('content') 
    <div class="page-content container-fluid">
        @include('voyager::alerts')
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-body">
                        <form method="get" action="{{ route('voyager.'.$type.'.index', $requests) }}">
                            <div class="col-sm-10">
                                <div class="row">                      
                                    <div class="col-sm-12">                                        
                                        <div class="col-md-3">
                                            <select onchange="$(this).parents('form').submit()" class="form-control" name="status">
                                                <option value="0" default>{{ __('adm.select_status') }}</option>
                                                @foreach($statuses as $status)
                                                    <option value="{{ $status['id'] }}" @if(isset($filter['filter_status']) && $filter['filter_status'] == $status['id']) selected @endif>{{ $status['name'] }}</option>       
                                                @endforeach    
                                            </select>
                                        </div>
                                        <div class="col-md-3">
                                            <select onchange="$(this).parents('form').submit()" class="form-control" name="type">
                                                <option value="0" default>{{ __('voyager::generic.type') }}</option>
                                                @foreach($types as $type_item)
                                                    <option value="{{ $type_item['id'] }}" @if(isset($filter['filter_type']) && $filter['filter_type'] == $type_item['id']) selected @endif>{{ $type_item['name'] }}</option>       
                                                @endforeach    
                                            </select>
                                        </div>
                                        <div class="col-md-3">
                                            <select onchange="$(this).parents('form').submit()" class="form-control" name="role_id">
                                                <option value="0" default>{{ __('adm.role') }}</option>
                                                @foreach($roles as $role)
                                                    <option value="{{ $role['id'] }}" @if(isset($filter['filter_role']) && $filter['filter_role'] == $role['id']) selected @endif>{{ $role['display_name'] }}</option>       
                                                @endforeach    
                                            </select>
                                        </div>                                         
                                        <div class="col-md-3">
                                            <select onchange="$(this).parents('form').submit()" class="form-control" id="branch_id" name="branch_id">                                                
                                                @if(isset($filter['filter_branch']) && $filter['filter_branch'] && $branch) 
                                                    <option value="{{ $branch->id }}" selected>{{ $branch->name }}</option>
                                                @else
                                                    <option value="0">Department</option>
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="col-md-3">
                                            <input type="text" class="form-control" name="search" value="@if(isset($filter['filter_search']) && $filter['filter_search'] != ''){{ $filter['filter_search'] }}@endif" placeholder="{{ __('voyager::generic.search') }}">
                                        </div>              
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <input type="submit" style="margin:0;" class="btn btn-success" value="OK">
                                <a class="btn btn-danger" style="margin:0;" href="{{ route('voyager.'.$type.'.index') }}">Clear</a>
                            </div>
                        </form>
                        <table id="dataTable" class="table table-hover">
                            <thead>
                            <tr>                                
                                <th>ID</th> 
                                <th>Name</th> 
                                <th>E-mail</th>
                                <th>Type</th>
                                <th>Role</th>
                                <th>Image</th>
                                <th>Last visit from IP</th>
                                <th>Status</th>                              
                                <th class="actions text-right">{{ __('voyager::generic.actions') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($many as $one)
                                <tr style="font-weight:400;color:#000;background-color:{{ $one->status_info['color'] }}">
                                    <td>{{ $one->id }}</td>
                                    <td>{{ $one->name.' '.$one->surname }}</td>
                                    <td>{{ $one->email }}</td>
                                    <td>{{ $one->type_info['name'] }}</td>
                                    <td>{{ $one->role_info['display_name'] }}</td>
                                    <td>
                                        <img src="{{ filter_var($one->avatar, FILTER_VALIDATE_URL) ? $one->avatar : Voyager::image( $one->avatar ) }}" style="width:70px; height:auto; clear:both; display:block; padding:2px;border-radius: 50%;" />
                                    </td>
                                    <td>{{ $one->ip }}</td>
                                    <td><strong>{{ $one->status_info['name'] }}</strong></td>
                                    <td class="no-sort no-click bread-actions">
                                        @can('delete', $model)
                                            <div class="btn btn-sm btn-danger pull-right delete" data-id="{{ $one->id }}">
                                                <i class="voyager-trash"></i> {{ __('voyager::generic.delete') }}
                                            </div>
                                        @endcan
                                        @can('edit', $model)
                                            <a href="{{ route('voyager.'.$type.'.edit', [$one->id, $requests]) }}" class="btn btn-sm btn-primary pull-right edit">
                                                <i class="voyager-edit"></i> {{ __('voyager::generic.edit') }}
                                            </a>
                                        @endcan                                        
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="panel-body">                        
                        <div class="pull-right">{{ $many->appends(\Request::except('page'))->links() }}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal modal-danger fade" tabindex="-1" id="delete_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="{{ __('voyager::generic.close') }}">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">
                        <i class="voyager-trash"></i> {{ __('voyager::generic.delete_question') }}?
                    </h4>
                </div>
                <div class="modal-footer">
                    <form action="#" id="delete_form" method="POST">
                        {{ method_field("DELETE") }}
                        {{ csrf_field() }}                        
                        <div class="form-group" style="text-align: left;">
                            <label for="delete_text">Type 'DELETE' to the field under this text!</label>
                            <input type="text" class="form-control" id="delete_text" value="">                            
                        </div>
                        <input type="submit" class="btn btn-danger pull-right delete-confirm" value="{{ __('voyager::generic.delete_this_confirm') }}" onclick="return document.getElementById('delete_text').value == 'DELETE' ? true : false;">
                    </form>
                    <button type="button" class="btn btn-default pull-right" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                </div>
            </div>
        </div>
    </div>
@stop

@section('javascript') 
    <script src="/js/select.js"></script>  
    <script> 
        $('td').on('click', '.delete', function (e) {
            $('#delete_form')[0].action = '{{ route('voyager.'.$type.'.destroy', ['id' => '__item', $requests]) }}'.replace('__item', $(this).data('id'));

            $('#delete_modal').modal('show');
        });

        $('#branch_id').select2({
            minimumInputLength: 3,
            ajax: {
                url: '{{ route("voyager.getbranches") }}',
                type: 'post',
                dataType: 'json',
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                },
                data:function(params) {
                  return {
                    search: params.term,
                  };
                },
                processResults: function (data) {
                    if (data) {
                        return {
                            results: data
                        }; 
                    }
                }
            }
        });
    </script>
@stop
