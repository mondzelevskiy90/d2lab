@extends('voyager::master')

@section('page_title', __('voyager::generic.'.(isset($data->id) ? 'edit' : 'add')).' '.$data->name)

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('page_header')
    <h1 class="page-title">
        <i class="voyager-edit"></i>
        {{ __('voyager::generic.'.(isset($data->id) ? 'edit' : 'add')).' '.$data->name }}
    </h1>
@stop

@section('content')
    <div class="page-content container-fluid">
        <form class="form-edit-add" role="form" action="@if(!is_null($data->id)){{ route('voyager.'.$type.'.update', [$data->id, $requests]) }}@else{{ route('voyager.'.$type.'.store', [$requests]) }}@endif" method="POST" enctype="multipart/form-data" autocomplete="off">            
            @if(isset($data->id))
                {{ method_field("PUT") }}
            @endif
            {{ csrf_field() }}

            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-bordered">                   
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <div class="panel-body">
                            <div class="form-group">
                                <label for="status">{{ __('adm.status') }}</label>
                                <select class="form-control" name="status">                                    
                                    <option value="1" @if(isset($data->status) && $data->status == 1) selected @endif>{{ __('adm.status_on') }}</option>
                                    <option value="2" @if(!isset($data->status) || $data->status == 2) selected @endif>{{ __('adm.status_off') }}</option>   
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="sort_order">{{ __('adm.sort_order') }}</label>
                                <input type="text" class="form-control" id="sort_order" name="sort_order" placeholder="{{ __('adm.sort_order') }}" value="{{ old('sort_order', $data->sort_order ?? 0) }}">
                            </div>                            
                            <div class="form-group">
                                <label for="image">{{ __('adm.image') }}</label>
                                @if(isset($data->image) && $data->image != '')
                                    <img src="{{ filter_var($data->image, FILTER_VALIDATE_URL) ? $data->image : Voyager::image( $data->image ) }}" style="width:200px; height:auto; clear:both; display:block; padding:2px; border:1px solid #ddd; margin-bottom:10px;" />
                                @endif
                                <input type="file" data-name="image" name="image" id="image">
                            </div>                       
                            <div class="form-group">
                                <label for="slug">Url</label>
                                <input type="text" class="form-control" id="slug" name="slug" placeholder="Url" value="{{ old('slug', $data->slug ?? '') }}">
                            </div>
                        </div>
                        <div class="panel-body">
                            <ul class="nav nav-tabs">
                            @foreach ($content as $key => $value)                             
                                <li @if ($value['language_default'] == 1) class="active" @endif>
                                <a data-toggle="tab" href="#{{ $value['code'] }}">{{ $value['language_name'] }}</a>
                                </li>                                
                            @endforeach
                            </ul>
                            <div class="tab-content">
                            @foreach ($content as $key => $value) 
                                <div class="tab-pane @if ($value['language_default'] == 1) active @endif" id="{{ $value['code'] }}">
                                    <div class="form-group">
                                        <label for="name_{{ $key }}">{{ __('adm.name') }}</label>
                                        <input type="text" class="form-control" id="name_{{ $key }}" name="content[{{ $key }}][name]" placeholder="{{ __('adm.name') }}" value="{{ old('content.'.$key.'.name', $content[$key]['name'] ?? '') }}" required="required">
                                    </div>
                                    <div class="form-group">
                                        <label for="meta_title_{{ $key }}">{{ __('adm.meta_title') }}</label>
                                        <input type="text" class="form-control" id="meta_title_{{ $key }}" name="content[{{ $key }}][meta_title]" placeholder="{{ __('adm.meta_title') }}" value="{{ old('content.'.$key.'.meta_title', $content[$key]['meta_title'] ?? '') }}">
                                    </div>
                                    <div class="form-group">
                                        <label for="meta_description_{{ $key }}">{{ __('adm.meta_description') }}</label>
                                        <input type="text" class="form-control" id="meta_title_{{ $key }}" name="content[{{ $key }}][meta_description]" placeholder="{{ __('adm.meta_description') }}" value="{{ old('content.'.$key.'.meta_description', $content[$key]['meta_description'] ?? '') }}">
                                    </div>
                                    <div class="form-group">
                                        <label for="description_{{ $key }}">{{ __('adm.description') }}</label>
                                        <textarea class="richTextBox form-control" id="description_{{ $key }}" name="content[{{ $key }}][description]" placeholder="{{ __('adm.description') }}">{{ old('content.'.$key.'.description', $content[$key]['description'] ?? '') }}</textarea>
                                    </div>
                                </div>
                            @endforeach 
                            </div>
                        </div>
                    </div>
                </div>                
            </div>

            <button type="submit" class="btn btn-primary pull-right save">
                {{ __('voyager::generic.save') }}
            </button>
        </form>

        <iframe id="form_target" name="form_target" style="display:none"></iframe>
        <form id="my_form" action="{{ route('voyager.upload') }}" target="form_target" method="post" enctype="multipart/form-data" style="width:0px;height:0;overflow:hidden">
            {{ csrf_field() }}
            <input name="image" id="upload_file" type="file" onchange="$('#my_form').submit();this.value='';">
            <input type="hidden" name="type_slug" id="type_slug" value="{{ $type }}">
        </form>
    </div>
@stop

@section('javascript')
    <script>
        $('document').ready(function () {
            $('.toggleswitch').bootstrapToggle();
        });
    </script>
@stop
