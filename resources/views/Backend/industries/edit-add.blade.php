@extends('voyager::master')

@section('page_title', __('voyager::generic.'.(isset($data->id) ? 'edit' : 'add')).' '.$data->name)

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('page_header')
    <h1 class="page-title">
        <i class="voyager-edit"></i>
        {{ __('voyager::generic.'.(isset($data->id) ? 'edit' : 'add')).' '.$data->name }}
    </h1>
@stop

@section('content')
    <div class="page-content container-fluid">
        <form class="form-edit-add" role="form" action="@if(!is_null($data->id)){{ route('voyager.'.$type.'.update', [$data->id, $requests]) }}@else{{ route('voyager.'.$type.'.store', [$requests]) }}@endif" method="POST" enctype="multipart/form-data" autocomplete="off">            
            @if(isset($data->id))
                {{ method_field("PUT") }}
            @endif
            {{ csrf_field() }}

            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-bordered">  
                        <div class="panel-body">                 
                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif                                                    
                            <div class="form-group">
                                <label for="status">{{ __('adm.status') }}</label>
                                <select class="form-control" name="status">        
                                    <option value="1" @if(isset($data->status) && $data->status == 1) selected @endif>Active</option>       
                                    <option value="2" @if(isset($data->status) && $data->status == 2) selected @endif>Disactive</option>                                      
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="name">{{ __('adm.name') }}</label>
                                <input type="text" class="form-control" id="name" name="name" placeholder="{{ __('adm.name') }}" value="{{ old('name', $data->name ?? '') }}">
                            </div>                 
                            <div class="form-group">
                                <label for="sort_order">{{ __('adm.sort_order') }}</label>
                                <input type="text" class="form-control" id="sort_order" name="sort_order" placeholder="{{ __('adm.sort_order') }}" value="{{ old('sort_order', $data->sort_order ?? 0) }}">
                            </div> 
                            <div class="form-group">
                                <label for="parent_id">Parent (leave empty if main)</label>
                                <select class="form-control" name="parent_id">        
                                    <option value="0" @if(isset($data->parent_id) && $data->parent_id == 0) selected @endif>---- </option>
                                    @foreach($parents as $parent) 
                                        <option value="{{ $parent->id }}" @if(isset($data->parent_id) && $data->parent_id == $parent->id) selected @endif>{{ $parent->name }}</option>
                                    @endforeach                 
                                </select>
                            </div>                           
                        </div>
                    </div>
                </div>                
            </div>

            <button type="submit" class="btn btn-primary pull-right save">
                {{ __('voyager::generic.save') }}
            </button>
        </form>

       
    </div>
@stop

@section('javascript')
    <script>
        $('document').ready(function () {
            $('.toggleswitch').bootstrapToggle();
        });
    </script>
@stop
