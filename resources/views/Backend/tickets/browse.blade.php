@extends('voyager::master')

@section('page_title', __('voyager::generic.viewing').' '.ucfirst($type))

@section('page_header')
    <h1 class="page-title">
        <i class="voyager-list-add"></i> {{ ucfirst($type) }}        
    </h1>
@stop
 
@section('content') 
    <div class="page-content container-fluid">
        @include('voyager::alerts')
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-body">
                        <form method="get" action="{{ route('voyager.'.$type.'.index', $requests) }}">
                            <div class="col-sm-10">
                                <div class="row">                      
                                    <div class="col-sm-12">  
                                        <div class="col-md-3">
                                            <select onchange="$(this).parents('form').submit()" class="form-control" name="has_answer">
                                                <option value="0" default>-- Answer status --</option> 
                                                <option value="2" @if(isset($filter['filter_answer']) && $filter['filter_answer'] == 2) selected @endif>Has no answer</option>
                                            </select>
                                        </div>  
                                        <div class="col-md-3">
                                            <select onchange="$(this).parents('form').submit()" class="form-control" id="user_id" name="user_id"> 
                                                <option value="0">-- Select user --</option>  
                                                @foreach ($users as $user)
                                                    <option value="{{ $user->user_id }}" @if(isset($filter['filter_user']) && $filter['filter_user'] && $filter['filter_user'] == $user->user_id){{ 'selected' }}@endif>{{ $user->user->name.' '.$user->user->surname }}</option>
                                                @endforeach 
                                            </select>
                                        </div>                                    
                                        <div class="col-md-3">
                                            <input type="text" class="form-control" name="id" value="@if(isset($filter['filter_id']) && $filter['filter_id'] != ''){{ $filter['filter_id'] }}@endif" placeholder="ID...">
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <input type="submit" style="margin:0;" class="btn btn-success" value="OK">
                                <a class="btn btn-danger" style="margin:0;" href="{{ route('voyager.'.$type.'.index') }}">Clear</a>
                            </div>
                        </form>
                        <table id="dataTable" class="table table-hover">
                            <thead>
                            <tr>                                
                                <th>ID</th>     
                                <th>User</th>
                                <th>Text</th>                               
                                <th>Status</th>                          
                                <th class="actions text-right">{{ __('voyager::generic.actions') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($many as $one)
                                <tr style="@if(!$one->answer){{ 'color:#000;background-color:rgba(255, 0, 0, 0.35);' }}@endif">
                                    <td>#{{ $one->id }}</td>
                                    <td>
                                        @if ($one->user)
                                            {{ $one->user->name.' '.$one->user->surname }}
                                        @endif
                                    </td>
                                    <td>
                                        {{ $one->question }}
                                    </td>
                                    <td>
                                    	@php
                                    		if ($one->answer) {
                                                echo 'Has answer';
                                            } else {
                                                echo 'Has no answer!';
                                            }
                                    	@endphp
                                    </td>
                                    <td class="no-sort no-click bread-actions">
                                        @can('edit', $model)
                                            <a href="{{ route('voyager.'.$type.'.edit', [$one->id, $requests]) }}" class="btn btn-sm btn-primary pull-right edit">
                                                <i class="voyager-edit"></i> {{ __('voyager::generic.edit') }}
                                            </a>
                                        @endcan                                        
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="panel-body">	                    
                        <div class="pull-right">{{ $many->appends(\Request::except('page'))->links() }}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('javascript') 
    <script src="/js/select.js"></script>  
    <script> 
        $('#user_id').select2({
            minimumInputLength: -1,            
        });
    </script>
@stop