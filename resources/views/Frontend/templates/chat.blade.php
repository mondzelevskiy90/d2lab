@if ($data->id)
	<link rel="stylesheet" href="{{ mix('/css/chat.css') }}">
	<div class="chat-block">
		<div class="chat-body">
			<div id="chat_content" class="chat-content">
				
			</div>
			<div id="chat_controls" class="chat-controls">
				<div class="fx fac chat-controls-inputs">
					<textarea id="chat_message" maxlength="1000" placeholder="{{ __('translations.chat_message') }}"></textarea>
					<div id="chat_add_file" class="bi chat-add-file" title="{{ __('translations.chat_add_file') }}"></div>
					<input type="file" id="chat_file" onchange="document.getElementById('chat_add_file').classList.add('chat-delete-file')" value="">
				</div>
				<div class="chat-controls-btns">
					<div id="chat_save" class="btn btn-small" onclick="saveChat()">{{ __('translations.send_chat_message') }}</div>
					<div class="btn btn-small btn-primary" onclick="getChat()">Refresh</div>
				</div>
			</div>
		</div>
	</div>
	<script>
		var scroll = false;

		document.addEventListener('DOMContentLoaded', function(){
			getChat();

			setInterval('getChat()', 4000);

			$(document).on('click', '.chat-add-file', function(){
				if ($(this).hasClass('chat-delete-file')) {
					$(this).removeClass('chat-delete-file');
					$('#chat_save').removeClass('chat-save-file');
					$('#chat_file').val('');
				} else {
					$('#chat_file').click();
					$('#chat_save').addClass('chat-save-file');
				}
			});

			$(document).on('keypress',function(e) {
			    if(e.which == 13) {
			        $('#chat_save').click();
			    }
			});
			
			

			$('#chat_content').on('scroll', function(){
				scroll = true;

				var content = $('#chat_content')[0];

				if (content.scrollTop + 400 >= content.scrollHeight) scroll = false;
			});

			var viewed = false;

			$(document).on('scroll', function(){
				var content = document.getElementById('chat_content').getBoundingClientRect().top;
				
				if (window.scrollY >= content && !viewed) {					
					viewed = true;

					$.ajax({
						url: '/ajax/getData',
						type: 'post',
			            dataType: 'json',
			            headers: {
			                'X-CSRF-TOKEN': '{{ csrf_token() }}'
			            },
					    data: {action:'addChatView', order_id: '{{ $data->id }}'},
					    success: function (data) {
						}		
					});	
						
				}
			});
		}, !1);

		function getChat() {
			$.ajax({
				url: '/ajax/getData',
				type: 'post',
	            dataType: 'json',
	            headers: {
	                'X-CSRF-TOKEN': '{{ csrf_token() }}'
	            },
			    data: {action:'getChat', order_id: '{{ $data->id }}'},
			    success: function (data) {
			    	if (data) {
			    		var html = '';

			    		data.forEach(function(item){
			    			if (item.user) {
			    				if(!isIOSDevice()) {
				    				var date = new Date(item.created_at);
				    				var day = new Intl.DateTimeFormat('en', { day: '2-digit' }).format(date);
				    				var month = new Intl.DateTimeFormat('en', { month: '2-digit' }).format(date);
				    				var year = new Intl.DateTimeFormat('en', { year: 'numeric' }).format(date);
			    				}

				    			html += '<div class="fx chat-content-line '+(item.user.id == {{ Auth::user()->id  }} ? 'chat-content-lineR' : '')+'">';
				    			html += '<div class="chat-content-lineBody">';				    			
				    			html += '<div class="chat-content-name">'+item.user.name+' '+item.user.surname+'</div>';

				    			if(!isIOSDevice()) {
					    			html += '<div class="chat-content-date">'+month+'-'+day+'-'+year+'</div>';
					    		}

				    			html += '<div class="chat-content-text">'+item.message+'</div>';

				    			if (item.file) {
				    				html += '<div class="chat-content-img">';

				    				if (item.file.slice(-4) == '.jpg' || item.file.slice(-4) == '.png') {
					    				html += '<a href="{{ url('/').'/storage/' }}'+item.file+'" class="popup-link" rel="colorbox">';
					    				html += '<img src="{{ url('/').'/storage/' }}'+item.file+'" width="50px" height="auto">';
					    				html += '</a>';
					    			} else {
					    				html += '<a href="{{ url('/').'/storage/' }}'+item.file+'">File link</a>';
					    			}

				    				html += '</div>';
				    			}

				    			html += '</div>';
				    			html += '</div>';
				    		}
			    		});
			    	
			    		$('#chat_content').html(html);

			    		scrollChat();
			    	} 

			    	$('.popup-link').colorbox({
						overlayClose: true,
						rel: "colorbox",
				        maxWidth:'95%', 
				        maxHeight:'auto'
					});		    	
			    }
			});
		}

		function saveChat() {
			if ($('#chat_message').val() == '' && !$('.chat-add-file').hasClass('chat-delete-file')) {
				showPopup('{{ __('translations.chat_message_empty') }}');
				return false;
			}

			var files = false;

			if ($('#chat_file').val() != '') {	
				var files = $('#chat_file').prop('files')[0];					

				if (files.type && files.size > 50000000) {
					showPopup('{{ __('translations.portfolio_file_error') }}');

					return false;
				}
			}

    		var form_data = new FormData();
    		
    		form_data.append('action', 'saveChat');
    		form_data.append('order_id', '{{ $data->id }}');
    		form_data.append('message', $('#chat_message').val() != '' ? $('#chat_message').val() : '.');
    		form_data.append('file', files);

			$.ajax({
				url: '/ajax/getData',
				type: 'post',
	            dataType: 'json',
	            headers: {
	                'X-CSRF-TOKEN': '{{ csrf_token() }}'
	            },
	            processData: false,
	            contentType: false,
            	data: form_data,
			    success: function (data) {
					getChat();
					$('#chat_file').val('');
					$('#chat_add_file').removeClass('chat-delete-file');
					$('#chat_message').val('');
			    }
			});
		}

		function scrollChat() {
			if (!scroll) {
				var content = $('#chat_content')[0];

				content.scrollTop = content.scrollHeight;
			}
		}

		function isIOSDevice(){
		   return !!navigator.platform && /iPad|iPhone|iPod/.test(navigator.platform);
		}
	</script>
@else
	<p>Chat error!!!</p>
@endif