<div class="breadcrumbs">
	<div class="content">
		<ul>
			@php $last_crumb = array_pop($breadcrumbs); @endphp
			@foreach ($breadcrumbs as $breadcrumb)
				<li><a href="{{ $breadcrumb['href'] }}">{{ $breadcrumb['text'] }}</a></li>
			@endforeach
			<li>{{ $last_crumb['text'] }}</li>
		</ul>
	</div>
</div>