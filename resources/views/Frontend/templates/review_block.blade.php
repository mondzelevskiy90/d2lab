<link rel="stylesheet" href="{{ mix('/css/reviews.css') }}">
<div id="order_reviews_block">
	@foreach($data->reviews as $review)
		@if ($review->status == 1)
			<div id="order_review_{{ $review->id }}" class="fx fxb order-review-line">
				<div class="fx fxc order-review-left">
					<div class="bi order-review-avatar" style="background-image: url('{{ \App\Services\Img::getIcon($review->user->avatar) }}');"></div>
				</div>
				<div class="order-review-right">
					<div class="fx fac order-review-top">
						<div class="order-review-user">{{ $review->user->name .' '. $review->user->surname }}</div>
						<div class="order-review-date">
							{{ date('m-d-Y', strtotime($review->created_at)) }}
						</div>
						@if ($review->user_id == Auth::user()->id && $data->review_access && $data->review_access->can_edit)
							<div class="fx review-rating-stars">
								@php $show_star = true; @endphp
								@for ($i = 1; $i <= 5; $i++)
								    <input type="radio" name="review_star{{ $review->id }}" id="review_rating_{{ $review->id }}_{{ $i }}" class="review-rating-radio review-rating{{ $review->id }} @if ($show_star){{ 'review-rating-radioAct' }}@endif" value="{{ $i }}" onchange="showReviewStars({{ $i }}, {{ $review->id}})" title="{{ $i }}" @if ($review->rating == $i){{ 'checked' }}@endif>
								    @php if ($review->rating == $i) $show_star = false; @endphp
								@endfor							
							</div>
						@else
							<div class="order-review-rating order-review-rating{{ $review->rating }}"></div>
						@endif
					</div>
					<div class="order-review-center">
						@if ($review->user_id == Auth::user()->id && $data->review_access && $data->review_access->can_edit)
							<textarea id="order_review{{ $review->id }}" class="form-input form-input-required order-review-edit" value="" placeholder="{{ __('translations.review') }}" maxlength="1000" disabled>{{ $review->review }}</textarea>
						@else
							{{ $review->review }}
						@endif
					</div>
					<div class="fx fxb order-review-bottom">
						<div class="order-review-edited">
							@if($review->updated_at > $review->created_at)
								{{ __('translations.edited_at')  }}: {{ date('m-d-Y', strtotime($review->updated_at)) }}
							@endif
						</div>
						<div class="order-review-btns">
							@if ($review->user_id == Auth::user()->id && $data->review_access && $data->review_access->can_edit)
								<div class="btn btn-small" onclick="editReview({{ $review->id }}, 'order_review')">{{ __('translations.edit') }}</div>
							@elseif ($review->moderation == 2)
								<div>{{ __('translations.review_need_moderation') }}</div>
							@else 
								<div class="tooltip-block review-claim">
									<span class="tooltip-icon"></span>
									<div class="content-tooltip">
										<div onclick="setReviewToModeration({{ $review->id }}, '{{ __('translations.claim1') }}','order_review')">{{ __('translations.claim1') }}</div>
										<div onclick="setReviewToModeration({{ $review->id }}, '{{ __('translations.claim2') }}','order_review')">{{ __('translations.claim2') }}</div>
										<div onclick="setReviewToModeration({{ $review->id }}, '{{ __('translations.claim3') }}','order_review')">{{ __('translations.claim3') }}</div>
										<div onclick="setReviewToModeration({{ $review->id }}, '{{ __('translations.claim4') }}','order_review')">{{ __('translations.claim4') }}</div>
										<div onclick="setReviewToModeration({{ $review->id }}, '{{ __('translations.claim5') }}','order_review')">{{ __('translations.claim5') }}</div>
									</div>
								</div>
							@endif
						</div>
					</div>
				</div>
				<div id="order_comments_{{ $review->id }}" class="order-review-comments">
					@if ($review->comments && count($review->comments))
						@foreach($review->comments as $comment)
							<div id="order_review_{{ $review->id }}_{{ $comment->id }}" class="fx fxb order-review-comment-line">
								<div class="fx fxc order-review-left">
									<div class="bi order-review-avatar" style="background-image: url('{{ \App\Services\Img::getIcon($comment->user->avatar) }}');"></div>
								</div>
								<div class="order-review-right">
									<div class="fx fac order-review-top">
										<div class="order-review-user">{{ $comment->user->name .' '. $comment->user->lastname }}</div>
										<div class="order-review-date">
											{{ date('m-d-Y', strtotime($comment->created_at)) }}
										</div>
									</div>
									<div class="order-review-center">
										@if ($comment->user_id == Auth::user()->id && $data->review_access && $data->review_access->can_edit)
											<textarea id="order_review_comment{{ $comment->id }}" class="form-input form-input-required" value="" placeholder="{{ __('translations.comment') }}" maxlength="1000">{{ $comment->review }}</textarea>
										@else
											{{ $comment->review }}
										@endif
									</div>
									<div class="fx fxb order-review-bottom">
										<div class="order-review-edited">
											@if($comment->updated_at > $comment->created_at)
												{{ __('translations.edited_at')  }}: {{ date('m-d-Y', strtotime($comment->updated_at)) }}
											@endif
										</div>
										<div class="order-review-btns">
											@if ($comment->user_id == Auth::user()->id && $data->review_access && $data->review_access->can_edit)
												<div class="btn btn-small" onclick="editReview({{ $comment->id }}, 'order_review_comment')">{{ __('translations.edit') }}</div>
											@elseif ($comment->moderation == 2)
												<div>{{ __('translations.review_need_moderation') }}</div>
											@else 
												<div class="tooltip-block review-claim">
													<span class="tooltip-icon"></span>
													<div class="content-tooltip">
														<div onclick="setReviewToModeration({{ $review->id }}, '{{ __('translations.claim1') }}','order_review_comment')">{{ __('translations.claim1') }}</div>
														<div onclick="setReviewToModeration({{ $review->id }}, '{{ __('translations.claim2') }}','order_review_comment')">{{ __('translations.claim2') }}</div>
														<div onclick="setReviewToModeration({{ $review->id }}, '{{ __('translations.claim3') }}','order_review_comment')">{{ __('translations.claim3') }}</div>
														<div onclick="setReviewToModeration({{ $review->id }}, '{{ __('translations.claim4') }}','order_review_comment')">{{ __('translations.claim4') }}</div>
														<div onclick="setReviewToModeration({{ $review->id }}, '{{ __('translations.claim5') }}','order_review_comment')">{{ __('translations.claim5') }}</div>
													</div>
												</div>
											@endif
										</div>
									</div>
								</div>
							</div>
						@endforeach
					@endif
				</div>
			</div>

			@if (Auth::user()->type == config('types.lab') && $data->review_access && !in_array($review->id, $data->review_access->cant_comment))
				<div id="order_review_add{{ $review->id }}" class="order-review-add">
					<div class="btn" onclick="addCommentToReview({{ $review->id }}, {{ $data->review_access->order_id }})">{{ __('translations.add_comment_review') }}</div>
				</div>
			@endif	
		@endif
	@endforeach
</div>
@if (Auth::user()->type == config('types.clinic') && $data->review_access && $data->review_access->can_review)
	<div id="order_review_add" class="order-review-add">
		<div class="btn" onclick="addReview({{ $data->review_access->order_id }})">{{ __('translations.add_review') }}</div>
	</div>
@endif