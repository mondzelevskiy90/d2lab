<div class="panel panel-half dashboard-item dashboard-item-half dashboard-item-{{ $code }}">
	<div class="panel-head">{{ __('translations.invoices_data') }}</div>
	@if ($content['text'] && $content['text'] != '')
		<div class="panel-intro">{!! $content['text'] !!}</div>
	@endif
	<div class="panel-content">
		@if ($content['labels'] && $content['total'] && $content['paid'] && $content['unpaid'])
			<div style="width:100%;height:400px;margin: auto">
				<canvas id="graph" width="200" height="400"></canvas>	
			</div>
			<script>
				document.addEventListener('DOMContentLoaded', function(){
					var ctx = document.getElementById('graph');	

					var labels_arr = [],
						total_arr = [],
						paid_arr = [],
						unpaid_arr = [],
						labels = {!! $content['labels'] !!},
						total = {!! $content['total'] !!},
						paid = {!! $content['paid'] !!},
						unpaid = {!! $content['unpaid'] !!};

					for(var i in labels) {
						labels_arr.push(labels[i]);
						total_arr.push(total[i]);
						paid_arr.push(paid[i]);
						unpaid_arr.push(unpaid[i]);						
					}				
					
					var myChart = new Chart(ctx, {
					    type: 'line',
					    data: {
					        labels: labels_arr,
					        datasets: [
					        	{
						            label: 'Total, $',
						            data: total_arr,
						            fill: false,
						            lineTension: 0.1,
						            borderColor: 'rgba(135, 15, 235, 1)',
						            borderWidth: 1
					        	},
					        	{
						            label: 'Paid, $',
						            data: paid_arr,
						            fill: false,
						            lineTension: 0.1,
						            borderColor: 'rgba(25, 249, 64, 1)',
						            borderWidth: 1
					        	},
					        	{
						            label: 'Unpaid, $',
						            data: unpaid_arr,
						            fill: false,
						            lineTension: 0.1,
						            borderColor: 'rgba(205, 30, 5, 1)',
						            borderWidth: 1
					        	}

					        ]
					    },
					    options: {  
						    responsive: true,
						    maintainAspectRatio: false
						}
					});
				}, !1);
			</script>
		@else 
			<p>{{ __('translations.no_invoices_data') }}</p>
		@endif
	</div>
</div>
