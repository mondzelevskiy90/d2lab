<div class="panel dashboard-item dashboard-item-{{ $code }}">
	<div class="panel-head">{{ __('translations.locations_list') }}</div>
	@if ($content['text'] && $content['text'] != '')
		<div class="panel-intro">{!! $content['text'] !!}</div>
	@endif
	<div class="panel-content">
		@if ($content['data'] && count($content['data']))
			<div class="dashboard-locations-list">
				<div class="fx fac fxb dashboard-locations-line dashboard-locations-lineHead">
					<div class="dashboard-locations-logo"></div>
					<div class="dashboard-locations-name">{{ __('translations.branch') }}</div>
					<div class="dashboard-locations-cancelled">
						{{ __('translations.cancelled_orders') }}
					</div>
					<div class="dashboard-locations-procces">
						{{ __('translations.procces_orders') }}
					</div>
					<div class="dashboard-locations-complited">
						{{ __('translations.сomplited_orders') }}
					</div>
					<div class="dashboard-locations-unpaid">{{ __('translations.unpaid_orders') }}</div>
					<div class="dashboard-locations-paid">{{ __('translations.paid_orders') }}</div>
				</div>
				@foreach ($content['data'] as $item)
					<div class="fx fac fxb dashboard-locations-line">
						<div class="fx fxc dashboard-locations-logo"><a class="bi" href="{{ route('branches.edit', $item['id']) }}" style="background-image: url({{ App\Services\Img::getIcon($item['logo']) }})"></a></div>
						<div class="dashboard-locations-name">
							<a href="{{ route('branches.edit', $item['id']) }}" style="font-weight: bold;">{{ $item['name'] }}</a>
							@if (Auth::user()->role_id == config('roles.clinic_head'))
								@php 
									$staff_check_symbol = '<span style="color:red;font-weight:bold;margin-right:5px;">&#8722;</span> <a href="'.route('staff.create').'">'.__('translations.staff').'</a>';

									$staff_check = \App\Models\Branch_staff::where('branch_id', $item['id'])->first();

									if ($staff_check) $staff_check_symbol = '<span style="color:green;font-weight:bold;margin-right:5px;">&#10004;</span> <a href="'.route('staff').'">'.__('translations.staff').'</a>';
								@endphp
								<div class="location-check-results">
									<div class="fx fac location-check-result">{!! $staff_check_symbol !!}</div>
								</div>
							@endif
							@if (Auth::user()->role_id == config('roles.lab_head'))
								@php
									$price_check_str = '<span style="color:red;font-weight:bold;margin-right:5px;">&#8722;</span> <a href="'.route('prices.create').'">'.__('translations.price').'</a>';
									$staff_check_str = '<span style="color:red;font-weight:bold;margin-right:5px;">&#8722;</span> <a href="'.route('staff.create').'">'.__('translations.staff').'</a>';
									$portfolio_check_str = '<span style="color:red;font-weight:bold;margin-right:5px;">&#8722;</span> <a href="'.route('portfolio.create').'">'.__('translations.portfolio').'</a>';

									$price_check = \App\Models\Price::where('branch_id', $item['id'])->first();
									$staff_check = \App\Models\Branch_staff::where('branch_id', $item['id'])->first();
									$portfolio_check = \App\Models\Portfolio::where('branch_id', $item['id'])->first();

									if ($price_check) $price_check_str = '<span style="color:green;font-weight:bold;margin-right:5px;">&#10004;</span> <a href="'.route('prices').'">'.__('translations.price').'</a>';
									if ($staff_check) $staff_check_str = '<span style="color:green;font-weight:bold;margin-right:5px;">&#10004;</span> <a href="'.route('staff').'">'.__('translations.staff').'</a>';
									if ($portfolio_check) $portfolio_check_str = '<span style="color:green;font-weight:bold;margin-right:5px;">&#10004;</span> <a href="'.route('portfolios').'">'.__('translations.portfolio').'</a>';
								@endphp
								<div class="location-check-results">
									<div class="fx fac location-check-result">{!! $price_check_str !!}</div>
									<div class="fx fac location-check-result">{!! $staff_check_str !!}</div>
									<div class="fx fac location-check-result">{!! $portfolio_check_str !!}</div>
								</div>
							@endif
						</div>
						<div class="dashboard-locations-cancelled">
							<a href="{{ route('orders') }}">
								{{ $item['cancelled_orders'] }}
								<span>(${{ $item['cancelled_payments'] }})</span>
							</a>
						</div>
						<div class="dashboard-locations-procces">
							<a href="{{ route('orders') }}">
								{{ $item['procces_orders'] }}
								<span>(${{ $item['procces_payments'] }})</span>
							</a>
						</div>
						<div class="dashboard-locations-complited">
							<a href="{{ route('orders') }}">
								{{ $item['сomplited_orders'] }}
								<span>(${{ $item['сomplited_payments'] }})</span>
							</a>
						</div>
						<div class="dashboard-locations-unpaid">
							<a href="{{ route('invoices', ['branch' => $item['id']]) }}">${{ $item['unpaid'] }}</a>
						</div>
						<div class="dashboard-locations-paid">
							<a href="{{ route('invoices', ['branch' => $item['id']]) }}">${{ $item['paid'] }}</a>
						</div>
					</div>
				@endforeach
			</div>
		@else 
			<p>{{ __('translations.no_locations_list') }}</p>
		@endif
	</div>
</div>
