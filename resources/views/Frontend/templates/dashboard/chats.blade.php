<div class="panel  panel-half dashboard-item dashboard-item-half dashboard-item-{{ $code }}">
	<div class="panel-head">{{ __('translations.latest_chat') }}</div>
	@if ($content['text'] && $content['text'] != '')
		<div class="panel-intro">{!! $content['text'] !!}</div>
	@endif
	<div class="panel-content">
		@if ($content['data'] && count($content['data']))
			@foreach ($content['data'] as $item)
				@if ($item->user)
				<div class="dashboard-message-line">
					<a href="{{ route('reviews.edit', $item->order_id) }}">
						<span class="dashboard-message-name">{{ $item->user->name.' '.$item->user->surname }}</span>
						<span class="dashboard-message-date">{{ date('m-d-Y', strtotime($item->created_at)) }}</span>
						<span class="dashboard-message-content">{{ $item->message }}</span>
					</a>
				</div>
				@endif
			@endforeach
		@else
			<p>{{ __('translations.no_latest_chat') }}</p>
		@endif
	</div>
</div>