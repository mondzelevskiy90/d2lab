<div class="panel panel-half dashboard-item dashboard-item-half dashboard-item-{{ $code }}">
	<div class="panel-head">{{ __('translations.order_statuses_list') }}</div>
	@if ($content['text'] && $content['text'] != '')
		<div class="panel-intro">{!! $content['text'] !!}</div>
	@endif
	<div class="panel-content">
		@if ($content['data'] && count($content['data']))
			<div class="dashboard-statuses-list">
				@foreach ($content['data'] as $item)
					<a href="{{ route('orders', ['status' => $item['id']]) }}" class="fx fac fxb dashboard-statuses-line">
						<span class="bi dashboard-statuses-icon" style="background-image: url({{ url('/').'/storage/'.$item['icon'] }})"></span>
						<span class="dashboard-statuses-name">{{ $item['name'] }}</span>
						<span class="dashboard-statuses-count">{{ $item['count'] }}</span>
					</a>
				@endforeach
			</div>
		@endif
	</div>
</div>