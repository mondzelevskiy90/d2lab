<div class="panel panel-half dashboard-item dashboard-item-half dashboard-item-{{ $code }}">
	<div class="panel-head">{{ __('translations.orders_list') }}</div>
	@if ($content['text'] && $content['text'] != '')
		<div class="panel-intro">{!! $content['text'] !!}</div>
	@endif
	<div class="panel-content">
		@if ($content['data'] && count($content['data']))
			<div class="dashboard-orders-list">
				@foreach ($content['data'] as $item)
					<a href="{{ route('orders.edit', $item->id) }}" class="fx fac fxb dashboard-orders-line @if($item->status_info->change_by == Auth::user()->type){{ 'dashboard-orders-lineAct' }}@endif">
						<span class="dashboard-orders-id">#{{ $item->id }}</span>
						<span class="dashboard-orders-name">{{ $item->patient_name.' '.$item->patient_lastname }}</span>
						<span class="dashboard-orders-date">{{ $item->ship_date }}</span>
						<span class="bi dashboard-orders-icon" style="background-image: url({{ url('/').'/storage/'.$item->status_info->icon }})" title="{{ $item->status_info->name }}"></span>
					</a>
				@endforeach
			</div>
		@else 
			<p>{{ __('translations.no_orders_list') }}</p>
		@endif
	</div>
</div>