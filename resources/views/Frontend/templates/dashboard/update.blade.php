<div class="panel panel-transparent dashboard-item dashboard-item-{{ $code }}">
	<a class="btn btn-danger dashboard-item-bigbtn" href="{{ route('settings') }}">{!! $content !!}</a>
</div>