@if ($content['data'] && count($content['data']))
	<div class="panel dashboard-item dashboard-item-{{ $code }}">
		<div class="panel-head">{{ __('translations.notification_list') }}</div>
		@if ($content['text'] && $content['text'] != '')
			<div class="panel-intro">{!! $content['text'] !!}</div>
		@endif
		<div class="panel-content">
			@foreach ($content['data'] as $item)
				<div id="notification_{{ $item->id }}" class="fx fac fxb dashboard-notification-line">
					<div class="dashboard-notification-date">{{ date('m-d-Y', strtotime($item->created_at)) }}</div>
					<div class="dashboard-notification-text">{!! $item->text !!}</div>
					<div class="dashboard-notification-action">
						<div class="btn btn-small" onclick="notificationViewed({{ $item->id }})">Ok!</div>
					</div>
				</div>
			@endforeach
		</div>
	</div>
	<script>
		function notificationViewed(id) {
			$.ajax({
				url: '/ajax/getData',
				type: 'post',
			    dataType: 'json',
			    headers: {
			        'X-CSRF-TOKEN': '{{ csrf_token() }}'
			    },
			    data: {action:'notificationViewed', id: id},	
			    success: function(data) {
			    	$('#notification_'+id).remove();
			    	
			    	if ($('.dashboard-notification-line').length == 0) window.location.reload();
			    }
			});
		}
	</script>
@endif
