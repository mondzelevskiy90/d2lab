<script>
	function addReview(order) {
		var html = getReviewBlock('order_review', order, false);

		$('#order_reviews_block').append(html);
		$('#order_review_add').remove();

		showReviewStars(5, 'new');
	}

	function addCommentToReview(review, order) {
		var html = getReviewBlock('order_review_comment', order, review)

		$('#order_comments_'+review).append(html);
		$('#order_review_add'+review).remove();
	}

	function getReviewBlock(type, order, review) {
		var html = '';

		html += '<div class="order-new-review">';

		if (type == 'order_review') {
			html += '<div class="fx review-rating-stars">';
			html += '<input type="radio" name="review[rating]" id="review_rating_new_1" class="review-rating-radio review-rating-new" value="1" onchange="showReviewStars(1, \'new\')" title="1">';
			html += '<input type="radio" name="review[rating]" id="review_rating_new_2" class="review-rating-radio review-rating-new" value="2" onchange="showReviewStars(2, \'new\')" title="2">';
			html += '<input type="radio" name="review[rating]" id="review_rating_new_3" class="review-rating-radio review-rating-new" value="3" onchange="showReviewStars(3, \'new\')" title="3">';
			html += '<input type="radio" name="review[rating]" id="review_rating_new_4" class="review-rating-radio review-rating-new" value="4" onchange="showReviewStars(4, \'new\')" title="4">';
			html += '<input type="radio" name="review[rating]" id="review_rating_new_5" class="review-rating-radio review-rating-new review-rating-radioAct" value="5" onchange="showReviewStars(5, \'new\')" title="5" checked>';
			html += '</div>';
		}

		html += '<label for="order_new_review">{{ __('translations.new_review') }}</label>'
		html += '<textarea id="order_new_review" class="form-input form-input-required" value="" placeholder="{{ __('translations.new_review') }}" maxlength="1000"></textarea>';
		html += '<div class="order-new-btns">';
		html += '<div class="btn btn-small btn-danger" onclick="window.location.reload();" style="margin-right: 5px;">{{ __('translations.delete') }}</div>';
		html += '<div class="btn btn-small" onclick="saveReview(\''+type+'\', '+order+', '+review+')">{{ __('translations.save') }}</div>';
		html += '</div>';
		html += '</div>';

		return html;
	}

	function saveReview(type, order, review_id) {
		var rating = false;
		var review = $(document).find('#order_new_review').val(); 

		if (type == 'order_review') rating = $(document).find('input.review-rating-new:checked').val();

		showLoader();
		$.ajax({
			url: '/ajax/getData',
			type: 'post',
		    dataType: 'json',
		    headers: {
		        'X-CSRF-TOKEN': '{{ csrf_token() }}'
		    },
		    data: {action:'saveReview', type: type, order: order, review_id: review_id, rating: rating, review: review},	
		    success: function(data) {
		    	setTimeout(function (){
		    		window.location.reload();
		    	}, 2000);
		    }
		});
	}

	function editReview(id, type) {
		var rating = $(document).find('input.review-rating'+id+':checked').val();
		var review = $(document).find('#'+type+id);

		if (review.hasClass('order-review-edit')) {
			review.prop('disabled', false);
			review.removeClass('order-review-edit');
		} else {
			showLoader();

			review.prop('disabled', true);
			review.addClass('order-review-edit');

			$.ajax({
				url: '/ajax/getData',
				type: 'post',
			    dataType: 'json',
			    headers: {
			        'X-CSRF-TOKEN': '{{ csrf_token() }}'
			    },
			    data: {action:'editReview', id: id, type: type, rating: rating, review: review.val()},	
			    success: function(data) {
			    	setTimeout(function (){
			    		window.location.reload();
			    	}, 2000);
			    }
			});
		}
	}

	function setReviewToModeration(id, text,type) {
		$.ajax({
			url: '/ajax/getData',
			type: 'post',
		    dataType: 'json',
		    headers: {
		        'X-CSRF-TOKEN': '{{ csrf_token() }}'
		    },
		    data: {action:'editReview', id: id, text: text, type: type, moderation: 2},	
		    success: function(data) {
		    	showPopup('<div class="success">{{ __('translations.review_on_moderation') }}</div>');
		    }
		});
	}

	function showReviewStars(index, id) {
		$(document).find('.review-rating-radio').removeClass('review-rating-radioAct');

		if (id) {
			var id_data = '#review_rating_'+id+'_';
		} else {
			var id_data = '#review_rating_';
		}

		for (var i = 1; i <= index; i++) {
			$(document).find(id_data+i).addClass('review-rating-radioAct');
		}
	}
</script>