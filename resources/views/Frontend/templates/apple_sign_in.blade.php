<script src="https://appleid.cdn-apple.com/appleauth/static/jsapi/appleid/1/en_US/appleid.auth.js"></script>
<div style="display: none;" id="appleid-signin" data-color="black" data-border="true" data-type="sign in"></div>
<script>
	AppleID.auth.init({
	    clientId : 'dentist2lab.com2',
	    scope : 'email name',
	    redirectURI : 'https://dentist2lab.com/{{ $redirect_url }}',
	    usePopup : true
	});    

   document.addEventListener('AppleIDSignInOnSuccess', function (data) {
		console.log(data);

		if (data.detail) {
		  	$.ajax({
				url: '/ajax/getData',
				type: 'post',
			    dataType: 'json',
			    headers: {
			        'X-CSRF-TOKEN': '{{ csrf_token() }}'
			    },
			    data: {action:'appleUser', apple: data.detail},	
			    success: function(data) {
			    	if (data) {
			    	 	window.location.href = '/catalog/dentistry/labs';
			    	} else {
			    		showPopup(('Social network error!'));
			    	}
			    },
			    error: function () {
			    	showPopup(('Social network error!'));
			    }
			});
		} else {
			showPopup(('Social network error!'));

			return false;
		}
	}); 

	document.addEventListener('AppleIDSignInOnFailure', function (error) {
		showPopup(('Social network error!'));
	    console.log(error);
	});
</script>