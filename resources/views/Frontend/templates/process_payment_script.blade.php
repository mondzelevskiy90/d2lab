function processPaymentType() {
	var invoices = $('.invoices-check');
	var invoices_data = [];
	var current_pay = $(document).find('#current_pay').val();
	var payment_system = $(document).find('input[name=\'payment_system_input\']:checked').val();

	invoices.each(function(i){
		if (this.checked === true) {
			invoices_data[i] = this.value;
		}
	});
	
	if (invoices_data.length) {
		if (payment_system == 1) {
			showLoader();

			var reload = true;

			$.ajax({
				url: '/ajax/getData',
				type: 'post',
	            dataType: 'json',
	            headers: {
	                'X-CSRF-TOKEN': '{{ csrf_token() }}'
	            },
			    data: {action:'closeInvoices', invoices: invoices_data, status: 5},
			    success: function(data) {
			    	if (data) {
			    		hideLoader();
			    		showPopup('<div class="success">{{ __('translations.changed_to_bank_payment') }}</div>');
			    	}
			    }						    
			});

			if (reload) setTimeout(function() {
					window.location.reload();
			}, 5000);	
		}

		if (payment_system == 2) {
			var card = $(document).find('#payment_card');
			var exp = $(document).find('#payment_exp');
			var cvv = $(document).find('#payment_cvv');			

			if (!validateCardNum(card.val())) {
				card.addClass('input-error');
				return false;
			} else {
				card.removeClass('input-error');
			}

			if (!validateExpDate(exp.val())) {
				exp.addClass('input-error');
				return false;
			} else {
				exp.removeClass('input-error');
			}

			if (cvv.val() == '') {
				cvv.addClass('input-error');
				return false;
			} else {
				cvv.removeClass('input-error');
			}

			showLoader();

			$('#cboxClose').click();

			$.ajax({
				url: '/ajax/getData',
				type: 'post',
		        dataType: 'json',
		        headers: {
		            'X-CSRF-TOKEN': '{{ csrf_token() }}'
		        },
			    data: {action:'payOnlineInvoices', invoices: invoices_data, card: card.val(), exp: exp.val(), cvv: cvv.val()},
			    success: function(data) {
			    	if (data) {
			    		showPopup('<div class="success">'+data+'</div>');
			    		hideLoader();

			    		setTimeout(function(){
							window.location.reload();
			    		}, 3000);
			    	}
			    }						    
			});
		}
	}
}

@include ('Frontend.templates.online_payment_form_script')