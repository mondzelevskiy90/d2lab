<script src="/js/masked.js" defer></script>
<script>
    function paySingleInvoice(index) {
        var invoices = document.querySelectorAll('.invoices-check');

        invoices.forEach(function(el){
            el.checked = false;
            el.nextElementSibling.classList.remove('show-check-pseudo');
        });
        
        document.getElementById('invoice_'+index).checked = true;
        document.getElementById('invoice_'+index).nextElementSibling.classList.add('show-check-pseudo');

        paySelectedInvoices();
    }

    function paySingleFromInvoicePage() {
        paySelectedInvoices();

        return false;
    }

    function paySelectedInvoices() {
        var invoices = $('.invoices-check');
        var invoices_data = [];
        var reload = false;

        invoices.each(function(i){
            if (this.checked === true) {
                reload = true;
                invoices_data[i] = this.value;
            }
        });

        if (invoices_data.length) {

            @if (Auth::user()->type == config('types.lab'))
                if (confirm('{{ __('translations.confirm_pay_by_lab') }}')) {
                    showLoader();

                    $.ajax({
                        url: '/ajax/getData',
                        type: 'post',
                        dataType: 'json',
                        headers: {
                            'X-CSRF-TOKEN': '{{ csrf_token() }}'
                        },
                        data: {action:'closeInvoices', invoices: invoices_data, status: 4},                         
                    });
                        

                    if (reload) setTimeout(function() {
                            window.location.reload();
                    }, 5000);  
                }         
            @endif

            @if (Auth::user()->type == config('types.clinic'))
                $.ajax({
                    url: '/ajax/getData',
                    type: 'post',
                    dataType: 'json',
                    headers: {
                        'X-CSRF-TOKEN': '{{ csrf_token() }}'
                    },
                    data: {action:'getSelectedInvoicesData', invoices: invoices_data},  
                    success: function(data) {
                        if (data) {
                            var html = '';

                            html += '<div class="panel-head payment-popup-head">{{ __('translations.payment') }}</div>';

                            if (data.payment_systems) {
                                html += '<div class="panel-subhead">{{ __('translations.payment_type') }}:</div>';
                                html += '<div class="form-line">';

                                for (i in data.payment_systems) {
                                    html += '<div class="fx fac fxb form-checkbox">';
                                    html += '<input type="radio" name="payment_system_input" id="payment_system_'+data.payment_systems[i]['id']+'" value="'+data.payment_systems[i]['id']+'" '+(data.payment_systems[i]['id'] == 1 ? 'checked="checked"' : '')+' onclick="showOnlinePaymentForm('+data.payment_systems[i]['id']+');">';
                                    html += '<label for="payment_system_'+data.payment_systems[i]['id']+'">'+data.payment_systems[i]['name']+'</label>';
                                    html += '</div>';
                                }

                                html += '</div>';
                            }

                            html += '<div class="panel-subhead">{{ __('translations.payment_details') }}:</div>';
                            html += '<div id="online_payment_form" class="fx fxb" '+(data.can_online ? 'style="height:80px;"' : '')+'><p></p><p>{{ __('translations.bank_transfer_text') }}</p><p></p></div>';

                            html += '<div class="panel-subhead">{{ __('translations.totals') }}:</div>';
                           /*
                            html += '<div class="fx fac fxb form-line payment-popup-total">{{ __('translations.invoices_total') }}: <span>{{ $currency->symbol }}'+data.total+'</span></div>';
                            */
                            html += '<div class="fx fac fxb form-line payment-popup-pay">{{ __('translations.invoices_pay') }}: <span>{{ $currency->symbol }}'+data.pay+' <i id="service_charge_msg" style="display:none;">'+data.service_charge+'% service charge!</i></span><input type="hidden" name="current_pay" id="current_pay" value="'+data.pay+'"></div>';
                            /*
                            if (data.debt > 0) {
                                html += '<div class="fx fac fxb form-line payment-popup-debt">{{ __('translations.invoices_debt') }}: <span>{{ $currency->symbol }}'+data.debt+'</span></div>';
                            }
                            */
                            html += '<div class="form-line payment-popup-btn">';
                            html += '<div class="btn btn-small btn-danger" onclick="document.getElementById(\'cboxClose\').click();" style="margin-right: 5px;">{{ __('translations.cancel') }}</div>';
                            html += '<div class="btn btn-small" onclick="processPaymentType();">{{ __('translations.pay') }}</div>';            
                            html += '</div>';

                            $.colorbox({
                                html: '<div class="popup-content"><div class="payment-popup">'+ html +'</div></div>',
                                onComplete : function() {
                                    $(this).colorbox.resize();
                                }
                            });
                        }
                    }                       
                });
            @endif
        }
    }

    @if (Auth::user()->type == config('types.clinic'))
        @include('Frontend.templates.process_payment_script')
    @endif
</script>