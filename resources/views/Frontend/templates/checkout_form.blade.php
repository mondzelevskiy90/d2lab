{{ csrf_field() }}	
<div class="fx fxb panel-body">
	<div class="panel">
		<div class="panel-head panel-head-collapse panel-head-collapsed">{{ __('translations.lab_info') }} - {{ $branch->name }}</div>
		<div class="fx fxb panel-block panel-block-collapsed">
			<div class="checkout-branch-left">
				<div class="checkout-branch-logo" style="background-image: url({{ App\Services\Img::resizeImage($branch->logo, 235, 235) }})">
				</div>
			</div>
			<div class="checkout-branch-right">
				<div class="fx fxb checkout-branch-info">
					<div >
						<div class="checkout-branch-infoName">
							{{ $branch->name }}
						</div>
						<div class="checkout-branch-icons">
							<div class="bi checkout-branch-icon checkout-branch-verified">{{ __('translations.verified') }}</div>
							<div class="bi checkout-branch-icon checkout-branch-email">{{ $branch->email }}</div>
							<div class="bi checkout-branch-icon checkout-branch-phone">{{ $branch->phone }}</div>
							<div class="bi checkout-branch-icon checkout-branch-location">{{ $branch->zip_code .', '. $branch->address .', '. App\Services\Userdata::getLocality($branch->locality) }}</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="fx fxb panel-body">
	<div class="panel">
		<div class="panel-head panel-head-collapse panel-head-collapsed">{{ __('translations.order_user_info') }}</div>
		<div class="fx fxb panel-block panel-block-collapsed">
			<input type="hidden" name="user_id" value="@if(isset($data->id)){{ $data->user_id }}@else{{ Auth::user()->id }}@endif"/>
			<input type="hidden" name="user_name" value="@if(isset($data->user_name)){{ $data->user_name }}@else{{ Auth::user()->name.' '.Auth::user()->surname }}@endif"/>
			<input type="hidden" id="to_branch_id" name="to_branch_id" value="@if(isset($data->to_branch_id)){{ $data->to_branch_id }}@else{{ $branch->id }}@endif"/>
			<input type="hidden" name="to_branch_name" value="@if(isset($data->to_branch_name)){{ $data->to_branch_name }}@else{{ $branch->name }}@endif"/>
			
			@if ($branches && count($branches))
				<div class="form-line">	
					<label for="branch_id">{{ __('translations.branch') }}</label>			
					<select name="branch_id" id="branch_id" class="form-input form-input-required" required="required" onchange="getBranchAddreess();">						
						@foreach($branches as $branch)
							<option value="{{ $branch->id }} @if(isset($data->branch_id) && (int)$data->branch_id == (int)$branch->id){{ 'selected="selected"' }}@endif">{{ $branch->name }}</option>
						@endforeach
					</select>
				</div>
			@elseif ($user_branch) 
				<input type="hidden" id="branch_id" name="branch_id" value="@if(isset($data->branch_id)){{ $data->branch_id }}@else{{ $user_branch->branch->id }}@endif"/>
				<input type="hidden" name="branch_name" value="@if(isset($data->branch_name)){{ $data->branch_name }}@else{{ $user_branch->branch->name }}@endif"/>
			@else
				<input type="hidden" id="branch_id" name="branch_id" value="@if(isset($data->branch_id)){{ $data->branch_id }}@elseif($user_branch->branch){{ $user_branch->branch->id }}@else{{'0'}}@endif"/>
				<input type="hidden" name="branch_name" value="@if(isset($data->branch_name)){{ $data->branch_name }}@elseif($user_branch->branch){{ $user_branch->branch->name }}@else{{'Branch not detected!'}}@endif"/>
			@endif

			<div class="form-line">	
				<label for="user_email">{{ __('translations.email') }}</label>
				<input type="email" name="user_email" id="user_email" class="form-inut form-input-required" value="@if(isset($data->user_email)){{ $data->user_email }}@else{{ Auth::user()->email }}@endif" placeholder="{{ __('translations.email') }}" maxlength="255" required="required">
			</div>
			<div class="form-line">	
				<label for="user_phone">{{ __('translations.phone') }}</label>
				<input type="text" name="user_phone" id="user_phone" class="form-inut form-input-required" value="@if(isset($data->user_phone)){{ $data->user_phone }}@elseif(Auth::user()->phone){{ Auth::user()->phone }}@else{{'+1 (111) 111-1111'}}@endif" placeholder="{{ __('translations.phone') }}" maxlength="32" required="required">
			</div>
		</div>
	</div>
</div>
<div class="fx fxb panel-body">
	<div class="panel">
		<div class="panel-head panel-head-collapse">{{ __('translations.order_patient_info') }}</div>
		<div class="fx fxb panel-block">
			<div class="form-line">	
				<label for="patient_name">{{ __('translations.name') }}</label>
				<input type="text" name="patient_name" id="patient_name" class="form-inut form-input-required" value="@if(isset($data->patient_name)){{ $data->patient_name }}@endif" placeholder="{{ __('translations.patient_name') }}" maxlength="255" required="required">
			</div>
			<div class="form-line">	
				<label for="patient_lastname">{{ __('translations.lastname') }}</label>
				<input type="text" name="patient_lastname" id="patient_lastname" class="form-inut form-input-required" value="@if(isset($data->patient_lastname)){{ $data->patient_lastname }}@endif" placeholder="{{ __('translations.patient_lastname') }}" maxlength="255" required="required">
			</div>
			<div class="form-line">	
				<label for="patient_age">{{ __('translations.age') }}</label>
				<input type="text" name="patient_age" id="patient_age" class="form-inut" value="@if(isset($data->patient_age)){{ $data->patient_age }}@endif" placeholder="{{ __('translations.patient_age') }}" maxlength="3" onchange="isNaN(this.value) ? this.value = null : this.value = this.value;">
			</div>
			<div class="form-line">	
				<label for="patient_sex">{{ __('translations.sex') }}</label>			
				<select name="patient_sex" id="patient_sex" class="form-input">
					<option value="{{ __('translations.male') }}" @if(isset($data->patient_sex) && $data->patient_sex == __('translations.male')){{ 'selected="selected"' }}@endif>{{ __('translations.male') }}</option>
					<option value="{{ __('translations.female') }}" @if(isset($data->patient_sex) && $data->patient_sex == __('translations.female')){{ 'selected="selected"' }}@endif>{{ __('translations.female') }}</option>
				</select>
			</div>
		</div>
	</div>
</div>
<div class="fx fxb panel-body">
	<div class="panel">
		<div class="panel-head panel-head-collapse">{{ __('translations.order_info') }}</div>
		<div class="fx fxb panel-block">							
			<div class="form-line" style="display: none;">	
				<label for="order_type">{{ __('translations.order_type') }}</label>
				<select name="order_type" id="order_type" class="form-input form-input-required" required="required">
					@foreach($order_types as $order_type)
						<option value="{{ $order_type->id }}"  @if(isset($data->order_type) && $data->order_type == $order_type->id){{ 'selected="selected"' }}@endif>{{ $order_type->name }}</option>
					@endforeach
				</select>
			</div>					
			<div class="form-line">	
				<label for="ship_date">{{ __('translations.ship_date') }}</label>
				<input type="text" name="ship_date" id="ship_date" class="form-input form-input-required" value="@if(isset($data->ship_date)){{ $data->ship_date }}@endif" placeholder="{{ __('translations.ship_date') }}" maxlength="255" required="required">
			</div>
			<div class="form-line">	
				<label for="ship_address">{{ __('translations.ship_address') }}</label>
				@php
					$address = '';

					if ($branches && count($branches)) {
						foreach ($branches as $branch) {
							$address = $branch->zip_code .', '. $branch->address .', '. App\Services\Userdata::getLocality($branch->locality);
							break;
						}
					}

					if ($user_branch) {
						$address = $user_branch->branch->zip_code .', '. $user_branch->branch->address .', '. App\Services\Userdata::getLocality($user_branch->branch->locality);
					}
				@endphp
				<input type="text" name="ship_address" id="ship_address" class="form-input form-input-required" value="@if(isset($data->ship_date)){{ $data->ship_address }}@else{{ $address }}@endif" placeholder="{{ __('translations.ship_address') }}" maxlength="255" required="required">
			</div>
			<div class="form-line">
				@php
					$attached = array();

					if (isset($data->attached) && $data->attached) {
						$attached = explode(';', $data->attached);
					}
				@endphp

				<label>{{ __('translations.order_attached') }}</label>
				<div class="form-checkbox">								
					<input type="checkbox" name="attached[]" id="attached1" value="{{ __('translations.attached_impressions') }}" @if(in_array(__('translations.attached_impressions'), $attached)){{ 'checked="checked"' }}@endif>
					<label for="attached1">{{ __('translations.attached_impressions') }}</label>
				</div>
				<div class="form-checkbox">								
					<input type="checkbox" name="attached[]" id="attached2" value="{{ __('translations.attached_models') }}"  @if(in_array(__('translations.attached_models'), $attached)){{ 'checked="checked"' }}@endif>
					<label for="attached2">{{ __('translations.attached_models') }}</label>
				</div>
				<div class="form-checkbox">								
					<input type="checkbox" name="attached[]" id="attached3" value="{{ __('translations.attached_bite') }}"  @if(in_array(__('translations.attached_bite'), $attached)){{ 'checked="checked"' }}@endif>
					<label for="attached3">{{ __('translations.attached_bite') }}</label>
				</div>
				<div class="form-checkbox">								
					<input type="checkbox" name="attached[]" id="attached4" value="{{ __('translations.attached_photos') }}"  @if(in_array(__('translations.attached_photos'), $attached)){{ 'checked="checked"' }}@endif>
					<label for="attached4">{{ __('translations.attached_photos') }}</label>
				</div>
				@php
					$attached_other = '';

					foreach ($attached as $attach) {
						if ($attach != __('translations.attached_photos') && $attach != __('translations.attached_bite') && $attach != __('translations.attached_models') && $attach != __('translations.attached_impressions')) $attached_other = $attach;
					}
				@endphp
				<div class="form-line">
					<input type="text" name="attached[]" id="attached5" class="form-input" value="{{ $attached_other }}" placeholder="{{ __('translations.attached_other') }}" maxlength="220" style="padding:8px 10px;margin-top: 10px;font-size: 12px;">
				</div>
			</div>
			<div class="form-line">	
				<label for="">{{ __('translations.numbering_system') }}</label>
				<select name="numbering_system_id" id="numbering_system" class="form-input form-input-required" required="required" onchange="getNumberingSystemSvg();">
					@foreach($numbering_systems as $numbering_system)
						<option value="{{ $numbering_system->id }}" @if(isset($data->numbering_system_id) && $data->numbering_system_id == $numbering_system->id){{ 'selected="selected"' }}@endif>{{ $numbering_system->name }}</option>
					@endforeach									
				</select>
			</div>
			@php $file_index = 9999; @endphp
			<div class="form-line">
				<label for="files">{{ __('translations.order_files') }}</label>				
				<div id="order_files" class="fx">
					@if (isset($data->files))
						@foreach($data->files as $file)
							<div id="order_file_line{{ $file_index }}" class="fx fxb order-file-line order-file-icon">
								@php 
									$extension = pathinfo(public_path().'/storage/'.$file->file)['extension'];
								@endphp
								<div id="order_file_name{{ $file_index }}" class="fx fxc order-file-name">
									<a href="{{ url('/').'/storage/'.$file->file }}" target="_blank" class="fx fxc">
									@if (in_array($extension, ['png', 'PNG', 'jpg', 'jpeg', 'JPEG', 'JPG']))
											<img src="{{ '/storage/'.$file->file}}" width="100%" height="auto" />
									@else 															
										<span>.{{ $extension }}</span>
									@endif
									</a>
									<div class="order-file-remove" onclick="removeFileFromCheckoutForm('{{ $file_index }}')" title="{{ __('translations.delete') }}"><div></div></div>
									<input type="hidden" id="order_files_save{{ $file_index }}" name="order_files[{{ $file_index }}]" value="{{ $file->file }}">	
								</div>
							</div>
							@php $file_index++; @endphp
						@endforeach
					@endif	
				</div>
				<br>
				<div id="order_files_add" class="btn btn-small" onclick="addFileToCheckoutForm()" style="max-width:120px;">{{ __('translations.add_file') }}</div>
			</div>							
		</div>
	</div>
</div>
<div class="fx fxb panel-body">
	<div class="panel">
		<div class="panel-head panel-head-collapse">{{ __('translations.add_order_prescriptions') }}</div>
		<div class="fx fxb panel-block">
			<div class="form-line">
				<label>***{{ __('translations.add_order_prescriptions_intro') }}</label>
			</div>		
			<div class="form-line">	
				<label for="service_id">{{ __('translations.service_name') }}</label>
				<select name="service_id" id="service_id" class="form-input form-input-required" required="required">
					@php $service_cnt = 0;  @endphp
					@foreach($services as $service)	
						
							@php 
								$show_group = false;
								$close_group = false;

								if(!isset($services[$service_cnt - 1]) || ((string)$service['parent_name'] !== (string)$services[$service_cnt - 1]['parent_name'])) {
									$show_group = true;
								}

								if(!isset($services[$service_cnt + 1]) || ((string)$service['parent_name'] !== (string)$services[$service_cnt + 1]['parent_name'])) {
									$close_group = true;
								}
							@endphp
							@if($show_group)
								<optgroup label="{{ $service['parent_name'] }}">	
							@endif			
							<option value="{{ $service['service']['id'] }}">{{ $service['service']['name'] }} @if($service['min_days'] && $service['min_days'] > 0) - {{ __('translations.from').' '.$service['price'].$currency->symbol }}, {{ $service['min_days'].' days' }}@endif</option>
							@if($close_group)
								</optgroup>
							@endif	
							@php $service_cnt++ @endphp
					
					@endforeach
				</select>
			</div>							
			<div class="form-line order-add-prescription">
				<div id="add_prescription_button" class="btn" onclick="addPrescriptionBlock()">{{ __('translations.add_service') }}</div>
			</div>
			<div class="fx fxb form-line order-prescription-selects">
				<div class="order-prescription-img">
					<div class="fx fxc" id="numbering_system_mask" style="@if(isset($data->prescriptions) && count($data->prescriptions)){{ 'display: none;' }}@endif">{{ __('translations.select_service') }}</div>
					<div id="numbering_system_block">
						@foreach($numbering_systems as $numbering_system)
							@if (isset($data->numbering_system_id))
								@if ($numbering_system->id == $data->numbering_system_id && $numbering_system->svg)
									{!! $numbering_system->svg !!}
								@endif
							@else
								@if ($numbering_system->svg)
									{!! $numbering_system->svg !!}
								@endif
								@php 
									break;
								@endphp
							@endif
						@endforeach						
					</div>
					<!--<div class="prescription-tutorial-link" onclick="showTutorial()">{{ __('translations.tutorial') }}</div> -->
				</div>
				<div class="order-prescription-services">
					<div class="panel-head order-prescription-servicesHead" style="@if(!isset($prescriptions) || (isset($prescriptions) && !count($prescriptions)) ){{ 'display: none;' }}@else{{ 'display:block' }}@endif text-align: center">{{ __('translations.selected_order_services') }}:</div>
					@php
						$prescription_colors = [
							1 => '#ffbab0',
							2 => '#F2EDA2',
							3 => '#BBDCEF',
							4 => '#CAE8CE',
							5 => '#F7C69C',
							6 => '#f3d9da',
							7 => '#ffb6ff',
							8 => '#b58585',
							9 => '#d2d222',
							10 => '#DADADA'
						];
					@endphp
					<div id="order_prescription_services">
						@if(isset($prescriptions) && count($prescriptions))
							@foreach ($prescriptions as $prescription)
									@php
										$indx = $prescription_index <= 10 ? $prescription_index : 1;
										$color = $prescription_colors[$indx];
									@endphp
									<div id="order_prescription-service{{ $prescription_index }}" class="fx fxb order-prescription-service">
									<input type="hidden" name="prescriptions[{{ $prescription_index }}][price]" id="order_prescription_price{{ $prescription_index }}" class="order-prescription-price-input" value="{{ $prescription['advance_price'] }}">
									<input type="hidden" name="prescriptions[{{ $prescription_index }}][price_for]" id="order_prescription_price_for{{ $prescription_index }}" value="{{ $prescription['price_for'] }}">									
									<input type="hidden" name="prescriptions[{{ $prescription_index }}][selected_option]" id="order_prescription_selected_option{{ $prescription_index }}" value="{{ $prescription['selected_option'] }}">
									<input type="hidden" name="prescriptions[{{ $prescription_index }}][price_order_date]" id="order_prescription_price_order_date{{ $prescription_index }}" value="{{ $prescription['price_order_date'] }}">
									<div id="order_prescription_name{{ $prescription_index }}" class="fx fac order-prescription-name @if($prescription_index == 1){{ 'order-prescription-name-act' }}@endif" onclick="showActivePrescription({{ $prescription_index }})">
									<div class="order-prescription-check" title="{{ $prescription['service_name'] }}"></div>
									<div id="order_prescription_color{{ $prescription_index }}" class="order-prescription-color" data-color="{{ $color }}" style="background-color:{{ $color }}" title="{{ $prescription['service_name'] }}"></div>
									<div style="width: calc(100% - 55px);">#{{ $prescription_index }} <strong>{{ $prescription['service_name'] }}</strong></div>
									  </div>						
									  <div class="order-prescription-scroll" onclick="scrollToblock(\'order_prescription-block{{ $prescription_index }}\')" title="{{ $prescription['service_name'] }}">{{ __('translations.show_prescription') }}</div>
										<div class="btn btn-danger delete-order-prescription" onclick="removePrescriptionFromOrder({{ $prescription_index }});">{{ __('translations.delete') }}</div>
									<input type="text" name="prescriptions[{{ $prescription_index }}][selecteds]" id="prescriptions_selecteds_{{ $prescription_index }}" class="prescription-selecteds" value="{{ $prescription['selecteds_data'] }}" @if($prescription['selected_option'] != 0) required @endif>
									<div class="prescription-selecteds-text" id="order_prescription_text{{ $prescription_index }}">
										{{ __('translations.selected_items') }}: 
										<span id="prescriptions_selecteds_text_{{ $prescription_index }}">{{ $prescription['selecteds'] }}</span>
									</div>
									@if ($prescription['available_options'])
										@php 
											$available_options = explode(',', $prescription['available_options']);
										@endphp
										<div class="prescription-selecteds-options" id="order_prescription_options{{ $prescription_index }}">
											<span>{{ __('translations.select') }}:</span>
											@if (in_array(1, $available_options))
												<div class="order-option-check @if($prescription['selected_option'] && $prescription['selected_option'] == 1){{ 'order-option-checkAct' }}@endif" id="order_option_check{{ $prescription_index }}_1" onclick="selectOneTooth(1, {{ $prescription_index }}, 1)">Crown</div>
											@endif

											@if (in_array(2, $available_options))
												<div class="order-option-check @if($prescription['selected_option'] && $prescription['selected_option'] == 2){{ 'order-option-checkAct' }}@endif" id="order_option_check{{ $prescription_index }}_2" onclick="selectBridge({{ $prescription_index }})">Bridge</div>
											@endif

											@if (in_array(3, $available_options))
												@php
													$lower_class = '';
													$upper_class = '';

													if ($prescription['selected_option'] && $prescription['selected_option'] == 3) {

														if (strpos($prescription['selecteds'], '-32') !== false) {
															$lower_class = 'order-option-checkAct';
														}

														if (strpos($prescription['selecteds'], '1-') !== false) {
															$upper_class = 'order-option-checkAct';
														}														
													}
												@endphp
												<div class="order-option-check {{ $upper_class }}" id="order_option_check{{ $prescription_index }}_3_2" onclick="selectAllTooths(2, {{ $prescription_index }})">Upper</div>
												<div class="order-option-check {{ $lower_class }}" id="order_option_check{{ $prescription_index }}_3_1" onclick="selectAllTooths(1, {{ $prescription_index }})">Lower</div>
												
											@endif

											@if (in_array(4, $available_options))
												<div class="order-option-check @if($prescription['selected_option'] && $prescription['selected_option'] == 4){{ 'order-option-checkAct' }}@endif" id="order_option_check{{ $prescription_index }}_4" onclick="selectOneTooth(4, {{ $prescription_index }}, 2)">Partial denture (Flipper, Partial prosthetics)</div>
											@endif

											@if (in_array(5, $available_options))
												@php
													$lower_class = '';
													$upper_class = '';

													if ($prescription['selected_option'] && $prescription['selected_option'] == 5) {
														
														$prescription_selecteds = explode(', ', $prescription['selecteds']);

														if (min($prescription_selecteds) > 16) {
															$lower_class = 'order-option-checkAct';
														}

														if (max($prescription_selecteds) < 17) {
															$upper_class = 'order-option-checkAct';
														}														
													}
												@endphp
												<div class="order-option-check {{ $upper_class }}" id="order_option_check{{ $prescription_index }}_5_2" onclick="selectOneTooth(5, {{ $prescription_index }}, 2)">Partial denture Upper</div>
												<div class="order-option-check {{ $lower_class }}" id="order_option_check{{ $prescription_index }}_5_1" onclick="selectOneTooth(5, {{ $prescription_index }}, 1)">Partial denture Lower</div>
												
											@endif
										</div>
									@endif
									</div>
								@php $prescription_index++; @endphp
							@endforeach
							<script>
								
							</script>
						@endif
					</div>
					<div class="order-prescription-total">
						{{ __('translations.advance_price') }}: {{ $currency->symbol }}<span id="order_prescription_total">@if(isset($data->total)){{ $data->total->advance_price }}@else{{ '0' }}@endif</span> 
						<input type="hidden" id="advance_price" name="advance_price" value="@if(isset($data->total)){{ $data->total->advance_price }}@else{{ '0' }}@endif">
					</div>
				</div>
				<div class="form-line">	
					<label for="comment">{{ __('translations.comment') }}</label>
					<textarea name="comment" id="comment" class="form-inut" value="" placeholder="{{ __('translations.comment') }}" maxlength="1000">@if(isset($data->comment) && $data->comment != ''){{ $data->comment }}@endif</textarea>
				</div>
			</div>
		</div>
	</div>
</div>				
<div id="order_prescriptions">
	@if(isset($prescriptions) && count($prescriptions))
		@php $prescription_index2 = 1; @endphp
		@foreach ($prescriptions as $prescription)
			<div id="order_prescription-block{{ $prescription_index2 }}" class="fx fxb panel-body">
				<div class="panel">
					<div class="btn btn-danger delete-order-prescription" onclick="removePrescriptionFromOrder({{ $prescription_index2 }});">{{ __('translations.delete') }}</div>
					<div class="panel-head">{{ __('translations.order_prescription') }} #{{ $prescription_index2 }}</div>
					<div id="order_prescription{{ $prescription_index2 }}" class="panel-block">
						<div class="form-line">
							<label>{{ __('translations.numbering_system') }}: <strong>{{ $prescription['numbering_system_name'] }}</strong></label>
							<input type="hidden" name="prescriptions[{{ $prescription_index2 }}][numbering_system_id]" value="{{ $prescription['numbering_system_id'] }}">
						</div>
						<div class="form-line">
							<label>{{ __('translations.service_name') }}: <strong>{{ $prescription['service_name']}} ({{ $prescription['parent_name'] }})</strong></label>
							<input type="hidden" name="prescriptions[{{ $prescription_index2 }}][service_id]" value="{{ $prescription['service_id'] }}">
						</div>
			
						@if ($prescription['fields'])
							@foreach ($prescription['fields'] as $field)
								<div class="form-line">	
									<div class="prescription-line-head">
										<label for="prescription{{ $field['id'] }}" class="prescription-name">
											{{ $field['name'] }}
											@if ($field['help'])
												<span class="prescription-line-help" onclick="showFieldHelp({{ $field['id'] }})">?</span>
											@endif
										</label>
										@if ($field['description'])
											<p class="prescription-desc">{{ $field['description'] }}</p>
										@endif
									</div>
									<div class="fx fac prescription-line-content">
										@if ($field['image']) 
											<div class="prescription-image">
												<img src="{{ url('/').'/storage/'.$field['image'] }}">
											</div>
										@endif

										<div class="fx prescription-inputs @if($field['image']){{ 'prescription-inputs-half'}}@endif">
		
										@if ($field['field_type'] == 'text')
											<input type="text" name="prescriptions[{{ $prescription_index2 }}][fields][{{ $field['id'] }}]" id="prescription_{{ $prescription_index2 }}_{{ $field['id'] }}" value="@if(isset($prescription['user_fields'][$field['id']])){{ $prescription['user_fields'][$field['id']]['value'] }}@endif" placeholder="{{ $field['name'] }}" maxlength="255" @if($field['required'] == 1){{ 'class="form-input form-input-required" required="required"' }}@else{{ 'class="form-input' }}@endif>
										@endif

										@php
											$selected_items = array();

											if (isset($prescription['user_fields'][$field['id']])) {
												$selected_items = explode(';', $prescription['user_fields'][$field['id']]['value']);
											}
										@endphp

										@if ($field['field_type'] == 'select' && $field['values']) 
											<select name="prescriptions[{{ $prescription_index2 }}][fields][{{ $field['id'] }}]" id="prescription_{{ $prescription_index2 }}_{{ $field['id'] }}" @if($field['required'] == 1){{ 'class="form-input form-input-required" required="required"' }}@else{{ 'class="form-input' }}@endif>	
												@foreach ($field['values'] as $value)
													@php 
														$selected_item = false; 
														
														if (in_array($value['value'], $selected_items)) {
															$selected_item = true;
														}
													@endphp
													<option value="{{ $value['value'] }}" @if ($selected_item){{ 'selected="selected"' }}@endif>{{ $value['value'] }}</option>
												@endforeach
											</select>
										@endif

										@if ($field['field_type'] == 'checkbox' && $field['values'])
											@foreach ($field['values'] as $value)
												<div class="form-checkbox">	
													@if ($value['image'])
														<img src="{{ url('/').'/storage/'.$value['image'] }}">
													@endif

													@php 
														$selected_item = false; 
														
														if (in_array($value['value'], $selected_items)) {
															$selected_item = true;
														}
													@endphp

													<input type="checkbox" name="prescriptions[{{ $prescription_index2 }}][fields][{{ $field['id'] }}][{{ $value['id'] }}]" id="prescription_{{ $prescription_index2 }}_{{ $field['id'] }}_{{ $value['id'] }}" value="{{ $value['value'] }}" @if ($selected_item){{ 'checked="checked"' }}@endif>
													<label for="prescription_{{ $prescription_index2 }}_{{ $field['id'] }}_{{ $value['id'] }}">{{ $value['value'] }}</label>
												</div>
											@endforeach
										@endif

										@if ($field['field_type'] == 'radio' && $field['values'])
											@foreach ($field['values'] as $value)
												<div class="form-checkbox">	
													@if ($value['image'])
														<img src="{{ url('/').'/storage/'.$value['image'] }}">
													@endif

													@php 
														$selected_item = false; 
														
														if (in_array($value['value'], $selected_items)) {
															$selected_item = true;
														}
													@endphp

													<input type="radio" name="prescriptions[{{ $prescription_index2 }}][fields][{{ $field['id'] }}]" id="prescription_{{ $prescription_index2 }}_{{ $field['id'] }}_{{ $value['id'] }}" value="{{ $value['value'] }}" @if ($selected_item){{ 'checked="checked"' }}@endif>
													<label for="prescription_{{ $prescription_index2 }}_{{ $field['id'] }}_{{ $value['id'] }}">{{ $value['value'] }}</label>
												</div>
											@endforeach
										@endif
						
									</div>
								</div>
								</div>
							@endforeach
						@endif

						<div class="form-line">	
							<label for="prescription_{{ $prescription_index2 }}_comment">{{ __('translations.comment') }}</label>
							<textarea name="prescriptions[{{ $prescription_index2 }}][comment]" id="prescription_{{ $prescription_index2 }}_comment" class="form-inut" placeholder="{{ __('translations.comment') }}" maxlength="1000">@if($prescription['comment']){{ $prescription['comment'] }}@endif</textarea>
						</div>
					</div>

					@if ($prescription['prescription_description'])
						<div class="panel-head panel-subhead panel-head-collapse panel-head-collapsed">{{ __('translations.prescription_instruction') }}</div>
						<div class="fx fxb panel-block panel-block-collapsed">
							{!! $prescription['prescription_description'] !!}
						</div>
					@endif

				</div>
			</div>
			@php $prescription_index2++; @endphp
		@endforeach
	@endif
</div>		
<div class="panel panel-transparent panel-buttons" style="@if(!isset($prescriptions) || (isset($prescriptions) && !count($prescriptions)) ){{ 'display: none;' }}@endif" id="create_order_button">
	<div class="form-line button-submit">	
		<div id="add_more_prescriptions" class="btn btn-primary" onclick="scrollToblock('service_id')">{{ __('translations.add_more_prescriptions') }}</div>								
		<input id="submit" type="submit" class="btn" value="@if(!isset($data->prescriptions) || (isset($data->prescriptions) && !count($data->prescriptions)) ){{ __('translations.order') }}@else{{ __('translations.save') }}@endif" style="margin-bottom: 0;">
	</div>
</div>