@php
	$branches = array();
        
        switch (Auth::user()->role_id) {
            case config('roles.clinic_head'):
            case config('roles.lab_head'):
                $branches = \App\Models\Branch::where('owner_id', Auth::user()->id)->pluck('id')->toArray();
                break;
            case config('roles.clinic_staff'):
            case config('roles.doctor'):
            case config('roles.lab_staff'):
            case config('roles.tech'):
                $staff = \App\Models\Branch_staff::where('user_id', Auth::user()->id)->first();

                if ($staff) $branches[] = $staff->branch_id;                            
                break;
        }

        $viewed = \App\Models\Notification_view::select('notification_id')
            ->where('user_id', Auth::user()->id)
            ->pluck('notification_id')
            ->toArray();

        $notifications = \App\Models\Notification_message::whereIn('branch_id', $branches)
            ->whereNotIn('id', $viewed)
            ->where('created_at', '<=', date('Y-m-d').' 23:59:59')
            ->select('id')
            ->count();


@endphp
@if ($notifications) 
	<a href="{{ route('notifications') }}" class="fx fxc bi head-notifications" title="{{ __('translations.notifications') }}">
		<div class="fx fxc head-notifications-count">{{ $notifications > 99 ? '99+' : $notifications }}</div>
	</a>
@endif