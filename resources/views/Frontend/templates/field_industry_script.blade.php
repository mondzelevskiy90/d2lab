<script>
	document.addEventListener('DOMContentLoaded', function() {		
		$('#industry').on('change', function(){
			var value = $(this).val();
			
			$.ajax({
				url: '/ajax/getData',
				type: 'post',
                dataType: 'json',
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                },
			    data: {parent_id: value, action: 'getIndustries'},
				success: function (data) {
					if (data) {
						var html = '';

						for (index in data) {
							//console.log(data[index]);
							html += '<option value="'+data[index].id+'">'+data[index].name+'</option>';
						}

						$('#industry_id').html(html);
					}					
				}
			});
		});
			
		
	}, !1);
</script>