<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="viewport" content="width=device-width, minimum-scale=1, maximum-scale=1, initial-scale=1">
	@if (isset($seo) && $seo)
		<title>{{ $seo->meta_title." - " }}Dentist2Lab</title>
		<meta name="description" content="{{ $seo->meta_description }}, Dentist2Lab">
	@else
		<title>Dentist2Lab</title>
		<meta name="description" content="Dentist2Lab">
	@endif
    <link rel="icon" href="/favicon.ico" type="image/x-icon"/>
    <link rel="canonical" href="{{ url()->current() }}" />
    <base href="{{ url('/') }}"/>
	<link rel="stylesheet" href="{{ mix('/css/styles.css') }}">
	@yield('css')
</head>