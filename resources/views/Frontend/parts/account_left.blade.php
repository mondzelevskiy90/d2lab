<div id="account_menu" class="account-menu @if (Session::has('accountmenuclosed')) account-menu-cl @endif">
	<div id="account_menu_control" class="account-menu-control" onclick="transformAccountMenu()">
		<span></span>
		<span></span>
		<span></span>
	</div>
	<div id="account_menu_btn" onclick="openMobMenu()">
		<span></span>
		<span></span>
		<span></span>
	</div>
	<div id="account_menu_content" class="account-menu-content">
		<a href="{{ route('account') }}" class="account-settings-dashbord">{{ __('translations.account') }}</a>
		<a target="_blank" href="catalog/dentistry/labs" class="account-settings-catalog">{{ __('translations.catalog') }}</a>
		@php 			
			$menu = config('account_access.'.Auth::user()->role_id); 
		@endphp
		
		@if ($menu)
			@foreach ($menu as $item)
			@php if($item == 'notifications') continue; @endphp
				<a href="{{ route($item) }}" class="account-settings-{{ $item }} @if(route($item) == url()->current()) account-item-active @endif">{{ __('translations.'.$item) }}</a>
			@endforeach 
		@endif	
		@if (!Session::has('accountmenuclosed'))
			@php 
				$banners = \App\Models\Banner::where('position', 2)->where('status', 1)->orderBy('sort_order', 'ASC')->get();
			@endphp
			@if ($banners)
				<div class="fx fxc left-banners">
					@foreach ($banners as $banner)
						@if ($banner->image)
						<div class="banner-event left-banner" data-id="{{ $banner->id }}" data-token="{{ csrf_token() }}">
							<a href="{{ $banner->link }}" target="_blank">
								<img src="{{ '/storage/'.$banner->image }}" alt="{{ $banner->name }}">
							</a>
						</div>
						@endif
					@endforeach
				</div>
			@endif	
		@endif
	</div>
</div>

<script>
    function openMobMenu() {
    	if ($('#account_menu_content').hasClass('account-menu-contentAct')) {
    		$('#account_menu_btn').removeClass('account-menu-btnAct');
    		$('#account_menu_content').removeClass('account-menu-contentAct');
    	} else {
			$('#account_menu_btn').addClass('account-menu-btnAct');
			$('#account_menu_content').addClass('account-menu-contentAct');
    	}
    }

	function transformAccountMenu() {
		if ($('#account_menu').hasClass('account-menu-cl')) {
			$.ajax({
				url: '/ajax/getData',
				type: 'post',
                dataType: 'json',
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                },
			    data: {action:'setSession', method:'remove', name:'accountmenuclosed', value:'false'}
			});

			$('#account_menu').removeClass('account-menu-cl');
			$('section').removeClass('section-menu-cl');
		} else {
		    $.ajax({
				url: '/ajax/getData',
				type: 'post',
                dataType: 'json',
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                },
			    data: {action:'setSession', method:'add', name:'accountmenuclosed', value:'true'}
			});

			$('#account_menu').addClass('account-menu-cl');
			$('section').addClass('section-menu-cl');
		}
	}
</script>