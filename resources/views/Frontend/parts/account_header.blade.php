<div class="fx fac fsb">
	<div class="logo-block">
		<div class="bi logo" style="background-image: url('/img/logo.png')">
			@if (Auth::check())
				<a href="{{ route('account') }}"></a>
			@elseif (url()->current() != url('/'))
				<a href="{{ url('/') }}"></a>			
			@endif
		</div>
		<div class="logo-tagline">{{ __('translations.logo_tagline') }}</div>		
	</div>
	<div class="fx fxb fac auth-block">		
		@if (Auth::user()->role_id == config('roles.guest'))
			<div class="auth-block-upgrade">
				<a class="btn btn-danger" href="{{ route('settings') }}">{{ __('translations.upgrade') }}</a>
			</div>
		@else 
		    @include('Frontend.templates.head_notifications')
		@endif
	    <div class="auth-block-name">
			<a href="{{ route('account') }}">
				{{ Auth::user()->name }}
				<div>{{ \App\Models\Role::where('id', Auth::user()->role_id)->first()->display_name }}</div>
			</a>
	    </div>		
		<div class="fx fxc auth-block-logo">
			<a href="{{ route('account') }}">
				<img src="{{ \App\Services\Img::getIcon(Auth::user()->avatar) }}">
			</a>
		</div>	
		<div class="auth-block-logout">		
			<a href="{{ route('logout') }}">{{ __('translations.logout') }}</a>	
		</div>	
	</div>
</div>