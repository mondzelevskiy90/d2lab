@php 
	$banners = \App\Models\Banner::where('position', 1)->where('status', 1)->orderBy('sort_order', 'ASC')->get();
@endphp
@if ($banners)
	<div class="footer-banners">
		<div class="content">
			<div class="fx fxc">
				@foreach ($banners as $banner)
					@if ($banner->image)
					<div class="banner-event footer-banner" data-id="{{ $banner->id }}" data-token="{{ csrf_token() }}">
						<a href="{{ $banner->link }}" target="_blank">
							<img src="{{ '/storage/'.$banner->image }}" alt="{{ $banner->name }}">
						</a>
					</div>
					@endif
				@endforeach
			</div>
		</div>
	</div>
@endif
<footer>
	<div class="content">
		<div class="fx fxb">
			<div class="logo-block footer-logo">
				<div class="bi logo" style="background-image: url('/img/logo.png')"></div>
			</div>
			<div class="footer-links">
				<a href="{{ url('article/about') }}">{{ __('translations.about_us') }}</a>
				<a href="{{ url('article/terms') }}">{{ __('translations.terms') }}</a>
				<a href="{{ url('article/privacy-policy') }}">{{ __('translations.private_policy') }}</a>
				<a href="{{ url('article/faqs') }}">{{ __('translations.faq') }}</a>
				<a href="{{ url('sitemap') }}">{{ __('translations.sitemap') }}</a>
			</div>
			<div class="footer-socials">
				<a href="/" class="bi fcb"></a>
				<a href="/" class="bi twtr"></a>
				<a href="/" class="bi inst"></a>
			</div>
		</div>
	</div>
</footer>
<div id="loader" class="fx fxc">
	<div class="bi"></div>
</div>		
<script src="/js/common.js" defer></script>		
@yield('scripts')
@if (session('message'))
	<script>
		document.addEventListener('DOMContentLoaded', function() {
			showPopup('<div class="{{ session('alert-type') }}">{!! session('message') !!}</div>');
		}, !1);
	</script>	        
@endif