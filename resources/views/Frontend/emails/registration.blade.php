<h2>Welcome to Dentist2Lab service!</h2>
@if ($type)
@php 
	if ($type == config('types.clinic')) $account = 'Dentist';
	if ($type == config('types.lab')) $account = 'Lab';
@endphp
<p>Your account type is {{ $account }}.</p>
@else 
<p>Your account type is Guest.</p>
@endif

<p><a href="{{ $url }}">Confirm your e-mail</a></p>
<br>
<p>Warning! Do not share your login details to other people!</p>