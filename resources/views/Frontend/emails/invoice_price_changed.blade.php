<p>Your Invoice for <a href="{{ $order_url }}">Order #{{ $order_id }}</a> has been changed by lab!</p>
<p><a href="{{ $url }}">View changes</a></p>