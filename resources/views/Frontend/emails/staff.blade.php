<h2>Welcome to Dentist2Lab service!</h2>

<p>{{ $owner }} has invited you to our service as {{ $role }}.</p>
<p>Your login details:</p>
<p>Login: {{ $email }}</p>
<p>Password: {{ $password }}</p>

<p><a href="{{ $url }}">Sign in!</a></p>
<br>
<p>Warning! Do not share your login details to other people!</p>