<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
	@include('Frontend.parts.head')
	<body>
		<div class="fx fxb wrapper">
			<header>
				@include('Frontend.parts.account_header')
			</header>
			<aside>
				@include('Frontend.parts.account_left')
			</aside>
			<section class="@if (Session::has('accountmenuclosed')) section-menu-cl @endif">
				@yield('breadcrumbs')
				@yield('content')
			</section>
			@include('Frontend.parts.footer')
		</div>		
	</body>
</html>