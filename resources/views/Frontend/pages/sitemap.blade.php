@extends('Frontend/master')

@section('css')
	<link rel="stylesheet" href="{{ mix('/css/catalog.css') }}">
@stop

@section('breadcrumbs')
	@include('Frontend.templates.breadcrumbs')
@stop

@section('content')
	<h1>{{ __('translations.sitemap') }}</h1>
	<div class="page-text">
		<ul>			
			@foreach($data as $item)
				<li><a href="{{ url($item['href']) }}">{{ $item['name'] }}</a></li>
			@endforeach
		</ul>
		<div class="pagination-block">
			{{ $paginator->links() }}
		</div>
	</div>
@stop

@section('scripts')

@stop