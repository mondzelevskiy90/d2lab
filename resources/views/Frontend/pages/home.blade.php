@extends('Frontend/master')

@section('css')
	<link rel="stylesheet" href="{{ mix('/css/home.css') }}">
@stop

@section('after_head')
	<div class="bi main-top">
		<div class="fx fxb content">
			<div class="main-top-left">
				<div class="main-top-head">
					{{ __('translations.see_how') }}
				</div>
				<div class="main-top-content">
					<iframe width="100%" height="315" src="https://www.youtube.com/embed/sumzF2gIe0o" frameborder="0" allow="accelerometer; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
				</div>
			</div>
			<div class="fx fxc main-top-right">
				<div class="main-top-head">
					{{ __('translations.start_free') }}
				</div>
				<div class="main-top-content">
					<a href="{{ url('registration?type=3') }}" class="bi btn">{{ __('translations.reg_type3') }}</a>
					<a href="{{ url('registration?type=2') }}" class="bi btn btn-primary">{{ __('translations.reg_type2') }}</a>
				</div>
			</div>
		</div>
	</div>
@stop

@section('content')
	<div class="main-introtext">
		<div class="main-introtext-text">
			{!! __('translations.introtext') !!}
		</div>
		<div class="main-introtext-link">
			<a href="{{ url('catalog/dentistry/labs') }}" class="btn">{{ __('translations.view_all_labs') }}</a>
		</div>
	</div>
	<div class="main-counters">
		<div class="main-counters-head">
			{{ __('translations.current_users') }}
		</div>
		<div class="fx fxb main-counters-items">
			<div class="main-counters-item main-counters-item1">
				<div class="bi fx fac main-counters-itemImg">+{{ $registered }}</div>
				<div class="main-counters-itemTitle">{{ __('translations.users_reg') }}</div>
			</div>
			<div class="main-counters-item main-counters-item2">
				<div class="bi fx fac main-counters-itemImg">+{{ $clinics }}</div>
				<div class="main-counters-itemTitle">{{ __('translations.clinics') }}</div>
			</div>
			<div class="main-counters-item main-counters-item3">
				<div class="bi fx fac main-counters-itemImg">+{{ $labs }}</div>
				<div class="main-counters-itemTitle">{{ __('translations.labs') }}</div>
			</div>
		</div>
	</div>
@stop

@section('scripts')

@stop