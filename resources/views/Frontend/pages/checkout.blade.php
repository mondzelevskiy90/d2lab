@extends('Frontend/master')

@section('css')
	<link rel="stylesheet" href="/css/select.css">
	<link rel="stylesheet" href="{{ mix('/css/checkout.css') }}">	
	<link rel="stylesheet" href="/css/daterangepicker.css">
@stop

@section('breadcrumbs')
	@include('Frontend.templates.breadcrumbs')
@stop

@section('content')
	<h1>{{ $seo->name }}</h1>
	<div class="page-text">
		@if ($branch)
			<form method="POST" action="{{ route('checkout.create') }}" enctype="multipart/form-data" id="order_editing_form">
				@include('Frontend.templates.checkout_form')
			</form>
		@elseif (!$branch && $mylabs && count($mylabs))
			<form method="GET" action="{{ route('checkout') }}">
				<div class="fx fxb panel-body">
					<div class="panel">
						<div class="form-line">	
							<label for="branch_id">{{ __('translations.lab') }}</label>			
							<select name="branch_id" id="branch_id" class="form-input" required="required">
								<option value="0">{{ __('translations.no_lab_selected') }}</option>
								@foreach($mylabs as $mylab)
									@if ($mylab->branch)
										<option value="{{ $mylab->branch->id }}">{{ $mylab->branch->name }}</option>
									@endif
								@endforeach
							</select>
						</div>
					</div>
				</div>
				<div class="panel panel-transparent panel-buttons">
					<div class="form-line button-submit">				
						<input type="submit" id="submit" class="btn" value="{{ __('translations.select_lab') }}" onclick="return document.getElementById('branch_id').value == 0 ? false : true;">
					</div>
				</div>
			</form>
		@else
			<div class="fx fxb panel-body">
				<div class="panel panel-transparent">
					<br><br>
					<p style="text-align:center">{{ __('translations.no_lab_to_select') }}</p>
					<br>
					<br>
					<p style="text-align:center"><a class="btn" href="{{ url('catalog/dentistry/labs') }}">{{ __('translations.view_all_labs') }}</a></p>
				</div>
			</div>
		@endif
	</div>
@stop

@section('scripts')
	<script src="/js/select.js" defer></script>
	<script src="/js/moment.min.js" defer></script>
	<script src="/js/daterangepicker.js" defer></script>
	@include('Frontend.templates.checkout_scripts')
@stop