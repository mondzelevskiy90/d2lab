@extends('Frontend/master')

@section('css')
	<link rel="stylesheet" href="{{ mix('/css/catalog.css') }}">
	<link rel="stylesheet" href="/css/select.css">
@stop

@section('after_head')
	<div class="catalog-top">
		<form method="GET" action="{{ route('catalog.industry.type', [$industry->slug, $type->slug, $requests]) }}" id="catalog_filter">
			<div class="catalog-top-bottom">
				<div class="fx fxb content" style="position:relative;">					
					<div class="catalog-top-all">
						{{ $total.' '.ucfirst($type->catalog_name) }}
					</div>
					<div class="fx catalog-top-inputs">
						<!--
						<div class="catalog-input-line catalog-top-industry">
							<span class="catalog-input-label">{{ __('translations.industry') }}:</span>
							<select class="form-input" id="industry" onchange="showOtherIndustry()">
								<option value="{{ $industry->slug }}" default>{{ $industry->name }}</option>
							</select>
						</div>
						-->
						<div class="catalog-input-line catalog-top-zip">
							<span class="catalog-input-label">{{ __('translations.zip_code') }}:</span>
							<input type="text" name="zip_code" id="zip_code" class="form-input" value="@if($selected_filters['zip_code'] && $selected_filters['zip_code'] != ''){{ $selected_filters['zip_code'] }}@else{{''}}@endif" onchange="document.getElementById('catalog_filter').submit()" autocomplete="off">	
						</div>
						@if (!$selected_filters['zip_code'])
						<div class="catalog-input-line catalog-top-locality">
							<span class="catalog-input-label">{{ __('translations.city') }}:</span>
							<select class="form-input" name="locality_id" id="locality_id" onchange="changeLocality()">
								@if ($locality)
									<option value="{{ $locality->id }}" default>{{ $locality->name }}</option>
								@endif
								<option value="0"> --- </option>
							</select>
						</div>
						@endif
						<div class="catalog-input-line catalog-top-filter">
							@php
								$selected_filters_count = 0;
								
								foreach ($selected_filters as $key => $value) {
									if ($key != 'sort' && $value) $selected_filters_count++;
								}
							@endphp
							<div id="show_catalog_filter" class="catalog-top-filterBtn">
								{{ __('translations.filter') }}
								@if($selected_filters_count != 0)
									<span>({{ $selected_filters_count .' '. __('translations.selected') }})</span>
								@endif
							</div>							
						</div>
						
					</div>	
					<div id="filter">
						<div class="filter-head">{{ __('translations.filters') }}</div>
						<div class="fx catalog-inputs-line">
							<div class="catalog-input-line catalog-top-search">
								<span class="catalog-input-label">{{ __('translations.search') }}:</span>
								<input type="text" name="search" id="catalog_search" class="form-input" value="@if($selected_filters['search'] && $selected_filters['search'] != ''){{ $selected_filters['search'] }}@else{{''}}@endif" autocomplete="off">
							</div>
							<div class="catalog-input-line catalog-top-category">
								<span class="catalog-input-label">{{ __('translations.category') }}:</span>
								<select class="form-input" name="service_id" id="service_id">
									<option value="0"> --- </option>
									@foreach ($services as $service)
										<option value="{{ $service->id }}" @if($selected_filters['service'] && $selected_filters['service'] == $service->id){{ 'selected' }}@endif>{{ $service->name }}</option>
									@endforeach
								</select>
							</div>	
							<div class="catalog-input-line catalog-top-options">
								<span class="catalog-input-label">{{ __('translations.options') }}:</span>
								<select class="form-input" name="options" id="option_input">
									<option value="0"> --- </option>
									@foreach ($options as $option)
										<option value="{{ $option->id }}" @if($selected_filters['options'] && $selected_filters['options'] == $option->id){{ 'selected' }}@endif>{{ $option->name }}</option>
									@endforeach
								</select>
							</div>					
							<div class="catalog-input-line catalog-top-sort">
								<span class="catalog-input-label">{{ __('translations.sort_by') }}:</span>
								<select class="form-input" name="sort" id="sort_input">
									<option value="rating_desc" @if($selected_filters['sort'] && $selected_filters['sort'] == 'rating_desc'){{ 'selected' }}@endif>{{ __('translations.rating') }} - Top</option>
									<option value="rating_asc" @if($selected_filters['sort'] && $selected_filters['sort'] == 'rating_asc'){{ 'selected' }}@endif>{{ __('translations.rating') }} - End</option>
									<option value="name_asc" @if($selected_filters['sort'] && $selected_filters['sort'] == 'name_asc'){{ 'selected' }}@endif>{{ __('translations.name') }} A to Z</option>
									<option value="name_desc" @if($selected_filters['sort'] && $selected_filters['sort'] == 'name_desc'){{ 'selected' }}@endif>{{ __('translations.name') }} Z to A</option>
								</select>
							</div>
						</div>
						@if (Auth::check())							
							<div class="fx fxb catalog-inputs-line">								
								<div class="catalog-input-line">
									<span class="catalog-input-label">{{ __('translations.price') .' '. __('translations.from') }}:</span>
									<input type="text" name="price_from" id="price_from" class="form-input" value="@if($selected_filters['price_from'] && $selected_filters['price_from'] != ''){{ $selected_filters['price_from'] }}@else{{''}}@endif" autocomplete="off" onchange="isNaN(this.value) ? this.value = null : this.value = this.value;">
									<span class="catalog-input-symbol">{{ $currency->symbol }}</span>
								</div>
								<div class="catalog-input-line">
									<span class="catalog-input-label">{{ __('translations.price') .' '. __('translations.to') }}:</span>
									<input type="text" name="price_to" id="price_to" class="form-input" value="@if($selected_filters['price_to'] && $selected_filters['price_to'] != ''){{ $selected_filters['price_to'] }}@else{{''}}@endif" autocomplete="off" onchange="isNaN(this.value) ? this.value = null : this.value = this.value;">
									<span class="catalog-input-symbol">{{ $currency->symbol }}</span>
								</div>												
								<div class="catalog-input-line">
									<span class="catalog-input-label">{{ __('translations.rating') .' '. __('translations.from') }}:</span>
									<select class="form-input" name="rating_from" id="rating_from">
										<option value="0" @if($selected_filters['rating_from'] && $selected_filters['rating_from'] == 0){{ 'selected' }}@endif default>{{ __('translations.none') }}</option>
										<option value="1" @if($selected_filters['rating_from'] && $selected_filters['rating_from'] == 1){{ 'selected' }}@endif>&#8902;</option>
										<option value="2" @if($selected_filters['rating_from'] && $selected_filters['rating_from'] == 2){{ 'selected' }}@endif>&#8902; &#8902;</option>
										<option value="3" @if($selected_filters['rating_from'] && $selected_filters['rating_from'] == 3){{ 'selected' }}@endif>&#8902; &#8902; &#8902;</option>
										<option value="4" @if($selected_filters['rating_from'] && $selected_filters['rating_from'] == 4){{ 'selected' }}@endif>&#8902; &#8902; &#8902; &#8902;</option>
										<option value="5" @if($selected_filters['rating_from'] && $selected_filters['rating_from'] == 5){{ 'selected' }}@endif>&#8902; &#8902; &#8902; &#8902; &#8902;</option>
									</select>
								</div>
								<div class="catalog-input-line">
									<span class="catalog-input-label">{{ __('translations.rating') .' '. __('translations.to') }}:</span>
									<select class="form-input" name="rating_to" id="rating_to">
										<option value="5" @if($selected_filters['rating_to'] && $selected_filters['rating_to'] == 5){{ 'selected' }}@endif default>&#8902; &#8902; &#8902; &#8902; &#8902;</option>
										<option value="4" @if($selected_filters['rating_to'] && $selected_filters['rating_to'] == 4){{ 'selected' }}@endif>&#8902; &#8902; &#8902; &#8902;</option>
										<option value="3" @if($selected_filters['rating_to'] && $selected_filters['rating_to'] == 3){{ 'selected' }}@endif>&#8902; &#8902; &#8902;</option>
										<option value="2" @if($selected_filters['rating_to'] && $selected_filters['rating_to'] == 2){{ 'selected' }}@endif>&#8902; &#8902;</option>									
										<option value="1" @if($selected_filters['rating_to'] && $selected_filters['rating_to'] == 1){{ 'selected' }}@endif>&#8902;</option>
									</select>
								</div>
							</div>							
						@endif
						<div class="filter-btn-line">	
							<input type="submit" class="btn" value="{{ __('translations.filter_btn') }}"/>
							&nbsp;
							<a class="btn btn-primary" href="{{ route('catalog.industry.type', [$industry->slug, $type->slug]) }}">{{ __('translations.clear_filter') }}</a>
						</div>
					</div>
				</div>
			</div>
			{{ csrf_field() }}
		</form>		
	</div>
@stop

@section('breadcrumbs')
	@include('Frontend.templates.breadcrumbs')
@stop

@section('content')
    <div class="fx fxb catalog-headline">
    	<h1>{{ $seo->name }}</h1>
    	@if (count($data))
			<div class="catalog-show-allOnMap">
				<div onclick="showAllOnMap()">{{ __('translations.view_all_on_map') }}</div>
			</div>
		@endif	
    </div>	
	<div class="page-text">
		@if (count($data))
			<div class="catalog-items">
				@foreach($data as $item)
					@php 
						$href = route('branch', [$type->slug, $item->id]);
					@endphp
					<div class="fx fxb catalog-item">
						<div class="catalog-item-left">
							<div class="catalog-item-logo" style="background-image: url({{ App\Services\Img::resizeImage($item->logo, 235, 235) }})">
								<a href="{{ $href }}"></a>
							</div>
							<div class="catalog-item-link">
								<a href="{{ $href }}">{{ __('translations.view_profile') }}</a>
							</div>
						</div>
						<div class="catalog-item-right">
							<div class="fx fxb catalog-item-info">
								<div class="catalog-item-infoLeft">
									<div class="catalog-item-infoName">
										<a href="{{ $href }}">{{ $item->name }}</a>
									</div>
									<!--<div class="catalog-item-infoIndustry">
										{{ __('translations.industry') }}: {{ $industry->name }}
									</div> -->
									@if ($item->lat && $item->lon)
									<div class="catalog-item-infoMap">
										<div id="item_map{{ $item->id }}" onclick="showOnMap('item_map{{ $item->id }}')" data-lat="{{ $item->lat }}" data-lon="{{ $item->lon }}">{{ __('translations.view_on_map') }}</div>
									</div>
									@endif
								</div>
								<div class="fx fxb catalog-item-infoRigth">
								@if (Auth::check())									
									<div class="catalog-item-infoReviews">
										<div class="catalog-item-infoStars catalog-item-infoStars{{ floor($item->reviews_rating) }}"></div>
										{{ count($item->reviews) .' '. __('translations.reviews') }}
									</div>									
									<div class="catalog-item-infoRating">
										{{ __('translations.rating') }}<span>{{ $item->rating }}</span>
									</div>
								@else
									<div class="bi catalog-itemRatingMuted" onclick="showRegAlert();"></div>							
								@endif
								</div>
							</div>
							@if ($item->prices && count($item->prices))
								<div class="catalog-item-prices">
									<div class="catalog-item-pricesHead">{{ __('translations.prices') }}:</div>
									<div class="catalog-item-pricesBlock">
										@php 
											$cnt = 0;											
										@endphp
										@foreach($item->prices as $price)
											<div class="fx fxb catalog-item-priceLine">
												<div class="catalog-item-priceName"><span>{{ $price->name }}</span></div>
												<div class="catalog-item-priceValue">{{ $price->value }}</div>
											</div>
											@php 
												$cnt++;
												if($cnt == 4) break; 
											@endphp
										@endforeach
									</div>
									@if (count($item->prices) > 4)
										<div class="catalog-item-pricesLink">
											<a href="{{ $href }}#prices">{{ __('translations.more_prices') }}</a>
										</div>
									@endif
								</div>
							@endif
						</div>
					</div>
				@endforeach
			</div>
			<div class="pagination-block">{{ $data->appends(\Request::except('page'))->links() }}</div>
		@else
			<p>{{ __('translations.no_catalog_results') }}</p>
			<!--<div class="btn" onclick="window.history.back()">{{ __('translations.back') }}</a> -->
		@endif
	</div>
@stop

@section('pre_footer')
	@if (!Auth::check())
	<div class="bootom-content">
		<div class="bi bottom-banner">
			<div class="content">
				<div class="fx fxb">
					<div class="fx fxc bottom-banner-question">{{ __('translations.bottom_question') }}</div>
					<div class="fx fxc bottom-banner-answer">
						<a href="{{ route('login') }}" class="btn btn-primary">{{ __('translations.sign_up') }}</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	@endif
@stop

@section('scripts')
	<script src="/js/select.js" defer></script>
	<script src="https://maps.googleapis.com/maps/api/js?key={{ setting('admin.google_api_maps_key') }}"
    async defer></script>
    <script src="/js/masked.js" defer></script>
	<script>
		document.addEventListener('DOMContentLoaded', function() {
			$('#zip_code').mask('99999');

			$('#industry').select2({
				minimumInputLength: 2,
				ajax: {
					url: '/ajax/getData',
					type: 'post',
	                dataType: 'json',
	                headers: {
	                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
	                },
				    data:function(params) {
				      return {
				      	action: 'getIndustriesToSelect',
				        search: params.term,
				      };
				    },
					processResults: function (data) {
						return {
							results: data
						};
					}
				}
			});

			$('#service_id').select2({
				minimumInputLength: 0,
				ajax: {
					url: '/ajax/getData',
					type: 'post',
	                dataType: 'json',
	                headers: {
	                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
	                },
				    data:function(params) {
				      return {
				      	action: 'getServicesByIndustryToSelect',
				        search: params.term,
				        industry: {{ $industry->id }}
				      };
				    },
					processResults: function (data) {
						return {
							results: data
						};
					}
				}
			});

			$('#option_input').select2({
				minimumInputLength: 0,
				ajax: {
					url: '/ajax/getData',
					type: 'post',
	                dataType: 'json',
	                headers: {
	                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
	                },
				    data:function(params) {
				      return {
				      	action: 'getOptionsByIndustryToSelect',
				        search: params.term,
				        industry: {{ $industry->id }}
				      };
				    },
					processResults: function (data) {
						return {
							results: data
						};
					}
				}
			});

			$('#locality_id').select2({
				minimumInputLength: 0,
				ajax: {
					url: '/ajax/getData',
					type: 'post',
	                dataType: 'json',
	                headers: {
	                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
	                },
				    data:function(params) {
				      return {
				      	action: 'getLocalitiesToSelect',
				        search: params.term
				      };
				    },
					processResults: function (data) {
						return {
							results: data
						};
					}
				}
			});

			$('#sort_input').select2({
				minimumResultsForSearch: -1
			});

			$('#rating_from').select2({
				minimumResultsForSearch: -1
			});

			$('#rating_to').select2({
				minimumResultsForSearch: -1
			});

			$('#show_catalog_filter').on('click', function() {
				$(this).toggleClass('catalog-top-filterBtnOp');
				$('#filter').slideToggle();
			});
		}, !1);

		function showOnMap(id) {
			@if (Auth::check())		
				var lat = $('#'+id).data('lat'),
					lon = $('#'+id).data('lon');

				$.colorbox({
					html: '<div id="map"></div>',
					onComplete : function() {
						$(this).colorbox.resize();

						var position = {lat: lat, lng: lon};

						var mapOptions = {
						    center: position,
						    zoom: 15,
						    mapTypeId: google.maps.MapTypeId.ROADMAP
						};

						var map = new google.maps.Map(document.getElementById('map'), mapOptions);
						var marker = new google.maps.Marker({position: position, map: map});
					}
				});
			@else 
				showRegAlert();
			@endif
		}

		function showAllOnMap() {
			@if (Auth::check())
				$.ajax({
					url: '/ajax/getData',
					type: 'post',
	                dataType: 'json',
	                headers: {
	                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
	                },
				    data: {action:'getGeoLocationsById', ids: '{{ $geolocations }}'},
				    success: function(data) {
				    	if (data) {
							$.colorbox({
								html: '<div id="map"></div>',
								onComplete : function() {
									$(this).colorbox.resize();

									var position = {lat: parseFloat(data[0]['lat']), lng: parseFloat(data[0]['lon'])};

									var mapOptions = {
									    center: position,
									    zoom: 9,
									    mapTypeId: google.maps.MapTypeId.ROADMAP
									};

									var map = new google.maps.Map(document.getElementById('map'), mapOptions);

									for(index in data) {
										var marker_position = {lat: parseFloat(data[index]['lat']), lng: parseFloat(data[index]['lon'])};

										new google.maps.Marker({position: marker_position, map: map});
									}
									
								}
							});
						}
					}
				});
			@else 
				showRegAlert();
			@endif
		}

		function showRegAlert() {
			showPopup('<div class="error">{!! __('translations.only_reg_view') !!}</div>');
		}

		function showOtherIndustry() {
			var href = "{{ route('catalog.industry.type', [$industry->slug, $type->slug]) }}",
				value = document.getElementById('industry').value;

			href = href.replace('{{ $industry->slug }}', value);
			window.location.href = href;
		}

		function changeLocality() {
			$.ajax({
				url: '/ajax/getData',
				type: 'post',
                dataType: 'json',
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                },
			    data: {action:'setSession', method:'add', name:'location', value: $('#locality_id').val()},
			    success: function(data) {
			    	$('#catalog_filter').submit()
			    }
			});
		}
	</script>
@stop