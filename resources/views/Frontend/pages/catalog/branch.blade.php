@extends('Frontend/master')

@section('css')
	<link rel="stylesheet" href="{{ mix('/css/branch.css') }}">
@stop

@section('after_head')
	<div class="branch-top">
		<div class="fx fxb content">
			<div class="bi branch-logo" style="background-image: url({{ App\Services\Img::squareImage($data->logo, 75, 75) }})"></div>
			<div class="fx branch-menu">
				<div onclick="scrollToblock('summary')">{{ __('translations.summary') }}</div>
				<div onclick="scrollToblock('prices')">{{ __('translations.prices') }}</div>
				@if (count($data->portfolio))
					<div onclick="scrollToblock('portfolio')">{{ __('translations.portfolio') }}</div>
				@endif
				@if (!Auth::check() || count($data->reviews))
					<div onclick="scrollToblock('reviews')">{{ __('translations.reviews') }}</div>
				@endif
			</div>
		</div>
	</div>
@stop

@section('breadcrumbs')
	@include('Frontend.templates.breadcrumbs')
@stop

@section('content')
	<h1>{{ $seo->name }}</h1>
	<div class="page-text">
		<div id="summary">
			<div class="fx fxb branch-block-content">
	            <div class="fx fxb branch-summary-left">
	            	@if (Auth::check())
	            		<div class="fx branch-summary-rating">		            		
		            		<div class="branch-info-rating">
								{{ __('translations.rating') }}<span>{{ $data->rating }}</span>
							</div>								
							<div class="branch-info-reviews">
								<div class="bi branch-info-stars branch-info-stars{{ floor($data->reviews_rating) }}"></div>
								{{ count($data->reviews) .' '. __('translations.reviews') }}
							</div>
						</div>
						@if (Auth::user()->type == config('types.clinic'))
							<div id="branch_my_lab" class="branch-my-lab">									
								@if($mybranches)
									<div class="branch-list-remove">	
										{{ __('translations.added_to_labs') }}<br>					
										<div id="branch_list_action" class="btn btn-danger branch-list-action" data-branch="{{ $data->id }}" data-method="remove">{{ __('translations.remove') }}</div>
									</div>
								@else
									<div class="branch-list-add">
										<div id="branch_list_action" class="btn branch-list-action" data-method="add">
											<span>+</span>
											{{ __('translations.add_to_labs') }}
										</div>
									</div>
								@endif	
							</div>
						@endif
					@else
						<div class="bi branch-info-ratingMuted" onclick="showRegAlert();"></div>
					@endif
					@if ($data->description && $data->description != '')
						<div class="branch-info-description">
							{!! $data->description !!}
						</div>
					@endif
	            </div>
	            <div class="branch-summary-right">
	            	@if ($data->geolocation)
						<div id="map" @if(!Auth::check()) class="bi branch-map-muted" onclick="showRegAlert();" @endif></div>
					@endif
					<div class="branch-summary-icons">
						@if ($data->verified && $data->verified == 1)
							<div class="bi branch-summary-icon branch-summary-verified">{{ __('translations.verified') }}</div>
						@endif
						@if (Auth::check())	
							@if ($data->apl)
								<div class="bi branch-summary-icon branch-summary-apl" title="{{ $data->apl['title'] }}">{{ $data->apl['value'] }}</div>
							@endif
							<div class="bi branch-summary-icon branch-summary-email">{{ $data->email }}</div>
							<div class="bi branch-summary-icon branch-summary-phone">{{ $data->phone }}</div>
							<div class="bi branch-summary-icon branch-summary-location">{{ $data->zip_code .', '. $data->address .', '. $data->location_name }}</div>
						@endif
					</div>
	            </div>   
			</div>
		</div>
		@if ($data->prices && count($data->prices))
		<div id="prices">
			@if ($branch_focus)
			<div class="branch-focus-block">
				<div class="fx branch-focus-body">
					@foreach ($branch_focus as $item)
						<div class="branch-focus-service" style="width: {{ $item['percent'] }}%; background-color: {{ $item['color'] }};" title="{{ $item['name'] }}"></div>
					@endforeach					
				</div>
				@foreach ($branch_focus as $item)
					<div class="branch-focus-main">
						{{ $item['percent'] }}% {{ $item['name'] }}
					</div>
					@if ($loop->first)
						@break
					@endif
				@endforeach
			</div>
			@endif
			@if (Auth::check() && Auth::user()->type == config('types.clinic'))
			<div class="branch-checkout-link">
				<a class="btn" href="{{ route('checkout').'?branch_id='.$data->id }}">{{ __('translations.order') }}</a>
			</div>	
			@elseif (Auth::check() && Auth::user()->role_id == config('roles.guest'))
				<div class="branch-checkout-link">
					<a class="btn" href="{{ route('checkout') }}" onclick="showPopup('{{ __('translations.only_lab_order') }}'); return false;">{{ __('translations.order') }}</a>
				</div>
			@endif 		
			<div class="branch-block-head">{{ __('translations.prices') }}</div>
			<div class="branch-block-content">
				<div class="branch-prices-block">
					@foreach($data->prices as $price_cat)
						@if ($price_cat && isset($price_cat['name']))
						<div class="bi branch-prices-head">{{ $price_cat['name'] }}</div>
						<div class="branch-prices-prices">
							@foreach ($price_cat['prices'] as $price)
								<div class="fx fxb branch-prices-priceLine">
									<div class="branch-prices-priceName"><span><strong>{{ $price['name'] }}</strong> @if($price['options'] && $price['options'] != '')({{ $price['options'] }})@endif</span></div>
									<div class="branch-prices-priceValue">{{ $price['value'] }}</div>
									@if($price['show_prices_message'])
										<div style="color:red;width:100%;font-size:11px;text-align:right;">
											*** One or more your locations has custom price for this service!
										</div>
									@endif
								</div>
							@endforeach
						</div>
						@endif
					@endforeach
				</div>
			</div>
		</div>
		@endif
		@if (count($data->portfolio))
			<div id="portfolio">
				<div class="branch-block-head">{{ __('translations.portfolio') }}</div>
				<div class="branch-block-content">
					@foreach($data->portfolio as $portfolio)
						@php
							$images = explode(',', $portfolio->images);
						@endphp
						<div class="branch-portfolio-line">
							<div class="branch-portfolio-head" @if (count($data->portfolio) == 1) style="display:none;" @endif>
								{{ $portfolio->service->name }}
							</div>
							<div class="fx branch-portfolio-images">
								@foreach($images as $image)
									<div class="branch-portfolio-image">
										<a href="{{ '/storage/'.$image }}" class="popup-link">
											<div class="bi" style="background-image: url({{ \App\Services\Img::squareImage($image, 360, 360) }})"></div>
										</a>
									</div>
								@endforeach
							</div>
						</div>
					@endforeach
				</div>
			</div>
		@endif

		@if (Auth::check()) 
			@if (count($data->reviews))
				<div id="reviews">
					<div class="branch-block-head">{{ __('translations.reviews') }}</div>
					<div class="branch-block-content">
						<div class="branch-reviews">
							<div class="fx fac branch-reviews-totals">
								<div class="branch-reviews-reviewsRating">
									{{ $data->reviews_rating }}
								</div>
								<div class="branch-reviews-reviewsStars">
									<div class="bi branch-info-stars branch-info-stars{{ floor($data->reviews_rating) }}"></div>
								</div>
								<div class="branch-reviews-reviewsCount">
									{{ count($data->reviews) }} {{ __('translations.reviews') }}
								</div>
							</div>
							@include('Frontend.templates.review_block')
						</div>
					</div>
				</div>
			@endif
		@else 
			<div id="reviews">
				<div class="branch-block-head">{{ __('translations.reviews') }}</div>
				<div class="branch-block-content">
					<div class="fx fxc bi branch-reviews-muted">
						<span>{!! __('translations.only_reg_view') !!}</span>
					</div>
				</div>
			</div>
		@endif
	</div>
@stop

@section('scripts')	
	<script>
		document.addEventListener('DOMContentLoaded', function() {
			@if(Auth::check())
			$(document).on('click', '#branch_list_action', function(){
				var method = $(this).data('method'),
					user_id = '{{ Auth::user()->id }}';				

				$.ajax({
					url: '/ajax/getData',
					type: 'post',
	                dataType: 'json',
	                headers: {
	                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
	                },
				    data: {action:'addToMyLabs', method: method, branch_id: '{{ $data->id }}', user_id: user_id},
				    success: function(data) {
				    	var html = '';

				    	if (data) {
				    		if (method == 'add') {
				    			html += '<div class="branch-list-remove">';
				    			html += '{{ __('translations.added_to_labs') }}<br>';
				    			html += '<div id="branch_list_action" class="btn btn-danger branch-list-action" data-branch="{{ $data->id }}" data-method="remove">{{ __('translations.remove') }}</div>';
				    			html += '</div>';
				    		}

				    		if (method == 'remove') {
				    			html += '<div class="branch-list-add">';
				    			html += '<div id="branch_list_action" class="btn branch-list-action" data-method="add">';
				    			html += '<span>+</span>';
				    			html += '{{ __('translations.add_to_labs') }}';
				    			html += '</div>';
				    			html += '</div>';
				    		}

				    		showPopup('<div class="success">{!! __('translations.success') !!}</div>');
				    	}

				    	$('#branch_my_lab').html(html);
				    }
				});
			});
			@endif
		}, !1);

		function showRegAlert() {
			showPopup('<div class="error">{!! __('translations.only_reg_view') !!}</div>');
		}

		function scrollToblock(id) {
			$('html, body').animate({
		        scrollTop: $('#'+id).offset().top
		    }, 500);
		}  
	</script>
	@if (Auth::check() && $data->geolocation && $data->geolocation != '{}')	
		<script src="https://maps.googleapis.com/maps/api/js?key={{ setting('admin.google_api_maps_key') }}&callback=initMap"
	    async defer></script>
		<script>
			function initMap() {
				var position = JSON.parse('{!! $data->geolocation !!}');				
				var center = {lat: parseFloat(position.lat), lng: parseFloat(position.lon)};
				var mapOptions = {
				    center: center,
				    zoom: 13,
				    mapTypeId: google.maps.MapTypeId.ROADMAP,				    
					mapTypeControl: false,
					streetViewControl: false,
					rotateControl: false
				};
				var map = new google.maps.Map(document.getElementById('map'), mapOptions);
				var marker = new google.maps.Marker({position: center, map: map});
			}
		</script>
    @endif
@stop