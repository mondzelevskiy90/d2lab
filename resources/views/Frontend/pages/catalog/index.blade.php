@extends('Frontend/master')

@section('css')
	<link rel="stylesheet" href="{{ mix('/css/catalog.css') }}">
	<link rel="stylesheet" href="/css/select.css">
@stop

@section('breadcrumbs')
	@include('Frontend.templates.breadcrumbs')
@stop

@section('content')
	<h1>{{ $seo->name }}</h1>
	<div class="page-text">
		<ul>
			@foreach($data as $item)				
				<li>					
					<span>{{ $item->name }}</span>
					<ul>
					@if ($item->childrens)						
							@foreach($item->childrens as $children)
								<li>
									<span>{{ $children->name }}</span>
									<ul>
										@foreach($types as $type)
											@if ($type['catalog'] == 1)
												<li><a href="{{ '/catalog/'.$children->slug.'/'.$type['slug'] }}">{{ __('translations.find_type').' '.$type['name'] }}</a></li>
											@endif
										@endforeach
									</ul>
								</li>
							@endforeach						
					@else
						@foreach($types as $type)
							@if ($type->catalog == 1)
								<li><a href="{{ '/catalog/'.$industry->slug.'/'.$type['slug'] }}">{{ __('translations.find_type').' '.$type['name'] }}</a></li>
							@endif
						@endforeach
					@endif
					</ul>
				</li>
			@endforeach
		</ul>
	</div>
@stop

@section('scripts')
	
@stop