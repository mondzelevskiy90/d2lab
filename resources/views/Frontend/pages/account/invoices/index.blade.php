@extends('Frontend/account_master')

@section('css')
	<link rel="stylesheet" href="/css/select.css">
	<link rel="stylesheet" href="/css/daterangepicker.css">
	<link rel="stylesheet" href="{{ mix('/css/account.css') }}">
@stop

@section('breadcrumbs')
	@include('Frontend.templates.breadcrumbs')
@stop

@section('content')
	<h1 style="display: none;">{{ $seo->name }}</h1>	
	<div class="panel panel-transparent">
		<div id="order_filters_block" class="panel-filters" style="display: none;">
			<form method="GET" action="{{ route('invoices') }}" id="filters_block" class="fx fac">		
				{{ csrf_field() }}	
				@if ($branches && count($branches))
					<div class="form-filter">
						<select name="branch" id="branch" class="form-input" onchange="document.getElementById('filters_block').submit();">	
							<option value="0"> -- {{ __('translations.branch') }} --</option>
							@foreach($branches as $branch)
								@if($branch->status != 4)
									<option value="{{ $branch->id }}" @if(isset($filters['branch']) && $filters['branch'] == $branch->id){{ 'selected="selected"' }}@endif>{{ $branch->name }}</option>
								@endif
							@endforeach
						</select>		
					</div>
				@endif	
				<div class="form-filter">
					<select name="invoice_branch" id="invoice_branch" class="form-input" onchange="document.getElementById('filters_block').submit();">	
						<option value="0">
							@if (Auth::user()->type == config('types.clinic'))
								-- {{ __('translations.select').' '.__('translations.lab') }} --
							@endif
							@if (Auth::user()->type == config('types.lab'))
								-- {{ __('translations.select').' '.__('translations.clinic') }} --
							@endif
						</option>
						@foreach($invoice_branches as $invoice_branch)
							@if (Auth::user()->type == config('types.clinic'))
								<option value="{{ $invoice_branch->to_branch_id }}" @if(isset($filters['invoice_branch']) && $filters['invoice_branch'] == $invoice_branch->to_branch_id){{ 'selected="selected"' }}@endif>{{ $invoice_branch->to_branch->name }}</option>
							@endif
							@if (Auth::user()->type == config('types.lab'))
								<option value="{{ $invoice_branch->branch_id }}" @if(isset($filters['invoice_branch']) && $filters['invoice_branch'] == $invoice_branch->branch_id){{ 'selected="selected"' }}@endif>{{ $invoice_branch->branch->name }}</option>
							@endif					
						@endforeach
					</select>	
				</div>		
				<div class="form-filter">
					<select name="payment_type" id="payment_type" class="form-input">	
						<option value="0"> -- {{ __('translations.payment_type') }} --</option>
						@foreach($payment_types as $type)
							<option value="{{ $type->id }}" @if(isset($filters['payment_type']) && $filters['payment_type'] == $type->id){{ 'selected="selected"' }}@endif>{{ $type->name }}</option>
						@endforeach
					</select>		
				</div>
				<div class="form-filter">
					<select name="payment_status" id="payment_status" class="form-input">	
						<option value="0"> -- {{ __('translations.payment_status') }} --</option>
						@foreach($payment_statuses as $payment_status)
							<option value="{{ $payment_status->id }}" @if(isset($filters['payment_status']) && $filters['payment_status'] == $payment_status->id){{ 'selected="selected"' }}@endif>{{ $payment_status->name }}</option>
						@endforeach
					</select>		
				</div>		
				<div class="form-filter">
					<input type="text" name="from_date" id="from_date" class="form-input" placeholder="{{ __('translations.from_date') }}" value="@if(isset($filters['from_date']) && $filters['from_date']){{ $filters['from_date'] }}@endif">			
				</div>
				<div class="form-filter">
					<input type="text" name="to_date" id="to_date" class="form-input" placeholder="{{ __('translations.to_date') }}" value="@if(isset($filters['to_date']) && $filters['to_date']){{ $filters['to_date'] }}@endif">			
				</div>
				<div class="form-filter">
					<input type="text" name="search" id="search" class="form-input" placeholder="{{ __('translations.search_invoice_id') }}" value="@if(isset($filters['search']) && $filters['search']){{ $filters['search'] }}@endif">			
				</div>
				<div class="filter-buttons" style="text-align: right;">					
					<input type="submit" class="btn btn-small" value="{{ __('translations.filter') }}">
					<a href="{{ route('invoices') }}" class="btn btn-danger btn-small">{{ __('translations.clear') }}</a>
				</div>
			</form>
		</div>	

		<div class="fx fxb panel-buttons">
			<div id="show_filter_block" class="btn">{{ __('translations.filter_btn') }}</div>
		</div>
	</div>
	<div class="fx fxb panel-body">
		@if ($data && count($data) && $can_pay)
			<div class="panel panel-table-controls pay-controls" style="display: none;">
				<div class="btn btn-small btn-danger" id="pay_selected" onclick="paySelectedInvoices();">
					@if (Auth::user()->type == config('types.clinic'))
						{{ __('translations.pay_selected') }}
					@endif
					@if (Auth::user()->type == config('types.lab'))
						{{ __('translations.check_pay_selected') }}
					@endif
				</div>
			</div>
		@endif
		<div class="panel panel-table">
			@if ($data && count($data)) 
				<div class="account-list-table account-invoices-table">
					<div class="fx account-list-head">											
							<div class="account-list-cell"></div>
							<div class="account-list-cell">{{ __('translations.name') }}</div>
							<div class="account-list-cell">{{ __('translations.order_id') }}</div>
							<div class="account-list-cell">{{ __('translations.branch') }}</div>
							<div class="account-list-cell">
								@if (Auth::user()->type == config('types.clinic'))
									{{ __('translations.lab') }}
								@endif
								@if (Auth::user()->type == config('types.lab'))
									{{ __('translations.clinic') }}
								@endif
							</div>	
							<div class="account-list-cell">{{ __('translations.payment_type') }}</div>
							<div class="account-list-cell">{{ __('translations.payment_status') }}</div>
							<div class="account-list-cell"></div>						
					</div>
					<div class="account-list-content">
						@foreach($data as $item)
							<div class="fx fac account-list-line">
								<div class="account-list-cell">
									<div class="form-line">
										@if ($item->payment_status != 4 && !in_array($item->order_id, $changed_prices)) 
										<div class="fx fac form-checkbox">
											<input type="checkbox" id="invoice_{{ $item->id }}" class="invoices-check" name="invoices[{{ $item->id }}]" value="{{ $item->id }}">
											<label for="invoice_{{ $item->id }}"></label>
										</div>
										@endif
									</div>
								</div>								
								<div class="account-list-cell">
									<strong>{{ $item->name }}</strong> 
									@if (in_array($item->order_id, $changed_prices))
										@if (Auth::user()->type == config('types.clinic'))
											{!! '<span style="color: red;display:block;">'.__('translations.price_wait_confirm').'</span>' !!}
										@endif
										@if (Auth::user()->type == config('types.lab'))
											{!! '<span style="color: red;display:block;">'.__('translations.price_wait_changed').'</span>' !!}
										@endif
									@endif
								</div>	
								<div class="account-list-cell"><a target="_blank" href="{{ route('orders.edit', $item->order_id) }}">#{{ $item->order_id }} @if($item->order_info && $item->order_info->name != '')<br>({{ $item->order_info->name }})@endif</a></div>
								<div class="account-list-cell">
									@if (Auth::user()->type == config('types.clinic'))
										{{ $item->branch->name }}
									@endif
									@if (Auth::user()->type == config('types.lab'))
										{{ $item->to_branch->name }}
									@endif
								</div>
								<div class="account-list-cell">
									@if (Auth::user()->type == config('types.clinic'))
										{{ $item->to_branch->name }}
									@endif
									@if (Auth::user()->type == config('types.lab'))
										{{ $item->branch->name }}
									@endif
								</div>
								<div class="account-list-cell">
									@if ($item->payment_type != 0 && $item->total && $item->total->price != '0.00')
										{{ $item->payment_type_info->name }}
										<br>
									@endif
									@if ($item->total && $item->total->price)										
										<strong>${{ $item->total->price }}</strong>
									@endif
								</div>
								<div class="account-list-cell">
									@if ($item->payment_status && $item->payment_status_info->icon && $item->total && $item->total->price != '0.00')	
										<img src="{{ url('/').'/storage/'.$item->payment_status_info->icon }}" style="max-width: 35px;height:auto;" title="{{ $item->payment_status_info
											->name }}" alt="{{ $item->payment_status_info->name }}"> 
									@elseif ($item->total && $item->total->price != '0.00')
										{{ $item->payment_status_info->name }}
									@endif
								</div>
								<div class="account-list-cell">
									<a href="{{ route('invoices.edit', [$item->id, $requests]) }}" class="btn btn-small">{{ __('translations.view') }}</a>	
									@if ($item->payment_status != 4 && !in_array($item->order_id, $changed_prices) && $can_pay && $item->order_info && in_array($item->order_info->status, [12,14,15,16,17,18]) && $item->total && $item->total->price != '0.00') 
										<div class="btn btn-danger" onclick="paySingleInvoice('{{ $item->id }}');">
											@if (Auth::user()->type == config('types.clinic'))
												{{ __('translations.pay') }}
											@endif
											@if (Auth::user()->type == config('types.lab'))
												{{ __('translations.paid') }}
											@endif
										</div>
									@endif						
								</div>
							</div>
						@endforeach
					</div>					
				</div>
				<div class="pagination-block">{{ $data->appends(\Request::except('page'))->links() }}</div>
			@else 
				<p>{{ __('translations.no_invoices') }}</p>
			@endif
		</div>
	</div>
@stop

@section('scripts')
	<script src="/js/select.js" defer></script>
	<script src="/js/moment.min.js" defer></script>
	<script src="/js/daterangepicker.js" defer></script>
	<script>
		document.addEventListener('DOMContentLoaded', function() {
			$('#payment_type, #branch, #payment_status, #invoice_branch').select2({
				minimumResultsForSearch: -1
			});

			$('#from_date, #to_date').on('focus', function(){
				$(this).daterangepicker({				
					singleDatePicker: true,
					showDropdowns: true,
					startDate: false,				
					locale: {
				        format: 'YYYY-MM-DD'
				    },
				});
			});

			$('#show_filter_block').on('click', function(){
				$(this).toggleClass('show-filter-close');
				$('#order_filters_block').slideToggle();
			});

			$('.invoices-check + label').on('click', function(){
				if ($(this).hasClass('show-check-pseudo')) {
					$(this).removeClass('show-check-pseudo');
				} else {
					$(this).addClass('show-check-pseudo');
				}
			});

		}, !1);
	</script>
	@include('Frontend.templates.invoice_payment_script')
@stop