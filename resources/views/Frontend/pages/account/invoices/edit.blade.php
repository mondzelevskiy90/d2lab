@extends('Frontend/account_master')

@section('css')
	<link rel="stylesheet" href="{{ mix('/css/account.css') }}">
@stop

@section('content')		
	<form class="content-editing-form invoice-editing-form" method="POST" action="{{ route('invoices.update', [$data->id, $requests]) }}" enctype="multipart/form-data" id="invoice_editing_form">
		<div id="panel_top_buttons" class="fx fac fxb">
			@include('Frontend.templates.breadcrumbs')
			<div class="panel-top-buttons button-submit order-buttons">
				<a class="btn btn-small btn-grey" href="{{ route('invoices', [$requests]) }}">&larr; {{ __('translations.back') }}</a>
				<div class="btn btn-small" onclick="printContent('invoice_print_content');">{{ __('translations.print_invoice') }}</div>
				<a class="btn btn-small  btn-primary" href="{{ route('orders.edit', $data->order_id) }}">{{ __('translations.show_order') }}</a>
				@if ($can_pay && $data->payment_status != 4 && $data->total && $data->total->price_changed_to == '0.00' && $data->order_info && in_array($data->order_info->status, [15,16,17,18])) 
					<input type="checkbox" id="invoice_{{ $data->id }}" class="invoices-check" value="{{ $data->id }}" style="display: none;" checked="checked">
					<div class="btn btn-small btn-danger" onclick="paySingleFromInvoicePage();">
						@if (Auth::user()->type == config('types.clinic'))
							{{ __('translations.pay') }}
						@endif
						@if (Auth::user()->type == config('types.lab'))
							{{ __('translations.paid') }}
						@endif
					</div>
				@endif
				@if ($data->payment_status != 4 && Auth::user()->type == config('types.lab'))			
					<input type="submit" id="submit2" class="btn btn-small btn-danger" value="{{ __('translations.save') }}">
				@endif
			</div>
		</div>
		<h1 class="tc">{{ $seo->name }} {{ $data->name }} ({{ $data->payment_status_info->name }})</h1>
		{{ csrf_field() }}
		<div class="fx fxb panel-body">
			<div class="fx fac fxb panel">
				<div id="invoice_print_content" class="invoice-print-content">
					<div style="width: 1024px;">
						<div style="width: 1022px;position: relative;">
							<div class="fx fxb invoice-to_branch-info">
								<div class="invoice-to_branch-left">
									<p>{{ $data->to_branch->name}}</p>
									<br>
									<p>{{ $data->to_branch->zip_code .', '. $data->to_branch->address .', '. App\Services\Userdata::getLocality($data->to_branch->locality) }}</p>
									<p>Tel: {{ $data->to_branch->phone}}</p>
								</div>
								<div class="invoice-to_branch-right">
									<div class="invoice-invoice-head">{{ __('translations.invoice') }}</div>
									<br>
									<table class="invoice-table invoice-small-table invoice-table-date">
										<thead>
											<tr>
												<td>{{ __('translations.date') }}</td>
												<td>{{ __('translations.invoice') }} #</td>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td>{{ date('m-d-Y', strtotime($data->created_at)) }}</td>
												<td>
													{{ $data->order_id }}
													<br>
													{{ $data->name }}
												</td>
											</tr>
										</tbody>
									</table>						
								</div>
							</div>
							<div class="fx fxb invoice-branch-info">
								<table class="invoice-table invoice-short-table invoice-table-bill">
									<thead>
										<tr>
											<td>{{ __('translations.bill_to') }}</td>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>
												<p>{{ $data->branch->name}}</p>
												<p>{{ $data->branch->zip_code .', '. $data->branch->address .', '. App\Services\Userdata::getLocality($data->branch->locality) }}</p>
											</td>
										</tr>
									</tbody>
								</table>
								<table class="invoice-table invoice-short-table invoice-table-ship">
									<thead>
										<tr>
											<td>{{ __('translations.ship_to') }}</td>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>
												<p>{{ $data->branch->name}}</p>
												<p>{{ $data->order_info ? $data->order_info->ship_address : '' }}</p>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
							<div class="invoice-infos">
								<table class="invoice-table invoice-full-table  invoice-table-infos">
									<thead>
										<tr>
											<td>{{ __('translations.po_number') }}</td>
											<td>{{ __('translations.terms') }}</td>
											<td>{{ __('translations.rep') }}</td>
											<td>{{ __('translations.ship') }}</td>
											<td>{{ __('translations.via') }}</td>
											<td>{{ __('translations.fob') }}</td>
											<td>{{ __('translations.project') }}</td>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td></td>
											<td></td>
											<td></td>
											<td>{{ $data->order_info ? $data->order_info->ship_date : '' }}</td>
											<td></td>
											<td></td>
											<td></td>
										</tr>
									</tbody>
								</table>
							</div>
							@if ($prescriptions && count($prescriptions))
								<div class="invoice-invoice">
									<table class="invoice-table invoice-full-table invoice-table-invoice">
										<thead>
											<tr>
												<td>{{ __('translations.quantity') }}</td>
												<td>{{ __('translations.item_code') }}</td>
												<td>{{ __('translations.description') }}</td>
												<td>{{ __('translations.price_each') }}</td>
												<td>{{ __('translations.amount') }}</td>
											</tr>
										</thead>
										<tbody>
											@foreach ($prescriptions as $prescription)
												@php
													$quantity = 1;

													if ($prescription->price_for == 1 && $prescription->price_order_date != '0.00' && $prescription->advance_price != '0.00') {
															$quantity = round((float)$prescription->advance_price / (float)$prescription->price_order_date);
													}

													$price_changed_to = $prescription->price_changed_to;
													$price = $prescription->price;

													$price_each = '0.00'; 

													if ($price_changed_to != '0.00') {
														$price_each = number_format($price_changed_to / $quantity, 2);
													} else {
														$price_each = number_format($price / $quantity, 2);
													}
												@endphp
												<tr>
													<td>
														<div id="invoice_quantity_{{ $prescription->id }}">{{ $quantity }}</div>
													</td>
													<td>{{ $prescription->service->name }}</td>
													<td>
														<a target="_blank" href="{{ route('orders.edit', $data->order_id) }}#prescription_name_{{ $prescription->id }}">Order #{{ $data->order_id }}@if($data->order_info && $data->order_info->name != '') ({{ $data->order_info->name }})@endif</a>, {{ $prescription->service->name }} ({{ $prescription->numbering_system->name }}:{{ \App\Services\Content::getSelectedToothsText($prescription->selecteds) }})
														<br>
														{{ $data->order_info->patient_name }} {{ $data->order_info->patient_lastname }}
													</td>
													<td>
														<div class="invoice-each-price">
															{{ $currency->symbol }}<span id="price_each_{{ $prescription->id}}">{{ $price_each }}</span>
														</div>
													</td>
													<td>
														@if ($data->payment_status == 4)
															<div class="invoice-amount-price">
																{{ $currency->symbol }}<span id="price_amount_{{ $prescription->id }}">{{ $price }}</span>
															</div>
														@elseif (Auth::user()->type == config('types.lab'))
															@if ($price_changed_to != '0.00') 
																<div class="invoice-prev-price">
																	{{ $currency->symbol }}<span>{{ $price }}</span>
																</div>
															@endif
															@if ($can_pay)
																<input type="hidden" name="prescriptions[{{ $prescription->id }}][price]" value="{{ $price }}">
																<div class="fx fac prescription-price-setInput">
																	{{ $currency->symbol }}<input type="text" name="prescriptions[{{ $prescription->id }}][price_changed_to]" class="form-input prescription-price-input form-input-required" data-id="{{ $prescription->id }}" id="prescription_price_{{ $prescription->id }}" value="@if ($price_changed_to != '0.00'){{ $price_changed_to }}@else{{ $price }}@endif" required="required" onchange="isNaN(this.value) ? this.value = 0 : this.value = this.value;calculateOrderTotal();">
																</div>
															@else 
																<div class="invoice-amount-price">
																	@if ($price_changed_to != '0.00')
																		{{ $price_changed_to }}
																	@else
																		{{ $price }}
																	@endif
																</div>
															@endif
															@if ($price_changed_to != '0.00') 
																<div class="invoice-not-confirmed">{{ __('translations.price_wait_confirm') }}</div>
															@endif
															<div class="fx fac invoice-confirm-btns not-print">
																<br>
																<br>
																<!--<div class="btn invoice-confirm-btn" onclick="confirmPrecriptionAction({{ $prescription->id }}, 1)">{{ __('translations.remake') }}</div>-->
																<div class="btn btn-danger invoice-confirm-btn" onclick="confirmPrecriptionAction({{ $prescription->id }}, 2)">Remove from invoice</div>
																<input type="hidden" name="prescriptions[{{ $prescription->id }}][prescription_action]" id="prescription_action_{{ $prescription->id }}" value="0">
															</div>
														@else
															<div class="@if ($price_changed_to != '0.00'){{ '
															invoice-prev-price' }}@else{{ 'invoice-amount-price' }}@endif">
																{{ $currency->symbol }}<span id="price_amount_{{ $prescription->id }}">{{ $price }}</span>
															</div>

															@if ($price_changed_to != '0.00') 
																<div class="invoice-changed-price invoice-amount-price">
																	{{ $currency->symbol }}<span id="price_changed_{{ $prescription->id }}">{{ $price_changed_to }}</span>
																	<div class="invoice-not-confirmed">{{ __('translations.price_wait_confirm') }}</div>
																	@if ($can_pay)
																		<div class="fx fac invoice-confirm-btns not-print">
																			<div class="btn invoice-confirm-btn" onclick="confirmPriceChanging({{ $prescription->id }}, 1)">{{ __('translations.yes') }}</div>
																			<div class="btn btn-danger invoice-confirm-btn" onclick="confirmPriceChanging({{ $prescription->id }}, 2)">{{ __('translations.no') }}</div>
																		</div>
																	@endif
																</div>
															@endif
														@endif
													</td>
												</tr>
											@endforeach
										</tbody>
										<tfoot>
											<tr>
												<td colspan="3"></td>
												<td colspan="2">
													<div class="fx fac fxb invoice-total-line" style="position: relative;">
														@if ($data->payment_status == 4)
															<div class="fx fxc paid-background"><span>{{ __('translations.paid') }}</span></div>
														@endif
														<div>
															<strong>{{ __('translations.total') }}</strong>
														</div>
														<div>													
															<div class="invoice-total-price" >
																@if ($data->total->price_changed_to != '0.00') 
																	<div class="invoice-prev-price">
																		{{ $currency->symbol }}<span>{{ $data->total->price }}</span>
																	</div>
																@endif
																{{ $currency->symbol }}<span id="invoice_total_price">@if ($data->total->price_changed_to != '0.00'){{ $data->total->price_changed_to }}@else{{ $data->total->price }}@endif</span>
																@if (Auth::user()->type == config('types.lab'))
																	<input type="hidden" name="order_total" id="invoice_total_input" value="@if ($data->total->price_changed_to != '0.00'){{ $data->total->price_changed_to }}@else{{ $data->total->price }}@endif">
																
																@endif

																	@if ($data->total->price_changed_to != '0.00') 
																		<div class="invoice-not-confirmed">{{ __('translations.price_wait_confirm') }}</div>
																	@endif
															</div>
														</div>
													</div>
												</td>
											</tr>
										</tfoot>
									</table>
								</div>
							@endif
						</div>
					</div>
				</div>
			</div>
		</div>
	</form>
@stop

@section('scripts')
	<script>
		document.addEventListener('DOMContentLoaded', function(){
			
		}, !1);

		function printContent(id) {
			var new_window =window.open();
			var html = '<link rel="stylesheet" href="{{ mix('/css/styles.css') }}">';

			html += '<link rel="stylesheet" href="{{ mix('/css/account.css') }}">';
			html += '<link rel="stylesheet" href="{{ mix('/css/print_invoice.css') }}">';
			html += '<div class="fx fxb print-source"><span>{{ config('app.url') }}</span><span>{{ date('m-d-Y') }}</span></div>';
			html += document.getElementById(id).innerHTML;

			new_window.document.write(html);

			setTimeout(function(){
				new_window.print();
				new_window.close();
			}, 1000);
		}
	
		@if (Auth::user()->type == config('types.lab') && $data->payment_status != 4)
			function calculateOrderTotal() {
				var prices = $(document).find('.prescription-price-input');
				var total = 0;

				prices.each(function(i){
					var id = this.getAttribute('data-id');
					var quantity = parseInt($('#invoice_quantity_'+id).html());
					var price = parseFloat(prices[i].value);

					$('#price_each_'+id).html((price / quantity).toFixed(2));

					total = total + price;
				});

				$('#invoice_total_price').html(total.toFixed(2));
				$('#invoice_total_input').val(total.toFixed(2));
			}

			function confirmPrecriptionAction(id, action) {
				var html = '';

				html += '<div class="confirm-prescription-action-text">';

				if (action == 1) html += '{{ __('translations.remake_prescription_text') }}';
				if (action == 2) html += '{{ __('translations.delete_prescription_text') }}';

				html += '</div>';
				html += '<br>';
				html += '<br>';
				html += '<div class="confirm-prescription-action-btns">';
				html += '<div class="btn btn-small" onclick="setPrescriptionAction('+id+', '+action+')">{{ __('translations.yes') }}</div>&nbsp';
				html += '<div class="btn btn-small btn-danger" onclick="document.getElementById(\'cboxClose\').click()">{{ __('translations.no') }}</div>';
				html += '</div>';


				showPopup('<div class="warning">'+ html +'</div>', true);
			}

			function setPrescriptionAction(id, action) {
				showLoader();
				$(document).find('#cboxClose').click();
				$(document).find('#prescription_action_'+id).val(action);

				setTimeout(function(){
					$('#invoice_editing_form').submit();
				}, 1000)
				
			}
		@endif
		@if (Auth::user()->type == config('types.clinic') && $data->payment_status != 4)
			function confirmPriceChanging(id, status) {
				showLoader();

				$.ajax({
					url: '/ajax/getData',
					type: 'post',
		            dataType: 'json',
		            headers: {
		                'X-CSRF-TOKEN': '{{ csrf_token() }}'
		            },
				    data: {action:'confirmPriceChanging', id: id, status: status},
				    success: function(data) {
				    	if (data) {
				    		window.location.reload();
				    	}
				    }						    
				});
			}
		@endif
	</script>	
	@include('Frontend.templates.invoice_payment_script')
@stop