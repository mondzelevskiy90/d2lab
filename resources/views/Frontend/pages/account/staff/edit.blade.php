@extends('Frontend/account_master')

@section('css')
	<link rel="stylesheet" href="/css/select.css">
	<link rel="stylesheet" href="{{ mix('/css/account.css') }}">
@stop

@section('content')	
	<form class="content-editing-form " method="POST" action="{{ isset($data->id) ? route('staff.edit', $data->id) : route('staff.create') }}" enctype="multipart/form-data">
		<div id="panel_top_buttons" class="fx fac fxb">
			@include('Frontend.templates.breadcrumbs')
			<div class="panel-top-buttons button-submit order-buttons">
				<a class="btn btn-small btn-grey" href="{{ route('staff', [$requests]) }}">&larr; {{ __('translations.back') }}</a>
				<input type="submit" id="submit" class="btn btn-small" name="submit" value="@if(isset($data->id)){{ __('translations.save_changes') }}@else{{ __('translations.save') }}@endif">
			</div>
		</div>
		<h1 class="tc">{{ $seo->name }}</h1>	
		<div class="fx fxb panel-body staff-edit-form">		
			{{ csrf_field() }}			
			<div class="panel">
				@if (isset($data->id))					
					<div class="fx fac form-line">
						<div class="bi company-logo" style="margin: 0 15px 0 0; background-image: url(/storage/{{ $data->user->avatar}})"></div>
						<strong>{!! $data->user->name.' '.$data->user->surname.'<br>('.$data->user->email.')' !!}</strong>
					</div>
					<br>
				@else
					<div class="form-line">
						<label class="fx" for="name">{{ __('translations.name') }}</label>				
						<input type="text" @if(!isset($data->id)) name="name" required="required" @else disabled="disabled" @endif id="name" class="form-input" placeholder="{{ __('translations.name') }}" value="{{ old('name', $data->user->name ?? '') }}" required="required">
						@if ($errors->has('name'))
		                    <div class="error">{{ $errors->first('name') }}</div>
		                @endif				
					</div>	
					<div class="form-line">
						<label for="surname">{{ __('translations.surname') }}</label>				
						<input type="text" @if(!isset($data->id)) name="surname" required="required" @else disabled="disabled" @endif id="surname" class="form-input" placeholder="{{ __('translations.surname') }}" value="{{ old('surname', $data->user->surname ?? '') }}" required="required">
						@if ($errors->has('surname'))
		                    <div class="error">{{ $errors->first('surname') }}</div>
		                @endif				
					</div>
					<div class="form-line">
						<label for="email">{{ __('translations.email') }}</label>				
						<input type="email" @if(!isset($data->id)) name="email" required="required" @else disabled="disabled" @endif id="email" class="form-input" placeholder="{{ __('translations.email') }}" value="{{ old('email', $data->user->email ?? '') }}">
						@if ($errors->has('email'))
		                    <div class="error">{{ $errors->first('email') }}</div>
		                @endif				
					</div>	
				@endif
				@if(isset($data->id) && $data->user->status != 1)	
					<div class="form-line">
						<label for="status">{{ __('translations.status') }}</label>				
						<select id="status" name="status" class="form-controll" required="required">
							<option value="3" @if($data->user->status == 3) selected @endif>{{ __('translations.status_active') }}</option>
							<option value="4" @if($data->user->status == 4) selected @endif>{{ __('translations.status_disactive') }}</option>						
						</select>				
					</div>
			    @endif	
				<div class="form-line">
					<label for="branch_id">{{ __('translations.branch') }}</label>
					<select name="branch_id" id="branch_id" class="form-controll" required="required">
						@foreach ($branches as $branch)
							<option value="{{ $branch->id }}">{{ $branch->name }}</option>
						@endforeach
					</select>
				</div>
				<div class="form-line">
					<label for="role_id">{{ __('translations.role') }}</label>
					<select name="role_id" id="role_id" class="form-controll" required="required">
						@foreach ($roles as $role)
							<option value="{{ $role['id'] }}" @if(isset($data->id) && $data->user->role_id == $role['id']) selected="selected" @endif>{{ $role['display_name'] }}</option>
						@endforeach
					</select>
				</div>
				<div class="form-line">
					<label for="can_pay">{{ __('translations.can_pay') }}</label>
					<select name="can_pay" id="can_pay" class="form-controll" required="required">
						<option value="0" @if(isset($data->id) && $data->can_pay == 0) selected="selected" @endif>{{ __('translations.no') }}</option>
						<option value="1" @if(isset($data->id) && $data->can_pay == 1) selected="selected" @endif>{{ __('translations.yes') }}</option>						
					</select>
				</div>		
			</div>
		</div>
	</form>	
@stop

@section('scripts')
	<script src="/js/select.js" defer></script>	
	<script>
		document.addEventListener('DOMContentLoaded', function(){
			$('#status, #role_id, #branch_id, #can_pay').select2({
				minimumResultsForSearch: -1
			});

		}, !1);
	</script>
	@include('Frontend.templates.fields_script')
@stop