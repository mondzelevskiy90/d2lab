@extends('Frontend/account_master')

@section('css')
	<link rel="stylesheet" href="{{ mix('/css/account.css') }}">
@stop

@section('breadcrumbs')
	@include('Frontend.templates.breadcrumbs')
@stop

@section('content')
	<h1>{{ $seo->name }}</h1>
	@if ($branch)
		<div class="panel panel-transparent panel-mob-short">
			<div class="panel-buttons">
				<a href="{{ route('staff.create') }}" class="btn btn-primary">{{ __('translations.create') }}</a>
			</div>
		</div>
	@endif
	<div class="fx fxb panel-body staff-form">
		<div class="panel panel-table">
			@if ($data && count($data)) 
				<div class="account-list-table account-list-table6">
					<div class="fx account-list-head">						
							<div class="account-list-cell">{{ __('translations.name') }}</div>
							<div class="account-list-cell">{{ __('translations.avatar') }}</div>
							<div class="account-list-cell">{{ __('translations.role') }}</div>
							<div class="account-list-cell">{{ __('translations.branch') }}</div>
							<div class="account-list-cell">{{ __('translations.status') }}</div>
							<div class="account-list-cell"></div>						
					</div>
					<div class="account-list-content">
						@foreach($data as $item)
							@if($item->user)
							<div class="fx fac account-list-line">
								<div class="account-list-cell">{{ $item->user->name.' '.$item->user->surname }}</div>
								<div class="account-list-cell">
									<div class="bi company-logo" style="margin: auto; background-image: url(/storage/{{ $item->user->avatar}})"></div>
								</div>
								<div class="account-list-cell">{{ $item->role->display_name }}</div>
								<div class="account-list-cell">{{ $item->branch->name }}</div>
								@php
									$status = 'E-mail confirmation';

									if ($item->user->status == 2) $status = 'New';
									if ($item->user->status == 3) $status = __('translations.status_active');
									if ($item->user->status == 4) $status = __('translations.status_disactive');
								@endphp	
								<div class="account-list-cell">{{ $status }}</div>
								<div class="account-list-cell">
									<a href="{{ route('staff.edit', $item->id) }}" class="btn btn-small">{{ __('translations.edit') }}</a>
									<div data-href="{{ route('staff.delete', $item->user_id) }}" class="btn btn-danger btn-small delete-element" data-msg="{{ __('translations.delete_alert') }}">{{ __('translations.delete') }}</div>
								</div>
							</div>
							@endif
						@endforeach
					</div>
				</div>				
			@else 
				@if ($branch) 
					<p>{!! sprintf(__('translations.no_staff'), route('staff.create')) !!}</p>
				@else
					<p>{!! sprintf(__('translations.no_braches'), route('branches.create')) !!}</p>
				@endif
			@endif
		</div>
	</div>
@stop

@section('scripts')

@stop