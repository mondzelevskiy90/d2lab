@extends('Frontend/account_master')

@section('css')
	<link rel="stylesheet" href="{{ mix('/css/account.css') }}">
	<style>
		.form-line label{
			width: 100%;
			max-width: 100%;
		}
		.form-buttons{
			text-align: right;
		}
		.ticket{
			width: 100%;
			margin-bottom: 20px;
    		margin-top: 10px;
		}
		.ticket-head{
			position: relative;
		    width: 100%;
		    padding: 10px 45px 10px 20px;
		    border-radius: 5px;
		    background-color: transparent;
		    font-size: 18px;
		    cursor: pointer;		    
    		opacity: 0.8;
		}
		.ticket-head:hover{
			opacity: 1;
		}
		.ticket-head:after{
			content: "";
		    position: absolute;
		    top: 17px;
		    right: 17px;
		    width: 0;
		    height: 0;
		    border-style: solid;
		    border-width: 0 7px 10px 7px;
    		border-color: transparent transparent #fff transparent;
    		color: #000;
		} 
		.ticket-head-act:after{
			border-width: 10px 7px 0 7px;
    		border-color: #fff transparent transparent transparent;
		}
		.ticket-ok .ticket-head{
			color: #fff;
			background-color: green
		} 
		.ticket-wait .ticket-head{
			color: #fff;
			background-color: #f73f3f
		}
		.ticket-content{
			display: none;
			padding: 20px;
    		border-bottom: 1px solid #e9e9e9;
		} 
		.ticket-content-act{
			display: block;
		}
		
	</style>	
@stop

@section('content')	
	<form class="tickets-editing-form" method="POST" action="{{ route('tickets') }}">
		<div id="panel_top_buttons" class="fx fac fxb">
			@include('Frontend.templates.breadcrumbs')
			<div class="panel-top-buttons button-submit order-buttons">
				<a class="btn btn-small btn-grey" href="{{ route('account') }}">&larr; {{ __('translations.back') }}</a>
			</div>
		</div>
		<h1 class="tc">{{ $seo->name }}</h1>
		{{ csrf_field() }}		
		<div class="fx fxb panel-body">
			<div class="panel">
				<div class="form-line">
					<label for="question">{{ __('translations.problem_description_label') }}:</label>
					<textarea name="question" id="question" class="form-input form-input-required" placeholder="{{ __('translations.problem_description') }}" maxlength="1000" required="required"></textarea>
				</div>
				<div class="form-line form-buttons">
					<input type="submit" id="submit" class="btn btn-small" name="submit" value="{{ __('translations.create_ticket') }}">
				</div>
			</div>				
		</div>
	</form>
	@if ($data && count($data))
		<div class="fx fxb panel-body">
			<div class="panel">			
				@foreach($data as $key => $item)
					<div class="ticket @if ($item->answer){{ 'ticket-ok' }}@else{{ 'ticket-wait' }}@endif">
						<div class="ticket-head @if ($key == 0){{ 'ticket-head-act'}}@endif">
							{{ __('translations.ticket') }} #{{ $item->id }} ({{ date('m-d-Y', strtotime($item->created_at)) }})
						</div>
						<div class="ticket-content @if ($key == 0){{ 'ticket-content-act'}}@endif">
							<p><strong>{{ __('translations.you_question') }}:</strong></p>
							<p>{{ $item->question }}</p>
							<p><strong>{{ __('translations.adm_answer') }}:</strong></p>
							<p>
								@if ($item->answer)
									{{ $item->answer }}
								@else
									<span style="color:red">{{ __('translations.no_adm_answer') }}</span>
								@endif
							</p>
						</div>
					</div>
				@endforeach 			
			</div>
		</div>
	@endif
@stop

@section('scripts')	
	<script>
		document.addEventListener('DOMContentLoaded', function(){
			$('.ticket-head').on('click', function(){
				$('.ticket-head').removeClass('ticket-head-act');
				$('.ticket-content').removeClass('ticket-content-act');

				$(this).addClass('ticket-head-act').next('.ticket-content').addClass('ticket-content-act');
			});
		}, !1);
	</script>
@stop