@extends('Frontend/account_master')

@section('css')
	<link rel="stylesheet" href="/css/select.css">	
	<link rel="stylesheet" href="{{ mix('/css/account.css') }}">
@stop

@section('content')	
	<form class="editing-form content-editing-form "  method="POST" action="{{ route('settings') }}" enctype="multipart/form-data">
		<div id="panel_top_buttons" class="fx fac fxb">
			@include('Frontend.templates.breadcrumbs')
			<div class="panel-top-buttons button-submit order-buttons">
				<a class="btn btn-small btn-grey" href="{{ route('account') }}">&larr; {{ __('translations.back') }}</a>
				<input type="submit" id="submit" class="btn btn-small" name="submit" value="{{ __('translations.save') }}">
			</div>
		</div>
		<h1 class="tc">{{ $seo->name }}</h1>
		{{ csrf_field() }}		
		<div class="fx fxb panel-body settings-form">
			<div class="panel @if (Auth::user()->role_id != config('roles.doctor') && Auth::user()->role_id != config('roles.tech')) panel-half @endif">
				<div class="tc tb panel-head">{{ __('translations.setting_person') }}</div>
				<div class="form-line">	
					<label for="email">{{ __('translations.email') }}</label>			
					<input type="email" id="email" class="form-input" value="{{ $data->email }}" disabled="disabled">
				</div>
				<div class="form-line">	
					<label for="type">{{ __('translations.account_type') }}</label>					
					<input type="text" class="form-input" value="{{ $data->type_info->name }}" disabled="disabled">
					<input type="hidden" name="type" id="type" value="{{ old('type') }}">					
				</div>
				<div class="form-line">	
					<label for="name">{{ __('translations.name') }}</label>			
					<input type="text" name="name" id="name" class="form-input" placeholder="{{ __('translations.name') }}" value="{{ $data->name ?? '' }}" required="required">
					@if ($errors->has('name'))
		                <div class="error">{{ $errors->first('name') }}</div>
		            @endif				
				</div>
				<div class="form-line">		
				    <label for="surname">{{ __('translations.surname') }}</label>		
					<input type="text" name="surname" id="surname" class="form-input" placeholder="{{ __('translations.surname') }}" value="{{ $data->surname ?? '' }}" required="required">
					@if ($errors->has('surname'))
		                <div class="error">{{ $errors->first('surname') }}</div>
		            @endif				
				</div>
				<div class="form-line">		
					<label for="phone">{{ __('translations.phone') }}</label>		
					<input type="text" name="phone" id="phone" class="form-input" placeholder="{{ __('translations.phone') }}" value="{{ $data->phone ?? '' }}" @if (Auth::user()->type != config('types.guest'))required="required"@endif>
					@if ($errors->has('phone'))
		                <div class="error">{{ $errors->first('phone') }}</div>
		            @endif				
				</div>	
				<div class="form-line">	
				    <label for="password">{{ __('translations.settings_password') }}</label>			
					<input type="password" name="password" id="password" class="form-input" placeholder="{{ __('translations.settings_password') }}" value="">
					@if ($errors->has('password'))
		                <div class="error">{{ $errors->first('password') }}</div>
		            @endif				
				</div>
				<div class="form-line setting-logo-line">				
						<div class="fx fxb fac">
							@if ($data->logo) 
							<div class="fx fac company-logo-title">
								{{ __('translations.avatar') }}: <div class="bi company-logo" style="background-image: url({{ App\Services\Img::getIcon($data->avatar) }})"></div>
							</div>

							@endif
							<div id="file_psev_input2" onclick="$('#avatar').click();">
								<span>{{ __('translations.select_new_avatar') }}</span>
							</div>
						</div>
						
						<input type="hidden" name="data_avatar[status]" id="file_psev_input2_status" value="0">
						<input type="hidden" name="data_avatar[width]" id="file_psev_input2_width" value="">
						<input type="hidden" name="data_avatar[height]" id="file_psev_input2_height" value="">
						<input type="hidden" name="data_avatar[x]" id="file_psev_input2_x" value="">
						<input type="hidden" name="data_avatar[y]" id="file_psev_input2_y" value="">
										
						<input type="file" class="uploaded-image" data-prefix="data_avatar" data-input="file_psev_input2" data-text="{{ __('translations.select_new_avatar') }}" name="avatar" id="avatar" value="" accept="image/jpeg,image/jpg,image/png" style="height: 1px; opacity: 0;padding: 0;">
						@if ($errors->has('avatar'))
			                <div class="error">{{ $errors->first('avatar') }}</div>
			            @endif				
					</div>
			</div>
			@if (Auth::user()->role_id != config('roles.doctor') && Auth::user()->role_id != config('roles.tech'))
				<div class="panel panel-half" id="company_settings">
					@if (Auth::user()->type == config('types.guest') && Auth::user()->role_id == config('roles.guest'))
						<div class="fx fxc panel-mask" id="company_upgrade" @if($errors->has('company') || $errors->has('locality') || $errors->has('address') || $errors->has('logo')) style="display: none;" @endif>
							<div class="btn show-company-panel" data-type="{{ config('types.lab') }}" data-text="{{ __('translations.change_account_type3') }}">{{ __('translations.upgrade_lab') }}</div>
							<span>or</span>
							<div class="btn btn-primary show-company-panel" data-type="{{ config('types.clinic') }}" data-text="{{ __('translations.change_account_type2') }}">{{ __('translations.upgrade_clinic') }}</div>
						</div>
					@endif
					@if (Auth::user()->type == config('types.guest') && Auth::user()->role_id == config('roles.guest'))
						<div id="cancel_company_line" class="form-line cancel-company-line" @if($errors->has('company') || $errors->has('locality') || $errors->has('address') || $errors->has('logo')) style="display: block;" @else style="display: none;" @endif style="display: none;">
							<p id="upgrade_text">@if(old('type')){{ __('translations.change_account_type'.old('type')) }}@endif</p>
							<div class="btn btn-danger" onclick="hideCompanyPanel();">{{ __('translations.cancel_upgrade') }}</div>
						</div>
					@endif	
					@if (Auth::user()->role_id == config('roles.lab_head') || Auth::user()->role_id == config('roles.clinic_head'))	
					<div class="tc tbp panel-head">{{ __('translations.setting_company') }}</div>		
					<div class="form-line">	
						<label for="company">{{ __('translations.company') }}</label>			
						<input type="text" name="company" id="company" class="form-input" placeholder="{{ __('translations.company') }}" value="{{ $data->company ?? '' }}" @if (Auth::user()->type != config('types.guest'))required="required"@endif>						
						@if ($errors->has('company'))
			                <div class="error">{{ $errors->first('company') }}</div>
			            @endif				
					</div>
					@endif
					<div class="form-line">	
					    <label for="zip_code">{{ __('translations.zip_code') }}</label>			
						<input type="text" name="zip_code" id="zip_code" class="form-input" placeholder="{{ __('translations.zip_code') }}" value="{{ $data->zip_code ?? '' }}" @if (Auth::user()->type != config('types.guest'))required="required"@endif>
						@if ($errors->has('zip_code'))
			                <div class="error">{{ $errors->first('zip_code') }}</div>
			            @endif				
					</div>
					<div class="form-line">
						<label for="country">{{ __('translations.country') }}</label>
						<select id="country" name="country" class="form-controll" required="required">
							@if($locality)
								<option value="{{ $locality->country->id }}">{{ $locality->country->name }}</option>
							@else 
								<option value="0">{{ __('translations.country') }}</option>
							@endif							
						</select>
						@if ($errors->has('country'))
		                    <div class="error">{{ $errors->first('country') }}</div>
		                @endif
					</div>
					<div class="form-line">
						<label for="region">{{ __('translations.region') }}</label>
						<select id="region" name="region" class="form-controll" @if (Auth::user()->type != config('types.guest'))required="required"@endif>
							@if($locality)
								<option value="{{ $locality->region->id }}">{{ $locality->region->name }}</option>
							@else 
								<option value="0">{{ __('translations.region') }}</option>
							@endif
						</select>
						@if ($errors->has('region'))
			                <div class="error">{{ $errors->first('region') }}</div>
			            @endif
					</div>
					<div class="form-line">
						<label for="locality">{{ __('translations.city') }}</label>
						<select id="locality" name="locality" class="form-controll" @if (Auth::user()->type != config('types.guest'))required="required"@endif>
							@if($locality)
								<option value="{{ $locality->id }}">{{ $locality->name }}</option>
							@else 
								<option value="0">{{ __('translations.locality') }}</option>
							@endif
						</select>
						@if ($errors->has('locality'))
			                <div class="error">{{ $errors->first('locality') }}</div>
			            @endif
					</div>
					<div class="form-line">
						<label for="address">{{ __('translations.address') }}</label>				
						<input type="text" name="address" id="address" class="form-input" placeholder="{{ __('translations.address') }}" value="{{ $data->address ?? '' }}" @if (Auth::user()->type != config('types.guest'))required="required"@endif>
						@if ($errors->has('address'))
			                <div class="error">{{ $errors->first('address') }}</div>
			            @endif				
					</div>
					@if (Auth::user()->role_id == config('roles.lab_head') || Auth::user()->role_id == config('roles.clinic_head'))	
					<div class="form-line setting-logo-line">				
						<div class="fx fxb fac">
							@if ($data->logo) 
							<div class="fx fac company-logo-title">
								{{ __('translations.logo') }}: <div class="bi company-logo" style="background-image: url({{ App\Services\Img::getIcon($data->logo) }})"></div>
							</div>
							@endif
							<div id="file_psev_input" onclick="$('#logo').click();">
								<span>{{ __('translations.select_new_logo') }}</span>
							</div>
						</div>

						<input type="hidden" name="data_logo[status]" id="file_psev_input_status" value="0">
						<input type="hidden" name="data_logo[width]" id="file_psev_input_width" value="">
						<input type="hidden" name="data_logo[height]" id="file_psev_input_height" value="">
						<input type="hidden" name="data_logo[x]" id="file_psev_input_x" value="">
						<input type="hidden" name="data_logo[y]" id="file_psev_input_y" value="">
										
						<input type="file" class="uploaded-image" data-prefix="data_logo" data-input="file_psev_input" data-text="{{ __('translations.select_new_logo') }}" name="logo" id="logo" value="" accept="image/jpeg,image/jpg,image/png" style="height: 1px; opacity: 0;padding: 0;">
						@if ($errors->has('logo'))
			                <div class="error">{{ $errors->first('logo') }}</div>
			            @endif				
					</div>	
					@endif
				</div>
			@endif
		</div>
	</form>
@stop

@section('scripts')	
	@include('Frontend.templates.fields_script')
	@include('Frontend.templates.img_upload_script')
	@if (Auth::user()->type == config('types.guest') && Auth::user()->role_id == config('roles.guest'))
		<script>
			document.addEventListener('DOMContentLoaded', function(){
				$('.show-company-panel').on('click', function(){
					var type = $(this).data('type')
						text = $(this).data('text');
					
					$('#type').val(type);
					$('#upgrade_text').html(text);
					$('#company_upgrade').fadeOut();
					$('#cancel_company_line').slideToggle(0);
				});
			}, !1);

			function hideCompanyPanel() {
				$('#company_upgrade').fadeIn();
				$('#cancel_company_line').slideToggle(0);
				$('#type').val('');
				$('#upgrade_text').html('');
			}
		</script>
	@endif
@stop