@extends('Frontend/account_master')

@section('css')
	<link rel="stylesheet" href="/css/select.css">
	<link rel="stylesheet" href="{{ mix('/css/account.css') }}">
@stop

@section('breadcrumbs')
	@include('Frontend.templates.breadcrumbs')
@stop

@php
function create_services_list($items, $currency, $price_services, $padding = 0) {
	foreach ($items as $item) {
		$line = '<div class="fx fac account-list-line '.(!empty($item['childrens']) ? 'account-list-headline' : '').' ap'.$padding.'">';
		$line .= '<div class="account-list-cell">'.$item['name'].'</div>';

		if (empty($item['childrens'])) {
			$line .= '<div class="account-list-cell">';
			$line .= '<input type="text" name="services['.$item['id'].'][price]" id="service_price['.$item['id'].']" class="form-input" placeholder="'.__('translations.price').'" value="'.(isset($price_services[$item['id']]) ? $price_services[$item['id']]['price'] : '').'" onchange="isNaN(this.value) ? this.value = null : this.value = this.value;">';
			$line .= '</div>';
			$line .= '<div class="account-list-cell">';
			$line .= '<input type="hidden" name="services['.$item['id'].'][currency_id]" value="'.$currency->id.'"/>';
			$line .= ''.$currency->code.'';
			$line .= '</div>';
			$line .= '<div class="account-list-cell account-list-cellOptions">';

			if (count($item['options'])) {				
				foreach ($item['options'] as $option) {
					$line .= '<div class="form-checkbox">';
					$line .= '<input type="checkbox" name="services['.$item['id'].'][options]['.$option->id.']" id="service_'.$item['id'].'_option_'.$option->id.'" class="" '.(isset($price_services[$item['id']]['options'][$option->id]) ? 'checked="checked"' : '').'>';
					$line .= '<label for="service_'.$item['id'].'_option_'.$option->id.'">'.$option->option_info->name.'</label>';
					$line .= '</div>';
				}				 
			}

			$line .= '</div>';
		}
		
		$line .= '</div>';

		echo $line;

		if (!empty($item['childrens'])) create_services_list($item['childrens'], $currency, $price_services, $padding + 10);
	}
}
@endphp

@section('content')
	<h1>{{ $seo->name }}</h1>	
	<form method="POST" action="{{ isset($data->id) ? route('prices.edit', $data->id) : route('prices.create') }}" enctype="multipart/form-data">
		<div class="fx fxb panel-body prices-edit-form">		
			{{ csrf_field() }}			
			<div class="panel">	
			    @if(isset($data->id))
			    	<div class="form-line">
			    		<label for="branch">{{ __('translations.branch') }}</label>
						<input type="text" id="branch" class="form-input" value="{{ $branch->name }}" disabled="disabled">
			    	</div>
					<div class="form-line">
						<label for="name">{{ __('translations.name') }}</label>				
						<input type="text" name="name" id="name" class="form-input" placeholder="{{ __('translations.name') }}" value="{{ old('name', $data->name) }}" required="required">
						@if ($errors->has('name'))
		                    <div class="error">{{ $errors->first('name') }}</div>
		                @endif				
					</div>	
					@if ($services && count($services))
						<br><br>
						<div class="form-line">
							<label class="panel-head">{{ __('translations.services_list') }} ({{ __('translations.select_only_needed') }})</label>
							<div class="account-long-list">
								<div class="account-list-table account-list-table4 account-list-tableServices">
									<div class="fx account-list-head">						
											<div class="account-list-cell">{{ __('translations.service_name') }}</div>
											<div class="account-list-cell">{{ __('translations.price_value') }}</div>
											<div class="account-list-cell">{{ __('translations.currency') }}</div>
									</div>
									<div class="account-list-content">
										@php 
											create_services_list($services, $currency, $price_services);				
										@endphp
									</div>
								</div>
							</div>
						</div>
					@endif
			    @else 
					<div class="form-line">
						<label for="branch_id">{{ __('translations.branch') }}</label>
						<select name="branch_id" id="branch_id" class="form-controll" required="required">
							@foreach ($branches as $branch)
								<option value="{{ $branch->id }}">{{ $branch->name }}</option>
							@endforeach
						</select>
					</div>
			    @endif					
			</div>
			<div class="panel panel-transparent panel-buttons">
				<div class="form-line button-submit">				
					<input type="submit" id="submit" class="btn" name="submit" value="@if(isset($data->id)){{ __('translations.save_changes') }}@else{{ __('translations.save') }}@endif">
				</div>
			</div>
		</div>
	</form>	
@stop

@section('scripts')
	@include('Frontend.templates.fields_script')
@stop