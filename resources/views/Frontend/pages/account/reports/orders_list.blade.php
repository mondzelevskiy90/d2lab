@extends('Frontend/account_master')

@section('css')
	<link rel="stylesheet" href="/css/select.css">
	<link rel="stylesheet" href="/css/daterangepicker.css">
	<link rel="stylesheet" href="{{ mix('/css/account.css') }}">
	<style>
		.table-condensed thead tr:nth-child(2), .table-condensed tbody {
			display: none
		}
		.daterangepicker select.monthselect{
			min-width: 120px;
		}
	</style>
@stop

@section('content')
	<div id="panel_top_buttons" class="fx fac fxb">
		@include('Frontend.templates.breadcrumbs')
		<div class="panel-top-buttons button-submit order-buttons">
			<a class="btn btn-small btn-grey" href="{{ route('reports') }}">&larr; {{ __('translations.back') }}</a>
			<div class="btn btn-small" onclick="printContent('invoice_print_content');">{{ __('translations.print_all_prescriptions') }}</div>
		</div>
	</div>
	<div class="panel panel-transparent">
		<form method="GET" action="{{ route('report', $data->slug) }}" id="filters_block" class="fx fac">
			{{ csrf_field() }}	
			@if ($branches && count($branches) && (Auth::user()->role_id == config('roles.lab_head') || Auth::user()->role_id == config('roles.clinic_head')))
				<div class="form-filter">
					<select name="branch" id="branch" class="form-input">	
						<option value="0"> -- {{ __('translations.branch') }} --</option>
						@foreach($branches as $branch)
							@if($branch['status'] != 4)
								<option value="{{ $branch['id'] }}" @if(isset($filters['branch']) && $filters['branch'] == $branch['id']){{ 'selected="selected"' }}@endif>{{ $branch['name'] }}</option>
							@endif
						@endforeach
					</select>		
				</div>
			@endif	
			<div class="form-filter">
				<select name="order_branche" id="order_branche" class="form-input">	
					<option value="0">
						@if (Auth::user()->type == config('types.clinic'))
							-- {{ __('translations.select').' '.__('translations.lab') }} --
						@endif
						@if (Auth::user()->type == config('types.lab'))
							-- {{ __('translations.select').' '.__('translations.clinic') }} --
						@endif
					</option>
					@foreach($order_branches as $order_branche)
						@if (Auth::user()->type == config('types.clinic'))
							<option value="{{ $order_branche['to_branch_id'] }}" @if(isset($filters['order_branche']) && $filters['order_branche'] == $order_branche['to_branch_id']){{ 'selected="selected"' }}@endif>{{ $order_branche['to_branch']['name'] }}</option>
						@endif
						@if (Auth::user()->type == config('types.lab'))
							<option value="{{ $order_branche['branch_id'] }}" @if(isset($filters['order_branche']) && $filters['order_branche'] == $order_branche['branch_id']){{ 'selected="selected"' }}@endif>{{ $order_branche['branch']['name'] }}</option>
						@endif					
					@endforeach
				</select>	
			</div>
			<div class="form-filter">
				<input type="text" name="date" id="date" class="form-input" placeholder="{{ __('translations.from_date') }}" value="@if(isset($filters['date']) && $filters['date']){{ $filters['date'] }}@endif">			
			</div>
			<div class="filter-buttons" style="text-align: right;">					
				<input type="submit" class="btn btn-small" value="{{ __('translations.filter') }}">
				<a href="{{ route('report', $data->slug) }}" class="btn btn-danger btn-small">{{ __('translations.clear') }}</a>
			</div>
		</form>
	</div>
	<!--<h1 class="tc">{{ $seo->name }}</h1>-->
	<div id="invoice_print_content">
		@php $cnt = 0; @endphp
		@foreach ($content as $branch)
			@foreach($branch['order_branches'] as $order_branche)
				@if ($order_branche['report_data'] && $order_branche['report_data']['orders'] && count($order_branche['report_data']['orders']))
					<div class="fx fxb panel-body">
						<div class="fx fac fxb panel">
							<div id="invoice_print_content{{ $cnt }}" class="invoice-print-content">
								<div style="width: 1022px;position: relative; page-break-after: always;">
									<div class="fx invoice-to_branch-info" style="justify-content: flex-end;">
										<div class="invoice-to_branch-right">
											<div class="invoice-invoice-head">{{ $data->name }}</div>
											<br>
											<table class="invoice-table invoice-small-table invoice-table-date">
												<thead>
													<tr>
														<td>{{ __('translations.from') }}</td>
														<td>{{ __('translations.to') }}</td>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td>{{ date('m-d-Y', strtotime($filters['date'])) }}</td>
														<td>{{ date('m-t-Y', strtotime($filters['date'])) }}</td>
													</tr>
												</tbody>
											</table>						
										</div>
									</div>									
									<div class="fx fxb invoice-branch-info">
										<table class="invoice-table invoice-short-table invoice-table-bill">
											<thead>
												<tr>
													<td>{{ __('translations.clinic') }} {{ __('translations.branch') }}</td>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td>
														@if (Auth::user()->type == config('types.clinic'))
															<p><strong>{{ $branch['name'] }}</strong></p>
															<p>{{ $branch['zip_code'] .', '. $branch['address'] .', '. App\Services\Userdata::getLocality($branch['locality']) }}</p>
														@endif
														@if (Auth::user()->type == config('types.lab'))
															<p><strong>{{ $order_branche['branch']['name'] }}</strong></p>
															<p>{{ $order_branche['branch']['zip_code'] .', '. $order_branche['branch']['address'] .', '. App\Services\Userdata::getLocality($order_branche['branch']['locality']) }}</p>
														@endif
													</td>
												</tr>
											</tbody>
										</table>
										<table class="invoice-table invoice-short-table invoice-table-ship">
											<thead>
												<tr>
													<td>{{ __('translations.lab') }}</td>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td>
														@if (Auth::user()->type == config('types.clinic'))
															<p><strong>{{ $order_branche['branch']['name'] }}</strong></p>
															<p>{{ $order_branche['branch']['zip_code'] .', '. $order_branche['branch']['address'] .', '. App\Services\Userdata::getLocality($order_branche['branch']['locality']) }}</p>
														@endif
														@if (Auth::user()->type == config('types.lab'))
															<p><strong>{{ $branch['name'] }}</strong></p>
															<p>{{ $branch['zip_code'] .', '. $branch['address'] .', '. App\Services\Userdata::getLocality($branch['locality']) }}</p>
														@endif														
													</td>
												</tr>
											</tbody>
										</table>
									</div>
									<table class="invoice-table invoice-full-table orders-report-table">
										<thead>
											<tr>
												<td>ID</td>
												<td>{{ __('translations.date') }}</td>
												<td>
													@if (Auth::user()->type == config('types.clinic'))
														{{ __('translations.created_by') }}
													@endif
													@if (Auth::user()->type == config('types.lab'))
														{{ __('translations.executed_by') }}
													@endif
												</td>
												<td>{{ __('translations.status') }}</td>
												<td>{{ __('translations.payment_status') }}</td>
												<td>{{ __('translations.amount') }}</td>
											</tr>
										</thead>
										<tbody>
											@php $total = 0.00; @endphp
											@foreach($order_branche['report_data']['orders'] as $order) 
												<tr>
													<td><a href="{{ route('orders.edit', $order->id) }}">#{{ $order->id }}</a></td>
													<td>{{ date('m-d-Y', strtotime($order->created_at)) }}</td>
													<td>
														@if (Auth::user()->type == config('types.clinic'))
															{{ $order->user_name }}
														@endif
														@if (Auth::user()->type == config('types.lab'))
															@if ($order->executor_id != 0)
																{{ $order->executor_name }}
															@else 
																{{ __('translations.not_assigned') }}
															@endif
														@endif
													</td>
													<td>{{ $order->status_info->name }}</td>
													<td>{{ $order->payment_info->name }}</td>
													<td style="text-align: right">
														@if ($order->total && $order->total->price && $order->total->price != '0.00')
															@php $total = (float)$total + (float)$order->total->price @endphp
															{{ $currency->symbol }}{{ $order->total->price }}
														@else 
															---
														@endif
													</td>
												</tr>
											@endforeach
										</tbody>
										<tfoot>
											<tr>
												<td colspan="4"></td>
												<td colspan="2">
													<div class="fx fac fxb">
														<strong>{{ __('translations.total') }}</strong>
														<span><strong>{{ $currency->symbol }}{{ number_format($total, 2) }}</strong></span>
													</div>
												</td>
											</tr>
										</tfoot>
									</table>
								</div>								
								<br>
								<div class="form-line print-btn-block not-print">
									
										<div class="btn btn-small btn-green" onclick="exportToExcel({{ $branch['id'] }}, {{ $order_branche['branch']['id'] }})">
										    {{ __('translations.export_excel') }}
									    </div>
								
									
									<div class="btn btn-small" onclick="printContent('invoice_print_content{{ $cnt }}');">{{ __('translations.print') }}</div>&nbsp;
								</div>
							</div>
						</div>
					</div>
				@endif
				@php $cnt++; @endphp
			@endforeach	
		@endforeach	
	</div>		
@stop

@section('scripts')
	<script src="/js/select.js" defer></script>
	<script src="/js/moment.min.js" defer></script>
	<script src="/js/daterangepicker.js" defer></script>
	<script>
		document.addEventListener('DOMContentLoaded', function(){
			$('#branch, #order_branche').select2({
				minimumResultsForSearch: -1
			});

			$('#date').on('focus', function(){
				$(this).daterangepicker({				
					singleDatePicker: true,
					showDropdowns: true,
					startDate: '{{ date('y-m') }}',
					autoApply: true,
					locale: {
				        format: 'YYYY-MM-DD'
				    },
				}).on('showCalendar.daterangepicker', function (ev, picker) {
					var days = document.querySelectorAll('.table-condensed tbody td.available');

					for (var i = 0; i <= days.length; i++) {
						if (days[i].classList.contains('off')) {
							continue;
						} else {
							days[i].classList.add('active');
							days[i].classList.add('start-date');
							days[i].classList.add('end-date');

							var month_value = parseInt(document.querySelector('.monthselect').value) + 1;
							var month = month_value < 10 ? '0'+month_value : month_value;
							var year = document.querySelector('.yearselect').value;

							picker.setStartDate(moment(year+'-'+month+'-'+days[i].innerHTML, 'YYYY-MM-DD'));
							
							break;
						}
					}

				  	$(this).val(picker.startDate.format('YYYY-MM-DD'));
				  	$(document).find('.applyBtn').click();
				});
			});
		}, !1);

		function printContent(id) {
			var new_window =window.open();
			var html = '<link rel="stylesheet" href="{{ mix('/css/styles.css') }}">';

			html += '<link rel="stylesheet" href="{{ mix('/css/account.css') }}">';
			html += '<link rel="stylesheet" href="{{ mix('/css/print_invoice.css') }}">';
			html += '<div class="fx fxb print-source"><span>{{ config('app.url') }}</span><span>{{ date('m-d-Y') }}</span></div>';
			html += document.getElementById(id).innerHTML;

			new_window.document.write(html);

			setTimeout(function(){
				new_window.print();
				new_window.close();
			}, 1000);
		}

		function exportToExcel(branch_id, to_branch_id) {
			showLoader();
			$.ajax({
				url: '{{ route('excel') }}',
				type: 'post',
	            dataType: 'json',
	            headers: {
	                'X-CSRF-TOKEN': '{{ csrf_token() }}'
	            },
			    data: {type: '{{ $data->slug }}', branch_id: branch_id, to_branch_id: to_branch_id},
			    success: function(data) {
			    	if (data) {
			    		hideLoader();
			    		showPopup('<div class="success"><a class="btn" href="'+data+'" onclick="document.getElementById(\'cboxClose\').click();">Download</a></div>', true);
			    	}
			    }						    
			});
		}
	</script>	
@stop