@extends('Frontend/account_master')

@section('css')
	<link rel="stylesheet" href="/css/select.css">
	<link rel="stylesheet" href="/css/daterangepicker.css">
	<link rel="stylesheet" href="{{ mix('/css/account.css') }}">
	<style>
		.table-condensed thead tr:nth-child(2), .table-condensed tbody {
			display: none
		}
		.daterangepicker select.monthselect{
			min-width: 120px;
		}
	</style>
@stop

@section('content')
	<div id="panel_top_buttons" class="fx fac fxb">
		@include('Frontend.templates.breadcrumbs')
		<div class="panel-top-buttons button-submit order-buttons">
			<a class="btn btn-small btn-grey" href="{{ route('reports') }}">&larr; {{ __('translations.back') }}</a>
			<div class="btn btn-small" onclick="printContent('invoice_print_content');">{{ __('translations.print_all_prescriptions') }}</div>
		</div>
	</div>
	<div class="panel panel-transparent">
		<form method="GET" action="{{ route('report', $data->slug) }}" id="filters_block" class="fx fac">
			{{ csrf_field() }}	
			@if ($branches && count($branches) && (Auth::user()->role_id == config('roles.lab_head') || Auth::user()->role_id == config('roles.clinic_head')))
				<div class="form-filter">
					<select name="branch" id="branch" class="form-input">	
						<option value="0"> -- {{ __('translations.branch') }} --</option>
						@foreach($branches as $branch)
							@if($branch['status'] != 4)
								<option value="{{ $branch['id'] }}" @if(isset($filters['branch']) && $filters['branch'] == $branch['id']){{ 'selected="selected"' }}@endif>{{ $branch['name'] }}</option>
							@endif
						@endforeach
					</select>		
				</div>
			@endif	
			<div class="form-filter">
				<select name="order_branche" id="order_branche" class="form-input">	
					<option value="0">
						@if (Auth::user()->type == config('types.clinic'))
							-- {{ __('translations.select').' '.__('translations.lab') }} --
						@endif
						@if (Auth::user()->type == config('types.lab'))
							-- {{ __('translations.select').' '.__('translations.clinic') }} --
						@endif
					</option>
					@foreach($order_branches as $order_branche)
						@if (Auth::user()->type == config('types.clinic'))
							<option value="{{ $order_branche['to_branch_id'] }}" @if(isset($filters['order_branche']) && $filters['order_branche'] == $order_branche['to_branch_id']){{ 'selected="selected"' }}@endif>{{ $order_branche['to_branch']['name'] }}</option>
						@endif
						@if (Auth::user()->type == config('types.lab'))
							<option value="{{ $order_branche['branch_id'] }}" @if(isset($filters['order_branche']) && $filters['order_branche'] == $order_branche['branch_id']){{ 'selected="selected"' }}@endif>{{ $order_branche['branch']['name'] }}</option>
						@endif					
					@endforeach
				</select>	
			</div>
			<div class="form-filter">
				<input type="text" name="date" id="date" class="form-input" placeholder="{{ __('translations.from_date') }}" value="@if(isset($filters['date']) && $filters['date']){{ $filters['date'] }}@endif">			
			</div>
			<div class="filter-buttons" style="text-align: right;">					
				<input type="submit" class="btn btn-small" value="{{ __('translations.filter') }}">
				<a href="{{ route('report', $data->slug) }}" class="btn btn-danger btn-small">{{ __('translations.clear') }}</a>
			</div>
		</form>
	</div>
	<!--<h1 class="tc">{{ $seo->name }}</h1>-->
	<div id="invoice_print_content">
		@php 
			$cnt = 0;
		@endphp
		@foreach ($content as $branch)
			@foreach($branch['order_branches'] as $order_branche)
				@if ($order_branche['report_data'] && $order_branche['report_data']['invoices'] && count($order_branche['report_data']['invoices']))
					@php $show_pay_button = false; @endphp
					<div class="fx fxb panel-body">
						<div class="fx fac fxb panel">
							<div id="invoice_print_content{{ $cnt }}" class="invoice-print-content">
								<div style="width: 1022px;position: relative; page-break-after: always;">
									<div class="fx fxb invoice-to_branch-info">
										<div class="invoice-to_branch-left">
											@if (Auth::user()->type == config('types.clinic'))
												<p><strong>{{ $order_branche['branch']['name'] }}</strong></p>
												<br>
												<p>{{ $order_branche['branch']['zip_code'] .', '. $order_branche['branch']['address'] .', '. App\Services\Userdata::getLocality($order_branche['branch']['locality'] ) }}</p>
												<p>Tel: {{ $order_branche['branch']['phone'] }}</p>
											@endif
											@if (Auth::user()->type == config('types.lab'))
												<p><strong>{{ $branch['name']}}</strong></p>
												<br>
												<p>{{ $branch['zip_code'] .', '. $branch['address'] .', '. App\Services\Userdata::getLocality($branch['locality']) }}</p>
												<p>Tel: {{ $branch['phone'] }}</p>
											@endif
										</div>
										<div class="invoice-to_branch-right">
											<div class="invoice-invoice-head">{{ $data->name }}</div>
											<br>
											<table class="invoice-table invoice-one-table invoice-table-date">
												<thead>
													<tr>
														<td>{{ __('translations.date') }}</td>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td>{{ date('m-t-Y', strtotime($filters['date'])) }}</td>
													</tr>
												</tbody>
											</table>						
										</div>
									</div>
									<div class="fx fxb invoice-branch-info">
										<table class="invoice-table invoice-short-table invoice-table-bill">
											<thead>
												<tr>
													<td>To</td>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td>
														@if (Auth::user()->type == config('types.lab'))
															<p>{{ $order_branche['branch']['name']}}</p>		
															<p>{{ $order_branche['branch']['zip_code'] .', '. $order_branche['branch']['address'] .', '. App\Services\Userdata::getLocality($order_branche['branch']['locality']) }}</p>
														@endif
														@if (Auth::user()->type == config('types.clinic'))
															<p>{{ $branch['name'] }}</p>
															<p>{{ $branch['zip_code'] .', '. $branch['address'] .', '. App\Services\Userdata::getLocality($branch['locality']) }}</p>
														@endif	
													</td>
												</tr>
											</tbody>
										</table>
									</div>
									<div class="fx invoice-infos" style="justify-content: flex-end;">
										<table class="invoice-table statement-table-totals">
											<thead>
												<tr>
													<td>{{ __('translations.amount_due') }}</td>
													<td>{{ __('translations.amaunt_enc') }}</td>									
												</tr>
											</thead>
											<tbody>
												<tr>
													<td>{{ $currency->symbol }}{{ $order_branche['report_data']['amount_due'] }}</td>
													<td></td>
												</tr>
											</tbody>
										</table>
									</div>
									<div class="invoice-infos">
										<table class="invoice-table invoice-full-table statement-table-invoices">
											<thead>
												<tr>
													<td>{{ __('translations.date') }}</td>
													<td>{{ __('translations.transaction') }}</td>
													<td>{{ __('translations.amount') }}</td>
													<td>{{ __('translations.balance') }}</td>
												</tr>
											</thead>
											<tbody>
												<tr>
													@php $balance = $order_branche['report_data']['balance']; @endphp
													<td>{{ $order_branche['report_data']['first_date'] }}</td>
													<td>{{ __('translations.balance_forward') }}</td>
													<td></td>
													<td>{{ $balance }}</td>
												</tr>
												@foreach ($order_branche['report_data']['invoices'] as $invoice)
													<tr>
														<td>{{ date('m-d-Y', strtotime($invoice->created_at)) }}
															@if (Auth::user()->type == config('types.clinic') && $can_pay && $invoice->payment_status != 4 && $invoice->payment_status != 5)
															<div class="not-print" style="display: none;">
																@php $show_pay_button = true; @endphp
																<input type="checkbox" id="invoice_{{ $invoice->id }}" class="invoices-check" name="invoices[{{ $invoice->id }}]" value="{{ $invoice->id }}" checked="checked">
															</div>
															@endif
														</td>
														<td>
															{{ $invoice->name }} Due {{ date('m-d-Y', strtotime($invoice->created_at)) }}
															<br>
															@foreach($invoice->prescriptions as $prescription)
																@php
																	$quantity = 1;

																	if ($prescription->price_for == 1 && $prescription->price_order_date != '0.00' && $prescription->advance_price != '0.00') {
																		$quantity = round((float)$prescription->advance_price / (float)$prescription->price_order_date);
																	}

																	$price_each = number_format((float)$prescription->price / $quantity, 2);
																@endphp
																{{ $prescription->service->name }}, {{ $quantity }} @ {{ $currency->symbol }}{{ $price_each }} = {{ $prescription->price }}
																<br>
															@endforeach
															{{ $invoice->order_info->patient_name.' '.$invoice->order_info->patient_lastname }}
														</td>
														<td>
															<br>
															@foreach($invoice->prescriptions as $prescription)
																@if ($prescription->paid == 1) 
																	0.00
																@else
																	{{ $prescription->price }}
																@endif
																<br>
															@endforeach
														</td>
														<td>
															<br>
															@foreach($invoice->prescriptions as $prescription)
																@php
																	if ($prescription->paid != 1) {
																		$balance = number_format((float)$balance + (float)$prescription->price, 2);
																	}
																@endphp
																{{ $balance }}
																<br>
															@endforeach
														</td>
													</tr>
												@endforeach
											</tbody>
										</table>
									</div>
									<div class="invoice-infos">
										<table class="invoice-table invoice-full-table statement-table-dates">
											<thead>
												<tr>
													<td>{{ __('translations.current') }}</td>
													<td>{{ __('translations.1_days_due') }}</td>
													<td>{{ __('translations.31_days_due') }}</td>
													<td>{{ __('translations.61_days_due') }}</td>
													<td>{{ __('translations.91_days_due') }}</td>
													<td>{{ __('translations.amount_due') }}</td>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td>{{ $order_branche['report_data']['current'] }}</td>
													<td>{{ $order_branche['report_data']['1_days_due'] }}</td>
													<td>{{ $order_branche['report_data']['31_days_due'] }}</td>
													<td>{{ $order_branche['report_data']['61_days_due'] }}</td>
													<td>{{ $order_branche['report_data']['91_days_due'] }}</td>
													<td>{{ $currency->symbol }}{{ $order_branche['report_data']['amount_due'] }}</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
								<br>
								@if (Auth::user()->type == config('types.clinic') && $can_pay && $show_pay_button)
									<div class="form-line print-btn-block not-print">
										<div class="panel panel-table-controls pay-controls not-print">
											<div class="btn btn-small btn-danger" id="pay_selected" onclick="paySelectedInvoices();">
												{{ __('translations.pay_unpaid_from_list') }}
											</div>
										</div>
									</div>
								@endif
							</div>
						</div>
					</div>
				@endif
				@php $cnt++; @endphp
			@endforeach	
		@endforeach	
		
	</div>		
@stop

@section('scripts')
	<script src="/js/select.js" defer></script>
	<script src="/js/moment.min.js" defer></script>
	<script src="/js/daterangepicker.js" defer></script>
	<script>
		document.addEventListener('DOMContentLoaded', function(){
			$('#branch, #order_branche').select2({
				minimumResultsForSearch: -1
			});

			$('#date').on('focus', function(){
				$(this).daterangepicker({				
					singleDatePicker: true,
					showDropdowns: true,
					startDate: '{{ date('y-m') }}',
					autoApply: true,
					locale: {
				        format: 'YYYY-MM-DD'
				    },
				}).on('showCalendar.daterangepicker', function (ev, picker) {
					var days = document.querySelectorAll('.table-condensed tbody td.available');

					for (var i = 0; i <= days.length; i++) {
						if (days[i].classList.contains('off')) {
							continue;
						} else {
							days[i].classList.add('active');
							days[i].classList.add('start-date');
							days[i].classList.add('end-date');

							var month_value = parseInt(document.querySelector('.monthselect').value) + 1;
							var month = month_value < 10 ? '0'+month_value : month_value;
							var year = document.querySelector('.yearselect').value;

							picker.setStartDate(moment(year+'-'+month+'-'+days[i].innerHTML, 'YYYY-MM-DD'));
							
							break;
						}
					}

				  	$(this).val(picker.startDate.format('YYYY-MM-DD'));
				  	$(document).find('.applyBtn').click();
				});
			});
		}, !1);

		function printContent(id) {
			var new_window =window.open();
			var html = '<link rel="stylesheet" href="{{ mix('/css/styles.css') }}">';

			html += '<link rel="stylesheet" href="{{ mix('/css/account.css') }}">';
			html += '<link rel="stylesheet" href="{{ mix('/css/print_invoice.css') }}">';
			html += '<div class="fx fxb print-source"><span>{{ config('app.url') }}</span><span>{{ date('m-d-Y') }}</span></div>';
			html += document.getElementById(id).innerHTML;

			new_window.document.write(html);

			setTimeout(function(){
				new_window.print();
				new_window.close();
			}, 1000);
		}
	</script>	
	@include('Frontend.templates.invoice_payment_script')
@stop