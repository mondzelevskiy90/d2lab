@extends('Frontend/account_master')

@section('css')
	<link rel="stylesheet" href="{{ mix('/css/account.css') }}">
@stop

@section('breadcrumbs')
	@include('Frontend.templates.breadcrumbs')
@stop

@section('content')
	<h1>{{ $seo->name }}</h1>
	<div class="fx fxb panel-body">
		<div class="panel panel-table">
			@if ($data && count($data)) 
				<ul class="report-list">
					@foreach ($data as $item) 
						<li><a href="{{ route('report', $item->slug) }}" class="bi report-list-item" style="background-image: url('{{url('/').'/storage/'.$item->icon }}')">
								{{ $item->name }}
							</a>
						</li>
					@endforeach
				</ul>
			@endif
		</div>
	</div>
@stop

@section('scripts')

@stop