@extends('Frontend/account_master')

@section('css')
	<link rel="stylesheet" href="{{ mix('/css/account.css') }}">
@stop

@section('breadcrumbs')
	@include('Frontend.templates.breadcrumbs')
@stop

@section('content')
	<h1>{{ $seo->name }}</h1>
	<div class="fx fxb panel-body">
		<div class="panel panel-table">
			@if ($data && count($data)) 
				<div class="account-list-table account-list-table4">
					<div class="fx account-list-head">						
							<div class="account-list-cell">{{ __('translations.name') }}</div>
							<div class="account-list-cell">{{ __('translations.address') }}</div>
							<div class="account-list-cell">{{ __('translations.logo') }}</div>
							<div class="account-list-cell"></div>						
					</div>
					<div class="account-list-content">
						@foreach($data as $item)
							<div class="fx fac account-list-line">
								<div class="account-list-cell"><a href="{{ route('branch', ['labs', $item->branch_id]) }}">{{ $item->branch->name }}</a></div>
								<div class="account-list-cell">{{ app('App\Services\Userdata')::getUserAddress($item->branch) }}</div>
								<div class="account-list-cell">
									<div class="bi company-logo" style="margin: auto; background-image: url(/storage/{{ $item->branch->logo}})"></div>
								</div>								
								<div class="account-list-cell">
									<a href="{{ route('checkout') }}?branch_id={{ $item->branch_id }}" class="btn btn-small">{{ __('translations.order') }}</a>
									<div data-branch="{{ $item->branch_id }}" class="btn btn-danger btn-small delete-branch-from-list">{{ __('translations.delete') }}</div>
								</div>
							</div>
						@endforeach
					</div>
				</div>
				<div class="pagination-block">{{ $data->appends(\Request::except('page'))->links() }}</div>
			@else 
				<p style="text-align: center;">{{ __('translations.no_mylabs') }}</p>				
				<p style="text-align: center;"><a class="btn btn-primary" href="{{ url('catalog/dentistry/labs') }}">{{ __('translations.view_all_labs') }}</a></p>
			@endif
		</div>
	</div>
@stop

@section('scripts')
	<script>
		document.addEventListener('DOMContentLoaded', function() {		 	
			$('.delete-branch-from-list').on('click', function(){
				if (confirm('{{ __('translations.delete_alert') }}')) {
					var user_id = '{{ Auth::user()->id }}',
						branch_id = $(this).data('branch');				

					$.ajax({
						url: '/ajax/getData',
						type: 'post',
		                dataType: 'json',
		                headers: {
		                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
		                },
					    data: {action:'addToMyLabs', branch_id: branch_id, user_id: user_id},
					    success: function(data) {				    	
					    	if (data) {				    		
					    		window.location.href = '{{ route('mylabs') }}';
					    	}
					    }
					});
				}
			});			
		}, !1);
	</script>
@stop