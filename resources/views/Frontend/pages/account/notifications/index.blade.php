@extends('Frontend/account_master')

@section('css')
	<link rel="stylesheet" href="{{ mix('/css/account.css') }}">
@stop

@section('breadcrumbs')
	@include('Frontend.templates.breadcrumbs')
@stop

@section('content')
	<h1>{{ $seo->name }}</h1>
	<div class="fx fxb panel-body">
		<div class="panel panel-table">
			@if ($data && count($data)) 
				<div class="account-list-table account-list-table4">
					<div class="fx account-list-head">	
						<div class="account-list-cell">{{ __('translations.notification') }}</div>					
						<div class="account-list-cell">{{ __('translations.date') }}</div>
						<div class="account-list-cell"></div>						
					</div>
					<div class="account-list-content">
						@foreach($data as $item)						
							<div id="notification_{{ $item->id }}" class="fx fac account-list-line  dashboard-notification-line">
								<div class="account-list-cell">
									{!! $item->text !!}
								</div>								
								<div class="account-list-cell">
									{{ date('m-d-Y', strtotime($item->created_at)) }}
								</div>						
								<div class="account-list-cell">
									<div class="btn btn-small" onclick="notificationViewed({{ $item->id }})">Ok!</div>
								</div>
							</div>
						@endforeach
					</div>
				</div>
				<div class="pagination-block">{{ $data->appends(\Request::except('page'))->links() }}</div>
			@endif
		</div>
	</div>
@stop

@section('scripts')
	<script>
		function notificationViewed(id) {
			$.ajax({
				url: '/ajax/getData',
				type: 'post',
			    dataType: 'json',
			    headers: {
			        'X-CSRF-TOKEN': '{{ csrf_token() }}'
			    },
			    data: {action:'notificationViewed', id: id},	
			    success: function(data) {
			    	$('#notification_'+id).remove();
			    	
			    	if ($('.dashboard-notification-line').length == 0) window.location.reload();
			    }
			});
		}
	</script>
@stop