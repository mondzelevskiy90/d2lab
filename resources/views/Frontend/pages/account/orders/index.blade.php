@extends('Frontend/account_master')

@section('css')
	<link rel="stylesheet" href="/css/select.css">
	<link rel="stylesheet" href="/css/daterangepicker.css">
	<link rel="stylesheet" href="{{ mix('/css/account.css') }}">
@stop

@section('breadcrumbs')
	@include('Frontend.templates.breadcrumbs')
@stop

@section('content')
	<h1 style="display: none;">{{ $seo->name }}</h1>	
	<div class="panel panel-transparent">
		<div id="order_filters_block" class="panel-filters" style="display: none;">
			<form method="GET" action="{{ route('orders') }}" id="filters_block" class="fx fac">		
				{{ csrf_field() }}	
				@if ($branches && count($branches))
					<div class="form-filter">
						<select name="branch" id="branch" class="form-input" onchange="document.getElementById('filters_block').submit();">	
							<option value="0"> -- {{ __('translations.branch') }} --</option>
							@foreach($branches as $branch)
								@if($branch->status != 4)
									<option value="{{ $branch->id }}" @if(isset($filters['branch']) && $filters['branch'] == $branch->id){{ 'selected="selected"' }}@endif>{{ $branch->name }}</option>
								@endif
							@endforeach
						</select>		
					</div>
				@endif	
				@if ($branch_staff && count($branch_staff))
					<div class="form-filter">
						<select name="branch_staff" id="branch_staff" class="form-input">	
							<option value="0"> -- {{ __('translations.branch_staff') }} --</option>
							@foreach($branch_staff as $branch_user)
								@if($branch_user->status != 5 && $branch_user->user)
									<option value="{{ $branch_user->user_id }}" @if(isset($filters['branch_staff']) && $filters['branch_staff'] == $branch_user->user_id){{ 'selected="selected"' }}@endif>{{ $branch_user->user->name.' '.$branch_user->user->surname }}</option>
								@endif
							@endforeach
						</select>		
					</div>
				@endif		
				<div class="form-filter">
					<select name="status" id="status" class="form-input">	
						<option value="0"> -- {{ __('translations.status') }} --</option>
						@foreach($order_statuses as $status)
							<option value="{{ $status->id }}" @if(isset($filters['status']) && $filters['status'] == $status->id){{ 'selected="selected"' }}@endif>{{ $status->name }}</option>
						@endforeach
					</select>		
				</div>
				<div class="form-filter">
					<select name="order_type" id="order_type" class="form-input">	
						<option value="0"> -- {{ __('translations.order_type') }} --</option>
						@foreach($order_types as $type)
							<option value="{{ $type->id }}" @if(isset($filters['order_type']) && $filters['order_type'] == $type->id){{ 'selected="selected"' }}@endif>{{ $type->name }}</option>
						@endforeach
					</select>		
				</div>
				<div class="form-filter">
					<select name="payment_status" id="payment_status" class="form-input">	
						<option value="0"> -- {{ __('translations.payment_status') }} --</option>
						@foreach($payment_statuses as $payment_status)
							<option value="{{ $payment_status->id }}" @if(isset($filters['payment_status']) && $filters['payment_status'] == $payment_status->id){{ 'selected="selected"' }}@endif>{{ $payment_status->name }}</option>
						@endforeach
					</select>		
				</div>
				<div class="form-filter">
					<input type="text" name="search" id="search" class="form-input" placeholder="{{ __('translations.search_order_id') }}" value="@if(isset($filters['search']) && $filters['search']){{ $filters['search'] }}@endif">			
				</div>
				<div class="form-filter">
					<input type="text" name="patient" id="patient" class="form-input" placeholder="{{ __('translations.patient') }}" value="@if(isset($filters['patient']) && $filters['patient']){{ $filters['patient'] }}@endif">			
				</div>
				<div class="form-filter">
					<input type="text" name="ship_date" id="ship_date" class="form-input" placeholder="{{ __('translations.ship_date') }}" value="@if(isset($filters['ship_date']) && $filters['ship_date']){{ $filters['ship_date'] }}@endif">			
				</div>
				<div class="filter-buttons" style="text-align: right;">					
					<input type="submit" class="btn btn-small" value="{{ __('translations.filter') }}">
					<a href="{{ route('orders') }}" class="btn btn-danger btn-small">{{ __('translations.clear') }}</a>
				</div>
			</form>
		</div>
		<div class="fx fxb panel-buttons">			
			<div class="fx fac">
				<div id="show_filter_block" class="btn">{{ __('translations.filter_btn') }}</div>				
			</div>			
			@if (Auth::user()->type == config('types.clinic'))
				<a href="{{ route('checkout') }}" class="btn btn-primary ">{{ __('translations.create').' '.__('translations.order') }}</a>
			@endif
		</div>
			
	</div>
	<div class="fx fxb panel-body">
		<div class="panel panel-table">
			@if ($data && count($data)) 
				<div class="account-list-table account-order-table">
					<div class="fx account-list-head">											
							<div class="account-list-cell">#ID</div>
							<div class="account-list-cell tooltip-block">
								{{ __('translations.status') }}
								<span class="tooltip-icon">?</span>
								<div class="content-tooltip">
									<div><span class="tooltip-color-item" style="background-color: #75DB78">D</span> - {{ __('translations.green_action') }}</div>
									<div><span class="tooltip-color-item" style="background-color: #E2B222">D</span> - {{ __('translations.yellow_action') }}</div>
									<div><span class="tooltip-color-item" style="background-color: #DB6827">D</span> - {{ __('translations.orange_action') }}</div>
									<div><span class="tooltip-color-item" style="background-color: red">D</span> - {{ __('translations.red_action') }}</div>
									<div><span class="tooltip-color-item" style="background-color: #B3B3B3">D</span> - {{ __('translations.grey_action') }}</div>
									<div><span class="tooltip-color-item" style="background-color: #75DB78">D</span> - {{ __('translations.dentist_action') }}</div>
									<div><span class="tooltip-color-item" style="background-color: #75DB78">L</span> - {{ __('translations.lab_action') }}</div>

								</div>
							</div>
							<div class="account-list-cell">{{ __('translations.patient') }}</div>
							<div class="account-list-cell">
								@if (Auth::user()->type == config('types.clinic'))
									{{ __('translations.lab') }}
								@endif
								@if (Auth::user()->type == config('types.lab'))
									{{ __('translations.clinic') }}
								@endif
							</div>
							<div class="account-list-cell">{{ __('translations.branch') }}</div>
							<div class="account-list-cell">{{ __('translations.payment_status') }}</div>
							<div class="account-list-cell">{{ __('translations.delivery_status') }}</div>
							<div class="account-list-cell">{{ __('translations.ship_date') }}</div>
							<div class="account-list-cell"></div>						
					</div>
					<div class="account-list-content">		
						@foreach($data as $item)
						    @php
						    	$line_class = '';
						    	$show_line_class = false;

						    	if (Auth::user()->type == config('types.lab')) {
									$check_date = date('U', strtotime('+2 days'));
						    	} else {
						    		$check_date = date('U', strtotime('+1 day'));
						    	}

						    	if (date('U', strtotime(str_replace('-', '/', $item->ship_date))) <= $check_date) {
						    		$show_line_class = true;
						    	}


						    	if (in_array($item->status, $hightlight_ship_statuses) && $show_line_class) {
									$line_class = 'account-list-lineRed';
						    	}
						    @endphp
							<div class="fx fac account-list-line {{ $line_class }}">								
								<div class="account-list-cell">
									#{{ $item->id.($item->name ? ' ('.$item->name.')' : '').($item->version && $item->version > 1 ? ' v.'.$item->version : '') }} 
									<br>
									@if ($item->status_info)
										<div class="bi account-table-progress account-table-progress{{ $item->status_info->progress }}">
											<span></span>										
										</div>
									@endif
									@if ($item->created_at) 
										{{ date('m-d-Y', strtotime($item->created_at)) }}
									@endif
								</div>	
								<div class="account-list-cell">
									<div class="account-table-status">
									@php 
										$letter = false;
										$style = '';
										$colors = json_decode($item->status_info->status_color);

										if ($item->status_info->change_by == config('types.clinic')) {
											$letter = 'D';
											$style .= 'left:-10px;';
										}

										if ($item->status_info->change_by == config('types.lab')) {
											$letter = 'L';
											$style .= 'right:-10px;';
										}

										if (Auth::user()->type == config('types.clinic')) $style .= 'background-color:'.$colors->cli;
										if (Auth::user()->type == config('types.lab')) $style .= 'background-color:'.$colors->lab;

										if ($item->need_confirmation == 1 && $item->status_info->confirm_by != 0) {
											$style = 'background-color: red;';

											if ($item->status_info->change_by == config('types.clinic')) {
												$letter = 'L';
												$style .= 'right:-10px;';
											}

											if ($item->status_info->change_by == config('types.lab')) {
												$letter = 'D';
												$style .= 'left:-10px;';
											}
										}

										if ($item->ship_date_changed && Auth::user()->type == config('types.clinic')) {
											$style = 'background-color: red;';
											$letter = 'D';
											$style .= 'left:-10px;';
										}

									@endphp	
									@if ($item->status_info->icon)										
										<img src="{{ url('/').'/storage/'.$item->status_info->icon }}" style="max-width: 35px;height:auto;" title="{{ $item->status_info->name }}" alt="{{ $item->status_info->name }}"> 
									@else 
										{{ $item->status_info->name }}
									@endif
									@if ($letter)
										<span style="{{ $style }}">{{ $letter }}</span>
									@endif
									</div>
								</div>	
								<div class="account-list-cell">{{ $item->patient_name }} {{ $item->patient_lastname }} @if(isset($item->patient_age)){{ $item->patient_age }} y.o.@endif</div>
								<div class="account-list-cell">
									@if (Auth::user()->type == config('types.clinic'))
										{{ $item->to_branch_name }}
									@endif
									@if (Auth::user()->type == config('types.lab'))
										{{ $item->branch_name }}
									@endif
								</div>	
								<div class="account-list-cell">
									@if (Auth::user()->type == config('types.clinic'))
										{{ $item->branch_name }}
									@endif
									@if (Auth::user()->type == config('types.lab'))
										{{ $item->to_branch_name }}
									@endif
								</div>	
								<div class="account-list-cell">
									@if ($item->payment_info->icon && $item->payment_info->id != 1)	
										<img src="{{ url('/').'/storage/'.$item->payment_info->icon }}" style="max-width: 35px;height:auto;" title="{{ $item->payment_info->name }}" alt="{{ $item->payment_info->name }}"> 
									@else 
										{{ $item->payment_info->name }}
									@endif
								</div>			
								<div class="account-list-cell">									
									@if ($item->delivery_service && count($item->delivery_service))
										@foreach($item->delivery_service as $delivery_service)
											@php 
												$direction = 'right';

												if ($item->to_branch_id == $delivery_service->from_branch) {
													$direction = 'left';
												}  

												$delivey_data = $delivery_service->delivery_info->name;

												if (isset($delivery_service->delivery_invoice) && $delivery_service->delivery_invoice) {
													if ($delivery_service->delivery_info->tracking_link) {
														if (strpos($delivery_service->delivery_info->tracking_link, 'replace_number') !== false) {
															$delivey_data .= '<br>(<a target="blank" href="'.str_replace('replace_number', $delivery_service->delivery_invoice, $delivery_service->delivery_info->tracking_link).'">'.__('translations.tracking').'</a>)';
														}
													} 
												}

												if ($delivery_service->received == 1) {
													if ($direction == 'left') {
														$delivey_data = 'In clinic';
													} else {
														$delivey_data = 'In lab';
													}

													$direction = false;
												}
 											@endphp
 											@if ($direction)
												<div class="bi delivery-history-arrow delivery-history-{{ $direction }}"></div>
											@endif
											<div class="delivery-history-name">
												{!! $delivey_data !!}
											</div>
											@php break; @endphp
										@endforeach
									@else 
										---
									@endif
								</div>								
								<div class="account-list-cell" style="@if($line_class != '' || $item->ship_date_changed) font-weight:700;color:red; @endif">
									@if ($item->ship_date_changed)
										{{ $item->ship_date_changed }}
									@else 
										{{ $item->ship_date }}
									@endif
								</div>
								<div class="account-list-cell">
									<a href="{{ route('orders.edit', [$item->id, $requests]) }}" class="btn btn-small">
										{{ __('translations.view') }}
										@if (!is_null($item->chat) && is_null($item->chat_view))
											<div class="bi account-table-chat"></div>
										@endif
									</a>
									@if (Auth::user()->type == config('types.clinic') && in_array($item->status, [1,2,3]))	
										<br>
										<div data-order="{{ \Illuminate\Support\Facades\Crypt::encrypt($item->id) }}" class="btn btn-danger btn-small delete-order-from-list" style="margin-top:5px;">{{ __('translations.delete') }}</div>
									@endif								
								</div>
							</div>
						@endforeach
					</div>					
				</div>
				<div class="pagination-block">{{ $data->appends(\Request::except('page'))->links() }}</div>
			@else 
				<p>{{ __('translations.no_orders') }}</p>
			@endif
		</div>
	</div>

@stop

@section('scripts')
	<script src="/js/select.js" defer></script>
	<script src="/js/moment.min.js" defer></script>
	<script src="/js/daterangepicker.js" defer></script>
	<script>
		document.addEventListener('DOMContentLoaded', function() {
			detectActivity(60);

			$('#order_type').select2({
				minimumResultsForSearch: -1
			});

			$('#branch').select2({
				minimumResultsForSearch: -1
			});

			$('#branch_staff').select2({
				minimumResultsForSearch: -1
			});

			$('#status').select2({
				minimumResultsForSearch: -1
			});

			$('#payment_status').select2({
				minimumResultsForSearch: -1
			});

			$('#ship_date').on('focus', function(){
				$('#ship_date').daterangepicker({				
					singleDatePicker: true,
					showDropdowns: true,
					startDate: false,				
					locale: {
				        format: 'MM-DD-YYYY'
				    },
				});
			});

			$('#show_filter_block').on('click', function(){
				$(this).toggleClass('show-filter-close');
				$('#order_filters_block').slideToggle();
			});

			$('.delete-order-from-list').on('click', function(){
				if (confirm('{{ __('translations.delete_alert') }}')) {
					var user_id = '{{ Auth::user()->id }}',
						order = $(this).data('order');				

					$.ajax({
						url: '/ajax/getData',
						type: 'post',
		                dataType: 'json',
		                headers: {
		                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
		                },
					    data: {action:'markOrderAsDeleted', order: order},
					    success: function(data) {				    	
					    	if (data) {				    		
					    		window.location.href = '{{ route('orders') }}';
					    	}
					    }
					});
				}
			});
		}, !1);
	</script>
@stop