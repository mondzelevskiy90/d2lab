@extends('Frontend/account_master')

@section('css')
	<link rel="stylesheet" href="{{ mix('/css/account.css') }}">
@stop

@section('breadcrumbs')
	@include('Frontend.templates.breadcrumbs')
@stop

@section('content')
	<h1>{{ $seo->name }}</h1>
	@if ($branches && count($branches) && count($branches) != count($data))
		<div class="panel panel-transparent">
			<div class="panel-buttons">
				<a href="{{ route('branch_prescription.create') }}" class="btn btn-primary">{{ __('translations.create') }}</a>
			</div>
			<div class="panel-filters"></div>
		</div>
	@endif
	<div class="fx fxb panel-body">
		<div class="panel">
			@if ($data && count($data)) 
				<div class="account-list-table account-list-table3">
					<div class="fx account-list-head">						
							<div class="account-list-cell">{{ __('translations.name') }}</div>
							<div class="account-list-cell">{{ __('translations.branch') }}</div>
							<div class="account-list-cell"></div>						
					</div>
					<div class="account-list-content">
						@foreach($data as $item)
							@if ($item->branch)
							<div class="fx fac account-list-line">
								<div class="account-list-cell">{{ $item->name }}</div>
								<div class="account-list-cell">{{ $item->branch->name }}</div>
								<div class="account-list-cell">
									<a href="{{ route('branch_prescription.edit', $item->id) }}" class="btn btn-small">{{ __('translations.edit') }}</a>
									<div data-href="{{ route('branch_prescription.delete', $item->id) }}" class="btn btn-danger btn-small delete-element" data-msg="{{ __('translations.delete_alert') }}">{{ __('translations.delete') }}</div>
								</div>
							</div>
							@endif
						@endforeach
					</div>
				</div>				
			@else 
				@if ($branches && count($branches)) 
					<p>{{ __('translations.no_branch_prescription') }}</p>
				@else
					<p>{!! sprintf(__('translations.no_braches'), route('branches.create')) !!}</p>
				@endif
			@endif
		</div>
	</div>
@stop

@section('scripts')

@stop