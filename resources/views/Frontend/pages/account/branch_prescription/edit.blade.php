@extends('Frontend/account_master')

@section('css')
	<link rel="stylesheet" href="/css/select.css">
	<link rel="stylesheet" href="{{ mix('/css/account.css') }}">
@stop

@section('breadcrumbs')
	@include('Frontend.templates.breadcrumbs')
@stop

@section('content')
	<h1>{{ $seo->name }}</h1>	
	<form method="POST" action="{{ isset($data->id) ? route('branch_prescription.edit', $data->id) : route('branch_prescription.create') }}" enctype="multipart/form-data">
		<div class="fx fxb panel-body branch_prescription-edit-form">		
			{{ csrf_field() }}			
			<div class="panel">	
			    @if(isset($data->id))
			    	<div class="form-line">
			    		<label for="branch">{{ __('translations.branch') }}</label>
						<input type="text" id="branch" class="form-input" value="{{ $branch->name }}" disabled="disabled">
			    	</div>
					<div class="form-line">
						<label for="name">{{ __('translations.name') }}</label>				
						<input type="text" name="name" id="name" class="form-input" placeholder="{{ __('translations.name') }}" value="{{ old('name', $data->name) }}" required="required">
						@if ($errors->has('name'))
		                    <div class="error">{{ $errors->first('name') }}</div>
		                @endif				
					</div>
					<div class="form-line">
						<label for="status">{{ __('translations.status') }}</label>				
						<select id="status" name="status" class="form-controll" required="required">
							<option value="1" @if($data->status == 1) selected @endif>{{ __('translations.status_active') }}</option>
							<option value="2" @if($data->status == 2) selected @endif>{{ __('translations.status_disactive') }}</option>						
						</select>				
					</div>
			    @else 
					<div class="form-line">
						<label for="branch_id">{{ __('translations.branch') }}</label>
						<select name="branch_id" id="branch_id" class="form-controll" required="required">
							@foreach ($branches as $branch)
								<option value="{{ $branch->id }}">{{ $branch->name }}</option>
							@endforeach
						</select>
					</div>
			    @endif					
			</div>
			<div class="panel panel-transparent panel-buttons">
				<div class="form-line button-submit">				
					<input type="submit" id="submit" class="btn" name="submit" value="@if(isset($data->id)){{ __('translations.save_changes') }}@else{{ __('translations.save') }}@endif">
				</div>
			</div>
		</div>
	</form>	
@stop

@section('scripts')
	
@stop