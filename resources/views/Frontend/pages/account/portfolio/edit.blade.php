@extends('Frontend/account_master')

@section('css')
	<link rel="stylesheet" href="/css/select.css">
	<link rel="stylesheet" href="{{ mix('/css/account.css') }}">
@stop

@section('content')
	<form class="content-editing-form " method="POST" action="{{ isset($data->id) ? route('portfolio.edit', $data->id) : route('portfolio.create') }}" enctype="multipart/form-data" id="portfolio_form">
		<div id="panel_top_buttons" class="fx fac fxb">
			@include('Frontend.templates.breadcrumbs')
			<div class="panel-top-buttons button-submit order-buttons">
				<a class="btn btn-small btn-grey" href="{{ route('portfolios', [$requests]) }}">&larr; {{ __('translations.back') }}</a>
				<input type="submit" id="submit" class="btn btn-small" name="submit" value="@if(isset($data->id)){{ __('translations.save_changes') }}@else{{ __('translations.save') }}@endif">
			</div>
		</div>
		<h1 class="tc">{{ $seo->name }}</h1>	
		<div class="fx fxb panel-body portfolio-edit-form">
			{{ csrf_field() }}			
			<div class="panel">	
				@php $cnt = 0; @endphp
			    @if(isset($data->id))
			    	<div class="form-line">
			    		<label for="branch">{{ __('translations.branch') }}</label>
						<input type="text" id="branch" class="form-input" value="{{ $branch->name }}" disabled="disabled">
			    	</div>
					<div class="form-line">
						<label for="name">{{ __('translations.name') }}</label>				
						<input type="text" name="name" id="name" class="form-input" placeholder="{{ __('translations.name') }}" value="{{ old('name', $data->name) }}" required="required">
						@if ($errors->has('name'))
		                    <div class="error">{{ $errors->first('name') }}</div>
		                @endif				
					</div>
					<br>
					<div class="account-images-list">
						<div class="panel-head account-images-head">{{ __('translations.portfolio_cats') }}</div>
						<div class="account-images-content">							
							@foreach ($items as $item)
								<div class="account-images-line">
									<div class="account-images-lineHead">{{ $item['name'] }}</div>
									<div class="fx account-images-lineImages" id="images_block{{ $item['id'] }}">
										@foreach($item['images'] as $image)											
											<div id="image_block{{ $cnt }}" class="fx fac account-images-lineImage">
												<div class="account-images-lineRemove" onclick="removeImageBlock('{{ $cnt }}', '{{ $item['id'] }}')" title="{{ __('translations.delete_image') }}">
													<div></div>
												</div>
												<img src="{{ App\Services\Img::getThumb($image['image']) }}">
												<input type="hidden" name="images[{{ $item['id'] }}][{{ $cnt }}]" value="{{ $image['image'] }}">
											</div>
											@php $cnt++; @endphp
										@endforeach										
										<div id="images_block_add{{ $item['id'] }}" class="account-images-lineAdd" @if (count($item['images']) >= 8) style="display:none;" @endif >
											<input type="file" name="image_value" onchange="addImageToBlock($(this), '{{ $item['id'] }}')" title="{{ __('translations.add_image') }}" value="" accept="image/jpeg,image/jpg,image/png">
										</div>
									</div>
								</div>
							@endforeach							
						</div>
					</div>
			    @else 
					<div class="form-line">
						<label for="branch_id">{{ __('translations.branch') }}</label>
						<select name="branch_id" id="branch_id" class="form-controll" required="required">
							@foreach ($branches as $branch)
								<option value="{{ $branch->id }}">{{ $branch->name }}</option>
							@endforeach
						</select>
					</div>
			    @endif					
			</div>
		</div>
	</form>	
@stop

@section('scripts')
	<script>
		let cnt = {{ $cnt }};

		function addImageToBlock(elem, id) {
			var files = elem.prop('files')[0];

			if (!files || !elem.val()) return false;

			if (files.size > 5000000 || (files.type != 'image/jpeg' && files.type != 'image/png')) {
				showPopup('{{ __('translations.portfolio_file_error') }}');

				return false;
			}

			let form = $('#portfolio_form');
    		let form_data = new FormData();

    		form_data.append('image', files);
    		form_data.append('id', {{ Auth::user()->id }});
    		form_data.append('action', 'saveImage');

			$.ajax({
				url: '/ajax/getData',
				type: 'post',
                dataType: 'json',
                processData: false,
            	contentType: false,
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                },
                data: form_data,
                success: function(data) {
                	let html = '';

                	html += '<div id="image_block'+cnt+'" class="fx fac account-images-lineImage">';
					html += '<div class="account-images-lineRemove" onclick="removeImageBlock('+cnt+', '+id+')" title="{{ __('translations.delete_image') }}"><div></div></div>';
					html += '<img src="{{ url('/storage') }}/'+data['thumb']+'">';
					html += '<input type="hidden" name="images['+id+']['+cnt+']" value="'+data['image']+'">';
					html += '</div>';

					$('#images_block_add'+id).before(html);

					if ($('#images_block'+id+' > div').length < 9) {
						$('#images_block_add'+id).css('display', 'block');
					} else {
						$('#images_block_add'+id).css('display', 'none');
					}

					cnt++;
                }
			});

			
		}

		function removeImageBlock(id, parent_id) {
			$('#image_block'+id).remove();

			if ($('#images_block'+parent_id+' > div').length < 9) $('#images_block_add'+parent_id).css('display', 'block');
		}	
	</script>
@stop