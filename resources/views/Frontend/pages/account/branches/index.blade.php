@extends('Frontend/account_master')

@section('css')
	<link rel="stylesheet" href="{{ mix('/css/account.css') }}">
@stop

@section('breadcrumbs')
	@include('Frontend.templates.breadcrumbs')
@stop

@section('content')
	<h1>{{ $seo->name }}</h1>
	<div class="panel panel-transparent panel-mob-short">
		<div class="panel-filters">
			<form method="GET" action="{{ route('branches') }}" id="filters_block" class="fx fac">		
				{{ csrf_field() }}
				<div class="form-filter">
					<input type="text" name="search" id="search" class="form-input" placeholder="{{ __('translations.search_name') }}" value="@if($search){{ $search }}@endif" onchange="document.getElementById('filters_block').submit();">			
				</div>
				<div class="form-filter">					
					<a href="{{ route('branches') }}" class="btn btn-danger">{{ __('translations.clear') }}</a>
				</div>
		</div>
		@if ($data && count($data) < 51) 
		<div class="panel-buttons">
			<a href="{{ route('branches.create') }}" class="btn btn-primary">{{ __('translations.create') }}</a>
		</div>
		@endif		
	</div>
	<div class="fx fxb panel-body settings-form">
		<div class="panel panel-table">
			@if ($data && count($data)) 
				<div class="account-list-table account-list-table5">
					<div class="fx account-list-head">						
							<div class="account-list-cell">{{ __('translations.name') }}</div>
							<div class="account-list-cell">{{ __('translations.address') }}</div>
							<div class="account-list-cell">{{ __('translations.logo') }}</div>
							<div class="account-list-cell">{{ __('translations.status') }}</div>
							<div class="account-list-cell"></div>						
					</div>
					<div class="account-list-content">
						@foreach($data as $item)
							<div class="fx fac account-list-line">
								<div class="account-list-cell">
									<strong>{{ $item->name }}</strong>
									@if (Auth::user()->role_id == config('roles.clinic_head'))
										@php 
											$staff_check_str = '<span style="color:red;font-weight:bold;margin-right:5px;">&#8722;</span> <a href="'.route('staff.create').'">'.__('translations.staff').'</a>';

											$staff_check = \App\Models\Branch_staff::where('branch_id', $item['id'])->first();

											if ($staff_check) $staff_check_str = '<span style="color:green;font-weight:bold;margin-right:5px;">&#10004;</span> <a href="'.route('staff').'">'.__('translations.staff').'</a>';
										@endphp
										<div class="location-check-results">
											<div class="fx fac location-check-result">{!! $staff_check_str !!}</div>
										</div>
									@endif
									@if (Auth::user()->role_id == config('roles.lab_head'))
										@php
											$price_check_str = '<span style="color:red;font-weight:bold;margin-right:5px;">&#8722;</span> <a href="'.route('prices.create').'">'.__('translations.price').'</a>';
											$staff_check_str = '<span style="color:red;font-weight:bold;margin-right:5px;">&#8722;</span> <a href="'.route('staff.create').'">'.__('translations.staff').'</a>';
											$portfolio_check_str = '<span style="color:red;font-weight:bold;margin-right:5px;">&#8722;</span> <a href="'.route('portfolio.create').'">'.__('translations.portfolio').'</a>';

											$price_check = \App\Models\Price::where('branch_id', $item['id'])->first();
											$staff_check = \App\Models\Branch_staff::where('branch_id', $item['id'])->first();
											$portfolio_check = \App\Models\Portfolio::where('branch_id', $item['id'])->first();

											if ($price_check) $price_check_str = '<span style="color:green;font-weight:bold;margin-right:5px;">&#10004;</span> <a href="'.route('prices').'">'.__('translations.price').'</a>';
											if ($staff_check) $staff_check_str = '<span style="color:green;font-weight:bold;margin-right:5px;">&#10004;</span> <a href="'.route('staff').'">'.__('translations.staff').'</a>';
											if ($portfolio_check) $portfolio_check_str = '<span style="color:green;font-weight:bold;margin-right:5px;">&#10004;</span> <a href="'.route('portfolios').'">'.__('translations.portfolio').'</a>';
										@endphp
										<div class="location-check-results">
											<div class="fx fac location-check-result">{!! $price_check_str !!}</div>
											<div class="fx fac location-check-result">{!! $staff_check_str !!}</div>
											<div class="fx fac location-check-result">{!! $portfolio_check_str !!}</div>
										</div>
									@endif
								</div>
								<div class="account-list-cell">{{ app('App\Services\Userdata')::getUserAddress($item) }}</div>
								<div class="account-list-cell">
									<div class="bi company-logo" style="margin: auto; background-image: url(/storage/{{ $item->logo}})"></div>
								</div>
								@php 
									if ($item->status == 1) $status = __('translations.status_active');
									if ($item->status == 2) $status = __('translations.status_disactive');
									if ($item->status == 3) $status = __('translations.status_blocked');
									if ($item->verified == 1) $verified = __('translations.verified');
									if ($item->verified == 2) $verified = __('translations.not_verified');
								@endphp								
								<div class="account-list-cell">{{ $status }}</div>
								<div class="account-list-cell">
									<a href="{{ route('branches.edit', [$item->id, $requests]) }}" class="btn btn-small">{{ __('translations.edit') }}</a>
									<div data-href="{{ route('branches.delete', [$item->id, $requests]) }}" class="btn btn-danger btn-small delete-element" data-msg="{{ __('translations.delete_alert') }}">{{ __('translations.delete') }}</div>
								</div>
							</div>
						@endforeach
					</div>
					<br>
				<!--	<div class="account-list-notice" style="font-size: 12px;">*** {{ __('translations.verified_notice') }}</div> -->
				</div>
			@else 
				<p>{!! sprintf(__('translations.no_braches'), route('branches.create')) !!}</p>
			@endif
		</div>
	</div>
@stop

@section('scripts')

@stop