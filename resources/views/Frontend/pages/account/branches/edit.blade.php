@extends('Frontend/account_master')

@section('css')
	<link rel="stylesheet" href="/css/select.css">
	<link rel="stylesheet" href="{{ mix('/css/account.css') }}">
@stop

@section('content')
	<form class="editing-form content-editing-form " method="POST" action="{{ isset($data->id) ? route('branches.update', [$data->id, $requests]) : route('branches.store', $requests) }}" enctype="multipart/form-data">
		<div id="panel_top_buttons" class="fx fac fxb">
			@include('Frontend.templates.breadcrumbs')
			<div class="panel-top-buttons button-submit order-buttons">
				<a class="btn btn-small btn-grey" href="{{ route('branches', [$requests]) }}">&larr; {{ __('translations.back') }}</a>
				<input type="submit" id="submit" class="btn btn-small" name="submit" value="@if(isset($data->id)){{ __('translations.save_changes') }}@else{{ __('translations.save') }}@endif">
			</div>
		</div>
		<h1 class="tc">{{ $seo->name }}</h1>
		<div class="fx fxb panel-body branches-edit-form">		
			{{ csrf_field() }}			
			<div class="panel">	
				<div class="panel-head">{{ __('translations.branch_head') }}</div>
				@if (Auth::user()->role_id == config('roles.lab_head') && $data && !isset($data->nmi_info))
					<div class="form-line">							
						<p style="font-weight: bold;color:red">{{ __('translations.reg_nmi_account') }}</p>
						<p><a href="https://secure.nmi.com/merchants/login.php?cookie_check=1&auth_error=0">https://secure.nmi.com/merchants/login.php?cookie_check=1&auth_error=0</a></p>
						<br>
						<br>
					</div>
				@endif
				@if (!isset($data->id))
					<!--<div class="form-line">	
						<label for="industry">{{ __('translations.industry') }}</label>			
						<select id="industry" class="form-controll" required="required">
							<option value="0">{{ __('translations.select_industry_field') }}</option>
							@foreach($industries as $industry)
								<option value="{{ $industry->id }}">{{ $industry->name }}</option>
							@endforeach
						</select>		
					</div>
					<div class="form-line">	
						<label for="industry_id">{{ __('translations.industry_id') }}</label>			
						<select name="industry_id" id="industry_id" class="form-controll" required="required">
							<option value="0">{{ __('translations.select_industry_field') }}</option>
						</select>		
					</div> -->
					<input type="hidden" name="industry_id" value="2"/>
				@else 
					<div class="form-line" style="display:none">	
						<label for="industry_id">{{ __('translations.industry_id') }}</label>		
						<input type="text" id="industry_id" class="form-input" value="{{ $industry->name }}" disabled="disabled">
					</div>
				@endif
				
				<div class="form-line">	
					<label for="name">{{ __('translations.name') }}</label>			
					<input type="text" name="name" id="name" class="form-input" placeholder="{{ __('translations.name') }}" value="{{ old('name', $data->name ?? Auth::user()->company) }}" required="required" maxlength="255">
					@if ($errors->has('name'))
		                <div class="error">{{ $errors->first('name') }}</div>
		            @endif				
				</div>
				@if (!isset($data->status) || (isset($data->status) && $data->status != 3))
					<div class="form-line">
						<label for="status">{{ __('translations.status') }}</label>				
						<select id="status" name="status" class="form-controll" required="required">
							<option value="1" @if(isset($data->status) && $data->status == 1) selected @endif>{{ __('translations.status_active') }}</option>
							<option value="2" @if(isset($data->status) && $data->status == 2) selected @endif>{{ __('translations.status_disactive') }}</option>						
						</select>				
					</div>
				@endif
				<div class="form-line">	
					<label for="description">{{ __('translations.description') }}</label>			
					<textarea name="description" id="description" class="form-input" placeholder="{{ __('translations.description') }}" required="required" maxlength="1000" onchange="formatDescriptionField()">{{ old('description', $data->description ?? '') }}</textarea>
					@if ($errors->has('description'))
		                <div class="error">{{ $errors->first('description') }}</div>
		            @endif			
				</div>			
				<div class="form-line">	
					<label for="email">{{ __('translations.email') }}</label>			
					<input type="email" name="email" id="email" class="form-input" placeholder="{{ __('translations.email') }}" value="{{ old('email', $data->email ?? Auth::user()->email) }}" required="required">
					@if ($errors->has('email'))
		                <div class="error">{{ $errors->first('email') }}</div>
		            @endif				
				</div>
				<div class="form-line">		
					<label for="phone">{{ __('translations.phone') }}</label>		
					<input type="text" name="phone" id="phone" class="form-input" placeholder="{{ __('translations.phone') }}" value="{{ old('phone', $data->phone ?? Auth::user()->phone) }}" required="required">
					@if ($errors->has('phone'))
		                <div class="error">{{ $errors->first('phone') }}</div>
		            @endif				
				</div>
				<div class="form-line">	
				    <label for="zip_code">{{ __('translations.zip_code') }}</label>			
					<input type="text" name="zip_code" id="zip_code" class="form-input" placeholder="{{ __('translations.zip_code') }}" value="{{ old('zip_code', $data->zip_code ?? Auth::user()->zip_code) }}" required="required">
					@if ($errors->has('zip_code'))
		                <div class="error">{{ $errors->first('zip_code') }}</div>
		            @endif				
				</div>
				<div class="form-line">
					<label for="country">{{ __('translations.country') }}</label>
					<select id="country" name="country" class="form-controll" required="required">
						@if($locality)
							<option value="{{ $locality->country->id }}">{{ $locality->country->name }}</option>
						@else 
							<option value="0">{{ __('translations.country') }}</option>
						@endif							
					</select>
					@if ($errors->has('country'))
	                    <div class="error">{{ $errors->first('country') }}</div>
	                @endif
				</div>
				<div class="form-line">
					<label for="region">{{ __('translations.region') }}</label>
					<select id="region" name="region" class="form-controll" required="required">
						@if($locality)
							<option value="{{ $locality->region->id }}">{{ $locality->region->name }}</option>
						@else 
							<option value="0">{{ __('translations.region') }}</option>
						@endif
					</select>
					@if ($errors->has('region'))
		                <div class="error">{{ $errors->first('region') }}</div>
		            @endif
				</div>
				<div class="form-line">
					<label for="locality">{{ __('translations.locality') }}</label>
					<select id="locality" name="locality" class="form-controll" required="required">
						@if($locality)
							<option value="{{ $locality->id }}">{{ $locality->name }}</option>
						@else 
							<option value="0">{{ __('translations.locality') }}</option>
						@endif
					</select>
					@if ($errors->has('locality'))
		                <div class="error">{{ $errors->first('locality') }}</div>
		            @endif
				</div>
				<div class="form-line">
					<label for="address">{{ __('translations.address') }}</label>				
					<input type="text" name="address" id="address" class="form-input" placeholder="{{ __('translations.address') }}" value="{{ old('address', $data->address ?? Auth::user()->address) }}" required="required">
					@if ($errors->has('address'))
		                <div class="error">{{ $errors->first('address') }}</div>
		            @endif				
				</div>
				<!--<div class="form-line">	
				    <label for="tin">{{ __('translations.tin') }}</label>			
					<input type="text" name="tin" id="tin" class="form-input" placeholder="{{ __('translations.tin') }}" value="{{ old('tin', $data->tin ?? '') }}">
					@if ($errors->has('tin'))
		                <div class="error">{{ $errors->first('tin') }}</div>
		            @endif				
				</div> -->
				<div class="form-line setting-logo-line">				
					<div class="fx fxb fac">					
						<div class="fx fac company-logo-title">
							{{ __('translations.logo') }}: <div class="bi company-logo" style="background-image: url(/storage/{{ $data->logo ?? Auth::user()->logo }})"></div>
						</div>						
						<div id="file_psev_input" onclick="$('#logo').click();">
							<span>{{ __('translations.select_new_logo') }}</span>
						</div>
					</div>

					<input type="hidden" name="data_logo[status]" id="file_psev_input_status" value="0">
					<input type="hidden" name="data_logo[width]" id="file_psev_input_width" value="">
					<input type="hidden" name="data_logo[height]" id="file_psev_input_height" value="">
					<input type="hidden" name="data_logo[x]" id="file_psev_input_x" value="">
					<input type="hidden" name="data_logo[y]" id="file_psev_input_y" value="">
									
					<input type="file" class="uploaded-image" data-prefix="data_logo" data-input="file_psev_input" data-text="{{ __('translations.select_new_logo') }}" name="logo" id="logo" value="" accept="image/jpeg,image/jpg,image/png" style="height: 1px; opacity: 0;padding: 0;">
					@if ($errors->has('logo'))
		                <div class="error">{{ $errors->first('logo') }}</div>
		            @endif				
				</div>
			</div>
		</div>
	</form>	
@stop

@section('scripts')
	@include('Frontend.templates.fields_script')
	@include('Frontend.templates.field_industry_script')
	@include('Frontend.templates.img_upload_script')
	<script>
		function formatDescriptionField() {
			var text = document.getElementById('description').value;

			text = text.replace(/(\r\n|\n|\r)/gm, ' ');

			document.getElementById('description').value = text;
		}
	</script>
@stop