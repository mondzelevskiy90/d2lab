@extends('Frontend/account_master')

@section('css')
	<link rel="stylesheet" href="/css/select.css">
	<link rel="stylesheet" href="{{ mix('/css/account.css') }}">
@stop

@section('breadcrumbs')
	@include('Frontend.templates.breadcrumbs')
@stop

@section('content')
	<h1 class="tc">{{ $seo->name }}</h1>	
	<form method="POST" action="{{ route('prices.editcustom', $data->id) }}">
		<div class="fx fxb panel-body prices-edit-form">		
			{{ csrf_field() }}			
			<div class="panel">				    			    	
				<div class="form-line">
					<label for="name">{{ __('translations.clinic') }}</label>				
					<select name="branch_id" id="branch_id" onchange="changePriceServicesData(this.value)">
						<option value="0"> -- Select dentist --</option>
						@foreach($branches as $branch)
							<option value="{{ $branch->id }}">{{ $branch->name }}</option>
						@endforeach
					</select>				
				</div>
				<br><br>
				<div class="form-line">
					<div class="panel-head">
						{{ __('translations.services_list') }}
						<p style="font-size: 12px;font-weight: 400">*** {{ __('translations.select_only_needed') }}</p>
					</div>
					<div id="brach_services_table" class="account-long-list" style="display: none">
						<div class="account-list-table account-list-table3 account-list-tableServices">
							<div class="fx account-list-head">						
									<div class="account-list-cell">{{ __('translations.service_name') }}</div>
									<div class="account-list-cell">{{ __('translations.price_value') }}</div>
									<div class="account-list-cell">{{ __('translations.currency') }}</div>
									<div class="account-list-cell"></div>
							</div>
							<div id="brach_services_content" class="account-list-content"></div>
						</div>
					</div>
				</div>
						    				
			</div>
			<div class="panel panel-transparent panel-buttons">
				<div class="form-line button-submit">
					<a class="btn btn-grey btn-small" href="{{ route('prices.edit', $data->id) }}">Back</a>				
					<input type="submit" id="submit" class="btn" name="submit" value="{{ __('translations.save_changes') }}" style="display: none;">
				</div>
			</div>
		</div>
	</form>	
@stop

@section('scripts')
	<script src="/js/select.js" defer></script>
	<script>
		document.addEventListener('DOMContentLoaded', function(){
			$('#branch_id').select2();
		}, !1)

		function changePriceServicesData(val) {
			if (val != 0) {
				$.ajax({
					url: '/ajax/getData',
					type: 'post',
		            dataType: 'json',
		            headers: {
		                'X-CSRF-TOKEN': '{{ csrf_token() }}'
		            },
				    data: {action:'getPriceServicesDataForCustom', price_id: '{{ $data->id }}', branch_id: val},
				    success: function (data) {
				    	if (data) {
				    		var html = '';

							for (i in data) {
								if (data[i].name != '') {
									html += '<div class="fx fac account-list-line account-list-headline">';
									html += '<div class="account-list-cell">'+data[i].name+'</div>';
									html += '</div>';
								}

								var services = data[i].services;

								for(j in services) {
									html += '<div class="fx fac account-list-line ap'+(data[i].name != '' ? 20 : 0)+'">';
									html += '<div class="account-list-cell">'+services[j].industry_service_name+'</div>';									
									html += '<div class="account-list-cell">';
									html += '<input type="text" name="services['+services[j].industry_service_id+'][price]" class="form-input" placeholder="{{ __('translations.price') }}" value="'+(services[j].changed_price ? services[j].changed_price : '')+'" onchange="isNaN(this.value) ? this.value = null : this.value = this.value;">';
									html += '</div>';
									html += '<div class="account-list-cell">';
									html += '<input type="hidden" name="services['+services[j].industry_service_id+'][currency_id]" value="'+services[j].currency_id+'"/>';
									html += '<input type="hidden" name="services['+services[j].industry_service_id+'][price_id]" value="'+services[j].price_id+'"/>';
									html += '<input type="hidden" name="services['+services[j].industry_service_id+'][branch_id]" value="'+services[j].branch_id+'"/>';
									html += '<input type="hidden" name="services['+services[j].industry_service_id+'][industry_service_id]" value="'+services[j].industry_service_id+'"/>';
									html += '<input type="hidden" name="services['+services[j].industry_service_id+'][price_service_id]" value="'+services[j].price_service_id+'"/>';
									html += 'USD';
									html += '</div>';
									

									if (services[j].price_for == 2) {
										html += '<div style="width:100%;font-size: 12px;font-weight: 400">*** {{ __('translations.price_for_all_service') }}</div>';
									}

									html += '</div>';
								}								
							}

							$('#brach_services_content').html(html);				    		
				    		$('#submit, #brach_services_table').css('display', 'block');
				    	}
					}
				});				
			} else {
				$('#brach_services_content').html('');
				$('#submit, #brach_services_table').css('display', 'none');
			}			
		}
		
	</script>
@stop