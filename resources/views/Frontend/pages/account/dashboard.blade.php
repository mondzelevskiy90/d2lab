@extends('Frontend/account_master')

@section('css')
	<link rel="stylesheet" href="{{ mix('/css/account.css') }}">
@stop

@section('breadcrumbs')
	@include('Frontend.templates.breadcrumbs')
@stop

@section('content')
	<h1>{{ $seo->name }}</h1>
	@if ($defaults && count($defaults))
		<div class="fx panel panel-transparent panel-mob-short dashbord-setup">
			<div id="dashboard_setup" style="display: none;">
				<form action="{{ route('dashboard_setup') }}" method="POST" class="fx">
					{{ csrf_field() }}
					@foreach ($defaults as $default)
						@if ($default->dashboard)
							<div class="form-line">
								<div class="form-checkbox">								
									<input type="checkbox" name="dashboards[{{ $default->dashboard_id }}]" id="dashboards{{ $default->dashboard_id }}" value="{{ $default->dashboard_id }}" @if(!count($customs) || in_array($default->dashboard_id, $customs)){{ 'checked="checked"' }}@endif>
									<label for="dashboards{{ $default->dashboard_id }}">{{ $default->dashboard->name }}</label>
								</div>
							</div>
						@endif
					@endforeach
					<div class="form-line" style="width: 100%;">
						<input type="submit" class="btn btn-small" value="{{ __('translations.save') }}">
					</div>
				</form>
			</div>
			<div class="dashboard-controls">
				@if (Auth::user()->type == config('types.clinic'))
					@if ($mylabs)
						<a href="{{ route('checkout') }}" class="btn btn-danger">{{ __('translations.order') }}</a>
					@else
					    <a href="catalog/dentistry/labs" class="btn btn-danger">{{ __('translations.order') }}</a>
					@endif
				@endif
				<div id="dashboard_setup_btn" class="bi btn btn-small btn-grey" title="{{ __('translations.setup') }}"></div>
			</div>
		</div>
	@endif
	<div class="fx fxb panel-body dashbord-content">
		@if ($data && count($data))
			@foreach ($data as $item)
				{!! $item !!}
			@endforeach
		@endif
	</div>
@stop

@section('scripts')
	<script src="/js/chart.js" defer></script>
	<script>
		document.addEventListener('DOMContentLoaded', function(){			
			detectActivity(300);

			$('#dashboard_setup_btn').on('click', function(){
				$('#dashboard_setup').slideToggle();
			});
		}, !1);
	</script>
@stop