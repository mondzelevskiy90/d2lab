@extends('Frontend/account_master')

@section('css')
	<link rel="stylesheet" href="/css/select.css">
	<link rel="stylesheet" href="{{ mix('/css/account.css') }}">
@stop

@section('breadcrumbs')
	@include('Frontend.templates.breadcrumbs')
@stop

@section('content')
	<h1>{{ $seo->name }}</h1>
	@if ($branches_list && count($branches_list))
	<div class="panel panel-transparent panel-mob-short">
		<div id="order_filters_block" class="panel-filters" style="display: none;">
			<form method="GET" action="{{ route('reviews') }}" id="filters_block" class="fx fac">		
				{{ csrf_field() }}	
				<div class="form-filter">
					<select name="branch" id="branch" class="form-input" onchange="document.getElementById('filters_block').submit();">	
						<option value="0"> -- {{ __('translations.branch') }} --</option>
						@foreach($branches_list as $branch)
							@if (Auth::user()->type == config('types.clinic'))
								@if($branch->to_branch->status != 4)
									<option value="{{ $branch->to_branch->id }}" @if(isset($filters['branch']) && $filters['branch'] == $branch->to_branch->id){{ 'selected="selected"' }}@endif>{{ $branch->to_branch->name }}</option>
								@endif
							@endif
							@if (Auth::user()->type == config('types.lab'))
								@if($branch->status != 4)
									<option value="{{ $branch->id }}" @if(isset($filters['branch']) && $filters['branch'] == $branch->id){{ 'selected="selected"' }}@endif>{{ $branch->name }}</option>
								@endif
							@endif
						@endforeach
					</select>		
				</div>
			</form>
		</div>	
		<div class="fx fxb panel-buttons">
			<div id="show_filter_block" class="btn">{{ __('translations.filter_btn') }}</div>
		</div>
	</div>
	@endif
	<div class="fx fxb panel-body">
		<div class="panel panel-table">
			@if ($data && count($data)) 
				<div class="account-list-table  account-list-table3 account-reviews-table">
					<div class="fx account-list-head">											
							<div class="account-list-cell">{{ __('translations.order') }} ID</div>
							<div class="account-list-cell">{{ __('translations.branch') }}</div>
							<div class="account-list-cell"></div>						
					</div>
					<div class="account-list-content">
						@foreach($data as $item)
							<div class="fx fac account-list-line">
								<div class="account-list-cell">
									<a target="_blank" href="{{ route('orders.edit', $item->order_id) }}"><span class="hide-desc">{{ __('translations.order') }}</span> #{{ $item->order_id }}</a>
								</div>								
								<div class="account-list-cell">
									{{ $item->branch->name }}
								</div>									
								<div class="account-list-cell">
									<a href="{{ route('reviews.edit', [$item->order_id, $requests]) }}" class="btn btn-small">{{ __('translations.view') }}</a>	
								</div>
							</div>
						@endforeach
					</div>					
				</div>
				<div class="pagination-block">{{ $data->appends(\Request::except('page'))->links() }}</div>
			@else 
				<p>
					{{ __('translations.no_reviews') }}
					@if (Auth::user()->type == config('types.clinic'))
					{{ __('translations.no_reviews_add') }}
					@endif
				</p>
			@endif
		</div>
	</div>
@stop

@section('scripts')
	<script src="/js/select.js" defer></script>
	<script>
		document.addEventListener('DOMContentLoaded', function() {
			$('#branch').select2({
				minimumResultsForSearch: -1
			});

			$('#show_filter_block').on('click', function(){
				$(this).toggleClass('show-filter-close');
				$('#order_filters_block').slideToggle();
			});
		}, !1);		
	</script>
@stop