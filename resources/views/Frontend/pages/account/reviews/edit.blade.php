@extends('Frontend/account_master')

@section('css')
	<link rel="stylesheet" href="{{ mix('/css/account.css') }}">
@stop

@section('content')	
	<div id="panel_top_buttons" class="fx fac fxb">
		@include('Frontend.templates.breadcrumbs')
		<div class="panel-top-buttons button-submit order-buttons">
			<a class="btn btn-small btn-grey" href="{{ route('reviews', [$requests]) }}">&larr; {{ __('translations.back') }}</a>				
		</div>
	</div>
	<h1 class="tc">{{ __('translations.order') }} #{{ $data->id }} {{ $seo->name }}</h1>
	{{ csrf_field() }}
	<div class="fx fxb panel-body">
		<div class="fx fac fxb panel">
			@include('Frontend.templates.review_block')
		</div>
	</div>
	
@stop

@section('scripts')
	@include('Frontend.templates.review_functions')
@stop