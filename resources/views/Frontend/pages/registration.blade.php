@extends('Frontend/master')

@section('css')
	<link rel="stylesheet" href="{{ mix('/css/login.css') }}">
	<link rel="stylesheet" href="/css/select.css">
@stop

@section('breadcrumbs')
	@include('Frontend.templates.breadcrumbs')
@stop

@section('content')
	<div class="registration-types">
		<a href="{{ url('registration') }}" @if(!$type)class="registration-type-active"@endif>{{ __('translations.guest') }}</a>
		<a href="{{ url('registration?type='.config('types.clinic')) }}" @if($type && $type == config('types.clinic'))class="registration-type-active"@endif>{{ __('translations.clinic') }}</a>
		<a href="{{ url('registration?type='.config('types.lab')) }}" @if($type && $type == config('types.lab'))class="registration-type-active"@endif>{{ __('translations.lab') }}</a>
	</div>
	@if (!$type)
		<div class="registration-socials">
			<p>{{ __('translations.reg_social') }}</p>
			<div class="fx registration-social-items">
				<div id="ggl_reg_btn" class="fx fxc registration-social-item registration-social-ggl">G</div>
				<div id="fcb_reg_btn" class="fx fxc registration-social-item registration-social-fcb" onclick="regFcb()">f</div>
				<div id="app_reg_btn" class="fx fxc registration-social-item registration-social-app" onclick="document.getElementById('appleid-signin').click()" style="background-color: black;">
					<svg xmlns="http://www.w3.org/2000/svg" style="overflow: visible;" width="100%" height="100%" y="25%" viewBox="0 -11 111.046875 14" fill="#fff">
				        <defs>
				          <style>				            
							  @font-face {
							    font-family: "applied-button-font-0";
							    src: url(data:application/x-font-woff;charset=utf-8;base64,d09GRgABAAAAABRMABEAAAAAIawAAQAAAAAAAAAAAAAAAAAAAAAAAAAAAABHUE9TAAATFAAAALsAAAHIbUB2PEdTVUIAABPQAAAAZQAAAIxKSyvpT1MvMgAACjgAAABNAAAAYHLeeipic2xuAAAUOAAAABMAAABI/ykCnmNtYXAAAAqIAAAArAAAATzUgYTCY3Z0IAAAEagAAACGAAAA/h4jsglmcGdtAAALNAAABcMAAAviP64gqWdhc3AAABMMAAAACAAAAAgAAAAQZ2x5ZgAAAYAAAAfMAAAMDN+ERypoZWFkAAAJlAAAADYAAAA2FZUeyWhoZWEAAAoYAAAAIAAAACQQagbMaG10eAAACcwAAABMAAAATFWqCFBsb2NhAAAJbAAAACgAAAAoG5oe821heHAAAAlMAAAAIAAAACABaQyMbmFtZQAAEjAAAADFAAABhhtRNi1wb3N0AAAS+AAAABMAAAAg/tsAmnByZXAAABD4AAAArgAAAMdYpaDHeJzVVm1QVOcVPu/HvcvytXvdvbvoooG9sGtKQGVZKCXoIhGVxUJipCy7iEQsEhH50GD4GBVWg6KTmcRak6owdWS0DqBNNJlMWhs/8kOnsTZNmTo1an+YjvVHbSft1LDQc++yBC12+rezn+85573vc855zjkvUCgCEFYKx0EHMVDkPZNeVuGRgQMQDq8ApVu8AqOENJESG8oppy0RpSZvJSU+TzxAtB4foJMkSYy1piWzZKYQFyH4y/3B0M3d96j+IdWHWn9L3r9BdozvEo4/quJHQ8foK0CgfeKWEBA+hVSoD5+fCpTh04UmEDgTeCMwRvxASJsXOAe/SAC2A+JRnrQjgJjY+imrDkB0JgLJSXNtssloiNFDKknVxZrTBLvDQd1Zs7KzXZnzqFXJoIpdFGWzxeLKzM7OccUzJhx6+LPKylMPD7VdH6iNvy2/+pPq7o/r6n7RXX2k3nzbUHvs18rGKyRhZIQkfLpx5Z4PGit6Vuz5+tTJr/cUtL20aaTrBQwTNE/c4pfRt0yoCvuWAowDZ9CkBZE0Ilbqxzijb4QIfhCE7QK6Zn/SjOKK8vURow4BPdNnKOYMxSjGzkkzueZRFbk7y+F0OhzurMX0Cces1nlUNouigp43f6kPHKypPPBi8M2PLn/o7fvktcaP9r0o3Iwp6365Yndxx4H+gx+W9/7r9MDfDpDgCxtycms9qzfa0zMu9q871pyf33jYv7TG7a4pWLUu2WnfdLRh39XNG3+JEH848Re+lJdDFCiw2JMHVBAFKjZhHARKhBodpoT70eN2L4gi82NeO1gJAXtSos0qG+Ki9QKHKBIVhQkiiFqXnEFNrniqQnZnqamyWGQ5WdZcpf7SjmKROlhh28fXrbsudR25295x993Df9g2NsFW7iyvCbL+oh1r1uwoCr1Rc2BV79jw8MT+4D9Oe2PX7S/B3FDYML6Or+WrwQAuEuM9k4jZiX6OEDGRCECLbdpCF174wurvANJMYBw9IqJfR0Sx3RuFTmkpafJqudQTSjtpiS2c7oypHTpdi1fbBv9914L/ZVfr47s8aVMbOMo4rX7qRuSNz+exSUYCCzKedSY/M9sqm40uyaXXgYEYojH0qWEeOSIVYcLwO11aZbjdSphBkXRYXW6XhKyitYHg8uXBQKC7sLC7uvfa9tg/xvds3vNj46Lh5r6rmxuv9Xl31z0jfdbxp6s/GNjcMFBRMdCA36FDJfuq8pa/3nDDt/aNvx8f/OfeBeV5w9+c7zpfq1bPIPKlVLgPFmg6xwgjpNh7xo7xsQJAszfSFtq9AqeUNk0F3fqtBg2pb0o9s6aVYkw8cSiwgEUxp2APS0jDiEiuScpJipQVjoY0+IU0/1TH1pP+0baWinaPcD90MlBXfX5XqIDu6w4u63opFERm9SD4t0U8DEzw6iRkAXOlUf519egWLyfhrhppuQwNsH2tf0w5k1xtuT7PLCwViIvhJsGExzDELGuY2fRawcQM/H5T6uLUyJsXfHOBjzs8DqfH4fA48c//GVqNE6QLOcFgWRhrrEYG+hhAVUZ8Edm0ZRhONK5VGGwSxuCocP+RRWXcEWRcLnYwGRpUxoHKOCs+EGEz8BOiuvUfbFOFM5BxZs23bJNBVkxTbJPDbJtOtiPXjfN/2nH+0ujWttJtHl4w5K/7/GJoCe3r6SnsXBPqmey49YjXADZIV/vT1FmYQPDz8BBUq90qG22SLVzl4uQEnB56Z6Za9HgwljzdOHCvs/Or/mNfdXbe699/+PB+/Mi9obMj47294yNnQ70Xrvz87JUrZ9+7jBiOjB/iP8I+aoYUeMcTLREuxBDCaXG4ac7FtgN+JlKAdq+OcK4h7PDi0Ns61fGSETmGSrVUB/dMhmjzNDXzqdu1wFotMoF5iXKKJcVoiI9Df83ErA2UST+xezkVDLIWcNMTrY7e6Lu6pfHa3q1n0qTR68ZF5zMD3UVF3X5/z4rlQb5679jQUGhvTXnooXD6/ar6sYBvsLX1hK9ysKXlRCWyJ2/iET0tlEECBDzRsYTROIJTuzjMk0SVXy1qXohPRCpqzNDGYRNH756ubuU466Mlc6oiSUZdrA2HPaJXJLPakXPkyYYsKW7Xn0cfPCgum223zslL2v524dCQUDb+4FzoN2tKGX1PEPO662ndOTzmLWRNJi/AjKksBzbFco2h2skQUMneBFMsj6hQqFE6oAZfLa5pmtbpmkmWm8EcYTlBjOH54cai0+C7JZ55XZp/ovPcxdEtr3X3qSU/VLXh81/RS6FtwZ1v7aRdiLcAQHQgXgMs/SBOj+AomQyqAW9+TD09gvZxSat6AVRRGMBgkkxhFERhino/NYVfomP8i9t3pTu3xn9385Lxzmd3pIsqCmYf+5IXjH3ClqgfHBaETfxVeCQaIBe+D1HnivPtQBcirfCWFc9l8zxmRXbp1NuKeucK31qcGTRyFXNlLqZOVapdz7JzFtMclY/8qFTkW+tkSubCpGfz89OodUFq8oqX3fNzXUvS05fmfC9lUZnXmV78XGFz30pnkSc3wZq3rCxmzopSJSaaUHdy4qpyJ1vAD+oSl+TS2RlF1c8X1RdnyIQV1OYs9H3XlGAyzTFlV+YUnLpQ3TK87XmzzSzPlXclZc6lzKjPb3bYs5JI3Kz81n8DmAOACAABAAAAEwBqAAcAAAAAAAIALgA+AHcAAACpC+IAAAAAAAAAAAAAAEEArwEmAX0CTgKqAwEDWAOHA98EIQSmBPoFUgWVBZUGBgABAAAAAQAAhOuEOl8PPPUAAQgAAAAAANaoccYAAAAA1w1hff4i/eoKqAikAAAAAwACAAAAAAAACAAAYwIwAAAFjwBFBc0AeQU1AHAEqABlBPoAZAThAKUCFgCCAhYAggIjAKUEywCbBM8AZQT7AJsDDAA6BM4AkQZoAEACAAAAB0ABAnicY2BkYGCf90+IgYHb9J/S30Ku5QxAERQgDACBlAVKeJxjYGZ5xPiFgZWBgdWY5QwDA8NMCM10hmEWUy+Qz8DGAAeMDEjAMSDAB0gp/PjPPu+fEAMD+zzGdwoMjPNBciyOrItBcgzMANLgDugAAAB4nHXPTQrCQAwF4OdPFbSIPztXXfQi0iN4hlKEUlxY6lUU1F7BGyh4EL1Fd883M3Fp4CMNGZIUwBDAQFKE71RVT/mlPPF1DBctQrgqMe7dxvQlM27e1riZuYmkkJ2MpDRuTyV7mcpBaplJY+ZaGQVYAB0DLLXhqLzWC3dxomsybc61pdTEGk0SdST9zb9eoV6lPdbjh08+eGfLG6+88MzTOx6v/B/+iS+yhS92eJytVmlz01YUlbzFSchSstCiLk+8OE3tJ5NSCAZMCJJlF9zF2VoJSivFTrov0DLDb9CvuTLtDP3GT+u5km0MSdoZppmM7nnvHb27X5k0JUjb91xfiPYzbXa7TYXdex5dNmjND45EtO9RphT+XdSKWrcrDwzTJM0nzZGNvqZrTmBbpCsSwZFFGSV6gp53KLd6r7+mTzlu16WC65mULfk79z1TmkbkCep0sLXlG4JqjGq+L+KUHfZoDVuDlaB1Pl9n5vOOJ2BNFAqa6ngBdgSfTTHaYLQRGIHv+wbpFd+XpHW8Q9+3KKsE7smVQliWdzoe5aVNBWnDD5/0wKKckrBL9OL8gS34hC02Ugv4SYXA7VK2bOLQEZGIoCBez5fg5LYXdIxwx/ekb/qCtnY9nBns2kC/RXlFE06lr2XSSBWwlLZExKUdUubgiPQurKB82aIJJdjUaaf7LKcdCL6BtgKfKUEjMbWo+hPTmuPaZXMU+0n1ci6m0lv0Ckxw4Hcg3EiGnJckXprBMSVhwMihlciODBupiulTXqcVvKUZL1wbf+mMShzqT09lkWxDmn7ZtGhGxZmMS72wYdGsAlEIOuPc5dcBpO3TDK92sJrByqI5XDOfhEQgAl3opVknEFEgaBZBs2hetfe8ONdr+Cs0cyifWPSGam977d100zCxv5Dsn1WxNufse/HcnEN6aNNchWsWlWzHZ/gxgwfpy8hEttTxYg4evLUj5JfVlk2J14bYSM/5FbQC7/jwpAX7W9h9OVWnJDDWtAWJaDmkbfZ1XU9ytaC0WMu4ex7NSVu4NI3im5IoOFsEUP/X/LyuzWq2HQXx2UKFHleMCwjTInxbqFi0pGKd5TLizPKcirMs31RxjuVbKs6zPK/iAktDxRMs31ZxkeU7Kp5k+YGSw7hDNSIsRZX0B9wgFpXHDpdHhw/Tw8rY4ero8FF6+K7SaKbyGv69B//ehV0C/rE04R/LC/CPpYR/LFfgH8sS/GO5Cv9Yvg//WK7BP5ZKiXpSppaC2vlAOMht4CSpROsprtWqIqtCFrrwIhqgJU7JogxrkifivzIM9n59lFp9mS6W47y+5HoYZOzgh+OROX58SYkrib0fgae7x5WgO09Uzvva8p8a/zU2ZS2+pC/Bo8vwHwafbC+aIqxZdEVVz9Ut2vgvKgq4C/pVpERbLomqaHHjI5R3oqglW5gUHr4QGKyYBhu6vrQI/TVMqGU0F/4TCk06lcOoKoWoR7jr2otjUU3voBzuBEtQwLNia9t7mhFZYTzNrGbP+zbPzyJGsUzYsonOdV5tw4BnWPq5yDhBT1LWCXs4zjihARzw/Hr1nRAmYarLJnIooaEJvyASLbjvBCUynZQ5DAfEPo+Cyh+7FTeyR6XECDw76YR8oQspv84xENjJrw5iIOsIzY1km4poHiGassXKOFv1JGTswCCi2p5XFXV8XdniwaZgW4YhL5SwujP+IU8TdVIFDzIjuYxvDixwhqkJ+Ev/qovDVG5iHlQ5ak0M9bpfjav6Ihrw1mi7M7699TL7RM5tRbXKiZfaiq5VIijmYoG1xzlIS5WqoDqjChtGl4tLotSraJL0ugaGBub/a5Ri6/+qPjaf50tdYoSM5dv0Bza6HIyh/03235SDAAz8GLncgstLaXPilwH6cKFKl9GLH5+yfwczV19coCvAdxVdhWhz1FzEVTTxGRzG6RPF5UhtwE9VH3MG4DMAncHnqq8nOx2AZGebOS7ADnMY7DKHwR5zGOwz5zbAF8xh8CVzGHjMYeAzxwG4xxwG95nD4CvmMHjAnCbA18xh8A1zGATMYRAyxwY4YA6DLnMY9JjD4FDR9VGYj3hBm0DfJugW0HdJPWGxhcX3im6M2D/wImH/mCBm/5Qgpv6sqD6i/sKLhPprgpj6W4KY+lDRzRH1ES8S6u8JYuofCWLqY/V0MpcZ/vCyK1Q8pOxK58nwm2L9Aw8nY10AeJxj8N7BcCIoYiMjY1/kBsadHAwcDMkFGxnYnbZXpYW5GTGwMWiBeA58uRyxbL5slhzq7NIsbBxQwXK2VJZQFmc2A1ZFJrAgr9N+6QbxBuEGvgYuBnYGoHZOoKiw034GByQIFmVOcdmowtgRGLHBoSMCzFMD8XZxNDAwsjh0JIeABSOBwIEvnyOezZ/NmkOTXZaFjUdrB+P/1g0svRuZGFw2s6awMbi4AADrlS9DAAB4nGNgIBncBcJjDMdYShgY2Of9E2KZ+P8xiAaL72fYz7qYgYHFkYHh3zSQKOux/3dZwv6//leKrob1FYvj/3cIVaz/gere/esBq9sNhFMZpjL9Y3z+X/CfAkiMWZDx+3+mf5zI8mwHWFcyXWZ8BBJju8y6k+kI42ls9iC7hk0c4R4AmMtWvAAAeJyNj08LAVEUxX/DIGVtPQtbGv+iZmWjLNQUzcKWCTWMDPKVfBMfxodwzDwiKb3ePeeed+7tPKDEjDyWXQYC3YxbVNVlPEeFyPA8PmfDbWpcDS/gcDO8iGNVuEhp4dKkS92wnrQBO52IUHzEljmNVI10HPasWbLiQJJ2oTAUnlQXck4YSvelxMKp1LO84/R1zZHND4fz4fHUu8rUUF0IQ2XzXnn7yuvSUW0L/9kXpBkTdbF+9L37sSPb8Jyvv8/fASPuNJwAAAB4nGNgZgCDfzcYZjFgAQA4VwJ0AAABAAH//wAPeJyNUDEOwjAMPCcF2lQChPoAHsDICxBiYmRkQYiJqkPFAH9jZGIF8RIWxGAuKVI7dGhOsZ3zObINAeCwwBZ2uVpvkO0vZY5pvjsVmCFiFqrwqjqWRmxgj4eyQNa0TEq4EZLg46AEvRHjq2Uic6QE9Ko34q5ntB59tfMtyo8+O2sfXZW+A/b3bbzf1fzdav++ns4E+L2kGIfNWAyrTfLfHvoE6AdETJ0LuRFZIeNrPZvQOsauqvoB5z0tQgB4nGNgZGBg4GKIYihhYHZx8wlhEEmuLMphkMtJLMljUGJgAcoy/P/PAAPMjlGuCgxizkEhCgxyIUHeCgxqYHlGqDpGEAtMMzEw5+Qn5zCIIJNARYxgzAKlOYCYDawLyAYAAaIWnAAAAHicY2BkgAKmef81GMgGAGutAckA) format("woff")
							  }
				          </style>
				        </defs>
				        <text x="50%" y="10%" dominant-baseline="middle" text-anchor="middle" font-size="48px" textLength="111.046875" font-family="applied-button-font-0" direction="ltr"></text>
				      </svg>
				</div>
			</div>	
			<div class="registration-separator"><span>or</span></div>
		</div>
	@endif
	<div class="registration-form">
		<form class="editing-form" method="POST" action="{{ route('registration') }}" enctype="multipart/form-data">
			{{ csrf_field() }}
			<div class="form-line">				
				<input type="text" name="name" id="name" class="form-input" placeholder="{{ __('translations.name') }}" value="{{ old('name') }}" required="required">
				@if ($errors->has('name'))
                    <div class="error">{{ $errors->first('name') }}</div>
                @endif				
			</div>
			<div class="form-line">				
				<input type="text" name="surname" id="surname" class="form-input" placeholder="{{ __('translations.surname') }}" value="{{ old('surname') }}" required="required">
				@if ($errors->has('surname'))
                    <div class="error">{{ $errors->first('surname') }}</div>
                @endif				
			</div>
			<div class="form-line">				
				<input type="email" name="email" id="email" class="form-input" placeholder="{{ __('translations.email') }}" value="{{ old('email') }}" required="required">
				@if ($errors->has('email'))
                    <div class="error">{{ $errors->first('email') }}</div>
                @endif				
			</div>			
			<div class="form-line">				
				<input type="password" name="password" id="password" class="form-input" placeholder="{{ __('translations.password') }}" value="" required="required">
				@if ($errors->has('password'))
                    <div class="error">{{ $errors->first('password') }}</div>
                @endif				
			</div>
			<div class="form-line">				
				<input type="password" name="password_confirmation" id="password_confirmation" class="form-input" placeholder="{{ __('translations.password_confirmation') }}" value="" required="required">
				@if ($errors->has('password_confirmation'))
                    <div class="error">{{ $errors->first('password_confirmation') }}</div>
                @endif				
			</div>
			@if ($type)
				<div class="form-line">	
					@php
						if ($type == config('types.clinic')) $type_text = __('translations.clinic');
						if ($type == config('types.lab')) $type_text = __('translations.lab');
					@endphp			
					<input type="text" class="form-input" placeholder="Type: {{ $type_text }}" disabled="disabled">
					<input type="hidden" name="type" id="type" value="{{ $type }}">				
				</div>
				<div class="form-line">				
					<input type="text" name="company" id="company" class="form-input" placeholder="{{ __('translations.company') }}" value="{{ old('company') }}" required="required">
					@if ($errors->has('company'))
	                    <div class="error">{{ $errors->first('company') }}</div>
	                @endif				
				</div>
				<div class="form-line">				
					<input type="text" name="zip_code" id="zip_code" class="form-input" placeholder="{{ __('translations.zip_code') }}" value="{{ old('zip_code') }}" required="required">
					@if ($errors->has('zip_code'))
	                    <div class="error">{{ $errors->first('zip_code') }}</div>
	                @endif				
				</div>
				<div class="form-line">
					<select id="country" name="country" class="form-controll" required="required">
						<option value="0">{{ __('translations.country') }}</option>
					</select>
					@if ($errors->has('country'))
	                    <div class="error">{{ $errors->first('country') }}</div>
	                @endif
				</div>
				<div class="form-line">
					<select id="region" name="region" class="form-controll" required="required">
						<option value="0">{{ __('translations.region') }}</option>
					</select>
					@if ($errors->has('region'))
	                    <div class="error">{{ $errors->first('region') }}</div>
	                @endif
				</div>
				<div class="form-line">
					<select id="locality" name="locality" class="form-controll" required="required">
						<option value="0">{{ __('translations.city') }}</option>
					</select>
					@if ($errors->has('locality'))
	                    <div class="error">{{ $errors->first('locality') }}</div>
	                @endif
				</div>
				<div class="form-line">				
					<input type="text" name="address" id="address" class="form-input" placeholder="{{ __('translations.address') }}" value="{{ old('address') }}" required="required">
					@if ($errors->has('address'))
	                    <div class="error">{{ $errors->first('address') }}</div>
	                @endif				
				</div>
				<div class="form-line">				
					<input type="text" name="phone" id="phone" class="form-input" placeholder="{{ __('translations.phone') }}" value="{{ old('phone') }}" required="required">
					@if ($errors->has('phone'))
	                    <div class="error">{{ $errors->first('phone') }}</div>
	                @endif				
				</div>
				<div class="form-line">
					<div id="file_psev_input" class="form-input" onclick="$('#logo').click();">
						<span>{{ __('translations.logo') }}</span>
					</div>	

						<input type="hidden" name="data_logo[status]" id="file_psev_input_status" value="0">
						<input type="hidden" name="data_logo[width]" id="file_psev_input_width" value="">
						<input type="hidden" name="data_logo[height]" id="file_psev_input_height" value="">
						<input type="hidden" name="data_logo[x]" id="file_psev_input_x" value="">
						<input type="hidden" name="data_logo[y]" id="file_psev_input_y" value="">
										
						<input type="file" class="uploaded-image" data-prefix="data_logo" data-input="file_psev_input" data-text="{{ __('translations.select_new_logo') }}" name="logo" id="logo" value="" accept="image/jpeg,image/jpg,image/png" style="height: 1px; opacity: 0;padding: 0;">
					@if ($errors->has('logo'))
	                    <div class="error">{{ $errors->first('logo') }}</div>
	                @endif				
				</div>
			@endif
			<input type="hidden" name="social" id="social" value="{{ config('statuses.email') }}">
			<input type="hidden" name="social_img" id="social_img" value="">
			<p class="registration-terms">{!! __('translations.reg_terms') !!}</p>
			<div class="form-line">
				@php 
					$btn_text = __('translations.reg_button');

					if ($type && $type == config('types.clinic')) $btn_text = __('translations.reg_button2');
					if ($type && $type == config('types.lab')) $btn_text = __('translations.reg_button3');
				@endphp
				<input type="submit" id="submit" class="btn" name="submit" value="{{ $btn_text }}">
			</div>
		</form>
		<p class="registration-login">{!! __('translations.reg_login') !!}</p>
	</div>
@stop

@section('scripts')
	@include('Frontend.templates.fields_script')
	@include('Frontend.templates.img_upload_script')
	@include('Frontend.templates.apple_sign_in', ['redirect_url' => 'registration'])
	<script src="https://apis.google.com/js/api:client.js" defer onload="startGgl()"></script>	
	<script>
	  window.fbAsyncInit = function() {
	    FB.init({
	      appId      : '2706069246138083',
	      cookie     : true,
	      xfbml      : true,
	      version    : 'v6.0'
	    });	
      
	    FB.AppEvents.logPageView();
	  };

	  (function(d, s, id){
	     var js, fjs = d.getElementsByTagName(s)[0];
	     if (d.getElementById(id)) {return;}
	     js = d.createElement(s); js.id = id;
	     js.src = "https://connect.facebook.net/en_US/sdk.js";
	     fjs.parentNode.insertBefore(js, fjs);
	   }(document, 'script', 'facebook-jssdk'));		
	</script>
	<script>
		var googleUser = {};

		function startGgl() {
			gapi.load('auth2', function(){		      
				auth2 = gapi.auth2.init({
					client_id: '840235196337-2tscmi40hv0sg5a2jeggr5gf9ltv3vie.apps.googleusercontent.com',
					cookiepolicy: 'single_host_origin',		        
				});

				regGgl(document.getElementById('ggl_reg_btn'));
			});
		}

		function regGgl(element) {
			if (element) {
				auth2.attachClickHandler(element, {},
					function(googleUser) {	
					  	document.getElementById('name').value = googleUser.getBasicProfile().getGivenName();
					  	document.getElementById('surname').value = googleUser.getBasicProfile().getFamilyName();
					  	document.getElementById('email').value = googleUser.getBasicProfile().getEmail();
					  	document.getElementById('password').value = googleUser.getBasicProfile().getId();
					  	document.getElementById('password_confirmation').value = googleUser.getBasicProfile().getId();
					  	document.getElementById('social').value = {{ config('statuses.new') }};
					  	document.getElementById('social_img').value = googleUser.getBasicProfile().getImageUrl();

					  	document.getElementById('submit').click();
					}, 
					function(error) {
					  showPopup(('{{ __('translations.login_social_error') }}'));
					}
				); 	
			}					
		}

		function regFcb() {
			FB.login(function(response) {
			  if (response.status === 'connected') {
			    FB.api('/me', {fields: 'id, name, email'}, function(response) {
			    	console.log(response);
				    document.getElementById('name').value = response.name;
				  	document.getElementById('surname').value = response.name;
				  	document.getElementById('email').value = response.email;
				  	document.getElementById('password').value = response.id;
				  	document.getElementById('password_confirmation').value = response.id;
				  	document.getElementById('social').value = {{ config('statuses.new') }};
					document.getElementById('social_img').value ='http://graph.facebook.com/'+response.id+'/picture?type=square';
				  	
				  	document.getElementById('submit').click();
			    });
			  } else {
			    showPopup(('{{ __('translations.login_social_error') }}'));
			  }
			}, {scope: 'public_profile,email'});
		}

		document.addEventListener('DOMContentLoaded', function(){
			@if ($errors->has('email')) 
				var email_unique = true;
			@else 
				var email_unique = false;
			@endif

			setTimeout(function(){
				if(window.location.href.indexOf('?social=facebook') != -1 && !email_unique) {			
				    regFcb();
				}

				if(window.location.href.indexOf('?social=google') != -1 && !email_unique) {
				    document.getElementById('ggl_reg_btn').click();
				}
			}, 1000);
		},!1);			
	</script>		
@stop