@extends('Frontend/master')

@section('breadcrumbs')
	@include('Frontend.templates.breadcrumbs')
@stop
@section('content')
	<h1>{{ $seo->name }}</h1>
	<div class="page-text">
		@if ($_SERVER['REQUEST_URI'] == '/article/privacy-policy')
			<a href="/article/privacy-policy#app-policy">Dentist2Lab App Privacy Policy &rarr;</a>
			<br><br>
		@endif
		{!! $seo->description !!}
	</div>
@stop

@section('scripts')

@stop