@extends('Frontend/master')

@section('css')
	<link rel="stylesheet" href="{{ mix('/css/login.css') }}">
@stop

@section('breadcrumbs')
	@include('Frontend.templates.breadcrumbs')
@stop

@section('content')
	
	<div class="registration-form">	
		<p class="registration-terms"></p>
		<p class="registration-terms"></p>
		<p class="registration-terms"></p>
		<p class="registration-terms">{!! __('translations.forgot_text') !!}</p>	
		<p class="registration-terms"></p>		
		<form method="POST" action="{{ route('forgot') }}">
			{{ csrf_field() }}			
			<div class="form-line">				
				<input type="email" name="email" id="email" class="form-input" placeholder="{{ __('translations.email') }}" value="{{ old('email') }}" required="required">
				@if ($errors->has('email'))
                    <div class="error">{{ $errors->first('email') }}</div>
                @endif				
			</div>
			<div class="form-line">				
				<input type="submit" id="submit" class="btn" name="submit" value="{{ __('translations.forgot_button') }}">
			</div>
		</form>		
	</div>
@stop

@section('scripts')
	
@stop