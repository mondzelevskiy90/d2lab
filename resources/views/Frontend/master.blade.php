<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
	@include('Frontend.parts.head')
	<body>
		<div class="wrapper">
			<header>
				@include('Frontend.parts.header')
			</header>			
			@yield('after_head')
			@yield('breadcrumbs')
			<section class="content page-content">
				@yield('content')
			</section>
			@yield('pre_footer')
			@include('Frontend.parts.footer')
		</div>		
	</body>
</html>