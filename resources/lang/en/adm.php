<?php
return [
	'name' => 'Name',
	'status' => 'Status',
	'select_status' => 'Select status',
	'sort_order' => 'Sort order',
	'image' => 'image',
	'status_on' => 'On',
	'status_off' => 'Off',
	'meta_title' => 'Meta title',
	'meta_description' => 'Meta description',
	'description' => 'Description',
	'email' => 'Email',
	'address' => 'Address',
	'Phone' => 'phone',
	'role' => 'Role',
	'company_name' => 'Company name',
];