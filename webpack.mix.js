const mix = require('laravel-mix');

mix.webpackConfig({
   node: {
      fs: "empty"
   }
});

mix.sass('resources/sass/styles.scss', 'public/css')
   .sass('resources/sass/home.scss', 'public/css')
   .sass('resources/sass/catalog.scss', 'public/css')
   .sass('resources/sass/branch.scss', 'public/css')
   .sass('resources/sass/login.scss', 'public/css')
   .sass('resources/sass/account.scss', 'public/css')
   .sass('resources/sass/checkout.scss', 'public/css')
   .sass('resources/sass/print_order.scss', 'public/css')
   .sass('resources/sass/print_invoice.scss', 'public/css')
   .sass('resources/sass/reviews.scss', 'public/css')
   .sass('resources/sass/chat.scss', 'public/css')
    .version();