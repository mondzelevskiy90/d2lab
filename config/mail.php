<?php

return [
    'driver' => 'smtp',
    'host' => 'smtp.gmail.com',
    'port' => 465,
    'from' => [
        'address' => 'dentist2lab@gmail.com',
        'name' => 'Dendist2Lab',
    ],
    'encryption' => 'ssl',
    'username' => 'dentist2lab@gmail.com',
    'password' => 'brostel!',
    'sendmail' => '/usr/sbin/sendmail -bs',
    'markdown' => [
        'theme' => 'default',

        'paths' => [
            resource_path('views/vendor/mail'),
        ],
    ],
    'log_channel' => env('MAIL_LOG_CHANNEL'),
];
