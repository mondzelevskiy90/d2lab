<?php
return [
	6 => [
		'settings',
		'tickets'
	],

	7 => [
		'orders',
		'invoices',
		'reports',
		'branches',
		'staff',
		'mylabs',
		'reviews',
		'settings',
		'tickets',
		'notifications'
	],

	8 => [
		'orders',
		'invoices',	
		'mylabs',
		'reviews',
		'settings',
		'tickets',
		'notifications'
	],

	9 => [
		'orders',
		'invoices',
		'reports',
		'branches',
		'prices',
		'portfolios',
		'staff',
		'reviews',
		'settings',
		'tickets',
		'notifications'
	],

	10 => [
		'orders',
		'settings',
		'tickets',
		'notifications'
	],

	13 => [
		'orders',
		'invoices',
		'reports',
		'mylabs',
		'reviews',
		'settings',
		'tickets',
		'notifications'
	],

	14 => [
		'orders',
		'invoices',
		'reports',		
		'prices',
		'portfolios',
		'reviews',
		'settings',
		'tickets',
		'notifications'
	]
];