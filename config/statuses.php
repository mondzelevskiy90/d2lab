<?php 
return [
	'email' => 1,
	'new' => 2,
	'approved' => 3,
	'blocked' => 4,
	'removed' => 5,
];