<?php 

return [
	'guest' => 6,
	'clinic_head' => 7,
	'doctor' => 8,
	'lab_head' => 9,
	'tech' => 10,
	'clinic_staff' => 13,
	'lab_staff' => 14
];