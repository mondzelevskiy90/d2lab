function tinymce_init_callback(editor)
{
      editor.remove();
      editor = null;
 
      tinymce.init({
        selector: 'textarea.richTextBox',       
        min_height: 200,
        height: 300, 
        resize: 'vertical',
        plugins: 'print preview searchreplace autolink directionality visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor insertdatetime advlist lists textcolor wordcount imagetools contextmenu colorpicker textpattern code',
        extended_valid_elements: 'input[id|name|value|type|class|style|required|placeholder|autocomplete|onclick]',
        file_browser_callback: function (field_name, url, type, win) {
            if (type == 'image') {
                $('#upload_file').trigger('click');
            }
        },
        toolbar: 'styleselect bold italic underline | forecolor backcolor | alignleft aligncenter alignright | bullist numlist outdent indent | link anchor image table youtube giphy | code',
        convert_urls: false,
        image_caption: true,
        image_title: true,
        valid_elements:'h1[*],h2[*],h3[*],h4[*],h5[*],h6[*],div[*],p[*],span[*],ul[*],li[*],ol[*],hr,br,img[*],-b,-i,-u', 
        valid_children:'+li[span|p|div]'
    });
}