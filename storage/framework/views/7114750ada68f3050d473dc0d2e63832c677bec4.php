<?php $__env->startSection('page_title', __('voyager::generic.viewing').' '.ucfirst($type)); ?>

<?php $__env->startSection('page_header'); ?>
    <h1 class="page-title">
        <i class="voyager-list-add"></i> <?php echo e(ucfirst($type)); ?>        
    </h1>
<?php $__env->stopSection(); ?>
 
<?php $__env->startSection('content'); ?> 
    <div class="page-content container-fluid">
        <?php echo $__env->make('voyager::alerts', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-body">
                        <form method="get" action="<?php echo e(route('voyager.'.$type.'.index', $requests)); ?>">
                            <div class="col-sm-10">
                                <div class="row">                      
                                    <div class="col-sm-12">  
                                        <div class="col-md-3">
                                            <select onchange="$(this).parents('form').submit()" class="form-control" name="has_answer">
                                                <option value="0" default>-- Answer status --</option> 
                                                <option value="2" <?php if(isset($filter['filter_answer']) && $filter['filter_answer'] == 2): ?> selected <?php endif; ?>>Has no answer</option>
                                            </select>
                                        </div>  
                                        <div class="col-md-3">
                                            <select onchange="$(this).parents('form').submit()" class="form-control" id="user_id" name="user_id"> 
                                                <option value="0">-- Select user --</option>  
                                                <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <option value="<?php echo e($user->user_id); ?>" <?php if(isset($filter['filter_user']) && $filter['filter_user'] && $filter['filter_user'] == $user->user_id): ?><?php echo e('selected'); ?><?php endif; ?>><?php echo e($user->user->name.' '.$user->user->surname); ?></option>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> 
                                            </select>
                                        </div>                                    
                                        <div class="col-md-3">
                                            <input type="text" class="form-control" name="id" value="<?php if(isset($filter['filter_id']) && $filter['filter_id'] != ''): ?><?php echo e($filter['filter_id']); ?><?php endif; ?>" placeholder="ID...">
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <input type="submit" style="margin:0;" class="btn btn-success" value="OK">
                                <a class="btn btn-danger" style="margin:0;" href="<?php echo e(route('voyager.'.$type.'.index')); ?>">Clear</a>
                            </div>
                        </form>
                        <table id="dataTable" class="table table-hover">
                            <thead>
                            <tr>                                
                                <th>ID</th>     
                                <th>User</th>
                                <th>Text</th>                               
                                <th>Status</th>                          
                                <th class="actions text-right"><?php echo e(__('voyager::generic.actions')); ?></th>
                            </tr>
                            </thead>
                            <tbody>
                                <?php $__currentLoopData = $many; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $one): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr style="<?php if(!$one->answer): ?><?php echo e('color:#000;background-color:rgba(255, 0, 0, 0.35);'); ?><?php endif; ?>">
                                    <td>#<?php echo e($one->id); ?></td>
                                    <td>
                                        <?php if($one->user): ?>
                                            <?php echo e($one->user->name.' '.$one->user->surname); ?>

                                        <?php endif; ?>
                                    </td>
                                    <td>
                                        <?php echo e($one->question); ?>

                                    </td>
                                    <td>
                                    	<?php
                                    		if ($one->answer) {
                                                echo 'Has answer';
                                            } else {
                                                echo 'Has no answer!';
                                            }
                                    	?>
                                    </td>
                                    <td class="no-sort no-click bread-actions">
                                        <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('edit', $model)): ?>
                                            <a href="<?php echo e(route('voyager.'.$type.'.edit', [$one->id, $requests])); ?>" class="btn btn-sm btn-primary pull-right edit">
                                                <i class="voyager-edit"></i> <?php echo e(__('voyager::generic.edit')); ?>

                                            </a>
                                        <?php endif; ?>                                        
                                    </td>
                                </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="panel-body">	                    
                        <div class="pull-right"><?php echo e($many->appends(\Request::except('page'))->links()); ?></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?> 
    <script src="/js/select.js"></script>  
    <script> 
        $('#user_id').select2({
            minimumInputLength: -1,            
        });
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('voyager::master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/dentist2lab.com/resources/views/Backend/tickets/browse.blade.php ENDPATH**/ ?>