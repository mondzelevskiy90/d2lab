<?php $__env->startSection('css'); ?>
	<link rel="stylesheet" href="<?php echo e(mix('/css/login.css')); ?>">
	<link rel="stylesheet" href="/css/select.css">
	<link rel="stylesheet" href="/css/intlTelInput.min.css">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('breadcrumbs'); ?>
	<?php echo $__env->make('Frontend.templates.breadcrumbs', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
	<div class="registration-types">
		<a href="<?php echo e(url('registration')); ?>" <?php if(!$type): ?>class="registration-type-active"<?php endif; ?>><?php echo e(__('translations.guest')); ?></a>
		<a href="<?php echo e(url('registration?type='.config('types.clinic'))); ?>" <?php if($type && $type == config('types.clinic')): ?>class="registration-type-active"<?php endif; ?>><?php echo e(__('translations.clinic')); ?></a>
		<a href="<?php echo e(url('registration?type='.config('types.lab'))); ?>" <?php if($type && $type == config('types.lab')): ?>class="registration-type-active"<?php endif; ?>><?php echo e(__('translations.lab')); ?></a>
	</div>
	<?php if(!$type): ?>
		<div class="registration-socials">
			<p><?php echo e(__('translations.reg_social')); ?></p>
			<div class="fx registration-social-items">
				<div id="ggl_reg_btn" class="fx fxc registration-social-item registration-social-ggl">G</div>
				<div id="fcb_reg_btn" class="fx fxc registration-social-item registration-social-fcb" onclick="regFcb()">f</div>
			</div>	
			<div class="registration-separator"><span>or</span></div>
		</div>
	<?php endif; ?>
	<div class="registration-form">
		<form method="POST" action="<?php echo e(route('registration')); ?>" enctype="multipart/form-data">
			<?php echo e(csrf_field()); ?>

			<div class="form-line">				
				<input type="text" name="name" id="name" class="form-input" placeholder="<?php echo e(__('translations.name')); ?>" value="<?php echo e(old('name')); ?>" required="required">
				<?php if($errors->has('name')): ?>
                    <div class="error"><?php echo e($errors->first('name')); ?></div>
                <?php endif; ?>				
			</div>
			<div class="form-line">				
				<input type="text" name="surname" id="surname" class="form-input" placeholder="<?php echo e(__('translations.surname')); ?>" value="<?php echo e(old('surname')); ?>" required="required">
				<?php if($errors->has('surname')): ?>
                    <div class="error"><?php echo e($errors->first('surname')); ?></div>
                <?php endif; ?>				
			</div>
			<div class="form-line">				
				<input type="email" name="email" id="email" class="form-input" placeholder="<?php echo e(__('translations.email')); ?>" value="<?php echo e(old('email')); ?>" required="required">
				<?php if($errors->has('email')): ?>
                    <div class="error"><?php echo e($errors->first('email')); ?></div>
                <?php endif; ?>				
			</div>
			<div class="form-line">				
				<input type="password" name="password" id="password" class="form-input" placeholder="<?php echo e(__('translations.password')); ?>" value="" required="required">
				<?php if($errors->has('password')): ?>
                    <div class="error"><?php echo e($errors->first('password')); ?></div>
                <?php endif; ?>				
			</div>
			<?php if($type): ?>
				<div class="form-line">	
					<?php
						if ($type == config('types.clinic')) $type_text = __('translations.clinic');
						if ($type == config('types.lab')) $type_text = __('translations.lab');
					?>			
					<input type="text" class="form-input" placeholder="Type: <?php echo e($type_text); ?>" disabled="disabled">
					<input type="hidden" name="type" id="type" value="<?php echo e($type); ?>">				
				</div>
				<div class="form-line">				
					<input type="text" name="company" id="company" class="form-input" placeholder="<?php echo e(__('translations.company')); ?>" value="<?php echo e(old('company')); ?>" required="required">
					<?php if($errors->has('company')): ?>
	                    <div class="error"><?php echo e($errors->first('company')); ?></div>
	                <?php endif; ?>				
				</div>
				<div class="form-line">				
					<input type="text" name="zip_code" id="zip_code" class="form-input" placeholder="<?php echo e(__('translations.zip_code')); ?>" value="<?php echo e(old('zip_code')); ?>" required="required">
					<?php if($errors->has('zip_code')): ?>
	                    <div class="error"><?php echo e($errors->first('zip_code')); ?></div>
	                <?php endif; ?>				
				</div>
				<div class="form-line">
					<select id="country" name="country" class="form-controll" required="required">
						<option value="0"><?php echo e(__('translations.country')); ?></option>
					</select>
					<?php if($errors->has('country')): ?>
	                    <div class="error"><?php echo e($errors->first('country')); ?></div>
	                <?php endif; ?>
				</div>
				<div class="form-line">
					<select id="region" name="region" class="form-controll" required="required">
						<option value="0"><?php echo e(__('translations.region')); ?></option>
					</select>
					<?php if($errors->has('region')): ?>
	                    <div class="error"><?php echo e($errors->first('region')); ?></div>
	                <?php endif; ?>
				</div>
				<div class="form-line">
					<select id="locality" name="locality" class="form-controll" required="required">
						<option value="0"><?php echo e(__('translations.locality')); ?></option>
					</select>
					<?php if($errors->has('locality')): ?>
	                    <div class="error"><?php echo e($errors->first('locality')); ?></div>
	                <?php endif; ?>
				</div>
				<div class="form-line">				
					<input type="text" name="address" id="address" class="form-input" placeholder="<?php echo e(__('translations.address')); ?>" value="<?php echo e(old('address')); ?>" required="required">
					<?php if($errors->has('address')): ?>
	                    <div class="error"><?php echo e($errors->first('address')); ?></div>
	                <?php endif; ?>				
				</div>
				<div class="form-line">				
					<input type="text" name="phone" id="phone" class="form-input" placeholder="<?php echo e(__('translations.phone')); ?>" value="<?php echo e(old('phone')); ?>" required="required">
					<?php if($errors->has('phone')): ?>
	                    <div class="error"><?php echo e($errors->first('phone')); ?></div>
	                <?php endif; ?>				
				</div>
				<div class="form-line">
					<div id="file_psev_input" class="form-input" onclick="$('#logo').click();">
						<span><?php echo e(__('translations.logo')); ?></span>
					</div>	

						<input type="hidden" name="data_logo[status]" id="file_psev_input_status" value="0">
						<input type="hidden" name="data_logo[width]" id="file_psev_input_width" value="">
						<input type="hidden" name="data_logo[height]" id="file_psev_input_height" value="">
						<input type="hidden" name="data_logo[x]" id="file_psev_input_x" value="">
						<input type="hidden" name="data_logo[y]" id="file_psev_input_y" value="">
										
						<input type="file" class="uploaded-image" data-input="file_psev_input" data-text="<?php echo e(__('translations.select_new_logo')); ?>" name="logo" id="logo" value="" accept="image/jpeg,image/jpg,image/png" style="height: 1px; opacity: 0;padding: 0;">
					<?php if($errors->has('logo')): ?>
	                    <div class="error"><?php echo e($errors->first('logo')); ?></div>
	                <?php endif; ?>				
				</div>
			<?php endif; ?>
			<input type="hidden" name="social" id="social" value="<?php echo e(config('statuses.email')); ?>">
			<input type="hidden" name="social_img" id="social_img" value="">
			<p class="registration-terms"><?php echo __('translations.reg_terms'); ?></p>
			<div class="form-line">
				<?php 
					$btn_text = __('translations.reg_button');

					if ($type && $type == config('types.clinic')) $btn_text = __('translations.reg_button2');
					if ($type && $type == config('types.lab')) $btn_text = __('translations.reg_button3');
				?>
				<input type="submit" id="submit" class="btn" name="submit" value="<?php echo e($btn_text); ?>">
			</div>
		</form>
		<p class="registration-login"><?php echo __('translations.reg_login'); ?></p>
	</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
	<?php echo $__env->make('Frontend.templates.fields_script', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
	<?php echo $__env->make('Frontend.templates.img_upload_script', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
	<script src="https://apis.google.com/js/api:client.js" defer onload="startGgl()"></script>
	<script>
	  window.fbAsyncInit = function() {
	    FB.init({
	      appId      : '2706069246138083',
	      cookie     : true,
	      xfbml      : true,
	      version    : 'v6.0'
	    });	
      
	    FB.AppEvents.logPageView();
	  };

	  (function(d, s, id){
	     var js, fjs = d.getElementsByTagName(s)[0];
	     if (d.getElementById(id)) {return;}
	     js = d.createElement(s); js.id = id;
	     js.src = "https://connect.facebook.net/en_US/sdk.js";
	     fjs.parentNode.insertBefore(js, fjs);
	   }(document, 'script', 'facebook-jssdk'));		
	</script>
	<script>
		var googleUser = {};

		function startGgl() {
			gapi.load('auth2', function(){		      
				auth2 = gapi.auth2.init({
					client_id: '840235196337-2tscmi40hv0sg5a2jeggr5gf9ltv3vie.apps.googleusercontent.com',
					cookiepolicy: 'single_host_origin',		        
				});

				regGgl(document.getElementById('ggl_reg_btn'));
			});
		}

		function regGgl(element) {
			if (element) {
				auth2.attachClickHandler(element, {},
					function(googleUser) {	
					  	document.getElementById('name').value = googleUser.getBasicProfile().getGivenName();
					  	document.getElementById('surname').value = googleUser.getBasicProfile().getFamilyName();
					  	document.getElementById('email').value = googleUser.getBasicProfile().getEmail();
					  	document.getElementById('password').value = googleUser.getBasicProfile().getId();
					  	document.getElementById('social').value = <?php echo e(config('statuses.new')); ?>;
					  	document.getElementById('social_img').value = googleUser.getBasicProfile().getImageUrl();

					  	document.getElementById('submit').click();
					}, 
					function(error) {
					  showPopup(('<?php echo e(__('translations.login_social_error')); ?>'));
					}
				); 	
			}					
		}

		function regFcb() {
			FB.login(function(response) {
			  if (response.status === 'connected') {
			    FB.api('/me', {fields: 'id, name, email'}, function(response) {
			    	console.log(response);
				    document.getElementById('name').value = response.name;
				  	document.getElementById('surname').value = response.name;
				  	document.getElementById('email').value = response.email;
				  	document.getElementById('password').value = response.id;
				  	document.getElementById('social').value = <?php echo e(config('statuses.new')); ?>;
					document.getElementById('social_img').value ='http://graph.facebook.com/'+response.id+'/picture?type=square';
				  	
				  	document.getElementById('submit').click();
			    });
			  } else {
			    showPopup(('<?php echo e(__('translations.login_social_error')); ?>'));
			  }
			}, {scope: 'public_profile,email'});
		}

		document.addEventListener('DOMContentLoaded', function(){
			<?php if($errors->has('email')): ?> 
				var email_unique = true;
			<?php else: ?> 
				var email_unique = false;
			<?php endif; ?>

			setTimeout(function(){
				if(window.location.href.indexOf('?social=facebook') != -1 && !email_unique) {			
				    regFcb();
				}

				if(window.location.href.indexOf('?social=google') != -1 && !email_unique) {
				    document.getElementById('ggl_reg_btn').click();
				}
			}, 1000);
		},!1);		
	</script>		
<?php $__env->stopSection(); ?>
<?php echo $__env->make('Frontend/master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/d2l.sitepark.ua/resources/views/Frontend/pages/registration.blade.php ENDPATH**/ ?>