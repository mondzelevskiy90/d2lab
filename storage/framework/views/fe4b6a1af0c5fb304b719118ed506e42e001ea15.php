<?php $__env->startSection('css'); ?>
	<link rel="stylesheet" href="<?php echo e(mix('/css/account.css')); ?>">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('breadcrumbs'); ?>
	<?php echo $__env->make('Frontend.templates.breadcrumbs', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
	<h1><?php echo e($seo->name); ?></h1>
	<?php if($branches && count($branches) && count($branches) != count($data)): ?>
		<div class="panel panel-transparent">
			<div class="panel-buttons">
				<a href="<?php echo e(route('prices.create')); ?>" class="btn btn-primary"><?php echo e(__('translations.create')); ?></a>
			</div>
			<div class="panel-filters"></div>
		</div>
	<?php endif; ?>
	<div class="fx fxb panel-body settings-form">
		<div class="panel panel-table">
			<?php if($data && count($data)): ?> 
				<div class="account-list-table account-list-table3">
					<div class="fx account-list-head">						
							<div class="account-list-cell"><?php echo e(__('translations.name')); ?></div>
							<div class="account-list-cell"><?php echo e(__('translations.branch')); ?></div>
							<div class="account-list-cell"></div>						
					</div>
					<div class="account-list-content">
						<?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<?php if($item->branch): ?>
							<div class="fx fac account-list-line">
								<div class="account-list-cell"><?php echo e($item->name); ?></div>
								<div class="account-list-cell"><?php echo e($item->branch->name); ?></div>
								<div class="account-list-cell">
									<a href="<?php echo e(route('prices.edit', $item->id)); ?>" class="btn btn-small"><?php echo e(__('translations.edit')); ?></a>
									<!--<div data-href="<?php echo e(route('prices.delete', $item->id)); ?>" class="btn btn-danger btn-small delete-element" data-msg="<?php echo e(__('translations.delete_alert')); ?>"><?php echo e(__('translations.delete')); ?></div> -->
								</div>
							</div>
							<?php endif; ?>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					</div>
				</div>				
			<?php else: ?> 
				<?php if($branches && count($branches)): ?> 
					<p><?php echo sprintf(__('translations.no_prices'), route('prices.create')); ?></p>
				<?php else: ?>
					<p><?php echo sprintf(__('translations.no_braches'), route('branches.create')); ?></p>
				<?php endif; ?>
			<?php endif; ?>
		</div>
	</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('Frontend/account_master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/d2l.sitepark.ua/resources/views/Frontend/pages/account/prices/index.blade.php ENDPATH**/ ?>