<script src="/js/select.js" defer></script>
<script src="/js/masked.js" defer></script>
<script>
	document.addEventListener('DOMContentLoaded', function() {
		$('#phone').mask('+1 (999) 999-9999');
		$('#zip_code').mask('99999');
		$('#tin').mask('999-99-9999');

		$('#country').select2({
			minimumInputLength: 2,
			ajax: {
				url: '/ajax/getData',
				type: 'post',
                dataType: 'json',
                headers: {
                    'X-CSRF-TOKEN': '<?php echo e(csrf_token()); ?>'
                },
			    data:function(params) {
			      return {
			      	action: 'getCountries',
			        search: params.term,
			      };
			    },
				processResults: function (data) {
					return {
						results: data
					};
				}
			}
		});

		$('#region').select2({
			minimumInputLength: 2,
			ajax: {
				url: '/ajax/getData',
				type: 'post',
                dataType: 'json',
                headers: {
                    'X-CSRF-TOKEN': '<?php echo e(csrf_token()); ?>'
                },
			    data:function(params) {
			      return {
			      	action: 'getRegions',
			        search: params.term,
			        country_id: $('#country').val()
			      };
			    },
				processResults: function (data) {
					if ($('#country').val() == 0) {
						showPopup('<?php echo e(__('translations.select_country')); ?>');
					} else if (data) {
						return {
							results: data
						};
					} 
				}
			}
		});

		$('#locality').select2({
			minimumInputLength: 3,
			ajax: {
				url: '/ajax/getData',
				type: 'post',
                dataType: 'json',
                headers: {
                    'X-CSRF-TOKEN': '<?php echo e(csrf_token()); ?>'
                },
			    data:function(params) {
			      return {
			      	action: 'getLocalities',
			        search: params.term,
			        region_id: $('#region').val(),
			        country_id: $('#country').val()
			      };
			    },
				processResults: function (data) {
					if ($('#region').val() == 0) {
						showPopup('<?php echo e(__('translations.select_region')); ?>');
					} else if (data) {
						return {
							results: data
						};
					} 				
				}
			}
		});
	}, !1);
</script><?php /**PATH /var/www/d2l.sitepark.ua/resources/views/Frontend/templates/fields_script.blade.php ENDPATH**/ ?>