<?php $__env->startSection('page_title', __('voyager::generic.viewing').' '.ucfirst($type)); ?>

<?php $__env->startSection('page_header'); ?>
    <h1 class="page-title">
        <i class="voyager-list-add"></i> <?php echo e(ucfirst($type)); ?>        
    </h1>
<?php $__env->stopSection(); ?>
 
<?php $__env->startSection('content'); ?> 
    <div class="page-content container-fluid">
        <?php echo $__env->make('voyager::alerts', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-body">
                        <form method="get" action="<?php echo e(route('voyager.'.$type.'.index', $requests)); ?>">
                            <div class="col-sm-10">
                                <div class="row">                      
                                    <div class="col-sm-12">  
                                        <div class="col-md-3">
                                            <select onchange="$(this).parents('form').submit()" class="form-control" name="moderation">
                                                <option value="0" default>Moderation</option>
                                                <option value="1" <?php if(isset($filter['filter_moderation']) && $filter['filter_moderation'] == 1): ?> selected <?php endif; ?>>Not need</option>
                                                <option value="2" <?php if(isset($filter['filter_moderation']) && $filter['filter_moderation'] == 2): ?> selected <?php endif; ?>>Need</option>           
                                            </select>
                                        </div>                                      
                                        <div class="col-md-3">
                                            <select onchange="$(this).parents('form').submit()" class="form-control" name="status">
                                                <option value="0" default>Select status</option>
                                                <option value="1" <?php if(isset($filter['filter_status']) && $filter['filter_status'] == 1): ?> selected <?php endif; ?>>Actice</option>
                                                <option value="2" <?php if(isset($filter['filter_status']) && $filter['filter_status'] == 2): ?> selected <?php endif; ?>>Disactice</option>           
                                            </select>
                                        </div>
                                        <div class="col-md-3">
                                            <select onchange="$(this).parents('form').submit()" class="form-control" id="branch" name="branch">                                                
                                                <?php if(isset($filter['filter_branch']) && $filter['filter_branch'] && $branch): ?> 
                                                    <option value="<?php echo e($branch->id); ?>" selected><?php echo e($branch->name); ?></option>
                                                <?php else: ?>
                                                    <option value="0">Location</option>
                                                <?php endif; ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                                        
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <input type="submit" style="margin:0;" class="btn btn-success" value="OK">
                                <a class="btn btn-danger" style="margin:0;" href="<?php echo e(route('voyager.'.$type.'.index')); ?>">Clear</a>
                            </div>
                        </form>
                        <table id="dataTable" class="table table-hover">
                            <thead>
                            <tr>                                
                                <th>ID</th>     
                                <th>Location</th>
                                <th>Moderation</th>                               
                                <th>Status</th>                          
                                <th class="actions text-right"><?php echo e(__('voyager::generic.actions')); ?></th>
                            </tr>
                            </thead>
                            <tbody>
                                <?php $__currentLoopData = $many; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $one): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr style="<?php if($one->moderation == 2): ?><?php echo e('color:#000;background-color:rgba(255, 0, 0, 0.35);'); ?><?php endif; ?>">
                                    <td>Review for Order #<?php echo e($one->order_id); ?> (<?php echo e($one->user->name.' '.$one->user->surname); ?>)</td>
                                    <td>
                                        <?php if(isset($one->branch)): ?>
                                            <?php echo e($one->branch->name); ?>

                                        <?php endif; ?>
                                    </td>
                                    <td>
                                        <?php
                                            if ($one->moderation == 1) echo 'No';
                                            if ($one->moderation == 2) echo 'Yes';
                                        ?>
                                    </td>
                                    <td>
                                    	<?php
                                    		if ($one->status == 1) echo 'Actice';
                                    		if ($one->status == 2) echo 'Disactice';
                                    	?>
                                    </td>
                                    <td class="no-sort no-click bread-actions">
                                        <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('edit', $model)): ?>
                                            <a href="<?php echo e(route('voyager.'.$type.'.edit', [$one->id, 'type' => $one->type_str, $requests])); ?>" class="btn btn-sm btn-primary pull-right edit">
                                                <i class="voyager-edit"></i> <?php echo e(__('voyager::generic.edit')); ?>

                                            </a>
                                        <?php endif; ?>                                        
                                    </td>
                                </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="panel-body">	                    
                        <div class="pull-right"><?php echo e($many->appends(\Request::except('page'))->links()); ?></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?> 
    <script src="/js/select.js"></script>  
    <script> 
        $('#branch').select2({
            minimumInputLength: 3,
            ajax: {
                url: '<?php echo e(route("voyager.getbranches")); ?>',
                type: 'post',
                dataType: 'json',
                headers: {
                    'X-CSRF-TOKEN': '<?php echo e(csrf_token()); ?>'
                },
                data:function(params) {
                  return {
                    search: params.term,
                  };
                },
                processResults: function (data) {
                    if (data) {
                        return {
                            results: data
                        }; 
                    }
                }
            }
        });
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('voyager::master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/d2l.sitepark.ua/resources/views/Backend/order-reviews/browse.blade.php ENDPATH**/ ?>