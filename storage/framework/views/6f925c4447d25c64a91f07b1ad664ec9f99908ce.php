<?php $__env->startSection('css'); ?>
	<link rel="stylesheet" href="/css/select.css">	
	<link rel="stylesheet" href="<?php echo e(mix('/css/account.css')); ?>">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>	
	<form class="content-editing-form "  method="POST" action="<?php echo e(route('settings')); ?>" enctype="multipart/form-data">
		<div id="panel_top_buttons" class="fx fac fxb">
			<?php echo $__env->make('Frontend.templates.breadcrumbs', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
			<div class="panel-top-buttons button-submit order-buttons">
				<a class="btn btn-small btn-grey" href="<?php echo e(route('account')); ?>">&larr; <?php echo e(__('translations.back')); ?></a>
				<input type="submit" id="submit" class="btn btn-small" name="submit" value="<?php echo e(__('translations.save')); ?>">
			</div>
		</div>
		<h1 class="tc"><?php echo e($seo->name); ?></h1>
		<?php echo e(csrf_field()); ?>		
		<div class="fx fxb panel-body settings-form">
			<div class="panel <?php if(Auth::user()->role_id != config('roles.doctor') && Auth::user()->role_id != config('roles.tech')): ?> panel-half <?php endif; ?>">
				<div class="tc tb panel-head"><?php echo e(__('translations.setting_person')); ?></div>
				<div class="form-line">	
					<label for="email"><?php echo e(__('translations.email')); ?></label>			
					<input type="email" id="email" class="form-input" value="<?php echo e($data->email); ?>" disabled="disabled">
				</div>
				<div class="form-line">	
					<label for="type"><?php echo e(__('translations.account_type')); ?></label>					
					<input type="text" class="form-input" value="<?php echo e($data->type_info->name); ?>" disabled="disabled">
					<input type="hidden" name="type" id="type" value="<?php echo e(old('type')); ?>">					
				</div>
				<div class="form-line">	
					<label for="name"><?php echo e(__('translations.name')); ?></label>			
					<input type="text" name="name" id="name" class="form-input" placeholder="<?php echo e(__('translations.name')); ?>" value="<?php echo e($data->name ?? ''); ?>" required="required">
					<?php if($errors->has('name')): ?>
		                <div class="error"><?php echo e($errors->first('name')); ?></div>
		            <?php endif; ?>				
				</div>
				<div class="form-line">		
				    <label for="surname"><?php echo e(__('translations.surname')); ?></label>		
					<input type="text" name="surname" id="surname" class="form-input" placeholder="<?php echo e(__('translations.surname')); ?>" value="<?php echo e($data->surname ?? ''); ?>" required="required">
					<?php if($errors->has('surname')): ?>
		                <div class="error"><?php echo e($errors->first('surname')); ?></div>
		            <?php endif; ?>				
				</div>
				<div class="form-line">		
					<label for="phone"><?php echo e(__('translations.phone')); ?></label>		
					<input type="text" name="phone" id="phone" class="form-input" placeholder="<?php echo e(__('translations.phone')); ?>" value="<?php echo e($data->phone ?? ''); ?>" <?php if(Auth::user()->type != config('types.guest')): ?>required="required"<?php endif; ?>>
					<?php if($errors->has('phone')): ?>
		                <div class="error"><?php echo e($errors->first('phone')); ?></div>
		            <?php endif; ?>				
				</div>	
				<div class="form-line">	
				    <label for="password"><?php echo e(__('translations.settings_password')); ?></label>			
					<input type="password" name="password" id="password" class="form-input" placeholder="<?php echo e(__('translations.settings_password')); ?>" value="">
					<?php if($errors->has('password')): ?>
		                <div class="error"><?php echo e($errors->first('password')); ?></div>
		            <?php endif; ?>				
				</div>
				<div class="form-line setting-logo-line">				
						<div class="fx fxb fac">
							<?php if($data->logo): ?> 
							<div class="fx fac company-logo-title">
								<?php echo e(__('translations.avatar')); ?>: <div class="bi company-logo" style="background-image: url(<?php echo e(App\Services\Img::getIcon($data->avatar)); ?>)"></div>
							</div>

							<?php endif; ?>
							<div id="file_psev_input2" onclick="$('#avatar').click();">
								<span><?php echo e(__('translations.select_new_avatar')); ?></span>
							</div>
						</div>
						
						<input type="hidden" name="data_avatar[status]" id="file_psev_input2_status" value="0">
						<input type="hidden" name="data_avatar[width]" id="file_psev_input2_width" value="">
						<input type="hidden" name="data_avatar[height]" id="file_psev_input2_height" value="">
						<input type="hidden" name="data_avatar[x]" id="file_psev_input2_x" value="">
						<input type="hidden" name="data_avatar[y]" id="file_psev_input2_y" value="">
										
						<input type="file" class="uploaded-image" data-input="file_psev_input2" data-text="<?php echo e(__('translations.select_new_avatar')); ?>" name="avatar" id="avatar" value="" accept="image/jpeg,image/jpg,image/png" style="height: 1px; opacity: 0;padding: 0;">
						<?php if($errors->has('avatar')): ?>
			                <div class="error"><?php echo e($errors->first('avatar')); ?></div>
			            <?php endif; ?>				
					</div>
			</div>
			<?php if(Auth::user()->role_id != config('roles.doctor') && Auth::user()->role_id != config('roles.tech')): ?>
				<div class="panel panel-half" id="company_settings">
					<?php if(Auth::user()->type == config('types.guest') && Auth::user()->role_id == config('roles.guest')): ?>
						<div class="fx fxc panel-mask" id="company_upgrade" <?php if($errors->has('company') || $errors->has('locality') || $errors->has('address') || $errors->has('logo')): ?> style="display: none;" <?php endif; ?>>
							<div class="btn show-company-panel" data-type="<?php echo e(config('types.lab')); ?>" data-text="<?php echo e(__('translations.change_account_type3')); ?>"><?php echo e(__('translations.upgrade_lab')); ?></div>
							<span>or</span>
							<div class="btn btn-primary show-company-panel" data-type="<?php echo e(config('types.clinic')); ?>" data-text="<?php echo e(__('translations.change_account_type2')); ?>"><?php echo e(__('translations.upgrade_clinic')); ?></div>
						</div>
					<?php endif; ?>
					<div class="tc tbp panel-head"><?php echo e(__('translations.setting_company')); ?></div>	
					<?php if(Auth::user()->type == config('types.guest') && Auth::user()->role_id == config('roles.guest')): ?>
						<div id="cancel_company_line" class="form-line cancel-company-line" <?php if($errors->has('company') || $errors->has('locality') || $errors->has('address') || $errors->has('logo')): ?> style="display: block;" <?php else: ?> style="display: none;" <?php endif; ?> style="display: none;">
							<p id="upgrade_text"><?php if(old('type')): ?><?php echo e(__('translations.change_account_type'.old('type'))); ?><?php endif; ?></p>
							<div class="btn btn-danger" onclick="hideCompanyPanel();"><?php echo e(__('translations.cancel_upgrade')); ?></div>
						</div>
					<?php endif; ?>				
					<div class="form-line">	
						<label for="company"><?php echo e(__('translations.company')); ?></label>			
						<input type="text" name="company" id="company" class="form-input" placeholder="<?php echo e(__('translations.company')); ?>" value="<?php echo e($data->company ?? ''); ?>" <?php if(Auth::user()->type != config('types.guest')): ?>required="required"<?php endif; ?>>						
						<?php if($errors->has('company')): ?>
			                <div class="error"><?php echo e($errors->first('company')); ?></div>
			            <?php endif; ?>				
					</div>
					<div class="form-line">	
					    <label for="zip_code"><?php echo e(__('translations.zip_code')); ?></label>			
						<input type="text" name="zip_code" id="zip_code" class="form-input" placeholder="<?php echo e(__('translations.zip_code')); ?>" value="<?php echo e($data->zip_code ?? ''); ?>" <?php if(Auth::user()->type != config('types.guest')): ?>required="required"<?php endif; ?>>
						<?php if($errors->has('zip_code')): ?>
			                <div class="error"><?php echo e($errors->first('zip_code')); ?></div>
			            <?php endif; ?>				
					</div>
					<div class="form-line">
						<label for="country"><?php echo e(__('translations.country')); ?></label>
						<select id="country" name="country" class="form-controll" required="required">
							<?php if($locality): ?>
								<option value="<?php echo e($locality->country->id); ?>"><?php echo e($locality->country->name); ?></option>
							<?php else: ?> 
								<option value="0"><?php echo e(__('translations.country')); ?></option>
							<?php endif; ?>							
						</select>
						<?php if($errors->has('country')): ?>
		                    <div class="error"><?php echo e($errors->first('country')); ?></div>
		                <?php endif; ?>
					</div>
					<div class="form-line">
						<label for="region"><?php echo e(__('translations.region')); ?></label>
						<select id="region" name="region" class="form-controll" <?php if(Auth::user()->type != config('types.guest')): ?>required="required"<?php endif; ?>>
							<?php if($locality): ?>
								<option value="<?php echo e($locality->region->id); ?>"><?php echo e($locality->region->name); ?></option>
							<?php else: ?> 
								<option value="0"><?php echo e(__('translations.region')); ?></option>
							<?php endif; ?>
						</select>
						<?php if($errors->has('region')): ?>
			                <div class="error"><?php echo e($errors->first('region')); ?></div>
			            <?php endif; ?>
					</div>
					<div class="form-line">
						<label for="locality"><?php echo e(__('translations.locality')); ?></label>
						<select id="locality" name="locality" class="form-controll" <?php if(Auth::user()->type != config('types.guest')): ?>required="required"<?php endif; ?>>
							<?php if($locality): ?>
								<option value="<?php echo e($locality->id); ?>"><?php echo e($locality->name); ?></option>
							<?php else: ?> 
								<option value="0"><?php echo e(__('translations.locality')); ?></option>
							<?php endif; ?>
						</select>
						<?php if($errors->has('locality')): ?>
			                <div class="error"><?php echo e($errors->first('locality')); ?></div>
			            <?php endif; ?>
					</div>
					<div class="form-line">
						<label for="address"><?php echo e(__('translations.address')); ?></label>				
						<input type="text" name="address" id="address" class="form-input" placeholder="<?php echo e(__('translations.address')); ?>" value="<?php echo e($data->address ?? ''); ?>" <?php if(Auth::user()->type != config('types.guest')): ?>required="required"<?php endif; ?>>
						<?php if($errors->has('address')): ?>
			                <div class="error"><?php echo e($errors->first('address')); ?></div>
			            <?php endif; ?>				
					</div>
					<div class="form-line setting-logo-line">				
						<div class="fx fxb fac">
							<?php if($data->logo): ?> 
							<div class="fx fac company-logo-title">
								<?php echo e(__('translations.logo')); ?>: <div class="bi company-logo" style="background-image: url(<?php echo e(App\Services\Img::getIcon($data->logo)); ?>)"></div>
							</div>
							<?php endif; ?>
							<div id="file_psev_input" onclick="$('#logo').click();">
								<span><?php echo e(__('translations.select_new_logo')); ?></span>
							</div>
						</div>

						<input type="hidden" name="data_logo[status]" id="file_psev_input_status" value="0">
						<input type="hidden" name="data_logo[width]" id="file_psev_input_width" value="">
						<input type="hidden" name="data_logo[height]" id="file_psev_input_height" value="">
						<input type="hidden" name="data_logo[x]" id="file_psev_input_x" value="">
						<input type="hidden" name="data_logo[y]" id="file_psev_input_y" value="">
										
						<input type="file" class="uploaded-image" data-input="file_psev_input" data-text="<?php echo e(__('translations.select_new_logo')); ?>" name="logo" id="logo" value="" accept="image/jpeg,image/jpg,image/png" style="height: 1px; opacity: 0;padding: 0;">
						<?php if($errors->has('logo')): ?>
			                <div class="error"><?php echo e($errors->first('logo')); ?></div>
			            <?php endif; ?>				
					</div>	
				</div>
			<?php endif; ?>
		</div>
	</form>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>	
	<?php echo $__env->make('Frontend.templates.fields_script', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
	<?php echo $__env->make('Frontend.templates.img_upload_script', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
	<?php if(Auth::user()->type == config('types.guest') && Auth::user()->role_id == config('roles.guest')): ?>
		<script>
			document.addEventListener('DOMContentLoaded', function(){
				$('.show-company-panel').on('click', function(){
					var type = $(this).data('type')
						text = $(this).data('text');
					
					$('#type').val(type);
					$('#upgrade_text').html(text);
					$('#company_upgrade').fadeOut();
					$('#cancel_company_line').slideToggle(0);
				});
			}, !1);

			function hideCompanyPanel() {
				$('#company_upgrade').fadeIn();
				$('#cancel_company_line').slideToggle(0);
				$('#type').val('');
				$('#upgrade_text').html('');
			}
		</script>
	<?php endif; ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('Frontend/account_master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/d2l.sitepark.ua/resources/views/Frontend/pages/account/settings.blade.php ENDPATH**/ ?>