<style>
	.crop-popup-content h2{
		margin-top: 0;
	}
	.crop-area{
		position: relative;
		overflow: hidden;
		margin: 30px auto 20px;
	}
	.crop-square{
		position: absolute;
		top: 1px;
		left: 1px;
		border: 2px solid #fff;
	}
	.crop-square > div{
		position: relative;
		width: 100%;
		height: 100%;
		border: 1px solid #000;
	}
	.crop-square-move {
		position: absolute;
		top: 0;
		left: 0;
		width: 15px;
		height: 15px;
		background-color: #fff;
		background-image: url('/img/move.png');
		background-repeat: no-repeat;
		background-position: center center;
		background-size: 90%;
		cursor: pointer;
		z-index: 2;
	}
	.crop-square-resize {
		position: absolute;
		bottom: 0;
		right: 0;
		width: 15px;
		height: 15px;
		background-color: #fff;
		background-image: url('/img/resize.png');
		background-repeat: no-repeat;
		background-position: center center;
		background-size: 90%;
		cursor: pointer;
		z-index: 2;
	}
	.crop-buttons{
		text-align: right;
	}
	.crop-buttons div.btn{
		margin-left: 10px;
		padding: 8px 20px;
	}
</style>
<script>
	document.addEventListener('DOMContentLoaded', function(){
		$('.uploaded-image').on('change', function(){
			var id = $(this).data('input'),
				file = $(this)[0].files[0],
				text = $(this).data('text'),
				value = $(this).val();

			if (!id || !file) return false;

			var img = new Image();
			
			img.src = window.URL.createObjectURL(file);

			img.onload = function() {

				if (img.width < 50 || img.height < 50) {
			   		showPopup(('<?php echo e(__('translations.img_upload_small')); ?>'));	
			   		return false;
			   	}

			   	if (img.width == img.height) {
					$('#'+id).html('<span>'+value+'</span>');
			   	} else {
			   		var width = 500, crop = 100;

			   		if ($(window).width() < 768) width = 275;
			   		
			   		if (img.width < width) {
			   			width = img.width;
			   			crop = 48;
			   		}

			   		var index = img.width / width;

			   		html = '<h2><?php echo e(__('translations.img_crop_head')); ?></h2>';
			   		html += '<p><?php echo e(__('translations.img_crop_intro')); ?></p>';
			   		html += '<div id="crop_area" class="crop-area" style="width:'+width+'px;">';
			   		html += '<img src="'+img.src+'" width="100%" height="auto">';
			   		html += '<div id="crop_square" class="crop-square" style="width:'+crop+'px;height:'+crop+'px;">';
			   		html += '<div>';
			   		html += '<div id="crop_square_move" class="crop-square-move"></div>';
			   		html += '<div id="crop_square_resize" class="crop-square-resize"></div>';
			   		html += '</div>';
			   		html += '</div>';			   		
					html += '</div>';
					html += '<div class="crop-buttons">';
			   		html += '<div class="btn btn-danger" id="crop_cancel"><?php echo e(__('translations.img_upload_cancel')); ?></div>';
					html += '<div class="btn" id="crop_save"><?php echo e(__('translations.img_upload_crop')); ?></div>';
					html += '</div>';

			   		$.colorbox({
						html: '<div class="popup-content crop-popup-content">'+html+'</div>',
						onComplete : function() {
							$(this).colorbox.resize();

							var x = 0, y = 0;
							var move = false;
							var resize = false;

							$('#crop_square_move').on('mousedown', function(e){
								e.preventDefault();
								move = true;
							});

							$('#crop_square_resize').on('mousedown', function(e){
								e.preventDefault();
								resize = true;
							});

							$('#crop_area').on('mouseup', function(){
								move = false;
								resize = false;					
							});

							$('#crop_area').on('mousemove', function(e){
								if (move) {
									x = e.pageX - $('#crop_area').offset().left;
									y = e.pageY - $('#crop_area').offset().top;

									if ((x + $('#crop_square').width()) > $('#crop_area').width() || (y + $('#crop_square').height()) > $('#crop_area').height()) return false;

									$('#crop_square').css({
										'top': y+'px',
										'left': x+'px'
									});
								}

								if (resize) {
									current_x = e.pageX - $('#crop_area').offset().left;
									current_y = e.pageY - $('#crop_area').offset().top;

									crop = e.pageX - $('#crop_square').offset().left;

									var max_size = 50;

			   						if (img.width < img.height) {
			   							max_size = $('#crop_area').width()  - ($('#crop_square').offset().left - $('#crop_area').offset().left);
			   						} else {
										max_size = $('#crop_area').height() - ($('#crop_square').offset().top - $('#crop_area').offset().top);
			   						}

									if (crop < 50 || crop > max_size) return false;

									$('#crop_square').css({
										'width': crop+'px',
										'height': crop+'px'
									});
								}
							});
							

							$('#crop_cancel').on('click', function(){
								$(this).val('');
								$('#'+id).html('<span>'+text+'</span>');
								$('#'+id+'_status').val(0);
								$('#'+id+'_width').val('');
								$('#'+id+'_hight').val('');
								$('#'+id+'_x').val('');
								$('#'+id+'_y').val('');	
								
								$.colorbox.close();
							});

							$('#crop_save').on('click', function(){
								$('#'+id).html('<span>'+value+'</span>');
								$('#'+id+'_status').val(1);
								$('#'+id+'_width').val(crop * index);
								$('#'+id+'_height').val(crop * index);
								$('#'+id+'_x').val(x * index);
								$('#'+id+'_y').val(y * index);

								$.colorbox.close();
							});
						}
					});
			   }
			}
		});
	}, !1);
</script><?php /**PATH /var/www/d2l.sitepark.ua/resources/views/Frontend/templates/img_upload_script.blade.php ENDPATH**/ ?>