<h2>Welcome to Dentist2Lab service!</h2>

<p><?php echo e($owner); ?> has invited you to our service as <?php echo e($role); ?>.</p>
<p>Your login details:</p>
<p>Login: <?php echo e($email); ?></p>
<p>Password: <?php echo e($password); ?></p>

<p><a href="<?php echo e($url); ?>">Sign in!</a></p>
<br>
<p>Warning! Do not share your login details to other people!</p><?php /**PATH /var/www/dentist2lab.com/resources/views/Frontend/emails/staff.blade.php ENDPATH**/ ?>