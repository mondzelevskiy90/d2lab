<div class="panel dashboard-item dashboard-item-<?php echo e($code); ?>">
	<div class="panel-head"><?php echo e(__('translations.locations_list')); ?></div>
	<?php if($content['text'] && $content['text'] != ''): ?>
		<div class="panel-intro"><?php echo $content['text']; ?></div>
	<?php endif; ?>
	<div class="panel-content">
		<?php if($content['data'] && count($content['data'])): ?>
			<div class="dashboard-locations-list">
				<div class="fx fac fxb dashboard-locations-line dashboard-locations-lineHead">
					<div class="dashboard-locations-logo"></div>
					<div class="dashboard-locations-name"><?php echo e(__('translations.branch')); ?></div>
					<div class="dashboard-locations-cancelled">
						<?php echo e(__('translations.cancelled_orders')); ?>

					</div>
					<div class="dashboard-locations-procces">
						<?php echo e(__('translations.procces_orders')); ?>

					</div>
					<div class="dashboard-locations-complited">
						<?php echo e(__('translations.сomplited_orders')); ?>

					</div>
					<div class="dashboard-locations-unpaid"><?php echo e(__('translations.unpaid_orders')); ?></div>
					<div class="dashboard-locations-paid"><?php echo e(__('translations.paid_orders')); ?></div>
				</div>
				<?php $__currentLoopData = $content['data']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
					<div class="fx fac fxb dashboard-locations-line">
						<div class="fx fxc dashboard-locations-logo"><a class="bi" href="<?php echo e(route('branches.edit', $item['id'])); ?>" style="background-image: url(<?php echo e(App\Services\Img::getIcon($item['logo'])); ?>)"></a></div>
						<div class="dashboard-locations-name"><a href="<?php echo e(route('branches.edit', $item['id'])); ?>"><?php echo e($item['name']); ?></a></div>
						<div class="dashboard-locations-cancelled">
							<a href="<?php echo e(route('orders')); ?>">
								<?php echo e($item['cancelled_orders']); ?>

								<span>($<?php echo e($item['cancelled_payments']); ?>)</span>
							</a>
						</div>
						<div class="dashboard-locations-procces">
							<a href="<?php echo e(route('orders')); ?>">
								<?php echo e($item['procces_orders']); ?>

								<span>($<?php echo e($item['procces_payments']); ?>)</span>
							</a>
						</div>
						<div class="dashboard-locations-complited">
							<a href="<?php echo e(route('orders')); ?>">
								<?php echo e($item['сomplited_orders']); ?>

								<span>($<?php echo e($item['сomplited_payments']); ?>)</span>
							</a>
						</div>
						<div class="dashboard-locations-unpaid">
							<a href="<?php echo e(route('invoices', ['branch' => $item['id']])); ?>">$<?php echo e($item['unpaid']); ?></a>
						</div>
						<div class="dashboard-locations-paid">
							<a href="<?php echo e(route('invoices', ['branch' => $item['id']])); ?>">$<?php echo e($item['paid']); ?></a>
						</div>
					</div>
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
			</div>
		<?php else: ?> 
			<p><?php echo e(__('translations.no_locations_list')); ?></p>
		<?php endif; ?>
	</div>
</div>
<?php /**PATH /var/www/d2l.sitepark.ua/resources/views/Frontend/templates/dashboard/locations.blade.php ENDPATH**/ ?>