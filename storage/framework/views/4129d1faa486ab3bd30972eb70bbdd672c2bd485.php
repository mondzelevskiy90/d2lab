<?php $__env->startSection('css'); ?>
	<link rel="stylesheet" href="<?php echo e(mix('/css/login.css')); ?>">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('breadcrumbs'); ?>
	<?php echo $__env->make('Frontend.templates.breadcrumbs', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
	
	<div class="registration-form">	
		<p class="registration-terms"></p>
		<p class="registration-terms"></p>
		<p class="registration-terms"></p>
		<p class="registration-terms"><?php echo __('translations.forgot_text'); ?></p>	
		<p class="registration-terms"></p>		
		<form method="POST" action="<?php echo e(route('forgot')); ?>">
			<?php echo e(csrf_field()); ?>			
			<div class="form-line">				
				<input type="email" name="email" id="email" class="form-input" placeholder="<?php echo e(__('translations.email')); ?>" value="<?php echo e(old('email')); ?>" required="required">
				<?php if($errors->has('email')): ?>
                    <div class="error"><?php echo e($errors->first('email')); ?></div>
                <?php endif; ?>				
			</div>
			<div class="form-line">				
				<input type="submit" id="submit" class="btn" name="submit" value="<?php echo e(__('translations.forgot_button')); ?>">
			</div>
		</form>		
	</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
	
<?php $__env->stopSection(); ?>
<?php echo $__env->make('Frontend/master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/d2l.sitepark.ua/resources/views/Frontend/pages/forgot.blade.php ENDPATH**/ ?>