<?php $__env->startSection('css'); ?>
	<link rel="stylesheet" href="/css/select.css">
	<link rel="stylesheet" href="<?php echo e(mix('/css/account.css')); ?>">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
	<form class="editing-form content-editing-form " method="POST" action="<?php echo e(isset($data->id) ? route('branches.update', [$data->id, $requests]) : route('branches.store', $requests)); ?>" enctype="multipart/form-data">
		<div id="panel_top_buttons" class="fx fac fxb">
			<?php echo $__env->make('Frontend.templates.breadcrumbs', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
			<div class="panel-top-buttons button-submit order-buttons">
				<a class="btn btn-small btn-grey" href="<?php echo e(route('branches', [$requests])); ?>">&larr; <?php echo e(__('translations.back')); ?></a>
				<input type="submit" id="submit" class="btn btn-small" name="submit" value="<?php if(isset($data->id)): ?><?php echo e(__('translations.save_changes')); ?><?php else: ?><?php echo e(__('translations.save')); ?><?php endif; ?>">
			</div>
		</div>
		<h1 class="tc"><?php echo e($seo->name); ?></h1>
		<div class="fx fxb panel-body branches-edit-form">		
			<?php echo e(csrf_field()); ?>			
			<div class="panel">	
				<div class="panel-head"><?php echo e(__('translations.branch_head')); ?></div>
				<?php if(Auth::user()->role_id == config('roles.lab_head') && $data && !isset($data->nmi_info)): ?>
					<div class="form-line">							
						<p style="font-weight: bold;color:red"><?php echo e(__('translations.reg_nmi_account')); ?></p>
						<p><a href="https://secure.nmi.com/merchants/login.php?cookie_check=1&auth_error=0">https://secure.nmi.com/merchants/login.php?cookie_check=1&auth_error=0</a></p>
						<br>
						<br>
					</div>
				<?php endif; ?>
				<?php if(!isset($data->id)): ?>
					<!--<div class="form-line">	
						<label for="industry"><?php echo e(__('translations.industry')); ?></label>			
						<select id="industry" class="form-controll" required="required">
							<option value="0"><?php echo e(__('translations.select_industry_field')); ?></option>
							<?php $__currentLoopData = $industries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $industry): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
								<option value="<?php echo e($industry->id); ?>"><?php echo e($industry->name); ?></option>
							<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
						</select>		
					</div>
					<div class="form-line">	
						<label for="industry_id"><?php echo e(__('translations.industry_id')); ?></label>			
						<select name="industry_id" id="industry_id" class="form-controll" required="required">
							<option value="0"><?php echo e(__('translations.select_industry_field')); ?></option>
						</select>		
					</div> -->
					<input type="hidden" name="industry_id" value="2"/>
				<?php else: ?> 
					<div class="form-line" style="display:none">	
						<label for="industry_id"><?php echo e(__('translations.industry_id')); ?></label>		
						<input type="text" id="industry_id" class="form-input" value="<?php echo e($industry->name); ?>" disabled="disabled">
					</div>
				<?php endif; ?>
				
				<div class="form-line">	
					<label for="name"><?php echo e(__('translations.name')); ?></label>			
					<input type="text" name="name" id="name" class="form-input" placeholder="<?php echo e(__('translations.name')); ?>" value="<?php echo e(old('name', $data->name ?? Auth::user()->company)); ?>" required="required" maxlength="255">
					<?php if($errors->has('name')): ?>
		                <div class="error"><?php echo e($errors->first('name')); ?></div>
		            <?php endif; ?>				
				</div>
				<?php if(!isset($data->status) || (isset($data->status) && $data->status != 3)): ?>
					<div class="form-line">
						<label for="status"><?php echo e(__('translations.status')); ?></label>				
						<select id="status" name="status" class="form-controll" required="required">
							<option value="1" <?php if(isset($data->status) && $data->status == 1): ?> selected <?php endif; ?>><?php echo e(__('translations.status_active')); ?></option>
							<option value="2" <?php if(isset($data->status) && $data->status == 2): ?> selected <?php endif; ?>><?php echo e(__('translations.status_disactive')); ?></option>						
						</select>				
					</div>
				<?php endif; ?>
				<div class="form-line">	
					<label for="description"><?php echo e(__('translations.description')); ?></label>			
					<textarea name="description" id="description" class="form-input" placeholder="<?php echo e(__('translations.description')); ?>" required="required" maxlength="1000" onchange="formatDescriptionField()"><?php echo e(old('description', $data->description ?? '')); ?></textarea>
					<?php if($errors->has('description')): ?>
		                <div class="error"><?php echo e($errors->first('description')); ?></div>
		            <?php endif; ?>			
				</div>			
				<div class="form-line">	
					<label for="email"><?php echo e(__('translations.email')); ?></label>			
					<input type="email" name="email" id="email" class="form-input" placeholder="<?php echo e(__('translations.email')); ?>" value="<?php echo e(old('email', $data->email ?? Auth::user()->email)); ?>" required="required">
					<?php if($errors->has('email')): ?>
		                <div class="error"><?php echo e($errors->first('email')); ?></div>
		            <?php endif; ?>				
				</div>
				<div class="form-line">		
					<label for="phone"><?php echo e(__('translations.phone')); ?></label>		
					<input type="text" name="phone" id="phone" class="form-input" placeholder="<?php echo e(__('translations.phone')); ?>" value="<?php echo e(old('phone', $data->phone ?? Auth::user()->phone)); ?>" required="required">
					<?php if($errors->has('phone')): ?>
		                <div class="error"><?php echo e($errors->first('phone')); ?></div>
		            <?php endif; ?>				
				</div>
				<div class="form-line">	
				    <label for="zip_code"><?php echo e(__('translations.zip_code')); ?></label>			
					<input type="text" name="zip_code" id="zip_code" class="form-input" placeholder="<?php echo e(__('translations.zip_code')); ?>" value="<?php echo e(old('zip_code', $data->zip_code ?? Auth::user()->zip_code)); ?>" required="required">
					<?php if($errors->has('zip_code')): ?>
		                <div class="error"><?php echo e($errors->first('zip_code')); ?></div>
		            <?php endif; ?>				
				</div>
				<div class="form-line">
					<label for="country"><?php echo e(__('translations.country')); ?></label>
					<select id="country" name="country" class="form-controll" required="required">
						<?php if($locality): ?>
							<option value="<?php echo e($locality->country->id); ?>"><?php echo e($locality->country->name); ?></option>
						<?php else: ?> 
							<option value="0"><?php echo e(__('translations.country')); ?></option>
						<?php endif; ?>							
					</select>
					<?php if($errors->has('country')): ?>
	                    <div class="error"><?php echo e($errors->first('country')); ?></div>
	                <?php endif; ?>
				</div>
				<div class="form-line">
					<label for="region"><?php echo e(__('translations.region')); ?></label>
					<select id="region" name="region" class="form-controll" required="required">
						<?php if($locality): ?>
							<option value="<?php echo e($locality->region->id); ?>"><?php echo e($locality->region->name); ?></option>
						<?php else: ?> 
							<option value="0"><?php echo e(__('translations.region')); ?></option>
						<?php endif; ?>
					</select>
					<?php if($errors->has('region')): ?>
		                <div class="error"><?php echo e($errors->first('region')); ?></div>
		            <?php endif; ?>
				</div>
				<div class="form-line">
					<label for="locality"><?php echo e(__('translations.locality')); ?></label>
					<select id="locality" name="locality" class="form-controll" required="required">
						<?php if($locality): ?>
							<option value="<?php echo e($locality->id); ?>"><?php echo e($locality->name); ?></option>
						<?php else: ?> 
							<option value="0"><?php echo e(__('translations.locality')); ?></option>
						<?php endif; ?>
					</select>
					<?php if($errors->has('locality')): ?>
		                <div class="error"><?php echo e($errors->first('locality')); ?></div>
		            <?php endif; ?>
				</div>
				<div class="form-line">
					<label for="address"><?php echo e(__('translations.address')); ?></label>				
					<input type="text" name="address" id="address" class="form-input" placeholder="<?php echo e(__('translations.address')); ?>" value="<?php echo e(old('address', $data->address ?? Auth::user()->address)); ?>" required="required">
					<?php if($errors->has('address')): ?>
		                <div class="error"><?php echo e($errors->first('address')); ?></div>
		            <?php endif; ?>				
				</div>
				<!--<div class="form-line">	
				    <label for="tin"><?php echo e(__('translations.tin')); ?></label>			
					<input type="text" name="tin" id="tin" class="form-input" placeholder="<?php echo e(__('translations.tin')); ?>" value="<?php echo e(old('tin', $data->tin ?? '')); ?>">
					<?php if($errors->has('tin')): ?>
		                <div class="error"><?php echo e($errors->first('tin')); ?></div>
		            <?php endif; ?>				
				</div> -->
				<div class="form-line setting-logo-line">				
					<div class="fx fxb fac">					
						<div class="fx fac company-logo-title">
							<?php echo e(__('translations.logo')); ?>: <div class="bi company-logo" style="background-image: url(/storage/<?php echo e($data->logo ?? Auth::user()->logo); ?>)"></div>
						</div>						
						<div id="file_psev_input" onclick="$('#logo').click();">
							<span><?php echo e(__('translations.select_new_logo')); ?></span>
						</div>
					</div>

					<input type="hidden" name="data_logo[status]" id="file_psev_input_status" value="0">
					<input type="hidden" name="data_logo[width]" id="file_psev_input_width" value="">
					<input type="hidden" name="data_logo[height]" id="file_psev_input_height" value="">
					<input type="hidden" name="data_logo[x]" id="file_psev_input_x" value="">
					<input type="hidden" name="data_logo[y]" id="file_psev_input_y" value="">
									
					<input type="file" class="uploaded-image" data-prefix="data_logo" data-input="file_psev_input" data-text="<?php echo e(__('translations.select_new_logo')); ?>" name="logo" id="logo" value="" accept="image/jpeg,image/jpg,image/png" style="height: 1px; opacity: 0;padding: 0;">
					<?php if($errors->has('logo')): ?>
		                <div class="error"><?php echo e($errors->first('logo')); ?></div>
		            <?php endif; ?>				
				</div>
			</div>
		</div>
	</form>	
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
	<?php echo $__env->make('Frontend.templates.fields_script', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
	<?php echo $__env->make('Frontend.templates.field_industry_script', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
	<?php echo $__env->make('Frontend.templates.img_upload_script', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
	<script>
		function formatDescriptionField() {
			var text = document.getElementById('description').value;

			text = text.replace(/(\r\n|\n|\r)/gm, ' ');

			document.getElementById('description').value = text;
		}
	</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('Frontend/account_master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/dentist2lab.com/resources/views/Frontend/pages/account/branches/edit.blade.php ENDPATH**/ ?>