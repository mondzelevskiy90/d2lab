<div class="panel  panel-half dashboard-item dashboard-item-half dashboard-item-<?php echo e($code); ?>">
	<div class="panel-head"><?php echo e(__('translations.latest_reviews')); ?></div>
	<?php if($content['text'] && $content['text'] != ''): ?>
		<div class="panel-intro"><?php echo $content['text']; ?></div>
	<?php endif; ?>
	<div class="panel-content">
		<?php if($content['data'] && count($content['data'])): ?>
			<?php $__currentLoopData = $content['data']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
				<?php if($item->user): ?>
				<div class="dashboard-message-line">
					<a href="<?php echo e(route('reviews.edit', $item->order_id)); ?>">
						<span class="dashboard-message-name"><?php echo e($item->user->name.' '.$item->user->surname); ?></span>
						<span class="dashboard-message-date"><?php echo e(date('m-d-Y', strtotime($item->created_at))); ?></span>
						<span class="dashboard-message-content"><?php echo e($item->review); ?></span>
					</a>
				</div>
				<?php endif; ?>
			<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
		<?php else: ?>
			<p><?php echo e(__('translations.no_latest_reviews')); ?></p>
		<?php endif; ?>
	</div>
</div><?php /**PATH /var/www/dentist2lab.com/resources/views/Frontend/templates/dashboard/reviews.blade.php ENDPATH**/ ?>