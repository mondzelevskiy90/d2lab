<script src="/js/masked.js" defer></script>
<script>
	var d = document;

	var s = function (selector) {
		return d.querySelector(selector);
	}

	var active = 1,
		price_for = 1;
		block = false;
		block_lower = false;
		block_upper = false;
	
	var statuses = [],
		available_statuses = [],
		selected_option = [],
		empties = [],
		selected = [];	

	d.addEventListener('DOMContentLoaded', function() {
		$('.prescription-price-input').on('keypress', function(e){
			if (e.keyCode == 13) {
				return false;
			}
		});

		$('#patient_sex, #order_type, #branch_id, #numbering_system, #status').select2({
			minimumResultsForSearch: -1
		});

		$('#user_phone').mask('+1 (999) 999-9999');

		$('#service_id').select2();

		$('#ship_date').daterangepicker({
			singleDatePicker: true,
			showDropdowns: true,
			minDate: '<?php if(isset($data->ship_date)): ?><?php echo e($data->ship_date); ?><?php else: ?><?php echo e(date('m-d-Y')); ?><?php endif; ?>',
			minYear: <?php echo e(date('Y')); ?>,
			maxYear: <?php echo e(date('Y')); ?> + 1,
			locale: {
		        format: 'MM-DD-YYYY'
		    },
		});

		<?php if(isset($prescriptions) && count($prescriptions)): ?>
			svgDataRequest().done(function(data) {
			   	if (data) {
					for(i = 0; i < data.length; i++) {
		    			if(data[i]) {
		    				statuses[i] = [];
		    				selected[i] = [];

		    				for(k = 1; k <= 32; k++) {
		    					if (data[i][k]) {
		    						tooth_data = data[i][k].split('_');

									empties[k] = false;
									statuses[i][k] = false;
		    						selected[i][k] = false;

		    						if (tooth_data[1]) {
										statuses[i][k] = tooth_data[1];
										selected[i][k] = data[i][k];
		    							if (tooth_data[1] == 5 || tooth_data[1] == 6 || tooth_data[1] == 7) empties[k] = k;
		    						}
		    					}
		    				}
		    			}
		    		}
				}

				listenTooths();				
				showActivePrescription(active);
			}).fail(function() {
			    listenTooths();	
			});
		<?php else: ?> 
			listenTooths();		
		<?php endif; ?>		
	}, !1);

	<?php if(isset($prescriptions) && count($prescriptions)): ?>
	function svgDataRequest() {
		return $.ajax({
			url: '/ajax/getData',
			type: 'post',
            dataType: 'json',
            headers: {
                'X-CSRF-TOKEN': '<?php echo e(csrf_token()); ?>'
            },
		    data: {action:'getOrderPrescriptionSelecteds', order_id: '<?php echo e($data->id); ?>'},
		    success: function (data) {
		    	
			}
		});
	}
	<?php endif; ?>

	function getBranchAddreess() {
		var branch_id = $('#branch_id').val();

		if (branch_id && branch_id != 0) {
			$.ajax({
				url: '/ajax/getData',
				type: 'post',
                dataType: 'json',
                headers: {
                    'X-CSRF-TOKEN': '<?php echo e(csrf_token()); ?>'
                },
			    data: {action:'getBranchAddreess', branch_id: branch_id},
			    success: function(data) {
			    	if (data) $('#ship_address').val(data);
			    }
			});
		}
	}

	var file_index = <?php echo e($file_index); ?>;

	function addFileToCheckoutForm() {
		if ($(d).find('#order_file_input'+file_index).length) {
			file_index++;
			addFileToCheckoutForm();

			return false;
		}

		createFileFieldCheckoutForm(file_index);

		$(d).find('#order_file_input'+file_index).click();

		var order_files = $(d).find('.order-file-line');

		if (order_files.length > 4) $('#order_files_add').css('display', 'none');

		file_index++;
	}

	function createFileFieldCheckoutForm(index) {
		var html = '';

		html += '<div id="order_file_line'+index+'" class="fx fxb order-file-line order-file-icon">';			
		html += '<div id="order_file_name'+index+'" class="fx fxc order-file-name">';	
		html += '<span>Empty!</span>';			
		html += '<div class="order-file-remove" onclick="removeFileFromCheckoutForm('+index+')" title="<?php echo e(__('translations.delete')); ?>"><div></div></div>';
		html += '<input type="hidden" id="order_files_save'+index+'" name="order_files['+index+']" value="">';	
		html += '<input type="file" id="order_file_input'+index+'" onchange="checkFileFieldCheckoutForm('+index+');">';
		html += '</div>';
		html += '</div>';

		$('#order_files').append(html);
	}

	function removeFileFromCheckoutForm(index) {
		$(d).find('#order_file_line'+index).remove();

		var order_files = $(d).find('.order-file-line');

		if (order_files.length <= 4) $('#order_files_add').css('display', 'block');
	}

	function checkFileFieldCheckoutForm(index) {
		var file = $(d).find('#order_file_input'+index);
		var value = file.prop('files')[0];

		if (!value || !file.val()) {
			removeFileFromCheckoutForm(index);

			return false;
		}

		if (value.size > 10000000) {
			showPopup('<?php echo e(__('translations.checkout_file_error')); ?>');
			removeFileFromCheckoutForm(index);

			return false;
		}

		let form_data = new FormData();

		form_data.append('order_file', value);	
		form_data.append('file_index', index);			
		form_data.append('action', 'saveCheckoutFile');		

		$.ajax({
			url: '/ajax/getData',
			type: 'post',
            dataType: 'json',
            processData: false,
        	contentType: false,
            headers: {
                'X-CSRF-TOKEN': '<?php echo e(csrf_token()); ?>'
            },
            data: form_data,
            success: function(data) {
            	if (data) {
            		$(d).find('#order_files_save'+index).val(data.file);

            		var html = '<a href="/storage/'+data.file+'" target="_blank" class="fx fxc">';

            		if (data.extension == 'jpg' || data.extension == 'jpeg' || data.extension == 'JPEG' || data.extension == 'JPG' || data.extension == 'png' || data.extension == 'PNG') {
						html += '<img src="/storage/'+data.file+'" width="100%" height="auto" />';
            		} else {
            			html += '<span>.'+data.extension+'</span>';
            		}

            		html += '</a>';

            		$('#order_file_name'+index+' span').replaceWith(html);
            	}
            }
		});			
	}

	var prescription_index = <?php echo e($prescription_index); ?>;
	var prescription_colors = {
		1: '#ffbab0',
		2: '#F2EDA2',
		3: '#BBDCEF',
		4: '#CAE8CE',
		5: '#F7C69C',
		6: '#f3d9da',
		7: '#ffb6ff',
		8: '#b58585',
		9: '#d2d222',
		10: '#DADADA'
	};

	function addPrescriptionBlock() {
		if ($(d).find('#order_prescription-block'+prescription_index).length) {
			prescription_index++;
			addPrescriptionBlock();
			
			return false;
		}
		var service = $('#service_id').val();
		var system = $('#numbering_system').val();
		var branch_id = $('#to_branch_id').val();
		var from_branch_id = $('#branch_id').val();

		var $data = $.ajax({
			url: '/ajax/getData',
			type: 'post',
            dataType: 'json',
            headers: {
                'X-CSRF-TOKEN': '<?php echo e(csrf_token()); ?>'
            },
		    data: {action:'getOrderPrescription', service: service, system: system, branch_id: branch_id, from_branch_id: from_branch_id},
		    success: function (data) {
		    	if (data) {
		    		var html = '';
		    		
					html += '<div id="order_prescription-block'+prescription_index+'" class="fx fxb panel-body">';
					html += '	<div class="panel">';
					html += '		<div class="btn btn-danger delete-order-prescription" onclick="removePrescriptionFromOrder('+prescription_index+');"><?php echo e(__('translations.delete')); ?></div>';
					html += '		<div class="panel-head"><?php echo e(__('translations.order_prescription')); ?> #'+prescription_index+'</div>';
					html += '		<div id="order_prescription'+prescription_index+'" class="panel-block">';
					html += '			<div class="form-line">';
					html += '				<label><?php echo e(__('translations.numbering_system')); ?>: <strong>'+data.numbering_system_name+'</strong></label>';
					html += '				<input type="hidden" name="prescriptions['+prescription_index+'][numbering_system_id]" value="'+data.numbering_system_id+'">';
					html += '			</div>';
					html += '			<div class="form-line">';
					html += '				<label><?php echo e(__('translations.service_name')); ?>: <strong>'+data.service_name+'</strong></label>';
					html += '				<input type="hidden" name="prescriptions['+prescription_index+'][service_id]" value="'+data.service_id+'">';
					html += '			</div>';

					if (data.min_days) {
						html += '				<input type="hidden" class="prescription-service-min-days" value="'+data.min_days+'"/>';
					}
					
					
					if (data.fields) {
						for (index in data.fields) {
							html += '<div class="form-line">';	
							html += '<div class="prescription-line-head">';
							html += '<label for="prescription'+data.fields[index]['id']+'" class="prescription-name">'+data.fields[index]['name']+(data.fields[index]['help'] ? '<span class="prescription-line-help" onclick="showFieldHelp('+data.fields[index]['id']+')">?</span>' : '')+'</label>';

							if (data.fields[index]['description']) {
								html += '<p class="prescription-desc">'+data.fields[index]['description']+'</p>';
							}

							html += '</div>';
							html += '<div class="fx fac prescription-line-content">';

							if (data.fields[index]['question'] && data.fields[index]['question'] == 1) {
								html += '<div class="prescription-line-question">';
								html += '<select id="select_prescription_'+prescription_index+'_'+data.fields[index]['id']+'" class="form-input select-select2-select" onchange="showPrescriptionFieldContent(\''+prescription_index+'_'+data.fields[index]['id']+'\');">';
								html += '<option value="no" default><?php echo e(__('translations.no')); ?></option>';
								html += '<option value="yes"><?php echo e(__('translations.yes')); ?></option>';
								html += '</select>';
								html += '</div>';
							}

							if (data.fields[index]['image']) { 
								html += '<div class="prescription-image">';
								html += '<img src="<?php echo e(url('/').'/storage/'); ?>'+data.fields[index]['image']+'">';
								html += '</div>';
							}

							html += '<div id="inputs_prescription_'+prescription_index+'_'+data.fields[index]['id']+'" class="fx prescription-inputs '+(data.fields[index]['image'] ? 'prescription-inputs-half' : '')+'" style="display:'+(data.fields[index]['question'] && data.fields[index]['question'] == 1 ? 'none' : 'flex')+'">';

							if (data.fields[index]['field_type'] == 'text') {
								html += '<input type="text" name="prescriptions['+prescription_index+'][fields]['+data.fields[index]['id']+']" id="prescription_'+prescription_index+'_'+data.fields[index]['id']+'" value="" placeholder="'+data.fields[index]['name']+'" maxlength="255" '+(data.fields[index]['required'] == 1 ? 'class="form-input form-input-required" required="required"' : 'class="form-input"')+'>';
							}

							if (data.fields[index]['field_type'] == 'select' && data.fields[index]['values']) {
								html += '<select name="prescriptions['+prescription_index+'][fields]['+data.fields[index]['id']+']" id="prescription_'+prescription_index+'_'+data.fields[index]['id']+'" '+(data.fields[index]['required'] == 1 ? 'class="form-input select-select2-select form-input-required" required="required"' : 'class="form-input select-select2-select"')+'>';
								
								for (index2 in data.fields[index]['values']) {
									html += '<option value="'+data.fields[index]['values'][index2]['value']+'" '+(data.fields[index]['values'][index2]['selected'] == 1 ? 'selected="selected"' : '')+'>'+data.fields[index]['values'][index2]['value']+'</option>';
								}

								html += '</select>';
							}

							if (data.fields[index]['field_type'] == 'checkbox' && data.fields[index]['values']) {
								for (index3 in data.fields[index]['values']) {
									html += '<div class="form-checkbox">';	

									if (data.fields[index]['values'][index3]['image']) {
										html += '<img src="<?php echo e(url('/').'/storage/'); ?>'+data.fields[index]['values'][index3]['image']+'">';
									}

									html += '<input type="checkbox" name="prescriptions['+prescription_index+'][fields]['+data.fields[index]['id']+']['+data.fields[index]['values'][index3]['id']+']" id="prescription_'+prescription_index+'_'+data.fields[index]['id']+'_'+data.fields[index]['values'][index3]['id']+'" value="'+data.fields[index]['values'][index3]['value']+'" '+(data.fields[index]['values'][index3]['selected'] == 1 ? 'checked="checked"' : '')+'>';
									html += '<label for="prescription_'+prescription_index+'_'+data.fields[index]['id']+'_'+data.fields[index]['values'][index3]['id']+'">'+data.fields[index]['values'][index3]['value']+'</label>';
									html += '</div>';
								}
							}

							if (data.fields[index]['field_type'] == 'radio' && data.fields[index]['values']) {
								for (index4 in data.fields[index]['values']) {
									html += '<div class="form-checkbox">';	

									if (data.fields[index]['values'][index4]['image']) {
										html += '<img src="<?php echo e(url('/').'/storage/'); ?>'+data.fields[index]['values'][index4]['image']+'">';
									}

									html += '<input type="radio" name="prescriptions['+prescription_index+'][fields]['+data.fields[index]['id']+']" id="prescription_'+prescription_index+'_'+data.fields[index]['id']+'_'+data.fields[index]['values'][index4]['id']+'" value="'+data.fields[index]['values'][index4]['value']+'" '+(data.fields[index]['values'][index4]['selected'] == 1 ? 'checked="checked"' : '')+'>';
									html += '<label for="prescription_'+prescription_index+'_'+data.fields[index]['id']+'_'+data.fields[index]['values'][index4]['id']+'">'+data.fields[index]['values'][index4]['value']+'</label>';
									html += '</div>';
								}
							}
									
							html += '</div>';
							html += '</div>';
							html += '</div>';
						}
					}

					html += '			<div class="form-line">';	
					html += '				<label for="prescription_'+prescription_index+'_comment"><?php echo e(__('translations.comment')); ?></label>';
					html += '				<textarea name="prescriptions['+prescription_index+'][comment]" id="prescription_'+prescription_index+'_comment" class="form-inut" placeholder="<?php echo e(__('translations.comment')); ?>" maxlength="1000"></textarea>';
					html += '			</div>';
					html += '		</div>';

					if (data.prescription_description) {
						html += '		<div class="panel-head panel-subhead panel-head-collapse panel-head-collapsed"><?php echo e(__('translations.prescription_instruction')); ?></div>';
						html += '		<div class="fx fxb panel-block panel-block-collapsed">';
						html += data.prescription_description;
						html += '		</div>';
					}

					html += '	</div>';
					html += '</div>';

					var html2 = '';

					var color_index = prescription_index;

					if (prescription_index > 10) color_index = 1;

					var color = prescription_colors[color_index];

					html2 += '<div id="order_prescription-service'+prescription_index+'" class="fx fxb order-prescription-service">';
					html2 += '  <input type="hidden" name="prescriptions['+prescription_index+'][price]" id="order_prescription_price'+prescription_index+'" class="order-prescription-price-input" value="'+( data.available_options && data.available_options != '' ? '0.00' : data.price )+'">';	
					html2 += '  <input type="hidden" name="prescriptions['+prescription_index+'][price_for]" id="order_prescription_price_for'+prescription_index+'" value="'+( data.available_options && data.available_options != '' ? 1 : 4 )+'">';
					html2 += '  <input type="hidden" name="prescriptions['+prescription_index+'][selected_option]" id="order_prescription_selected_option'+prescription_index+'" value="">';			
					html2 += '  <input type="hidden" name="prescriptions['+prescription_index+'][price_order_date]" id="order_prescription_price_order_date'+prescription_index+'" value="'+data.price+'">';
					html2 += '	<div id="order_prescription_name'+prescription_index+'" class="fx fac order-prescription-name" onclick="showActivePrescription('+prescription_index+')">';
					html2 += ' <div class="order-prescription-check" title="'+data.service_name+'"></div>'
					html2 += '  <div id="order_prescription_color'+prescription_index+'" class="order-prescription-color" data-color="'+color+'" style="background-color:'+color+'" title="'+data.service_name+'"></div>';
					html2 += '<div style="width: calc(100% - 55px);">#'+prescription_index+' <strong>'+data.service_name+'</strong></div>';
					html2 += '  </div>';						
					html2 += '  <div class="order-prescription-scroll" onclick="scrollToblock(\'order_prescription-block'+prescription_index+'\')" title="'+data.service_name+'"><?php echo e(__('translations.show_prescription')); ?></div>';
					html2 += '	<div class="btn btn-danger delete-order-prescription" onclick="removePrescriptionFromOrder('+prescription_index+');"><?php echo e(__('translations.delete')); ?></div>';
					html2 += '<input type="text" name="prescriptions['+prescription_index+'][selecteds]" id="prescriptions_selecteds_'+prescription_index+'" class="prescription-selecteds" value="" '+(data.available_options && data.available_options != '' ? 'required' : '')+'>';

					if (data.available_options && data.available_options != '') {
						html2 += '<div class="prescription-selecteds-text" id="order_prescription_text'+prescription_index+'"><?php echo e(__('translations.selected_items')); ?>: <span id="prescriptions_selecteds_text_'+prescription_index+'"></span></div>';

						html2 += '<div class="prescription-selecteds-options" id="order_prescription_options'+prescription_index+'">';
							html2 += '<span><?php echo e(__('translations.select')); ?>:</span>';

							for (index in data.available_options) {
								if (data.available_options[index] == 1) {
									html2 += '<div class="order-option-check '+(index == 0 ? 'order-option-checkAct' : '')+'" id="order_option_check'+prescription_index+'_1" onclick="selectOneTooth(1, '+prescription_index+', 1)">Crown</div>';
								}

								if (data.available_options[index] == 2) {
									html2 += '<div class="order-option-check '+(index == 0 ? 'order-option-checkAct' : '')+'" id="order_option_check'+prescription_index+'_2" onclick="selectBridge('+prescription_index+')">Bridge</div>';
								}								

								if (data.available_options[index] == 3) {
									html2 += '<div class="order-option-check" id="order_option_check'+prescription_index+'_3_2" onclick="selectAllTooths(2, '+prescription_index+')">Upper</div>';
									html2 += '<div class="order-option-check" id="order_option_check'+prescription_index+'_3_1" onclick="selectAllTooths(1, '+prescription_index+')">Lower</div>';
								}

								if (data.available_options[index] == 4) {
									html2 += '<div class="order-option-check '+(index == 0 ? 'order-option-checkAct' : '')+'" id="order_option_check'+prescription_index+'_4" onclick="selectOneTooth(4, '+prescription_index+', 2)">Partial denture (Flipper, Partial prosthetics)</div>';
								}

								if (data.available_options[index] == 5) {
									html2 += '<div class="order-option-check" id="order_option_check'+prescription_index+'_5_2" onclick="selectOneTooth(5, '+prescription_index+', 2)">Partial denture Upper</div>';
									html2 += '<div class="order-option-check" id="order_option_check'+prescription_index+'_5_1" onclick="selectOneTooth(5, '+prescription_index+', 1)">Partial denture Lower</div>';
								}
							}

						html2 += '</div>';
					}
					
					html2 += '</div>';

					$('#order_prescription_services').append(html2);
					$('#order_prescriptions').append(html);
					$('#create_order_button').css('display', 'block');

					if (data.available_options && data.available_options != '') {
						$('#numbering_system_mask').css('display', 'none');

						setTotalPrice();
					}

					$('.order-prescription-servicesHead').css('display', 'block');

					$('.select-select2-select').select2({
						minimumResultsForSearch: -1
					});

					showActivePrescription(prescription_index);
					selected[prescription_index] = [];
					statuses[prescription_index] = [];
					selected_option[prescription_index] = 1;

					if (data.available_options && data.available_options[0]) {
						selected_option[prescription_index] = data.available_options[0];

						if (data.available_options[0] == 1) {
							selectOneTooth(1, prescription_index, 1);
						}

						if (data.available_options[0] == 2) {
							selectBridge(prescription_index);							
						}

						if (data.available_options[0] == 3) {
							selectAllTooths(2, prescription_index, true);
						}

						if (data.available_options[0] == 4) {
							selectOneTooth(4, prescription_index, 2);
						}

						if (data.available_options[0] == 5) {
							selectOneTooth(5, prescription_index, 1);
						}
					}

					prescription_index++;

					if (data.min_days && data.min_days != 0) {
						checkOrderFormCustomFields(data.min_days);
					}
		    	}
		    }
		});
	}

	function removePrescriptionFromOrder(index) {
		$(d).find('#order_prescription-service'+index).remove();
		$(d).find('#order_prescription-block'+index).remove();

		if (selected[index]) delete selected[index];
		if (statuses[index]) delete statuses[index];

		rebuildSvg();

		if (!$('.order-prescription-service').length) {
			$('#order_prescription_total').html(0);
			$('#advance_price').val(0);
			$('#create_order_button').css('display', 'none');
			$('.order-prescription-servicesHead').css('display', 'none');
			$('#numbering_system_mask').css('display', 'flex');
		}
	}	

	function getNumberingSystemSvg() {
		var system = $('#numbering_system').val();

		if (system) {
			$.ajax({
				url: '/ajax/getData',
				type: 'post',
                dataType: 'json',
                headers: {
                    'X-CSRF-TOKEN': '<?php echo e(csrf_token()); ?>'
                },
			    data: {action:'getNumberingSystemSvg', system: system},
			    success: function (data) {
			    	if (data) {
			    		$('#numbering_system_block').html(data);

			    		listenTooths();
			    	}
			    }
			});
			
    		active = 1;
    		available_statuses = [];
			statuses = [];
			empties = [];
			selected = [];

			$('#order_prescription_total').html(0);
			$('#advance_price').val(0);
			$('#order_prescription_services').html('');
    		$('#order_prescriptions').html('');
    		$('#create_order_button').css('display', 'none');
    		$('.order-prescription-servicesHead').css('display', 'none');
    		$('#numbering_system_mask').css('display', 'flex');

    		prescription_index = 1;
		}
	}	

	function scrollToblock(id) {
		$('html, body').animate({
	        scrollTop: $(d).find('#'+id).offset().top
	    }, 500);
	} 

	function showActivePrescription(index) {
		$(d).find('.order-prescription-name').removeClass('order-prescription-name-act');
		$(d).find('#order_prescription_name'+index).addClass('order-prescription-name-act');
		$(d).find('.prescription-selecteds-text').removeClass('order-prescription-text-act');
		$(d).find('#order_prescription_text'+index).addClass('order-prescription-text-act');
		$(d).find('.prescription-selecteds-options').removeClass('order-prescription-options-act');
		$(d).find('#order_prescription_options'+index).addClass('order-prescription-options-act');

		active = index;	
		rebuildSvg();
	}

	//click events at single tooth
	function initTooth(num) {
		if (block) return false;

		num = parseInt(num);

		//check if defined set for current prescription
		if (active) {
			var check_available_statuses = available_statuses[active];
		} else {
			var check_available_statuses = available_statuses;
		}

		if (selected_option[active] == 5 && block_lower && num >= 17) {
			return false;
		}

		if (selected_option[active] == 5 && block_upper && num <= 16) {
			return false;
		}

		//add if not defined
		statuses[active][num] ? statuses[active][num]++ : statuses[active][num] = 2;

		//if not in set - continue
		if (!check_available_statuses.includes(statuses[active][num])) statuses[active][num]++;

		//block bridge start for last tooth and if not in set
		if(statuses[active][num] == 3 && (num == 16 || num == 32 || !check_available_statuses.includes(statuses[active][num]))) statuses[active][num]++;

		//block bridge srart after bridge start
		if (statuses[active][num] == 3 && (statuses[active][num - 1] && statuses[active][num - 1] == 3)) statuses[active][num]++;

		//block bridge srart before bridge start
		if (statuses[active][num] == 3 && (statuses[active][num + 1] && statuses[active][num + 1] == 3)) statuses[active][num]++;

		//block bridge end for first tooth and if not in set
		if(statuses[active][num] == 4 && (num == 1 || num == 17 || !check_available_statuses.includes(statuses[active][num]))) statuses[active][num]++; 

		//block bridge end after bridge end
		if (statuses[active][num] == 4 && (statuses[active][num - 1] && statuses[active][num - 1] == 4)) statuses[active][num]++;

		//block bridge end before bridge end
		if (statuses[active][num] == 4 && (statuses[active][num + 1] && statuses[active][num + 1] == 4)) statuses[active][num]++;

		//cantileaver before bridge start
		if(statuses[active][num] == 5) {
			if (!statuses[active][num + 1] || num + 1 == 16 || num + 1 == 32 || selected_option[active] != 2) {
				statuses[active][num]++;
			} else if (statuses[active][num + 1] && statuses[active][num + 1] != 3 && statuses[active][num + 1] != 5 || selected_option[active] != 2) {
				statuses[active][num]++;
			}
		} 

		//cantileaver after bridge end
		if(statuses[active][num] == 6 ) {
			if (!statuses[active][num - 1] || num - 1 == 1 || num - 1 == 17 || selected_option[active] != 2) {
				statuses[active][num] = 7;
			} else if (statuses[active][num - 1] && statuses[active][num - 1] != 4 && statuses[active][num - 1] != 6 || selected_option[active] != 2) {
				statuses[active][num]++;
			} 
		}

		//reset after empty
		if(statuses[active][num] > 7) statuses[active][num] = 1;

		//block empty if not in set
		if (statuses[active][num] == 7 && !check_available_statuses.includes(statuses[active][num])) statuses[active][num] = 1;

		//remember empty selected and cantileaver for all prescriptions as empty
		statuses[active][num] == 5 || statuses[active][num] == 6 || statuses[active][num] == 7 ? empties[num] = num : delete empties[num];

		addToothToService(num);
	}

	function addToothToService(num) {
		var values = selected[active],
			tooth = s('#tooth_'+num).getAttribute('data-tooth');

		if (statuses[active][num] && statuses[active][num] != 1) {
			values[num] = tooth+'_'+statuses[active][num]+'_'+s('#tooth_'+num+' text').innerHTML+'_'+selected_option[active];

			//delete all between bridge start - end
			if (selected_option[active] == 1 || selected_option[active] == 2) {
				var end = 0;
				var start = 0;

				//detect bridge start
				if (statuses[active][num] == 3) {
					start = num + 1;

					for (i = start; i <= 32; i++) {
						if (statuses[active][i] == 4) {
							end = i - 1;
							break;
						}
					}
				}

				//detect bridge end
				if (statuses[active][num] == 4) {
					end = num - 1;
					for (i = end; i >= 1; i--) {
						if (statuses[active][i] == 3) {
							start = i + 1;
							break;
						}
					}
				}

				if (start != num) {
					for (i = start; i <= end; i++) {
						if (statuses[active][i] != 7) {
							delete selected[active][i];
							delete statuses[active][i];
						}
					}
				}
			}
		} else {
			delete values[num];
		}

		rebuildSvg();
		showSelectedsText();

		s('#prescriptions_selecteds_'+active).value = values.join(',');
	}

	function rebuildSvg() {	
		//clear all selections	
		for (i = 1; i <= 32; i++) {
			s('#tooth_body_'+i).style.fill = '#fff';

			if (s('#tooth_prev_'+i)) s('#tooth_prev_'+i).style.display = 'none';
			if (s('#tooth_next_'+i)) s('#tooth_next_'+i).style.display = 'none';
			
			s('#tooth_point_'+i).style.display = 'none';
			s('#tooth_empty_'+i).style.display = 'none';
		}

		if (!d.querySelectorAll('.order-prescription-service').length) return false;

		//show selected
		for (i = 1; i <= selected.length; i++) {
			var indx = i <= 10 ? i : 1;
			var color = prescription_colors[indx];
			var lined = [];

			if (selected[i]) {
				var show = false;
				var end = false;				

				for (k = 1; k <= 32; k++) {	
					if (selected[i][k]) {
						var tooth_data = splitData(selected[i][k], '_');

						if (empties[k]) {
							s('#tooth_empty_'+k).style.display = 'block';
							s('#tooth_body_'+k).style.fill = '#fff';

							if (tooth_data && tooth_data[1]) {
								if (tooth_data[1] == 5) s('#tooth_next_'+k).style.display = 'block';
								if (tooth_data[1] == 6) s('#tooth_prev_'+k).style.display = 'block';
							}
						} else {
							if (tooth_data && tooth_data[1]) {								
								s('#tooth_empty_'+k).style.display = 'none';
							
								if (tooth_data[1] == 3) {
									s('#tooth_point_'+k).style.display = 'block';
									s('#tooth_next_'+k).style.display = 'block';
									show = k;
									end = false;										
								}

								if (tooth_data[1] == 4) {
									s('#tooth_prev_'+k).style.display = 'block';
									s('#tooth_point_'+k).style.display = 'block';

									show = false;
									end = true;
								}		

								s('#tooth_body_'+k).style.fill = color;
							}
						}
					}

					if (show) {
						for (y = k +1; y <= 32; y++) {
							if (statuses[i][y] == 4) {
								lined[k] = k;
							}

							if (statuses[i][y] == 3) {
								break;
							}
						} 
					}
				}

				if (end) {
					for (j = 1; j <= 32; j++) {	
						if (lined[j]) {
							if (!empties[j]) s('#tooth_body_'+j).style.fill = color;

							if (j != 16 && j != 32) s('#tooth_next_'+j).style.display = 'block';
						};
					}
				}
			}
		}		
		//show active color
		if (selected[active]) {
			var active_color = s('#order_prescription_color'+active).getAttribute('data-color');
			for (i = 1; i <= selected[active].length; i++) {
				if (selected[active][i]) {
					var tooth_data = splitData(selected[active][i], '_');

					if (tooth_data && tooth_data[1]) {
						s('#tooth_body_'+i).style.fill = active_color;
					}
				}				
			}
		}
		//calculate prices
		setPrices();
	}	

	function listenTooths() {		
		var tooths = d.querySelectorAll('.tooth-item');

		tooths.forEach(function(tooth){
			tooth.addEventListener('mouseenter', function(){				
				this.style.fontWeight = 'bold';				
			});

			tooth.addEventListener('mouseleave', function(){
				this.style.fontWeight = 'normal';
			});

			tooth.addEventListener('click', function(){
				var num = this.getAttribute('data-num');
				
				initTooth(num);				
			});
		});
		
	}

	function splitData(str, sep) {
		return str.split(sep);
	}

	function showTutorial() {
		$.colorbox({
			html: '<div class="popup-content">Tutorial here!</div>',
			onComplete : function() {
				$(this).colorbox.resize();
			}
		});
	}

	function showFieldHelp(id) {
		return $.ajax({
			url: '/ajax/getData',
			type: 'post',
            dataType: 'json',
            headers: {
                'X-CSRF-TOKEN': '<?php echo e(csrf_token()); ?>'
            },
		    data: {action:'showFieldHelp', id: id},
		    success: function (data) {
		    	showPopup(data, true);
			}
		});
	}

	function showPrescriptionFieldContent(id) {
		var display = 'none';

		if ($(document).find('#select_prescription_'+id).val() == 'yes') {
			var display = 'flex';
		} else {
			$($(document).find('#inputs_prescription_'+id+' input[type=checkbox]')).each(function(i){
				$(this).prop('checked', false);
			});
		}

		$(document).find('#inputs_prescription_'+id).css('display', display);
	}

	function selectAllTooths(type, index, init) {
		if (index != active) return false;

		block = true;
		block_upper = false;
		block_lower = false;
		selected_option[index] = 3;

		$(d).find('#order_option_check'+index+'_1').removeClass('order-option-checkAct');
		$(d).find('#order_option_check'+index+'_2').removeClass('order-option-checkAct');
		$(d).find('#order_option_check'+index+'_4').removeClass('order-option-checkAct');
		$(d).find('#order_prescription_selected_option'+index).val(3);
		$(d).find('#order_prescription_price_for'+index).val(3);

		var start = 1;
		var end = 16;

		if (type == 1) {
			start = 17;
			end = 32;
		}

		if ($(d).find('#order_option_check'+index+'_3_'+type).hasClass('order-option-checkAct') && !init) {
			$(d).find('#order_option_check'+index+'_3_'+type).removeClass('order-option-checkAct');

			for (i = 1; i <= 32; i++) {
				if (i >= start && i <= end) {
					delete selected[index][i];
					delete statuses[index][i];
				}
			}
		} else {
			$(d).find('#order_option_check'+index+'_3_'+type).addClass('order-option-checkAct');

			for (i = 1; i <= 32; i++) {
				if (i >= start && i <= end) {
					statuses[index][i] = 2;

					var tooth = s('#tooth_'+i).getAttribute('data-tooth');

					statuses[index][i] && statuses[index][i] != 1 ? selected[index][i] = tooth+'_'+statuses[index][i]+'_'+s('#tooth_'+i+' text').innerHTML+'_'+selected_option[index] : delete selected[index][i];
				} else {
					if (selected[index][i]) {
						var tooth_data = splitData(selected[index][i], '_');

						if (!tooth_data || (tooth_data && tooth_data[3] && tooth_data[3] != 3)) {
							delete selected[index][i];
							delete statuses[index][i];
						}
					} else {
						delete selected[index][i];
						delete statuses[index][i];
					}
					
					
				}				
			}
		}

		rebuildSvg();
		showSelectedsText();

		s('#prescriptions_selecteds_'+index).value = selected[index].join(',');
	}

	function selectOneTooth(type, index, price_type) {
		if (index != active) return false;

		showActivePrescription(index);
		selected[index] = [];
		statuses[index] = [];

		block = false;
		selected_option[index] = type;	

		available_statuses[index] = [2,3,4];

		if (type == 4) {
			block_upper = false;
			block_lower = false;
			available_statuses[index] = [2,7];

			$(d).find('#order_option_check'+index+'_1').removeClass('order-option-checkAct');
		} else if (type == 5 && price_type) {
			available_statuses[index] = [2,7];

			$(d).find('#order_option_check'+index+'_1').removeClass('order-option-checkAct');
			$(d).find('#order_option_check'+index+'_4').removeClass('order-option-checkAct');

			select_type = price_type;
			price_type = 2;

			if (!$(d).find('#order_option_check'+index+'_5_'+select_type).hasClass('order-option-checkAct')) {		
				if (select_type == 1) {
					$(d).find('#order_option_check'+index+'_5_1').addClass('order-option-checkAct');
					$(d).find('#order_option_check'+index+'_5_2').removeClass('order-option-checkAct');

					block_lower = false;
					block_upper = true;
				} else {
					$(d).find('#order_option_check'+index+'_5_2').addClass('order-option-checkAct');
					$(d).find('#order_option_check'+index+'_5_1').removeClass('order-option-checkAct');

					block_upper = false;
					block_lower = true;
				}	
				
			}
		} else {
			block_upper = false;
			block_lower = false;
			$(d).find('#order_option_check'+index+'_4').removeClass('order-option-checkAct');
			$(d).find('#order_option_check'+index+'_5_1').removeClass('order-option-checkAct');
			$(d).find('#order_option_check'+index+'_5_2').removeClass('order-option-checkAct');
		}
		
		$(d).find('#order_option_check'+index+'_2').removeClass('order-option-checkAct');
		$(d).find('#order_option_check'+index+'_3_1').removeClass('order-option-checkAct');
		$(d).find('#order_option_check'+index+'_3_2').removeClass('order-option-checkAct');		
		$(d).find('#order_prescription_selected_option'+index).val(type);
		$(d).find('#order_prescription_price_for'+index).val(price_type);
		$(d).find('#order_option_check'+index+'_'+type).addClass('order-option-checkAct');

		rebuildSvg();
		showSelectedsText();
	}

	function selectBridge(index) {
		if (index != active) return false;

		showActivePrescription(index);
		selected[index] = [];
		statuses[index] = [];

		block = false;
		block_upper = false;
		block_lower = false;

		available_statuses[index] = [3,4,5,6,7];
		selected_option[index] = 2;	

		$(d).find('#order_option_check'+index+'_1').removeClass('order-option-checkAct');
		$(d).find('#order_option_check'+index+'_3_1').removeClass('order-option-checkAct');
		$(d).find('#order_option_check'+index+'_3_2').removeClass('order-option-checkAct');
		$(d).find('#order_option_check'+index+'_4').removeClass('order-option-checkAct');
		$(d).find('#order_prescription_selected_option'+index).val(2);
		$(d).find('#order_prescription_price_for'+index).val(1);

		$(d).find('#order_option_check'+index+'_2').addClass('order-option-checkAct');

		rebuildSvg();
		showSelectedsText();
	}	

	function setPrices() {
		for (i = 1; i <= selected.length; i++) {
			var start = false,
				price = 0,
				count = 0;

			if (selected[i]) {				
				var	price_for = parseInt(s('#order_prescription_price_for'+i).value),
					price_order_date = parseFloat(s('#order_prescription_price_order_date'+i).value);

				for (k = 1; k <= 32; k++) {
					if (start) count++;	

					if (selected[i][k]) {						
						var tooth_data = splitData(selected[i][k], '_');

						if (tooth_data && tooth_data[1]) {	
						    if (tooth_data[1] == 2 && !start) count++;
						    if (tooth_data[1] == 5) count++;
						    if (tooth_data[1] == 6) count++;

							if (tooth_data[1] == 3) {								
								start = k;
								count++;				
							}							

							if (tooth_data[1] == 4) {								
								start = false;
							}
						}
					}
				}								

				//One price for all selected
				if (price_for == 2 && count != 0) count = 1;
				//Full denture - half jaw selected
				if (price_for == 3 && count != 0 && count <= 16) count = 1;
				//Full denture - all jaw selected
				if (price_for == 3 && count != 0 && count > 16) count = 2;
				if (price_for == 4 && count == 0) count = 1;

				price = price_order_date * count;

				if (price < 0) price = 0;

				s('#order_prescription_price'+i).value = price.toFixed(2);
			}
		}

		setTotalPrice();
	}

	function setTotalPrice() {
		var prices = d.querySelectorAll('.order-prescription-price-input');
		var total = 0;

		prices.forEach(function(el){
			var price = parseFloat(el.value);

			total = total + price;
		});

		if (total < 0) total = 0;

		s('#order_prescription_total').innerHTML = total.toFixed(2);
		s('#advance_price').value = total.toFixed(2);
	}

	function showSelectedsText() {
		if (statuses[active]) {			
			var text = [];
			var lined = false;
			var lined2 = false;
			var need_more = false;

			for(i = 1; i <= 32; i++) {
				var index = i;

				if (!statuses[active][i]) {					
					continue;
				}

				if (statuses[active][i] == 2) {
					if (selected[active][i]) {	
						var tooth_data = splitData(selected[active][i], '_');

						if (tooth_data && tooth_data[3] && tooth_data[3] == 3) {
							//show all selected with '-' for Full denture
							if (!statuses[active][i - 1]) {
								lined2 = s('#tooth_'+index+' text').innerHTML + '-';
							} else if (!statuses[active][i + 1]) {
								lined2 += s('#tooth_'+index+' text').innerHTML;
								lined2 = lined2.replace('false', '?-');
								text[i] = lined2;
								lined2 = false;
							}
						} else if (tooth_data && tooth_data[3] && tooth_data[3] == 4) {
							//Partial denture can be single or group selected. Show group selected with '-'
							if ((!statuses[active][i - 1] || (statuses[active][i - 1] && statuses[active][i - 1] == 1)) && statuses[active][i + 1] && statuses[active][i + 1] != 1) {
								lined2 = s('#tooth_'+index+' text').innerHTML + '-';
							} else if (statuses[active][i - 1] && statuses[active][i - 1] !=1  && (!statuses[active][i + 1] || (statuses[active][i + 1] && statuses[active][i + 1] == 1))) {
								lined2 += s('#tooth_'+index+' text').innerHTML;
								lined2 = lined2.replace('false', '?-');
								text[i] = lined2;
								lined2 = false;
							} else if (lined || lined2){
								continue;
							} else {
								text[i] = s('#tooth_'+index+' text').innerHTML;	
								lined2 = false;
							}
						} else {
							text[i] = s('#tooth_'+index+' text').innerHTML;	
						}
					} else {
						text[i] = s('#tooth_'+index+' text').innerHTML;	

					}								
				}

				//prevent other events for Partial denture
				if (selected_option[active] == 4) continue;

				//if cantileaver before bridge start - start from cantileaver
				if (statuses[active][i] == 5) {
                    if (statuses[active][i - 1] && statuses[active][i - 1] == 5) {
                        //continue if 2 cantileaver one by one
                    } else {
                        lined = s('#tooth_'+index+' text').innerHTML + '-';
                        need_more = true;
                    }
                }

                //bridge start
				if (statuses[active][i] == 3) {
					if (!need_more) lined = s('#tooth_'+index+' text').innerHTML + '-';
				}

				//bridge end
				if (statuses[active][i] == 4) {
					if (statuses[active][i + 1] && statuses[active][i + 1] == 6) {
						need_more = true;
					} else {
						need_more = false;
					}

					if (!need_more) {
						lined += s('#tooth_'+index+' text').innerHTML;
						text[i] = lined.replace('false', '?-');
						lined = false;
					}
				}

				//if cantileaver after bridge end - end at cantileaver
				if (statuses[active][i] == 6) {
                    if (statuses[active][i + 1] && statuses[active][i + 1] == 6) {
                    	//continue if 2 cantileaver one by one
                        need_more = true;
                    } else {
                        lined += s('#tooth_'+index+' text').innerHTML;
						text[i] = lined.replace('false', '?-');
						lined = false;
                        need_more = false;
                    }
                }

			}
	
			s('#prescriptions_selecteds_text_'+active).innerHTML = text.filter(Boolean).join(', ');
		}
	}

	function checkOrderFormCustomFields(min_days) {
		min_days = parseInt(min_days);

		var max_days = [];
		var ship_date = new Date(($('#ship_date').val()).replace(/-/g, "/"));
		var current_date = new Date();			
		var diff_date = Math.ceil((ship_date - current_date) / 1000 / 60 / 60 / 24);

		if (!diff_date) diff_date = 0;

		if (diff_date < min_days) {
		    var available_date = current_date.setDate(current_date.getDate() + min_days);
			var d = new Date(available_date);				
			var day = d.getDate() > 9 ? d.getDate() : '0' + d.getDate();
			var month = (d.getMonth() + 1) > 9 ? (d.getMonth() + 1) : '0' + (d.getMonth() + 1);
			var year = d.getFullYear();
			var text = '<?php echo e(__('translations.select_other_checkout_date')); ?>'; 

			d = month + '-' + day + '-' + year;

			text = text.replace('%s', d);
		
			showPopup(text);
		}
	}
</script><?php /**PATH /var/www/dentist2lab.com/resources/views/Frontend/templates/checkout_scripts.blade.php ENDPATH**/ ?>