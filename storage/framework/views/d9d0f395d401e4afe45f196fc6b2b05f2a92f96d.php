<div class="fx fac fsb">
	<div class="logo-block">
		<div class="bi logo" style="background-image: url('/img/logo.png')">
			<?php if(url()->current() != url('/')): ?>
				<a href="<?php echo e(url('/')); ?>"></a>
			<?php endif; ?>
		</div>
		<div class="logo-tagline"><?php echo e(__('translations.logo_tagline')); ?></div>		
	</div>
	<div class="fx fxb fac auth-block">
		<?php if(Auth::user()->role_id == config('roles.guest')): ?>
			<div class="auth-block-upgrade">
				<a class="btn btn-danger" href="<?php echo e(route('settings')); ?>"><?php echo e(__('translations.upgrade')); ?></a>
			</div>
		<?php endif; ?>
	    <div class="auth-block-name">
			<a href="<?php echo e(route('account')); ?>">
				<?php echo e(Auth::user()->name); ?>

				<div><?php echo e(\App\Models\Role::where('id', Auth::user()->role_id)->first()->display_name); ?></div>
			</a>
	    </div>		
		<div class="fx fxc auth-block-logo">
			<a href="<?php echo e(route('account')); ?>">
				<img src="<?php echo e(\App\Services\Img::getIcon(Auth::user()->avatar)); ?>">
			</a>
		</div>	
		<div class="auth-block-logout">		
			<a href="<?php echo e(route('logout')); ?>"><?php echo e(__('translations.logout')); ?></a>	
		</div>	
	</div>
</div><?php /**PATH /var/www/d2l.sitepark.ua/resources/views/Frontend/parts/account_header.blade.php ENDPATH**/ ?>