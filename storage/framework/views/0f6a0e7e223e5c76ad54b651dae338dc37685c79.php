<?php $__env->startSection('css'); ?>
	<link rel="stylesheet" href="<?php echo e(mix('/css/catalog.css')); ?>">
	<link rel="stylesheet" href="/css/select.css">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('after_head'); ?>
	<div class="catalog-top">
		<form method="GET" action="<?php echo e(route('catalog.industry.type', [$industry->slug, $type->slug, $requests])); ?>" id="catalog_filter">
			<div class="catalog-top-bottom">
				<div class="fx fxb content" style="position:relative;">					
					<div class="catalog-top-all">
						<?php echo e($total.' '.ucfirst($type->catalog_name)); ?>

					</div>
					<div class="fx catalog-top-inputs">
						<!--
						<div class="catalog-input-line catalog-top-industry">
							<span class="catalog-input-label"><?php echo e(__('translations.industry')); ?>:</span>
							<select class="form-input" id="industry" onchange="showOtherIndustry()">
								<option value="<?php echo e($industry->slug); ?>" default><?php echo e($industry->name); ?></option>
							</select>
						</div>
						-->
						<div class="catalog-input-line catalog-top-zip">
							<span class="catalog-input-label"><?php echo e(__('translations.zip_code')); ?>:</span>
							<input type="text" name="zip_code" id="zip_code" class="form-input" value="<?php if($selected_filters['zip_code'] && $selected_filters['zip_code'] != ''): ?><?php echo e($selected_filters['zip_code']); ?><?php else: ?><?php echo e(''); ?><?php endif; ?>" onchange="document.getElementById('catalog_filter').submit()" autocomplete="off">	
						</div>
						<?php if(!$selected_filters['zip_code']): ?>
						<div class="catalog-input-line catalog-top-locality">
							<span class="catalog-input-label"><?php echo e(__('translations.city')); ?>:</span>
							<select class="form-input" name="locality_id" id="locality_id" onchange="changeLocality()">
								<?php if($locality): ?>
									<option value="<?php echo e($locality->id); ?>" default><?php echo e($locality->name); ?></option>
								<?php endif; ?>
								<option value="0"> --- </option>
							</select>
						</div>
						<?php endif; ?>
						<div class="catalog-input-line catalog-top-filter">
							<?php
								$selected_filters_count = 0;
								
								foreach ($selected_filters as $key => $value) {
									if ($key != 'sort' && $value) $selected_filters_count++;
								}
							?>
							<div id="show_catalog_filter" class="catalog-top-filterBtn">
								<?php echo e(__('translations.filter')); ?>

								<?php if($selected_filters_count != 0): ?>
									<span>(<?php echo e($selected_filters_count .' '. __('translations.selected')); ?>)</span>
								<?php endif; ?>
							</div>							
						</div>
						
					</div>	
					<div id="filter">
						<div class="filter-head"><?php echo e(__('translations.filters')); ?></div>
						<div class="fx catalog-inputs-line">
							<div class="catalog-input-line catalog-top-search">
								<span class="catalog-input-label"><?php echo e(__('translations.search')); ?>:</span>
								<input type="text" name="search" id="catalog_search" class="form-input" value="<?php if($selected_filters['search'] && $selected_filters['search'] != ''): ?><?php echo e($selected_filters['search']); ?><?php else: ?><?php echo e(''); ?><?php endif; ?>" autocomplete="off">
							</div>
							<div class="catalog-input-line catalog-top-category">
								<span class="catalog-input-label"><?php echo e(__('translations.category')); ?>:</span>
								<select class="form-input" name="service_id" id="service_id">
									<option value="0"> --- </option>
									<?php $__currentLoopData = $services; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $service): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
										<option value="<?php echo e($service->id); ?>" <?php if($selected_filters['service'] && $selected_filters['service'] == $service->id): ?><?php echo e('selected'); ?><?php endif; ?>><?php echo e($service->name); ?></option>
									<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
								</select>
							</div>	
							<div class="catalog-input-line catalog-top-options">
								<span class="catalog-input-label"><?php echo e(__('translations.options')); ?>:</span>
								<select class="form-input" name="options" id="option_input">
									<option value="0"> --- </option>
									<?php $__currentLoopData = $options; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $option): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
										<option value="<?php echo e($option->id); ?>" <?php if($selected_filters['options'] && $selected_filters['options'] == $option->id): ?><?php echo e('selected'); ?><?php endif; ?>><?php echo e($option->name); ?></option>
									<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
								</select>
							</div>					
							<div class="catalog-input-line catalog-top-sort">
								<span class="catalog-input-label"><?php echo e(__('translations.sort_by')); ?>:</span>
								<select class="form-input" name="sort" id="sort_input">
									<option value="rating_desc" <?php if($selected_filters['sort'] && $selected_filters['sort'] == 'rating_desc'): ?><?php echo e('selected'); ?><?php endif; ?>><?php echo e(__('translations.rating')); ?> - Top</option>
									<option value="rating_asc" <?php if($selected_filters['sort'] && $selected_filters['sort'] == 'rating_asc'): ?><?php echo e('selected'); ?><?php endif; ?>><?php echo e(__('translations.rating')); ?> - End</option>
									<option value="name_asc" <?php if($selected_filters['sort'] && $selected_filters['sort'] == 'name_asc'): ?><?php echo e('selected'); ?><?php endif; ?>><?php echo e(__('translations.name')); ?> A to Z</option>
									<option value="name_desc" <?php if($selected_filters['sort'] && $selected_filters['sort'] == 'name_desc'): ?><?php echo e('selected'); ?><?php endif; ?>><?php echo e(__('translations.name')); ?> Z to A</option>
								</select>
							</div>
						</div>
						<?php if(Auth::check()): ?>							
							<div class="fx fxb catalog-inputs-line">								
								<div class="catalog-input-line">
									<span class="catalog-input-label"><?php echo e(__('translations.price') .' '. __('translations.from')); ?>:</span>
									<input type="text" name="price_from" id="price_from" class="form-input" value="<?php if($selected_filters['price_from'] && $selected_filters['price_from'] != ''): ?><?php echo e($selected_filters['price_from']); ?><?php else: ?><?php echo e(''); ?><?php endif; ?>" autocomplete="off" onchange="isNaN(this.value) ? this.value = null : this.value = this.value;">
									<span class="catalog-input-symbol"><?php echo e($currency->symbol); ?></span>
								</div>
								<div class="catalog-input-line">
									<span class="catalog-input-label"><?php echo e(__('translations.price') .' '. __('translations.to')); ?>:</span>
									<input type="text" name="price_to" id="price_to" class="form-input" value="<?php if($selected_filters['price_to'] && $selected_filters['price_to'] != ''): ?><?php echo e($selected_filters['price_to']); ?><?php else: ?><?php echo e(''); ?><?php endif; ?>" autocomplete="off" onchange="isNaN(this.value) ? this.value = null : this.value = this.value;">
									<span class="catalog-input-symbol"><?php echo e($currency->symbol); ?></span>
								</div>												
								<div class="catalog-input-line">
									<span class="catalog-input-label"><?php echo e(__('translations.rating') .' '. __('translations.from')); ?>:</span>
									<select class="form-input" name="rating_from" id="rating_from">
										<option value="0" <?php if($selected_filters['rating_from'] && $selected_filters['rating_from'] == 0): ?><?php echo e('selected'); ?><?php endif; ?> default><?php echo e(__('translations.none')); ?></option>
										<option value="1" <?php if($selected_filters['rating_from'] && $selected_filters['rating_from'] == 1): ?><?php echo e('selected'); ?><?php endif; ?>>&#8902;</option>
										<option value="2" <?php if($selected_filters['rating_from'] && $selected_filters['rating_from'] == 2): ?><?php echo e('selected'); ?><?php endif; ?>>&#8902; &#8902;</option>
										<option value="3" <?php if($selected_filters['rating_from'] && $selected_filters['rating_from'] == 3): ?><?php echo e('selected'); ?><?php endif; ?>>&#8902; &#8902; &#8902;</option>
										<option value="4" <?php if($selected_filters['rating_from'] && $selected_filters['rating_from'] == 4): ?><?php echo e('selected'); ?><?php endif; ?>>&#8902; &#8902; &#8902; &#8902;</option>
										<option value="5" <?php if($selected_filters['rating_from'] && $selected_filters['rating_from'] == 5): ?><?php echo e('selected'); ?><?php endif; ?>>&#8902; &#8902; &#8902; &#8902; &#8902;</option>
									</select>
								</div>
								<div class="catalog-input-line">
									<span class="catalog-input-label"><?php echo e(__('translations.rating') .' '. __('translations.to')); ?>:</span>
									<select class="form-input" name="rating_to" id="rating_to">
										<option value="5" <?php if($selected_filters['rating_to'] && $selected_filters['rating_to'] == 5): ?><?php echo e('selected'); ?><?php endif; ?> default>&#8902; &#8902; &#8902; &#8902; &#8902;</option>
										<option value="4" <?php if($selected_filters['rating_to'] && $selected_filters['rating_to'] == 4): ?><?php echo e('selected'); ?><?php endif; ?>>&#8902; &#8902; &#8902; &#8902;</option>
										<option value="3" <?php if($selected_filters['rating_to'] && $selected_filters['rating_to'] == 3): ?><?php echo e('selected'); ?><?php endif; ?>>&#8902; &#8902; &#8902;</option>
										<option value="2" <?php if($selected_filters['rating_to'] && $selected_filters['rating_to'] == 2): ?><?php echo e('selected'); ?><?php endif; ?>>&#8902; &#8902;</option>									
										<option value="1" <?php if($selected_filters['rating_to'] && $selected_filters['rating_to'] == 1): ?><?php echo e('selected'); ?><?php endif; ?>>&#8902;</option>
									</select>
								</div>
							</div>							
						<?php endif; ?>
						<div class="filter-btn-line">	
							<input type="submit" class="btn" value="<?php echo e(__('translations.filter_btn')); ?>"/>
							&nbsp;
							<a class="btn btn-primary" href="<?php echo e(route('catalog.industry.type', [$industry->slug, $type->slug])); ?>"><?php echo e(__('translations.clear_filter')); ?></a>
						</div>
					</div>
				</div>
			</div>
			<?php echo e(csrf_field()); ?>

		</form>		
	</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('breadcrumbs'); ?>
	<?php echo $__env->make('Frontend.templates.breadcrumbs', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="fx fxb catalog-headline">
    	<h1><?php echo e($seo->name); ?></h1>
    	<?php if(count($data)): ?>
			<div class="catalog-show-allOnMap">
				<div onclick="showAllOnMap()"><?php echo e(__('translations.view_all_on_map')); ?></div>
			</div>
		<?php endif; ?>	
    </div>	
	<div class="page-text">
		<?php if(count($data)): ?>
			<div class="catalog-items">
				<?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
					<?php 
						$href = route('branch', [$type->slug, $item->id]);
					?>
					<div class="fx fxb catalog-item">
						<div class="catalog-item-left">
							<div class="catalog-item-logo" style="background-image: url(<?php echo e(App\Services\Img::resizeImage($item->logo, 235, 235)); ?>)">
								<a href="<?php echo e($href); ?>"></a>
							</div>
							<div class="catalog-item-link">
								<a href="<?php echo e($href); ?>"><?php echo e(__('translations.view_profile')); ?></a>
							</div>
						</div>
						<div class="catalog-item-right">
							<div class="fx fxb catalog-item-info">
								<div class="catalog-item-infoLeft">
									<div class="catalog-item-infoName">
										<a href="<?php echo e($href); ?>"><?php echo e($item->name); ?></a>
									</div>
									<!--<div class="catalog-item-infoIndustry">
										<?php echo e(__('translations.industry')); ?>: <?php echo e($industry->name); ?>

									</div> -->
									<?php if($item->lat && $item->lon): ?>
									<div class="catalog-item-infoMap">
										<div id="item_map<?php echo e($item->id); ?>" onclick="showOnMap('item_map<?php echo e($item->id); ?>')" data-lat="<?php echo e($item->lat); ?>" data-lon="<?php echo e($item->lon); ?>"><?php echo e(__('translations.view_on_map')); ?></div>
									</div>
									<?php endif; ?>
								</div>
								<div class="fx fxb catalog-item-infoRigth">
								<?php if(Auth::check()): ?>									
									<div class="catalog-item-infoReviews">
										<div class="catalog-item-infoStars catalog-item-infoStars<?php echo e(floor($item->reviews_rating)); ?>"></div>
										<?php echo e(count($item->reviews) .' '. __('translations.reviews')); ?>

									</div>									
									<div class="catalog-item-infoRating">
										<?php echo e(__('translations.rating')); ?><span><?php echo e($item->rating); ?></span>
									</div>
								<?php else: ?>
									<div class="bi catalog-itemRatingMuted" onclick="showRegAlert();"></div>							
								<?php endif; ?>
								</div>
							</div>
							<?php if($item->prices && count($item->prices)): ?>
								<div class="catalog-item-prices">
									<div class="catalog-item-pricesHead"><?php echo e(__('translations.prices')); ?>:</div>
									<div class="catalog-item-pricesBlock">
										<?php 
											$cnt = 0;											
										?>
										<?php $__currentLoopData = $item->prices; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $price): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
											<div class="fx fxb catalog-item-priceLine">
												<div class="catalog-item-priceName"><span><?php echo e($price->name); ?></span></div>
												<div class="catalog-item-priceValue"><?php echo e($price->value); ?></div>
											</div>
											<?php 
												$cnt++;
												if($cnt == 4) break; 
											?>
										<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
									</div>
									<?php if(count($item->prices) > 4): ?>
										<div class="catalog-item-pricesLink">
											<a href="<?php echo e($href); ?>#prices"><?php echo e(__('translations.more_prices')); ?></a>
										</div>
									<?php endif; ?>
								</div>
							<?php endif; ?>
						</div>
					</div>
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
			</div>
			<div class="pagination-block"><?php echo e($data->appends(\Request::except('page'))->links()); ?></div>
		<?php else: ?>
			<p><?php echo e(__('translations.no_catalog_results')); ?></p>
			<!--<div class="btn" onclick="window.history.back()"><?php echo e(__('translations.back')); ?></a> -->
		<?php endif; ?>
	</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('pre_footer'); ?>
	<?php if(!Auth::check()): ?>
	<div class="bootom-content">
		<div class="bi bottom-banner">
			<div class="content">
				<div class="fx fxb">
					<div class="fx fxc bottom-banner-question"><?php echo e(__('translations.bottom_question')); ?></div>
					<div class="fx fxc bottom-banner-answer">
						<a href="<?php echo e(route('login')); ?>" class="btn btn-primary"><?php echo e(__('translations.sign_up')); ?></a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php endif; ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
	<script src="/js/select.js" defer></script>
	<script src="https://maps.googleapis.com/maps/api/js?key=<?php echo e(setting('admin.google_api_maps_key')); ?>"
    async defer></script>
    <script src="/js/masked.js" defer></script>
	<script>
		document.addEventListener('DOMContentLoaded', function() {
			$('#zip_code').mask('99999');

			$('#industry').select2({
				minimumInputLength: 2,
				ajax: {
					url: '/ajax/getData',
					type: 'post',
	                dataType: 'json',
	                headers: {
	                    'X-CSRF-TOKEN': '<?php echo e(csrf_token()); ?>'
	                },
				    data:function(params) {
				      return {
				      	action: 'getIndustriesToSelect',
				        search: params.term,
				      };
				    },
					processResults: function (data) {
						return {
							results: data
						};
					}
				}
			});

			$('#service_id').select2({
				minimumInputLength: 0,
				ajax: {
					url: '/ajax/getData',
					type: 'post',
	                dataType: 'json',
	                headers: {
	                    'X-CSRF-TOKEN': '<?php echo e(csrf_token()); ?>'
	                },
				    data:function(params) {
				      return {
				      	action: 'getServicesByIndustryToSelect',
				        search: params.term,
				        industry: <?php echo e($industry->id); ?>

				      };
				    },
					processResults: function (data) {
						return {
							results: data
						};
					}
				}
			});

			$('#option_input').select2({
				minimumInputLength: 0,
				ajax: {
					url: '/ajax/getData',
					type: 'post',
	                dataType: 'json',
	                headers: {
	                    'X-CSRF-TOKEN': '<?php echo e(csrf_token()); ?>'
	                },
				    data:function(params) {
				      return {
				      	action: 'getOptionsByIndustryToSelect',
				        search: params.term,
				        industry: <?php echo e($industry->id); ?>

				      };
				    },
					processResults: function (data) {
						return {
							results: data
						};
					}
				}
			});

			$('#locality_id').select2({
				minimumInputLength: 0,
				ajax: {
					url: '/ajax/getData',
					type: 'post',
	                dataType: 'json',
	                headers: {
	                    'X-CSRF-TOKEN': '<?php echo e(csrf_token()); ?>'
	                },
				    data:function(params) {
				      return {
				      	action: 'getLocalitiesToSelect',
				        search: params.term
				      };
				    },
					processResults: function (data) {
						return {
							results: data
						};
					}
				}
			});

			$('#sort_input').select2({
				minimumResultsForSearch: -1
			});

			$('#rating_from').select2({
				minimumResultsForSearch: -1
			});

			$('#rating_to').select2({
				minimumResultsForSearch: -1
			});

			$('#show_catalog_filter').on('click', function() {
				$(this).toggleClass('catalog-top-filterBtnOp');
				$('#filter').slideToggle();
			});
		}, !1);

		function showOnMap(id) {
			<?php if(Auth::check()): ?>		
				var lat = $('#'+id).data('lat'),
					lon = $('#'+id).data('lon');

				$.colorbox({
					html: '<div id="map"></div>',
					onComplete : function() {
						$(this).colorbox.resize();

						var position = {lat: lat, lng: lon};

						var mapOptions = {
						    center: position,
						    zoom: 15,
						    mapTypeId: google.maps.MapTypeId.ROADMAP
						};

						var map = new google.maps.Map(document.getElementById('map'), mapOptions);
						var marker = new google.maps.Marker({position: position, map: map});
					}
				});
			<?php else: ?> 
				showRegAlert();
			<?php endif; ?>
		}

		function showAllOnMap() {
			<?php if(Auth::check()): ?>
				$.ajax({
					url: '/ajax/getData',
					type: 'post',
	                dataType: 'json',
	                headers: {
	                    'X-CSRF-TOKEN': '<?php echo e(csrf_token()); ?>'
	                },
				    data: {action:'getGeoLocationsById', ids: '<?php echo e($geolocations); ?>'},
				    success: function(data) {
				    	if (data) {
							$.colorbox({
								html: '<div id="map"></div>',
								onComplete : function() {
									$(this).colorbox.resize();

									var position = {lat: parseFloat(data[0]['lat']), lng: parseFloat(data[0]['lon'])};

									var mapOptions = {
									    center: position,
									    zoom: 9,
									    mapTypeId: google.maps.MapTypeId.ROADMAP
									};

									var map = new google.maps.Map(document.getElementById('map'), mapOptions);

									for(index in data) {
										var marker_position = {lat: parseFloat(data[index]['lat']), lng: parseFloat(data[index]['lon'])};

										new google.maps.Marker({position: marker_position, map: map});
									}
									
								}
							});
						}
					}
				});
			<?php else: ?> 
				showRegAlert();
			<?php endif; ?>
		}

		function showRegAlert() {
			showPopup('<div class="error"><?php echo __('translations.only_reg_view'); ?></div>');
		}

		function showOtherIndustry() {
			var href = "<?php echo e(route('catalog.industry.type', [$industry->slug, $type->slug])); ?>",
				value = document.getElementById('industry').value;

			href = href.replace('<?php echo e($industry->slug); ?>', value);
			window.location.href = href;
		}

		function changeLocality() {
			$.ajax({
				url: '/ajax/getData',
				type: 'post',
                dataType: 'json',
                headers: {
                    'X-CSRF-TOKEN': '<?php echo e(csrf_token()); ?>'
                },
			    data: {action:'setSession', method:'add', name:'location', value: $('#locality_id').val()},
			    success: function(data) {
			    	$('#catalog_filter').submit()
			    }
			});
		}
	</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('Frontend/master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/dentist2lab.com/resources/views/Frontend/pages/catalog/type.blade.php ENDPATH**/ ?>