<?php $__env->startSection('css'); ?>
	<link rel="stylesheet" href="<?php echo e(mix('/css/home.css')); ?>">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('after_head'); ?>
	<div class="bi main-top">
		<div class="fx fxb content">
			<div class="main-top-left">
				<div class="main-top-head">
					<?php echo e(__('translations.see_how')); ?>

				</div>
				<div class="main-top-content">
					<iframe width="100%" height="315" src="https://www.youtube.com/embed/sOlP10fDclE" frameborder="0" allow="accelerometer; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
				</div>
			</div>
			<div class="fx fxc main-top-right">
				<div class="main-top-head">
					<?php echo e(__('translations.start_free')); ?>

				</div>
				<div class="main-top-content">
					<a href="<?php echo e(url('registration?type=3')); ?>" class="bi btn"><?php echo e(__('translations.reg_type3')); ?></a>
					<a href="<?php echo e(url('registration?type=2')); ?>" class="bi btn btn-primary"><?php echo e(__('translations.reg_type2')); ?></a>
				</div>
			</div>
		</div>
	</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
	<div class="main-introtext">
		<div class="main-introtext-text">
			<?php echo __('translations.introtext'); ?>

		</div>
		<div class="main-introtext-link">
			<a href="<?php echo e(url('catalog/dentistry/labs')); ?>" class="btn"><?php echo e(__('translations.view_all_labs')); ?></a>
		</div>
	</div>
	<div class="main-counters">
		<div class="main-counters-head">
			<?php echo e(__('translations.current_users')); ?>

		</div>
		<div class="fx fxb main-counters-items">
			<div class="main-counters-item main-counters-item1">
				<div class="bi fx fac main-counters-itemImg">+<?php echo e($registered); ?></div>
				<div class="main-counters-itemTitle"><?php echo e(__('translations.users_reg')); ?></div>
			</div>
			<div class="main-counters-item main-counters-item2">
				<div class="bi fx fac main-counters-itemImg">+<?php echo e($clinics); ?></div>
				<div class="main-counters-itemTitle"><?php echo e(__('translations.clinics')); ?></div>
			</div>
			<div class="main-counters-item main-counters-item3">
				<div class="bi fx fac main-counters-itemImg">+<?php echo e($labs); ?></div>
				<div class="main-counters-itemTitle"><?php echo e(__('translations.labs')); ?></div>
			</div>
		</div>
	</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('Frontend/master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/d2l.sitepark.ua/resources/views/Frontend/pages/home.blade.php ENDPATH**/ ?>