<?php $__env->startSection('page_title', __('voyager::generic.viewing').' '.ucfirst($type)); ?>

<?php $__env->startSection('page_header'); ?>
    <h1 class="page-title">
        <i class="voyager-list-add"></i> <?php echo e(ucfirst($type)); ?>        
    </h1>
<?php $__env->stopSection(); ?>
 
<?php $__env->startSection('content'); ?> 
    <div class="page-content container-fluid">
        <?php echo $__env->make('voyager::alerts', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-body">
                        <form method="get" action="<?php echo e(route('voyager.'.$type.'.index', $requests)); ?>">
                            <div class="col-sm-10">
                                <div class="row">                      
                                    <div class="col-sm-12">                                              
                                        <div class="col-md-3">
                                            <input type="text" class="form-control" name="search" value="<?php if(isset($filter['filter_search']) && $filter['filter_search'] != ''): ?><?php echo e($filter['filter_search']); ?><?php endif; ?>" placeholder="Search by Branch name">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                                        
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <input type="submit" style="margin:0;" class="btn btn-success" value="OK">
                                <a class="btn btn-danger" style="margin:0;" href="<?php echo e(route('voyager.'.$type.'.index')); ?>">Clear</a>
                            </div>
                        </form>
                        <table id="dataTable" class="table table-hover">
                            <thead>
                            <tr>                                
                                <th>ID</th> 
                                <th>Image</th>
                                <th>Service group</th> 
                                <th>Branch</th>                            
                                <th class="actions text-right"><?php echo e(__('voyager::generic.actions')); ?></th>
                            </tr>
                            </thead>
                            <tbody>
                                <?php $__currentLoopData = $many; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $one): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                    <td><?php echo e($one->id); ?></td>                                    
                                    <td><img src="<?php echo e(App\Services\Img::getIcon($one->image)); ?>" style="width:50px; height:auto; clear:both; display:block; padding:2px;" /></td>
                                    <td><?php echo e($one->service->name); ?></td>
                                    <td><a href="<?php echo e(route('voyager.branches.edit', $one->branch->id)); ?>"><?php echo e($one->branch->name); ?></a></td>                                    
                                    <td class="no-sort no-click bread-actions">
                                        <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('delete', $model)): ?>
                                            <div class="btn btn-sm btn-danger pull-right delete" data-id="<?php echo e($one->id); ?>">
                                                <i class="voyager-trash"></i> <?php echo e(__('voyager::generic.delete')); ?>

                                            </div>
                                        <?php endif; ?>                                                                            
                                    </td>
                                </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="panel-body">                        
                        <div class="pull-right"><?php echo e($many->appends(\Request::except('page'))->links()); ?></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal modal-danger fade" tabindex="-1" id="delete_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="<?php echo e(__('voyager::generic.close')); ?>">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">
                        <i class="voyager-trash"></i> <?php echo e(__('voyager::generic.delete_question')); ?>?
                    </h4>
                </div>
                <div class="modal-footer">
                    <form action="#" id="delete_form" method="POST">
                        <?php echo e(method_field("DELETE")); ?>

                        <?php echo e(csrf_field()); ?>

                        <input type="submit" class="btn btn-danger pull-right delete-confirm" value="<?php echo e(__('voyager::generic.delete_this_confirm')); ?>">
                    </form>
                    <button type="button" class="btn btn-default pull-right" data-dismiss="modal"><?php echo e(__('voyager::generic.cancel')); ?></button>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>   
    <script> 
        $('td').on('click', '.delete', function (e) {
            $('#delete_form')[0].action = '<?php echo e(route('voyager.'.$type.'.destroy', ['id' => '__item', $requests])); ?>'.replace('__item', $(this).data('id'));

            $('#delete_modal').modal('show');
        });
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('voyager::master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/dentist2lab.com/resources/views/Backend/portfolios/browse.blade.php ENDPATH**/ ?>