<?php $__env->startSection('page_title', __('voyager::generic.viewing').' '.ucfirst($type)); ?>

<?php $__env->startSection('page_header'); ?>
    <h1 class="page-title">
        <i class="voyager-list-add"></i> <?php echo e(ucfirst($type)); ?>        
    </h1>
<?php $__env->stopSection(); ?>
 
<?php $__env->startSection('content'); ?> 
    <div class="page-content container-fluid">
        <?php echo $__env->make('voyager::alerts', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-body">
                        <form method="get" action="<?php echo e(route('voyager.'.$type.'.index', $requests)); ?>">
                            <div class="col-sm-10">
                                <div class="row">                      
                                    <div class="col-sm-12">                                        
                                        <div class="col-md-3">
                                            <select onchange="$(this).parents('form').submit()" class="form-control" name="status">
                                                <option value="0" default><?php echo e(__('adm.select_status')); ?></option>
                                                <?php $__currentLoopData = $statuses; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $status): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <option value="<?php echo e($status['id']); ?>" <?php if(isset($filter['filter_status']) && $filter['filter_status'] == $status['id']): ?> selected <?php endif; ?>><?php echo e($status['name']); ?></option>       
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>    
                                            </select>
                                        </div>
                                        <div class="col-md-3">
                                            <select onchange="$(this).parents('form').submit()" class="form-control" name="type">
                                                <option value="0" default><?php echo e(__('voyager::generic.type')); ?></option>
                                                <?php $__currentLoopData = $types; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $type_item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <option value="<?php echo e($type_item['id']); ?>" <?php if(isset($filter['filter_type']) && $filter['filter_type'] == $type_item['id']): ?> selected <?php endif; ?>><?php echo e($type_item['name']); ?></option>       
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>    
                                            </select>
                                        </div>
                                        <div class="col-md-3">
                                            <select onchange="$(this).parents('form').submit()" class="form-control" name="role_id">
                                                <option value="0" default><?php echo e(__('adm.role')); ?></option>
                                                <?php $__currentLoopData = $roles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $role): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <option value="<?php echo e($role['id']); ?>" <?php if(isset($filter['filter_role']) && $filter['filter_role'] == $role['id']): ?> selected <?php endif; ?>><?php echo e($role['display_name']); ?></option>       
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>    
                                            </select>
                                        </div>                                         
                                        <div class="col-md-3">
                                            <select onchange="$(this).parents('form').submit()" class="form-control" id="branch_id" name="branch_id">                                                
                                                <?php if(isset($filter['filter_branch']) && $filter['filter_branch'] && $branch): ?> 
                                                    <option value="<?php echo e($branch->id); ?>" selected><?php echo e($branch->name); ?></option>
                                                <?php else: ?>
                                                    <option value="0">Department</option>
                                                <?php endif; ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="col-md-3">
                                            <input type="text" class="form-control" name="search" value="<?php if(isset($filter['filter_search']) && $filter['filter_search'] != ''): ?><?php echo e($filter['filter_search']); ?><?php endif; ?>" placeholder="<?php echo e(__('voyager::generic.search')); ?>">
                                        </div>              
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <input type="submit" style="margin:0;" class="btn btn-success" value="OK">
                                <a class="btn btn-danger" style="margin:0;" href="<?php echo e(route('voyager.'.$type.'.index')); ?>">Clear</a>
                            </div>
                        </form>
                        <table id="dataTable" class="table table-hover">
                            <thead>
                            <tr>                                
                                <th>ID</th> 
                                <th>Name</th> 
                                <th>E-mail</th>
                                <th>Type</th>
                                <th>Role</th>
                                <th>Image</th>
                                <th>Last visit from IP</th>
                                <th>Status</th>                              
                                <th class="actions text-right"><?php echo e(__('voyager::generic.actions')); ?></th>
                            </tr>
                            </thead>
                            <tbody>
                                <?php $__currentLoopData = $many; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $one): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr style="font-weight:400;color:#000;background-color:<?php echo e($one->status_info['color']); ?>">
                                    <td><?php echo e($one->id); ?></td>
                                    <td><?php echo e($one->name.' '.$one->surname); ?></td>
                                    <td><?php echo e($one->email); ?></td>
                                    <td><?php echo e($one->type_info['name']); ?></td>
                                    <td><?php echo e($one->role_info['display_name']); ?></td>
                                    <td>
                                        <img src="<?php echo e(filter_var($one->avatar, FILTER_VALIDATE_URL) ? $one->avatar : Voyager::image( $one->avatar )); ?>" style="width:70px; height:auto; clear:both; display:block; padding:2px;border-radius: 50%;" />
                                    </td>
                                    <td><?php echo e($one->ip); ?></td>
                                    <td><strong><?php echo e($one->status_info['name']); ?></strong></td>
                                    <td class="no-sort no-click bread-actions">
                                        <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('delete', $model)): ?>
                                            <div class="btn btn-sm btn-danger pull-right delete" data-id="<?php echo e($one->id); ?>">
                                                <i class="voyager-trash"></i> <?php echo e(__('voyager::generic.delete')); ?>

                                            </div>
                                        <?php endif; ?>
                                        <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('edit', $model)): ?>
                                            <a href="<?php echo e(route('voyager.'.$type.'.edit', [$one->id, $requests])); ?>" class="btn btn-sm btn-primary pull-right edit">
                                                <i class="voyager-edit"></i> <?php echo e(__('voyager::generic.edit')); ?>

                                            </a>
                                        <?php endif; ?>                                        
                                    </td>
                                </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="panel-body">                        
                        <div class="pull-right"><?php echo e($many->appends(\Request::except('page'))->links()); ?></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal modal-danger fade" tabindex="-1" id="delete_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="<?php echo e(__('voyager::generic.close')); ?>">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">
                        <i class="voyager-trash"></i> <?php echo e(__('voyager::generic.delete_question')); ?>?
                    </h4>
                </div>
                <div class="modal-footer">
                    <form action="#" id="delete_form" method="POST">
                        <?php echo e(method_field("DELETE")); ?>

                        <?php echo e(csrf_field()); ?>                        
                        <div class="form-group" style="text-align: left;">
                            <label for="delete_text">Type 'DELETE' to the field under this text!</label>
                            <input type="text" class="form-control" id="delete_text" value="">                            
                        </div>
                        <input type="submit" class="btn btn-danger pull-right delete-confirm" value="<?php echo e(__('voyager::generic.delete_this_confirm')); ?>" onclick="return document.getElementById('delete_text').value == 'DELETE' ? true : false;">
                    </form>
                    <button type="button" class="btn btn-default pull-right" data-dismiss="modal"><?php echo e(__('voyager::generic.cancel')); ?></button>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?> 
    <script src="/js/select.js"></script>  
    <script> 
        $('td').on('click', '.delete', function (e) {
            $('#delete_form')[0].action = '<?php echo e(route('voyager.'.$type.'.destroy', ['id' => '__item', $requests])); ?>'.replace('__item', $(this).data('id'));

            $('#delete_modal').modal('show');
        });

        $('#branch_id').select2({
            minimumInputLength: 3,
            ajax: {
                url: '<?php echo e(route("voyager.getbranches")); ?>',
                type: 'post',
                dataType: 'json',
                headers: {
                    'X-CSRF-TOKEN': '<?php echo e(csrf_token()); ?>'
                },
                data:function(params) {
                  return {
                    search: params.term,
                  };
                },
                processResults: function (data) {
                    if (data) {
                        return {
                            results: data
                        }; 
                    }
                }
            }
        });
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('voyager::master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/d2l.sitepark.ua/resources/views/Backend/users/browse.blade.php ENDPATH**/ ?>