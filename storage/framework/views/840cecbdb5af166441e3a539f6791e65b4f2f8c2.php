<?php $__env->startSection('css'); ?>
	<link rel="stylesheet" href="<?php echo e(mix('/css/login.css')); ?>">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('breadcrumbs'); ?>
	<?php echo $__env->make('Frontend.templates.breadcrumbs', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>	
	
	<div class="registration-socials">
		<?php if(!$email): ?>
		<p><?php echo e(__('translations.login_social')); ?></p>
		<div class="fx registration-social-items">
			<div id="ggl_reg_btn" class="fx fxc registration-social-item registration-social-ggl">G</div>
			<div id="fcb_reg_btn" class="fx fxc registration-social-item registration-social-fcb" onclick="regFcb()">f</div>
		</div>	
		<div class="registration-separator"><span>or</span></div>
		<?php else: ?>
			<br><br>
			<p>Sign in as <?php echo e($email); ?>:</p>
		<?php endif; ?>
	</div>
	
	<div class="registration-form">
		<form method="POST" action="<?php echo e(route('login')); ?>" id="login_form">
			<?php echo e(csrf_field()); ?>			
			<div class="form-line">				
				<input type="email" name="email" id="email" class="form-input" placeholder="<?php echo e(__('translations.email')); ?>" value="<?php if($email): ?><?php echo e($email); ?><?php endif; ?>" required="required">
				<?php if($errors->has('email')): ?>
                    <div class="error"><?php echo e($errors->first('email')); ?></div>
                <?php endif; ?>				
			</div>
			<div class="form-line">				
				<input type="password" name="password" id="password" class="form-input" placeholder="<?php echo e(__('translations.password')); ?>" value="" required="required">								
			</div>	

			<div class="form-line" style="display: none;">				
				<input type="text" name="social" id="social" value="1">							
			</div>
			<div class="form-line" style="display: none;">				
				<input type="text" name="social_type" id="social_type" value="">							
			</div>			
			<p class="registration-terms"><?php echo __('translations.login_forgot'); ?></p>		
			<div class="form-line">				
				<input type="submit" id="submit" class="btn" name="submit" value="<?php echo e(__('translations.login_button')); ?>">
			</div>
		</form>
		<p class="registration-login"><?php echo __('translations.login_reg'); ?></p>
	</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
	<script src="https://apis.google.com/js/api:client.js" defer onload="startGgl()"></script>
	<script>
	  window.fbAsyncInit = function() {
	    FB.init({
	      appId      : '2706069246138083',
	      cookie     : true,
	      xfbml      : true,
	      version    : 'v6.0'
	    });	
      
	    FB.AppEvents.logPageView();
	  };

	  (function(d, s, id){
	     var js, fjs = d.getElementsByTagName(s)[0];
	     if (d.getElementById(id)) {return;}
	     js = d.createElement(s); js.id = id;
	     js.src = "https://connect.facebook.net/en_US/sdk.js";
	     fjs.parentNode.insertBefore(js, fjs);
	   }(document, 'script', 'facebook-jssdk'));
	</script>
	<script>
		var googleUser = {};

		function startGgl() {
			gapi.load('auth2', function(){		      
				auth2 = gapi.auth2.init({
					client_id: '840235196337-2tscmi40hv0sg5a2jeggr5gf9ltv3vie.apps.googleusercontent.com',
					cookiepolicy: 'single_host_origin',		        
				});

				regGgl(document.getElementById('ggl_reg_btn'));
			});
		}

		function regGgl(element) {
			if (element) {
				auth2.attachClickHandler(element, {},
					function(googleUser) {						  	
					  	document.getElementById('email').value = googleUser.getBasicProfile().getEmail();
					  	document.getElementById('password').value = googleUser.getBasicProfile().getId();
					  	document.getElementById('social').value = <?php echo e(config('statuses.new')); ?>;
					  	document.getElementById('social_type').value = 'google';

					  	document.getElementById('submit').click();
					}, 
					function(error) {
					  showPopup(('<?php echo e(__('translations.login_social_error')); ?>'));
					}
				); 	
			}					
		}

		function regFcb() {
			FB.login(function(response) {
			  if (response.status === 'connected') {
			    FB.api('/me', {fields: 'id, name, email'}, function(response) {			   
				  	document.getElementById('email').value = response.email;
				  	document.getElementById('password').value = response.id;
				  	document.getElementById('social').value = <?php echo e(config('statuses.new')); ?>;
					document.getElementById('social_type').value = 'facebook';
				  	
				  	document.getElementById('submit').click();
			      
			    });
			  } else {
			    showPopup(('<?php echo e(__('translations.login_social_error')); ?>'));
			  }
			}, {scope: 'public_profile,email'});
		}
	</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('Frontend/master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/d2l.sitepark.ua/resources/views/Frontend/pages/login.blade.php ENDPATH**/ ?>