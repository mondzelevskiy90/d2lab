<?php $__env->startSection('css'); ?>
	<link rel="stylesheet" href="/css/select.css">
	<link rel="stylesheet" href="<?php echo e(mix('/css/account.css')); ?>">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('breadcrumbs'); ?>
	<?php echo $__env->make('Frontend.templates.breadcrumbs', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
	<h1 class="tc"><?php echo e($seo->name); ?></h1>	
	<form method="POST" action="<?php echo e(route('prices.editcustom', $data->id)); ?>">
		<div class="fx fxb panel-body prices-edit-form">		
			<?php echo e(csrf_field()); ?>			
			<div class="panel">				    			    	
				<div class="form-line">
					<label for="name"><?php echo e(__('translations.clinic')); ?></label>				
					<select name="branch_id" id="branch_id" onchange="changePriceServicesData(this.value)">
						<option value="0"> -- Select dentist --</option>
						<?php $__currentLoopData = $branches; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $branch): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<option value="<?php echo e($branch->id); ?>"><?php echo e($branch->name); ?></option>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					</select>				
				</div>
				<br><br>
				<div class="form-line">
					<div class="panel-head">
						<?php echo e(__('translations.services_list')); ?>

						<p style="font-size: 12px;font-weight: 400">*** <?php echo e(__('translations.select_only_needed')); ?></p>
					</div>
					<div id="brach_services_table" class="account-long-list" style="display: none">
						<div class="account-list-table account-list-table3 account-list-tableServices">
							<div class="fx account-list-head">						
									<div class="account-list-cell"><?php echo e(__('translations.service_name')); ?></div>
									<div class="account-list-cell"><?php echo e(__('translations.price_value')); ?></div>
									<div class="account-list-cell"><?php echo e(__('translations.currency')); ?></div>
									<div class="account-list-cell"></div>
							</div>
							<div id="brach_services_content" class="account-list-content"></div>
						</div>
					</div>
				</div>
						    				
			</div>
			<div class="panel panel-transparent panel-buttons">
				<div class="form-line button-submit">
					<a class="btn btn-grey btn-small" href="<?php echo e(route('prices.edit', $data->id)); ?>">Back</a>				
					<input type="submit" id="submit" class="btn" name="submit" value="<?php echo e(__('translations.save_changes')); ?>" style="display: none;">
				</div>
			</div>
		</div>
	</form>	
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
	<script src="/js/select.js" defer></script>
	<script>
		document.addEventListener('DOMContentLoaded', function(){
			$('#branch_id').select2();
		}, !1)

		function changePriceServicesData(val) {
			if (val != 0) {
				$.ajax({
					url: '/ajax/getData',
					type: 'post',
		            dataType: 'json',
		            headers: {
		                'X-CSRF-TOKEN': '<?php echo e(csrf_token()); ?>'
		            },
				    data: {action:'getPriceServicesDataForCustom', price_id: '<?php echo e($data->id); ?>', branch_id: val},
				    success: function (data) {
				    	if (data) {
				    		var html = '';

							for (i in data) {
								if (data[i].name != '') {
									html += '<div class="fx fac account-list-line account-list-headline">';
									html += '<div class="account-list-cell">'+data[i].name+'</div>';
									html += '</div>';
								}

								var services = data[i].services;

								for(j in services) {
									html += '<div class="fx fac account-list-line ap'+(data[i].name != '' ? 20 : 0)+'">';
									html += '<div class="account-list-cell">'+services[j].industry_service_name+'</div>';									
									html += '<div class="account-list-cell">';
									html += '<input type="text" name="services['+services[j].industry_service_id+'][price]" class="form-input" placeholder="<?php echo e(__('translations.price')); ?>" value="'+(services[j].changed_price ? services[j].changed_price : '')+'" onchange="isNaN(this.value) ? this.value = null : this.value = this.value;">';
									html += '</div>';
									html += '<div class="account-list-cell">';
									html += '<input type="hidden" name="services['+services[j].industry_service_id+'][currency_id]" value="'+services[j].currency_id+'"/>';
									html += '<input type="hidden" name="services['+services[j].industry_service_id+'][price_id]" value="'+services[j].price_id+'"/>';
									html += '<input type="hidden" name="services['+services[j].industry_service_id+'][branch_id]" value="'+services[j].branch_id+'"/>';
									html += '<input type="hidden" name="services['+services[j].industry_service_id+'][industry_service_id]" value="'+services[j].industry_service_id+'"/>';
									html += '<input type="hidden" name="services['+services[j].industry_service_id+'][price_service_id]" value="'+services[j].price_service_id+'"/>';
									html += 'USD';
									html += '</div>';
									

									if (services[j].price_for == 2) {
										html += '<div style="width:100%;font-size: 12px;font-weight: 400">*** <?php echo e(__('translations.price_for_all_service')); ?></div>';
									}

									html += '</div>';
								}								
							}

							$('#brach_services_content').html(html);				    		
				    		$('#submit, #brach_services_table').css('display', 'block');
				    	}
					}
				});				
			} else {
				$('#brach_services_content').html('');
				$('#submit, #brach_services_table').css('display', 'none');
			}			
		}
		
	</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('Frontend/account_master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/dentist2lab.com/resources/views/Frontend/pages/account/prices/editcustom.blade.php ENDPATH**/ ?>