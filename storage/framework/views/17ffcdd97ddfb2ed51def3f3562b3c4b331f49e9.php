<?php $__env->startSection('page_title', __('voyager::generic.'.(isset($data->id) ? 'edit' : 'add')).' '.$data->name); ?>

<?php $__env->startSection('css'); ?>
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('page_header'); ?>
    <h1 class="page-title">
        <i class="voyager-edit"></i>
        <?php echo e(__('voyager::generic.'.(isset($data->id) ? 'edit' : 'add')).' '.$data->name); ?>

    </h1>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="page-content container-fluid">
        <form class="form-edit-add" role="form" action="<?php if(!is_null($data->id)): ?><?php echo e(route('voyager.'.$type.'.update', [$data->id, $requests])); ?><?php else: ?><?php echo e(route('voyager.'.$type.'.store', [$requests])); ?><?php endif; ?>" method="POST" enctype="multipart/form-data" autocomplete="off">            
            <?php if(isset($data->id)): ?>
                <?php echo e(method_field("PUT")); ?>

            <?php endif; ?>
            <?php echo e(csrf_field()); ?>


            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-bordered">  
                        <div class="panel-body">                 
                            <?php if(count($errors) > 0): ?>
                                <div class="alert alert-danger">
                                    <ul>
                                        <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <li><?php echo e($error); ?></li>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </ul>
                                </div>
                            <?php endif; ?>       
                            <div class="form-group">
                                <p>Views: <?php echo e($data->views ?? 0); ?></p>
                            </div>                                             
                            <div class="form-group">
                                <label for="status"><?php echo e(__('adm.status')); ?></label>
                                <select class="form-control" name="status">        
                                    <option value="1" <?php if(isset($data->status) && $data->status == 1): ?> selected <?php endif; ?>>Active</option>       
                                    <option value="2" <?php if(isset($data->status) && $data->status == 2): ?> selected <?php endif; ?>>Disabled</option>
                                </select>
                            </div>                             
                            <div class="form-group">
                                <label for="name"><?php echo e(__('adm.name')); ?></label>
                                <input type="text" class="form-control" id="name" name="name" placeholder="<?php echo e(__('adm.name')); ?>" value="<?php echo e(old('name', $data->name ?? '')); ?>" required="required">
                            </div>
                            <div class="form-group">
                                <label for="link">Banner link</label>
                                <input type="text" class="form-control" id="link" name="link" placeholder="Link" value="<?php echo e(old('link', $data->link ?? '')); ?>">
                            </div>
                            <div class="form-group">
                                <label for="position">Position</label>
                                <select name="position" class="form-control" id="position">
                                    <option value="1" <?php if(isset($data->position) && $data->position == 1): ?> selected <?php endif; ?>>Footer</option>
                                    <option value="2" <?php if(isset($data->position) && $data->position == 2): ?> selected <?php endif; ?>>Left</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="sort_order">Sort order</label>
                                <input type="text" class="form-control" id="sort_order" name="sort_order" placeholder="Sort order" value="<?php echo e(old('sort_order', $data->sort_order ?? '')); ?>">
                            </div>
                            <div class="form-group">
                                <label for="image"><?php echo e(__('adm.image')); ?></label>
                                <?php if(isset($data->image) && $data->image != ''): ?>
                                    <img src="<?php echo e(filter_var($data->image, FILTER_VALIDATE_URL) ? $data->image : Voyager::image( $data->image )); ?>" style="width:200px; height:auto; clear:both; display:block; padding:2px; border:1px solid #ddd; margin-bottom:10px;" />
                                <?php endif; ?>
                            </div>
                            <input type="file" data-name="image" name="image" id="image">
                        </div>
                    </div>
                </div>                
            </div>

            <button type="submit" class="btn btn-primary pull-right save">
                <?php echo e(__('voyager::generic.save')); ?>

            </button>
            <div class="clearfix"></div>
        </form> 
        <iframe id="form_target" name="form_target" style="display:none"></iframe>
        <form id="my_form" action="<?php echo e(route('voyager.upload')); ?>" target="form_target" method="post" enctype="multipart/form-data" style="width:0px;height:0;overflow:hidden">
            <?php echo e(csrf_field()); ?>

            <input name="image" id="upload_file" type="file" onchange="$('#my_form').submit();this.value='';">
            <input type="hidden" name="type_slug" id="type_slug" value="<?php echo e($type); ?>">
        </form>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>
    <script>
        $('document').ready(function () {
            $('.toggleswitch').bootstrapToggle();
        });
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('voyager::master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/dentist2lab.com/resources/views/Backend/banners/edit-add.blade.php ENDPATH**/ ?>