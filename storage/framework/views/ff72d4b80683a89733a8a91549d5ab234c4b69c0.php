<div class="panel panel-half dashboard-item dashboard-item-half dashboard-item-<?php echo e($code); ?>">
	<div class="panel-head"><?php echo e(__('translations.invoices_data')); ?></div>
	<?php if($content['text'] && $content['text'] != ''): ?>
		<div class="panel-intro"><?php echo $content['text']; ?></div>
	<?php endif; ?>
	<div class="panel-content">
		<?php if($content['labels'] && $content['total'] && $content['paid'] && $content['unpaid']): ?>
			<div style="width:100%;height:400px;margin: auto">
				<canvas id="graph" width="200" height="400"></canvas>	
			</div>
			<script>
				document.addEventListener('DOMContentLoaded', function(){
					var ctx = document.getElementById('graph');	

					var labels_arr = [],
						total_arr = [],
						paid_arr = [],
						unpaid_arr = [],
						labels = <?php echo $content['labels']; ?>,
						total = <?php echo $content['total']; ?>,
						paid = <?php echo $content['paid']; ?>,
						unpaid = <?php echo $content['unpaid']; ?>;

					for(var i in labels) {
						labels_arr.push(labels[i]);
						total_arr.push(total[i]);
						paid_arr.push(paid[i]);
						unpaid_arr.push(unpaid[i]);						
					}				
					
					var myChart = new Chart(ctx, {
					    type: 'line',
					    data: {
					        labels: labels_arr,
					        datasets: [
					        	{
						            label: 'Total, $',
						            data: total_arr,
						            fill: false,
						            lineTension: 0.1,
						            borderColor: 'rgba(135, 15, 235, 1)',
						            borderWidth: 1
					        	},
					        	{
						            label: 'Paid, $',
						            data: paid_arr,
						            fill: false,
						            lineTension: 0.1,
						            borderColor: 'rgba(25, 249, 64, 1)',
						            borderWidth: 1
					        	},
					        	{
						            label: 'Unpaid, $',
						            data: unpaid_arr,
						            fill: false,
						            lineTension: 0.1,
						            borderColor: 'rgba(205, 30, 5, 1)',
						            borderWidth: 1
					        	}

					        ]
					    },
					    options: {  
						    responsive: true,
						    maintainAspectRatio: false
						}
					});
				}, !1);
			</script>
		<?php else: ?> 
			<p><?php echo e(__('translations.no_invoices_data')); ?></p>
		<?php endif; ?>
	</div>
</div>
<?php /**PATH /var/www/dentist2lab.com/resources/views/Frontend/templates/dashboard/graph.blade.php ENDPATH**/ ?>