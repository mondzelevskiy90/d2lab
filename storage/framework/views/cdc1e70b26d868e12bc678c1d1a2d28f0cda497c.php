<?php $__env->startSection('css'); ?>
	<link rel="stylesheet" href="<?php echo e(mix('/css/account.css')); ?>">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('breadcrumbs'); ?>
	<?php echo $__env->make('Frontend.templates.breadcrumbs', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
	<h1><?php echo e($seo->name); ?></h1>
	<?php if($branch): ?>
		<div class="panel panel-transparent panel-mob-short">
			<div class="panel-buttons">
				<a href="<?php echo e(route('staff.create')); ?>" class="btn btn-primary"><?php echo e(__('translations.create')); ?></a>
			</div>
		</div>
	<?php endif; ?>
	<div class="fx fxb panel-body staff-form">
		<div class="panel panel-table">
			<?php if($data && count($data)): ?> 
				<div class="account-list-table account-list-table6">
					<div class="fx account-list-head">						
							<div class="account-list-cell"><?php echo e(__('translations.name')); ?></div>
							<div class="account-list-cell"><?php echo e(__('translations.avatar')); ?></div>
							<div class="account-list-cell"><?php echo e(__('translations.role')); ?></div>
							<div class="account-list-cell"><?php echo e(__('translations.branch')); ?></div>
							<div class="account-list-cell"><?php echo e(__('translations.status')); ?></div>
							<div class="account-list-cell"></div>						
					</div>
					<div class="account-list-content">
						<?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<?php if($item->user): ?>
							<div class="fx fac account-list-line">
								<div class="account-list-cell"><?php echo e($item->user->name.' '.$item->user->surname); ?></div>
								<div class="account-list-cell">
									<div class="bi company-logo" style="margin: auto; background-image: url(/storage/<?php echo e($item->user->avatar); ?>)"></div>
								</div>
								<div class="account-list-cell"><?php echo e($item->role->display_name); ?></div>
								<div class="account-list-cell"><?php echo e($item->branch->name); ?></div>
								<?php
									$status = 'E-mail confirmation';

									if ($item->user->status == 2) $status = 'New';
									if ($item->user->status == 3) $status = __('translations.status_active');
									if ($item->user->status == 4) $status = __('translations.status_disactive');
								?>	
								<div class="account-list-cell"><?php echo e($status); ?></div>
								<div class="account-list-cell">
									<a href="<?php echo e(route('staff.edit', $item->id)); ?>" class="btn btn-small"><?php echo e(__('translations.edit')); ?></a>
									<div data-href="<?php echo e(route('staff.delete', $item->user_id)); ?>" class="btn btn-danger btn-small delete-element" data-msg="<?php echo e(__('translations.delete_alert')); ?>"><?php echo e(__('translations.delete')); ?></div>
								</div>
							</div>
							<?php endif; ?>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					</div>
				</div>				
			<?php else: ?> 
				<?php if($branch): ?> 
					<p><?php echo sprintf(__('translations.no_staff'), route('staff.create')); ?></p>
				<?php else: ?>
					<p><?php echo sprintf(__('translations.no_braches'), route('branches.create')); ?></p>
				<?php endif; ?>
			<?php endif; ?>
		</div>
	</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('Frontend/account_master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/dentist2lab.com/resources/views/Frontend/pages/account/staff/index.blade.php ENDPATH**/ ?>