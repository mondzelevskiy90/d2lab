<?php $__env->startSection('page_title', __('voyager::generic.'.(isset($data->id) ? 'edit' : 'add')).' '.$data->name); ?>

<?php $__env->startSection('css'); ?>
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('page_header'); ?>
    <h1 class="page-title">
        <i class="voyager-edit"></i>
        <?php echo e(__('voyager::generic.'.(isset($data->id) ? 'edit' : 'add')).' '.$data->name); ?>

    </h1>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="page-content container-fluid">
        <form class="form-edit-add" role="form" action="<?php if(!is_null($data->id)): ?><?php echo e(route('voyager.'.$type.'.update', [$data->id, $requests])); ?><?php else: ?><?php echo e(route('voyager.'.$type.'.store', [$requests])); ?><?php endif; ?>" method="POST" enctype="multipart/form-data" autocomplete="off">            
            <?php if(isset($data->id)): ?>
                <?php echo e(method_field("PUT")); ?>

            <?php endif; ?>
            <?php echo e(csrf_field()); ?>


            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-bordered">  
                        <div class="panel-body">                 
                            <?php if(count($errors) > 0): ?>
                                <div class="alert alert-danger">
                                    <ul>
                                        <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <li><?php echo e($error); ?></li>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </ul>
                                </div>
                            <?php endif; ?>                                                    
                            <div class="form-group">
                                <label for="branch_id">Location</label>
                                <?php if(is_null($data->id)): ?>
                                    <select class="form-control" name="branch_id" id="branch_id" required="required">
                                        <option value="0">--- Select Location ---</option>
                                        <?php $__currentLoopData = $branches; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $branch): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($branch->id); ?>"><?php echo e($branch->name); ?>(<?php echo e(\App\Services\Userdata::getUserAddress($branch)); ?>)</option> 
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </select>
                                <?php else: ?> 
                                    <p><?php echo e($data->branch->name); ?> (<?php echo e(\App\Services\Userdata::getUserAddress($data->branch)); ?>)</p>
                                <?php endif; ?>
                            </div>
                            <div class="form-group">
                                <label for="public_key">Public key</label>
                                <input type="text" class="form-control" id="public_key" name="public_key" placeholder="Public key" value="<?php echo e(old('public_key', $data->public_key ?? '')); ?>" maxlength="255" required="required">
                            </div> 
                            <div class="form-group">
                                <label for="private_key">Private key</label>
                                <input type="text" class="form-control" id="private_key" name="private_key" placeholder="Private key" value="<?php echo e(old('private_key', $data->private_key ?? '')); ?>" maxlength="255" required="required">                            
                            </div>                         
                        </div>
                    </div>
                </div>                
            </div>

            <button type="submit" class="btn btn-primary pull-right save">
                <?php echo e(__('voyager::generic.save')); ?>

            </button>
        </form>

       
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>
    <script src="/js/select.js"></script>  
    <script> 
        $('#branch_id').select2({
            minimumInputLength: -2,            
        });
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('voyager::master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/d2l.sitepark.ua/resources/views/Backend/branch-nmi-data/edit-add.blade.php ENDPATH**/ ?>