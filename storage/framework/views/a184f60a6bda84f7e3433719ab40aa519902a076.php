<?php $__env->startSection('css'); ?>
	<link rel="stylesheet" href="<?php echo e(mix('/css/account.css')); ?>">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>		
	<form class="content-editing-form invoice-editing-form" method="POST" action="<?php echo e(route('invoices.update', [$data->id, $requests])); ?>" enctype="multipart/form-data" id="invoice_editing_form">
		<div id="panel_top_buttons" class="fx fac fxb">
			<?php echo $__env->make('Frontend.templates.breadcrumbs', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
			<div class="panel-top-buttons button-submit order-buttons">
				<a class="btn btn-small btn-grey" href="<?php echo e(route('invoices', [$requests])); ?>">&larr; <?php echo e(__('translations.back')); ?></a>
				<div class="btn btn-small" onclick="printContent('invoice_print_content');"><?php echo e(__('translations.print_invoice')); ?></div>
				<a class="btn btn-small  btn-primary" href="<?php echo e(route('orders.edit', $data->order_id)); ?>"><?php echo e(__('translations.show_order')); ?></a>
				<?php if($can_pay && $data->payment_status != 4 && $data->total->price_changed_to == '0.00'): ?> 
					<input type="checkbox" id="invoice_<?php echo e($data->id); ?>" class="invoices-check" value="<?php echo e($data->id); ?>" style="display: none;" checked="checked">
					<div class="btn btn-small btn-danger" onclick="paySingleFromInvoicePage();">
						<?php if(Auth::user()->type == config('types.clinic')): ?>
							<?php echo e(__('translations.pay')); ?>

						<?php endif; ?>
						<?php if(Auth::user()->type == config('types.lab')): ?>
							<?php echo e(__('translations.paid')); ?>

						<?php endif; ?>
					</div>
				<?php endif; ?>
				<?php if($data->payment_status != 4 && Auth::user()->type == config('types.lab')): ?>			
					<input type="submit" id="submit2" class="btn btn-small btn-danger" value="<?php echo e(__('translations.save')); ?>">
				<?php endif; ?>
			</div>
		</div>
		<h1 class="tc"><?php echo e($seo->name); ?> <?php echo e($data->name); ?> (<?php echo e($data->payment_status_info->name); ?>)</h1>
		<?php echo e(csrf_field()); ?>

		<div class="fx fxb panel-body">
			<div class="fx fac fxb panel">
				<div id="invoice_print_content" class="invoice-print-content">
					<div style="width: 1024px;">
						<div style="width: 1022px;position: relative;">
							<div class="fx fxb invoice-to_branch-info">
								<div class="invoice-to_branch-left">
									<p><?php echo e($data->to_branch->name); ?></p>
									<br>
									<p><?php echo e($data->to_branch->zip_code .', '. $data->to_branch->address .', '. App\Services\Userdata::getLocality($data->to_branch->locality)); ?></p>
									<p>Tel: <?php echo e($data->to_branch->phone); ?></p>
								</div>
								<div class="invoice-to_branch-right">
									<div class="invoice-invoice-head"><?php echo e(__('translations.invoice')); ?></div>
									<br>
									<table class="invoice-table invoice-small-table invoice-table-date">
										<thead>
											<tr>
												<td><?php echo e(__('translations.date')); ?></td>
												<td><?php echo e(__('translations.invoice')); ?> #</td>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td><?php echo e(date('m-d-Y', strtotime($data->created_at))); ?></td>
												<td>
													<?php echo e($data->order_id); ?>

													<br>
													<?php echo e($data->name); ?>

												</td>
											</tr>
										</tbody>
									</table>						
								</div>
							</div>
							<div class="fx fxb invoice-branch-info">
								<table class="invoice-table invoice-short-table invoice-table-bill">
									<thead>
										<tr>
											<td><?php echo e(__('translations.bill_to')); ?></td>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>
												<p><?php echo e($data->branch->name); ?></p>
												<p><?php echo e($data->branch->zip_code .', '. $data->branch->address .', '. App\Services\Userdata::getLocality($data->branch->locality)); ?></p>
											</td>
										</tr>
									</tbody>
								</table>
								<table class="invoice-table invoice-short-table invoice-table-ship">
									<thead>
										<tr>
											<td><?php echo e(__('translations.ship_to')); ?></td>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>
												<p><?php echo e($data->branch->name); ?></p>
												<p><?php echo e($data->order_info->ship_address); ?></p>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
							<div class="invoice-infos">
								<table class="invoice-table invoice-full-table  invoice-table-infos">
									<thead>
										<tr>
											<td><?php echo e(__('translations.po_number')); ?></td>
											<td><?php echo e(__('translations.terms')); ?></td>
											<td><?php echo e(__('translations.rep')); ?></td>
											<td><?php echo e(__('translations.ship')); ?></td>
											<td><?php echo e(__('translations.via')); ?></td>
											<td><?php echo e(__('translations.fob')); ?></td>
											<td><?php echo e(__('translations.project')); ?></td>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td></td>
											<td></td>
											<td></td>
											<td><?php echo e($data->order_info->ship_date); ?></td>
											<td></td>
											<td></td>
											<td></td>
										</tr>
									</tbody>
								</table>
							</div>
							<?php if($prescriptions && count($prescriptions)): ?>
								<div class="invoice-invoice">
									<table class="invoice-table invoice-full-table invoice-table-invoice">
										<thead>
											<tr>
												<td><?php echo e(__('translations.quantity')); ?></td>
												<td><?php echo e(__('translations.item_code')); ?></td>
												<td><?php echo e(__('translations.description')); ?></td>
												<td><?php echo e(__('translations.price_each')); ?></td>
												<td><?php echo e(__('translations.amount')); ?></td>
											</tr>
										</thead>
										<tbody>
											<?php $__currentLoopData = $prescriptions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $prescription): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
												<?php
													$quantity = 1;

													if ($prescription->price_for == 1 && $prescription->price_order_date != '0.00' && $prescription->advance_price != '0.00') {
															$quantity = round((float)$prescription->advance_price / (float)$prescription->price_order_date);
													}

													$price_changed_to = $prescription->price_changed_to;
													$price = $prescription->price;

													$price_each = '0.00'; 

													if ($price_changed_to != '0.00') {
														$price_each = number_format($price_changed_to / $quantity, 2);
													} else {
														$price_each = number_format($price / $quantity, 2);
													}
												?>
												<tr>
													<td>
														<div id="invoice_quantity_<?php echo e($prescription->id); ?>"><?php echo e($quantity); ?></div>
													</td>
													<td><?php echo e($prescription->service->name); ?></td>
													<td>
														<a target="_blank" href="<?php echo e(route('orders.edit', $data->order_id)); ?>#prescription_name_<?php echo e($prescription->id); ?>">Order #<?php echo e($data->order_id); ?></a>, <?php echo e($prescription->service->name); ?> (<?php echo e($prescription->numbering_system->name); ?>:<?php echo e(\App\Services\Content::getSelectedToothsText($prescription->selecteds)); ?>)
														<br>
														<?php echo e($data->order_info->patient_name); ?> <?php echo e($data->order_info->patient_lastname); ?>

													</td>
													<td>
														<div class="invoice-each-price">
															<?php echo e($currency->symbol); ?><span id="price_each_<?php echo e($prescription->id); ?>"><?php echo e($price_each); ?></span>
														</div>
													</td>
													<td>
														<?php if($data->payment_status == 4): ?>
															<div class="invoice-amount-price">
																<?php echo e($currency->symbol); ?><span id="price_amount_<?php echo e($prescription->id); ?>"><?php echo e($price); ?></span>
															</div>
														<?php elseif(Auth::user()->type == config('types.lab')): ?>
															<?php if($price_changed_to != '0.00'): ?> 
																<div class="invoice-prev-price">
																	<?php echo e($currency->symbol); ?><span><?php echo e($price); ?></span>
																</div>
															<?php endif; ?>
															<?php if($can_pay): ?>
																<input type="hidden" name="prescriptions[<?php echo e($prescription->id); ?>][price]" value="<?php echo e($price); ?>">
																<div class="fx fac prescription-price-setInput">
																	<?php echo e($currency->symbol); ?><input type="text" name="prescriptions[<?php echo e($prescription->id); ?>][price_changed_to]" class="form-input prescription-price-input form-input-required" data-id="<?php echo e($prescription->id); ?>" id="prescription_price_<?php echo e($prescription->id); ?>" value="<?php if($price_changed_to != '0.00'): ?><?php echo e($price_changed_to); ?><?php else: ?><?php echo e($price); ?><?php endif; ?>" required="required" onchange="isNaN(this.value) ? this.value = 0 : this.value = this.value;calculateOrderTotal();">
																</div>
															<?php else: ?> 
																<div class="invoice-amount-price">
																	<?php if($price_changed_to != '0.00'): ?>
																		<?php echo e($price_changed_to); ?>

																	<?php else: ?>
																		<?php echo e($price); ?>

																	<?php endif; ?>
																</div>
															<?php endif; ?>
															<?php if($price_changed_to != '0.00'): ?> 
																<div class="invoice-not-confirmed"><?php echo e(__('translations.price_wait_confirm')); ?></div>
															<?php endif; ?>
															<div class="fx fac invoice-confirm-btns not-print">
																<br>
																<br>
																<div class="btn invoice-confirm-btn" onclick="confirmPrecriptionAction(<?php echo e($prescription->id); ?>, 1)"><?php echo e(__('translations.remake')); ?></div>
																<div class="btn btn-danger invoice-confirm-btn" onclick="confirmPrecriptionAction(<?php echo e($prescription->id); ?>, 2)"><?php echo e(__('translations.delete')); ?></div>
																<input type="hidden" name="prescriptions[<?php echo e($prescription->id); ?>][prescription_action]" id="prescription_action_<?php echo e($prescription->id); ?>" value="0">
															</div>
														<?php else: ?>
															<div class="<?php if($price_changed_to != '0.00'): ?><?php echo e('
															invoice-prev-price'); ?><?php else: ?><?php echo e('invoice-amount-price'); ?><?php endif; ?>">
																<?php echo e($currency->symbol); ?><span id="price_amount_<?php echo e($prescription->id); ?>"><?php echo e($price); ?></span>
															</div>

															<?php if($price_changed_to != '0.00'): ?> 
																<div class="invoice-changed-price invoice-amount-price">
																	<?php echo e($currency->symbol); ?><span id="price_changed_<?php echo e($prescription->id); ?>"><?php echo e($price_changed_to); ?></span>
																	<div class="invoice-not-confirmed"><?php echo e(__('translations.price_wait_confirm')); ?></div>
																	<?php if($can_pay): ?>
																		<div class="fx fac invoice-confirm-btns not-print">
																			<div class="btn invoice-confirm-btn" onclick="confirmPriceChanging(<?php echo e($prescription->id); ?>, 1)"><?php echo e(__('translations.yes')); ?></div>
																			<div class="btn btn-danger invoice-confirm-btn" onclick="confirmPriceChanging(<?php echo e($prescription->id); ?>, 2)"><?php echo e(__('translations.no')); ?></div>
																		</div>
																	<?php endif; ?>
																</div>
															<?php endif; ?>
														<?php endif; ?>
													</td>
												</tr>
											<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
										</tbody>
										<tfoot>
											<tr>
												<td colspan="3"></td>
												<td colspan="2">
													<div class="fx fac fxb invoice-total-line" style="position: relative;">
														<?php if($data->payment_status == 4): ?>
															<div class="fx fxc paid-background"><span><?php echo e(__('translations.paid')); ?></span></div>
														<?php endif; ?>
														<div>
															<strong><?php echo e(__('translations.total')); ?></strong>
														</div>
														<div>													
															<div class="invoice-total-price" >
																<?php if($data->total->price_changed_to != '0.00'): ?> 
																	<div class="invoice-prev-price">
																		<?php echo e($currency->symbol); ?><span><?php echo e($data->total->price); ?></span>
																	</div>
																<?php endif; ?>
																<?php echo e($currency->symbol); ?><span id="invoice_total_price"><?php if($data->total->price_changed_to != '0.00'): ?><?php echo e($data->total->price_changed_to); ?><?php else: ?><?php echo e($data->total->price); ?><?php endif; ?></span>
																<?php if(Auth::user()->type == config('types.lab')): ?>
																	<input type="hidden" name="order_total" id="invoice_total_input" value="<?php if($data->total->price_changed_to != '0.00'): ?><?php echo e($data->total->price_changed_to); ?><?php else: ?><?php echo e($data->total->price); ?><?php endif; ?>">
																
																<?php endif; ?>

																	<?php if($data->total->price_changed_to != '0.00'): ?> 
																		<div class="invoice-not-confirmed"><?php echo e(__('translations.price_wait_confirm')); ?></div>
																	<?php endif; ?>
															</div>
														</div>
													</div>
												</td>
											</tr>
										</tfoot>
									</table>
								</div>
							<?php endif; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</form>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
	<script>
		document.addEventListener('DOMContentLoaded', function(){
			
		}, !1);

		function printContent(id) {
			var new_window =window.open();
			var html = '<link rel="stylesheet" href="<?php echo e(mix('/css/styles.css')); ?>">';

			html += '<link rel="stylesheet" href="<?php echo e(mix('/css/account.css')); ?>">';
			html += '<link rel="stylesheet" href="<?php echo e(mix('/css/print_invoice.css')); ?>">';
			html += '<div class="fx fxb print-source"><span><?php echo e(config('app.url')); ?></span><span><?php echo e(date('m-d-Y')); ?></span></div>';
			html += document.getElementById(id).innerHTML;

			new_window.document.write(html);

			setTimeout(function(){
				new_window.print();
				new_window.close();
			}, 1000);
		}
	
		<?php if(Auth::user()->type == config('types.lab') && $data->payment_status != 4): ?>
			function calculateOrderTotal() {
				var prices = $(document).find('.prescription-price-input');
				var total = 0;

				prices.each(function(i){
					var id = this.getAttribute('data-id');
					var quantity = parseInt($('#invoice_quantity_'+id).html());
					var price = parseFloat(prices[i].value);

					$('#price_each_'+id).html((price / quantity).toFixed(2));

					total = total + price;
				});

				$('#invoice_total_price').html(total.toFixed(2));
				$('#invoice_total_input').val(total.toFixed(2));
			}

			function confirmPrecriptionAction(id, action) {
				var html = '';

				html += '<div class="confirm-prescription-action-text">';

				if (action == 1) html += '<?php echo e(__('translations.remake_prescription_text')); ?>';
				if (action == 2) html += '<?php echo e(__('translations.delete_prescription_text')); ?>';

				html += '</div>';
				html += '<br>';
				html += '<br>';
				html += '<div class="confirm-prescription-action-btns">';
				html += '<div class="btn btn-small" onclick="setPrescriptionAction('+id+', '+action+')"><?php echo e(__('translations.yes')); ?></div>&nbsp';
				html += '<div class="btn btn-small btn-danger" onclick="document.getElementById(\'cboxClose\').click()"><?php echo e(__('translations.no')); ?></div>';
				html += '</div>';


				showPopup('<div class="warning">'+ html +'</div>', true);
			}

			function setPrescriptionAction(id, action) {
				showLoader();
				$(document).find('#cboxClose').click();
				$(document).find('#prescription_action_'+id).val(action);

				setTimeout(function(){
					$('#invoice_editing_form').submit();
				}, 1000)
				
			}
		<?php endif; ?>
		<?php if(Auth::user()->type == config('types.clinic') && $data->payment_status != 4): ?>
			function confirmPriceChanging(id, status) {
				showLoader();

				$.ajax({
					url: '/ajax/getData',
					type: 'post',
		            dataType: 'json',
		            headers: {
		                'X-CSRF-TOKEN': '<?php echo e(csrf_token()); ?>'
		            },
				    data: {action:'confirmPriceChanging', id: id, status: status},
				    success: function(data) {
				    	if (data) {
				    		window.location.reload();
				    	}
				    }						    
				});
			}
		<?php endif; ?>
	</script>	
	<?php echo $__env->make('Frontend.templates.invoice_payment_script', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('Frontend/account_master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/d2l.sitepark.ua/resources/views/Frontend/pages/account/invoices/edit.blade.php ENDPATH**/ ?>