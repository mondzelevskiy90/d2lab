<!DOCTYPE html>
<html lang="<?php echo e(app()->getLocale()); ?>">
	<?php echo $__env->make('Frontend.parts.head', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
	<body>
		<div class="fx fxb wrapper">
			<header>
				<?php echo $__env->make('Frontend.parts.account_header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
			</header>
			<aside>
				<?php echo $__env->make('Frontend.parts.account_left', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
			</aside>
			<section class="<?php if(Session::has('accountmenuclosed')): ?> section-menu-cl <?php endif; ?>">
				<?php echo $__env->yieldContent('breadcrumbs'); ?>
				<?php echo $__env->yieldContent('content'); ?>
			</section>
			<?php echo $__env->make('Frontend.parts.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
		</div>		
	</body>
</html><?php /**PATH /var/www/dentist2lab.com/resources/views/Frontend/account_master.blade.php ENDPATH**/ ?>