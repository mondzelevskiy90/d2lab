<?php $__env->startSection('page_title', __('voyager::generic.viewing').' '.ucfirst($type)); ?>

<?php $__env->startSection('page_header'); ?>
    <h1 class="page-title">
        <i class="voyager-list-add"></i> <?php echo e(ucfirst($type)); ?>

        <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('add', $model)): ?>
            <a href="<?php echo e(route('voyager.'.$type.'.create', $requests)); ?>" class="btn btn-success">
                <i class="voyager-plus"></i> <?php echo e(__('voyager::generic.add_new')); ?>

            </a>
        <?php endif; ?>
    </h1>
<?php $__env->stopSection(); ?>
 
<?php $__env->startSection('content'); ?> 
    <div class="page-content container-fluid">
        <?php echo $__env->make('voyager::alerts', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-body">
                        <form method="get" action="<?php echo e(route('voyager.'.$type.'.index', $requests)); ?>">
                            <div class="col-sm-10">
                                <div class="row">                      
                                    <div class="col-sm-12">                                        
                                        <div class="col-md-3">
                                            <select onchange="$(this).parents('form').submit()" class="form-control" name="status">
                                                <option value="0" default>-- Select status --</option>
                                                <option value="1" <?php if(isset($filter['filter_status']) && $filter['filter_status'] == 1): ?> selected <?php endif; ?>><?php echo e(__('voyager::generic.status_on')); ?></option>
                                                <option value="2" <?php if(isset($filter['filter_status']) && $filter['filter_status'] == 2): ?> selected <?php endif; ?>><?php echo e(__('voyager::generic.status_off')); ?></option>           
                                            </select>
                                        </div>
                                        <div class="col-md-3">
                                            <input type="text" class="form-control" name="name" value="<?php if(isset($filter['filter_name']) && $filter['filter_name'] != ''): ?><?php echo e($filter['filter_name']); ?><?php endif; ?>" placeholder="<?php echo e(__('voyager::generic.search')); ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                                        
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <input type="submit" style="margin:0;" class="btn btn-success" value="OK">
                                <a class="btn btn-danger" style="margin:0;" href="<?php echo e(route('voyager.'.$type.'.index')); ?>">Clear</a>
                            </div>
                        </form>
                        <table id="dataTable" class="table table-hover">
                            <thead>
                            <tr>                                
                                <th>ID</th>     
                                <th><?php echo e(__('adm.name')); ?></th>
                                <th><?php echo e(__('adm.sort_order')); ?></th> 
                                <th><?php echo e(__('adm.status')); ?></th>                          
                                <th class="actions text-right"><?php echo e(__('voyager::generic.actions')); ?></th>
                            </tr>
                            </thead>
                            <tbody>
                                <?php $__currentLoopData = $many; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $one): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                    <td><?php echo e($one->id); ?></td>
                                    <td>
                                        <?php if(isset($one->content[0])): ?>
                                        <?php echo e($one->content[0]->name); ?>

                                        <?php endif; ?>
                                    </td>
                                    <td><?php echo e($one->sort_order); ?></td>
                                    <td>
                                    	<?php
                                    		if ($one->status == 1) echo __('voyager::generic.status_on');
                                    		if ($one->status == 2) echo __('voyager::generic.status_off');
                                    	?>
                                    </td>
                                    <td class="no-sort no-click bread-actions">
                                        <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('delete', $model)): ?>
                                            <div class="btn btn-sm btn-danger pull-right delete" data-id="<?php echo e($one->id); ?>">
                                                <i class="voyager-trash"></i> <?php echo e(__('voyager::generic.delete')); ?>

                                            </div>
                                        <?php endif; ?>
                                        <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('edit', $model)): ?>
                                            <a href="<?php echo e(route('voyager.'.$type.'.edit', [$one->id, $requests])); ?>" class="btn btn-sm btn-primary pull-right edit">
                                                <i class="voyager-edit"></i> <?php echo e(__('voyager::generic.edit')); ?>

                                            </a>
                                        <?php endif; ?>                                        
                                    </td>
                                </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="panel-body">	                    
                        <div class="pull-right"><?php echo e($many->appends(\Request::except('page'))->links()); ?></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal modal-danger fade" tabindex="-1" id="delete_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="<?php echo e(__('voyager::generic.close')); ?>">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">
                        <i class="voyager-trash"></i> <?php echo e(__('voyager::generic.delete_question')); ?>?
                    </h4>
                </div>
                <div class="modal-footer">
                    <form action="#" id="delete_form" method="POST">
                        <?php echo e(method_field("DELETE")); ?>

                        <?php echo e(csrf_field()); ?>

                        <input type="submit" class="btn btn-danger pull-right delete-confirm" value="<?php echo e(__('voyager::generic.delete_this_confirm')); ?>">
                    </form>
                    <button type="button" class="btn btn-default pull-right" data-dismiss="modal"><?php echo e(__('voyager::generic.cancel')); ?></button>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>   
    <script> 
        $('td').on('click', '.delete', function (e) {
            $('#delete_form')[0].action = '<?php echo e(route('voyager.'.$type.'.destroy', ['id' => '__item', $requests])); ?>'.replace('__item', $(this).data('id'));

            $('#delete_modal').modal('show');
        });
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('voyager::master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/d2l.sitepark.ua/resources/views/Backend/articles/browse.blade.php ENDPATH**/ ?>