<?php $__env->startSection('css'); ?>
	<link rel="stylesheet" href="/css/select.css">
	<link rel="stylesheet" href="<?php echo e(mix('/css/checkout.css')); ?>">	
	<link rel="stylesheet" href="/css/daterangepicker.css">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('breadcrumbs'); ?>
	<?php echo $__env->make('Frontend.templates.breadcrumbs', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
	<h1><?php echo e($seo->name); ?></h1>
	<div class="page-text">
		<?php if($branch): ?>
			<form method="POST" action="<?php echo e(route('checkout2.create')); ?>" enctype="multipart/form-data" id="order_editing_form">
				<?php echo $__env->make('Frontend.templates.checkout_form2', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
			</form>
		<?php elseif(!$branch && $mylabs && count($mylabs)): ?>
			<form method="GET" action="<?php echo e(route('checkout2')); ?>">
				<div class="fx fxb panel-body">
					<div class="panel">
						<div class="form-line">	
							<label for="branch_id"><?php echo e(__('translations.lab')); ?></label>			
							<select name="branch_id" id="branch_id" class="form-input" required="required">
								<option value="0"><?php echo e(__('translations.no_lab_selected')); ?></option>
								<?php $__currentLoopData = $mylabs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $mylab): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
									<?php if($mylab->branch): ?>
										<option value="<?php echo e($mylab->branch->id); ?>"><?php echo e($mylab->branch->name); ?></option>
									<?php endif; ?>
								<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
							</select>
						</div>
					</div>
				</div>
				<div class="panel panel-transparent panel-buttons">
					<div class="form-line button-submit">				
						<input type="submit" id="submit" class="btn" value="<?php echo e(__('translations.select_lab')); ?>" onclick="return document.getElementById('branch_id').value == 0 ? false : true;">
					</div>
				</div>
			</form>
		<?php else: ?>
			<div class="fx fxb panel-body">
				<div class="panel panel-transparent">
					<br><br>
					<p style="text-align:center"><?php echo e(__('translations.no_lab_to_select')); ?></p>
					<br>
					<br>
					<p style="text-align:center"><a class="btn" href="<?php echo e(url('catalog/dentistry/labs')); ?>"><?php echo e(__('translations.view_all_labs')); ?></a></p>
				</div>
			</div>
		<?php endif; ?>
	</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
	<script src="/js/select.js" defer></script>
	<script src="/js/moment.min.js" defer></script>
	<script src="/js/daterangepicker.js" defer></script>
	<?php echo $__env->make('Frontend.templates.checkout_scripts2', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('Frontend/master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/dentist2lab.com/resources/views/Frontend/pages/checkout2.blade.php ENDPATH**/ ?>