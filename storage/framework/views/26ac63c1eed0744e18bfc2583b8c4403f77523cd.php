<?php $__env->startSection('page_title', __('voyager::generic.'.(isset($data->id) ? 'edit' : 'add')).' '.$data->name); ?>

<?php $__env->startSection('css'); ?>
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('page_header'); ?>
    <h1 class="page-title">
        <i class="voyager-edit"></i>
        <?php echo e(__('voyager::generic.'.(isset($data->id) ? 'edit' : 'add')).' '.$data->name); ?>

    </h1>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="page-content container-fluid">
        <form class="form-edit-add" role="form" action="<?php if(!is_null($data->id)): ?><?php echo e(route('voyager.'.$type.'.update', [$data->id, $requests])); ?><?php else: ?><?php echo e(route('voyager.'.$type.'.store', [$requests])); ?><?php endif; ?>" method="POST" enctype="multipart/form-data" autocomplete="off">            
            <?php if(isset($data->id)): ?>
                <?php echo e(method_field("PUT")); ?>

            <?php endif; ?>
            <?php echo e(csrf_field()); ?>


            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-bordered">  
                        <div class="panel-body">                 
                            <?php if(count($errors) > 0): ?>
                                <div class="alert alert-danger">
                                    <ul>
                                        <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <li><?php echo e($error); ?></li>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </ul>
                                </div>
                            <?php endif; ?>                                                    
                            <div class="form-group">
                                <label for="status"><?php echo e(__('adm.status')); ?></label>
                                <select class="form-control" name="status">                                    
                                    <?php $__currentLoopData = $statuses; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $status): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option value="<?php echo e($status['id']); ?>" <?php if(!is_null($data->id) && $status['id'] == $data->status): ?> selected <?php endif; ?>><?php echo e($status['name']); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>    
                                </select>
                            </div> 
                            <div class="form-group">
                                <label for="type"><?php echo e(__('voyager::generic.type')); ?></label>
                                <?php if(!is_null($data->id)): ?>
                                    <?php $__currentLoopData = $types; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $type_item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <?php if($type_item['id'] == $data->type): ?>
                                            <input type="text" class="form-control" id="type" value="<?php echo e($type_item['name']); ?>" disabled="disabled">
                                            <input type="hidden" name="type" value="<?php echo e($data->type); ?>">
                                        <?php endif; ?>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php else: ?>
                                    <select class="form-control" name="type">
                                        <?php $__currentLoopData = $types; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $type_item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($type_item['id']); ?>"><?php echo e($type_item['name']); ?></option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </select>
                                <?php endif; ?>   
                            </div>
                             <div class="form-group">
                                <label for="role_id"><?php echo e(__('adm.role')); ?></label>
                                <select class="form-control" name="role_id">                                    
                                    <?php $__currentLoopData = $roles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $role): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option value="<?php echo e($role['id']); ?>" <?php if(!is_null($data->id) && $role['id'] == $data->role_id): ?> selected <?php endif; ?>><?php echo e($role['display_name']); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>    
                                </select>
                            </div> 
                            <div class="form-group">
                                <label for="name"><?php echo e(__('adm.name')); ?></label>
                                <input type="text" class="form-control" id="name" name="name" placeholder="<?php echo e(__('adm.name')); ?>" value="<?php echo e(old('name', $data->name ?? '')); ?>">
                            </div> 
                            <div class="form-group">
                                <label for="surname"><?php echo e(__('adm.surname')); ?></label>
                                <input type="text" class="form-control" id="surname" name="surname" placeholder="<?php echo e(__('adm.surname')); ?>" value="<?php echo e(old('surname', $data->surname ?? '')); ?>">
                            </div>
                            <div class="form-group">
                                <label for="email"><?php echo e(__('adm.email')); ?></label>
                                <?php if(!is_null($data->id)): ?>
                                    <input type="text" class="form-control" id="email" placeholder="<?php echo e(__('adm.email')); ?>" value="<?php echo e($data->email ?? ''); ?>" disabled="disabled">
                                    <input type="hidden" name="email" value="<?php echo e($data->email); ?>">
                                <?php else: ?>
                                    <input type="text" class="form-control" name="email" id="email" placeholder="<?php echo e(__('adm.email')); ?>" value="<?php echo e($data->email ?? ''); ?>">
                                <?php endif; ?>
                            </div> 
                            <div class="form-group">
                                <label for="phone"><?php echo e(__('adm.phone')); ?></label>
                                <input type="text" class="form-control" id="phone" name="phone" placeholder="<?php echo e(__('adm.phone')); ?>" value="<?php echo e(old('phone', $data->phone ?? '')); ?>">
                            </div>
                            <?php if(!is_null($data->id)): ?>
                                <div class="form-group">
                                    <label for="address"><?php echo e(__('adm.address')); ?></label>
                                    <input type="text" class="form-control" id="address" value="<?php echo e($address); ?>" disabled="disabled">
                                </div>  
                            <?php endif; ?>                            
                            <div class="form-group">
                                <label for="company"><?php echo e(__('adm.company_name')); ?></label>
                                <input type="text" class="form-control" name="company" id="company" placeholder="<?php echo e(__('adm.company_name')); ?>" value="<?php echo e(old('company', $data->company ?? '')); ?>">
                            </div>
                            <div class="form-group">
                                <label for="ip">Last visit from IP</label>
                                <input type="text" class="form-control" id="ip" value="<?php echo e($data->ip ?? ''); ?>" disabled="disabled">
                            </div>
                            <div class="form-group">
                                <label for="avatar"><?php echo e(__('adm.image')); ?></label>
                                <?php if(isset($data->avatar) && $data->avatar != ''): ?>
                                    <img src="<?php echo e(filter_var($data->avatar, FILTER_VALIDATE_URL) ? $data->avatar : Voyager::image( $data->avatar )); ?>" style="width:50px; height:auto; clear:both; display:block; padding:2px; border:1px solid #ddd; border-radius:50%;margin-bottom:10px;" />
                                <?php endif; ?>
                                <input type="file" data-name="avatar" name="avatar" id="avatar">
                            </div>
                        </div>
                    </div>
                </div>                
            </div>

            <button type="submit" class="btn btn-primary pull-right save">
                <?php echo e(__('voyager::generic.save')); ?>

            </button>
        </form>

        <iframe id="form_target" name="form_target" style="display:none"></iframe>
        <form id="my_form" action="<?php echo e(route('voyager.upload')); ?>" target="form_target" method="post" enctype="multipart/form-data" style="width:0px;height:0;overflow:hidden">
            <?php echo e(csrf_field()); ?>

            <input name="image" id="upload_file" type="file" onchange="$('#my_form').submit();this.value='';">
            <input type="hidden" name="type_slug" id="type_slug" value="<?php echo e($type); ?>">
        </form>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>
    <script>
        $('document').ready(function () {
            $('.toggleswitch').bootstrapToggle();
        });
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('voyager::master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/d2l.sitepark.ua/resources/views/Backend/users/edit-add.blade.php ENDPATH**/ ?>