<?php $__env->startSection('css'); ?>
	<link rel="stylesheet" href="<?php echo e(mix('/css/account.css')); ?>">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('breadcrumbs'); ?>
	<?php echo $__env->make('Frontend.templates.breadcrumbs', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
	<h1><?php echo e($seo->name); ?></h1>
	<?php if($defaults && count($defaults)): ?>
		<div class="fx panel panel-transparent panel-mob-short dashbord-setup">
			<div id="dashboard_setup" style="display: none;">
				<form action="<?php echo e(route('dashboard_setup')); ?>" method="POST" class="fx">
					<?php echo e(csrf_field()); ?>

					<?php $__currentLoopData = $defaults; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $default): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
						<?php if($default->dashboard): ?>
							<div class="form-line">
								<div class="form-checkbox">								
									<input type="checkbox" name="dashboards[<?php echo e($default->dashboard_id); ?>]" id="dashboards<?php echo e($default->dashboard_id); ?>" value="<?php echo e($default->dashboard_id); ?>" <?php if(!count($customs) || in_array($default->dashboard_id, $customs)): ?><?php echo e('checked="checked"'); ?><?php endif; ?>>
									<label for="dashboards<?php echo e($default->dashboard_id); ?>"><?php echo e($default->dashboard->name); ?></label>
								</div>
							</div>
						<?php endif; ?>
					<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					<div class="form-line" style="width: 100%;">
						<input type="submit" class="btn btn-small" value="<?php echo e(__('translations.save')); ?>">
					</div>
				</form>
			</div>
			<div class="dashboard-controls">
				<div id="dashboard_setup_btn" class="bi btn btn-small btn-grey" title="<?php echo e(__('translations.setup')); ?>"></div>
			</div>
		</div>
	<?php endif; ?>
	<div class="fx fxb panel-body dashbord-content">
		<?php if($data && count($data)): ?>
			<?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
				<?php echo $item; ?>

			<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
		<?php endif; ?>
	</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
	<script src="/js/chart.js" defer></script>
	<script>
		document.addEventListener('DOMContentLoaded', function(){			
			detectActivity(300);

			$('#dashboard_setup_btn').on('click', function(){
				$('#dashboard_setup').slideToggle();
			});
		}, !1);
	</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('Frontend/account_master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/d2l.sitepark.ua/resources/views/Frontend/pages/account/dashboard.blade.php ENDPATH**/ ?>