<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="viewport" content="width=device-width, minimum-scale=1, maximum-scale=1, initial-scale=1">
	<?php if(isset($seo) && $seo): ?>
		<title><?php echo e($seo->meta_title." - "); ?>Dentist2Lab</title>
		<meta name="description" content="<?php echo e($seo->meta_description); ?>, Dentist2Lab">
	<?php else: ?>
		<title>Dentist2Lab</title>
		<meta name="description" content="Dentist2Lab">
	<?php endif; ?>
    <link rel="icon" href="/favicon.ico" type="image/x-icon"/>
    <link rel="canonical" href="<?php echo e(url()->current()); ?>" />
    <base href="<?php echo e(url('/')); ?>"/>
	<link rel="stylesheet" href="<?php echo e(mix('/css/styles.css')); ?>">
	<?php echo $__env->yieldContent('css'); ?>
</head><?php /**PATH /var/www/d2l.sitepark.ua/resources/views/Frontend/parts/head.blade.php ENDPATH**/ ?>