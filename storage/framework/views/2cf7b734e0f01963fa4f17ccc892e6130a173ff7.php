<?php $__env->startSection('css'); ?>
	<link rel="stylesheet" href="/css/select.css">
	<link rel="stylesheet" href="/css/daterangepicker.css">
	<link rel="stylesheet" href="<?php echo e(mix('/css/account.css')); ?>">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('breadcrumbs'); ?>
	<?php echo $__env->make('Frontend.templates.breadcrumbs', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
	<h1 style="display: none;"><?php echo e($seo->name); ?></h1>	
	<div class="panel panel-transparent">
		<div id="order_filters_block" class="panel-filters" style="display: none;">
			<form method="GET" action="<?php echo e(route('invoices')); ?>" id="filters_block" class="fx fac">		
				<?php echo e(csrf_field()); ?>	
				<?php if($branches && count($branches)): ?>
					<div class="form-filter">
						<select name="branch" id="branch" class="form-input" onchange="document.getElementById('filters_block').submit();">	
							<option value="0"> -- <?php echo e(__('translations.branch')); ?> --</option>
							<?php $__currentLoopData = $branches; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $branch): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
								<?php if($branch->status != 4): ?>
									<option value="<?php echo e($branch->id); ?>" <?php if(isset($filters['branch']) && $filters['branch'] == $branch->id): ?><?php echo e('selected="selected"'); ?><?php endif; ?>><?php echo e($branch->name); ?></option>
								<?php endif; ?>
							<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
						</select>		
					</div>
				<?php endif; ?>	
				<div class="form-filter">
					<select name="invoice_branch" id="invoice_branch" class="form-input" onchange="document.getElementById('filters_block').submit();">	
						<option value="0">
							<?php if(Auth::user()->type == config('types.clinic')): ?>
								-- <?php echo e(__('translations.select').' '.__('translations.lab')); ?> --
							<?php endif; ?>
							<?php if(Auth::user()->type == config('types.lab')): ?>
								-- <?php echo e(__('translations.select').' '.__('translations.clinic')); ?> --
							<?php endif; ?>
						</option>
						<?php $__currentLoopData = $invoice_branches; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $invoice_branch): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<?php if(Auth::user()->type == config('types.clinic')): ?>
								<option value="<?php echo e($invoice_branch->to_branch_id); ?>" <?php if(isset($filters['invoice_branch']) && $filters['invoice_branch'] == $invoice_branch->to_branch_id): ?><?php echo e('selected="selected"'); ?><?php endif; ?>><?php echo e($invoice_branch->to_branch->name); ?></option>
							<?php endif; ?>
							<?php if(Auth::user()->type == config('types.lab')): ?>
								<option value="<?php echo e($invoice_branch->branch_id); ?>" <?php if(isset($filters['invoice_branch']) && $filters['invoice_branch'] == $invoice_branch->branch_id): ?><?php echo e('selected="selected"'); ?><?php endif; ?>><?php echo e($invoice_branch->branch->name); ?></option>
							<?php endif; ?>					
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					</select>	
				</div>		
				<div class="form-filter">
					<select name="payment_type" id="payment_type" class="form-input">	
						<option value="0"> -- <?php echo e(__('translations.payment_type')); ?> --</option>
						<?php $__currentLoopData = $payment_types; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $type): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<option value="<?php echo e($type->id); ?>" <?php if(isset($filters['payment_type']) && $filters['payment_type'] == $type->id): ?><?php echo e('selected="selected"'); ?><?php endif; ?>><?php echo e($type->name); ?></option>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					</select>		
				</div>
				<div class="form-filter">
					<select name="payment_status" id="payment_status" class="form-input">	
						<option value="0"> -- <?php echo e(__('translations.payment_status')); ?> --</option>
						<?php $__currentLoopData = $payment_statuses; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $payment_status): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<option value="<?php echo e($payment_status->id); ?>" <?php if(isset($filters['payment_status']) && $filters['payment_status'] == $payment_status->id): ?><?php echo e('selected="selected"'); ?><?php endif; ?>><?php echo e($payment_status->name); ?></option>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					</select>		
				</div>		
				<div class="form-filter">
					<input type="text" name="from_date" id="from_date" class="form-input" placeholder="<?php echo e(__('translations.from_date')); ?>" value="<?php if(isset($filters['from_date']) && $filters['from_date']): ?><?php echo e($filters['from_date']); ?><?php endif; ?>">			
				</div>
				<div class="form-filter">
					<input type="text" name="to_date" id="to_date" class="form-input" placeholder="<?php echo e(__('translations.to_date')); ?>" value="<?php if(isset($filters['to_date']) && $filters['to_date']): ?><?php echo e($filters['to_date']); ?><?php endif; ?>">			
				</div>
				<div class="form-filter">
					<input type="text" name="search" id="search" class="form-input" placeholder="<?php echo e(__('translations.search_invoice_id')); ?>" value="<?php if(isset($filters['search']) && $filters['search']): ?><?php echo e($filters['search']); ?><?php endif; ?>">			
				</div>
				<div class="filter-buttons" style="text-align: right;">					
					<input type="submit" class="btn btn-small" value="<?php echo e(__('translations.filter')); ?>">
					<a href="<?php echo e(route('invoices')); ?>" class="btn btn-danger btn-small"><?php echo e(__('translations.clear')); ?></a>
				</div>
			</form>
		</div>	

		<div class="fx fxb panel-buttons">
			<div id="show_filter_block" class="btn"><?php echo e(__('translations.filter_btn')); ?></div>
		</div>
	</div>
	<div class="fx fxb panel-body">
		<?php if($data && count($data) && $can_pay): ?>
			<div class="panel panel-table-controls pay-controls" style="display: none;">
				<div class="btn btn-small btn-danger" id="pay_selected" onclick="paySelectedInvoices();">
					<?php if(Auth::user()->type == config('types.clinic')): ?>
						<?php echo e(__('translations.pay_selected')); ?>

					<?php endif; ?>
					<?php if(Auth::user()->type == config('types.lab')): ?>
						<?php echo e(__('translations.check_pay_selected')); ?>

					<?php endif; ?>
				</div>
			</div>
		<?php endif; ?>
		<div class="panel panel-table">
			<?php if($data && count($data)): ?> 
				<div class="account-list-table account-invoices-table">
					<div class="fx account-list-head">											
							<div class="account-list-cell"></div>
							<div class="account-list-cell"><?php echo e(__('translations.name')); ?></div>
							<div class="account-list-cell"><?php echo e(__('translations.order_id')); ?></div>
							<div class="account-list-cell"><?php echo e(__('translations.branch')); ?></div>
							<div class="account-list-cell">
								<?php if(Auth::user()->type == config('types.clinic')): ?>
									<?php echo e(__('translations.lab')); ?>

								<?php endif; ?>
								<?php if(Auth::user()->type == config('types.lab')): ?>
									<?php echo e(__('translations.clinic')); ?>

								<?php endif; ?>
							</div>	
							<div class="account-list-cell"><?php echo e(__('translations.payment_type')); ?></div>
							<div class="account-list-cell"><?php echo e(__('translations.payment_status')); ?></div>
							<div class="account-list-cell"></div>						
					</div>
					<div class="account-list-content">
						<?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<div class="fx fac account-list-line">
								<div class="account-list-cell">
									<div class="form-line">
										<?php if($item->payment_status != 4 && !in_array($item->order_id, $changed_prices)): ?> 
										<div class="fx fac form-checkbox">
											<input type="checkbox" id="invoice_<?php echo e($item->id); ?>" class="invoices-check" name="invoices[<?php echo e($item->id); ?>]" value="<?php echo e($item->id); ?>">
											<label for="invoice_<?php echo e($item->id); ?>"></label>
										</div>
										<?php endif; ?>
									</div>
								</div>								
								<div class="account-list-cell">
									<strong><?php echo e($item->name); ?></strong> 
									<?php if(in_array($item->order_id, $changed_prices)): ?>
										<?php if(Auth::user()->type == config('types.clinic')): ?>
											<?php echo '<span style="color: red;display:block;">'.__('translations.price_wait_confirm').'</span>'; ?>

										<?php endif; ?>
										<?php if(Auth::user()->type == config('types.lab')): ?>
											<?php echo '<span style="color: red;display:block;">'.__('translations.price_wait_changed').'</span>'; ?>

										<?php endif; ?>
									<?php endif; ?>
								</div>	
								<div class="account-list-cell"><a target="_blank" href="<?php echo e(route('orders.edit', $item->order_id)); ?>">#<?php echo e($item->order_id); ?> <?php if($item->order_info && $item->order_info->name != ''): ?><br>(<?php echo e($item->order_info->name); ?>)<?php endif; ?></a></div>
								<div class="account-list-cell">
									<?php if(Auth::user()->type == config('types.clinic')): ?>
										<?php echo e($item->branch->name); ?>

									<?php endif; ?>
									<?php if(Auth::user()->type == config('types.lab')): ?>
										<?php echo e($item->to_branch->name); ?>

									<?php endif; ?>
								</div>
								<div class="account-list-cell">
									<?php if(Auth::user()->type == config('types.clinic')): ?>
										<?php echo e($item->to_branch->name); ?>

									<?php endif; ?>
									<?php if(Auth::user()->type == config('types.lab')): ?>
										<?php echo e($item->branch->name); ?>

									<?php endif; ?>
								</div>
								<div class="account-list-cell">
									<?php if($item->payment_type != 0 && $item->total && $item->total->price != '0.00'): ?>
										<?php echo e($item->payment_type_info->name); ?>

										<br>
									<?php endif; ?>
									<?php if($item->total && $item->total->price): ?>										
										<strong>$<?php echo e($item->total->price); ?></strong>
									<?php endif; ?>
								</div>
								<div class="account-list-cell">
									<?php if($item->payment_status && $item->payment_status_info->icon && $item->total && $item->total->price != '0.00'): ?>	
										<img src="<?php echo e(url('/').'/storage/'.$item->payment_status_info->icon); ?>" style="max-width: 35px;height:auto;" title="<?php echo e($item->payment_status_info
											->name); ?>" alt="<?php echo e($item->payment_status_info->name); ?>"> 
									<?php elseif($item->total && $item->total->price != '0.00'): ?>
										<?php echo e($item->payment_status_info->name); ?>

									<?php endif; ?>
								</div>
								<div class="account-list-cell">
									<a href="<?php echo e(route('invoices.edit', [$item->id, $requests])); ?>" class="btn btn-small"><?php echo e(__('translations.view')); ?></a>	
									<?php if($item->payment_status != 4 && !in_array($item->order_id, $changed_prices) && $can_pay && $item->order_info && in_array($item->order_info->status, [12,14,15,16,17,18]) && $item->total && $item->total->price != '0.00'): ?> 
										<div class="btn btn-danger" onclick="paySingleInvoice('<?php echo e($item->id); ?>');">
											<?php if(Auth::user()->type == config('types.clinic')): ?>
												<?php echo e(__('translations.pay')); ?>

											<?php endif; ?>
											<?php if(Auth::user()->type == config('types.lab')): ?>
												<?php echo e(__('translations.paid')); ?>

											<?php endif; ?>
										</div>
									<?php endif; ?>						
								</div>
							</div>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					</div>					
				</div>
				<div class="pagination-block"><?php echo e($data->appends(\Request::except('page'))->links()); ?></div>
			<?php else: ?> 
				<p><?php echo e(__('translations.no_invoices')); ?></p>
			<?php endif; ?>
		</div>
	</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
	<script src="/js/select.js" defer></script>
	<script src="/js/moment.min.js" defer></script>
	<script src="/js/daterangepicker.js" defer></script>
	<script>
		document.addEventListener('DOMContentLoaded', function() {
			$('#payment_type, #branch, #payment_status, #invoice_branch').select2({
				minimumResultsForSearch: -1
			});

			$('#from_date, #to_date').on('focus', function(){
				$(this).daterangepicker({				
					singleDatePicker: true,
					showDropdowns: true,
					startDate: false,				
					locale: {
				        format: 'YYYY-MM-DD'
				    },
				});
			});

			$('#show_filter_block').on('click', function(){
				$(this).toggleClass('show-filter-close');
				$('#order_filters_block').slideToggle();
			});

			$('.invoices-check + label').on('click', function(){
				if ($(this).hasClass('show-check-pseudo')) {
					$(this).removeClass('show-check-pseudo');
				} else {
					$(this).addClass('show-check-pseudo');
				}
			});

		}, !1);
	</script>
	<?php echo $__env->make('Frontend.templates.invoice_payment_script', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('Frontend/account_master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/dentist2lab.com/resources/views/Frontend/pages/account/invoices/index.blade.php ENDPATH**/ ?>