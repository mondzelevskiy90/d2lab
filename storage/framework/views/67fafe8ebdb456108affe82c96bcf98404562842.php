<div id="account_menu" class="account-menu <?php if(Session::has('accountmenuclosed')): ?> account-menu-cl <?php endif; ?>">
	<div id="account_menu_control" class="account-menu-control" onclick="transformAccountMenu()">
		<span></span>
		<span></span>
		<span></span>
	</div>
	<div id="account_menu_btn" onclick="openMobMenu()">
		<span></span>
		<span></span>
		<span></span>
	</div>
	<div id="account_menu_content" class="account-menu-content">
		<a href="<?php echo e(route('account')); ?>" class="account-settings-dashbord"><?php echo e(__('translations.account')); ?></a>
		<a target="_blank" href="catalog/dentistry/labs" class="account-settings-catalog"><?php echo e(__('translations.catalog')); ?></a>
		<?php 			
			$menu = config('account_access.'.Auth::user()->role_id); 
		?>
		
		<?php if($menu): ?>
			<?php $__currentLoopData = $menu; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
				<a href="<?php echo e(route($item)); ?>" class="account-settings-<?php echo e($item); ?> <?php if(route($item) == url()->current()): ?> account-item-active <?php endif; ?>"><?php echo e(__('translations.'.$item)); ?></a>
			<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> 
		<?php endif; ?>		
	</div>
</div>

<script>
    function openMobMenu() {
    	if ($('#account_menu_content').hasClass('account-menu-contentAct')) {
    		$('#account_menu_btn').removeClass('account-menu-btnAct');
    		$('#account_menu_content').removeClass('account-menu-contentAct');
    	} else {
			$('#account_menu_btn').addClass('account-menu-btnAct');
			$('#account_menu_content').addClass('account-menu-contentAct');
    	}
    }

	function transformAccountMenu() {
		if ($('#account_menu').hasClass('account-menu-cl')) {
			$.ajax({
				url: '/ajax/getData',
				type: 'post',
                dataType: 'json',
                headers: {
                    'X-CSRF-TOKEN': '<?php echo e(csrf_token()); ?>'
                },
			    data: {action:'setSession', method:'remove', name:'accountmenuclosed', value:'false'}
			});

			$('#account_menu').removeClass('account-menu-cl');
			$('section').removeClass('section-menu-cl');
		} else {
		    $.ajax({
				url: '/ajax/getData',
				type: 'post',
                dataType: 'json',
                headers: {
                    'X-CSRF-TOKEN': '<?php echo e(csrf_token()); ?>'
                },
			    data: {action:'setSession', method:'add', name:'accountmenuclosed', value:'true'}
			});

			$('#account_menu').addClass('account-menu-cl');
			$('section').addClass('section-menu-cl');
		}
	}
</script><?php /**PATH /var/www/d2l.sitepark.ua/resources/views/Frontend/parts/account_left.blade.php ENDPATH**/ ?>