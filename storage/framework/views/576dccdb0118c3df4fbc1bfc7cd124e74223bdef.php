<?php $__env->startSection('page_title', __('voyager::generic.viewing').' Locations'); ?>

<?php $__env->startSection('page_header'); ?>
    <h1 class="page-title">
        <i class="voyager-list-add"></i> Locations        
    </h1>
<?php $__env->stopSection(); ?>
 
<?php $__env->startSection('content'); ?> 
    <div class="page-content container-fluid">
        <?php echo $__env->make('voyager::alerts', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-body">
                        <div class="col-xs-12">
                                <div class="col-sm-12">
                                    General service charge value (0.01 = 1% ... 0.99 = 99%)
                                </div>
                                <div class="col-sm-4">
                                    <input type="text" id="service_charge" class="form-control" value="<?php echo e(setting('admin.service_charge')); ?>" placeholder="General service_charge value">
                                </div>
                                <div class="col-sm-8">
                                    <div class="btn btn-danger" onclick="saveServiceCharge()" style="position: relative;top: -5px;">Save service charge value</div>
                                </div>
                           
                        </div>
                        <br>
                        <br>
                        <form method="get" action="<?php echo e(route('voyager.'.$type.'.index', $requests)); ?>">
                            <div class="col-sm-10">
                                <div class="row">                      
                                    <div class="col-sm-12">                                        
                                        <div class="col-md-3">
                                            <select onchange="$(this).parents('form').submit()" class="form-control" name="verified">
                                                <option value="0" default>Select verified status</option>                                                
                                                <option value="1" <?php if(isset($filter['filter_verified']) && $filter['filter_verified'] == 1): ?> selected <?php endif; ?>>Verified</option>       
                                                <option value="2" <?php if(isset($filter['filter_verified']) && $filter['filter_verified'] == 2): ?> selected <?php endif; ?>>Not verified</option>
                                            </select>
                                        </div>
                                        <div class="col-md-3">
                                            <select onchange="$(this).parents('form').submit()" class="form-control" name="status">
                                                <option value="0" default><?php echo e(__('adm.select_status')); ?></option>                                                
                                                <option value="1" <?php if(isset($filter['filter_status']) && $filter['filter_status'] == 1): ?> selected <?php endif; ?>>Active</option>       
                                                <option value="2" <?php if(isset($filter['filter_status']) && $filter['filter_status'] == 2): ?> selected <?php endif; ?>>Suspended</option>
                                                <option value="3" <?php if(isset($filter['filter_status']) && $filter['filter_status'] == 3): ?> selected <?php endif; ?>>Blocked</option>
                                                <option value="4" <?php if(isset($filter['filter_status']) && $filter['filter_status'] == 4): ?> selected <?php endif; ?>>Removed</option>  
                                            </select>
                                        </div>                                                                       
                                        <div class="col-md-3">
                                            <input type="text" class="form-control" name="search" value="<?php if(isset($filter['filter_search']) && $filter['filter_search'] != ''): ?><?php echo e($filter['filter_search']); ?><?php endif; ?>" placeholder="<?php echo e(__('voyager::generic.search')); ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                                        
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <input type="submit" style="margin:0;" class="btn btn-success" value="OK">
                                <a class="btn btn-danger" style="margin:0;" href="<?php echo e(route('voyager.'.$type.'.index')); ?>">Clear</a>
                            </div>
                        </form>
                        <table id="dataTable" class="table table-hover">
                            <thead>
                            <tr>                                
                                <th>ID</th> 
                                <th>Name</th> 
                                <th>Owner</th>
                                <th>Address</th>
                                <th>Logo</th>
                                <th>Verified</th>
                                <th>Status</th>                             
                                <th class="actions text-right"><?php echo e(__('voyager::generic.actions')); ?></th>
                            </tr>
                            </thead>
                            <tbody>
                                <?php $__currentLoopData = $many; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $one): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <?php
                                    if ($one->verified == 1) {
                                        $color = 'rgba(33, 152, 31, 0.35)';
                                        $verified = 'Verified';
                                    }

                                    if ($one->verified == 2) {
                                        $color = 'rgba(255, 0, 0, 0.35)';
                                        $verified = 'Not verified';
                                    }
                                ?>
                                <tr style="font-weight:400;color:#000;background-color:<?php echo e($color); ?>">
                                    <td><?php echo e($one->id); ?></td>                                    
                                    <td><?php echo e($one->name); ?></td>
                                    <td>
                                        <?php if($one->owner): ?>
                                        <a href="<?php echo e(route('voyager.users.edit', $one->owner->id)); ?>"><?php echo e($one->owner->company); ?></a>
                                        <?php endif; ?>
                                    </td>
                                    <td><?php echo e(app('App\Services\Userdata')::getUserAddress($one)); ?></td>
                                    <td>
                                        <img src="<?php echo e(App\Services\Img::getIcon($one->logo)); ?>" style="width:50px; height:auto; clear:both; display:block; padding:2px;border-radius: 50%;" />
                                    </td>
                                    <td><strong><?php echo e($verified); ?></strong></td> 
                                    <td>
                                        <?php
                                            if ($one->status == 1) $status = 'Active';
                                            if ($one->status == 2) $status = 'Suspended';
                                            if ($one->status == 3) $status = 'Blocked';
                                            if ($one->status == 4) $status = 'Removed';
                                        ?>
                                        <strong><?php echo e($status); ?></strong>
                                    </td>
                                    <td class="no-sort no-click bread-actions">
                                        <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('delete', $model)): ?>
                                            <div class="btn btn-sm btn-danger pull-right delete" data-id="<?php echo e($one->id); ?>">
                                                <i class="voyager-trash"></i> <?php echo e(__('voyager::generic.delete')); ?>

                                            </div>
                                        <?php endif; ?>
                                        <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('edit', $model)): ?>
                                            <a href="<?php echo e(route('voyager.'.$type.'.edit', [$one->id, $requests])); ?>" class="btn btn-sm btn-primary pull-right edit">
                                                <i class="voyager-edit"></i> <?php echo e(__('voyager::generic.edit')); ?>

                                            </a>
                                        <?php endif; ?>                                        
                                    </td>
                                </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="panel-body">                        
                        <div class="pull-right"><?php echo e($many->appends(\Request::except('page'))->links()); ?></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal modal-danger fade" tabindex="-1" id="delete_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="<?php echo e(__('voyager::generic.close')); ?>">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">
                        <i class="voyager-trash"></i> <?php echo e(__('voyager::generic.delete_question')); ?>?
                    </h4>
                </div>
                <div class="modal-footer">
                    <form action="#" id="delete_form" method="POST">
                        <?php echo e(method_field("DELETE")); ?>

                        <?php echo e(csrf_field()); ?>

                        <input type="submit" class="btn btn-danger pull-right delete-confirm" value="<?php echo e(__('voyager::generic.delete_this_confirm')); ?>">
                    </form>
                    <button type="button" class="btn btn-default pull-right" data-dismiss="modal"><?php echo e(__('voyager::generic.cancel')); ?></button>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>   
    <script> 
        $('td').on('click', '.delete', function (e) {
            $('#delete_form')[0].action = '<?php echo e(route('voyager.'.$type.'.destroy', ['id' => '__item', $requests])); ?>'.replace('__item', $(this).data('id'));

            $('#delete_modal').modal('show');
        });

        function saveServiceCharge() {
            var value = $('#service_charge').val();

            if (value && value != '') {
                $.ajax({
                    url: '/ajax/getData',
                    type: 'post',
                    dataType: 'json',
                    headers: {
                        'X-CSRF-TOKEN': '<?php echo e(csrf_token()); ?>'
                    },
                    data: {action:'changeServiceCharge', value: value, key: 'admin.service_charge'}
                });
            }
        }
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('voyager::master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/dentist2lab.com/resources/views/Backend/branches/browse.blade.php ENDPATH**/ ?>