<?php $__env->startSection('breadcrumbs'); ?>
	<?php echo $__env->make('Frontend.templates.breadcrumbs', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
	<h1><?php echo e($seo->name); ?></h1>
	<div class="page-text">
		<?php echo $seo->description; ?>

	</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('Frontend/master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/d2l.sitepark.ua/resources/views/Frontend/pages/article.blade.php ENDPATH**/ ?>