<link rel="stylesheet" href="<?php echo e(mix('/css/reviews.css')); ?>">
<div id="order_reviews_block">
	<?php $__currentLoopData = $data->reviews; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $review): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
		<?php if($review->status == 1): ?>
			<div id="order_review_<?php echo e($review->id); ?>" class="fx fxb order-review-line">
				<div class="fx fxc order-review-left">
					<div class="bi order-review-avatar" style="background-image: url('<?php echo e(\App\Services\Img::getIcon($review->user->avatar)); ?>');"></div>
				</div>
				<div class="order-review-right">
					<div class="fx fac order-review-top">
						<div class="order-review-user"><?php echo e($review->user->name .' '. $review->user->surname); ?></div>
						<div class="order-review-date">
							<?php echo e(date('m-d-Y', strtotime($review->created_at))); ?>

						</div>
						<?php if($review->user_id == Auth::user()->id && $data->review_access && $data->review_access->can_edit): ?>
							<div class="fx review-rating-stars">
								<?php $show_star = true; ?>
								<?php for($i = 1; $i <= 5; $i++): ?>
								    <input type="radio" name="review_star<?php echo e($review->id); ?>" id="review_rating_<?php echo e($review->id); ?>_<?php echo e($i); ?>" class="review-rating-radio review-rating<?php echo e($review->id); ?> <?php if($show_star): ?><?php echo e('review-rating-radioAct'); ?><?php endif; ?>" value="<?php echo e($i); ?>" onchange="showReviewStars(<?php echo e($i); ?>, <?php echo e($review->id); ?>)" title="<?php echo e($i); ?>" <?php if($review->rating == $i): ?><?php echo e('checked'); ?><?php endif; ?>>
								    <?php if ($review->rating == $i) $show_star = false; ?>
								<?php endfor; ?>							
							</div>
						<?php else: ?>
							<div class="order-review-rating order-review-rating<?php echo e($review->rating); ?>"></div>
						<?php endif; ?>
					</div>
					<div class="order-review-center">
						<?php if($review->user_id == Auth::user()->id && $data->review_access && $data->review_access->can_edit): ?>
							<textarea id="order_review<?php echo e($review->id); ?>" class="form-input form-input-required order-review-edit" value="" placeholder="<?php echo e(__('translations.review')); ?>" maxlength="1000" disabled><?php echo e($review->review); ?></textarea>
						<?php else: ?>
							<?php echo e($review->review); ?>

						<?php endif; ?>
					</div>
					<div class="fx fxb order-review-bottom">
						<div class="order-review-edited">
							<?php if($review->updated_at > $review->created_at): ?>
								<?php echo e(__('translations.edited_at')); ?>: <?php echo e(date('m-d-Y', strtotime($review->updated_at))); ?>

							<?php endif; ?>
						</div>
						<div class="order-review-btns">
							<?php if($review->user_id == Auth::user()->id && $data->review_access && $data->review_access->can_edit): ?>
								<div class="btn btn-small" onclick="editReview(<?php echo e($review->id); ?>, 'order_review')"><?php echo e(__('translations.edit')); ?></div>
							<?php elseif($review->moderation == 2): ?>
								<div><?php echo e(__('translations.review_need_moderation')); ?></div>
							<?php else: ?> 
								<div class="tooltip-block review-claim">
									<span class="tooltip-icon"></span>
									<div class="content-tooltip">
										<div onclick="setReviewToModeration(<?php echo e($review->id); ?>, '<?php echo e(__('translations.claim1')); ?>','order_review')"><?php echo e(__('translations.claim1')); ?></div>
										<div onclick="setReviewToModeration(<?php echo e($review->id); ?>, '<?php echo e(__('translations.claim2')); ?>','order_review')"><?php echo e(__('translations.claim2')); ?></div>
										<div onclick="setReviewToModeration(<?php echo e($review->id); ?>, '<?php echo e(__('translations.claim3')); ?>','order_review')"><?php echo e(__('translations.claim3')); ?></div>
										<div onclick="setReviewToModeration(<?php echo e($review->id); ?>, '<?php echo e(__('translations.claim4')); ?>','order_review')"><?php echo e(__('translations.claim4')); ?></div>
										<div onclick="setReviewToModeration(<?php echo e($review->id); ?>, '<?php echo e(__('translations.claim5')); ?>','order_review')"><?php echo e(__('translations.claim5')); ?></div>
									</div>
								</div>
							<?php endif; ?>
						</div>
					</div>
				</div>
				<div id="order_comments_<?php echo e($review->id); ?>" class="order-review-comments">
					<?php if($review->comments && count($review->comments)): ?>
						<?php $__currentLoopData = $review->comments; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $comment): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<div id="order_review_<?php echo e($review->id); ?>_<?php echo e($comment->id); ?>" class="fx fxb order-review-comment-line">
								<div class="fx fxc order-review-left">
									<div class="bi order-review-avatar" style="background-image: url('<?php echo e(\App\Services\Img::getIcon($comment->user->avatar)); ?>');"></div>
								</div>
								<div class="order-review-right">
									<div class="fx fac order-review-top">
										<div class="order-review-user"><?php echo e($comment->user->name .' '. $comment->user->lastname); ?></div>
										<div class="order-review-date">
											<?php echo e(date('m-d-Y', strtotime($comment->created_at))); ?>

										</div>
									</div>
									<div class="order-review-center">
										<?php if($comment->user_id == Auth::user()->id && $data->review_access && $data->review_access->can_edit): ?>
											<textarea id="order_review_comment<?php echo e($comment->id); ?>" class="form-input form-input-required" value="" placeholder="<?php echo e(__('translations.comment')); ?>" maxlength="1000"><?php echo e($comment->review); ?></textarea>
										<?php else: ?>
											<?php echo e($comment->review); ?>

										<?php endif; ?>
									</div>
									<div class="fx fxb order-review-bottom">
										<div class="order-review-edited">
											<?php if($comment->updated_at > $comment->created_at): ?>
												<?php echo e(__('translations.edited_at')); ?>: <?php echo e(date('m-d-Y', strtotime($comment->updated_at))); ?>

											<?php endif; ?>
										</div>
										<div class="order-review-btns">
											<?php if($comment->user_id == Auth::user()->id && $data->review_access && $data->review_access->can_edit): ?>
												<div class="btn btn-small" onclick="editReview(<?php echo e($comment->id); ?>, 'order_review_comment')"><?php echo e(__('translations.edit')); ?></div>
											<?php elseif($comment->moderation == 2): ?>
												<div><?php echo e(__('translations.review_need_moderation')); ?></div>
											<?php else: ?> 
												<div class="tooltip-block review-claim">
													<span class="tooltip-icon"></span>
													<div class="content-tooltip">
														<div onclick="setReviewToModeration(<?php echo e($review->id); ?>, '<?php echo e(__('translations.claim1')); ?>','order_review_comment')"><?php echo e(__('translations.claim1')); ?></div>
														<div onclick="setReviewToModeration(<?php echo e($review->id); ?>, '<?php echo e(__('translations.claim2')); ?>','order_review_comment')"><?php echo e(__('translations.claim2')); ?></div>
														<div onclick="setReviewToModeration(<?php echo e($review->id); ?>, '<?php echo e(__('translations.claim3')); ?>','order_review_comment')"><?php echo e(__('translations.claim3')); ?></div>
														<div onclick="setReviewToModeration(<?php echo e($review->id); ?>, '<?php echo e(__('translations.claim4')); ?>','order_review_comment')"><?php echo e(__('translations.claim4')); ?></div>
														<div onclick="setReviewToModeration(<?php echo e($review->id); ?>, '<?php echo e(__('translations.claim5')); ?>','order_review_comment')"><?php echo e(__('translations.claim5')); ?></div>
													</div>
												</div>
											<?php endif; ?>
										</div>
									</div>
								</div>
							</div>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					<?php endif; ?>
				</div>
			</div>

			<?php if(Auth::user()->type == config('types.lab') && $data->review_access && !in_array($review->id, $data->review_access->cant_comment)): ?>
				<div id="order_review_add<?php echo e($review->id); ?>" class="order-review-add">
					<div class="btn" onclick="addCommentToReview(<?php echo e($review->id); ?>, <?php echo e($data->review_access->order_id); ?>)"><?php echo e(__('translations.add_comment_review')); ?></div>
				</div>
			<?php endif; ?>	
		<?php endif; ?>
	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
</div>
<?php if(Auth::user()->type == config('types.clinic') && $data->review_access && $data->review_access->can_review): ?>
	<div id="order_review_add" class="order-review-add">
		<div class="btn" onclick="addReview(<?php echo e($data->review_access->order_id); ?>)"><?php echo e(__('translations.add_review')); ?></div>
	</div>
<?php endif; ?><?php /**PATH /var/www/d2l.sitepark.ua/resources/views/Frontend/templates/review_block.blade.php ENDPATH**/ ?>