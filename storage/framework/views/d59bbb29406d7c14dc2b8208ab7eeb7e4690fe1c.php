function showOnlinePaymentForm(type, prefix) {
	var html = '';

	if (!prefix) {
		var prefix = 'payment_';	
	}

	if (type == 1) {
		html += '<p></p><p><?php echo e(__('translations.bank_transfer_text')); ?></p><p></p>';

		$(document).find('#service_charge_msg').css('display', 'none');
	}

	if (type == 2) {
		html += '<div class="popup-payment-line">';
		html += '<label for="payment_card"><?php echo e(__('translations.payment_card')); ?></label>';
		html += '<input type="text" name="'+prefix+'params[card]" id="payment_card" value="" placeholder="XXXXXXXXXXXXXXXX" onkeypress="checkSymbols(event);" '+(prefix ? 'onchange="validateCardNum(this.value) ? console.log(1) : this.value = \'\'"' : '')+' required="required" />';
		html += '</div>';
		html += '<div class="popup-payment-line popup-payment-half">';
		html += '<label for="payment_exp"><?php echo e(__('translations.payment_exp')); ?></label>';
		html += '<input type="text" name="'+prefix+'params[exp]" id="payment_exp" value="" placeholder="XX/XX" '+(prefix ? 'onchange="validateExpDate(this.value) ? console.log(1) : this.value = \'\'"' : '')+' required="required"/>';
		html += '</div>';
		html += '<div class="popup-payment-line popup-payment-half">';
		html += '<label for="payment_cvv"><?php echo e(__('translations.payment_cvv')); ?></label>';
		html += '<input type="text" name="'+prefix+'params[cvv]" id="payment_cvv" value="" placeholder="XXX" required="required"/>';
		html += '</div>';

		$(document).find('#service_charge_msg').css('display', 'inline-block');
	}

	$('#online_payment_form').html(html);

	//$(document).find('#payment_card').mask('9999-9999-9999-9999');
	$(document).find('#payment_exp').mask('99/99');
	$(document).find('#payment_cvv').mask('999');
}

function validateCardNum(num) {
  	var result = false;

	if (num == '') return result; 

	num = num.replace (/\s/g, "");

  	var card_params = new Array();

  	card_params[0] = {
		//LaserCard
    	length: [16,17,18,19], 
        prefixes: [6304,6706,6771,6709]
    };

	card_params[1] = {
    	//Switch
    	length: [16,18,19], 
        prefixes: [4903,4905,4911,4936,564182,633110,6333,6759]			
    };

	card_params[2] = {
	    	//Maestro
	    	length: [12,13,14,15,16,18,19], 
	        prefixes: [5018,5020,5038,6304,6759,6761,6762,6763]	    	
	};
	
	card_params[3] = {
	    	//CarteBlanche
	    	length: [14], 
	        prefixes: [300,301,302,303,304,305]
	};
	
	card_params[4] = {
	    	//VisaElectron
	    	length: [16], 
	        prefixes: [4026,417500,4508,4844,4913,4917]	    	
	};
	
	card_params[5] = {
	    	//Discover
	    	length: [16], 
	        prefixes: [6011,622,64,65]
	};
	
	card_params[6] = {
	    	//Solo
	    	length: [16,18,19], 
	        prefixes: [6334,6767]	    	
	};
	
	card_params[7] = {
	    	//enRoute
	    	length: [15], 
	        prefixes: [2014,2149]
	};
	
	card_params[8] = {
	    	//JCB
	    	length: [16], 
	        prefixes: [35]
	};

	card_params[9] = {
	    	//MasterCard, 
	        length: [16], 
	        prefixes: [51,52,53,54,55]
	};

	card_params[10] = {
	    	//DinersClub
	    	length: [14,15,16], 
	        prefixes: [36,38,54,55]
	};

	card_params[11] = {
	    	//AmEx
	    	length: [15], 
	        prefixes: [34]
	};

	card_params[12] = {
	    	//visa
	        length: [16], 
	        prefixes: [4]	    	
	};

  	for(i = 0; i < card_params.length; i++) {
  		var length_ok = false;
  		var prefix_ok = false;

  		for(j = 0; j < card_params[i]['length'].length; j++) {
  			if (num.length == card_params[i]['length'][j]) {
				length_ok = true;
				break; 
  			}
  		}

  		for(j = 0; j < card_params[i]['prefixes'].length; j++) {
  			var exp = new RegExp ("^" + card_params[i]['prefixes'][j]);

			if (exp.test(num)) {
				prefix_ok = true;
				break; 
  			}
  		}

  		if (length_ok && prefix_ok) {
  			result = true;
  			break;
  		}
  	}

	return result;
}

function validateExpDate(num) {
	if (num == '') return false;
	num = num.replace (/\s/g, "");

	num = num.split('/');

	if (num[0] > 12) return false;
	if (num[1] < 20) return false;

	return true;
}

function checkSymbols(evt) {
	 var theEvent = evt || window.event;

	if (theEvent.type === 'paste') {
	  	key = event.clipboardData.getData('text/plain');
	} else {
	  	var key = theEvent.keyCode || theEvent.which;
	  	key = String.fromCharCode(key);
	}

	var regex = /[0-9]|\./;

	if( !regex.test(key) ) {
		theEvent.returnValue = false;
		if(theEvent.preventDefault) theEvent.preventDefault();
	}
}<?php /**PATH /var/www/d2l.sitepark.ua/resources/views/Frontend/templates/online_payment_form_script.blade.php ENDPATH**/ ?>