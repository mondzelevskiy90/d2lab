<?php $__env->startSection('page_title', __('voyager::generic.'.(isset($data->id) ? 'edit' : 'add')).' '.$data->name); ?>

<?php $__env->startSection('css'); ?>
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('page_header'); ?>
    <h1 class="page-title">
        <i class="voyager-edit"></i>
        <?php echo e(__('voyager::generic.'.(isset($data->id) ? 'edit' : 'add')).' '.$data->name); ?>

    </h1>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="page-content container-fluid">
        <form class="form-edit-add" role="form" action="<?php if(!is_null($data->id)): ?><?php echo e(route('voyager.'.$type.'.update', [$data->id, $requests])); ?><?php else: ?><?php echo e(route('voyager.'.$type.'.store', [$requests])); ?><?php endif; ?>" method="POST" enctype="multipart/form-data" autocomplete="off">            
            <?php if(isset($data->id)): ?>
                <?php echo e(method_field("PUT")); ?>

            <?php endif; ?>
            <?php echo e(csrf_field()); ?>


            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-bordered">  
                        <div class="panel-body">                 
                            <?php if(count($errors) > 0): ?>
                                <div class="alert alert-danger">
                                    <ul>
                                        <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <li><?php echo e($error); ?></li>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </ul>
                                </div>
                            <?php endif; ?>                                                    
                            <div class="form-group">
                                <label for="status"><?php echo e(__('adm.status')); ?></label>
                                <select class="form-control" name="status">        
                                    <option value="1" <?php if(isset($data->status) && $data->status == 1): ?> selected <?php endif; ?>>Active</option>       
                                    <option value="2" <?php if(isset($data->status) && $data->status == 2): ?> selected <?php endif; ?>>Disactive</option>                                      
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="name"><?php echo e(__('adm.name')); ?></label>
                                <input type="text" class="form-control" id="name" name="name" placeholder="<?php echo e(__('adm.name')); ?>" value="<?php echo e(old('name', $data->name ?? '')); ?>">
                            </div> 
                            <div class="form-group">
                                <label for="prescription_description"><?php echo e(__('adm.description')); ?></label>
                                <textarea class="richTextBox form-control" id="prescription_description" name="prescription_description" placeholder="<?php echo e(__('adm.description')); ?>"><?php echo e(old('prescription_description', $data->prescription_description ?? '')); ?></textarea>
                            </div>
                            <div class="form-group">
                                <label for="sort_order"><?php echo e(__('adm.sort_order')); ?></label>
                                <input type="text" class="form-control" id="sort_order" name="sort_order" placeholder="<?php echo e(__('adm.sort_order')); ?>" value="<?php echo e(old('sort_order', $data->sort_order ?? 0)); ?>">
                            </div> 
                            <div class="form-group">
                                <label for="default_list">Show for default price list?</label>
                                <select class="form-control" name="default_list">        
                                    <option value="1" <?php if(isset($data->id) && $data->default_list == 1): ?> selected <?php endif; ?>>Yes</option>       
                                    <option value="2" <?php if(isset($data->id) && $data->default_list == 2): ?> selected <?php endif; ?>>No</option>                                      
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="price_for">Price for</label>
                                <select class="form-control" name="price_for">        
                                    <option value="1" <?php if(isset($data->id) && $data->price_for == 1): ?> selected <?php endif; ?>>For 1 item (e.g. 1 tooth)</option>       
                                    <option value="2" <?php if(isset($data->id) && $data->price_for == 2): ?> selected <?php endif; ?>>For service</option>                                      
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="available_statuses">Selected statuses for SVG</label>
                                <select class="form-control" name="available_statuses">        
                                    <option value="1,2,3,4,5,6,7" <?php if(isset($data->id) && $data->available_statuses == '1,2,3,4,5,6,7'): ?> selected <?php endif; ?>>All elements</option>       
                                    <option value="1,2,7" <?php if(isset($data->id) && $data->available_statuses == '1,2,7'): ?> selected <?php endif; ?>>Only active and empty</option> 
                                    <option value="1,7" <?php if(isset($data->id) && $data->available_statuses == '1,7'): ?> selected <?php endif; ?>>Only empty</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="available_options">Selected options</label>
                                <select class="form-control" name="available_options">        
                                    <option value="0" <?php if(isset($data->id) && $data->available_options == 0): ?> selected <?php endif; ?>>None</option>       
                                    <option value="1" <?php if(isset($data->id) && $data->available_options == 1): ?> selected <?php endif; ?>>Lower</option>
                                    <option value="2" <?php if(isset($data->id) && $data->available_options == 2): ?> selected <?php endif; ?>>Upper</option>
                                    <option value="3" <?php if(isset($data->id) && $data->available_options == 3): ?> selected <?php endif; ?>>Both</option>
                                </select>
                            </div>
                            <div class="form-group" style="display:none;">
                                <label for="industry_id">Industry</label>
                                <!--<select class="form-control" id="industry_id" name="industry_id">
                                    <?php $__currentLoopData = $industries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $industry): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
                                        <option value="<?php echo e($industry->id); ?>" <?php if(isset($data->id) && $data->industry_id == $industry->id): ?> selected <?php endif; ?>><?php echo e($industry->name); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>                 
                                </select> -->
                                <input type="hidden" name="industry_id" value="2"/>
                            </div>
                            <div class="form-group">
                                <label for="parent_id">Parent (leave empty if main)</label>
                                <select class="form-control" id="parent_id" name="parent_id">        
                                    <option value="0" <?php if(isset($data->parent_id) && $data->parent_id == 0): ?> selected <?php endif; ?>>---- </option>
                                    <?php $__currentLoopData = $parents; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $parent): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
                                        <?php if (isset($data->id) && $data->id == $parent->id) continue; ?>
                                        <option value="<?php echo e($parent->id); ?>" <?php if(isset($data->id) && $data->parent_id == $parent->id): ?> selected <?php endif; ?>><?php echo e($parent->name); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>                 
                                </select>
                            </div>  
                            <div class="form-group">
                                <label for="parent_id">Service options</label>
                                <div id="options_list">
                                    <?php if(count($options)): ?>
                                        <?php $__currentLoopData = $options; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $option): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <div>
                                                <label>
                                                    <input type="checkbox" name="options[<?php echo e($option->id); ?>]" id="option<?php echo e($option->id); ?>" <?php if(isset($data->id) && isset($option->selected->id)): ?> checked="checked" <?php endif; ?>> <?php echo e($option->name); ?>

                                                </label>
                                            </div>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    <?php else: ?>
                                        <p>There are no any available options for selected industry!</p>
                                    <?php endif; ?>
                                </div>
                            </div>                         
                        </div>
                    </div>
                </div>                
            </div>

            <button type="submit" class="btn btn-primary pull-right save">
                <?php echo e(__('voyager::generic.save')); ?>

            </button>
        </form>

       
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>
    <script>
        $('document').ready(function () {
            $('.toggleswitch').bootstrapToggle();

            $('#industry_id').on('change', function(){
                $.ajax({
                    url: '<?php echo e(route('voyager.getservices')); ?>',
                    type: 'post',
                    dataType: 'json',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    data:{industry: $(this).val()},
                    success: function(data) {
                        html = '<option value="0">---- </option>';

                        if (data) {
                            for (index in data) {
                                html += '<option value="'+data[index]['id']+'">'+data[index]['name']+'</option>';
                            }

                            $('#parent_id').html(html);                           
                        }                            
                    },
                    error: function(){
                        console.log("ajax_error");
                    }
                });

                $.ajax({
                    url: '<?php echo e(route('voyager.getoptions')); ?>',
                    type: 'post',
                    dataType: 'json',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    data:{industry: $(this).val()},
                    success: function(data) {
                        html = '<p>There are no any available options for selected industry!</p>';

                        if (data && data[0]) {
                            html = '';

                            for (index in data) {
                                html += '<div>';
                                html += '<label>';
                                html += '<input type="checkbox" name="options['+data[index]['id']+']" id="option'+data[index]['id']+'"> '+data[index]['name']+'';
                                html += '</label>';
                                html += '</div>';
                            }                        
                        } 

                        $('#options_list').html(html);                            
                    },
                    error: function(){
                        console.log("ajax_error");
                    }
                });
            });
        });
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('voyager::master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/d2l.sitepark.ua/resources/views/Backend/industry-services/edit-add.blade.php ENDPATH**/ ?>