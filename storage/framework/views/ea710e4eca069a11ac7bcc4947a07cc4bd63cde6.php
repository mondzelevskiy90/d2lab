<p>Warning!</p>
<p>Lab created personal price for your clinic! Some services has new price:</p>
<ul>
	<?php $__currentLoopData = $services; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $service): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
		<li><?php echo e($service['name']); ?> - <?php echo e($service['price']); ?> USD</li>
	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
</ul><?php /**PATH /var/www/dentist2lab.com/resources/views/Frontend/emails/dentist_price_changed.blade.php ENDPATH**/ ?>