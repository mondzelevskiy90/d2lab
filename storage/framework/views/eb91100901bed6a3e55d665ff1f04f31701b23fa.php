<div class="panel panel-half dashboard-item dashboard-item-half dashboard-item-<?php echo e($code); ?>">
	<div class="panel-head"><?php echo e(__('translations.orders_list')); ?></div>
	<?php if($content['text'] && $content['text'] != ''): ?>
		<div class="panel-intro"><?php echo $content['text']; ?></div>
	<?php endif; ?>
	<div class="panel-content">
		<?php if($content['labels'] && $content['pies']): ?>
			<div style="width:100%;height:400px;margin: auto">
				<canvas id="diagram" width="200" height="400"></canvas>	
			</div>
			<script>
				document.addEventListener('DOMContentLoaded', function(){
					var ctx = document.getElementById('diagram');

					var labels_arr = [],
						pies_arr = [],
						labels = <?php echo $content['labels']; ?>,
						pies = <?php echo $content['pies']; ?>;

					for(var i in labels) {
						if (pies[i] != 0) {
							labels_arr.push(labels[i]);
							pies_arr.push(pies[i]);
						}
					}
					
					var myChart = new Chart(ctx, {
					    type: 'pie',
					    data: {
					        labels: labels_arr,
					        datasets: [{
					            label: '',
					            data: pies_arr,
					            backgroundColor: [
					                'rgba(255, 0, 0, 0.2)',
					                'rgba(135, 15, 235, 0.2)',
					                'rgba(255, 196, 64, 0.2)',
					                'rgba(75, 192, 192, 0.2)',
					                'rgba(165, 45, 105, 0.2)',
					                'rgba(153, 102, 255, 0.2)',
					                'rgba(35, 159, 64, 0.2)',
					                'rgba(205, 30, 205, 0.2)',
					                'rgba(255, 85, 0, 0.2)',
					                'rgba(85, 15, 235, 0.2)',
					                'rgba(55, 255, 74, 0.2)',
					                'rgba(75, 102, 92, 0.2)',
					                'rgba(5, 45, 105, 0.2)',
					                'rgba(3, 102, 255, 0.2)',
					                'rgba(255, 9, 54, 0.2)',
					                'rgba(105, 30, 205, 0.2)',
					                'rgba(155, 155, 155, 0.2)',
					                'rgba(75, 215, 135, 0.2)',
					                'rgba(255, 196, 64, 0.2)',
					                'rgba(85, 102, 202, 0.2)',
					                'rgba(255, 5, 255, 0.2)',
					                'rgba(53, 102, 255, 0.2)',
					                'rgba(25, 249, 64, 0.2)',
					                'rgba(205, 30, 5, 0.2)'
					            ],
					            borderColor: [
					                'rgba(255, 0, 0, 1)',
					                'rgba(135, 15, 235, 1)',
					                'rgba(255, 196, 64, 1)',
					                'rgba(75, 192, 192, 1)',
					                'rgba(165, 45, 105, 1)',
					                'rgba(153, 102, 255, 1)',
					                'rgba(35, 159, 64, 1)',
					                'rgba(205, 30, 205, 1)',
					                'rgba(255, 85, 0, 1)',
					                'rgba(85, 15, 235, 1)',
					                'rgba(55, 255, 74, 1)',
					                'rgba(75, 102, 92, 1)',
					                'rgba(5, 45, 105, 1)',
					                'rgba(3, 102, 255, 1)',
					                'rgba(255, 9, 54, 1)',
					                'rgba(105, 30, 205, 1)',
					                'rgba(155, 155, 155, 1)',
					                'rgba(75, 215, 135, 1)',
					                'rgba(255, 196, 64, 1)',
					                'rgba(85, 102, 202, 1)',
					                'rgba(255, 5, 255, 1)',
					                'rgba(53, 102, 255, 1)',
					                'rgba(25, 249, 64, 1)',
					                'rgba(205, 30, 5, 1)'
					            ],
					            borderWidth: 1
					        }]
					    },
					    options: {  
						    responsive: true,
						    maintainAspectRatio: false
						}
					});
				}, !1);
			</script>
		<?php else: ?> 
			<p><?php echo e(__('translations.no_orders_list')); ?></p>
		<?php endif; ?>
	</div>
</div>
<?php /**PATH /var/www/d2l.sitepark.ua/resources/views/Frontend/templates/dashboard/diagram.blade.php ENDPATH**/ ?>