<?php $__env->startSection('css'); ?>
	<link rel="stylesheet" href="/css/select.css">
	<link rel="stylesheet" href="<?php echo e(mix('/css/account.css')); ?>">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
	<form class="content-editing-form " method="POST" action="<?php echo e(isset($data->id) ? route('portfolio.edit', $data->id) : route('portfolio.create')); ?>" enctype="multipart/form-data" id="portfolio_form">
		<div id="panel_top_buttons" class="fx fac fxb">
			<?php echo $__env->make('Frontend.templates.breadcrumbs', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
			<div class="panel-top-buttons button-submit order-buttons">
				<a class="btn btn-small btn-grey" href="<?php echo e(route('portfolios', [$requests])); ?>">&larr; <?php echo e(__('translations.back')); ?></a>
				<input type="submit" id="submit" class="btn btn-small" name="submit" value="<?php if(isset($data->id)): ?><?php echo e(__('translations.save_changes')); ?><?php else: ?><?php echo e(__('translations.save')); ?><?php endif; ?>">
			</div>
		</div>
		<h1 class="tc"><?php echo e($seo->name); ?></h1>	
		<div class="fx fxb panel-body portfolio-edit-form">
			<?php echo e(csrf_field()); ?>			
			<div class="panel">	
				<?php $cnt = 0; ?>
			    <?php if(isset($data->id)): ?>
			    	<div class="form-line">
			    		<label for="branch"><?php echo e(__('translations.branch')); ?></label>
						<input type="text" id="branch" class="form-input" value="<?php echo e($branch->name); ?>" disabled="disabled">
			    	</div>
					<div class="form-line">
						<label for="name"><?php echo e(__('translations.name')); ?></label>				
						<input type="text" name="name" id="name" class="form-input" placeholder="<?php echo e(__('translations.name')); ?>" value="<?php echo e(old('name', $data->name)); ?>" required="required">
						<?php if($errors->has('name')): ?>
		                    <div class="error"><?php echo e($errors->first('name')); ?></div>
		                <?php endif; ?>				
					</div>
					<br>
					<div class="account-images-list">
						<div class="panel-head account-images-head"><?php echo e(__('translations.portfolio_cats')); ?></div>
						<div class="account-images-content">							
							<?php $__currentLoopData = $items; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
								<div class="account-images-line">
									<div class="account-images-lineHead"><?php echo e($item['name']); ?></div>
									<div class="fx account-images-lineImages" id="images_block<?php echo e($item['id']); ?>">
										<?php $__currentLoopData = $item['images']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $image): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>											
											<div id="image_block<?php echo e($cnt); ?>" class="fx fac account-images-lineImage">
												<div class="account-images-lineRemove" onclick="removeImageBlock('<?php echo e($cnt); ?>', '<?php echo e($item['id']); ?>')" title="<?php echo e(__('translations.delete_image')); ?>">
													<div></div>
												</div>
												<img src="<?php echo e(App\Services\Img::getThumb($image['image'])); ?>">
												<input type="hidden" name="images[<?php echo e($item['id']); ?>][<?php echo e($cnt); ?>]" value="<?php echo e($image['image']); ?>">
											</div>
											<?php $cnt++; ?>
										<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>										
										<div id="images_block_add<?php echo e($item['id']); ?>" class="account-images-lineAdd" <?php if(count($item['images']) >= 8): ?> style="display:none;" <?php endif; ?> >
											<input type="file" name="image_value" onchange="addImageToBlock($(this), '<?php echo e($item['id']); ?>')" title="<?php echo e(__('translations.add_image')); ?>" value="" accept="image/jpeg,image/jpg,image/png">
										</div>
									</div>
								</div>
							<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>							
						</div>
					</div>
			    <?php else: ?> 
					<div class="form-line">
						<label for="branch_id"><?php echo e(__('translations.branch')); ?></label>
						<select name="branch_id" id="branch_id" class="form-controll" required="required">
							<?php $__currentLoopData = $branches; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $branch): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
								<option value="<?php echo e($branch->id); ?>"><?php echo e($branch->name); ?></option>
							<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
						</select>
					</div>
			    <?php endif; ?>					
			</div>
		</div>
	</form>	
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
	<script>
		let cnt = <?php echo e($cnt); ?>;

		function addImageToBlock(elem, id) {
			var files = elem.prop('files')[0];

			if (!files || !elem.val()) return false;

			if (files.size > 5000000 || (files.type != 'image/jpeg' && files.type != 'image/png')) {
				showPopup('<?php echo e(__('translations.portfolio_file_error')); ?>');

				return false;
			}

			let form = $('#portfolio_form');
    		let form_data = new FormData();

    		form_data.append('image', files);
    		form_data.append('id', <?php echo e(Auth::user()->id); ?>);
    		form_data.append('action', 'saveImage');

			$.ajax({
				url: '/ajax/getData',
				type: 'post',
                dataType: 'json',
                processData: false,
            	contentType: false,
                headers: {
                    'X-CSRF-TOKEN': '<?php echo e(csrf_token()); ?>'
                },
                data: form_data,
                success: function(data) {
                	let html = '';

                	html += '<div id="image_block'+cnt+'" class="fx fac account-images-lineImage">';
					html += '<div class="account-images-lineRemove" onclick="removeImageBlock('+cnt+', '+id+')" title="<?php echo e(__('translations.delete_image')); ?>"><div></div></div>';
					html += '<img src="<?php echo e(url('/storage')); ?>/'+data['thumb']+'">';
					html += '<input type="hidden" name="images['+id+']['+cnt+']" value="'+data['image']+'">';
					html += '</div>';

					$('#images_block_add'+id).before(html);

					if ($('#images_block'+id+' > div').length < 9) {
						$('#images_block_add'+id).css('display', 'block');
					} else {
						$('#images_block_add'+id).css('display', 'none');
					}

					cnt++;
                }
			});

			
		}

		function removeImageBlock(id, parent_id) {
			$('#image_block'+id).remove();

			if ($('#images_block'+parent_id+' > div').length < 9) $('#images_block_add'+parent_id).css('display', 'block');
		}	
	</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('Frontend/account_master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/d2l.sitepark.ua/resources/views/Frontend/pages/account/portfolio/edit.blade.php ENDPATH**/ ?>