<?php $__env->startSection('css'); ?>
	<link rel="stylesheet" href="/css/select.css">
	<link rel="stylesheet" href="/css/daterangepicker.css">
	<link rel="stylesheet" href="<?php echo e(mix('/css/account.css')); ?>">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('breadcrumbs'); ?>
	<?php echo $__env->make('Frontend.templates.breadcrumbs', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
	<h1 style="display: none;"><?php echo e($seo->name); ?></h1>	
	<div class="panel panel-transparent">
		<div id="order_filters_block" class="panel-filters" style="display: none;">
			<form method="GET" action="<?php echo e(route('orders')); ?>" id="filters_block" class="fx fac">		
				<?php echo e(csrf_field()); ?>	
				<?php if($branches && count($branches)): ?>
					<div class="form-filter">
						<select name="branch" id="branch" class="form-input" onchange="document.getElementById('filters_block').submit();">	
							<option value="0"> -- <?php echo e(__('translations.branch')); ?> --</option>
							<?php $__currentLoopData = $branches; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $branch): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
								<?php if($branch->status != 4): ?>
									<option value="<?php echo e($branch->id); ?>" <?php if(isset($filters['branch']) && $filters['branch'] == $branch->id): ?><?php echo e('selected="selected"'); ?><?php endif; ?>><?php echo e($branch->name); ?></option>
								<?php endif; ?>
							<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
						</select>		
					</div>
				<?php endif; ?>	
				<?php if($branch_staff && count($branch_staff)): ?>
					<div class="form-filter">
						<select name="branch_staff" id="branch_staff" class="form-input">	
							<option value="0"> -- <?php echo e(__('translations.branch_staff')); ?> --</option>
							<?php $__currentLoopData = $branch_staff; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $branch_user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
								<?php if($branch_user->status != 5 && $branch_user->user): ?>
									<option value="<?php echo e($branch_user->user_id); ?>" <?php if(isset($filters['branch_staff']) && $filters['branch_staff'] == $branch_user->user_id): ?><?php echo e('selected="selected"'); ?><?php endif; ?>><?php echo e($branch_user->user->name.' '.$branch_user->user->surname); ?></option>
								<?php endif; ?>
							<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
						</select>		
					</div>
				<?php endif; ?>		
				<div class="form-filter">
					<select name="status" id="status" class="form-input">	
						<option value="0"> -- <?php echo e(__('translations.status')); ?> --</option>
						<?php $__currentLoopData = $order_statuses; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $status): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<option value="<?php echo e($status->id); ?>" <?php if(isset($filters['status']) && $filters['status'] == $status->id): ?><?php echo e('selected="selected"'); ?><?php endif; ?>><?php echo e($status->name); ?></option>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					</select>		
				</div>
				<div class="form-filter">
					<select name="order_type" id="order_type" class="form-input">	
						<option value="0"> -- <?php echo e(__('translations.order_type')); ?> --</option>
						<?php $__currentLoopData = $order_types; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $type): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<option value="<?php echo e($type->id); ?>" <?php if(isset($filters['order_type']) && $filters['order_type'] == $type->id): ?><?php echo e('selected="selected"'); ?><?php endif; ?>><?php echo e($type->name); ?></option>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					</select>		
				</div>
				<div class="form-filter">
					<select name="payment_status" id="payment_status" class="form-input">	
						<option value="0"> -- <?php echo e(__('translations.payment_status')); ?> --</option>
						<?php $__currentLoopData = $payment_statuses; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $payment_status): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<option value="<?php echo e($payment_status->id); ?>" <?php if(isset($filters['payment_status']) && $filters['payment_status'] == $payment_status->id): ?><?php echo e('selected="selected"'); ?><?php endif; ?>><?php echo e($payment_status->name); ?></option>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					</select>		
				</div>
				<div class="form-filter">
					<input type="text" name="search" id="search" class="form-input" placeholder="<?php echo e(__('translations.search_order_id')); ?>" value="<?php if(isset($filters['search']) && $filters['search']): ?><?php echo e($filters['search']); ?><?php endif; ?>">			
				</div>
				<div class="form-filter">
					<input type="text" name="patient" id="patient" class="form-input" placeholder="<?php echo e(__('translations.patient')); ?>" value="<?php if(isset($filters['patient']) && $filters['patient']): ?><?php echo e($filters['patient']); ?><?php endif; ?>">			
				</div>
				<div class="form-filter">
					<input type="text" name="ship_date" id="ship_date" class="form-input" placeholder="<?php echo e(__('translations.ship_date')); ?>" value="<?php if(isset($filters['ship_date']) && $filters['ship_date']): ?><?php echo e($filters['ship_date']); ?><?php endif; ?>">			
				</div>
				<div class="filter-buttons" style="text-align: right;">					
					<input type="submit" class="btn btn-small" value="<?php echo e(__('translations.filter')); ?>">
					<a href="<?php echo e(route('orders')); ?>" class="btn btn-danger btn-small"><?php echo e(__('translations.clear')); ?></a>
				</div>
			</form>
		</div>
		<div class="fx fxb panel-buttons">			
			<div class="fx fac">
				<div id="show_filter_block" class="btn"><?php echo e(__('translations.filter_btn')); ?></div>				
			</div>			
			<?php if(Auth::user()->type == config('types.clinic')): ?>
				<a href="<?php echo e(route('checkout')); ?>" class="btn btn-primary "><?php echo e(__('translations.create').' '.__('translations.order')); ?></a>
			<?php endif; ?>
		</div>
			
	</div>
	<div class="fx fxb panel-body">
		<div class="panel panel-table">
			<?php if($data && count($data)): ?> 
				<div class="account-list-table account-order-table">
					<div class="fx account-list-head">											
							<div class="account-list-cell">#ID</div>
							<div class="account-list-cell tooltip-block">
								<?php echo e(__('translations.status')); ?>

								<span class="tooltip-icon">?</span>
								<div class="content-tooltip">
									<div><span class="tooltip-color-item" style="background-color: #75DB78">D</span> - <?php echo e(__('translations.green_action')); ?></div>
									<div><span class="tooltip-color-item" style="background-color: #E2B222">D</span> - <?php echo e(__('translations.yellow_action')); ?></div>
									<div><span class="tooltip-color-item" style="background-color: #DB6827">D</span> - <?php echo e(__('translations.orange_action')); ?></div>
									<div><span class="tooltip-color-item" style="background-color: red">D</span> - <?php echo e(__('translations.red_action')); ?></div>
									<div><span class="tooltip-color-item" style="background-color: #B3B3B3">D</span> - <?php echo e(__('translations.grey_action')); ?></div>
									<div><span class="tooltip-color-item" style="background-color: #75DB78">D</span> - <?php echo e(__('translations.dentist_action')); ?></div>
									<div><span class="tooltip-color-item" style="background-color: #75DB78">L</span> - <?php echo e(__('translations.lab_action')); ?></div>

								</div>
							</div>
							<div class="account-list-cell"><?php echo e(__('translations.patient')); ?></div>
							<div class="account-list-cell">
								<?php if(Auth::user()->type == config('types.clinic')): ?>
									<?php echo e(__('translations.lab')); ?>

								<?php endif; ?>
								<?php if(Auth::user()->type == config('types.lab')): ?>
									<?php echo e(__('translations.clinic')); ?>

								<?php endif; ?>
							</div>
							<div class="account-list-cell"><?php echo e(__('translations.branch')); ?></div>
							<div class="account-list-cell"><?php echo e(__('translations.payment_status')); ?></div>
							<div class="account-list-cell"><?php echo e(__('translations.delivery_status')); ?></div>
							<div class="account-list-cell"><?php echo e(__('translations.ship_date')); ?></div>
							<div class="account-list-cell"></div>						
					</div>
					<div class="account-list-content">		
						<?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
						    <?php
						    	$line_class = '';
						    	$show_line_class = false;

						    	if (Auth::user()->type == config('types.lab')) {
									$check_date = date('U', strtotime('+2 days'));
						    	} else {
						    		$check_date = date('U', strtotime('+1 day'));
						    	}

						    	if (date('U', strtotime(str_replace('-', '/', $item->ship_date))) <= $check_date) {
						    		$show_line_class = true;
						    	}


						    	if (in_array($item->status, $hightlight_ship_statuses) && $show_line_class) {
									$line_class = 'account-list-lineRed';
						    	}
						    ?>
							<div class="fx fac account-list-line <?php echo e($line_class); ?>">								
								<div class="account-list-cell">
									#<?php echo e($item->id.($item->name ? ' ('.$item->name.')' : '').($item->version && $item->version > 1 ? ' v.'.$item->version : '')); ?> 
									<br>
									<?php if($item->status_info): ?>
										<div class="bi account-table-progress account-table-progress<?php echo e($item->status_info->progress); ?>">
											<span></span>										
										</div>
									<?php endif; ?>
									<?php if($item->created_at): ?> 
										<?php echo e(date('m-d-Y', strtotime($item->created_at))); ?>

									<?php endif; ?>
								</div>	
								<div class="account-list-cell">
									<div class="account-table-status">
									<?php 
										$letter = false;
										$style = '';
										$colors = json_decode($item->status_info->status_color);

										if ($item->status_info->change_by == config('types.clinic')) {
											$letter = 'D';
											$style .= 'left:-10px;';
										}

										if ($item->status_info->change_by == config('types.lab')) {
											$letter = 'L';
											$style .= 'right:-10px;';
										}

										if (Auth::user()->type == config('types.clinic')) $style .= 'background-color:'.$colors->cli;
										if (Auth::user()->type == config('types.lab')) $style .= 'background-color:'.$colors->lab;

										if ($item->need_confirmation == 1 && $item->status_info->confirm_by != 0) {
											$style = 'background-color: red;';

											if ($item->status_info->change_by == config('types.clinic')) {
												$letter = 'L';
												$style .= 'right:-10px;';
											}

											if ($item->status_info->change_by == config('types.lab')) {
												$letter = 'D';
												$style .= 'left:-10px;';
											}
										}

										if ($item->ship_date_changed && Auth::user()->type == config('types.clinic')) {
											$style = 'background-color: red;';
											$letter = 'D';
											$style .= 'left:-10px;';
										}

									?>	
									<?php if($item->status_info->icon): ?>										
										<img src="<?php echo e(url('/').'/storage/'.$item->status_info->icon); ?>" style="max-width: 35px;height:auto;" title="<?php echo e($item->status_info->name); ?>" alt="<?php echo e($item->status_info->name); ?>"> 
									<?php else: ?> 
										<?php echo e($item->status_info->name); ?>

									<?php endif; ?>
									<?php if($letter): ?>
										<span style="<?php echo e($style); ?>"><?php echo e($letter); ?></span>
									<?php endif; ?>
									</div>
								</div>	
								<div class="account-list-cell"><?php echo e($item->patient_name); ?> <?php echo e($item->patient_lastname); ?> <?php if(isset($item->patient_age)): ?><?php echo e($item->patient_age); ?> y.o.<?php endif; ?></div>
								<div class="account-list-cell">
									<?php if(Auth::user()->type == config('types.clinic')): ?>
										<?php echo e($item->to_branch_name); ?>

									<?php endif; ?>
									<?php if(Auth::user()->type == config('types.lab')): ?>
										<?php echo e($item->branch_name); ?>

									<?php endif; ?>
								</div>	
								<div class="account-list-cell">
									<?php if(Auth::user()->type == config('types.clinic')): ?>
										<?php echo e($item->branch_name); ?>

									<?php endif; ?>
									<?php if(Auth::user()->type == config('types.lab')): ?>
										<?php echo e($item->to_branch_name); ?>

									<?php endif; ?>
								</div>	
								<div class="account-list-cell">
									<?php if($item->payment_info->icon && $item->payment_info->id != 1): ?>	
										<img src="<?php echo e(url('/').'/storage/'.$item->payment_info->icon); ?>" style="max-width: 35px;height:auto;" title="<?php echo e($item->payment_info->name); ?>" alt="<?php echo e($item->payment_info->name); ?>"> 
									<?php else: ?> 
										<?php echo e($item->payment_info->name); ?>

									<?php endif; ?>
								</div>			
								<div class="account-list-cell">									
									<?php if($item->delivery_service && count($item->delivery_service)): ?>
										<?php $__currentLoopData = $item->delivery_service; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $delivery_service): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
											<?php 
												$direction = 'right';

												if ($item->to_branch_id == $delivery_service->from_branch) {
													$direction = 'left';
												}  

												$delivey_data = $delivery_service->delivery_info->name;

												if (isset($delivery_service->delivery_invoice) && $delivery_service->delivery_invoice) {
													if ($delivery_service->delivery_info->tracking_link) {
														if (strpos($delivery_service->delivery_info->tracking_link, 'replace_number') !== false) {
															$delivey_data .= '<br>(<a target="blank" href="'.str_replace('replace_number', $delivery_service->delivery_invoice, $delivery_service->delivery_info->tracking_link).'">'.__('translations.tracking').'</a>)';
														}
													} 
												}

												if ($delivery_service->received == 1) {
													if ($direction == 'left') {
														$delivey_data = 'In clinic';
													} else {
														$delivey_data = 'In lab';
													}

													$direction = false;
												}
 											?>
 											<?php if($direction): ?>
												<div class="bi delivery-history-arrow delivery-history-<?php echo e($direction); ?>"></div>
											<?php endif; ?>
											<div class="delivery-history-name">
												<?php echo $delivey_data; ?>

											</div>
											<?php break; ?>
										<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
									<?php else: ?> 
										---
									<?php endif; ?>
								</div>								
								<div class="account-list-cell" style="<?php if($line_class != '' || $item->ship_date_changed): ?> font-weight:700;color:red; <?php endif; ?>">
									<?php if($item->ship_date_changed): ?>
										<?php echo e($item->ship_date_changed); ?>

									<?php else: ?> 
										<?php echo e($item->ship_date); ?>

									<?php endif; ?>
								</div>
								<div class="account-list-cell">
									<a href="<?php echo e(route('orders.edit', [$item->id, $requests])); ?>" class="btn btn-small">
										<?php echo e(__('translations.view')); ?>

										<?php if(!is_null($item->chat) && is_null($item->chat_view)): ?>
											<div class="bi account-table-chat"></div>
										<?php endif; ?>
									</a>
									<?php if(Auth::user()->type == config('types.clinic') && in_array($item->status, [1,2,3])): ?>	
										<br>
										<div data-order="<?php echo e(\Illuminate\Support\Facades\Crypt::encrypt($item->id)); ?>" class="btn btn-danger btn-small delete-order-from-list" style="margin-top:5px;"><?php echo e(__('translations.delete')); ?></div>
									<?php endif; ?>								
								</div>
							</div>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					</div>					
				</div>
				<div class="pagination-block"><?php echo e($data->appends(\Request::except('page'))->links()); ?></div>
			<?php else: ?> 
				<p><?php echo e(__('translations.no_orders')); ?></p>
			<?php endif; ?>
		</div>
	</div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
	<script src="/js/select.js" defer></script>
	<script src="/js/moment.min.js" defer></script>
	<script src="/js/daterangepicker.js" defer></script>
	<script>
		document.addEventListener('DOMContentLoaded', function() {
			detectActivity(60);

			$('#order_type').select2({
				minimumResultsForSearch: -1
			});

			$('#branch').select2({
				minimumResultsForSearch: -1
			});

			$('#branch_staff').select2({
				minimumResultsForSearch: -1
			});

			$('#status').select2({
				minimumResultsForSearch: -1
			});

			$('#payment_status').select2({
				minimumResultsForSearch: -1
			});

			$('#ship_date').on('focus', function(){
				$('#ship_date').daterangepicker({				
					singleDatePicker: true,
					showDropdowns: true,
					startDate: false,				
					locale: {
				        format: 'MM-DD-YYYY'
				    },
				});
			});

			$('#show_filter_block').on('click', function(){
				$(this).toggleClass('show-filter-close');
				$('#order_filters_block').slideToggle();
			});

			$('.delete-order-from-list').on('click', function(){
				if (confirm('<?php echo e(__('translations.delete_alert')); ?>')) {
					var user_id = '<?php echo e(Auth::user()->id); ?>',
						order = $(this).data('order');				

					$.ajax({
						url: '/ajax/getData',
						type: 'post',
		                dataType: 'json',
		                headers: {
		                    'X-CSRF-TOKEN': '<?php echo e(csrf_token()); ?>'
		                },
					    data: {action:'markOrderAsDeleted', order: order},
					    success: function(data) {				    	
					    	if (data) {				    		
					    		window.location.href = '<?php echo e(route('orders')); ?>';
					    	}
					    }
					});
				}
			});
		}, !1);
	</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('Frontend/account_master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/dentist2lab.com/resources/views/Frontend/pages/account/orders/index.blade.php ENDPATH**/ ?>