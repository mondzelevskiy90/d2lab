<?php $__env->startSection('css'); ?>
	<link rel="stylesheet" href="/css/select.css">
	<link rel="stylesheet" href="/css/daterangepicker.css">
	<link rel="stylesheet" href="<?php echo e(mix('/css/account.css')); ?>">
	<style>
		.table-condensed thead tr:nth-child(2), .table-condensed tbody {
			display: none
		}
		.daterangepicker select.monthselect{
			min-width: 120px;
		}
	</style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
	<div id="panel_top_buttons" class="fx fac fxb">
		<?php echo $__env->make('Frontend.templates.breadcrumbs', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
		<div class="panel-top-buttons button-submit order-buttons">
			<a class="btn btn-small btn-grey" href="<?php echo e(route('reports')); ?>">&larr; <?php echo e(__('translations.back')); ?></a>
			<div class="btn btn-small" onclick="printContent('invoice_print_content');"><?php echo e(__('translations.print_all_prescriptions')); ?></div>
		</div>
	</div>
	<div class="panel panel-transparent">
		<form method="GET" action="<?php echo e(route('report', $data->slug)); ?>" id="filters_block" class="fx fac">
			<?php echo e(csrf_field()); ?>	
			<?php if($branches && count($branches) && (Auth::user()->role_id == config('roles.lab_head') || Auth::user()->role_id == config('roles.clinic_head'))): ?>
				<div class="form-filter">
					<select name="branch" id="branch" class="form-input">	
						<option value="0"> -- <?php echo e(__('translations.branch')); ?> --</option>
						<?php $__currentLoopData = $branches; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $branch): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<?php if($branch['status'] != 4): ?>
								<option value="<?php echo e($branch['id']); ?>" <?php if(isset($filters['branch']) && $filters['branch'] == $branch['id']): ?><?php echo e('selected="selected"'); ?><?php endif; ?>><?php echo e($branch['name']); ?></option>
							<?php endif; ?>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					</select>		
				</div>
			<?php endif; ?>	
			<div class="form-filter">
				<select name="order_branche" id="order_branche" class="form-input">	
					<option value="0">
						<?php if(Auth::user()->type == config('types.clinic')): ?>
							-- <?php echo e(__('translations.select').' '.__('translations.lab')); ?> --
						<?php endif; ?>
						<?php if(Auth::user()->type == config('types.lab')): ?>
							-- <?php echo e(__('translations.select').' '.__('translations.clinic')); ?> --
						<?php endif; ?>
					</option>
					<?php $__currentLoopData = $order_branches; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $order_branche): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
						<?php if(Auth::user()->type == config('types.clinic')): ?>
							<option value="<?php echo e($order_branche['to_branch_id']); ?>" <?php if(isset($filters['order_branche']) && $filters['order_branche'] == $order_branche['to_branch_id']): ?><?php echo e('selected="selected"'); ?><?php endif; ?>><?php echo e($order_branche['to_branch']['name']); ?></option>
						<?php endif; ?>
						<?php if(Auth::user()->type == config('types.lab')): ?>
							<option value="<?php echo e($order_branche['branch_id']); ?>" <?php if(isset($filters['order_branche']) && $filters['order_branche'] == $order_branche['branch_id']): ?><?php echo e('selected="selected"'); ?><?php endif; ?>><?php echo e($order_branche['branch']['name']); ?></option>
						<?php endif; ?>					
					<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				</select>	
			</div>
			<div class="form-filter">
				<input type="text" name="date" id="date" class="form-input" placeholder="<?php echo e(__('translations.from_date')); ?>" value="<?php if(isset($filters['date']) && $filters['date']): ?><?php echo e($filters['date']); ?><?php endif; ?>">			
			</div>
			<div class="filter-buttons" style="text-align: right;">					
				<input type="submit" class="btn btn-small" value="<?php echo e(__('translations.filter')); ?>">
				<a href="<?php echo e(route('report', $data->slug)); ?>" class="btn btn-danger btn-small"><?php echo e(__('translations.clear')); ?></a>
			</div>
		</form>
	</div>
	<!--<h1 class="tc"><?php echo e($seo->name); ?></h1>-->
	<div id="invoice_print_content">
		<?php 
			$cnt = 0;
			$show_pay_button = false;
		?>
		<?php $__currentLoopData = $content; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $branch): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
			<?php $__currentLoopData = $branch['order_branches']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $order_branche): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
				<?php if($order_branche['report_data'] && $order_branche['report_data']['invoices'] && count($order_branche['report_data']['invoices'])): ?>
					<div class="fx fxb panel-body">
						<div class="fx fac fxb panel">
							<div id="invoice_print_content<?php echo e($cnt); ?>" class="invoice-print-content">
								<div style="width: 1022px;position: relative; page-break-after: always;">
									<div class="fx fxb invoice-to_branch-info">
										<div class="invoice-to_branch-left">
											<?php if(Auth::user()->type == config('types.clinic')): ?>
												<p><strong><?php echo e($order_branche['branch']['name']); ?></strong></p>
												<br>
												<p><?php echo e($order_branche['branch']['zip_code'] .', '. $order_branche['branch']['address'] .', '. App\Services\Userdata::getLocality($order_branche['branch']['locality'] )); ?></p>
												<p>Tel: <?php echo e($order_branche['branch']['phone']); ?></p>
											<?php endif; ?>
											<?php if(Auth::user()->type == config('types.lab')): ?>
												<p><strong><?php echo e($branch['name']); ?></strong></p>
												<br>
												<p><?php echo e($branch['zip_code'] .', '. $branch['address'] .', '. App\Services\Userdata::getLocality($branch['locality'])); ?></p>
												<p>Tel: <?php echo e($branch['phone']); ?></p>
											<?php endif; ?>
										</div>
										<div class="invoice-to_branch-right">
											<div class="invoice-invoice-head"><?php echo e($data->name); ?></div>
											<br>
											<table class="invoice-table invoice-one-table invoice-table-date">
												<thead>
													<tr>
														<td><?php echo e(__('translations.date')); ?></td>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td><?php echo e(date('m-t-Y', strtotime($filters['date']))); ?></td>
													</tr>
												</tbody>
											</table>						
										</div>
									</div>
									<div class="fx fxb invoice-branch-info">
										<table class="invoice-table invoice-short-table invoice-table-bill">
											<thead>
												<tr>
													<td>To</td>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td>
														<?php if(Auth::user()->type == config('types.lab')): ?>
															<p><?php echo e($order_branche['branch']['name']); ?></p>		
															<p><?php echo e($order_branche['branch']['zip_code'] .', '. $order_branche['branch']['address'] .', '. App\Services\Userdata::getLocality($order_branche['branch']['locality'])); ?></p>
														<?php endif; ?>
														<?php if(Auth::user()->type == config('types.clinic')): ?>
															<p><?php echo e($branch['name']); ?></p>
															<p><?php echo e($branch['zip_code'] .', '. $branch['address'] .', '. App\Services\Userdata::getLocality($branch['locality'])); ?></p>
														<?php endif; ?>	
													</td>
												</tr>
											</tbody>
										</table>
									</div>
									<div class="fx invoice-infos" style="justify-content: flex-end;">
										<table class="invoice-table statement-table-totals">
											<thead>
												<tr>
													<td><?php echo e(__('translations.amount_due')); ?></td>
													<td><?php echo e(__('translations.amaunt_enc')); ?></td>									
												</tr>
											</thead>
											<tbody>
												<tr>
													<td><?php echo e($currency->symbol); ?><?php echo e($order_branche['report_data']['amount_due']); ?></td>
													<td></td>
												</tr>
											</tbody>
										</table>
									</div>
									<div class="invoice-infos">
										<table class="invoice-table invoice-full-table statement-table-invoices">
											<thead>
												<tr>
													<td><?php echo e(__('translations.date')); ?></td>
													<td><?php echo e(__('translations.transaction')); ?></td>
													<td><?php echo e(__('translations.amount')); ?></td>
													<td><?php echo e(__('translations.balance')); ?></td>
												</tr>
											</thead>
											<tbody>
												<tr>
													<?php $balance = $order_branche['report_data']['balance']; ?>
													<td><?php echo e($order_branche['report_data']['first_date']); ?></td>
													<td><?php echo e(__('translations.balance_forward')); ?></td>
													<td></td>
													<td><?php echo e($balance); ?></td>
												</tr>
												<?php $__currentLoopData = $order_branche['report_data']['invoices']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $invoice): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
													<tr>
														<td><?php echo e(date('m-d-Y', strtotime($invoice->created_at))); ?>

															<?php if(Auth::user()->type == config('types.clinic') && $can_pay && $invoice->payment_status != 4 && $invoice->payment_status != 5): ?>
															<div class="not-print" style="display: none;">
																<?php $show_pay_button = true; ?>
																<input type="checkbox" id="invoice_<?php echo e($invoice->id); ?>" class="invoices-check" name="invoices[<?php echo e($invoice->id); ?>]" value="<?php echo e($invoice->id); ?>" checked="checked">
															</div>
															<?php endif; ?>
														</td>
														<td>
															<?php echo e($invoice->name); ?> Due <?php echo e(date('m-d-Y', strtotime($invoice->created_at))); ?>

															<br>
															<?php $__currentLoopData = $invoice->prescriptions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $prescription): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
																<?php
																	$quantity = 1;

																	if ($prescription->price_for == 1 && $prescription->price_order_date != '0.00' && $prescription->advance_price != '0.00') {
																		$quantity = round((float)$prescription->advance_price / (float)$prescription->price_order_date);
																	}

																	$price_each = number_format((float)$prescription->price / $quantity, 2);
																?>
																<?php echo e($prescription->service->name); ?>, <?php echo e($quantity); ?> @ <?php echo e($currency->symbol); ?><?php echo e($price_each); ?> = <?php echo e($prescription->price); ?>

																<br>
															<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
															<?php echo e($invoice->order_info->patient_name.' '.$invoice->order_info->patient_lastname); ?>

														</td>
														<td>
															<br>
															<?php $__currentLoopData = $invoice->prescriptions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $prescription): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
																<?php if($prescription->paid == 1): ?> 
																	0.00
																<?php else: ?>
																	<?php echo e($prescription->price); ?>

																<?php endif; ?>
																<br>
															<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
														</td>
														<td>
															<br>
															<?php $__currentLoopData = $invoice->prescriptions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $prescription): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
																<?php
																	if ($prescription->paid != 1) {
																		$balance = number_format((float)$balance + (float)$prescription->price, 2);
																	}
																?>
																<?php echo e($balance); ?>

																<br>
															<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
														</td>
													</tr>
												<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
											</tbody>
										</table>
									</div>
									<div class="invoice-infos">
										<table class="invoice-table invoice-full-table statement-table-dates">
											<thead>
												<tr>
													<td><?php echo e(__('translations.current')); ?></td>
													<td><?php echo e(__('translations.1_days_due')); ?></td>
													<td><?php echo e(__('translations.31_days_due')); ?></td>
													<td><?php echo e(__('translations.61_days_due')); ?></td>
													<td><?php echo e(__('translations.91_days_due')); ?></td>
													<td><?php echo e(__('translations.amount_due')); ?></td>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td><?php echo e($order_branche['report_data']['current']); ?></td>
													<td><?php echo e($order_branche['report_data']['1_days_due']); ?></td>
													<td><?php echo e($order_branche['report_data']['31_days_due']); ?></td>
													<td><?php echo e($order_branche['report_data']['61_days_due']); ?></td>
													<td><?php echo e($order_branche['report_data']['91_days_due']); ?></td>
													<td><?php echo e($currency->symbol); ?><?php echo e($order_branche['report_data']['amount_due']); ?></td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
								<br>
								<?php if(Auth::user()->type == config('types.clinic') && $can_pay && $show_pay_button): ?>
									<div class="form-line print-btn-block not-print">
										<div class="panel panel-table-controls pay-controls not-print">
											<div class="btn btn-small btn-danger" id="pay_selected" onclick="paySelectedInvoices();">
												<?php echo e(__('translations.pay_unpaid_from_list')); ?>

											</div>
										</div>
									</div>
								<?php endif; ?>
							</div>
						</div>
					</div>
				<?php endif; ?>
				<?php $cnt++; ?>
			<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>	
		<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>	
		
	</div>		
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
	<script src="/js/select.js" defer></script>
	<script src="/js/moment.min.js" defer></script>
	<script src="/js/daterangepicker.js" defer></script>
	<script>
		document.addEventListener('DOMContentLoaded', function(){
			$('#branch, #order_branche').select2({
				minimumResultsForSearch: -1
			});

			$('#date').on('focus', function(){
				$(this).daterangepicker({				
					singleDatePicker: true,
					showDropdowns: true,
					startDate: '<?php echo e(date('y-m')); ?>',
					autoApply: true,
					locale: {
				        format: 'YYYY-MM-DD'
				    },
				}).on('showCalendar.daterangepicker', function (ev, picker) {
					var days = document.querySelectorAll('.table-condensed tbody td.available');

					for (var i = 0; i <= days.length; i++) {
						if (days[i].classList.contains('off')) {
							continue;
						} else {
							days[i].classList.add('active');
							days[i].classList.add('start-date');
							days[i].classList.add('end-date');

							var month_value = parseInt(document.querySelector('.monthselect').value) + 1;
							var month = month_value < 10 ? '0'+month_value : month_value;
							var year = document.querySelector('.yearselect').value;

							picker.setStartDate(moment(year+'-'+month+'-'+days[i].innerHTML, 'YYYY-MM-DD'));
							
							break;
						}
					}

				  	$(this).val(picker.startDate.format('YYYY-MM-DD'));
				  	$(document).find('.applyBtn').click();
				});
			});
		}, !1);

		function printContent(id) {
			var new_window =window.open();
			var html = '<link rel="stylesheet" href="<?php echo e(mix('/css/styles.css')); ?>">';

			html += '<link rel="stylesheet" href="<?php echo e(mix('/css/account.css')); ?>">';
			html += '<link rel="stylesheet" href="<?php echo e(mix('/css/print_invoice.css')); ?>">';
			html += '<div class="fx fxb print-source"><span><?php echo e(config('app.url')); ?></span><span><?php echo e(date('m-d-Y')); ?></span></div>';
			html += document.getElementById(id).innerHTML;

			new_window.document.write(html);

			setTimeout(function(){
				new_window.print();
				new_window.close();
			}, 1000);
		}
	</script>	
	<?php echo $__env->make('Frontend.templates.invoice_payment_script', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('Frontend/account_master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/d2l.sitepark.ua/resources/views/Frontend/pages/account/reports/statement.blade.php ENDPATH**/ ?>