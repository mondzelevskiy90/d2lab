<?php $__env->startSection('css'); ?>
	<link rel="stylesheet" href="/css/select.css">
	<link rel="stylesheet" href="<?php echo e(mix('/css/account.css')); ?>">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>	
	<form class="content-editing-form " method="POST" action="<?php echo e(isset($data->id) ? route('staff.edit', $data->id) : route('staff.create')); ?>" enctype="multipart/form-data">
		<div id="panel_top_buttons" class="fx fac fxb">
			<?php echo $__env->make('Frontend.templates.breadcrumbs', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
			<div class="panel-top-buttons button-submit order-buttons">
				<a class="btn btn-small btn-grey" href="<?php echo e(route('staff', [$requests])); ?>">&larr; <?php echo e(__('translations.back')); ?></a>
				<input type="submit" id="submit" class="btn btn-small" name="submit" value="<?php if(isset($data->id)): ?><?php echo e(__('translations.save_changes')); ?><?php else: ?><?php echo e(__('translations.save')); ?><?php endif; ?>">
			</div>
		</div>
		<h1 class="tc"><?php echo e($seo->name); ?></h1>	
		<div class="fx fxb panel-body staff-edit-form">		
			<?php echo e(csrf_field()); ?>			
			<div class="panel">
				<?php if(isset($data->id)): ?>					
					<div class="fx fac form-line">
						<div class="bi company-logo" style="margin: 0 15px 0 0; background-image: url(/storage/<?php echo e($data->user->avatar); ?>)"></div>
						<strong><?php echo $data->user->name.' '.$data->user->surname.'<br>('.$data->user->email.')'; ?></strong>
					</div>
					<br>
				<?php else: ?>
					<div class="form-line">
						<label class="fx" for="name"><?php echo e(__('translations.name')); ?></label>				
						<input type="text" <?php if(!isset($data->id)): ?> name="name" required="required" <?php else: ?> disabled="disabled" <?php endif; ?> id="name" class="form-input" placeholder="<?php echo e(__('translations.name')); ?>" value="<?php echo e(old('name', $data->user->name ?? '')); ?>" required="required">
						<?php if($errors->has('name')): ?>
		                    <div class="error"><?php echo e($errors->first('name')); ?></div>
		                <?php endif; ?>				
					</div>	
					<div class="form-line">
						<label for="surname"><?php echo e(__('translations.surname')); ?></label>				
						<input type="text" <?php if(!isset($data->id)): ?> name="surname" required="required" <?php else: ?> disabled="disabled" <?php endif; ?> id="surname" class="form-input" placeholder="<?php echo e(__('translations.surname')); ?>" value="<?php echo e(old('surname', $data->user->surname ?? '')); ?>" required="required">
						<?php if($errors->has('surname')): ?>
		                    <div class="error"><?php echo e($errors->first('surname')); ?></div>
		                <?php endif; ?>				
					</div>
					<div class="form-line">
						<label for="email"><?php echo e(__('translations.email')); ?></label>				
						<input type="email" <?php if(!isset($data->id)): ?> name="email" required="required" <?php else: ?> disabled="disabled" <?php endif; ?> id="email" class="form-input" placeholder="<?php echo e(__('translations.email')); ?>" value="<?php echo e(old('email', $data->user->email ?? '')); ?>">
						<?php if($errors->has('email')): ?>
		                    <div class="error"><?php echo e($errors->first('email')); ?></div>
		                <?php endif; ?>				
					</div>	
				<?php endif; ?>
				<?php if(isset($data->id) && $data->user->status != 1): ?>	
					<div class="form-line">
						<label for="status"><?php echo e(__('translations.status')); ?></label>				
						<select id="status" name="status" class="form-controll" required="required">
							<option value="3" <?php if($data->user->status == 3): ?> selected <?php endif; ?>><?php echo e(__('translations.status_active')); ?></option>
							<option value="4" <?php if($data->user->status == 4): ?> selected <?php endif; ?>><?php echo e(__('translations.status_disactive')); ?></option>						
						</select>				
					</div>
			    <?php endif; ?>	
				<div class="form-line">
					<label for="branch_id"><?php echo e(__('translations.branch')); ?></label>
					<select name="branch_id" id="branch_id" class="form-controll" required="required">
						<?php $__currentLoopData = $branches; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $branch): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<option value="<?php echo e($branch->id); ?>"><?php echo e($branch->name); ?></option>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					</select>
				</div>
				<div class="form-line">
					<label for="role_id"><?php echo e(__('translations.role')); ?></label>
					<select name="role_id" id="role_id" class="form-controll" required="required">
						<?php $__currentLoopData = $roles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $role): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<option value="<?php echo e($role['id']); ?>" <?php if(isset($data->id) && $data->user->role_id == $role['id']): ?> selected="selected" <?php endif; ?>><?php echo e($role['display_name']); ?></option>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					</select>
				</div>
				<div class="form-line">
					<label for="can_pay"><?php echo e(__('translations.can_pay')); ?></label>
					<select name="can_pay" id="can_pay" class="form-controll" required="required">
						<option value="0" <?php if(isset($data->id) && $data->can_pay == 0): ?> selected="selected" <?php endif; ?>><?php echo e(__('translations.no')); ?></option>
						<option value="1" <?php if(isset($data->id) && $data->can_pay == 1): ?> selected="selected" <?php endif; ?>><?php echo e(__('translations.yes')); ?></option>						
					</select>
				</div>		
			</div>
		</div>
	</form>	
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
	<script src="/js/select.js" defer></script>	
	<script>
		document.addEventListener('DOMContentLoaded', function(){
			$('#status, #role_id, #branch_id, #can_pay').select2({
				minimumResultsForSearch: -1
			});

		}, !1);
	</script>
	<?php echo $__env->make('Frontend.templates.fields_script', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('Frontend/account_master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/dentist2lab.com/resources/views/Frontend/pages/account/staff/edit.blade.php ENDPATH**/ ?>