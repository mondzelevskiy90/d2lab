<div class="panel panel-half dashboard-item dashboard-item-half dashboard-item-<?php echo e($code); ?>">
	<div class="panel-head"><?php echo e(__('translations.orders_list')); ?></div>
	<?php if($content['text'] && $content['text'] != ''): ?>
		<div class="panel-intro"><?php echo $content['text']; ?></div>
	<?php endif; ?>
	<div class="panel-content">
		<?php if($content['data'] && count($content['data'])): ?>
			<div class="dashboard-orders-list">
				<?php $__currentLoopData = $content['data']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
					<a href="<?php echo e(route('orders.edit', $item->id)); ?>" class="fx fac fxb dashboard-orders-line <?php if($item->status_info->change_by == Auth::user()->type): ?><?php echo e('dashboard-orders-lineAct'); ?><?php endif; ?>">
						<span class="dashboard-orders-id">#<?php echo e($item->id); ?></span>
						<span class="dashboard-orders-name"><?php echo e($item->patient_name.' '.$item->patient_lastname); ?></span>
						<span class="dashboard-orders-date"><?php echo e($item->ship_date); ?></span>
						<span class="bi dashboard-orders-icon" style="background-image: url(<?php echo e(url('/').'/storage/'.$item->status_info->icon); ?>)" title="<?php echo e($item->status_info->name); ?>"></span>
					</a>
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
			</div>
		<?php else: ?> 
			<p><?php echo e(__('translations.no_orders_list')); ?></p>
		<?php endif; ?>
	</div>
</div><?php /**PATH /var/www/dentist2lab.com/resources/views/Frontend/templates/dashboard/orders.blade.php ENDPATH**/ ?>