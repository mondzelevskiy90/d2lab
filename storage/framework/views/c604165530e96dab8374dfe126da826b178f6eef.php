<?php 
	$banners = \App\Models\Banner::where('position', 1)->where('status', 1)->orderBy('sort_order', 'ASC')->get();
?>
<?php if($banners): ?>
	<div class="footer-banners">
		<div class="content">
			<div class="fx fxc">
				<?php $__currentLoopData = $banners; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $banner): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
					<?php if($banner->image): ?>
					<div class="banner-event footer-banner" data-id="<?php echo e($banner->id); ?>" data-token="<?php echo e(csrf_token()); ?>">
						<a href="<?php echo e($banner->link); ?>" target="_blank">
							<img src="<?php echo e('/storage/'.$banner->image); ?>" alt="<?php echo e($banner->name); ?>">
						</a>
					</div>
					<?php endif; ?>
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
			</div>
		</div>
	</div>
<?php endif; ?>
<footer>
	<div class="content">
		<div class="fx fxb">
			<div class="logo-block footer-logo">
				<div class="bi logo" style="background-image: url('/img/logo.png')"></div>
			</div>
			<div class="footer-links">
				<a href="<?php echo e(url('article/about')); ?>"><?php echo e(__('translations.about_us')); ?></a>
				<a href="<?php echo e(url('article/terms')); ?>"><?php echo e(__('translations.terms')); ?></a>
				<a href="<?php echo e(url('article/privacy-policy')); ?>"><?php echo e(__('translations.private_policy')); ?></a>
				<a href="<?php echo e(url('article/faqs')); ?>"><?php echo e(__('translations.faq')); ?></a>
				<a href="<?php echo e(url('sitemap')); ?>"><?php echo e(__('translations.sitemap')); ?></a>
			</div>
			<div class="footer-socials">
				<a href="/" class="bi fcb"></a>
				<a href="/" class="bi twtr"></a>
				<a href="/" class="bi inst"></a>
			</div>
		</div>
	</div>
</footer>
<div id="loader" class="fx fxc">
	<div class="bi"></div>
</div>		
<script src="/js/common.js" defer></script>		
<?php echo $__env->yieldContent('scripts'); ?>
<?php if(session('message')): ?>
	<script>
		document.addEventListener('DOMContentLoaded', function() {
			showPopup('<div class="<?php echo e(session('alert-type')); ?>"><?php echo session('message'); ?></div>');
		}, !1);
	</script>	        
<?php endif; ?><?php /**PATH /var/www/dentist2lab.com/resources/views/Frontend/parts/footer.blade.php ENDPATH**/ ?>