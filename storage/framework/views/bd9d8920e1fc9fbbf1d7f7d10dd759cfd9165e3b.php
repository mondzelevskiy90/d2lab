<?php $__env->startSection('page_title', __('voyager::generic.'.(isset($data->id) ? 'edit' : 'add')).' '.$data->name); ?>

<?php $__env->startSection('css'); ?>
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('page_header'); ?>
    <h1 class="page-title">
        <i class="voyager-edit"></i>
        <?php echo e(__('voyager::generic.'.(isset($data->id) ? 'edit' : 'add')).' '.$data->name); ?>

    </h1>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="page-content container-fluid">
        <form class="form-edit-add" role="form" action="<?php if(!is_null($data->id)): ?><?php echo e(route('voyager.'.$type.'.update', [$data->id, $requests])); ?><?php else: ?><?php echo e(route('voyager.'.$type.'.store', [$requests])); ?><?php endif; ?>" method="POST" enctype="multipart/form-data" autocomplete="off">            
            <?php if(isset($data->id)): ?>
                <?php echo e(method_field("PUT")); ?>

            <?php endif; ?>
            <?php echo e(csrf_field()); ?>


            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-bordered">                   
                        <?php if(count($errors) > 0): ?>
                            <div class="alert alert-danger">
                                <ul>
                                    <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <li><?php echo e($error); ?></li>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </ul>
                            </div>
                        <?php endif; ?>

                        <div class="panel-body">
                            <div class="form-group">
                                <label for="status"><?php echo e(__('adm.status')); ?></label>
                                <select class="form-control" name="status">                                    
                                    <option value="1" <?php if(isset($data->status) && $data->status == 1): ?> selected <?php endif; ?>><?php echo e(__('adm.status_on')); ?></option>
                                    <option value="2" <?php if(!isset($data->status) || $data->status == 2): ?> selected <?php endif; ?>><?php echo e(__('adm.status_off')); ?></option>   
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="sort_order"><?php echo e(__('adm.sort_order')); ?></label>
                                <input type="text" class="form-control" id="sort_order" name="sort_order" placeholder="<?php echo e(__('adm.sort_order')); ?>" value="<?php echo e(old('sort_order', $data->sort_order ?? 0)); ?>">
                            </div>                            
                            <div class="form-group">
                                <label for="image"><?php echo e(__('adm.image')); ?></label>
                                <?php if(isset($data->image) && $data->image != ''): ?>
                                    <img src="<?php echo e(filter_var($data->image, FILTER_VALIDATE_URL) ? $data->image : Voyager::image( $data->image )); ?>" style="width:200px; height:auto; clear:both; display:block; padding:2px; border:1px solid #ddd; margin-bottom:10px;" />
                                <?php endif; ?>
                                <input type="file" data-name="image" name="image" id="image">
                            </div>                       
                            <div class="form-group">
                                <label for="slug">Url</label>
                                <input type="text" class="form-control" id="slug" name="slug" placeholder="Url" value="<?php echo e(old('slug', $data->slug ?? '')); ?>">
                            </div>
                        </div>
                        <div class="panel-body">
                            <ul class="nav nav-tabs">
                            <?php $__currentLoopData = $content; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>                             
                                <li <?php if($value['language_default'] == 1): ?> class="active" <?php endif; ?>>
                                <a data-toggle="tab" href="#<?php echo e($value['code']); ?>"><?php echo e($value['language_name']); ?></a>
                                </li>                                
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </ul>
                            <div class="tab-content">
                            <?php $__currentLoopData = $content; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
                                <div class="tab-pane <?php if($value['language_default'] == 1): ?> active <?php endif; ?>" id="<?php echo e($value['code']); ?>">
                                    <div class="form-group">
                                        <label for="name_<?php echo e($key); ?>"><?php echo e(__('adm.name')); ?></label>
                                        <input type="text" class="form-control" id="name_<?php echo e($key); ?>" name="content[<?php echo e($key); ?>][name]" placeholder="<?php echo e(__('adm.name')); ?>" value="<?php echo e(old('content.'.$key.'.name', $content[$key]['name'] ?? '')); ?>" required="required">
                                    </div>
                                    <div class="form-group">
                                        <label for="meta_title_<?php echo e($key); ?>"><?php echo e(__('adm.meta_title')); ?></label>
                                        <input type="text" class="form-control" id="meta_title_<?php echo e($key); ?>" name="content[<?php echo e($key); ?>][meta_title]" placeholder="<?php echo e(__('adm.meta_title')); ?>" value="<?php echo e(old('content.'.$key.'.meta_title', $content[$key]['meta_title'] ?? '')); ?>">
                                    </div>
                                    <div class="form-group">
                                        <label for="meta_description_<?php echo e($key); ?>"><?php echo e(__('adm.meta_description')); ?></label>
                                        <input type="text" class="form-control" id="meta_title_<?php echo e($key); ?>" name="content[<?php echo e($key); ?>][meta_description]" placeholder="<?php echo e(__('adm.meta_description')); ?>" value="<?php echo e(old('content.'.$key.'.meta_description', $content[$key]['meta_description'] ?? '')); ?>">
                                    </div>
                                    <div class="form-group">
                                        <label for="description_<?php echo e($key); ?>"><?php echo e(__('adm.description')); ?></label>
                                        <textarea class="richTextBox form-control" id="description_<?php echo e($key); ?>" name="content[<?php echo e($key); ?>][description]" placeholder="<?php echo e(__('adm.description')); ?>"><?php echo e(old('content.'.$key.'.description', $content[$key]['description'] ?? '')); ?></textarea>
                                    </div>
                                </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> 
                            </div>
                        </div>
                    </div>
                </div>                
            </div>

            <button type="submit" class="btn btn-primary pull-right save">
                <?php echo e(__('voyager::generic.save')); ?>

            </button>
        </form>

        <iframe id="form_target" name="form_target" style="display:none"></iframe>
        <form id="my_form" action="<?php echo e(route('voyager.upload')); ?>" target="form_target" method="post" enctype="multipart/form-data" style="width:0px;height:0;overflow:hidden">
            <?php echo e(csrf_field()); ?>

            <input name="image" id="upload_file" type="file" onchange="$('#my_form').submit();this.value='';">
            <input type="hidden" name="type_slug" id="type_slug" value="<?php echo e($type); ?>">
        </form>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>
    <script>
        $('document').ready(function () {
            $('.toggleswitch').bootstrapToggle();
        });
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('voyager::master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/dentist2lab.com/resources/views/Backend/articles/edit-add.blade.php ENDPATH**/ ?>