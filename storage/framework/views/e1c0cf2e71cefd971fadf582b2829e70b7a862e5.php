<?php echo e(csrf_field()); ?>	
<div class="fx fxb panel-body">
	<div class="panel">
		<div class="panel-head panel-head-collapse panel-head-collapsed"><?php echo e(__('translations.lab_info')); ?> - <?php echo e($branch->name); ?></div>
		<div class="fx fxb panel-block panel-block-collapsed">
			<div class="checkout-branch-left">
				<div class="checkout-branch-logo" style="background-image: url(<?php echo e(App\Services\Img::resizeImage($branch->logo, 235, 235)); ?>)">
				</div>
			</div>
			<div class="checkout-branch-right">
				<div class="fx fxb checkout-branch-info">
					<div >
						<div class="checkout-branch-infoName">
							<?php echo e($branch->name); ?>

						</div>
						<div class="checkout-branch-icons">
							<div class="bi checkout-branch-icon checkout-branch-verified"><?php echo e(__('translations.verified')); ?></div>
							<div class="bi checkout-branch-icon checkout-branch-email"><?php echo e($branch->email); ?></div>
							<div class="bi checkout-branch-icon checkout-branch-phone"><?php echo e($branch->phone); ?></div>
							<div class="bi checkout-branch-icon checkout-branch-location"><?php echo e($branch->zip_code .', '. $branch->address .', '. App\Services\Userdata::getLocality($branch->locality)); ?></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="fx fxb panel-body">
	<div class="panel">
		<div class="panel-head panel-head-collapse panel-head-collapsed"><?php echo e(__('translations.order_user_info')); ?></div>
		<div class="fx fxb panel-block panel-block-collapsed">
			<input type="hidden" name="user_id" value="<?php if(isset($data->id)): ?><?php echo e($data->user_id); ?><?php else: ?><?php echo e(Auth::user()->id); ?><?php endif; ?>"/>
			<input type="hidden" name="user_name" value="<?php if(isset($data->user_name)): ?><?php echo e($data->user_name); ?><?php else: ?><?php echo e(Auth::user()->name.' '.Auth::user()->surname); ?><?php endif; ?>"/>
			<input type="hidden" id="to_branch_id" name="to_branch_id" value="<?php if(isset($data->to_branch_id)): ?><?php echo e($data->to_branch_id); ?><?php else: ?><?php echo e($branch->id); ?><?php endif; ?>"/>
			<input type="hidden" name="to_branch_name" value="<?php if(isset($data->to_branch_name)): ?><?php echo e($data->to_branch_name); ?><?php else: ?><?php echo e($branch->name); ?><?php endif; ?>"/>
			
			<?php if($branches && count($branches)): ?>
				<div class="form-line">	
					<label for="branch_id"><?php echo e(__('translations.branch')); ?></label>			
					<select name="branch_id" id="branch_id" class="form-input form-input-required" required="required" onchange="getBranchAddreess();">						
						<?php $__currentLoopData = $branches; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $branch): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<option value="<?php echo e($branch->id); ?> <?php if(isset($data->branch_id) && (int)$data->branch_id == (int)$branch->id): ?><?php echo e('selected="selected"'); ?><?php endif; ?>"><?php echo e($branch->name); ?></option>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					</select>
				</div>
			<?php endif; ?>

			<?php if($user_branch): ?> 
				<input type="hidden" name="branch_id" value="<?php if(isset($data->branch_id)): ?><?php echo e($data->branch_id); ?><?php else: ?><?php echo e($user_branch->branch->id); ?><?php endif; ?>"/>
				<input type="hidden" name="branch_name" value="<?php if(isset($data->branch_name)): ?><?php echo e($data->branch_name); ?><?php else: ?><?php echo e($user_branch->branch->name); ?><?php endif; ?>"/>
			<?php endif; ?>

			<div class="form-line">	
				<label for="user_email"><?php echo e(__('translations.email')); ?></label>
				<input type="email" name="user_email" id="user_email" class="form-inut form-input-required" value="<?php if(isset($data->user_email)): ?><?php echo e($data->user_email); ?><?php else: ?><?php echo e(Auth::user()->email); ?><?php endif; ?>" placeholder="<?php echo e(__('translations.email')); ?>" maxlength="255" required="required">
			</div>
			<div class="form-line">	
				<label for="user_phone"><?php echo e(__('translations.phone')); ?></label>
				<input type="text" name="user_phone" id="user_phone" class="form-inut form-input-required" value="<?php if(isset($data->user_phone)): ?><?php echo e($data->user_phone); ?><?php else: ?><?php echo e(Auth::user()->phone); ?><?php endif; ?>" placeholder="<?php echo e(__('translations.user_phone')); ?>" maxlength="32" required="required">
			</div>
		</div>
	</div>
</div>
<div class="fx fxb panel-body">
	<div class="panel">
		<div class="panel-head panel-head-collapse"><?php echo e(__('translations.order_patient_info')); ?></div>
		<div class="fx fxb panel-block">
			<div class="form-line">	
				<label for="patient_name"><?php echo e(__('translations.name')); ?></label>
				<input type="text" name="patient_name" id="patient_name" class="form-inut form-input-required" value="<?php if(isset($data->patient_name)): ?><?php echo e($data->patient_name); ?><?php endif; ?>" placeholder="<?php echo e(__('translations.patient_name')); ?>" maxlength="255" required="required">
			</div>
			<div class="form-line">	
				<label for="patient_lastname"><?php echo e(__('translations.lastname')); ?></label>
				<input type="text" name="patient_lastname" id="patient_lastname" class="form-inut form-input-required" value="<?php if(isset($data->patient_lastname)): ?><?php echo e($data->patient_lastname); ?><?php endif; ?>" placeholder="<?php echo e(__('translations.patient_lastname')); ?>" maxlength="255" required="required">
			</div>
			<div class="form-line">	
				<label for="patient_age"><?php echo e(__('translations.age')); ?></label>
				<input type="text" name="patient_age" id="patient_age" class="form-inut" value="<?php if(isset($data->patient_age)): ?><?php echo e($data->patient_age); ?><?php endif; ?>" placeholder="<?php echo e(__('translations.patient_age')); ?>" maxlength="3" onchange="isNaN(this.value) ? this.value = null : this.value = this.value;">
			</div>
			<div class="form-line">	
				<label for="patient_sex"><?php echo e(__('translations.sex')); ?></label>			
				<select name="patient_sex" id="patient_sex" class="form-input">
					<option value="<?php echo e(__('translations.male')); ?>" <?php if(isset($data->patient_sex) && $data->patient_sex == __('translations.male')): ?><?php echo e('selected="selected"'); ?><?php endif; ?>><?php echo e(__('translations.male')); ?></option>
					<option value="<?php echo e(__('translations.female')); ?>" <?php if(isset($data->patient_sex) && $data->patient_sex == __('translations.female')): ?><?php echo e('selected="selected"'); ?><?php endif; ?>><?php echo e(__('translations.female')); ?></option>
				</select>
			</div>
		</div>
	</div>
</div>
<div class="fx fxb panel-body">
	<div class="panel">
		<div class="panel-head panel-head-collapse"><?php echo e(__('translations.order_info')); ?></div>
		<div class="fx fxb panel-block">							
			<div class="form-line">	
				<label for="order_type"><?php echo e(__('translations.order_type')); ?></label>
				<select name="order_type" id="order_type" class="form-input form-input-required" required="required">
					<?php $__currentLoopData = $order_types; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $order_type): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
						<option value="<?php echo e($order_type->id); ?>"  <?php if(isset($data->order_type) && $data->order_type == $order_type->id): ?><?php echo e('selected="selected"'); ?><?php endif; ?>><?php echo e($order_type->name); ?></option>
					<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				</select>
			</div>					
			<div class="form-line">	
				<label for="ship_date"><?php echo e(__('translations.ship_date')); ?></label>
				<input type="text" name="ship_date" id="ship_date" class="form-input form-input-required" value="<?php if(isset($data->ship_date)): ?><?php echo e($data->ship_date); ?><?php endif; ?>" placeholder="<?php echo e(__('translations.ship_date')); ?>" maxlength="255" required="required">
			</div>
			<div class="form-line">	
				<label for="ship_address"><?php echo e(__('translations.ship_address')); ?></label>
				<?php
					$address = '';

					if ($branches && count($branches)) {
						foreach ($branches as $branch) {
							$address = $branch->zip_code .', '. $branch->address .', '. App\Services\Userdata::getLocality($branch->locality);
							break;
						}
					}

					if ($user_branch) {
						$address = $user_branch->branch->zip_code .', '. $user_branch->branch->address .', '. App\Services\Userdata::getLocality($user_branch->branch->locality);
					}
				?>
				<input type="text" name="ship_address" id="ship_address" class="form-input form-input-required" value="<?php if(isset($data->ship_date)): ?><?php echo e($data->ship_address); ?><?php else: ?><?php echo e($address); ?><?php endif; ?>" placeholder="<?php echo e(__('translations.ship_address')); ?>" maxlength="255" required="required">
			</div>
			<div class="form-line">
				<?php
					$attached = array();

					if (isset($data->attached) && $data->attached) {
						$attached = explode(';', $data->attached);
					}
				?>

				<label><?php echo e(__('translations.order_attached')); ?></label>
				<div class="form-checkbox">								
					<input type="checkbox" name="attached[]" id="attached1" value="<?php echo e(__('translations.attached_impressions')); ?>" <?php if(in_array(__('translations.attached_impressions'), $attached)): ?><?php echo e('checked="checked"'); ?><?php endif; ?>>
					<label for="attached1"><?php echo e(__('translations.attached_impressions')); ?></label>
				</div>
				<div class="form-checkbox">								
					<input type="checkbox" name="attached[]" id="attached2" value="<?php echo e(__('translations.attached_models')); ?>"  <?php if(in_array(__('translations.attached_models'), $attached)): ?><?php echo e('checked="checked"'); ?><?php endif; ?>>
					<label for="attached2"><?php echo e(__('translations.attached_models')); ?></label>
				</div>
				<div class="form-checkbox">								
					<input type="checkbox" name="attached[]" id="attached3" value="<?php echo e(__('translations.attached_bite')); ?>"  <?php if(in_array(__('translations.attached_bite'), $attached)): ?><?php echo e('checked="checked"'); ?><?php endif; ?>>
					<label for="attached3"><?php echo e(__('translations.attached_bite')); ?></label>
				</div>
				<div class="form-checkbox">								
					<input type="checkbox" name="attached[]" id="attached4" value="<?php echo e(__('translations.attached_photos')); ?>"  <?php if(in_array(__('translations.attached_photos'), $attached)): ?><?php echo e('checked="checked"'); ?><?php endif; ?>>
					<label for="attached4"><?php echo e(__('translations.attached_photos')); ?></label>
				</div>
				<?php
					$attached_other = '';

					foreach ($attached as $attach) {
						if ($attach != __('translations.attached_photos') && $attach != __('translations.attached_bite') && $attach != __('translations.attached_models') && $attach != __('translations.attached_impressions')) $attached_other = $attach;
					}
				?>
				<div class="form-line">
					<input type="text" name="attached[]" id="attached5" class="form-input" value="<?php echo e($attached_other); ?>" placeholder="<?php echo e(__('translations.attached_other')); ?>" maxlength="220" style="padding:8px 10px;margin-top: 10px;font-size: 12px;">
				</div>
			</div>
			<div class="form-line">	
				<label for=""><?php echo e(__('translations.numbering_system')); ?></label>
				<select name="numbering_system_id" id="numbering_system" class="form-input form-input-required" required="required" onchange="getNumberingSystemSvg();">
					<?php $__currentLoopData = $numbering_systems; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $numbering_system): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
						<option value="<?php echo e($numbering_system->id); ?>" <?php if(isset($data->numbering_system_id) && $data->numbering_system_id == $numbering_system->id): ?><?php echo e('selected="selected"'); ?><?php endif; ?>><?php echo e($numbering_system->name); ?></option>
					<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>									
				</select>
			</div>
			<div class="form-line">
				<label for="files"><?php echo e(__('translations.order_files')); ?></label>
				<div id="order_files">
					<?php if(isset($data->files)): ?>
						<?php $__currentLoopData = $data->files; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $file): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<div id="order_file_line<?php echo e($file_index); ?>" class="fx fxb order-file-line">
								<div id="order_file_name<?php echo e($file_index); ?>" class="order-file-name"><a href="<?php echo e(url('/').'/storage/'.$file->file); ?>" target="_blank"><?php echo e($file->file); ?></a></div>
								<input type="hidden" id="order_file_input<?php echo e($file_index); ?>" name="order_files[]" value="<?php echo e($file->file); ?>">
								<div class="btn btn-small btn-danger order-file-remove" onclick="removeFileFromCheckoutForm('<?php echo e($file_index); ?>')"><?php echo e(__('translations.delete')); ?></div>										
							</div>
							<?php $file_index++; ?>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>	
					<?php endif; ?>	
				</div>
				<br>
				<div id="order_files_add" class="btn btn-small" onclick="addFileToCheckoutForm()" style="max-width:120px;"><?php echo e(__('translations.add_file')); ?></div>
			</div>							
		</div>
	</div>
</div>
<div class="fx fxb panel-body">
	<div class="panel">
		<div class="panel-head panel-head-collapse"><?php echo e(__('translations.add_order_prescriptions')); ?></div>
		<div class="fx fxb panel-block">
			<div class="form-line">
				<label>***<?php echo e(__('translations.add_order_prescriptions_intro')); ?></label>
			</div>
			<div class="form-line">	
				<label for="service_id"><?php echo e(__('translations.service_name')); ?></label>
				<select name="service_id" id="service_id" class="form-input form-input-required" required="required">
					<?php $__currentLoopData = $services; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $service): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>	
						<?php if($service->service): ?>					
							<option value="<?php echo e($service->service->id); ?>"><?php echo e($service->service->name); ?> <?php if(isset($service->parent)): ?>(<?php echo e($service->parent->name); ?>)<?php endif; ?> <?php if($service->min_days && $service->min_days > 0): ?> - <?php echo e(__('translations.from').' '.$service->price.$currency->symbol); ?>, <?php echo e($service->min_days.' days'); ?><?php endif; ?></option>
						<?php endif; ?>
					<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				</select>
			</div>							
			<div class="form-line order-add-prescription">
				<div id="add_prescription_button" class="btn" onclick="addPrescriptionBlock()"><?php echo e(__('translations.add_service')); ?></div>
			</div>
			<div class="fx fxb form-line order-prescription-selects">
				<div class="order-prescription-img">
					<div class="fx fxc" id="numbering_system_mask" style="<?php if(isset($data->prescriptions) && count($data->prescriptions)): ?><?php echo e('display: none;'); ?><?php endif; ?>"><?php echo e(__('translations.select_service')); ?></div>
					<div id="numbering_system_block">
						<?php $__currentLoopData = $numbering_systems; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $numbering_system): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<?php if(isset($data->numbering_system_id)): ?>
								<?php if($numbering_system->id == $data->numbering_system_id && $numbering_system->svg): ?>
									<?php echo $numbering_system->svg; ?>

								<?php endif; ?>
							<?php else: ?>
								<?php if($numbering_system->svg): ?>
									<?php echo $numbering_system->svg; ?>

								<?php endif; ?>
								<?php 
									break;
								?>
							<?php endif; ?>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>						
					</div>
					<div class="prescription-tutorial-link" onclick="showTutorial()"><?php echo e(__('translations.tutorial')); ?></div>
				</div>
				<div class="order-prescription-services">
					<div class="panel-head order-prescription-servicesHead" style="<?php if(!isset($prescriptions) || (isset($prescriptions) && !count($prescriptions)) ): ?><?php echo e('display: none;'); ?><?php else: ?><?php echo e('display:block'); ?><?php endif; ?> text-align: center"><?php echo e(__('translations.selected_order_services')); ?>:</div>
					<?php
						$prescription_colors = [
							1 => '#ffbab0',
							2 => '#F2EDA2',
							3 => '#BBDCEF',
							4 => '#CAE8CE',
							5 => '#F7C69C',
							6 => '#f3d9da',
							7 => '#ffb6ff',
							8 => '#b58585',
							9 => '#d2d222',
							10 => '#DADADA'
						];
					?>
					<div id="order_prescription_services">
						<?php if(isset($prescriptions) && count($prescriptions)): ?>
							<?php $__currentLoopData = $prescriptions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $prescription): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
									<?php
										$indx = $prescription_index <= 10 ? $prescription_index : 1;
										$color = $prescription_colors[$indx];
									?>
									<div id="order_prescription-service<?php echo e($prescription_index); ?>" class="fx fxb order-prescription-service">
									<input type="hidden" name="prescriptions[<?php echo e($prescription_index); ?>][price]" id="order_prescription_price<?php echo e($prescription_index); ?>" class="order-prescription-price-input" value="<?php echo e($prescription['advance_price']); ?>">
									<input type="hidden" name="prescriptions[<?php echo e($prescription_index); ?>][price_for]" id="order_prescription_price_for<?php echo e($prescription_index); ?>" value="<?php echo e($prescription['price_for']); ?>">
									<input type="hidden" id="order_prescription_available_statuses<?php echo e($prescription_index); ?>" value="<?php echo e($prescription['available_statuses']); ?>">
									<input type="hidden" name="prescriptions[<?php echo e($prescription_index); ?>][price_order_date]" id="order_prescription_price_order_date<?php echo e($prescription_index); ?>" value="<?php echo e($prescription['price_order_date']); ?>">
									<div id="order_prescription_name<?php echo e($prescription_index); ?>" class="fx fac order-prescription-name <?php if($prescription_index == 1): ?><?php echo e('order-prescription-name-act'); ?><?php endif; ?>" onclick="showActivePrescription(<?php echo e($prescription_index); ?>)">
									<div class="order-prescription-check" title="<?php echo e($prescription['service_name']); ?>"></div>
									<div id="order_prescription_color<?php echo e($prescription_index); ?>" class="order-prescription-color" data-color="<?php echo e($color); ?>" style="background-color:<?php echo e($color); ?>" title="<?php echo e($prescription['service_name']); ?>"></div>
									<div style="width: calc(100% - 55px);">#<?php echo e($prescription_index); ?> <strong><?php echo e($prescription['service_name']); ?></strong></div>
									  </div>						
									  <div class="order-prescription-scroll" onclick="scrollToblock(\'order_prescription-block<?php echo e($prescription_index); ?>\')" title="<?php echo e($prescription['service_name']); ?>"><?php echo e(__('translations.show_prescription')); ?></div>
										<div class="btn btn-danger delete-order-prescription" onclick="removePrescriptionFromOrder(<?php echo e($prescription_index); ?>);"><?php echo e(__('translations.delete')); ?></div>
									<input type="text" name="prescriptions[<?php echo e($prescription_index); ?>][selecteds]" id="prescriptions_selecteds_<?php echo e($prescription_index); ?>" class="prescription-selecteds" value="<?php echo e($prescription['selecteds_data']); ?>" required>
									<div class="prescription-selecteds-text"><?php echo e(__('translations.selected_items')); ?>: <span id="prescriptions_selecteds_text_<?php echo e($prescription_index); ?>"><?php echo e($prescription['selecteds']); ?></span></div>
										<?php if($prescription['available_options'] && $prescription['available_options'] != 0): ?>
											<div class="fx prescription-selecteds-options">
												<span><?php echo e(__('translations.select')); ?>:</span>
												<?php if($prescription['available_options'] == 1 || $prescription['available_options'] == 3): ?>
													<div class="order-option-check" id="order_option_check1" onclick="selectAllTooths(1, <?php echo e($prescription_index); ?>)">Lower</div>
												<?php endif; ?>

												<?php if($prescription['available_options'] == 2 || $prescription['available_options'] == 3): ?>
													<div class="order-option-check" id="order_option_check2" onclick="selectAllTooths(2, <?php echo e($prescription_index); ?>)">Upper</div>
												<?php endif; ?>
											</div>
										<?php endif; ?>
									</div>
								<?php $prescription_index++; ?>
							<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
							<script>
								
							</script>
						<?php endif; ?>
					</div>
					<div class="order-prescription-total">
						<?php echo e(__('translations.advance_price')); ?>: <?php echo e($currency->symbol); ?><span id="order_prescription_total"><?php if(isset($data->total)): ?><?php echo e($data->total->advance_price); ?><?php else: ?><?php echo e('0'); ?><?php endif; ?></span> 
						<input type="hidden" id="advance_price" name="advance_price" value="<?php if(isset($data->total)): ?><?php echo e($data->total->advance_price); ?><?php else: ?><?php echo e('0'); ?><?php endif; ?>">
					</div>
				</div>
				<div class="form-line">	
					<label for="comment"><?php echo e(__('translations.comment')); ?></label>
					<textarea name="comment" id="comment" class="form-inut" value="" placeholder="<?php echo e(__('translations.comment')); ?>" maxlength="1000"><?php if(isset($data->comment) && $data->comment != ''): ?><?php echo e($data->comment); ?><?php endif; ?></textarea>
				</div>
			</div>
		</div>
	</div>
</div>				
<div id="order_prescriptions">
	<?php if(isset($prescriptions) && count($prescriptions)): ?>
		<?php $prescription_index2 = 1; ?>
		<?php $__currentLoopData = $prescriptions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $prescription): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
			<div id="order_prescription-block<?php echo e($prescription_index2); ?>" class="fx fxb panel-body">
				<div class="panel">
					<div class="btn btn-danger delete-order-prescription" onclick="removePrescriptionFromOrder(<?php echo e($prescription_index2); ?>);"><?php echo e(__('translations.delete')); ?></div>
					<div class="panel-head"><?php echo e(__('translations.order_prescription')); ?> #<?php echo e($prescription_index2); ?></div>
					<div id="order_prescription<?php echo e($prescription_index2); ?>" class="panel-block">
						<div class="form-line">
							<label><?php echo e(__('translations.numbering_system')); ?>: <strong><?php echo e($prescription['numbering_system_name']); ?></strong></label>
							<input type="hidden" name="prescriptions[<?php echo e($prescription_index2); ?>][numbering_system_id]" value="<?php echo e($prescription['numbering_system_id']); ?>">
						</div>
						<div class="form-line">
							<label><?php echo e(__('translations.service_name')); ?>: <strong><?php echo e($prescription['service_name']); ?> (<?php echo e($prescription['parent_name']); ?>)</strong></label>
							<input type="hidden" name="prescriptions[<?php echo e($prescription_index2); ?>][service_id]" value="<?php echo e($prescription['service_id']); ?>">
						</div>
			
						<?php if($prescription['fields']): ?>
							<?php $__currentLoopData = $prescription['fields']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $field): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
								<div class="form-line">	
									<div class="prescription-line-head">
										<label for="prescription<?php echo e($field['id']); ?>" class="prescription-name">
											<?php echo e($field['name']); ?>

											<?php if($field['help']): ?>
												<span class="prescription-line-help" onclick="showFieldHelp(<?php echo e($field['id']); ?>)">?</span>
											<?php endif; ?>
										</label>
										<?php if($field['description']): ?>
											<p class="prescription-desc"><?php echo e($field['description']); ?></p>
										<?php endif; ?>
									</div>
									<div class="fx fac prescription-line-content">
										<?php if($field['image']): ?> 
											<div class="prescription-image">
												<img src="<?php echo e(url('/').'/storage/'.$field['image']); ?>">
											</div>
										<?php endif; ?>

										<div class="fx prescription-inputs <?php if($field['image']): ?><?php echo e('prescription-inputs-half'); ?><?php endif; ?>">
		
										<?php if($field['field_type'] == 'text'): ?>
											<input type="text" name="prescriptions[<?php echo e($prescription_index2); ?>][fields][<?php echo e($field['id']); ?>]" id="prescription_<?php echo e($prescription_index2); ?>_<?php echo e($field['id']); ?>" value="<?php if(isset($prescription['user_fields'][$field['id']])): ?><?php echo e($prescription['user_fields'][$field['id']]['value']); ?><?php endif; ?>" placeholder="<?php echo e($field['name']); ?>" maxlength="255" <?php if($field['required'] == 1): ?><?php echo e('class="form-input form-input-required" required="required"'); ?><?php else: ?><?php echo e('class="form-input'); ?><?php endif; ?>>
										<?php endif; ?>

										<?php
											$selected_items = array();

											if (isset($prescription['user_fields'][$field['id']])) {
												$selected_items = explode(';', $prescription['user_fields'][$field['id']]['value']);
											}
										?>

										<?php if($field['field_type'] == 'select' && $field['values']): ?> 
											<select name="prescriptions[<?php echo e($prescription_index2); ?>][fields][<?php echo e($field['id']); ?>]" id="prescription_<?php echo e($prescription_index2); ?>_<?php echo e($field['id']); ?>" <?php if($field['required'] == 1): ?><?php echo e('class="form-input form-input-required" required="required"'); ?><?php else: ?><?php echo e('class="form-input'); ?><?php endif; ?>>	
												<?php $__currentLoopData = $field['values']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
													<?php 
														$selected_item = false; 
														
														if (in_array($value['value'], $selected_items)) {
															$selected_item = true;
														}
													?>
													<option value="<?php echo e($value['value']); ?>" <?php if($selected_item): ?><?php echo e('selected="selected"'); ?><?php endif; ?>><?php echo e($value['value']); ?></option>
												<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
											</select>
										<?php endif; ?>

										<?php if($field['field_type'] == 'checkbox' && $field['values']): ?>
											<?php $__currentLoopData = $field['values']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
												<div class="form-checkbox">	
													<?php if($value['image']): ?>
														<img src="<?php echo e(url('/').'/storage/'.$value['image']); ?>">
													<?php endif; ?>

													<?php 
														$selected_item = false; 
														
														if (in_array($value['value'], $selected_items)) {
															$selected_item = true;
														}
													?>

													<input type="checkbox" name="prescriptions[<?php echo e($prescription_index2); ?>][fields][<?php echo e($field['id']); ?>][<?php echo e($value['id']); ?>]" id="prescription_<?php echo e($prescription_index2); ?>_<?php echo e($field['id']); ?>_<?php echo e($value['id']); ?>" value="<?php echo e($value['value']); ?>" <?php if($selected_item): ?><?php echo e('checked="checked"'); ?><?php endif; ?>>
													<label for="prescription_<?php echo e($prescription_index2); ?>_<?php echo e($field['id']); ?>_<?php echo e($value['id']); ?>"><?php echo e($value['value']); ?></label>
												</div>
											<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
										<?php endif; ?>

										<?php if($field['field_type'] == 'radio' && $field['values']): ?>
											<?php $__currentLoopData = $field['values']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
												<div class="form-checkbox">	
													<?php if($value['image']): ?>
														<img src="<?php echo e(url('/').'/storage/'.$value['image']); ?>">
													<?php endif; ?>

													<?php 
														$selected_item = false; 
														
														if (in_array($value['value'], $selected_items)) {
															$selected_item = true;
														}
													?>

													<input type="radio" name="prescriptions[<?php echo e($prescription_index2); ?>][fields][<?php echo e($field['id']); ?>]" id="prescription_<?php echo e($prescription_index2); ?>_<?php echo e($field['id']); ?>_<?php echo e($value['id']); ?>" value="<?php echo e($value['value']); ?>" <?php if($selected_item): ?><?php echo e('checked="checked"'); ?><?php endif; ?>>
													<label for="prescription_<?php echo e($prescription_index2); ?>_<?php echo e($field['id']); ?>_<?php echo e($value['id']); ?>"><?php echo e($value['value']); ?></label>
												</div>
											<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
										<?php endif; ?>
						
									</div>
								</div>
								</div>
							<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
						<?php endif; ?>

						<div class="form-line">	
							<label for="prescription_<?php echo e($prescription_index2); ?>_comment"><?php echo e(__('translations.comment')); ?></label>
							<textarea name="prescriptions[<?php echo e($prescription_index2); ?>][comment]" id="prescription_<?php echo e($prescription_index2); ?>_comment" class="form-inut" placeholder="<?php echo e(__('translations.comment')); ?>" maxlength="1000"><?php if($prescription['comment']): ?><?php echo e($prescription['comment']); ?><?php endif; ?></textarea>
						</div>
					</div>

					<?php if($prescription['prescription_description']): ?>
						<div class="panel-head panel-subhead panel-head-collapse panel-head-collapsed"><?php echo e(__('translations.prescription_instruction')); ?></div>
						<div class="fx fxb panel-block panel-block-collapsed">
							<?php echo $prescription['prescription_description']; ?>

						</div>
					<?php endif; ?>

				</div>
			</div>
			<?php $prescription_index2++; ?>
		<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
	<?php endif; ?>
</div>		
<div class="panel panel-transparent panel-buttons" style="<?php if(!isset($prescriptions) || (isset($prescriptions) && !count($prescriptions)) ): ?><?php echo e('display: none;'); ?><?php endif; ?>" id="create_order_button">
	<div class="form-line button-submit">	
		<div id="add_more_prescriptions" class="btn btn-primary" onclick="scrollToblock('service_id')"><?php echo e(__('translations.add_more_prescriptions')); ?></div>								
		<input id="submit" type="submit" class="btn" value="<?php if(!isset($data->prescriptions) || (isset($data->prescriptions) && !count($data->prescriptions)) ): ?><?php echo e(__('translations.order')); ?><?php else: ?><?php echo e(__('translations.save')); ?><?php endif; ?>">
	</div>
</div><?php /**PATH /var/www/d2l.sitepark.ua/resources/views/Frontend/templates/checkout_form.blade.php ENDPATH**/ ?>