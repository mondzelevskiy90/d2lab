<?php $__env->startSection('css'); ?>
	<link rel="stylesheet" href="<?php echo e(mix('/css/account.css')); ?>">
	<style>
		.form-line label{
			width: 100%;
			max-width: 100%;
		}
		.form-buttons{
			text-align: right;
		}
		.ticket{
			width: 100%;
			margin-bottom: 20px;
    		margin-top: 10px;
		}
		.ticket-head{
			position: relative;
		    width: 100%;
		    padding: 10px 45px 10px 20px;
		    border-radius: 5px;
		    background-color: transparent;
		    font-size: 18px;
		    cursor: pointer;		    
    		opacity: 0.8;
		}
		.ticket-head:hover{
			opacity: 1;
		}
		.ticket-head:after{
			content: "";
		    position: absolute;
		    top: 17px;
		    right: 17px;
		    width: 0;
		    height: 0;
		    border-style: solid;
		    border-width: 0 7px 10px 7px;
    		border-color: transparent transparent #fff transparent;
    		color: #000;
		} 
		.ticket-head-act:after{
			border-width: 10px 7px 0 7px;
    		border-color: #fff transparent transparent transparent;
		}
		.ticket-ok .ticket-head{
			color: #fff;
			background-color: green
		} 
		.ticket-wait .ticket-head{
			color: #fff;
			background-color: #f73f3f
		}
		.ticket-content{
			display: none;
			padding: 20px;
    		border-bottom: 1px solid #e9e9e9;
		} 
		.ticket-content-act{
			display: block;
		}
		
	</style>	
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>	
	<form class="tickets-editing-form" method="POST" action="<?php echo e(route('tickets')); ?>">
		<div id="panel_top_buttons" class="fx fac fxb">
			<?php echo $__env->make('Frontend.templates.breadcrumbs', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
			<div class="panel-top-buttons button-submit order-buttons">
				<a class="btn btn-small btn-grey" href="<?php echo e(route('account')); ?>">&larr; <?php echo e(__('translations.back')); ?></a>
			</div>
		</div>
		<h1 class="tc"><?php echo e($seo->name); ?></h1>
		<?php echo e(csrf_field()); ?>		
		<div class="fx fxb panel-body">
			<div class="panel">
				<div class="form-line">
					<label for="question"><?php echo e(__('translations.problem_description_label')); ?>:</label>
					<textarea name="question" id="question" class="form-input form-input-required" placeholder="<?php echo e(__('translations.problem_description')); ?>" maxlength="1000" required="required"></textarea>
				</div>
				<div class="form-line form-buttons">
					<input type="submit" id="submit" class="btn btn-small" name="submit" value="<?php echo e(__('translations.create_ticket')); ?>">
				</div>
			</div>				
		</div>
	</form>
	<?php if($data && count($data)): ?>
		<div class="fx fxb panel-body">
			<div class="panel">			
				<?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
					<div class="ticket <?php if($item->answer): ?><?php echo e('ticket-ok'); ?><?php else: ?><?php echo e('ticket-wait'); ?><?php endif; ?>">
						<div class="ticket-head <?php if($key == 0): ?><?php echo e('ticket-head-act'); ?><?php endif; ?>">
							<?php echo e(__('translations.ticket')); ?> #<?php echo e($item->id); ?> (<?php echo e(date('m-d-Y', strtotime($item->created_at))); ?>)
						</div>
						<div class="ticket-content <?php if($key == 0): ?><?php echo e('ticket-content-act'); ?><?php endif; ?>">
							<p><strong><?php echo e(__('translations.you_question')); ?>:</strong></p>
							<p><?php echo e($item->question); ?></p>
							<p><strong><?php echo e(__('translations.adm_answer')); ?>:</strong></p>
							<p>
								<?php if($item->answer): ?>
									<?php echo e($item->answer); ?>

								<?php else: ?>
									<span style="color:red"><?php echo e(__('translations.no_adm_answer')); ?></span>
								<?php endif; ?>
							</p>
						</div>
					</div>
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> 			
			</div>
		</div>
	<?php endif; ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>	
	<script>
		document.addEventListener('DOMContentLoaded', function(){
			$('.ticket-head').on('click', function(){
				$('.ticket-head').removeClass('ticket-head-act');
				$('.ticket-content').removeClass('ticket-content-act');

				$(this).addClass('ticket-head-act').next('.ticket-content').addClass('ticket-content-act');
			});
		}, !1);
	</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('Frontend/account_master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/dentist2lab.com/resources/views/Frontend/pages/account/tickets.blade.php ENDPATH**/ ?>