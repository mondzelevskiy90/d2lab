<?php $__env->startSection('css'); ?>
	<link rel="stylesheet" href="/css/select.css">
	<link rel="stylesheet" href="/css/daterangepicker.css">
	<link rel="stylesheet" href="<?php echo e(mix('/css/account.css')); ?>">
	<style>
		.table-condensed thead tr:nth-child(2), .table-condensed tbody {
			display: none
		}
		.daterangepicker select.monthselect{
			min-width: 120px;
		}
	</style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
	<div id="panel_top_buttons" class="fx fac fxb">
		<?php echo $__env->make('Frontend.templates.breadcrumbs', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
		<div class="panel-top-buttons button-submit order-buttons">
			<a class="btn btn-small btn-grey" href="<?php echo e(route('reports')); ?>">&larr; <?php echo e(__('translations.back')); ?></a>
			<div class="btn btn-small" onclick="printContent('invoice_print_content');"><?php echo e(__('translations.print_all_prescriptions')); ?></div>
		</div>
	</div>
	<div class="panel panel-transparent">
		<form method="GET" action="<?php echo e(route('report', $data->slug)); ?>" id="filters_block" class="fx fac">
			<?php echo e(csrf_field()); ?>	
			<?php if($branches && count($branches) && (Auth::user()->role_id == config('roles.lab_head') || Auth::user()->role_id == config('roles.clinic_head'))): ?>
				<div class="form-filter">
					<select name="branch" id="branch" class="form-input">	
						<option value="0"> -- <?php echo e(__('translations.branch')); ?> --</option>
						<?php $__currentLoopData = $branches; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $branch): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<?php if($branch['status'] != 4): ?>
								<option value="<?php echo e($branch['id']); ?>" <?php if(isset($filters['branch']) && $filters['branch'] == $branch['id']): ?><?php echo e('selected="selected"'); ?><?php endif; ?>><?php echo e($branch['name']); ?></option>
							<?php endif; ?>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					</select>		
				</div>
			<?php endif; ?>	
			<div class="form-filter">
				<select name="order_branche" id="order_branche" class="form-input">	
					<option value="0">
						<?php if(Auth::user()->type == config('types.clinic')): ?>
							-- <?php echo e(__('translations.select').' '.__('translations.lab')); ?> --
						<?php endif; ?>
						<?php if(Auth::user()->type == config('types.lab')): ?>
							-- <?php echo e(__('translations.select').' '.__('translations.clinic')); ?> --
						<?php endif; ?>
					</option>
					<?php $__currentLoopData = $order_branches; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $order_branche): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
						<?php if(Auth::user()->type == config('types.clinic')): ?>
							<option value="<?php echo e($order_branche['to_branch_id']); ?>" <?php if(isset($filters['order_branche']) && $filters['order_branche'] == $order_branche['to_branch_id']): ?><?php echo e('selected="selected"'); ?><?php endif; ?>><?php echo e($order_branche['to_branch']['name']); ?></option>
						<?php endif; ?>
						<?php if(Auth::user()->type == config('types.lab')): ?>
							<option value="<?php echo e($order_branche['branch_id']); ?>" <?php if(isset($filters['order_branche']) && $filters['order_branche'] == $order_branche['branch_id']): ?><?php echo e('selected="selected"'); ?><?php endif; ?>><?php echo e($order_branche['branch']['name']); ?></option>
						<?php endif; ?>					
					<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				</select>	
			</div>
			<div class="form-filter">
				<input type="text" name="date" id="date" class="form-input" placeholder="<?php echo e(__('translations.from_date')); ?>" value="<?php if(isset($filters['date']) && $filters['date']): ?><?php echo e($filters['date']); ?><?php endif; ?>">			
			</div>
			<div class="filter-buttons" style="text-align: right;">					
				<input type="submit" class="btn btn-small" value="<?php echo e(__('translations.filter')); ?>">
				<a href="<?php echo e(route('report', $data->slug)); ?>" class="btn btn-danger btn-small"><?php echo e(__('translations.clear')); ?></a>
			</div>
		</form>
	</div>
	<!--<h1 class="tc"><?php echo e($seo->name); ?></h1>-->
	<div id="invoice_print_content">
		<?php $cnt = 0; ?>
		<?php $__currentLoopData = $content; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $branch): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
			<?php $__currentLoopData = $branch['order_branches']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $order_branche): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
				<?php if($order_branche['report_data'] && $order_branche['report_data']['orders'] && count($order_branche['report_data']['orders'])): ?>
					<div class="fx fxb panel-body">
						<div class="fx fac fxb panel">
							<div id="invoice_print_content<?php echo e($cnt); ?>" class="invoice-print-content">
								<div style="width: 1022px;position: relative; page-break-after: always;">
									<div class="fx invoice-to_branch-info" style="justify-content: flex-end;">
										<div class="invoice-to_branch-right">
											<div class="invoice-invoice-head"><?php echo e($data->name); ?></div>
											<br>
											<table class="invoice-table invoice-small-table invoice-table-date">
												<thead>
													<tr>
														<td><?php echo e(__('translations.from')); ?></td>
														<td><?php echo e(__('translations.to')); ?></td>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td><?php echo e(date('m-d-Y', strtotime($filters['date']))); ?></td>
														<td><?php echo e(date('m-t-Y', strtotime($filters['date']))); ?></td>
													</tr>
												</tbody>
											</table>						
										</div>
									</div>									
									<div class="fx fxb invoice-branch-info">
										<table class="invoice-table invoice-short-table invoice-table-bill">
											<thead>
												<tr>
													<td><?php echo e(__('translations.clinic')); ?> <?php echo e(__('translations.branch')); ?></td>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td>
														<?php if(Auth::user()->type == config('types.clinic')): ?>
															<p><strong><?php echo e($branch['name']); ?></strong></p>
															<p><?php echo e($branch['zip_code'] .', '. $branch['address'] .', '. App\Services\Userdata::getLocality($branch['locality'])); ?></p>
														<?php endif; ?>
														<?php if(Auth::user()->type == config('types.lab')): ?>
															<p><strong><?php echo e($order_branche['branch']['name']); ?></strong></p>
															<p><?php echo e($order_branche['branch']['zip_code'] .', '. $order_branche['branch']['address'] .', '. App\Services\Userdata::getLocality($order_branche['branch']['locality'])); ?></p>
														<?php endif; ?>
													</td>
												</tr>
											</tbody>
										</table>
										<table class="invoice-table invoice-short-table invoice-table-ship">
											<thead>
												<tr>
													<td><?php echo e(__('translations.lab')); ?></td>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td>
														<?php if(Auth::user()->type == config('types.clinic')): ?>
															<p><strong><?php echo e($order_branche['branch']['name']); ?></strong></p>
															<p><?php echo e($order_branche['branch']['zip_code'] .', '. $order_branche['branch']['address'] .', '. App\Services\Userdata::getLocality($order_branche['branch']['locality'])); ?></p>
														<?php endif; ?>
														<?php if(Auth::user()->type == config('types.lab')): ?>
															<p><strong><?php echo e($branch['name']); ?></strong></p>
															<p><?php echo e($branch['zip_code'] .', '. $branch['address'] .', '. App\Services\Userdata::getLocality($branch['locality'])); ?></p>
														<?php endif; ?>														
													</td>
												</tr>
											</tbody>
										</table>
									</div>
									<table class="invoice-table invoice-full-table orders-report-table">
										<thead>
											<tr>
												<td>ID</td>
												<td><?php echo e(__('translations.date')); ?></td>
												<td>
													<?php if(Auth::user()->type == config('types.clinic')): ?>
														<?php echo e(__('translations.created_by')); ?>

													<?php endif; ?>
													<?php if(Auth::user()->type == config('types.lab')): ?>
														<?php echo e(__('translations.executed_by')); ?>

													<?php endif; ?>
												</td>
												<td><?php echo e(__('translations.status')); ?></td>
												<td><?php echo e(__('translations.payment_status')); ?></td>
												<td><?php echo e(__('translations.amount')); ?></td>
											</tr>
										</thead>
										<tbody>
											<?php $total = 0.00; ?>
											<?php $__currentLoopData = $order_branche['report_data']['orders']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $order): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
												<tr>
													<td><a href="<?php echo e(route('orders.edit', $order->id)); ?>">#<?php echo e($order->id); ?></a></td>
													<td><?php echo e(date('m-d-Y', strtotime($order->created_at))); ?></td>
													<td>
														<?php if(Auth::user()->type == config('types.clinic')): ?>
															<?php echo e($order->user_name); ?>

														<?php endif; ?>
														<?php if(Auth::user()->type == config('types.lab')): ?>
															<?php if($order->executor_id != 0): ?>
																<?php echo e($order->executor_name); ?>

															<?php else: ?> 
																<?php echo e(__('translations.not_assigned')); ?>

															<?php endif; ?>
														<?php endif; ?>
													</td>
													<td><?php echo e($order->status_info->name); ?></td>
													<td><?php echo e($order->payment_info->name); ?></td>
													<td style="text-align: right">
														<?php if($order->total && $order->total->price && $order->total->price != '0.00'): ?>
															<?php $total = (float)$total + (float)$order->total->price ?>
															<?php echo e($currency->symbol); ?><?php echo e($order->total->price); ?>

														<?php else: ?> 
															---
														<?php endif; ?>
													</td>
												</tr>
											<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
										</tbody>
										<tfoot>
											<tr>
												<td colspan="4"></td>
												<td colspan="2">
													<div class="fx fac fxb">
														<strong><?php echo e(__('translations.total')); ?></strong>
														<span><strong><?php echo e($currency->symbol); ?><?php echo e(number_format($total, 2)); ?></strong></span>
													</div>
												</td>
											</tr>
										</tfoot>
									</table>
								</div>								
								<br>
								<div class="form-line print-btn-block not-print">
									
										<div class="btn btn-small btn-green" onclick="exportToExcel(<?php echo e($branch['id']); ?>, <?php echo e($order_branche['branch']['id']); ?>)">
										    <?php echo e(__('translations.export_excel')); ?>

									    </div>
								
									
									<div class="btn btn-small" onclick="printContent('invoice_print_content<?php echo e($cnt); ?>');"><?php echo e(__('translations.print')); ?></div>&nbsp;
								</div>
							</div>
						</div>
					</div>
				<?php endif; ?>
				<?php $cnt++; ?>
			<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>	
		<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>	
	</div>		
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
	<script src="/js/select.js" defer></script>
	<script src="/js/moment.min.js" defer></script>
	<script src="/js/daterangepicker.js" defer></script>
	<script>
		document.addEventListener('DOMContentLoaded', function(){
			$('#branch, #order_branche').select2({
				minimumResultsForSearch: -1
			});

			$('#date').on('focus', function(){
				$(this).daterangepicker({				
					singleDatePicker: true,
					showDropdowns: true,
					startDate: '<?php echo e(date('y-m')); ?>',
					autoApply: true,
					locale: {
				        format: 'YYYY-MM-DD'
				    },
				}).on('showCalendar.daterangepicker', function (ev, picker) {
					var days = document.querySelectorAll('.table-condensed tbody td.available');

					for (var i = 0; i <= days.length; i++) {
						if (days[i].classList.contains('off')) {
							continue;
						} else {
							days[i].classList.add('active');
							days[i].classList.add('start-date');
							days[i].classList.add('end-date');

							var month_value = parseInt(document.querySelector('.monthselect').value) + 1;
							var month = month_value < 10 ? '0'+month_value : month_value;
							var year = document.querySelector('.yearselect').value;

							picker.setStartDate(moment(year+'-'+month+'-'+days[i].innerHTML, 'YYYY-MM-DD'));
							
							break;
						}
					}

				  	$(this).val(picker.startDate.format('YYYY-MM-DD'));
				  	$(document).find('.applyBtn').click();
				});
			});
		}, !1);

		function printContent(id) {
			var new_window =window.open();
			var html = '<link rel="stylesheet" href="<?php echo e(mix('/css/styles.css')); ?>">';

			html += '<link rel="stylesheet" href="<?php echo e(mix('/css/account.css')); ?>">';
			html += '<link rel="stylesheet" href="<?php echo e(mix('/css/print_invoice.css')); ?>">';
			html += '<div class="fx fxb print-source"><span><?php echo e(config('app.url')); ?></span><span><?php echo e(date('m-d-Y')); ?></span></div>';
			html += document.getElementById(id).innerHTML;

			new_window.document.write(html);

			setTimeout(function(){
				new_window.print();
				new_window.close();
			}, 1000);
		}

		function exportToExcel(branch_id, to_branch_id) {
			showLoader();
			$.ajax({
				url: '<?php echo e(route('excel')); ?>',
				type: 'post',
	            dataType: 'json',
	            headers: {
	                'X-CSRF-TOKEN': '<?php echo e(csrf_token()); ?>'
	            },
			    data: {type: '<?php echo e($data->slug); ?>', branch_id: branch_id, to_branch_id: to_branch_id},
			    success: function(data) {
			    	if (data) {
			    		hideLoader();
			    		showPopup('<div class="success"><a class="btn" href="'+data+'" onclick="document.getElementById(\'cboxClose\').click();">Download</a></div>', true);
			    	}
			    }						    
			});
		}
	</script>	
<?php $__env->stopSection(); ?>
<?php echo $__env->make('Frontend/account_master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/d2l.sitepark.ua/resources/views/Frontend/pages/account/reports/orders_list.blade.php ENDPATH**/ ?>