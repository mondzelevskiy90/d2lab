<h2>Welcome to Dentist2Lab service!</h2>
<?php if($type): ?>
<?php 
	if ($type == config('types.clinic')) $account = 'Dentist';
	if ($type == config('types.lab')) $account = 'Lab';
?>
<p>Your account type is <?php echo e($account); ?>.</p>
<?php else: ?> 
<p>Your account type is Guest.</p>
<?php endif; ?>

<p><a href="<?php echo e($url); ?>">Confirm your e-mail</a></p>
<br>
<p>Warning! Do not share your login details to other people!</p><?php /**PATH /var/www/d2l.sitepark.ua/resources/views/Frontend/emails/registration.blade.php ENDPATH**/ ?>