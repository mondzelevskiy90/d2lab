<div class="content">
	<div class="fx fxb">
		<div class="logo-block footer-logo">
			<div class="bi logo" style="background-image: url('/img/logo.png')"></div>
		</div>
		<div class="footer-links">
			<a href="<?php echo e(url('article/about')); ?>"><?php echo e(__('translations.about_us')); ?></a>
			<a href="<?php echo e(url('article/terms')); ?>"><?php echo e(__('translations.terms')); ?></a>
			<a href="<?php echo e(url('article/privacy-policy')); ?>"><?php echo e(__('translations.private_policy')); ?></a>
			<a href="<?php echo e(url('article/faqs')); ?>"><?php echo e(__('translations.faq')); ?></a>
			<a href="<?php echo e(url('sitemap')); ?>"><?php echo e(__('translations.sitemap')); ?></a>
		</div>
		<div class="footer-socials">
			<a href="/" class="bi fcb"></a>
			<a href="/" class="bi twtr"></a>
			<a href="/" class="bi inst"></a>
		</div>
	</div>
</div>
<div id="loader" class="fx fxc">
	<div class="bi"></div>
</div>		
<script src="/js/common.js" defer></script>		
<?php echo $__env->yieldContent('scripts'); ?>
<?php if(session('message')): ?>
	<script>
		document.addEventListener('DOMContentLoaded', function() {
			showPopup('<div class="<?php echo e(session('alert-type')); ?>"><?php echo session('message'); ?></div>');
		}, !1);
	</script>	        
<?php endif; ?><?php /**PATH /var/www/d2l.sitepark.ua/resources/views/Frontend/parts/footer.blade.php ENDPATH**/ ?>