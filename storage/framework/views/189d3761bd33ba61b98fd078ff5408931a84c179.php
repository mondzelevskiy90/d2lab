<?php
	$branches = array();
        
        switch (Auth::user()->role_id) {
            case config('roles.clinic_head'):
            case config('roles.lab_head'):
                $branches = \App\Models\Branch::where('owner_id', Auth::user()->id)->pluck('id')->toArray();
                break;
            case config('roles.clinic_staff'):
            case config('roles.doctor'):
            case config('roles.lab_staff'):
            case config('roles.tech'):
                $staff = \App\Models\Branch_staff::where('user_id', Auth::user()->id)->first();

                if ($staff) $branches[] = $staff->branch_id;                            
                break;
        }

        $viewed = \App\Models\Notification_view::select('notification_id')
            ->where('user_id', Auth::user()->id)
            ->pluck('notification_id')
            ->toArray();

        $notifications = \App\Models\Notification_message::whereIn('branch_id', $branches)
            ->whereNotIn('id', $viewed)
            ->where('created_at', '<=', date('Y-m-d').' 23:59:59')
            ->select('id')
            ->count();


?>
<?php if($notifications): ?> 
	<a href="<?php echo e(route('notifications')); ?>" class="fx fxc bi head-notifications" title="<?php echo e(__('translations.notifications')); ?>">
		<div class="fx fxc head-notifications-count"><?php echo e($notifications > 99 ? '99+' : $notifications); ?></div>
	</a>
<?php endif; ?><?php /**PATH /var/www/dentist2lab.com/resources/views/Frontend/templates/head_notifications.blade.php ENDPATH**/ ?>