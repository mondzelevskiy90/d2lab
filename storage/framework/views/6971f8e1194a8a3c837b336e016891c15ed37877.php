<?php $__env->startSection('page_title', __('voyager::generic.'.(isset($data->id) ? 'edit' : 'add')).' '.$data->name); ?>

<?php $__env->startSection('css'); ?>
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('page_header'); ?>
    <h1 class="page-title">
        <i class="voyager-edit"></i>
        <?php echo e(__('voyager::generic.'.(isset($data->id) ? 'edit' : 'add')).' '.$data->name); ?>

    </h1>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="page-content container-fluid">
        <form class="form-edit-add" role="form" action="<?php if(!is_null($data->id)): ?><?php echo e(route('voyager.'.$type.'.update', [$data->id, $requests])); ?><?php else: ?><?php echo e(route('voyager.'.$type.'.store', [$requests])); ?><?php endif; ?>" method="POST" enctype="multipart/form-data" autocomplete="off">            
            <?php if(isset($data->id)): ?>
                <?php echo e(method_field("PUT")); ?>

            <?php endif; ?>
            <?php echo e(csrf_field()); ?>


            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-bordered">  
                        <div class="panel-body">                 
                            <?php if(count($errors) > 0): ?>
                                <div class="alert alert-danger">
                                    <ul>
                                        <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <li><?php echo e($error); ?></li>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </ul>
                                </div>
                            <?php endif; ?>                                                    
                            <div class="form-group">
                                <label for="status"><?php echo e(__('adm.status')); ?></label>
                                <select class="form-control" name="status">        
                                    <option value="1" <?php if(isset($data->status) && $data->status == 1): ?> selected <?php endif; ?>>Active</option>       
                                    <option value="2" <?php if(isset($data->status) && $data->status == 2): ?> selected <?php endif; ?>>Disactive</option>                                      
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="name"><?php echo e(__('adm.name')); ?></label>
                                <input type="text" class="form-control" id="name" name="name" placeholder="<?php echo e(__('adm.name')); ?>" value="<?php echo e(old('name', $data->name ?? '')); ?>" required="required">
                            </div>
                            <div class="form-group">
                                <label for="code">Block Type</label>
                                <select name="code" id="code" class="form-control">
                                    <option value="text" <?php if(isset($data->code) && $data->code == 'text'): ?><?php echo e('selected="selected"'); ?><?php endif; ?>>Text block</option>
                                    <option value="reviews" <?php if(isset($data->code) && $data->code == 'reviews'): ?><?php echo e('selected="selected"'); ?><?php endif; ?>>Latest reviews</option>
                                    <option value="notifications" <?php if(isset($data->code) && $data->code == 'notifications'): ?><?php echo e('selected="selected"'); ?><?php endif; ?>>Notifications</option>
                                    <option value="orders" <?php if(isset($data->code) && $data->code == 'orders'): ?><?php echo e('selected="selected"'); ?><?php endif; ?>>Orders data</option>
                                    <option value="statuses" <?php if(isset($data->code) && $data->code == 'statuses'): ?><?php echo e('selected="selected"'); ?><?php endif; ?>>Orders statuses</option>
                                    <option value="diagram" <?php if(isset($data->code) && $data->code == 'diagram'): ?><?php echo e('selected="selected"'); ?><?php endif; ?>>Orders statuses diagram</option>
                                    <option value="locations" <?php if(isset($data->code) && $data->code == 'locations'): ?><?php echo e('selected="selected"'); ?><?php endif; ?>>Locations data</option>
                                    <option value="graph" <?php if(isset($data->code) && $data->code == 'graph'): ?><?php echo e('selected="selected"'); ?><?php endif; ?>>Invoices graph</option>
                                    <option value="catalog" <?php if(isset($data->code) && $data->code == 'catalog'): ?><?php echo e('selected="selected"'); ?><?php endif; ?>>Catalog button</option>
                                    <option value="update" <?php if(isset($data->code) && $data->code == 'update'): ?><?php echo e('selected="selected"'); ?><?php endif; ?>>Update button</option>
                                </select>
                            </div> 
                            <div class="form-group">
                                <label for="content">Text content</label>
                                <textarea class="richTextBox form-control" id="content" name="content" placeholder="Text content"><?php echo e(old('content', $data->content ?? '')); ?></textarea>
                            </div>
                            <div class="form-group">
                                <label for="sort_order"><?php echo e(__('adm.sort_order')); ?></label>
                                <input type="text" class="form-control" id="sort_order" name="sort_order" placeholder="<?php echo e(__('adm.sort_order')); ?>" value="<?php echo e(old('sort_order', $data->sort_order ?? 0)); ?>">
                            </div> 
                            <div class="form-group">
                                <label>Available for roles</label>
                                <?php $__currentLoopData = $roles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $role): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <label for="roles_<?php echo e($role->id); ?>" style="width:100%;margin-bottom: 5px;">
                                        <input type="checkbox" name="roles[<?php echo e($role->id); ?>]" id="roles_<?php echo e($role->id); ?>" value="<?php echo e($role->id); ?>" <?php if(in_array($role->id, $checked_roles)): ?><?php echo e('checked="checked"'); ?><?php endif; ?>>
                                        <?php echo e($role->display_name); ?>

                                    </label>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> 
                            </div>
                        </div>
                    </div>
                </div>                
            </div>

            <button type="submit" class="btn btn-primary pull-right save">
                <?php echo e(__('voyager::generic.save')); ?>

            </button>
        </form>

        <iframe id="form_target" name="form_target" style="display:none"></iframe>
        <form id="my_form" action="<?php echo e(route('voyager.upload')); ?>" target="form_target" method="post" enctype="multipart/form-data" style="width:0px;height:0;overflow:hidden">
            <?php echo e(csrf_field()); ?>

            <input name="image" id="upload_file" type="file" onchange="$('#my_form').submit();this.value='';">
            <input type="hidden" name="type_slug" id="type_slug" value="<?php echo e($type); ?>">
        </form>
       
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>
    <script>
        $('document').ready(function () {
            $('.toggleswitch').bootstrapToggle();
        });
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('voyager::master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/dentist2lab.com/resources/views/Backend/dashboard/edit-add.blade.php ENDPATH**/ ?>