<?php if($content['data'] && count($content['data'])): ?>
	<div class="panel dashboard-item dashboard-item-<?php echo e($code); ?>">
		<div class="panel-head"><?php echo e(__('translations.notification_list')); ?></div>
		<?php if($content['text'] && $content['text'] != ''): ?>
			<div class="panel-intro"><?php echo $content['text']; ?></div>
		<?php endif; ?>
		<div class="panel-content">
			<?php $__currentLoopData = $content['data']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
				<div id="notification_<?php echo e($item->id); ?>" class="fx fac fxb dashboard-notification-line">
					<div class="dashboard-notification-date"><?php echo e(date('m-d-Y', strtotime($item->created_at))); ?></div>
					<div class="dashboard-notification-text"><?php echo $item->text; ?></div>
					<div class="dashboard-notification-action">
						<div class="btn btn-small" onclick="notificationViewed(<?php echo e($item->id); ?>)">Ok!</div>
					</div>
				</div>
			<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
		</div>
	</div>
	<script>
		function notificationViewed(id) {
			$.ajax({
				url: '/ajax/getData',
				type: 'post',
			    dataType: 'json',
			    headers: {
			        'X-CSRF-TOKEN': '<?php echo e(csrf_token()); ?>'
			    },
			    data: {action:'notificationViewed', id: id},	
			    success: function(data) {
			    	$('#notification_'+id).remove();
			    	
			    	if ($('.dashboard-notification-line').length == 0) window.location.reload();
			    }
			});
		}
	</script>
<?php endif; ?>
<?php /**PATH /var/www/d2l.sitepark.ua/resources/views/Frontend/templates/dashboard/notifications.blade.php ENDPATH**/ ?>