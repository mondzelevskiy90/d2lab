<?php $__env->startSection('css'); ?>
	<link rel="stylesheet" href="/css/select.css">
	<link rel="stylesheet" href="<?php echo e(mix('/css/account.css')); ?>">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('breadcrumbs'); ?>
	<?php echo $__env->make('Frontend.templates.breadcrumbs', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php $__env->stopSection(); ?>

<?php
function create_services_list($items, $currency, $price_services, $padding = 0) {
	foreach ($items as $item) {
		$line = '<div class="fx fac account-list-line '.(!empty($item['childrens']) ? 'account-list-headline' : '').' ap'.$padding.'">';
		$line .= '<div class="account-list-cell">'.$item['name'].'</div>';

		if (empty($item['childrens'])) {
			$line .= '<div class="account-list-cell">';
			$line .= '<input type="text" name="services['.$item['id'].'][price]" id="service_price['.$item['id'].']" class="form-input" placeholder="'.__('translations.price').'" value="'.(isset($price_services[$item['id']]) ? $price_services[$item['id']]['price'] : '').'" onchange="isNaN(this.value) ? this.value = null : this.value = this.value;">';
			$line .= '</div>';
			$line .= '<div class="account-list-cell">';
			$line .= '<input type="hidden" name="services['.$item['id'].'][currency_id]" value="'.$currency->id.'"/>';
			$line .= ''.$currency->code.'';
			$line .= '</div>';
			$line .= '<div class="account-list-cell">';
			$line .= '<input type="text" name="services['.$item['id'].'][min_days]" id="service_price_'.$item['id'].'_min_days" class="form-input" placeholder="'.__('translations.min_days').'" value="'.(isset($price_services[$item['id']]['min_days']) ? $price_services[$item['id']]['min_days'] : '').'" onchange="isNaN(this.value) ? this.value = null : this.value = this.value;">';
			$line .= '</div>';
			$line .= '<div class="account-list-cell account-list-cellOptions">';

			if (count($item['options'])) {				
				foreach ($item['options'] as $option) {
					$line .= '<div class="form-checkbox">';
					$line .= '<input type="checkbox" name="services['.$item['id'].'][options]['.$option->option_id.']" id="service_'.$item['id'].'_option_'.$option->id.'" class="" '.(isset($price_services[$item['id']]['options'][$option->option_id]) ? 'checked="checked"' : '').'>';
					$line .= '<label for="service_'.$item['id'].'_option_'.$option->id.'">'.$option->option_info->name.'</label>';
					$line .= '</div>';
				}				 
			}

			$line .= '</div>';
		}

		if ($item['price_for'] && $item['price_for'] == 2) {
			$line .= '<div style="font-size: 12px;font-weight: 400">*** '.__('translations.price_for_all_service').'</div>';
		}
		
		$line .= '</div>';

		echo $line;

		if (!empty($item['childrens'])) create_services_list($item['childrens'], $currency, $price_services, $padding + 10);
	}
}
?>

<?php $__env->startSection('content'); ?>
	<h1 class="tc"><?php echo e($seo->name); ?></h1>	
	<form method="POST" action="<?php echo e(isset($data->id) ? route('prices.edit', $data->id) : route('prices.create')); ?>" enctype="multipart/form-data">
		<div class="fx fxb panel-body prices-edit-form">		
			<?php echo e(csrf_field()); ?>			
			<div class="panel">	
			    <?php if(isset($data->id)): ?>
			    	<div class="form-line">
			    		<label for="branch"><?php echo e(__('translations.branch')); ?></label>
						<input type="text" id="branch" class="form-input" value="<?php echo e($branch->name); ?>" disabled="disabled">
			    	</div>
					<div class="form-line">
						<label for="name"><?php echo e(__('translations.name')); ?></label>				
						<input type="text" name="name" id="name" class="form-input" placeholder="<?php echo e(__('translations.name')); ?>" value="<?php echo e(old('name', $data->name)); ?>" required="required">
						<?php if($errors->has('name')): ?>
		                    <div class="error"><?php echo e($errors->first('name')); ?></div>
		                <?php endif; ?>				
					</div>	
					<?php if($services && count($services)): ?>
						<br><br>
						<div class="form-line">
							<div class="panel-head">
								<?php echo e(__('translations.services_list')); ?>

								<p style="font-size: 12px;font-weight: 400">*** <?php echo e(__('translations.select_only_needed')); ?></p>
							</div>
							<div class="account-long-list">
								<div class="account-list-table account-list-table5 account-list-tableServices">
									<div class="fx account-list-head">						
											<div class="account-list-cell"><?php echo e(__('translations.service_name')); ?></div>
											<div class="account-list-cell"><?php echo e(__('translations.price_value')); ?></div>
											<div class="account-list-cell"><?php echo e(__('translations.currency')); ?></div>
											<div class="account-list-cell"><?php echo e(__('translations.min_days')); ?></div>
											<div class="account-list-cell"></div>
									</div>
									<div class="account-list-content">
										<?php 
											create_services_list($services, $currency, $price_services);			
										?>
									</div>
								</div>
							</div>
						</div>
					<?php endif; ?>
					<?php if($delivery && count($delivery)): ?> 
						<br><br>
						<div class="form-line">
							<div class="panel-head">
								<?php echo e(__('translations.delivery_list')); ?>

								<p style="font-size: 12px;font-weight: 400">*** <?php echo e(__('translations.select_only_needed')); ?></p>
							</div>
							<div class="account-long-list">
								<div class="account-list-table account-list-table3 account-list-tableServices">
									<div class="fx account-list-head">						
											<div class="account-list-cell"><?php echo e(__('translations.name')); ?></div>
											<div class="account-list-cell"><?php echo e(__('translations.price')); ?> <?php echo e(__('translations.from')); ?></div>
											<div class="account-list-cell"><?php echo e(__('translations.currency')); ?></div>
									</div>
									<div class="account-list-content">
										<?php $__currentLoopData = $delivery; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $delivery_item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
											<div class="fx fac account-list-line">
												<div class="account-list-cell">
													<?php echo e($delivery_item->name); ?>

												</div>
												<div class="account-list-cell">
													<?php 
														if (isset($price_delivery[$delivery_item->id])) {
															$price = $price_delivery[$delivery_item->id]['value_from'];
														} else {
															$price = '0.00';
														}
													?>
													<input 
														type="text" 
														name="delivery[<?php echo e($delivery_item->id); ?>][value_from]" 
														id="delivery_<?php echo e($delivery_item->id); ?>" 
														class="form-input" 
														placeholder="<?php echo e(__('translations.price')); ?>" 
														value="<?php echo e($price); ?>" 
														onchange="isNaN(this.value) ? this.value = null : this.value = this.value;">
												</div>
												<div class="account-list-cell">
													<?php echo e($currency->code); ?>

													<input type="hidden" name="delivery[<?php echo e($delivery_item->id); ?>][currency_id]" value="<?php echo e($currency->id); ?>"/>
												</div>
											</div>
										<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
									</div>
								</div>
							</div>
						</div>
					<?php endif; ?>
			    <?php else: ?> 
					<div class="form-line">
						<label for="branch_id"><?php echo e(__('translations.branch')); ?></label>
						<select name="branch_id" id="branch_id" class="form-controll" required="required">
							<?php $__currentLoopData = $branches; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $branch): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
								<option value="<?php echo e($branch->id); ?>"><?php echo e($branch->name); ?></option>
							<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
						</select>
					</div>
			    <?php endif; ?>					
			</div>
			<div class="panel panel-transparent panel-buttons">
				<div class="form-line button-submit">				
					<input type="submit" id="submit" class="btn" name="submit" value="<?php if(isset($data->id)): ?><?php echo e(__('translations.save_changes')); ?><?php else: ?><?php echo e(__('translations.save')); ?><?php endif; ?>">
				</div>
			</div>
		</div>
	</form>	
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
	<?php echo $__env->make('Frontend.templates.fields_script', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('Frontend/account_master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/d2l.sitepark.ua/resources/views/Frontend/pages/account/prices/edit.blade.php ENDPATH**/ ?>