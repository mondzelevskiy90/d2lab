<?php $__env->startSection('page_title', __('voyager::generic.'.(isset($data->id) ? 'edit' : 'add')).' '.$data->name); ?>

<?php $__env->startSection('css'); ?>
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('page_header'); ?>
    <h1 class="page-title">
        <i class="voyager-edit"></i>
        <?php echo e(__('voyager::generic.'.(isset($data->id) ? 'edit' : 'add')).' '.$data->name); ?>

    </h1>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="page-content container-fluid">
        <form class="form-edit-add" role="form" action="<?php if(!is_null($data->id)): ?><?php echo e(route('voyager.'.$type.'.update', [$data->id, $requests])); ?><?php else: ?><?php echo e(route('voyager.'.$type.'.store', [$requests])); ?><?php endif; ?>" method="POST" enctype="multipart/form-data" autocomplete="off">            
            <?php if(isset($data->id)): ?>
                <?php echo e(method_field("PUT")); ?>

            <?php endif; ?>
            <?php echo e(csrf_field()); ?>


            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-bordered">  
                        <div class="panel-body">                 
                            <?php if(count($errors) > 0): ?>
                                <div class="alert alert-danger">
                                    <ul>
                                        <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <li><?php echo e($error); ?></li>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </ul>
                                </div>
                            <?php endif; ?>                                                    
                            <div class="form-group">
                                <label for="status"><?php echo e(__('adm.status')); ?></label>
                                <select class="form-control" name="status">        
                                    <option value="1" <?php if(isset($data->status) && $data->status == 1): ?> selected <?php endif; ?>>Active</option>       
                                    <option value="2" <?php if(isset($data->status) && $data->status == 2): ?> selected <?php endif; ?>>Suspended</option>
                                    <option value="3" <?php if(isset($data->status) && $data->status == 3): ?> selected <?php endif; ?>>Blocked</option>
                                    <option value="4" <?php if(isset($data->status) && $data->status == 4): ?> selected <?php endif; ?>>Removed</option>     
                                </select>
                            </div> 
                            <div class="form-group">
                                <label for="verified">Verified status</label>                                
                                <select class="form-control" name="verified">
                                    <option value="1" <?php if(isset($data->verified) && $data->verified == 1): ?> selected <?php endif; ?>>Verified</option>       
                                    <option value="2" <?php if(isset($data->verified) && $data->verified == 2): ?> selected <?php endif; ?>>Not verified</option>
                                </select>                                
                            </div>
                            
                            <div class="form-group">
                                <label for="name"><?php echo e(__('adm.name')); ?></label>
                                <input type="text" class="form-control" id="name" name="name" placeholder="<?php echo e(__('adm.name')); ?>" value="<?php echo e(old('name', $data->name ?? '')); ?>">
                            </div> 
                            <div class="form-group">
                                <label for="description"><?php echo e(__('adm.description')); ?></label>
                                <textarea class="form-control" id="description" name="description" placeholder="<?php echo e(__('adm.description')); ?>"><?php echo e(old('description', $data->description ?? '')); ?></textarea>
                            </div>  
                            <div class="form-group">
                                <label for="email"><?php echo e(__('adm.email')); ?></label>
                                <input type="email" class="form-control" id="email" name="email" placeholder="<?php echo e(__('adm.email')); ?>" value="<?php echo e(old('email', $data->email ?? '')); ?>">
                            </div>                          
                            <div class="form-group">
                                <label for="phone"><?php echo e(__('adm.phone')); ?></label>
                                <input type="text" class="form-control" id="phone" name="phone" placeholder="<?php echo e(__('adm.phone')); ?>" value="<?php echo e(old('phone', $data->phone ?? '')); ?>">
                            </div>
                            <?php if(!is_null($data->id)): ?>
                                <div class="form-group">
                                    <label for="address"><?php echo e(__('adm.address')); ?></label>
                                    <input type="text" class="form-control" id="address" value="<?php echo e($address); ?>" disabled="disabled">
                                </div>  
                            <?php endif; ?> 
                            <p>Coordinate detection service - <strong><a target="_blank" href="https://3planeta.com/gps-tools/ru/index.html">---Click---</a></strong></p>                           
                            <div class="form-group">
                                <label for="latitude">Latitude</label>
                                <input type="text" class="form-control" name="latitude" id="latitude" placeholder="Latitude" value="<?php echo e(old('latitude', $latitude ?? '')); ?>">
                            </div>
                            <div class="form-group">
                                <label for="longitude">Longitude</label>
                                <input type="text" class="form-control" name="longitude" id="longitude" placeholder="Longitude" value="<?php echo e(old('longitude', $longitude ?? '')); ?>">
                            </div>
                            <?php if(isset($data->logo) && $data->logo != ''): ?>
                            <div class="form-group">
                                <label for="logo"><?php echo e(__('adm.image')); ?></label>
                                
                                    <img src="<?php echo e(filter_var($data->logo, FILTER_VALIDATE_URL) ? $data->logo : Voyager::image( $data->logo )); ?>" style="width:200px; height:auto; clear:both; display:block; padding:2px; border:1px solid #ddd; margin-bottom:10px;" />
                               
                            </div>
                            <?php endif; ?>
                            <input type="hidden" name="price" value="<?php if($price): ?><?php echo e($price->id); ?><?php endif; ?>">
                        </div>
                    </div>
                </div>                
            </div>

            <button type="submit" class="btn btn-primary pull-right save">
                <?php echo e(__('voyager::generic.save')); ?>

            </button>
            <div class="clearfix"></div>
        </form>
        <?php if(count($staff)): ?>
        <br>
        <br>
        <div class="panel panel-bordered"> 
            <div class="panel-body">
                <h3>Department staff</h3>
                <br>                
                <ul>
                <?php $__currentLoopData = $staff; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
                    <li><a href="<?php echo e(route('voyager.users.edit', [$user->user_id])); ?>"><?php echo e($user->user->name.' '.$user->user->surname.' ('.$user->role->display_name.')'); ?></a></li>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </ul>
            </div> 
        </div>
        <?php endif; ?>
        <?php if(count($portfolio)): ?>
        <br>
        <br>
        <div class="panel panel-bordered"> 
            <div class="panel-body">
                <h3>Department portfolio</h3>
                <br>                
                <?php $__currentLoopData = $portfolio; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $image): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
                    <img src="<?php echo e(App\Services\Img::getThumb($image->image)); ?>" style="width:125px; height:auto; clear:both; display:block; padding:2px; border:1px solid #ddd; margin-bottom:20px;margin-right: 15px; display: inline-block; vertical-align:top" />
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <br>

                <p><a class="btn btn-primary pull-right" href="<?php echo e(route('voyager.portfolios.index', ['search' => $data->name ])); ?>">Manage department portfolio</a></p>
                <div class="clearfix"></div>
            </div> 
        </div>
        <?php endif; ?>

        <iframe id="form_target" name="form_target" style="display:none"></iframe>
        <form id="my_form" action="<?php echo e(route('voyager.upload')); ?>" target="form_target" method="post" enctype="multipart/form-data" style="width:0px;height:0;overflow:hidden">
            <?php echo e(csrf_field()); ?>

            <input name="image" id="upload_file" type="file" onchange="$('#my_form').submit();this.value='';">
            <input type="hidden" name="type_slug" id="type_slug" value="<?php echo e($type); ?>">
        </form>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>
    <script>
        $('document').ready(function () {
            $('.toggleswitch').bootstrapToggle();
        });
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('voyager::master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/d2l.sitepark.ua/resources/views/Backend/branches/edit-add.blade.php ENDPATH**/ ?>