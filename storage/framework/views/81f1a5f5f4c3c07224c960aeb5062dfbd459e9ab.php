<div class="panel panel-half dashboard-item dashboard-item-half dashboard-item-<?php echo e($code); ?>">
	<div class="panel-head"><?php echo e(__('translations.order_statuses_list')); ?></div>
	<?php if($content['text'] && $content['text'] != ''): ?>
		<div class="panel-intro"><?php echo $content['text']; ?></div>
	<?php endif; ?>
	<div class="panel-content">
		<?php if($content['data'] && count($content['data'])): ?>
			<div class="dashboard-statuses-list">
				<?php $__currentLoopData = $content['data']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
					<a href="<?php echo e(route('orders', ['status' => $item['id']])); ?>" class="fx fac fxb dashboard-statuses-line">
						<span class="bi dashboard-statuses-icon" style="background-image: url(<?php echo e(url('/').'/storage/'.$item['icon']); ?>)"></span>
						<span class="dashboard-statuses-name"><?php echo e($item['name']); ?></span>
						<span class="dashboard-statuses-count"><?php echo e($item['count']); ?></span>
					</a>
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
			</div>
		<?php endif; ?>
	</div>
</div><?php /**PATH /var/www/d2l.sitepark.ua/resources/views/Frontend/templates/dashboard/statuses.blade.php ENDPATH**/ ?>