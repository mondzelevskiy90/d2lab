<?php $__env->startSection('css'); ?>
	<link rel="stylesheet" href="<?php echo e(mix('/css/account.css')); ?>">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('breadcrumbs'); ?>
	<?php echo $__env->make('Frontend.templates.breadcrumbs', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
	<h1><?php echo e($seo->name); ?></h1>
	<div class="panel panel-transparent panel-mob-short">
		<div class="panel-filters">
			<form method="GET" action="<?php echo e(route('branches')); ?>" id="filters_block" class="fx fac">		
				<?php echo e(csrf_field()); ?>

				<div class="form-filter">
					<input type="text" name="search" id="search" class="form-input" placeholder="<?php echo e(__('translations.search_name')); ?>" value="<?php if($search): ?><?php echo e($search); ?><?php endif; ?>" onchange="document.getElementById('filters_block').submit();">			
				</div>
				<div class="form-filter">					
					<a href="<?php echo e(route('branches')); ?>" class="btn btn-danger"><?php echo e(__('translations.clear')); ?></a>
				</div>
		</div>
		<?php if($data && count($data) < 51): ?> 
		<div class="panel-buttons">
			<a href="<?php echo e(route('branches.create')); ?>" class="btn btn-primary"><?php echo e(__('translations.create')); ?></a>
		</div>
		<?php endif; ?>		
	</div>
	<div class="fx fxb panel-body settings-form">
		<div class="panel panel-table">
			<?php if($data && count($data)): ?> 
				<div class="account-list-table account-list-table6">
					<div class="fx account-list-head">						
							<div class="account-list-cell"><?php echo e(__('translations.name')); ?></div>
							<div class="account-list-cell"><?php echo e(__('translations.address')); ?></div>
							<div class="account-list-cell"><?php echo e(__('translations.logo')); ?></div>
							<div class="account-list-cell"><?php echo e(__('translations.verified')); ?></div>
							<div class="account-list-cell"><?php echo e(__('translations.status')); ?></div>
							<div class="account-list-cell"></div>						
					</div>
					<div class="account-list-content">
						<?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<div class="fx fac account-list-line">
								<div class="account-list-cell"><?php echo e($item->name); ?></div>
								<div class="account-list-cell"><?php echo e(app('App\Services\Userdata')::getUserAddress($item)); ?></div>
								<div class="account-list-cell">
									<div class="bi company-logo" style="margin: auto; background-image: url(/storage/<?php echo e($item->logo); ?>)"></div>
								</div>
								<?php 
									if ($item->status == 1) $status = __('translations.status_active');
									if ($item->status == 2) $status = __('translations.status_disactive');
									if ($item->status == 3) $status = __('translations.status_blocked');
									if ($item->verified == 1) $verified = __('translations.verified');
									if ($item->verified == 2) $verified = __('translations.not_verified');
								?>
								<div class="account-list-cell"><?php echo e($verified); ?></div>
								<div class="account-list-cell"><?php echo e($status); ?></div>
								<div class="account-list-cell">
									<a href="<?php echo e(route('branches.edit', [$item->id, $requests])); ?>" class="btn btn-small"><?php echo e(__('translations.edit')); ?></a>
									<div data-href="<?php echo e(route('branches.delete', [$item->id, $requests])); ?>" class="btn btn-danger btn-small delete-element" data-msg="<?php echo e(__('translations.delete_alert')); ?>"><?php echo e(__('translations.delete')); ?></div>
								</div>
							</div>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					</div>
					<br>
					<div class="account-list-notice" style="font-size: 12px;">*** <?php echo e(__('translations.verified_notice')); ?></div>
				</div>
			<?php else: ?> 
				<p><?php echo sprintf(__('translations.no_braches'), route('branches.create')); ?></p>
			<?php endif; ?>
		</div>
	</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('Frontend/account_master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/d2l.sitepark.ua/resources/views/Frontend/pages/account/branches/index.blade.php ENDPATH**/ ?>