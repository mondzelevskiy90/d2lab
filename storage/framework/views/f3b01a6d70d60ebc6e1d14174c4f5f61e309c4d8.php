<?php $__env->startSection('css'); ?>
	<link rel="stylesheet" href="/css/select.css">
	<link rel="stylesheet" href="<?php echo e(mix('/css/account.css')); ?>">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('breadcrumbs'); ?>
	<?php echo $__env->make('Frontend.templates.breadcrumbs', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
	<h1><?php echo e($seo->name); ?></h1>
	<?php if($branches_list && count($branches_list)): ?>
	<div class="panel panel-transparent panel-mob-short">
		<div id="order_filters_block" class="panel-filters" style="display: none;">
			<form method="GET" action="<?php echo e(route('reviews')); ?>" id="filters_block" class="fx fac">		
				<?php echo e(csrf_field()); ?>	
				<div class="form-filter">
					<select name="branch" id="branch" class="form-input" onchange="document.getElementById('filters_block').submit();">	
						<option value="0"> -- <?php echo e(__('translations.branch')); ?> --</option>
						<?php $__currentLoopData = $branches_list; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $branch): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<?php if(Auth::user()->type == config('types.clinic')): ?>
								<?php if($branch->to_branch->status != 4): ?>
									<option value="<?php echo e($branch->to_branch->id); ?>" <?php if(isset($filters['branch']) && $filters['branch'] == $branch->to_branch->id): ?><?php echo e('selected="selected"'); ?><?php endif; ?>><?php echo e($branch->to_branch->name); ?></option>
								<?php endif; ?>
							<?php endif; ?>
							<?php if(Auth::user()->type == config('types.lab')): ?>
								<?php if($branch->status != 4): ?>
									<option value="<?php echo e($branch->id); ?>" <?php if(isset($filters['branch']) && $filters['branch'] == $branch->id): ?><?php echo e('selected="selected"'); ?><?php endif; ?>><?php echo e($branch->name); ?></option>
								<?php endif; ?>
							<?php endif; ?>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					</select>		
				</div>
			</form>
		</div>	
		<div class="fx fxb panel-buttons">
			<div id="show_filter_block" class="btn"><?php echo e(__('translations.filter_btn')); ?></div>
		</div>
	</div>
	<?php endif; ?>
	<div class="fx fxb panel-body">
		<div class="panel panel-table">
			<?php if($data && count($data)): ?> 
				<div class="account-list-table  account-list-table3 account-reviews-table">
					<div class="fx account-list-head">											
							<div class="account-list-cell"><?php echo e(__('translations.order')); ?> ID</div>
							<div class="account-list-cell"><?php echo e(__('translations.branch')); ?></div>
							<div class="account-list-cell"></div>						
					</div>
					<div class="account-list-content">
						<?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<div class="fx fac account-list-line">
								<div class="account-list-cell">
									<a target="_blank" href="<?php echo e(route('orders.edit', $item->order_id)); ?>"><span class="hide-desc"><?php echo e(__('translations.order')); ?></span> #<?php echo e($item->order_id); ?></a>
								</div>								
								<div class="account-list-cell">
									<?php echo e($item->branch->name); ?>

								</div>									
								<div class="account-list-cell">
									<a href="<?php echo e(route('reviews.edit', [$item->order_id, $requests])); ?>" class="btn btn-small"><?php echo e(__('translations.view')); ?></a>	
								</div>
							</div>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					</div>					
				</div>
				<div class="pagination-block"><?php echo e($data->appends(\Request::except('page'))->links()); ?></div>
			<?php else: ?> 
				<p>
					<?php echo e(__('translations.no_reviews')); ?>

					<?php if(Auth::user()->type == config('types.clinic')): ?>
					<?php echo e(__('translations.no_reviews_add')); ?>

					<?php endif; ?>
				</p>
			<?php endif; ?>
		</div>
	</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
	<script src="/js/select.js" defer></script>
	<script>
		document.addEventListener('DOMContentLoaded', function() {
			$('#branch').select2({
				minimumResultsForSearch: -1
			});

			$('#show_filter_block').on('click', function(){
				$(this).toggleClass('show-filter-close');
				$('#order_filters_block').slideToggle();
			});
		}, !1);		
	</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('Frontend/account_master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/dentist2lab.com/resources/views/Frontend/pages/account/reviews/index.blade.php ENDPATH**/ ?>