<?php $__env->startSection('css'); ?>
	<link rel="stylesheet" href="/css/select.css">
	<link rel="stylesheet" href="/css/daterangepicker.css">
	<?php if($data->status == 1): ?>
		<link rel="stylesheet" href="<?php echo e(mix('/css/checkout.css')); ?>">
	<?php endif; ?>
	<link rel="stylesheet" href="<?php echo e(mix('/css/account.css')); ?>">
	<link rel="stylesheet" href="/css/daterangepicker.css">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>		
	<form class="content-editing-form order-editing-form" method="POST" action="<?php echo e(route('orders.update', [$data->id, $requests])); ?>" enctype="multipart/form-data" id="order_editing_form">
		<div id="panel_top_buttons" class="fx fac fxb">
			<?php echo $__env->make('Frontend.templates.breadcrumbs', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
			<div class="panel-top-buttons button-submit order-buttons">
				<a class="btn btn-small btn-grey" href="<?php echo e(route('orders', [$requests])); ?>">&larr; <?php echo e(__('translations.back')); ?></a>
				<?php if($data->status != 1): ?>
					<div class="btn btn-small " onclick="printContent();"><?php echo e(__('translations.print_all_prescriptions')); ?></div>
				<?php endif; ?>
				<?php if($data->invoice && $data->invoice->status == 1): ?>
					<a class="btn btn-small  btn-primary" href="<?php echo e(route('invoices.edit', $data->invoice->id)); ?>"><?php echo e(__('translations.show_invoice')); ?></a>
				<?php endif; ?>
				<?php if(($data->status == 18 && $data->status_info->change_by == Auth::user()->type) || ($data->status_info->close_order != 1 && Auth::user()->type == $data->status_info->change_by) || ($data->status == 2 && Auth::user()->type == config('types.clinic')) || ($data->status_info->change_by == Auth::user()->type && $data->need_confirmation == 1)): ?>	
				 	<?php if(($data->status != 17) || ($data->status == 17 && $data->payment_status != 4)): ?>		
						<input type="submit" id="submit2" class="btn btn-small btn-danger" value="<?php echo e(__('translations.save')); ?>">
					<?php endif; ?>
				<?php endif; ?>
			</div>
		</div>
		<h1 class="tc"><?php echo e($seo->name); ?> #<?php echo e($data->id.($data->version && $data->version != 1 ? ' v.'.$data->version : '')); ?> (<?php echo e($data->status_info->name); ?><?php if($data->need_confirmation == 1): ?> - <?php echo e(__('translations.waiting_for_confirm')); ?><?php endif; ?>)</h1>
		<div class="fx fxb panel-body order-top-block">
			<div class="fx fxb panel panel-transparent">
				<div class="fx fac order-icons-bar">
					<div class="bi order-icons-icon order-status-icon" style="background-image: url(<?php echo e(url('/').'/storage/'.$data->status_info->icon); ?>)" title="<?php echo e($data->status_info->name); ?>"></div>
					<div class="bi order-icons-icon order-payment-icon" style="background-image: url(<?php echo e(url('/').'/storage/'.$data->payment_info->icon); ?>)" title="<?php echo e($data->payment_info->name); ?>"></div>
					<div class="bi account-table-progress account-table-progress<?php echo e($data->status_info->progress); ?>"><span></span></div>
					<?php
						$total_price = $data->total->advance_price;
						
						if($data->total->price != '0.00') $total_price = $data->total->price;
					?>
					<div class="fx fac order-total-block">
						<label for="order_total"><?php echo e(__('translations.order_total_price')); ?>:</label>
						<?php if($data->status == 2 && Auth::user()->type == config('types.lab')): ?>
							<input type="hidden" name="order_total" id="order_total_input" value="<?php echo e($total_price); ?>" required="required" onchange="isNaN(this.value) ? this.value = 0 : this.value = this.value;">							
						<?php endif; ?> 
						<?php echo e($currency->symbol); ?><span id="order_total"><?php echo e($total_price); ?></span> 
					</div>
				</div>	
				<div class="order-change-status">
				<?php 
					$available_statuses = array();

					if (isset($data->status_info->childrens)) {
						$available_statuses = explode(',', $data->status_info->childrens);
					}

					if ($data->status == 2 && Auth::user()->type == config('types.clinic')) $available_statuses = array(1);
				?>
				<?php if(($data->status == 18 && $data->status_info->change_by == Auth::user()->type) ||  ($data->status_info->change_by == Auth::user()->type && $data->need_confirmation != 1 && $data->status_info->close_order != 1) || ($data->status == 2 && Auth::user()->type == config('types.clinic')) || ($data->status_info->change_by == Auth::user()->type && $data->need_confirmation == 1)): ?> 				
					<?php if(($data->status != 17) || ($data->status == 17 && $data->payment_status != 4)): ?>		
						<div class="form-line" style="<?php if(count($available_statuses) == 0): ?><?php echo e('display:none;'); ?><?php endif; ?>">
							<?php if(Auth::user()->role_id == config('roles.tech')): ?>
								<select name="tech_status" id="tech_status" class="form-input form-input-required">
									<option value="0">-- <?php echo e(__('translations.select_status')); ?> --</option>
									<option value="1" <?php if($tech_status && $tech_status->status == 1): ?><?php echo e('selected="selected"'); ?><?php endif; ?>><?php echo e(__('translations.select_tech_status1')); ?></option>
									<option value="2" <?php if($tech_status && $tech_status->status == 2): ?><?php echo e('selected="selected"'); ?><?php endif; ?> ><?php echo e(__('translations.select_tech_status2')); ?></option>
								</select>
								<input type="hidden" name="status" id="status" value="<?php echo e($data->status); ?>">
							<?php else: ?> 
								<select name="status" id="status" class="form-input form-input-required" required="required">
									<option value="<?php echo e($data->status); ?>">-- <?php echo e(__('translations.select_status')); ?> --</option>
									<?php $__currentLoopData = $order_statuses; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $order_status): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
									    <?php 
									    	if (Auth::user()->role_id == config('roles.tech') && $order_status->id == 7) continue;
									    ?>
										<?php if(in_array($order_status->id, $available_statuses)): ?>
											<?php 
												if (Auth::user()->type == config('types.lab') && $order_status->id == 1) {
													$order_status_name = 'Dentist must edit';
												} else {
													$order_status_name = $order_status->name;
												}
											?>
											<option value="<?php echo e($order_status->id); ?>"><?php echo e($order_status_name); ?></option>
										<?php endif; ?>
									<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
								</select>
							<?php endif; ?>
						</div>	
					<?php endif; ?>			
				<?php endif; ?>
				<?php if($data->status_info->change_by != Auth::user()->type && $data->need_confirmation == 1): ?>
					<div>
						<span style="color: red;font-weight: bold;margin-bottom: 5px;display: block;"><?php echo e(__('translations.confirm_order_status_intro')); ?>:</span>
						<div class="btn btn-danger" onclick="document.getElementById('order_editing_form').submit();"><?php echo e(__('translations.confirm_order_status')); ?></div>
					</div>
				<?php endif; ?>
				</div>
			</div>
		</div>
		<div id="service_charge_msg"></div>
		<div id="order_delivery_params"></div>
		<div id="online_payment_form_block" style="display: none;">
			<div class="panel">
				<div class="panel-subhead"><?php echo e(__('translations.delivery_online_payment_head')); ?></div>
				<div id="online_payment_form"></div>
			</div>
		</div>
		<div id="order_review"></div>	
		<div id="order_newcomment"></div>		
		<div id="order_executor">
		<?php if($branch_staff && ($data->status == 9999999)): ?>
			<div class="fx fxb panel-body">
				<div class="panel">	
					<div class="fx fxb panel-block">
						<div class="form-line">	
							<label for="executor_id"><?php echo e(__('translations.executor')); ?></label>
							<select name="executor_id" id="executor_id" class="form-inut form-input-require">
								<?php if($branch_staff): ?>
									<option value="0">-- Select executor if needed! --</option>
									<?php $__currentLoopData = $branch_staff; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $branch_user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
										<option value="<?php echo e($branch_user['id']); ?>" <?php if($data->executor_id == $branch_user['id']): ?><?php echo e('selected="selected"'); ?><?php endif; ?>><?php echo e($branch_user['name']); ?></option>
									<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
								<?php endif; ?>
							</select>
						</div>
					</div>
				</div>
			</div>
		<?php endif; ?>
		</div>
		<?php if($data->status == 1): ?>
			<?php echo $__env->make('Frontend.templates.checkout_form', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
		<?php else: ?>
			<?php echo e(csrf_field()); ?>

			<div id="prescriptions_head" class="fx  fxb panel-body">
				<div class="fx fac fxb panel prescription-laboratory">
					<div class="prescription-laboratory-left">
						<img src="<?php echo e($url_qrcode); ?>">
					</div>
					<div class="fx prescription-laboratory-center">
						<div class="panel-subhead"><?php echo e($data->to_branch_name); ?></div>
						<?php if($branch): ?>
							<p>E-mail: <?php echo e($branch->email); ?></p>
							<p>Phone: <?php echo e($branch->phone); ?></p>
							<p>Address: <?php echo e($branch->zip_code .', '. $branch->address .', '. App\Services\Userdata::getLocality($branch->locality)); ?></p>
						<?php endif; ?>
					</div>
					<?php if($branch): ?>
						<div class="prescription-laboratory-right">
							<img src="<?php echo e(url(App\Services\Img::resizeImage($branch->logo, 100, 100))); ?>" alt="<?php echo e($data->to_branch_name); ?>">
						</div>
					<?php endif; ?>
				</div>
				<div class="panel prescriptions-head-print">					
					<div class="fx fxb panel-block">						
						<div class="panel-2quarter">							
							<div class="form-line form-line-text print-focus" style="font-size: 20px;">
								<p><strong style="background-color: rgb(242, 237, 162);padding: 5px;">Order ID: <?php echo e($data->id); ?>(D2L)</strong></p>
							</div>	
							<br>						
							<div class="form-line form-line-text not-print">
								<p>Payment status: <strong><?php echo e($data->payment_info->name); ?></strong></p>
							</div>
							<div class="form-line form-line-text">
								<p>Type: <strong><?php echo e($data->type_info->name); ?></strong></p>
							</div>
							<div class="form-line form-line-text">
								<?php
									$attached = '---';

									if ($data->attached) {
										$attached = $data->attached;
									}
								?>
								<p>Attached to order: <strong><?php echo e($attached); ?></strong></p>
							</div>	
							<div class="fx form-line form-line-text">
								<p>Ship date (Must be done on): 
									<strong id="ship_date_text">
										<?php if($data->ship_date_changed): ?>
											<?php echo e($data->ship_date); ?><span class="not-print"> --> <?php echo e($data->ship_date_changed); ?></span>
										<?php else: ?> 
											<?php echo e($data->ship_date); ?>

										<?php endif; ?>
									</strong>
								</p>
								<?php if(in_array($data->status, $hightlight_ship_statuses)): ?>
									<div class="btn ship-date-change not-print" style="position: relative;" onclick="showShipDatesList();">
										<?php echo e(__('translations.change')); ?>

										<input type="text" id="ship_date" class="form-input" value="" placeholder="<?php echo e(__('translations.ship_date')); ?>" maxlength="255" onchange="changeShipDate(<?php echo e($data->id); ?>);" style="width:1px; height: 1px; opacity:0;padding: 0;margin:0;">
									</div>
								<?php endif; ?>
							</div>							
							<?php if($data->delivery_service && count($data->delivery_service)): ?>
								
								<?php 
									foreach ($data->delivery_service as $item) {
										$delivey_data = $item->delivery_info->name;

										if (isset($item->delivery_invoice) && $item->delivery_invoice) {
											if ($item->delivery_info->tracking_link) {
												if (strpos($item->delivery_info->tracking_link, 'replace_number') !== false) {
													$delivey_data .= ' (<a target="blank" href="'.str_replace('replace_number', $item->delivery_invoice, $item->delivery_info->tracking_link).'">'.$item->delivery_invoice.'</a>)';
												} else {
													$delivey_data .= ' (<a target="blank" href="'.$item->delivery_info->tracking_link.'">'.$item->delivery_invoice.'</a>)';
												}
												
											} else {
												$delivey_data .= ' ('.$item->delivery_invoice.')';
											}
										}

										echo '<div class="fx form-line form-line-text">';
										echo '	<p>Delivery: '.$delivey_data.'</p>';
										echo '</div>';
										echo '<div class="fx form-line form-line-text">';
										echo '	<p>Sended on: '.$item->created_at.'</p>';
										echo '</div>';
										break;
									}
								?>														
							<?php endif; ?>	
							<div class="form-line form-line-text">
								<p>Ship address: <strong><?php echo e($data->ship_address); ?></strong></p>
							</div>					
						</div>
						<div class="panel-2quarter">
							<div class="form-line form-line-text"  style="font-size: 20px;">
								<p><strong style="background-color: rgb(242, 237, 162);padding: 5px;">Patient: <?php echo e($data->patient_name); ?> <?php echo e($data->patient_lastname); ?> <?php if(isset($data->patient_age)): ?><?php echo e($data->patient_age); ?> y.o.<?php endif; ?> <?php if(isset($data->patient_sex)): ?><?php echo e($data->patient_sex); ?> <?php endif; ?></strong></p>
							</div>
							<br>							
							<div class="form-line form-line-text">	
								<p>Created by: <strong><?php echo e($data->user_name); ?></strong></p>
							</div>
							<div class="form-line form-line-text">	
								<p>Phone: <strong><?php echo e($data->user_phone); ?></strong></p>
							</div>
							<div class="form-line form-line-text">	
								<p>E-mail: <strong><?php echo e($data->user_email); ?></strong></p>
							</div>
							<div class="form-line form-line-text">
								<p>From clinic: <strong><?php echo e($data->branch_name); ?></strong></p>
							</div>
							<div class="form-line form-line-text not-print">
								<p>To Lab: <strong><?php echo e($data->to_branch_name); ?></strong></p>
							</div>							
						</div>						
						<?php if($data->comment): ?>							
							<div class="form-line">	
								<br>								
								<p class="order-comment-line"><strong><?php echo e(__('translations.comment')); ?>:</strong> <?php echo $data->comment; ?></p>
							</div>
						<?php endif; ?>			
					</div>
				</div>
			</div>	
			<div id="all_prescriptions">		
				<?php if($prescriptions): ?>
					<?php $__currentLoopData = $prescriptions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $prescription): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
						<div id="prescription_name_<?php echo e($prescription['id']); ?>"></div>
						<?php
							$advanced_price = $prescription['advance_price'];
							$price = false;
							$price_warning_color = false;

							if($data->status == 2 && $prescription['price'] == '0.00') {
								$price = $advanced_price;
							} else if ($data->status > 2) {
								$price = $prescription['price'];
							}

							if ($prescription['price_changed_to'] != '0.00' && $price != $prescription['price_changed_to']) {
								$price = $prescription['price_changed_to'];
								
								if (Auth::user()->type == config('types.clinic')) {
									$price_warning_color = '#db2727';
								}
							}

							$price_class = 'price-normal';

							if (Auth::user()->type == config('types.clinic')) {
								if ($data->status > 2 && $price && (float)$price > (float)$advanced_price) $price_class = 'price-danger';
								if ($data->status > 2 && $price && (float)$price < (float)$advanced_price) $price_class = 'price-good';
							}

							if (Auth::user()->type == config('types.lab')) {
								if ($data->status > 2 && $price && (float)$price > (float)$advanced_price) $price_class = 'price-good';
								if ($data->status > 2 && $price && (float)$price < (float)$advanced_price) $price_class = 'price-danger';
							}
						?>	
						<div id="order_prescription-block<?php echo e($prescription_index); ?>" class="fx fxb panel-body order-prescription-Infoblock printable not-print" style="page-break-after: always;">
							<div id="order_prescription<?php echo e($prescription_index); ?>" class="fx fac fxb panel">
								<?php if($prescription['paid'] && $prescription['paid'] == 1): ?>
									<div class="fx fxc paid-background"><span><?php echo e(__('translations.paid')); ?></span></div>
								<?php endif; ?>
								<div class="panel-1quarter">
									<div class="order-prescription-imgBl">
										<span><strong><?php echo e(__('translations.service')); ?></strong> #<?php echo e($prescription_index); ?></span>
										<div id="order_prescription_img<?php echo e($prescription_index); ?>" class="order-prescription-imgSvg">
											<?php echo $prescription['numbering_system_svg']; ?>

										</div>
									</div>
								</div>							
								<div class="panel-3quarter">								
									<div class="fx fac fxb form-line prescription-service-name">
										<label class=""><?php echo e(__('translations.service_name')); ?>: <strong><?php echo e($prescription['service_name']); ?></strong><span class="not-print"><?php if($prescription['selecteds']): ?> (<?php echo e($prescription['selecteds']); ?>)<?php endif; ?></span></label>	
										<div class="not-print prescription-service-price">
											<?php if($data->payment_status != 4 && $data->invoice && $data->invoice->status == 1): ?>
												<a href="<?php echo e(route('invoices.edit', $data->invoice->id)); ?>">
											<?php endif; ?>
											<?php if($data->status < 3 || !$price || ($price && $data->status == 2)): ?>
												<span style="font-size: 12px;"><?php echo e(__('translations.advance_price')); ?>:</span> <?php echo e($currency->symbol); ?><?php echo e($advanced_price); ?>

											<?php elseif($price == $advanced_price): ?>
												<span class="price"><?php echo e($currency->symbol); ?><?php echo e($price); ?></span>
											<?php else: ?>
												<span class="old-price"><?php echo e($currency->symbol); ?><?php echo e($advanced_price); ?></span>
												<span class="price <?php echo e($price_class); ?>"><?php echo e($currency->symbol); ?><?php echo e($price); ?></span>
											<?php endif; ?>
											<?php if($data->payment_status != 4 && $data->invoice && $data->invoice->status == 1): ?>
												</a>
											<?php endif; ?>
											<?php if($price_warning_color && $data->invoice && $data->invoice->status == 1): ?>
												<a class="not-print" style="font-size: 14px;color:<?php echo e($price_warning_color); ?>;text-decoration: underline;" href="<?php echo e(route('invoices.edit', $data->invoice->id)); ?>">Confirm price!</a>
											<?php endif; ?>
											<?php if($data->payment_status != 4 && $data->invoice && $data->invoice->status == 1 && Auth::user()->type == config('types.lab')): ?>
												<a class="not-print" style="font-size: 14px;text-decoration: underline;" href="<?php echo e(route('invoices.edit', $data->invoice->id)); ?>">Change price!</a>
											<?php endif; ?>
										</div>
									</div>	
									<?php if($prescription['selected_option']): ?> 
										<?php
											$selected_option = 'Crown';

											if ($prescription['selected_option'] == 2) $selected_option = 'Bridge';
											if ($prescription['selected_option'] == 3) $selected_option = 'Full denture';
											if ($prescription['selected_option'] == 4) $selected_option = 'Partial denture (Flipper, Clammer, Partial prosthetics)';
											if ($prescription['selected_option'] == 5) $selected_option = 'Partial denture (Upper or Lower)';
										?>
										<div class="fx fac form-line">
											<label class="prescription-name">Selected service:</label>
											<div class="fx fac prescription-line-content">
											 	<?php echo e($selected_option); ?>

											</div>
										</div>
									<?php endif; ?>
									<?php if($prescription['selecteds']): ?>	
										<div class="fx fac form-line selecteds-for-print">
											<label class="prescription-name">Selected tooths:</label>
											<div class="fx fac prescription-line-content">
											 	<?php echo e($prescription['selecteds']); ?>

											</div>
										</div>	
									<?php endif; ?>									
									<?php if($prescription['fields']): ?>
										<?php $__currentLoopData = $prescription['fields']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $field): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
											<div class="fx fac form-line">
												<label for="prescription<?php echo e($field['id']); ?>" class="prescription-name"><?php echo e($field['name']); ?>:</label>
												<div class="fx fac prescription-line-content">
													<?php if($field['image']): ?> 
														<div class="prescription-image">
															<img src="<?php echo e(url('/').'/storage/'.$field['image']); ?>">
														</div>
													<?php endif; ?>
													<div class="fx prescription-inputs <?php if($field['image']): ?><?php echo e('prescription-inputs-half'); ?><?php endif; ?>">
														<?php if($field['field_type'] == 'checkbox' && $field['values']): ?>
															<?php $__currentLoopData = $field['values']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
																<?php
																	$checked = false;
																	$print = false;

																	if (isset($prescription['user_fields'][$field['id']])) {
																		$selected_values = explode(';', $prescription['user_fields'][$field['id']]['value']);

																		foreach($selected_values as $selected) {
																			if ($selected == $value['value']) {
																				$checked = true;
																				$print = true;
																				break;
																			}
																		}
																	}

																	if ($field['question'] && $field['question'] == 1&& !$checked) {
																		continue;
																	}
																?>
																<div class="fx fac form-checkbox <?php if(!$print): ?><?php echo e('not-print'); ?><?php endif; ?>">	
																	<?php if($value['image']): ?>
																		<img src="<?php echo e(url('/').'/storage/'.$value['image']); ?>">
																	<?php endif; ?>
																	
																	<input type="checkbox" id="prescription_<?php echo e($prescription_index); ?>_<?php echo e($field['id']); ?>_<?php echo e($value['id']); ?>" <?php if($checked): ?><?php echo e('checked="checked"'); ?><?php endif; ?> disabled="disabled">
																	<label for="prescription_<?php echo e($prescription_index); ?>_<?php echo e($field['id']); ?>_<?php echo e($value['id']); ?>"><?php echo e($value['value']); ?></label>
																</div>
															<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
														<?php elseif($field['field_type'] == 'radio' && $field['values']): ?>
															<?php $__currentLoopData = $field['values']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
																<?php
																	$checked = false;
																	$print = false;

																	if (isset($prescription['user_fields'][$field['id']]) && $prescription['user_fields'][$field['id']]['value'] == $value['value']) {
																			$checked = true;
																			$print = true;	
																	}
																?>
																<div class="fx fac form-checkbox <?php if(!$print): ?><?php echo e('not-print'); ?><?php endif; ?>"">	
																	<?php if($value['image']): ?>
																		<img src="<?php echo e(url('/').'/storage/'.$value['image']); ?>">
																	<?php endif; ?>						
																	<input type="radio" id="prescription_<?php echo e($prescription_index); ?>_<?php echo e($field['id']); ?>_<?php echo e($value['id']); ?>" value="<?php echo e($value['value']); ?>" <?php if($checked): ?><?php echo e('checked="checked"'); ?><?php endif; ?> disabled="disabled">
																	<label for="prescription_<?php echo e($prescription_index); ?>_<?php echo e($field['id']); ?>_<?php echo e($value['id']); ?>"><?php echo e($value['value']); ?></label>
																</div>
															<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
														<?php else: ?> 
															<?php if(isset($prescription['user_fields'][$field['id']])): ?>
																<?php echo e($prescription['user_fields'][$field['id']]['value']); ?>

															<?php else: ?>
																---
															<?php endif; ?>
														<?php endif; ?>											
													</div>
												</div>
											</div>
										<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
									<?php endif; ?>
									<?php if($prescription['comment']): ?>
									<div class="form-line">	
										<p><strong><?php echo e(__('translations.comment')); ?>:</strong> <?php echo e($prescription['comment']); ?></p>
									</div>
									<?php endif; ?>
								</div>
								<div class="form-line print-btn-block not-print">	
									<?php if($prescription['version'] > 1): ?>
										<div class="btn btn-notprimary" onclick="showPrescriptionVersion(<?php echo e((int)$prescription['version'] - 1); ?>, <?php echo e($prescription['id']); ?>)" style="margin-bottom: 5px;"><?php echo e(__('translations.show_priscription_version')); ?></div>
									<?php endif; ?>								
									<?php if($data->status != 17 && $data->status != 18 && Auth::user()->type == config('types.clinic')): ?>
										<div class="btn btn-danger" onclick="changePrescriptionSelecteds(<?php echo e($prescription['id']); ?>)" style="margin-bottom: 5px;"><?php echo e(__('translations.change_priscription_content')); ?></div>
									<?php endif; ?>
									<?php if($data->invoice): ?>
										<div class="btn btn-danger prescription-remake-btn" onclick="createPrescriptionRemake(<?php echo e($data->invoice->id); ?>, <?php echo e($data->id); ?>, <?php echo e($prescription['id']); ?>);" style="margin-bottom: 5px;"><?php echo e(__('translations.remake')); ?></div>
									<?php endif; ?>
									<div class="btn" onclick="printContent('order_prescription-block<?php echo e($prescription_index); ?>');" style="margin-bottom: 5px;"><?php echo e(__('translations.print_one_prescription')); ?></div>
								</div>							
							</div>
						</div>
						<?php 
							$prescription_index++;
						?>
					<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				<?php endif; ?>	
			</div>
			<?php if($data->status != 1): ?>
				<div class="fx fxb panel-body">
					<div class="panel">	
						<div class="fx fxb panel-block">
							<?php if($data->status_info->delivery_params && $data->status_info->delivery_params == 1): ?>
							<div class="panel-subhead"><?php echo e(__('translations.delivery_list')); ?></div>
							<div class="fx fxb fac delivery-form-block">							
								<div class="form-line delivery-form-line">
									<label for="delivery_id"><?php echo e(__('translations.delivery_method')); ?></label>
									<select id="delivery_id" class="form-input">
										<?php $__currentLoopData = $deliveries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $delivery): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
											<option value="<?php echo e($delivery->delivery->id); ?>"><?php echo e($delivery->delivery->name); ?></option>
										<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
									</select>
								</div>
								<div class="form-line delivery-form-line">
									<label for="delivery_params_tracker"><?php echo e(__('translations.delivery_tracker')); ?></label>
									<input type="text" id="delivery_params_tracker" class="form-input" value="" placeholder="<?php echo e(__('translations.delivery_tracker')); ?>">
								</div>
								<div class="form-line delivery-form-line" style="margin-bottom: 0;">
									<br>
									<div class="btn btn-small" onclick="proccessDeliveryService('add', false);"><?php echo e(__('translations.add_service')); ?></div>
								</div>
							</div>
							<?php endif; ?>
							<div id="delivery_history">
								<?php if($data->delivery_service && count($data->delivery_service)): ?>							
									<?php $__currentLoopData = $data->delivery_service; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $delivery_service): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
										<?php
											$direction = 'right';
											$added_by = config('types.clinic');

											if ($data->to_branch_id == $delivery_service->from_branch) {
												$direction = 'left';
												$added_by = config('types.lab');
											}  

											$delivey_data = $delivery_service->delivery_info->name;

											if (isset($delivery_service->delivery_invoice) && $delivery_service->delivery_invoice) {
												if ($delivery_service->delivery_info->tracking_link) {
													if (strpos($delivery_service->delivery_info->tracking_link, 'replace_number') !== false) {
														$delivey_data .= ' (<a target="blank" href="'.str_replace('replace_number', $delivery_service->delivery_invoice, $delivery_service->delivery_info->tracking_link).'">'.$delivery_service->delivery_invoice.'</a>)';
													} else {
														$delivey_data .= ' (<a target="blank" href="'.$delivery_service->delivery_info->tracking_link.'">'.$delivery_service->delivery_invoice.'</a>)';
													}
													
												} else {
													$delivey_data .= ' ('.$delivery_service->delivery_invoice.')';
												}
											}
										?>
										<div id="delivery_history_line<?php echo e($delivery_service->id); ?>" class="fx fxb delivery-history-line">
											<div class="delivery-history-clinic"><?php echo e(__('translations.clinic')); ?> <?php if($direction == 'right'): ?> (<?php echo e($delivery_service->created_at); ?>)<?php endif; ?></div>
											<div class="fx fac delivery-history-service">
												<div class="bi delivery-history-arrow delivery-history-<?php echo e($direction); ?>"></div>
												<div class="delivery-history-name">
													<?php echo $delivey_data; ?>

												</div>
												<div class="bi delivery-history-arrow delivery-history-<?php echo e($direction); ?>"></div>
											</div>
											<div class="delivery-history-lab"><?php echo e(__('translations.lab')); ?> <?php if($direction == 'left'): ?> (<?php echo e($delivery_service->created_at); ?>)<?php endif; ?></div>
											<div id="delivery_history_btn<?php echo e($delivery_service->id); ?>" class="delivery-history-delete">
												<?php if($delivery_service->received == 1): ?>
													Received
												<?php elseif($added_by == Auth::user()->type && $delivery_service->delivery_info->id != 12): ?> 
												<div class="btn btn-danger btn-small" onclick="proccessDeliveryService('delete', <?php echo e($delivery_service->id); ?>);"><?php echo e(__('translations.delete')); ?></div>
												<?php elseif($added_by != Auth::user()->type && $delivery_service->delivery_info->id != 12): ?>
												<div class="btn btn-danger btn-small" onclick="proccessDeliveryService('received', <?php echo e($delivery_service->id); ?>);">Received</div>
												<?php endif; ?>
											</div>
										</div>
									<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>							
								<?php endif; ?>
							</div>
						</div>
					</div>
				</div>			
			<?php endif; ?>
			<div id="attached_files_block">				
				<div class="fx fxb panel-body">
					<div class="panel">
						<div class="panel-head panel-head-collapse"><?php echo e(__('translations.order_files')); ?></div>
						<div class="fx fxb panel-block">
							<div class="form-line">							
								<div id="order_files" class="fx">
									<?php $file_index = 9999; ?>
									<?php if($data->files && count($data->files)): ?>
										<?php $__currentLoopData = $data->files; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $file): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
											<div id="order_file_line<?php echo e($file_index); ?>" class="fx fxb order-file-icon">
												<?php 
													$extension = pathinfo(public_path().'/storage/'.$file->file)['extension'];
												?>
												<div id="order_file_name<?php echo e($file_index); ?>" class="fx fxc order-file-name">
													<a href="<?php echo e(url('/').'/storage/'.$file->file); ?>" target="_blank" class="fx fxc">
														<?php if(in_array($extension, ['png', 'PNG', 'jpg', 'jpeg', 'JPEG', 'JPG'])): ?>
																<img src="<?php echo e('/storage/'.$file->file); ?>" width="100%" height="auto" />
														<?php else: ?>
															<span>.<?php echo e($extension); ?></span>
														<?php endif; ?>
													</a>
													<div class="order-file-remove" onclick="removeFileFromCheckoutForm('<?php echo e($file_index); ?>', <?php echo e($file->id); ?>)" title="<?php echo e(__('translations.delete')); ?>">
														<div></div>
													</div>
													<input type="hidden" id="order_files_save<?php echo e($file_index); ?>" name="order_files[<?php echo e($file_index); ?>]" value="<?php echo e($file->file); ?>">
												</div>											
											</div>
											<?php $file_index++; ?>
										<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>	
									<?php endif; ?>								
								</div>
								<?php if(Auth::user()->type == config('types.clinic')): ?>
									<br>
									<div id="order_files_add" class="btn btn-small" onclick="addFileToCheckoutForm()" style="max-width:120px;"><?php echo e(__('translations.add_file')); ?></div>
								<?php endif; ?>
							</div>	
						</div>
					</div>
				</div>
			</div>	
			<?php if($data->status_info->can_review && $data->status_info->can_review == 1): ?> 
				<?php if($data->reviews && count($data->reviews)): ?>
					<div class="fx fxb panel-body">
						<div class="panel">
							<div class="panel-head panel-head-collapse"><?php echo e(__('translations.reviews')); ?></div>
							<div class="panel-block">
								<?php echo $__env->make('Frontend.templates.review_block', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
							</div>
						</div>
					</div>
				<?php elseif(Auth::user()->type == config('types.clinic')): ?>
					<div class="fx fxb panel-body">
						<div class="panel">	
							<div class="fx fxb panel-block">
								<div class="form-line">	
									<label for="review"><?php echo e(__('translations.order_review')); ?></label>
									<div class="fx review-rating-stars">
										<input type="radio" name="review[rating]" id="review_rating_1" class="review-rating-radio review-rating-radioAct" value="1" onchange="showReviewStars(1)" title="1">
										<input type="radio" name="review[rating]" id="review_rating_2" class="review-rating-radio review-rating-radioAct" value="2" onchange="showReviewStars(2)" title="2">
										<input type="radio" name="review[rating]" id="review_rating_3" class="review-rating-radio review-rating-radioAct" value="3" onchange="showReviewStars(3)" title="3">
										<input type="radio" name="review[rating]" id="review_rating_4" class="review-rating-radio review-rating-radioAct" value="4" onchange="showReviewStars(4)" title="4">
										<input type="radio" name="review[rating]" id="review_rating_5" class="review-rating-radio review-rating-radioAct" value="5" onchange="showReviewStars(5)" title="5" checked>
									</div>
									<textarea name="review[review]" id="review" class="form-inut form-input-required" value="" placeholder="<?php echo e(__('translations.review')); ?>" maxlength="1000"></textarea>
								</div>
								<div class="form-line">
									<div id="add_first_review" class="btn btn-small" onclick="saveOrderFirstReview();"><?php echo e(__('translations.create')); ?></div>
								</div>
							</div>
						</div>
					</div>
				<?php endif; ?>
			<?php endif; ?>	
			<?php if($data->status != 1): ?>
				<div class="fx fxb panel-body">
					<div class="panel">
						<div class="panel-head panel-head-collapse"><?php echo e(__('translations.chat')); ?></div>
						<div class="panel-block">
							<?php echo $__env->make('Frontend.templates.chat', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
						</div>
					</div>
				</div>
			<?php endif; ?>		
			<div class="fx fxb panel-body">
				<div class="panel">
					<div class="panel-head panel-head-collapse panel-head-collapsed"><?php echo e(__('translations.order_history')); ?></div>
					<div class="panel-block panel-block-collapsed">
						<?php if($data->history && count($data->history)): ?> 
							<?php $__currentLoopData = $data->history; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $history): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
								<p><?php echo e($history->created_at); ?> - <?php echo e($history->comment); ?> (<?php echo e($history->user); ?>)</p>
							<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
						<?php endif; ?>
					</div>
				</div>
			</div>
		<?php endif; ?>		
	</form>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
	<script src="/js/select.js" defer></script>	
	<script src="/js/moment.min.js" defer></script>
	<script src="/js/daterangepicker.js" defer></script>
	<script>
		document.addEventListener('DOMContentLoaded', function(){			
			$('#status, #executor_id, #delivery_id, #tech_status').select2({
				minimumResultsForSearch: -1
			});

			$('#status').on('change', function(){
				var status = $(this).val();

				$.ajax({
					url: '/ajax/getData',
					type: 'post',
		            dataType: 'json',
		            headers: {
		                'X-CSRF-TOKEN': '<?php echo e(csrf_token()); ?>'
		            },
				    data: {action:'getOrderStatusActions', status: status},
				    success: function (data) {
				    	if (data) {
				    		var text = '';
				    		var class_text = 'success';

				    		if (data.need_comment && data.need_comment == 1) {
				    			class_text = 'warning';

				    			var html = '';

				    			html += '<div class="fx fxb panel-body">';
								html += '<div class="panel">';	
								html += '<div class="fx fxb panel-block">';
								html += '<div class="form-line">';	
								html += '<label for="comment"><?php echo e(__('translations.comment')); ?></label>';
								html += '<textarea name="comment" id="comment" class="form-inut form-input-required" value="" placeholder="<?php echo e(__('translations.comment')); ?>" maxlength="1000" required="required"></textarea>';
								html += '</div>';
								html += '</div>';
								html += '</div>';
								html += '</div>';

				    			$('#order_newcomment').html(html);

				    			text += '<p><?php echo e(__('translations.status_need_comment')); ?></p>';
				    		} else {
				    			$('#order_newcomment').html('');
				    		}

				    		if (data.confirm_by && data.confirm_by != 0) {
				    			class_text = 'warning';

				    			var confirm_by = '';

				    			if (data.confirm_by == <?php echo e(config('types.clinic')); ?>) confirm_by = '<?php echo e(__('translations.clinic')); ?>';
				    			if (data.confirm_by == <?php echo e(config('types.lab')); ?>) confirm_by = '<?php echo e(__('translations.lab')); ?>';

				    			text += '<p><?php echo e(__('translations.status_need_confirm')); ?> '+confirm_by+'!</p>';
				    		}

				    		if (data.change_by && data.change_by != <?php echo e(Auth::user()->type); ?>) {
				    			class_text = 'warning';

				    			var change_by = '';

				    			if (data.change_by == <?php echo e(config('types.clinic')); ?>) change_by = '<?php echo e(__('translations.clinic')); ?>';
				    			if (data.change_by == <?php echo e(config('types.lab')); ?>) change_by = '<?php echo e(__('translations.lab')); ?>';

				    			text += '<p><?php echo e(__('translations.status_changed_by')); ?> '+change_by+'!</p>';

				    			if (data.id == 1) text += '<p><?php echo e(__('translations.status_hide_to_lab')); ?></p>';
				    		}

				    		if (data.can_review && data.can_review == 1) {
								text += '<p><?php echo e(__('translations.status_need_review')); ?></p>';

								var html = '';

								html += '<div class="fx fxb panel-body">';
								html += '<div class="panel">';	
								html += '<div class="fx fxb panel-block">';
								html += '<div class="form-line">';	
								html += '<label for="review"><?php echo e(__('translations.order_review')); ?></label>';
								html += '<div class="fx review-rating-stars">';
								html += '<input type="radio" name="review[rating]" id="review_rating_1" class="review-rating-radio" value="1" onchange="showReviewStars(1)" title="1">';
								html += '<input type="radio" name="review[rating]" id="review_rating_2" class="review-rating-radio" value="2" onchange="showReviewStars(2)" title="2">';
								html += '<input type="radio" name="review[rating]" id="review_rating_3" class="review-rating-radio" value="3" onchange="showReviewStars(3)" title="3">';
								html += '<input type="radio" name="review[rating]" id="review_rating_4" class="review-rating-radio" value="4" onchange="showReviewStars(4)" title="4">';
								html += '<input type="radio" name="review[rating]" id="review_rating_5" class="review-rating-radio review-rating-radioAct" value="5" onchange="showReviewStars(5)" title="5" checked>';
								html += '</div>';
								html += '<textarea name="review[review]" id="review" class="form-inut form-input-required" value="" placeholder="<?php echo e(__('translations.review')); ?>" maxlength="1000"></textarea>';
								html += '</div>';
								html += '</div>';
								html += '</div>';
								html += '</div>';

								$('#order_review').html(html);

								showReviewStars(5);
				    		} else {
				    			$('#order_review').html('');
				    		}

							<?php if($data->status == 11): ?>
				    		if (data.need_executor && data.need_executor == 1) {
				    			class_text = 'warning';

				    			var html = '';

				    			html += '<div class="fx fxb panel-body">';
								html += '<div class="panel">';	
								html += '<div class="fx fxb panel-block">';
								html += '<div class="form-line">';	
								html += '<label for="executor_id"><?php echo e(__('translations.executor')); ?></label>';
								html += '<select name="executor_id" id="executor_id" class="form-inut"  required="required">';
								<?php if($branch_staff): ?>
									<?php $__currentLoopData = $branch_staff; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $branch_user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
										html += '<option value="<?php echo e($branch_user['id']); ?>"><?php echo e($branch_user['name']); ?></option>';
									<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
								<?php endif; ?>
								html += '</select>';
								html += '</div>';
								html += '</div>';
								html += '</div>';
								html += '</div>';

				    			text += '<p><?php echo e(__('translations.status_need_executor')); ?></p>';

				    			$('#order_executor').html(html);

				    			$('#executor_id').select2({
									minimumResultsForSearch: -1
								});
				    		}
				    		<?php endif; ?>				    	

				    		if (text != '') {
				    			showPopup('<div class="'+class_text+'">'+text+'</div>');
				    		}
				    	}
				    }
				});
			});

		}, !1);
	</script>
	<?php echo $__env->make('Frontend.templates.review_functions', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
	<?php if($data->status == 1): ?>
		<script src="/js/moment.min.js" defer></script>
		<script src="/js/daterangepicker.js" defer></script>
		<?php echo $__env->make('Frontend.templates.checkout_scripts', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
	<?php else: ?> 
	<script src="/js/masked.js" defer></script>
	<script>
		var prescription_colors = {
			0: 'red',
			1: '#ffbab0',
			2: '#F2EDA2',
			3: '#BBDCEF',
			4: '#CAE8CE',
			5: '#F7C69C',
			6: '#f3d9da',
			7: '#ffb6ff',
			8: '#b58585',
			9: '#d2d222',
			10: '#DADADA'
		};

		document.addEventListener('DOMContentLoaded', function(){
			detectActivity(180);
			
			$.ajax({
				url: '/ajax/getData',
				type: 'post',
	            dataType: 'json',
	            headers: {
	                'X-CSRF-TOKEN': '<?php echo e(csrf_token()); ?>'
	            },
			    data: {action:'getOrderPrescriptionSelecteds', order_id: '<?php echo e($data->id); ?>'},
			    success: function (data) {
			    	if (data) {			    		
						var empties = [],
							not_empties = [];

			    		for(i = 0; i < data.length; i++) {			    			
			    			var indx = i <= 10 ? i : 1;
							var color = prescription_colors[indx];
							var lined = [];

			    			if(data[i]) {
								var show = false;
								var end = false;	

			    				for(k = 1; k <= 32; k++) {
			    					if (data[i][k]) {
			    						tooth_data = data[i][k].split('_');

			    						if (tooth_data[1]) {
			    							if (tooth_data[1] == 5 || tooth_data[1] == 6 || tooth_data[1] == 7) empties[k] = k;

			    							

			    							if (tooth_data[1] == 5) {
			    								$(document).find('#order_prescription_img'+i+' #tooth_next_'+k).css('display', 'block');
			    								not_empties[k] = k;
			    							}
											if (tooth_data[1] == 6) {
												$(document).find('#order_prescription_img'+i+' #tooth_prev_'+k).css('display', 'block');

												not_empties[k] = k;
											}

			    							if (tooth_data[1] == 3) {
												$(document).find('#order_prescription_img'+i+' #tooth_point_'+k).css('display', 'block');
												$(document).find('#order_prescription_img'+i+' #tooth_next_'+k).css('display', 'block');
												show = true;
												end = false;				
											}

											if (tooth_data[1] == 4) {
												$(document).find('#order_prescription_img'+i+' #tooth_prev_'+k).css('display', 'block');
												$(document).find('#order_prescription_img'+i+' #tooth_point_'+k).css('display', 'block');
												show = false;
												end = true;
											}	

			    							$(document).find('#order_prescription_img'+i+' #tooth_body_'+k).css('fill', color);
			    						}
			    					}

			    					if (show) lined[k] = k;
			    				}

			    				if (end) {
									for (l = 1; l <= 32; l++) {	
										if (lined[l]) {
											if (!empties[l]) $(document).find('#order_prescription_img'+i+' #tooth_body_'+l).css('fill', color);
											if (l != 16 && l != 32) $(document).find('#order_prescription_img'+i+' #tooth_next_'+l).css('display', 'block');
										};
									}
								}
			    			}
			    		}

			    		for(j = 0; j < empties.length; j++) {
			    			if (!not_empties[j]) $(document).find('.order-prescription-imgSvg #tooth_body_'+empties[j]).css('fill', '#fff');

		    				$(document).find('.order-prescription-imgSvg #tooth_empty_'+empties[j]).css('display', 'block');
		    			}
			    	}
			    }
			});

			$('.prescription-price-input').on('keypress', function(e){
				if (e.keyCode == 13) {
					return false;
				}
			});

		}, !1);

		function printContent(id) {
			var new_window =window.open();
			var head = document.getElementById('prescriptions_head').innerHTML;
			var attached = document.getElementById('attached_files_block').innerHTML;
			var html = '<link rel="stylesheet" href="<?php echo e(mix('/css/styles.css')); ?>">';

			html += '<link rel="stylesheet" href="<?php echo e(mix('/css/account.css')); ?>">';
			html += '<link rel="stylesheet" href="<?php echo e(mix('/css/print_order.css')); ?>">';

			var printable_blocks = document.querySelectorAll('.printable');

			printable_blocks.forEach(function(el){
				if (id) {
					el.classList.add('not-print');
				} else {
					el.classList.remove('not-print');
				}
			});

			if (id) document.getElementById(id).classList.remove('not-print');

			var prescriptions = document.querySelectorAll('.order-prescription-Infoblock');

			prescriptions.forEach(function(el){				
				if (!el.classList.contains('not-print')) {
					html += '<div class="print-content" style="page-break-after: always;position:relative">';
					html += '<div class="fx fxb print-source"><span><?php echo e(config('app.url')); ?></span><span><?php echo e(date('m-d-Y')); ?></span></div>';
					html += head;
					html += '<div class="fx fxb panel-body order-prescription-Infoblock">' + el.innerHTML;
					html += '</div>';
					html += '<div class="order-prescription-attached">'+attached+'</div>';
					html += '</div>';
				}
			});

			new_window.document.write(html);

			setTimeout(function(){
				new_window.print();
				new_window.close();
			}, 1000);
			
		}

		function showShipDatesList() {
			$(document).find('#ship_date').daterangepicker({
				singleDatePicker: true,
				showDropdowns: true,
				minYear: <?php echo e(date('Y')); ?>,
				maxYear: <?php echo e(date('Y')); ?> + 1,
				locale: {
			        format: 'MM-DD-YYYY'
			    },
			});

			$(document).find('#ship_date').focus();			
		}

		function changeShipDate(id) {
			var date = $(document).find('#ship_date').val();

			if (date != '<?php echo e(date('m-d-Y')); ?>') {
				$.ajax({
					url: '/ajax/getData',
					type: 'post',
		            dataType: 'json',
		            headers: {
		                'X-CSRF-TOKEN': '<?php echo e(csrf_token()); ?>'
		            },
				    data: {action:'changeShipDate', id: id, date: date},
				    success: function (data) {
				    	$('#ship_date_text').html(data);

				    	confirmShipDate(id, 2);
				    }
				});
			}
		}

		function confirmShipDate(id, change) {
			$.ajax({
				url: '/ajax/getData',
				type: 'post',
	            dataType: 'json',
	            headers: {
	                'X-CSRF-TOKEN': '<?php echo e(csrf_token()); ?>'
	            },
			    data: {action:'confirmShipDate', id: id, change: change},
			    success: function (data) {
			    	$('#ship_date_text').html(data);
			    	$('.ship-date-confirm').remove();
			    }
			});
		}

		function addAdditionalDeliveryFields() {
			var type = $(document).find('#delivery_id').val();
			var html = '';

			if (type != 1 && type != 2) {
				html += '<div class="form-line">';
				html += '<label for="bill_to"><?php echo e(__('translations.delivery_bill_to')); ?></label>';
				html += '<select name="delivery_params[bill_to]" id="bill_to" class="form-input form-input-required" required="required" onchange="addAdditionalDeliveryPayment()">';
				html += '<option value="<?php echo e(config('types.lab')); ?>"><?php echo e(__('translations.delivery_bill_receiver')); ?></option>';
				html += '<option value="<?php echo e(config('types.clinic')); ?>"><?php echo e(__('translations.delivery_bill_shipper')); ?></option>';
				html += '</select>';
				html += '</div>';				
			}

			if (type == 8) {
				html += '<div class="form-line">';
				html += '<label for="delivery_service_type"><?php echo e(__('translations.delivery_service_type_ups')); ?></label>';
				html += '<select name="delivery_params[delivery_service_type]" id="delivery_service_type" class="form-input form-input-required" required="required">';				
				html += '<option value="03">Ground</option>';
				html += '<option value="01">Next Day Air</option>';
				html += '<option value="02">2nd Day Air</option>';
				html += '</select>';
				html += '</div>';
			}

			$(document).find('#add_delivery_fields').html(html);

			$('#delivery_service_type, #bill_to').select2({
				minimumResultsForSearch: -1
			});
		}

		function addAdditionalDeliveryPayment() {
			var bill_to = $(document).find('#bill_to').val();

			if (bill_to == <?php echo e(Auth::user()->type); ?>) {
				showOnlinePaymentForm(2, 'delivery_');
				$('#online_payment_form_block').css('display', 'block');
			} else {
				showOnlinePaymentForm(0, 'delivery_');
				$('#online_payment_form_block').css('display', 'none');
			}
		}

		<?php echo $__env->make('Frontend.templates.online_payment_form_script', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

		function proccessDeliveryService(method, delivery_service_id) {	
			var delivery_id = false;
			var delivery_invoice = false;
			var type = <?php echo e(Auth::user()->type); ?>;

			if (method == 'add') {
				delivery_id = $('#delivery_id').val();
				delivery_invoice = $('#delivery_params_tracker').val();
			}	

			if (confirm('<?php echo e(__('translations.add_delivery_question')); ?>')) {
				$.ajax({
					url: '/ajax/getData',
					type: 'post',
		            dataType: 'json',
		            headers: {
		                'X-CSRF-TOKEN': '<?php echo e(csrf_token()); ?>'
		            },
				    data: {action:'proccessDeliveryService', id: <?php echo e($data->id); ?>, method: method, type: type, delivery_id: delivery_id, delivery_invoice: delivery_invoice, delivery_service_id: delivery_service_id},
				    success: function (data) {
				    	if (data) {
				    		if (method == 'delete') {
				    			$(document).find('#delivery_history_line'+delivery_service_id).remove();
				    		}

				    		if (method == 'received') {
				    			$(document).find('#delivery_history_btn'+delivery_service_id).html('Received');
				    		}

				    		if (method == 'add') {
				    			var html = '';

				    			if (data['id']) {
				    				var direction = 'right';

									if (<?php echo e($data->to_branch_id); ?> == data['from_branch']) {
										direction = 'left';
									}

									var delivery_data = data['delivery_info']['name'];

									if (data['delivery_invoice']) {
										if (data['delivery_info']['tracking_link']) {
											delivery_data += ' (<a target="blank" href="'+(data['delivery_info']['tracking_link'].replace('replace_number', data['delivery_invoice']))+'">'+data['delivery_invoice']+'</a>)';
										} else {
											delivery_data += ' ('+data['delivery_invoice']+')';
										}
									}

				    				html += '<div id="delivery_history_line'+data['id']+'" class="fx fxb delivery-history-line">';
									html += '<div class="delivery-history-clinic"><?php echo e(__('translations.clinic')); ?></div>';
									html += '<div class="fx fac delivery-history-service">';
									html += '<div class="bi delivery-history-arrow delivery-history-'+direction+'"></div>';
									html += '<div class="delivery-history-name">'+delivery_data+'</div>';
									html += '<div class="bi delivery-history-arrow delivery-history-'+direction+'"></div>';
									html += '</div>';
									html += '<div class="delivery-history-lab"><?php echo e(__('translations.lab')); ?></div>';
									html += '<div class="delivery-history-delete">';
									html += '<div class="btn btn-danger btn-small" onclick="proccessDeliveryService(\'delete\', '+data['id']+');"><?php echo e(__('translations.delete')); ?></div>';
									html += '</div>';
									html += '</div>';
				    			}

				    			$('#delivery_history').prepend(html);

				    			showPopup('<p><?php echo e(__('translations.delivery_method_saved')); ?></p>');
				    		}
				    	}
				    }
				});
			}
		}
		
		<?php if($data->status_info->can_review && $data->status_info->can_review == 1 && Auth::user()->type == config('types.clinic')): ?>
			function saveOrderFirstReview() {
				var review = $('#review').val();
				var rating = $('.review-rating-radio:checked').val();
				var to_branch_id = '<?php echo e($data->to_branch_id); ?>';

				if (review && review != '') {
					$.ajax({
						url: '/ajax/getData',
						type: 'post',
			            dataType: 'json',
			            headers: {
			                'X-CSRF-TOKEN': '<?php echo e(csrf_token()); ?>'
			            },
					    data: {action:'saveOrderReview', id: <?php echo e($data->id); ?>, review: review, rating: rating, to_branch_id: to_branch_id},
					    success: function (data) {
					    	if (data) window.location.reload();
					    }
					});
				}
			}
		<?php endif; ?>

		function changePrescriptionSelecteds(id) {
			if (!id) return false;

			$.ajax({
				url: '/ajax/getData',
				type: 'post',
	            dataType: 'json',
	            headers: {
	                'X-CSRF-TOKEN': '<?php echo e(csrf_token()); ?>'
	            },
			    data: {action:'getOrderPrescriptionData', id: id},
			    success: function (data) {
			    	if (data) {
			    		//console.log(data);

			    		var html = '';

			    		html += '<link href="/css/checkout.css" rel="stylesheet">';
			    		html += '<form action="/ajax/getData" method="POST" id="edit_prescription_fields_form">';
			    		html += '   <input type="hidden" name="action" value="saveOrderPrescriptionData">';
			    		html += '   <input type="hidden" name="order_id" value="'+data.order_id+'">';
			    		html += '   <input type="hidden" name="prescription_id" value="'+data.id+'">'
			    		html += '   <div id="order_prescriptions">';

			    		if (data.fields) {
							for (index in data.fields) {
								html += '<div class="form-line" style="margin-bottom: 10px;">';	
								html += '<div class="prescription-line-head" style="font-size: 15px;">';
								html += '<label for="prescription'+data.fields[index]['id']+'" class="prescription-name">'+data.fields[index]['name']+(data.fields[index]['help'] ? '<span class="prescription-line-help" onclick="showFieldHelp('+data.fields[index]['id']+')">?</span>' : '')+'</label>';

								if (data.fields[index]['description']) {
									html += '<p class="prescription-desc">'+data.fields[index]['description']+'</p>';
								}

								html += '</div>';
								html += '<div class="fx fac prescription-line-content">';

								if (data.fields[index]['question'] && data.fields[index]['question'] == 1) {
									html += '<div class="prescription-line-question">';
									html += '<select id="select_prescription_edit_'+id+'_'+data.fields[index]['id']+'" class="form-input select-select2-select" onchange="showPrescriptionFieldContent(\'_edit_'+id+'_'+data.fields[index]['id']+'\');">';
									html += '<option value="no" default><?php echo e(__('translations.no')); ?></option>';
									html += '<option value="yes"><?php echo e(__('translations.yes')); ?></option>';
									html += '</select>';
									html += '</div>';
								}

								if (data.fields[index]['image']) { 
									html += '<div class="prescription-image">';
									html += '<img src="<?php echo e(url('/').'/storage/'); ?>'+data.fields[index]['image']+'">';
									html += '</div>';
								}

								html += '<div id="inputs_prescription_edit_'+id+'_'+data.fields[index]['id']+'" class="fx prescription-inputs '+(data.fields[index]['image'] ? 'prescription-inputs-half' : '')+'" style="display:'+(data.fields[index]['question'] && data.fields[index]['question'] == 1 ? 'none' : 'flex')+'">';

								if (data.fields[index]['field_type'] == 'text') {
									html += '<input type="text" name="prescriptions['+id+'][fields]['+data.fields[index]['id']+']" id="prescription_edit_'+id+'_'+data.fields[index]['id']+'" value="" placeholder="'+data.fields[index]['name']+'" maxlength="255" '+(data.fields[index]['required'] == 1 ? 'class="form-input form-input-required" required="required"' : 'class="form-input"')+'>';
								}

								if (data.fields[index]['field_type'] == 'select' && data.fields[index]['values']) {
									html += '<select name="prescriptions['+id+'][fields]['+data.fields[index]['id']+']" id="prescription_edit_'+id+'_'+data.fields[index]['id']+'" '+(data.fields[index]['required'] == 1 ? 'class="form-input select-select2-select form-input-required" required="required"' : 'class="form-input select-select2-select"')+'>';
									
									for (index2 in data.fields[index]['values']) {
										html += '<option value="'+data.fields[index]['values'][index2]['value']+'" '+(data.fields[index]['values'][index2]['selected'] == 1 ? 'selected="selected"' : '')+'>'+data.fields[index]['values'][index2]['value']+'</option>';
									}

									html += '</select>';
								}

								if (data.fields[index]['field_type'] == 'checkbox' && data.fields[index]['values']) {
									for (index3 in data.fields[index]['values']) {
										html += '<div class="form-checkbox">';	

										if (data.fields[index]['values'][index3]['image']) {
											html += '<img src="<?php echo e(url('/').'/storage/'); ?>'+data.fields[index]['values'][index3]['image']+'">';
										}

										html += '<input type="checkbox" name="prescriptions['+id+'][fields]['+data.fields[index]['id']+']['+data.fields[index]['values'][index3]['id']+']" id="prescription_edit_'+id+'_'+data.fields[index]['id']+'_'+data.fields[index]['values'][index3]['id']+'" value="'+data.fields[index]['values'][index3]['value']+'" '+(data.fields[index]['values'][index3]['selected'] == 1 ? 'checked="checked"' : '')+'>';
										html += '<label for="prescription_edit_'+id+'_'+data.fields[index]['id']+'_'+data.fields[index]['values'][index3]['id']+'">'+data.fields[index]['values'][index3]['value']+'</label>';
										html += '</div>';
									}
								}

								if (data.fields[index]['field_type'] == 'radio' && data.fields[index]['values']) {
									for (index4 in data.fields[index]['values']) {
										html += '<div class="form-checkbox">';	

										if (data.fields[index]['values'][index4]['image']) {
											html += '<img src="<?php echo e(url('/').'/storage/'); ?>'+data.fields[index]['values'][index4]['image']+'">';
										}

										html += '<input type="radio" name="prescriptions['+id+'][fields]['+data.fields[index]['id']+']" id="prescription_edit_'+id+'_'+data.fields[index]['id']+'_'+data.fields[index]['values'][index4]['id']+'" value="'+data.fields[index]['values'][index4]['value']+'" '+(data.fields[index]['values'][index4]['selected'] == 1 ? 'checked="checked"' : '')+'>';
										html += '<label for="prescription_edit_'+id+'_'+data.fields[index]['id']+'_'+data.fields[index]['values'][index4]['id']+'">'+data.fields[index]['values'][index4]['value']+'</label>';
										html += '</div>';
									}
								}
										
								html += '</div>';
								html += '</div>';
								html += '</div>';
							}
						}

						html += '			<div class="form-line" style="margin-bottom: 5px;">';	
						html += '				<label for="prescription_edit_'+id+'_comment" style="font-size: 15px;"><?php echo e(__('translations.comment')); ?></label>';
						html += '				<textarea name="prescriptions['+id+'][comment]" id="prescription_edit_'+id+'_comment" class="form-inut" placeholder="<?php echo e(__('translations.comment')); ?>" maxlength="1000"></textarea>';
						html += '			</div>';
						html += '		</div>';
						//html += '	</div>';

						html += '   <div class="form-line button-submit">';
						html += '   <div class="btn btn-small btn-danger" onclick="saveChangedPrescription()" style="padding:14px 30px"><?php echo e(__('translations.save')); ?></div>';
						html += '   <div class="btn btn-small" onclick="$.colorbox.close()" style="padding:14px 30px"><?php echo e(__('translations.cancel')); ?></div>';
						html += '   </div>';

						html += '</form>';



						showPopup(html, true);
			    	}
			    }
			});
		}

		function saveChangedPrescription() {
			var form = document.getElementById('edit_prescription_fields_form');
	  
	  		formData = new FormData(form);

	  		if (formData) {
	  			$.ajax({
					url: '/ajax/getData',
					type: 'post',
		            processData: false,
		            contentType: false,
		            dataType: 'json',
		            headers: {
		                'X-CSRF-TOKEN': '<?php echo e(csrf_token()); ?>'
		            },
				    data: formData,
				    success: function (data) {
				    	if (data) {
				    		window.location.reload();
				    	}
				    }
				});
		  	}			
		}

		function showPrescriptionVersion(version, prescription_id, max) {
			if (!version || (version && version == 0)) return false;
			if (!max) max = version;

			$.ajax({
				url: '/ajax/getData',
				type: 'post',
	            dataType: 'json',
	            headers: {
	                'X-CSRF-TOKEN': '<?php echo e(csrf_token()); ?>'
	            },
			    data: {action:'showPrescriptionVersion', id: prescription_id, version: version},
			    success: function (data) {
			    	if (data) {
			    		var html = '';

			    		html += '<link href="/css/checkout.css" rel="stylesheet">';			    		
			    		html += '<div id="order_prescriptions">';
			    		html += '<div class="form-line" style="margin-bottom: 10px;">';
			    		html += '<label for="version_selector">Select version</label>';
			    		html += '<select id="version_selector" class="" onchange="showPrescriptionVersion(this.value, '+prescription_id+', '+max+')">';

			    		for (var i = max; i > 0; i--) {
			    			html += '<option value="'+i+'" '+(i == version ? 'selected' : '')+'>Version #'+i+'</option>';
			    		}

			    		html += '</select>';
			    		html += '</div>';				    	

			    		if (data.fields) {
							for (index in data.fields) {
								html += '<div class="form-line" style="margin-bottom: 10px;">';	
								html += '<div class="prescription-line-head" style="font-size: 15px;">';
								html += '<label for="prescription'+data.fields[index]['id']+'" class="prescription-name">'+data.fields[index]['name']+(data.fields[index]['help'] ? '<span class="prescription-line-help" onclick="showFieldHelp('+data.fields[index]['id']+')">?</span>' : '')+'</label>';

								if (data.fields[index]['description']) {
									html += '<p class="prescription-desc">'+data.fields[index]['description']+'</p>';
								}

								html += '</div>';
								html += '<div class="fx fac prescription-line-content">';								

								if (data.fields[index]['image']) { 
									html += '<div class="prescription-image">';
									html += '<img src="<?php echo e(url('/').'/storage/'); ?>'+data.fields[index]['image']+'">';
									html += '</div>';
								}

								html += '<div id="inputs_prescription_edit_'+prescription_id+'_'+data.fields[index]['id']+'" class="fx prescription-inputs '+(data.fields[index]['image'] ? 'prescription-inputs-half' : '')+'" style="display:'+(data.fields[index]['question'] && data.fields[index]['question'] == 1 ? 'none' : 'flex')+'">';

								if (data.fields[index]['field_type'] == 'text') {
									html += '<input type="text" id="prescription_edit_'+prescription_id+'_'+data.fields[index]['id']+'" value="'+(data.selecteds[data.fields[index]['id']] ? data.selecteds[data.fields[index]['id']] : '')+'" placeholder="'+data.fields[index]['name']+'" maxlength="255" '+(data.fields[index]['required'] == 1 ? 'class="form-input form-input-required" required="required"' : 'class="form-input"')+' disabled>';
								}

								if (data.fields[index]['field_type'] == 'select' && data.fields[index]['values']) {
									html += '<select id="prescription_edit_'+id+'_'+data.fields[index]['id']+'" '+(data.fields[index]['required'] == 1 ? 'class="form-input select-select2-select form-input-required" required="required"' : 'class="form-input select-select2-select"')+' disabled>';
									
									for (index2 in data.fields[index]['values']) {
										html += '<option value="'+data.fields[index]['values'][index2]['value']+'" '+(data.selecteds[data.fields[index]['id']] && data.selecteds[data.fields[index]['id']] == data.fields[index]['values'][index2]['value'] ? 'selected="selected"' : '')+'>'+data.fields[index]['values'][index2]['value']+'</option>';
									}

									html += '</select>';
								}

								if (data.fields[index]['field_type'] == 'checkbox' && data.fields[index]['values']) {
									for (index3 in data.fields[index]['values']) {
										html += '<div class="form-checkbox">';	

										if (data.fields[index]['values'][index3]['image']) {
											html += '<img src="<?php echo e(url('/').'/storage/'); ?>'+data.fields[index]['values'][index3]['image']+'">';
										}

										html += '<input type="checkbox" id="prescription_edit_'+prescription_id+'_'+data.fields[index]['id']+'_'+data.fields[index]['values'][index3]['id']+'" value="'+data.fields[index]['values'][index3]['value']+'" '+(data.selecteds[data.fields[index]['id']] && data.selecteds[data.fields[index]['id']] == data.fields[index]['values'][index3]['value'] ? 'checked="checked"' : '')+'>';
										html += '<label for="prescription_edit_'+prescription_id+'_'+data.fields[index]['id']+'_'+data.fields[index]['values'][index3]['id']+'">'+data.fields[index]['values'][index3]['value']+'</label>';
										html += '</div>';
									}
								}

								if (data.fields[index]['field_type'] == 'radio' && data.fields[index]['values']) {
									for (index4 in data.fields[index]['values']) {
										html += '<div class="form-checkbox">';	

										if (data.fields[index]['values'][index4]['image']) {
											html += '<img src="<?php echo e(url('/').'/storage/'); ?>'+data.fields[index]['values'][index4]['image']+'">';
										}

										html += '<input type="radio" id="prescription_edit_'+prescription_id+'_'+data.fields[index]['id']+'_'+data.fields[index]['values'][index4]['id']+'" value="'+data.fields[index]['values'][index4]['value']+'" '+(data.selecteds[data.fields[index]['id']] && data.selecteds[data.fields[index]['id']] == data.fields[index]['values'][index4]['value'] ? 'checked="checked"' : '')+'>';
										html += '<label for="prescription_edit_'+prescription_id+'_'+data.fields[index]['id']+'_'+data.fields[index]['values'][index4]['id']+'">'+data.fields[index]['values'][index4]['value']+'</label>';
										html += '</div>';
									}
								}
										
								html += '</div>';
								html += '</div>';
								html += '</div>';
							}
						}

						html += '			<div class="form-line" style="margin-bottom: 5px;">';	
						html += '				<label for="prescription_edit_'+prescription_id+'_comment" style="font-size: 15px;"><?php echo e(__('translations.comment')); ?></label>';
						html += '				<textarea id="prescription_edit_'+prescription_id+'_comment" class="form-inut" placeholder="<?php echo e(__('translations.comment')); ?>" maxlength="1000" disabled>'+(data.comment ? data.comment : '')+'</textarea>';
						html += '			</div>';
						html += '		</div>';
						//html += '	</div>';

						html += '   <div class="form-line button-submit">';						
						html += '   <div class="btn btn-small" onclick="$.colorbox.close()" style="padding:14px 30px"><?php echo e(__('translations.cancel')); ?></div>';
						html += '   </div>';

						showPopup(html, true);
			    	}
			    }
			});
		}

		function createPrescriptionRemake(invoice, order, prescription) {

			$.ajax({
				url: '/ajax/getData',
				type: 'post',
	            dataType: 'json',
	            headers: {
	                'X-CSRF-TOKEN': '<?php echo e(csrf_token()); ?>'
	            },
			    data: {action:'createPrescriptionRemake', invoice_id: invoice, order_id: order, prescription_id: prescription},
			    success: function (data) {
			    	if (data) {
			    		window.location.href = '/account/orders';
			    	}
			    }
			});
		}

		var file_index = <?php echo e($file_index); ?>;

		function addFileToCheckoutForm() {
			if ($(document).find('#order_file_input'+file_index).length) {
				file_index++;
				addFileToCheckoutForm();

				return false;
			}

			createFileFieldCheckoutForm(file_index);

			$(document).find('#order_file_input'+file_index).click();

			var order_files = $(document).find('.order-file-icon');

			if (order_files.length > 4) $('#order_files_add').css('display', 'none');

			file_index++;
		}

		function createFileFieldCheckoutForm(index) {
			var html = '';

			html += '<div id="order_file_line'+index+'" class="fx fxb order-file-line order-file-icon">';			
			html += '<div id="order_file_name'+index+'" class="fx fxc order-file-name">';	
			html += '<span>Empty!</span>';			
			html += '<div class="order-file-remove" onclick="removeFileFromCheckoutForm('+index+')" title="<?php echo e(__('translations.delete')); ?>"><div></div></div>';
			html += '<input type="hidden" id="order_files_save'+index+'" name="order_files['+index+']" value="">';	
			html += '<input type="file" id="order_file_input'+index+'" onchange="checkFileFieldCheckoutForm('+index+');">';
			html += '</div>';
			html += '</div>';

			$('#order_files').append(html);
		}

		function removeFileFromCheckoutForm(index, file_id) {
			$(document).find('#order_file_line'+index).remove();

			var order_files = $(document).find('.order-file-icon');

			if (order_files.length <= 4) $('#order_files_add').css('display', 'block');

			if (file_id) {
				$.ajax({
					url: '/ajax/getData',
					type: 'post',
		            dataType: 'json',
		            headers: {
		                'X-CSRF-TOKEN': '<?php echo e(csrf_token()); ?>'
		            },
		            data: {action:'removeCheckoutFile', order_id: <?php echo e($data->id); ?>, file_id: file_id}
		        });
			}
		}

		function checkFileFieldCheckoutForm(index) {
			var file = $(document).find('#order_file_input'+index);
			var value = file.prop('files')[0];

			if (!value || !file.val()) {
				removeFileFromCheckoutForm(index);

				return false;
			}

			if (value.size > 10000000) {
				showPopup('<?php echo e(__('translations.checkout_file_error')); ?>');
				removeFileFromCheckoutForm(index);

				return false;
			}

			let form_data = new FormData();

			form_data.append('order_file', value);	
			form_data.append('file_index', index);
			form_data.append('order_id', <?php echo e($data->id); ?>);
			form_data.append('to_branch_id', <?php echo e($data->to_branch_id); ?>);			
			form_data.append('action', 'saveCheckoutFile');		

			$.ajax({
				url: '/ajax/getData',
				type: 'post',
	            dataType: 'json',
	            processData: false,
	        	contentType: false,
	            headers: {
	                'X-CSRF-TOKEN': '<?php echo e(csrf_token()); ?>'
	            },
	            data: form_data,
	            success: function(data) {
	            	if (data) {
	            		$(document).find('#order_files_save'+index).val(data.file);

	            		var html = '<a href="/storage/'+data.file+'" target="_blank" class="fx fxc">';

	            		if (data.extension == 'jpg' || data.extension == 'jpeg' || data.extension == 'JPEG' || data.extension == 'JPG' || data.extension == 'png' || data.extension == 'PNG') {
							html += '<img src="/storage/'+data.file+'" width="100%" height="auto" />';
	            		} else {
	            			html += '<span>.'+data.extension+'</span>';
	            		}

	            		html += '</a>';

	            		$('#order_file_name'+index+' span').replaceWith(html);
	            	}
	            }
			});			
		}
	</script>	
	<?php endif; ?>
	<?php if($data->status == 2): ?>
		<script> 
			function calculateOrderTotal() {
				var prices = $(document).find('.prescription-price-input');
				var total = 0;

				prices.each(function(i){
					total = total + parseFloat(prices[i].value);
				});

				$('#order_total').html(total.toFixed(2));
				$('#order_total_input').val(total.toFixed(2));
			}

			<?php if(Auth::user()->type == config('types.lab')): ?> 
				function editOrderPricesDialog() {					
					var inputs = $('.prescription-price-input');

					if (inputs) {
						showLoader();

						var html = '';

						html += '<div class="price-changing-popup">';
						html += '<h2><?php echo e(__('translations.change_final_prices')); ?>:</h2>';

						inputs.each(function(i){
							html += '<div class="fx form-line">';
							html += '<label for="prescription_popup_price_'+inputs[i].dataset.index+'"><?php echo e(__('translations.order_prescription')); ?> #'+inputs[i].dataset.index+'</label>';
							html += '<?php echo e($currency->symbol); ?><input type="text" class="form-input form-input-required" id="prescription_popup_price_'+inputs[i].dataset.index+'" value="'+inputs[i].value+'" onchange="isNaN(this.value) ? this.value = 0 : this.value = this.value;proccessPriceChanging('+inputs[i].dataset.index+')">';
							html += '</div>';
						});

						html += '<div class="fx fxc form-line">';
						html += '<div class="btn btn-small" onclick="savePriceChanging();">Save</div>';
						html += '</div>';
						html += '</div>';

						hideLoader();
		    			showPopup(html, true);
					}
				}

				function proccessPriceChanging(index) {
					var val = $(document).find('#prescription_popup_price_'+index).val();

					$(document).find('#prescription_price_'+index).val(val);

					calculateOrderTotal();
				}

				function savePriceChanging() {
					setTimeout(function() {
						$('#submit2').click();
					}, 700); 
				}
			<?php endif; ?>
		</script>
	<?php endif; ?>	
<?php $__env->stopSection(); ?>
<?php echo $__env->make('Frontend/account_master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/dentist2lab.com/resources/views/Frontend/pages/account/orders/edit.blade.php ENDPATH**/ ?>