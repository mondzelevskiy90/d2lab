<?php $__env->startSection('css'); ?>
	<link rel="stylesheet" href="<?php echo e(mix('/css/account.css')); ?>">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>	
	<div id="panel_top_buttons" class="fx fac fxb">
		<?php echo $__env->make('Frontend.templates.breadcrumbs', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
		<div class="panel-top-buttons button-submit order-buttons">
			<a class="btn btn-small btn-grey" href="<?php echo e(route('reviews', [$requests])); ?>">&larr; <?php echo e(__('translations.back')); ?></a>				
		</div>
	</div>
	<h1 class="tc"><?php echo e(__('translations.order')); ?> #<?php echo e($data->id); ?> <?php echo e($seo->name); ?></h1>
	<?php echo e(csrf_field()); ?>

	<div class="fx fxb panel-body">
		<div class="fx fac fxb panel">
			<?php echo $__env->make('Frontend.templates.review_block', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
		</div>
	</div>
	
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
	<?php echo $__env->make('Frontend.templates.review_functions', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('Frontend/account_master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/d2l.sitepark.ua/resources/views/Frontend/pages/account/reviews/edit.blade.php ENDPATH**/ ?>