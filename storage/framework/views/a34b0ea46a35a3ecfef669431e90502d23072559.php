<?php $__env->startSection('page_title', __('voyager::generic.'.(isset($data->id) ? 'edit' : 'add')).' review'); ?>

<?php $__env->startSection('css'); ?>
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('page_header'); ?>
    <h1 class="page-title">
        <i class="voyager-edit"></i>
        <?php echo e(__('voyager::generic.'.(isset($data->id) ? 'edit' : 'add')).' review'); ?>

    </h1>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="page-content container-fluid">
        <form class="form-edit-add" role="form" action="<?php if(!is_null($data->id)): ?><?php echo e(route('voyager.'.$type.'.update', [$data->id, $requests])); ?><?php else: ?><?php echo e(route('voyager.'.$type.'.store', [$requests])); ?><?php endif; ?>" method="POST" enctype="multipart/form-data" autocomplete="off">            
            <?php if(isset($data->id)): ?>
                <?php echo e(method_field("PUT")); ?>

            <?php endif; ?>
            <?php echo e(csrf_field()); ?>


            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-bordered">                   
                        <?php if(count($errors) > 0): ?>
                            <div class="alert alert-danger">
                                <ul>
                                    <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <li><?php echo e($error); ?></li>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </ul>
                            </div>
                        <?php endif; ?>
                        <div class="panel-body">
                            <input type="hidden" name="type_str" id="type_str" value="<?php echo e($data->type_str); ?>">
                            <div class="form-group">
                                <label for="status">Status</label>
                                <select class="form-control" name="status">                                    
                                    <option value="1" <?php if(isset($data->status) && $data->status == 1): ?> selected <?php endif; ?>>Active</option>
                                    <option value="2" <?php if(!isset($data->status) || $data->status == 2): ?> selected <?php endif; ?>>disactive</option>   
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="moderation">Moderation</label>
                                <select class="form-control" name="moderation">                                    
                                    <option value="1" <?php if(isset($data->moderation) && $data->moderation == 1): ?> selected <?php endif; ?>>Not need</option>
                                    <option value="2" <?php if(!isset($data->moderation) || $data->moderation == 2): ?> selected <?php endif; ?>>Need</option>   
                                </select>
                            </div>
                            <?php if(isset($data->moderation_text) && $data->moderation_text): ?>
                                <div class="form-group">
                                    <label for="review">Moderation reason</label>
                                    <p><?php echo e($data->moderation_text); ?></p>
                                </div>
                            <?php endif; ?>
                            <div class="form-group">
                                <label for="review">User</label>
                                <p><a href="<?php echo e(route('voyager.users.edit', $data->user_id)); ?>"><?php echo e($data->user->name.' '.$data->user->surname); ?></a></p>
                            </div>
                            <div class="form-group">
                                <label for="review">Review</label>
                                <p><strong><?php echo e($data->review); ?></strong></p>
                            </div>
                        </div>
                    </div>
                </div>                
            </div>

            <button type="submit" class="btn btn-primary pull-right save">
                <?php echo e(__('voyager::generic.save')); ?>

            </button>
        </form>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>
    <script>
        $('document').ready(function () {
            $('.toggleswitch').bootstrapToggle();
        });
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('voyager::master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/dentist2lab.com/resources/views/Backend/order-reviews/edit-add.blade.php ENDPATH**/ ?>