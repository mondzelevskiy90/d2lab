<?php $__env->startSection('css'); ?>
	<link rel="stylesheet" href="<?php echo e(mix('/css/account.css')); ?>">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('breadcrumbs'); ?>
	<?php echo $__env->make('Frontend.templates.breadcrumbs', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
	<h1><?php echo e($seo->name); ?></h1>
	<div class="fx fxb panel-body">
		<div class="panel panel-table">
			<?php if($data && count($data)): ?> 
				<div class="account-list-table account-list-table4">
					<div class="fx account-list-head">	
						<div class="account-list-cell"><?php echo e(__('translations.notification')); ?></div>					
						<div class="account-list-cell"><?php echo e(__('translations.date')); ?></div>
						<div class="account-list-cell"></div>						
					</div>
					<div class="account-list-content">
						<?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>						
							<div id="notification_<?php echo e($item->id); ?>" class="fx fac account-list-line  dashboard-notification-line">
								<div class="account-list-cell">
									<?php echo $item->text; ?>

								</div>								
								<div class="account-list-cell">
									<?php echo e(date('m-d-Y', strtotime($item->created_at))); ?>

								</div>						
								<div class="account-list-cell">
									<div class="btn btn-small" onclick="notificationViewed(<?php echo e($item->id); ?>)">Ok!</div>
								</div>
							</div>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					</div>
				</div>
				<div class="pagination-block"><?php echo e($data->appends(\Request::except('page'))->links()); ?></div>
			<?php endif; ?>
		</div>
	</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
	<script>
		function notificationViewed(id) {
			$.ajax({
				url: '/ajax/getData',
				type: 'post',
			    dataType: 'json',
			    headers: {
			        'X-CSRF-TOKEN': '<?php echo e(csrf_token()); ?>'
			    },
			    data: {action:'notificationViewed', id: id},	
			    success: function(data) {
			    	$('#notification_'+id).remove();
			    	
			    	if ($('.dashboard-notification-line').length == 0) window.location.reload();
			    }
			});
		}
	</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('Frontend/account_master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/dentist2lab.com/resources/views/Frontend/pages/account/notifications/index.blade.php ENDPATH**/ ?>