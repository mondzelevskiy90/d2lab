<?php $__env->startSection('page_title', __('voyager::generic.'.(isset($data->id) ? 'edit' : 'add')).' '.$data->name); ?>

<?php $__env->startSection('css'); ?>
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('page_header'); ?>
    <h1 class="page-title">
        <i class="voyager-edit"></i>
        <?php echo e(__('voyager::generic.'.(isset($data->id) ? 'edit' : 'add')).' '.$data->name); ?>

    </h1>
<?php $__env->stopSection(); ?>

<?php
function create_services_list($items, $selected, $parents = array(), $padding = 10) {    
    foreach ($items as $item) {      
        $line = '<tr>';
        $line .= '<td>';
        $line .= '<input type="checkbox" id="services'.$item['id'].'" name="services['.$item['id'].']" '.(in_array($item['id'], $selected) ? 'checked="checked"' : '').' data-parents="'.(count($parents) ? implode(',', $parents) : '').'" data-id="'.$item['id'].'" class="service-item-input">';
        $line .= '</td>';
        $line .= '<td style="padding-left:'.$padding.'px;'.($item['childrens'] ? 'font-weight:bold;' : '').($padding != 0 ? 'font-sise:'.(14 - ($padding / 30)).'px;' : '').'">'.$item['name'].'</td>';        
        $line .= '</tr>';

        echo $line;

        $parents_arr = $parents;
        $parents_arr[] = $item['id'];

        if (!empty($item['childrens'])) create_services_list($item['childrens'], $selected, $parents_arr, $padding + 30);
    }
}
?>

<?php $__env->startSection('content'); ?>
    <div class="page-content container-fluid">
        <form class="form-edit-add" role="form" action="<?php if(!is_null($data->id)): ?><?php echo e(route('voyager.'.$type.'.update', [$data->id, $requests])); ?><?php else: ?><?php echo e(route('voyager.'.$type.'.store', [$requests])); ?><?php endif; ?>" method="POST" enctype="multipart/form-data" autocomplete="off">            
            <?php if(isset($data->id)): ?>
                <?php echo e(method_field("PUT")); ?>

            <?php endif; ?>
            <?php echo e(csrf_field()); ?>


            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-bordered">  
                        <div class="panel-body">                 
                            <?php if(count($errors) > 0): ?>
                                <div class="alert alert-danger">
                                    <ul>
                                        <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <li><?php echo e($error); ?></li>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </ul>
                                </div>
                            <?php endif; ?>                                                    
                            <div class="form-group">
                                <label for="status"><?php echo e(__('adm.status')); ?></label>
                                <select class="form-control" name="status">        
                                    <option value="1" <?php if(isset($data->status) && $data->status == 1): ?> selected <?php endif; ?>>Active</option>       
                                    <option value="2" <?php if(isset($data->status) && $data->status == 2): ?> selected <?php endif; ?>>Disactive</option>                                      
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="name"><?php echo e(__('adm.name')); ?></label>
                                <input type="text" class="form-control" id="name" name="name" placeholder="<?php echo e(__('adm.name')); ?>" value="<?php echo e(old('name', $data->name ?? '')); ?>">
                            </div> 
                            <div class="form-group">
                                <label for="description"><?php echo e(__('adm.description')); ?></label>
                                <input type="text" class="form-control" id="description" name="description" placeholder="<?php echo e(__('adm.description')); ?>" value="<?php echo e(old('description', $data->description ?? '')); ?>">
                            </div>
                            <div class="form-group">
                                <label for="help">Help</label>
                                <input type="text" class="form-control" id="help" name="help" placeholder="Help" value="<?php echo e(old('help', $data->help ?? '')); ?>">
                            </div> 
                            <div class="form-group">
                                <label for="default_prescription">Use for default prescription</label>
                                <select class="form-control" name="default_prescription">        
                                    <option value="1" <?php if(isset($data->default_prescription) && $data->default_prescription == 1): ?> selected <?php endif; ?>>Active</option>       
                                    <option value="2" <?php if(isset($data->default_prescription) && $data->default_prescription == 2): ?> selected <?php endif; ?>>Disactive</option>                                      
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="required">Required</label>
                                <select class="form-control" name="required">        
                                    <option value="1" <?php if(isset($data->required) && $data->required == 1): ?> selected <?php endif; ?>>Yes</option>       
                                    <option value="2" <?php if(isset($data->required) && $data->required == 2): ?> selected <?php endif; ?>>No</option>                                      
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="field_type">Field type</label>
                                <select class="form-control" name="field_type" id="field_type">        
                                    <option value="text" <?php if(isset($data->field_type) && $data->field_type == 'text'): ?> selected <?php endif; ?>>Input type=text</option>       
                                    <option value="checkbox" <?php if(isset($data->field_type) && $data->field_type == 'checkbox'): ?> selected <?php endif; ?>>Input type=checkbox (multiple selection)</option>
                                    <option value="radio" <?php if(isset($data->field_type) && $data->field_type == 'radio'): ?> selected <?php endif; ?>>Input type=radio (single selection)</option>
                                    <option value="select" <?php if(isset($data->field_type) && $data->field_type == 'select'): ?> selected <?php endif; ?>>Select</option> 
                                    <option value="file" <?php if(isset($data->field_type) && $data->field_type == 'file'): ?> selected <?php endif; ?>>Input type=file</option>                                    
                                </select>
                            </div>   
                            <div id="field_values_block" class="form-group" style="<?php if(isset($data->field_type) && ($data->field_type == 'select' || $data->field_type == 'checkbox' || $data->field_type == 'radio')): ?><?php echo e('display: block;'); ?><?php else: ?><?php echo e('display:none;'); ?><?php endif; ?>">
                                <?php
                                    $cnt = 0;
                                ?>

                                <table id="dataTable" class="table table-hover">
                                    <thead>
                                        <tr>                                
                                            <th>Value</th>
                                            <th>Image (only for checkbox type!)</th>
                                            <th>Default</th>
                                            <th>Sort order</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody id="field_values_tbody">
                                        <?php $__currentLoopData = $field_values; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $field_value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <tr id="field_line<?php echo e($cnt); ?>">
                                                <td>
                                                    <input type="text" name="field_values[<?php echo e($cnt); ?>][value]" class="form-control" value="<?php echo e($field_value->value); ?>" placeholder="Value">
                                                </td>
                                                <td>
                                                    <?php if($field_value->image): ?>                            
                                                    <img src="<?php echo e(filter_var($field_value->image, FILTER_VALIDATE_URL) ? $field_value->image : Voyager::image( $field_value->image )); ?>" style="width:30px; height:auto; clear:both; display:block; padding:2px; border:1px solid #ddd;margin-bottom:10px;" />
                                                        <input type="hidden" name="field_values[<?php echo e($cnt); ?>][image]" class="form-control" value="<?php echo e($field_value->image); ?>">
                                                    <?php else: ?>
                                                        <input type="file" name="field_values[<?php echo e($cnt); ?>][image]" class="form-control" value="">
                                                    <?php endif; ?>
                                                </td>
                                                <td>
                                                    <input type="checkbox" name="field_values[<?php echo e($cnt); ?>][selected]" <?php if($field_value->selected && $field_value->selected == 1): ?><?php echo e('checked="checked"'); ?><?php endif; ?>>
                                                </td>
                                                <td>
                                                    <input type="text" name="field_values[<?php echo e($cnt); ?>][sort_order]" class="form-control" value="<?php echo e($field_value->sort_order); ?>">
                                                </td>
                                                <td>
                                                    <select class="form-control" name="field_values[<?php echo e($cnt); ?>][status]">
                                                        <option value="1" <?php if($field_value->status == 1): ?> selected="selected" <?php endif; ?>>Active</option>
                                                        <option value="2" <?php if($field_value->status == 2): ?> selected="selected" <?php endif; ?>>Disabled</option>
                                                    </select>
                                                </td>
                                                <td><div class="btn btn-danger" onclick="document.getElementById('field_line<?php echo e($cnt); ?>').remove();">Remove</div></td>
                                            </tr>
                                            <?php $cnt++; ?>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>                                    
                                    </tbody>
                                    <tfoot>
                                        <td colspan="4"></td>
                                        <td>
                                            <div class="btn btn-primary" onclick="addFieldValueLine()">Add</div>
                                        </td>
                                    </tfoot>
                                </table>


                                <input type="hidden" id="use_values" name="use_values" value="<?php if(count($field_values)): ?><?php echo e(1); ?><?php else: ?><?php echo e(0); ?><?php endif; ?>">
                            </div>             
                            <div class="form-group">
                                <label for="sort_order"><?php echo e(__('adm.sort_order')); ?></label>
                                <input type="text" class="form-control" id="sort_order" name="sort_order" placeholder="<?php echo e(__('voyager::generic.sort_order')); ?>" value="<?php echo e(old('sort_order', $data->sort_order ?? 0)); ?>">
                            </div> 
                            <div class="form-group">
                                <label for="image"><?php echo e(__('adm.image')); ?></label>
                                <?php if(isset($data->image) && $data->image != ''): ?>
                                    <img src="<?php echo e(filter_var($data->image, FILTER_VALIDATE_URL) ? $data->image : Voyager::image( $data->image )); ?>" style="width:50px; height:auto; clear:both; display:block; padding:2px; border:1px solid #ddd;margin-bottom:10px;" />
                                <?php endif; ?>
                                <input type="file" data-name="image" name="image" id="image">
                            </div> 
                            <?php if($numbering_systems): ?>
                                <div class="form-group">
                                    <br>
                                    <h3>Available for Tooth numbering systems:</h3>
                                    <?php $__currentLoopData = $numbering_systems; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $numbering_system): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
                                        <label>                                        
                                            <input type="checkbox" id="systems<?php echo e($numbering_system->id); ?>" name="systems[<?php echo e($numbering_system->id); ?>]" <?php if(in_array($numbering_system->id, $selected_systems_data)): ?> checked="checked" <?php endif; ?>>
                                            <?php echo e($numbering_system->name); ?>

                                        </label>
                                        <br>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </div>
                            <?php endif; ?>
                            <?php if($services): ?>
                                <div class="form-group">
                                    <br>
                                    <h3>Available for services:</h3>
                                    <table id="dataTable" class="table table-hover">
                                        <thead>
                                            <tr>                                
                                                <th></th>
                                                <th>Service name</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php 
                                                create_services_list($services, $selected_service_data); 
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            <?php endif; ?>                                                     
                        </div>
                    </div>
                </div>                
            </div>

            <button type="submit" class="btn btn-primary pull-right save">
                <?php echo e(__('voyager::generic.save')); ?>

            </button>
        </form>

       
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>
    <script>
        $('document').ready(function () {
            $('.toggleswitch').bootstrapToggle();

            $('#field_type').on('change', function(){
                var val = $(this).val();

                if (val == 'select' || val == 'checkbox' || val == 'radio') {
                    $('#field_values_block').css('display', 'block');
                    $('#use_values').val(1);
                } else {
                    $('#field_values_block').css('display', 'none');
                    $('#use_values').val(0);
                }
            });

            $('.service-item-input').on('change', function(){
                var index = $(this).data('id');
                var checked = $(this).prop('checked');
                var inputs = $('.service-item-input');

                inputs.each(function(i){
                    var parents = inputs[i].getAttribute('data-parents');

                    if (parents) {
                        parents = parents.split(',');

                        for(j = 0; j < parents.length; j++) {

                            if (parents[j] == index) {
                                 console.log(checked);
                                inputs[i].removeAttribute('checked');

                                if (checked) {
                                    inputs[i].setAttribute('checked', 'checked');
                                }                                 
                            }
                        };
                    }
                });
            });
        });

        var cnt = <?php echo e($cnt); ?>;

        function addFieldValueLine() {
            var html = '';

            html += '<tr id="field_line'+cnt+'">';
            html += '<td>';
            html += '<input type="text" name="field_values['+cnt+'][value]" class="form-control" value="" placeholder="Value">';
            html += '</td>';
            html += '<td>';
            html += '<input type="file" name="field_values['+cnt+'][image]" class="form-control" value="">';
            html += '</td>';
            html += '<td>';
            html += '<input type="checkbox" name="field_values['+cnt+'][selected]">';
            html += '</td>';
            html += '<td>';
            html += '<input type="text" name="field_values['+cnt+'][sort_order]" class="form-control" value="0">';
            html += '</td>';
            html += '<td>';
            html += '<select class="form-control" name="field_values['+cnt+'][status]">';
            html += '<option value="1">Active</option>';
            html += '<option value="2">Disabled</option>';
            html += '</select>';
            html += '</td>';
            html += '<td><div class="btn btn-danger" onclick="document.getElementById(\'field_line'+cnt+'\').remove();">Remove</div></td>';
            html += '</tr>';

            $('#field_values_tbody').append(html);

            cnt++;
        }
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('voyager::master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/d2l.sitepark.ua/resources/views/Backend/fields/edit-add.blade.php ENDPATH**/ ?>