<?php $__env->startSection('css'); ?>
	<link rel="stylesheet" href="<?php echo e(mix('/css/account.css')); ?>">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('breadcrumbs'); ?>
	<?php echo $__env->make('Frontend.templates.breadcrumbs', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
	<h1><?php echo e($seo->name); ?></h1>
	<div class="fx fxb panel-body">
		<div class="panel panel-table">
			<?php if($data && count($data)): ?> 
				<div class="account-list-table account-list-table4">
					<div class="fx account-list-head">						
							<div class="account-list-cell"><?php echo e(__('translations.name')); ?></div>
							<div class="account-list-cell"><?php echo e(__('translations.address')); ?></div>
							<div class="account-list-cell"><?php echo e(__('translations.logo')); ?></div>
							<div class="account-list-cell"></div>						
					</div>
					<div class="account-list-content">
						<?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<div class="fx fac account-list-line">
								<div class="account-list-cell"><a href="<?php echo e(route('branch', ['labs', $item->branch_id])); ?>"><?php echo e($item->branch->name); ?></a></div>
								<div class="account-list-cell"><?php echo e(app('App\Services\Userdata')::getUserAddress($item->branch)); ?></div>
								<div class="account-list-cell">
									<div class="bi company-logo" style="margin: auto; background-image: url(/storage/<?php echo e($item->branch->logo); ?>)"></div>
								</div>								
								<div class="account-list-cell">
									<a href="<?php echo e(route('checkout')); ?>?branch_id=<?php echo e($item->branch_id); ?>" class="btn btn-small"><?php echo e(__('translations.order')); ?></a>
									<div data-branch="<?php echo e($item->branch_id); ?>" class="btn btn-danger btn-small delete-branch-from-list"><?php echo e(__('translations.delete')); ?></div>
								</div>
							</div>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					</div>
				</div>
				<div class="pagination-block"><?php echo e($data->appends(\Request::except('page'))->links()); ?></div>
			<?php else: ?> 
				<p><?php echo e(__('translations.no_mylabs')); ?></p>
				<br>
				<a class="btn btn-primary" href="<?php echo e(url('catalog/dentistry/labs')); ?>"><?php echo e(__('translations.view_all_labs')); ?></a>
			<?php endif; ?>
		</div>
	</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
	<script>
		document.addEventListener('DOMContentLoaded', function() {		 	
			$('.delete-branch-from-list').on('click', function(){
				if (confirm('<?php echo e(__('translations.delete_alert')); ?>')) {
					var user_id = '<?php echo e(Auth::user()->id); ?>',
						branch_id = $(this).data('branch');				

					$.ajax({
						url: '/ajax/getData',
						type: 'post',
		                dataType: 'json',
		                headers: {
		                    'X-CSRF-TOKEN': '<?php echo e(csrf_token()); ?>'
		                },
					    data: {action:'addToMyLabs', branch_id: branch_id, user_id: user_id},
					    success: function(data) {				    	
					    	if (data) {				    		
					    		window.location.href = '<?php echo e(route('mylabs')); ?>';
					    	}
					    }
					});
				}
			});			
		}, !1);
	</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('Frontend/account_master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/d2l.sitepark.ua/resources/views/Frontend/pages/account/labs/index.blade.php ENDPATH**/ ?>