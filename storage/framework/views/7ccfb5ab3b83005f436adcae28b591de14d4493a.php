<?php $__env->startSection('css'); ?>
	<link rel="stylesheet" href="<?php echo e(mix('/css/branch.css')); ?>">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('after_head'); ?>
	<div class="branch-top">
		<div class="fx fxb content">
			<div class="bi branch-logo" style="background-image: url(<?php echo e(App\Services\Img::squareImage($data->logo, 75, 75)); ?>)"></div>
			<div class="fx branch-menu">
				<div onclick="scrollToblock('summary')"><?php echo e(__('translations.summary')); ?></div>
				<div onclick="scrollToblock('prices')"><?php echo e(__('translations.prices')); ?></div>
				<?php if(count($data->portfolio)): ?>
					<div onclick="scrollToblock('portfolio')"><?php echo e(__('translations.portfolio')); ?></div>
				<?php endif; ?>
				<?php if(!Auth::check() || count($data->reviews)): ?>
					<div onclick="scrollToblock('reviews')"><?php echo e(__('translations.reviews')); ?></div>
				<?php endif; ?>
			</div>
		</div>
	</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('breadcrumbs'); ?>
	<?php echo $__env->make('Frontend.templates.breadcrumbs', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
	<h1><?php echo e($seo->name); ?></h1>
	<div class="page-text">
		<div id="summary">
			<div class="fx fxb branch-block-content">
	            <div class="fx fxb branch-summary-left">
	            	<?php if(Auth::check()): ?>
	            		<div class="fx branch-summary-rating">		            		
		            		<div class="branch-info-rating">
								<?php echo e(__('translations.rating')); ?><span><?php echo e($data->rating); ?></span>
							</div>								
							<div class="branch-info-reviews">
								<div class="bi branch-info-stars branch-info-stars<?php echo e(floor($data->reviews_rating)); ?>"></div>
								<?php echo e(count($data->reviews) .' '. __('translations.reviews')); ?>

							</div>
						</div>
						<?php if(Auth::user()->type == config('types.clinic')): ?>
							<div id="branch_my_lab" class="branch-my-lab">									
								<?php if($mybranches): ?>
									<div class="branch-list-remove">	
										<?php echo e(__('translations.added_to_labs')); ?><br>					
										<div id="branch_list_action" class="btn btn-danger branch-list-action" data-branch="<?php echo e($data->id); ?>" data-method="remove"><?php echo e(__('translations.remove')); ?></div>
									</div>
								<?php else: ?>
									<div class="branch-list-add">
										<div id="branch_list_action" class="btn branch-list-action" data-method="add">
											<span>+</span>
											<?php echo e(__('translations.add_to_labs')); ?>

										</div>
									</div>
								<?php endif; ?>	
							</div>
						<?php endif; ?>
					<?php else: ?>
						<div class="bi branch-info-ratingMuted" onclick="showRegAlert();"></div>
					<?php endif; ?>
					<?php if($data->description && $data->description != ''): ?>
						<div class="branch-info-description">
							<?php echo $data->description; ?>

						</div>
					<?php endif; ?>
	            </div>
	            <div class="branch-summary-right">
					<div id="map" <?php if(!Auth::check()): ?> class="bi branch-map-muted" onclick="showRegAlert();" <?php endif; ?>></div>
					<div class="branch-summary-icons">
						<div class="bi branch-summary-icon branch-summary-verified"><?php echo e(__('translations.verified')); ?></div>
						<?php if(Auth::check()): ?>	
							<?php if($data->apl): ?>
								<div class="bi branch-summary-icon branch-summary-apl" title="<?php echo e($data->apl['title']); ?>"><?php echo e($data->apl['value']); ?></div>
							<?php endif; ?>
							<div class="bi branch-summary-icon branch-summary-email"><?php echo e($data->email); ?></div>
							<div class="bi branch-summary-icon branch-summary-phone"><?php echo e($data->phone); ?></div>
							<div class="bi branch-summary-icon branch-summary-location"><?php echo e($data->zip_code .', '. $data->address .', '. $data->location_name); ?></div>
						<?php endif; ?>
					</div>
	            </div>   
			</div>
		</div>
		<div id="prices">
			<?php if($branch_focus): ?>
			<div class="branch-focus-block">
				<div class="fx branch-focus-body">
					<?php $__currentLoopData = $branch_focus; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
						<div class="branch-focus-service" style="width: <?php echo e($item['percent']); ?>%; background-color: <?php echo e($item['color']); ?>;" title="<?php echo e($item['name']); ?>"></div>
					<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>					
				</div>
				<?php $__currentLoopData = $branch_focus; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
					<div class="branch-focus-main">
						<?php echo e($item['percent']); ?>% <?php echo e($item['name']); ?>

					</div>
					<?php if($loop->first): ?>
						<?php break; ?>
					<?php endif; ?>
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
			</div>
			<?php endif; ?>
			<?php if(Auth::check() && Auth::user()->type == config('types.clinic')): ?>
			<div class="branch-checkout-link">
				<a class="btn" href="<?php echo e(route('checkout').'?branch_id='.$data->id); ?>"><?php echo e(__('translations.order')); ?></a>
			</div>	
			<?php elseif(Auth::check() && Auth::user()->role_id == config('roles.guest')): ?>
				<div class="branch-checkout-link">
					<a class="btn" href="<?php echo e(route('checkout')); ?>" onclick="showPopup('<?php echo e(__('translations.only_lab_order')); ?>'); return false;"><?php echo e(__('translations.order')); ?></a>
				</div>
			<?php endif; ?> 		
			<div class="branch-block-head"><?php echo e(__('translations.prices')); ?></div>
			<div class="branch-block-content">
				<div class="branch-prices-block">
					<?php $__currentLoopData = $data->prices; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $price_cat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
						<div class="bi branch-prices-head"><?php echo e($price_cat['name']); ?></div>
						<div class="branch-prices-prices">
							<?php $__currentLoopData = $price_cat['prices']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $price): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
								<div class="fx fxb branch-prices-priceLine">
									<div class="branch-prices-priceName"><span><strong><?php echo e($price['name']); ?></strong> <?php if($price['options'] && $price['options'] != ''): ?>(<?php echo e($price['options']); ?>)<?php endif; ?></span></div>
									<div class="branch-prices-priceValue"><?php echo e($price['value']); ?></div>
								</div>
							<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
						</div>
					<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				</div>
			</div>
		</div>

		<?php if(count($data->portfolio)): ?>
			<div id="portfolio">
				<div class="branch-block-head"><?php echo e(__('translations.portfolio')); ?></div>
				<div class="branch-block-content">
					<?php $__currentLoopData = $data->portfolio; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $portfolio): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
						<?php
							$images = explode(',', $portfolio->images);
						?>
						<div class="branch-portfolio-line">
							<div class="branch-portfolio-head" <?php if(count($data->portfolio) == 1): ?> style="display:none;" <?php endif; ?>>
								<?php echo e($portfolio->service->name); ?>

							</div>
							<div class="fx branch-portfolio-images">
								<?php $__currentLoopData = $images; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $image): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
									<div class="branch-portfolio-image">
										<a href="<?php echo e('/storage/'.$image); ?>" class="popup-link">
											<div class="bi" style="background-image: url(<?php echo e(\App\Services\Img::squareImage($image, 360, 360)); ?>)"></div>
										</a>
									</div>
								<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
							</div>
						</div>
					<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				</div>
			</div>
		<?php endif; ?>

		<?php if(Auth::check()): ?> 
			<?php if(count($data->reviews)): ?>
				<div id="reviews">
					<div class="branch-block-head"><?php echo e(__('translations.reviews')); ?></div>
					<div class="branch-block-content">
						<div class="branch-reviews">
							<div class="fx fac branch-reviews-totals">
								<div class="branch-reviews-reviewsRating">
									<?php echo e($data->reviews_rating); ?>

								</div>
								<div class="branch-reviews-reviewsStars">
									<div class="bi branch-info-stars branch-info-stars<?php echo e(floor($data->reviews_rating)); ?>"></div>
								</div>
								<div class="branch-reviews-reviewsCount">
									<?php echo e(count($data->reviews)); ?> <?php echo e(__('translations.reviews')); ?>

								</div>
							</div>
							<?php echo $__env->make('Frontend.templates.review_block', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
						</div>
					</div>
				</div>
			<?php endif; ?>
		<?php else: ?> 
			<div id="reviews">
				<div class="branch-block-head"><?php echo e(__('translations.reviews')); ?></div>
				<div class="branch-block-content">
					<div class="fx fxc bi branch-reviews-muted">
						<span><?php echo __('translations.only_reg_view'); ?></span>
					</div>
				</div>
			</div>
		<?php endif; ?>
	</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>	
	<script>
		document.addEventListener('DOMContentLoaded', function() {
			<?php if(Auth::check()): ?>
			$(document).on('click', '#branch_list_action', function(){
				var method = $(this).data('method'),
					user_id = '<?php echo e(Auth::user()->id); ?>';				

				$.ajax({
					url: '/ajax/getData',
					type: 'post',
	                dataType: 'json',
	                headers: {
	                    'X-CSRF-TOKEN': '<?php echo e(csrf_token()); ?>'
	                },
				    data: {action:'addToMyLabs', method: method, branch_id: '<?php echo e($data->id); ?>', user_id: user_id},
				    success: function(data) {
				    	var html = '';

				    	if (data) {
				    		if (method == 'add') {
				    			html += '<div class="branch-list-remove">';
				    			html += '<?php echo e(__('translations.added_to_labs')); ?><br>';
				    			html += '<div id="branch_list_action" class="btn btn-danger branch-list-action" data-branch="<?php echo e($data->id); ?>" data-method="remove"><?php echo e(__('translations.remove')); ?></div>';
				    			html += '</div>';
				    		}

				    		if (method == 'remove') {
				    			html += '<div class="branch-list-add">';
				    			html += '<div id="branch_list_action" class="btn branch-list-action" data-method="add">';
				    			html += '<span>+</span>';
				    			html += '<?php echo e(__('translations.add_to_labs')); ?>';
				    			html += '</div>';
				    			html += '</div>';
				    		}

				    		showPopup('<div class="success"><?php echo __('translations.success'); ?></div>');
				    	}

				    	$('#branch_my_lab').html(html);
				    }
				});
			});
			<?php endif; ?>
		}, !1);

		function showRegAlert() {
			showPopup('<div class="error"><?php echo __('translations.only_reg_view'); ?></div>');
		}

		function scrollToblock(id) {
			$('html, body').animate({
		        scrollTop: $('#'+id).offset().top
		    }, 500);
		}  
	</script>
	<?php if(Auth::check() && $data->geolocation && $data->geolocation != '{}'): ?>	
		<script src="https://maps.googleapis.com/maps/api/js?key=<?php echo e(setting('admin.google_api_maps_key')); ?>&callback=initMap"
	    async defer></script>
		<script>
			function initMap() {
				var position = JSON.parse('<?php echo $data->geolocation; ?>');				
				var center = {lat: parseFloat(position.lat), lng: parseFloat(position.lon)};
				var mapOptions = {
				    center: center,
				    zoom: 13,
				    mapTypeId: google.maps.MapTypeId.ROADMAP,				    
					mapTypeControl: false,
					streetViewControl: false,
					rotateControl: false
				};
				var map = new google.maps.Map(document.getElementById('map'), mapOptions);
				var marker = new google.maps.Marker({position: center, map: map});
			}
		</script>
    <?php endif; ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('Frontend/master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/d2l.sitepark.ua/resources/views/Frontend/pages/catalog/branch.blade.php ENDPATH**/ ?>