<?php 
	if ($type == config('types.clinic')) $account = 'Dentist';
	if ($type == config('types.lab')) $account = 'Lab';
?>

<p><?php echo e($name); ?> (<?php echo e($email); ?>) create new <?php echo e($account); ?> account at Dentist2Lab service!
<?php /**PATH /var/www/dentist2lab.com/resources/views/Frontend/emails/adm_registration.blade.php ENDPATH**/ ?>