<div class="breadcrumbs">
	<div class="content">
		<ul>
			<?php $last_crumb = array_pop($breadcrumbs); ?>
			<?php $__currentLoopData = $breadcrumbs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $breadcrumb): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
				<li><a href="<?php echo e($breadcrumb['href']); ?>"><?php echo e($breadcrumb['text']); ?></a></li>
			<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
			<li><?php echo e($last_crumb['text']); ?></li>
		</ul>
	</div>
</div><?php /**PATH /var/www/d2l.sitepark.ua/resources/views/Frontend/templates/breadcrumbs.blade.php ENDPATH**/ ?>