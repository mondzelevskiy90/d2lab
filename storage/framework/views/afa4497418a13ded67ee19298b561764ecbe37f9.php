<script src="/js/masked.js" defer></script>
<script>
	var d = document;

	var s = function (selector) {
		return d.querySelector(selector);
	}

	var active = 1;	
	
	var statuses = [],
		available_statuses = [],
		empties = [],
		selected = [];		

	d.addEventListener('DOMContentLoaded', function() {
		$('#patient_sex, #order_type, #branch_id, #numbering_system, #status').select2({
			minimumResultsForSearch: -1
		});

		$('#user_phone').mask('+1 (999) 999-9999');

		$('#service_id').select2();

		$('#ship_date').daterangepicker({
			singleDatePicker: true,
			showDropdowns: true,
			minDate: '<?php if(isset($data->ship_date)): ?><?php echo e($data->ship_date); ?><?php else: ?><?php echo e(date('m-d-Y')); ?><?php endif; ?>',
			minYear: <?php echo e(date('Y')); ?>,
			maxYear: <?php echo e(date('Y')); ?> + 1,
			locale: {
		        format: 'MM-DD-YYYY'
		    },
		});

		<?php if(isset($prescriptions) && count($prescriptions)): ?>
			svgDataRequest().done(function(data) {
			   	if (data) {
					for(i = 0; i < data.length; i++) {
		    			if(data[i]) {
		    				statuses[i] = [];
		    				selected[i] = [];

		    				for(k = 1; k <= 32; k++) {
		    					if (data[i][k]) {
		    						tooth_data = data[i][k].split('_');

									empties[k] = false;
									statuses[i][k] = false;
		    						selected[i][k] = false;

		    						if (tooth_data[1]) {
										statuses[i][k] = tooth_data[1];
										selected[i][k] = data[i][k];
		    							if (tooth_data[1] == 5 || tooth_data[1] == 6 || tooth_data[1] == 7) empties[k] = k;
		    						}
		    					}
		    				}
		    			}
		    		}
				}
				listenTooths();				
				showActivePrescription(active);
			}).fail(function() {
			    listenTooths();	
			});
		<?php else: ?> 
			listenTooths();		
		<?php endif; ?>		
	}, !1);

	<?php if(isset($prescriptions) && count($prescriptions)): ?>
	function svgDataRequest() {
		return $.ajax({
			url: '/ajax/getData',
			type: 'post',
            dataType: 'json',
            headers: {
                'X-CSRF-TOKEN': '<?php echo e(csrf_token()); ?>'
            },
		    data: {action:'getOrderPrescriptionSelecteds', order_id: '<?php echo e($data->id); ?>'},
		    success: function (data) {
		    	
			}
		});
	}
	<?php endif; ?>

	function getBranchAddreess() {
		var branch_id = $('#branch_id').val();

		if (branch_id && branch_id != 0) {
			$.ajax({
				url: '/ajax/getData',
				type: 'post',
                dataType: 'json',
                headers: {
                    'X-CSRF-TOKEN': '<?php echo e(csrf_token()); ?>'
                },
			    data: {action:'getBranchAddreess', branch_id: branch_id},
			    success: function(data) {
			    	if (data) $('#ship_address').val(data);
			    }
			});
		}
	}

	var file_index = <?php echo e($file_index); ?>;

	function addFileToCheckoutForm() {
		if ($(d).find('#order_file_input'+file_index).length) {
			file_index++;
			addFileToCheckoutForm();

			return false;
		}

		createFileFieldCheckoutForm(file_index);

		$(d).find('#order_file_input'+file_index).click();

		var order_files = $(d).find('.order-file-line');

		if (order_files.length > 4) $('#order_files_add').css('display', 'none');

		file_index++;
	}

	function createFileFieldCheckoutForm(index) {
		var html = '';

		html += '<div id="order_file_line'+index+'" class="fx fxb order-file-line order-file-icon">';			
		html += '<div id="order_file_name'+index+'" class="fx fxc order-file-name">';	
		html += '<span>Empty!</span>';			
		html += '<div class="order-file-remove" onclick="removeFileFromCheckoutForm('+index+')" title="<?php echo e(__('translations.delete')); ?>"><div></div></div>';
		html += '<input type="hidden" id="order_files_save'+index+'" name="order_files['+index+']" value="">';	
		html += '<input type="file" id="order_file_input'+index+'" onchange="checkFileFieldCheckoutForm('+index+');">';
		html += '</div>';
		html += '</div>';

		$('#order_files').append(html);
	}

	function removeFileFromCheckoutForm(index) {
		$(d).find('#order_file_line'+index).remove();

		var order_files = $(d).find('.order-file-line');

		if (order_files.length <= 4) $('#order_files_add').css('display', 'block');
	}

	function checkFileFieldCheckoutForm(index) {
		var file = $(d).find('#order_file_input'+index);
		var value = file.prop('files')[0];

		if (!value || !file.val()) {
			removeFileFromCheckoutForm(index);

			return false;
		}

		if (value.size > 10000000) {
			showPopup('<?php echo e(__('translations.checkout_file_error')); ?>');
			removeFileFromCheckoutForm(index);

			return false;
		}

		let form_data = new FormData();

		form_data.append('order_file', value);	
		form_data.append('file_index', index);			
		form_data.append('action', 'saveCheckoutFile');		

		$.ajax({
			url: '/ajax/getData',
			type: 'post',
            dataType: 'json',
            processData: false,
        	contentType: false,
            headers: {
                'X-CSRF-TOKEN': '<?php echo e(csrf_token()); ?>'
            },
            data: form_data,
            success: function(data) {
            	if (data) {
            		$(d).find('#order_files_save'+index).val(data.file);

            		var html = '<a href="/storage/'+data.file+'" target="_blank" class="fx fxc">';

            		if (data.extension == 'jpg' || data.extension == 'jpeg' || data.extension == 'JPEG' || data.extension == 'JPG' || data.extension == 'png' || data.extension == 'PNG') {
						html += '<img src="/storage/'+data.file+'" width="100%" height="auto" />';
            		} else {
            			html += '<span>.'+data.extension+'</span>';
            		}

            		html += '</a>';

            		$('#order_file_name'+index+' span').replaceWith(html);
            	}
            }
		});			
	}

	var prescription_index = <?php echo e($prescription_index); ?>;
	var prescription_colors = {
		1: '#ffbab0',
		2: '#F2EDA2',
		3: '#BBDCEF',
		4: '#CAE8CE',
		5: '#F7C69C',
		6: '#f3d9da',
		7: '#ffb6ff',
		8: '#b58585',
		9: '#d2d222',
		10: '#DADADA'
	};

	function addPrescriptionBlock() {
		if ($(d).find('#order_prescription-block'+prescription_index).length) {
			prescription_index++;
			addPrescriptionBlock();
			
			return false;
		}
		var service = $('#service_id').val();
		var system = $('#numbering_system').val();
		var branch_id = $('#to_branch_id').val();

		var $data = $.ajax({
			url: '/ajax/getData',
			type: 'post',
            dataType: 'json',
            headers: {
                'X-CSRF-TOKEN': '<?php echo e(csrf_token()); ?>'
            },
		    data: {action:'getOrderPrescription', service: service, system: system, branch_id: branch_id},
		    success: function (data) {
		    	if (data) {
		    		var html = '';
		    		
					html += '<div id="order_prescription-block'+prescription_index+'" class="fx fxb panel-body">';
					html += '	<div class="panel">';
					html += '		<div class="btn btn-danger delete-order-prescription" onclick="removePrescriptionFromOrder('+prescription_index+');"><?php echo e(__('translations.delete')); ?></div>';
					html += '		<div class="panel-head"><?php echo e(__('translations.order_prescription')); ?> #'+prescription_index+'</div>';
					html += '		<div id="order_prescription'+prescription_index+'" class="panel-block">';
					html += '			<div class="form-line">';
					html += '				<label><?php echo e(__('translations.numbering_system')); ?>: <strong>'+data.numbering_system_name+'</strong></label>';
					html += '				<input type="hidden" name="prescriptions['+prescription_index+'][numbering_system_id]" value="'+data.numbering_system_id+'">';
					html += '			</div>';
					html += '			<div class="form-line">';
					html += '				<label><?php echo e(__('translations.service_name')); ?>: <strong>'+data.service_name+' ('+data.parent_name+')</strong></label>';
					html += '				<input type="hidden" name="prescriptions['+prescription_index+'][service_id]" value="'+data.service_id+'">';
					html += '			</div>';

					if (data.min_days) {
						html += '				<input type="hidden" class="prescription-service-min-days" value="'+data.min_days+'"/>';
					}
					
					
					if (data.fields) {
						for (index in data.fields) {
							html += '<div class="form-line">';	
							html += '<div class="prescription-line-head">';
							html += '<label for="prescription'+data.fields[index]['id']+'" class="prescription-name">'+data.fields[index]['name']+(data.fields[index]['help'] ? '<span class="prescription-line-help" onclick="showFieldHelp('+data.fields[index]['id']+')">?</span>' : '')+'</label>';

							if (data.fields[index]['description']) {
								html += '<p class="prescription-desc">'+data.fields[index]['description']+'</p>';
							}

							html += '</div>';
							html += '<div class="fx fac prescription-line-content">';

							if (data.fields[index]['question'] && data.fields[index]['question'] == 1) {
								html += '<div class="prescription-line-question">';
								html += '<select id="select_prescription_'+prescription_index+'_'+data.fields[index]['id']+'" class="form-input select-select2-select" onchange="showPrescriptionFieldContent(\''+prescription_index+'_'+data.fields[index]['id']+'\');">';
								html += '<option value="no" default><?php echo e(__('translations.no')); ?></option>';
								html += '<option value="yes"><?php echo e(__('translations.yes')); ?></option>';
								html += '</select>';
								html += '</div>';
							}

							if (data.fields[index]['image']) { 
								html += '<div class="prescription-image">';
								html += '<img src="<?php echo e(url('/').'/storage/'); ?>'+data.fields[index]['image']+'">';
								html += '</div>';
							}

							html += '<div id="inputs_prescription_'+prescription_index+'_'+data.fields[index]['id']+'" class="fx prescription-inputs '+(data.fields[index]['image'] ? 'prescription-inputs-half' : '')+'" style="display:'+(data.fields[index]['question'] && data.fields[index]['question'] == 1 ? 'none' : 'flex')+'">';

							if (data.fields[index]['field_type'] == 'text') {
								html += '<input type="text" name="prescriptions['+prescription_index+'][fields]['+data.fields[index]['id']+']" id="prescription_'+prescription_index+'_'+data.fields[index]['id']+'" value="" placeholder="'+data.fields[index]['name']+'" maxlength="255" '+(data.fields[index]['required'] == 1 ? 'class="form-input form-input-required" required="required"' : 'class="form-input"')+'>';
							}

							if (data.fields[index]['field_type'] == 'select' && data.fields[index]['values']) {
								html += '<select name="prescriptions['+prescription_index+'][fields]['+data.fields[index]['id']+']" id="prescription_'+prescription_index+'_'+data.fields[index]['id']+'" '+(data.fields[index]['required'] == 1 ? 'class="form-input select-select2-select form-input-required" required="required"' : 'class="form-input select-select2-select"')+'>';
								
								for (index2 in data.fields[index]['values']) {
									html += '<option value="'+data.fields[index]['values'][index2]['value']+'" '+(data.fields[index]['values'][index2]['selected'] == 1 ? 'selected="selected"' : '')+'>'+data.fields[index]['values'][index2]['value']+'</option>';
								}

								html += '</select>';
							}

							if (data.fields[index]['field_type'] == 'checkbox' && data.fields[index]['values']) {
								for (index3 in data.fields[index]['values']) {
									html += '<div class="form-checkbox">';	

									if (data.fields[index]['values'][index3]['image']) {
										html += '<img src="<?php echo e(url('/').'/storage/'); ?>'+data.fields[index]['values'][index3]['image']+'">';
									}

									html += '<input type="checkbox" name="prescriptions['+prescription_index+'][fields]['+data.fields[index]['id']+']['+data.fields[index]['values'][index3]['id']+']" id="prescription_'+prescription_index+'_'+data.fields[index]['id']+'_'+data.fields[index]['values'][index3]['id']+'" value="'+data.fields[index]['values'][index3]['value']+'" '+(data.fields[index]['values'][index3]['selected'] == 1 ? 'checked="checked"' : '')+'>';
									html += '<label for="prescription_'+prescription_index+'_'+data.fields[index]['id']+'_'+data.fields[index]['values'][index3]['id']+'">'+data.fields[index]['values'][index3]['value']+'</label>';
									html += '</div>';
								}
							}

							if (data.fields[index]['field_type'] == 'radio' && data.fields[index]['values']) {
								for (index4 in data.fields[index]['values']) {
									html += '<div class="form-checkbox">';	

									if (data.fields[index]['values'][index4]['image']) {
										html += '<img src="<?php echo e(url('/').'/storage/'); ?>'+data.fields[index]['values'][index4]['image']+'">';
									}

									html += '<input type="radio" name="prescriptions['+prescription_index+'][fields]['+data.fields[index]['id']+']" id="prescription_'+prescription_index+'_'+data.fields[index]['id']+'_'+data.fields[index]['values'][index4]['id']+'" value="'+data.fields[index]['values'][index4]['value']+'" '+(data.fields[index]['values'][index4]['selected'] == 1 ? 'checked="checked"' : '')+'>';
									html += '<label for="prescription_'+prescription_index+'_'+data.fields[index]['id']+'_'+data.fields[index]['values'][index4]['id']+'">'+data.fields[index]['values'][index4]['value']+'</label>';
									html += '</div>';
								}
							}
									
							html += '</div>';
							html += '</div>';
							html += '</div>';
						}
					}

					html += '			<div class="form-line">';	
					html += '				<label for="prescription_'+prescription_index+'_comment"><?php echo e(__('translations.comment')); ?></label>';
					html += '				<textarea name="prescriptions['+prescription_index+'][comment]" id="prescription_'+prescription_index+'_comment" class="form-inut" placeholder="<?php echo e(__('translations.comment')); ?>" maxlength="1000"></textarea>';
					html += '			</div>';
					html += '		</div>';

					if (data.prescription_description) {
						html += '		<div class="panel-head panel-subhead panel-head-collapse panel-head-collapsed"><?php echo e(__('translations.prescription_instruction')); ?></div>';
						html += '		<div class="fx fxb panel-block panel-block-collapsed">';
						html += data.prescription_description;
						html += '		</div>';
					}

					html += '	</div>';
					html += '</div>';

					var html2 = '';

					var color_index = prescription_index;

					if (prescription_index > 10) color_index = 1;

					var color = prescription_colors[color_index];

					html2 += '<div id="order_prescription-service'+prescription_index+'" class="fx fxb order-prescription-service">';
					html2 += '  <input type="hidden" name="prescriptions['+prescription_index+'][price]" id="order_prescription_price'+prescription_index+'" class="order-prescription-price-input" value="0.00">';
					html2 += '  <input type="hidden" name="prescriptions['+prescription_index+'][price_for]" id="order_prescription_price_for'+prescription_index+'" value="'+data.price_for+'">';
					html2 += '  <input type="hidden" id="order_prescription_available_statuses'+prescription_index+'" value="'+data.available_statuses+'">';
					html2 += '  <input type="hidden" name="prescriptions['+prescription_index+'][price_order_date]" id="order_prescription_price_order_date'+prescription_index+'" value="'+data.price+'">';
					html2 += '	<div id="order_prescription_name'+prescription_index+'" class="fx fac order-prescription-name" onclick="showActivePrescription('+prescription_index+')">';
					html2 += ' <div class="order-prescription-check" title="'+data.service_name+'"></div>'
					html2 += '  <div id="order_prescription_color'+prescription_index+'" class="order-prescription-color" data-color="'+color+'" style="background-color:'+color+'" title="'+data.service_name+'"></div>';
					html2 += '<div style="width: calc(100% - 55px);">#'+prescription_index+' <strong>'+data.service_name+'</strong></div>';
					html2 += '  </div>';						
					html2 += '  <div class="order-prescription-scroll" onclick="scrollToblock(\'order_prescription-block'+prescription_index+'\')" title="'+data.service_name+'"><?php echo e(__('translations.show_prescription')); ?></div>';
					html2 += '	<div class="btn btn-danger delete-order-prescription" onclick="removePrescriptionFromOrder('+prescription_index+');"><?php echo e(__('translations.delete')); ?></div>';
					html2 += '<input type="text" name="prescriptions['+prescription_index+'][selecteds]" id="prescriptions_selecteds_'+prescription_index+'" class="prescription-selecteds" value="" required>';
					html2 += '<div class="prescription-selecteds-text"><?php echo e(__('translations.selected_items')); ?>: <span id="prescriptions_selecteds_text_'+prescription_index+'"></span></div>';

					if (data.available_options && data.available_options != 0) {
						html2 += '<div class="fx prescription-selecteds-options">';
							html2 += '<span><?php echo e(__('translations.select')); ?>:</span>';
							if (data.available_options == 1 || data.available_options == 3) {
								html2 += '<div class="order-option-check" id="order_option_check1" onclick="selectAllTooths(1, '+prescription_index+')">Lower</div>';
							}

							if (data.available_options == 2 || data.available_options == 3) {
								html2 += '<div class="order-option-check" id="order_option_check2" onclick="selectAllTooths(2, '+prescription_index+')">Upper</div>';
							}

						html2 += '</div>';
					}
					
					html2 += '</div>';

					$('#order_prescription_services').append(html2);
					$('#order_prescriptions').append(html);
					$('#create_order_button').css('display', 'block');
					$('#numbering_system_mask').css('display', 'none');
					$('.order-prescription-servicesHead').css('display', 'block');

					$('.select-select2-select').select2({
						minimumResultsForSearch: -1
					});

					showActivePrescription(prescription_index);
					selected[prescription_index] = [];
					statuses[prescription_index] = [];

					prescription_index++;
		    	}
		    }
		});
	}

	function removePrescriptionFromOrder(index) {
		$(d).find('#order_prescription-service'+index).remove();
		$(d).find('#order_prescription-block'+index).remove();

		if (selected[index]) delete selected[index];
		if (statuses[index]) delete statuses[index];

		rebuildSvg();

		if (!$('.order-prescription-service').length) {
			$('#order_prescription_total').html(0);
			$('#advance_price').val(0);
			$('#create_order_button').css('display', 'none');
			$('.order-prescription-servicesHead').css('display', 'none');
			$('#numbering_system_mask').css('display', 'flex');
		}
	}	

	function getNumberingSystemSvg() {
		var system = $('#numbering_system').val();

		if (system) {
			$.ajax({
				url: '/ajax/getData',
				type: 'post',
                dataType: 'json',
                headers: {
                    'X-CSRF-TOKEN': '<?php echo e(csrf_token()); ?>'
                },
			    data: {action:'getNumberingSystemSvg', system: system},
			    success: function (data) {
			    	if (data) {
			    		$('#numbering_system_block').html(data);

			    		listenTooths();
			    	}
			    }
			});
			
    		active = 1;
    		available_statuses = [];
			statuses = [];
			empties = [];
			selected = [];

			$('#order_prescription_total').html(0);
			$('#advance_price').val(0);
			$('#order_prescription_services').html('');
    		$('#order_prescriptions').html('');
    		$('#create_order_button').css('display', 'none');
    		$('.order-prescription-servicesHead').css('display', 'none');
    		$('#numbering_system_mask').css('display', 'flex');

    		prescription_index = 1;
		}
	}	

	function scrollToblock(id) {
		$('html, body').animate({
	        scrollTop: $(d).find('#'+id).offset().top
	    }, 500);
	} 

	function showActivePrescription(index) {
		$(d).find('.order-prescription-name').removeClass('order-prescription-name-act');
		$(d).find('#order_prescription_name'+index).addClass('order-prescription-name-act');

		var statuses_str = $(d).find('#order_prescription_available_statuses'+index).val();

		active = index;
		available_statuses = statuses_str.split(',');

		rebuildSvg();
	}

	function initTooth(num) {
		num = parseInt(num);

		statuses[active][num] ? statuses[active][num]++ : statuses[active][num] = 2;

		if (!available_statuses[statuses[active][num]]) statuses[active][num]++;
		
		if(statuses[active][num] == 3 && (num == 16 || num == 32 || !available_statuses[statuses[active][num]])) statuses[active][num]++;

		if(statuses[active][num] == 4 && (num == 1 || num == 17 || !available_statuses[statuses[active][num]])) statuses[active][num]++; 

		if(statuses[active][num] == 5) {
			if (!statuses[active][num + 1] || num + 1 == 16 || num + 1 == 32) {
				statuses[active][num]++;
			} else if (statuses[active][num + 1] && statuses[active][num + 1] != 3 && statuses[active][num + 1] != 5) {
				statuses[active][num]++;
			}
		} 

		if(statuses[active][num] == 6) {
			if (!statuses[active][num - 1] || num - 1 == 1 || num - 1 == 17) {
				statuses[active][num] = 7;
			} else if (statuses[active][num - 1] && statuses[active][num - 1] != 4 && statuses[active][num - 1] != 6) {
				statuses[active][num]++;
			} 
		}

		if(statuses[active][num] > 7) statuses[active][num] = 1;

		statuses[active][num] == 5 || statuses[active][num] == 6 || statuses[active][num] == 7 ? empties[num] = num : delete empties[num];

		addToothToService(num);
	}

	function addToothToService(num) {
		var values = selected[active],
			tooth = s('#tooth_'+num).getAttribute('data-tooth');

		statuses[active][num] && statuses[active][num] != 1 ? values[num] = tooth+'_'+statuses[active][num]+'_'+s('#tooth_'+num+' text').innerHTML : delete values[num];

		rebuildSvg();
		showSelectedsText();

		s('#prescriptions_selecteds_'+active).value = values.join(',');
	}

	function rebuildSvg() {	
		//clear all selections	
		for (i = 1; i <= 32; i++) {
			s('#tooth_body_'+i).style.fill = '#fff';

			if (s('#tooth_prev_'+i)) s('#tooth_prev_'+i).style.display = 'none';
			if (s('#tooth_next_'+i)) s('#tooth_next_'+i).style.display = 'none';
			
			s('#tooth_point_'+i).style.display = 'none';
			s('#tooth_empty_'+i).style.display = 'none';
		}

		if (!d.querySelectorAll('.order-prescription-service').length) return false;

		//show selected
		for (i = 1; i <= selected.length; i++) {
			var indx = i <= 10 ? i : 1;
			var color = prescription_colors[indx];
			var lined = [];

			if (selected[i]) {
				var show = false;
				var end = false;				

				for (k = 1; k <= 32; k++) {	
					if (selected[i][k]) {
						var tooth_data = splitData(selected[i][k], '_');

						if (empties[k]) {
							s('#tooth_empty_'+k).style.display = 'block';
							s('#tooth_body_'+k).style.fill = '#fff';

							if (tooth_data && tooth_data[1]) {
								if (tooth_data[1] == 5) s('#tooth_next_'+k).style.display = 'block';
								if (tooth_data[1] == 6) s('#tooth_prev_'+k).style.display = 'block';
							}
						} else {
							if (tooth_data && tooth_data[1]) {								
								s('#tooth_empty_'+k).style.display = 'none';
							
								if (tooth_data[1] == 3) {
									s('#tooth_point_'+k).style.display = 'block';
									s('#tooth_next_'+k).style.display = 'block';
									show = true;
									end = false;				
								}

								if (tooth_data[1] == 4) {
									s('#tooth_prev_'+k).style.display = 'block';
									s('#tooth_point_'+k).style.display = 'block';
									show = false;
									end = true;
								}		

								s('#tooth_body_'+k).style.fill = color;
							}
						}
					}

					if (show) lined[k] = k;
				}

				if (end) {
					for (j = 1; j <= 32; j++) {	
						if (lined[j]) {
							if (!empties[j]) s('#tooth_body_'+j).style.fill = color;

							if (j != 16 && j != 32) s('#tooth_next_'+j).style.display = 'block';
						};
					}
				}
			}
		}		
		//show active color
		if (selected[active]) {
			var active_color = s('#order_prescription_color'+active).getAttribute('data-color');
			for (i = 1; i <= selected[active].length; i++) {
				if (selected[active][i]) {
					var tooth_data = splitData(selected[active][i], '_');

					if (tooth_data && tooth_data[1] && tooth_data[1] != 7) {
						s('#tooth_body_'+i).style.fill = active_color;
					}
				}				
			}
		}
		//calculate prices
		setPrices();
	}	

	function setPrices() {
		for (i = 1; i <= selected.length; i++) {
			var start = false,
				price = 0,
				count = 0;

			if (selected[i]) {				
				var	price_for = parseInt(s('#order_prescription_price_for'+i).value),
					price_order_date = parseFloat(s('#order_prescription_price_order_date'+i).value);

				for (k = 1; k <= 32; k++) {
					if (start) count++;	

					if (selected[i][k]) {						
						var tooth_data = splitData(selected[i][k], '_');

						if (tooth_data && tooth_data[1]) {	
						    if (tooth_data[1] == 2) count++;
						    if (tooth_data[1] == 5) count++;
						    if (tooth_data[1] == 6) count++;

							if (tooth_data[1] == 3) {								
								start = true;
								count++;				
							}							

							if (tooth_data[1] == 4) {								
								start = false;
							}
						}
					}
				}

				if (price_for == 2 && count != 0) count = 1;

				price = price_order_date * count;

				if (price < 0) price = 0;

				s('#order_prescription_price'+i).value = price.toFixed(2); 
			}
		}

		setTotalPrice();
	}

	function setTotalPrice() {
		var prices = d.querySelectorAll('.order-prescription-price-input');
		var total = 0;

		prices.forEach(function(el){
			var price = parseFloat(el.value);

			total = total + price;
		});

		if (total < 0) total = 0;

		s('#order_prescription_total').innerHTML = total.toFixed(2);
		s('#advance_price').value = total.toFixed(2);
	}

	function listenTooths() {
		var tooths = d.querySelectorAll('.tooth-item');

		tooths.forEach(function(tooth){
			tooth.addEventListener('mouseenter', function(){
				this.style.fontWeight = 'bold';
			});

			tooth.addEventListener('mouseleave', function(){
				this.style.fontWeight = 'normal';
			});

			tooth.addEventListener('click', function(){
				var num = this.getAttribute('data-num');

				initTooth(num);
			});
		});
	}

	function showSelectedsText() {
		if (statuses[active]) {			
			var text = [];
			var lined = false;
			var need_more = false;

			for(i = 1; i <= 32; i++) {
				var index = i;

				if (statuses[active][i]) {
					if (statuses[active][i] == 2) text[i] = s('#tooth_'+index+' text').innerHTML;

					if (statuses[active][i] == 5) {
                        if (statuses[active][i - 1] && statuses[active][i - 1] == 5) {
                            
                        } else {
                            lined = s('#tooth_'+index+' text').innerHTML + '-';
                            need_more = true;
                        }
                    }

					if (statuses[active][i] == 3) {
						if (!need_more) lined = s('#tooth_'+index+' text').innerHTML + '-';
					}
					if (statuses[active][i] == 4) {
						if (statuses[active][i + 1] && statuses[active][i + 1] == 6) {
							need_more = true;
						} else {
							need_more = false;
						}

						if (!need_more) {
							lined += s('#tooth_'+index+' text').innerHTML;
							text[i] = lined.replace('false', '?-');
							lined = false;
						}
					}

					if (statuses[active][i] == 6) {
                        if (statuses[active][i + 1] && statuses[active][i + 1] == 6) {
                            need_more = true;
                        } else {
                            lined += s('#tooth_'+index+' text').innerHTML;
							text[i] = lined.replace('false', '?-');
							lined = false;
                            need_more = false;
                        }
                    }
				}
			}
	
			s('#prescriptions_selecteds_text_'+active).innerHTML = text.filter(Boolean).join(',');
		}
	}

	function splitData(str, sep) {
		return str.split(sep);
	}

	function showTutorial() {
		$.colorbox({
			html: '<div class="popup-content">Tutorial here!</div>',
			onComplete : function() {
				$(this).colorbox.resize();
			}
		});
	}

	function showFieldHelp(id) {
		return $.ajax({
			url: '/ajax/getData',
			type: 'post',
            dataType: 'json',
            headers: {
                'X-CSRF-TOKEN': '<?php echo e(csrf_token()); ?>'
            },
		    data: {action:'showFieldHelp', id: id},
		    success: function (data) {
		    	showPopup(data, true);
			}
		});
	}

	function showPrescriptionFieldContent(id) {
		var display = 'none';

		if ($(document).find('#select_prescription_'+id).val() == 'yes') {
			var display = 'flex';
		} else {
			$($(document).find('#inputs_prescription_'+id+' input[type=checkbox]')).each(function(i){
				$(this).prop('checked', false);
			});
		}

		$(document).find('#inputs_prescription_'+id).css('display', display);
	}

	function selectAllTooths(type, index) {
		var start = 1;
		var end = 16;

		if (type == 1) {
			start = 17;
			end = 32;
		}

		if ($(d).find('#order_option_check'+type).hasClass('order-option-checkAct')) {
			$(d).find('#order_option_check'+type).removeClass('order-option-checkAct');

			for (i = 1; i <= 32; i++) {
				if (i >= start && i <= end && !empties[i]) {
					delete selected[index][i];
					delete statuses[index][i];
				}
			}
		} else {
			$(d).find('#order_option_check'+type).addClass('order-option-checkAct');

			for (i = 1; i <= 32; i++) {
				if (i >= start && i <= end && !empties[i]) {
					statuses[index][i] = 2;

					var tooth = s('#tooth_'+i).getAttribute('data-tooth');

					statuses[index][i] && statuses[index][i] != 1 ? selected[index][i] = tooth+'_'+statuses[index][i]+'_'+s('#tooth_'+i+' text').innerHTML : delete selected[index][i];
				}
			}
		}

		rebuildSvg();
		showSelectedsText();

		s('#prescriptions_selecteds_'+index).value = selected[index].join(',');
	}

	function checkOrderFormCustomFields(e) {
		var min_days = $(document).find('.prescription-service-min-days');
		
		if (min_days) {
			var max_days = [];
			var ship_date = new Date(($('#ship_date').val()).replace(/-/g, "/"));
			var current_date = new Date();

			min_days.each(function(i){				
				max_days[i] = parseInt(min_days[i].value);
			});

			var max_day = Math.max.apply(null, max_days);
			var diff_date = Math.ceil((ship_date - current_date) / 1000 / 60 / 60 / 24);

			if (!max_day) max_day = 0;
			if (!diff_date) diff_date = 0;

			if (diff_date < max_day) {	
				e.preventDefault();		
			    var available_date = current_date.setDate(current_date.getDate() + max_day);
				var d = new Date(available_date);				
				var day = d.getDate() > 9 ? d.getDate() : '0' + d.getDate();
				var month = (d.getMonth() + 1) > 9 ? (d.getMonth() + 1) : '0' + (d.getMonth() + 1);
				var year = d.getFullYear();
				var text = '<?php echo e(__('translations.select_other_checkout_date')); ?>'; 

				d = month + '-' + day + '-' + year;

				text = text.replace('%s', d);
			
				showPopup(text);

				return false;
			}
		}
	}
</script><?php /**PATH /var/www/dentist2lab.com/resources/views/Frontend/templates/checkout_scripts2.blade.php ENDPATH**/ ?>