<?php $__env->startSection('breadcrumbs'); ?>
	<?php echo $__env->make('Frontend.templates.breadcrumbs', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
	<h1><?php echo e($seo->name); ?></h1>
	<div class="page-text">
		<?php if($_SERVER['REQUEST_URI'] == '/article/privacy-policy'): ?>
			<a href="/article/privacy-policy#app-policy">Dentist2Lab App Privacy Policy &rarr;</a>
			<br><br>
		<?php endif; ?>
		<?php echo $seo->description; ?>

	</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('Frontend/master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/dentist2lab.com/resources/views/Frontend/pages/article.blade.php ENDPATH**/ ?>