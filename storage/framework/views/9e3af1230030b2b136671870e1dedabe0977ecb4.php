<!DOCTYPE html>
<html lang="<?php echo e(app()->getLocale()); ?>">
	<?php echo $__env->make('Frontend.parts.head', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
	<body>
		<div class="wrapper">
			<header>
				<?php echo $__env->make('Frontend.parts.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
			</header>			
			<?php echo $__env->yieldContent('after_head'); ?>
			<?php echo $__env->yieldContent('breadcrumbs'); ?>
			<section class="content page-content">
				<?php echo $__env->yieldContent('content'); ?>
			</section>
			<?php echo $__env->yieldContent('pre_footer'); ?>
			<?php echo $__env->make('Frontend.parts.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
		</div>		
	</body>
</html><?php /**PATH /var/www/dentist2lab.com/resources/views/Frontend/master.blade.php ENDPATH**/ ?>