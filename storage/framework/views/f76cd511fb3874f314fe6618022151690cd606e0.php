<?php $__env->startSection('page_title', __('voyager::generic.'.(isset($data->id) ? 'edit' : 'add')).' '.$data->name); ?>

<?php $__env->startSection('css'); ?>
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('page_header'); ?>
    <h1 class="page-title">
        <i class="voyager-edit"></i>
        <?php echo e(__('voyager::generic.'.(isset($data->id) ? 'edit' : 'add')).' '.$data->name); ?>

    </h1>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="page-content container-fluid">
        <form class="form-edit-add" role="form" action="<?php if(!is_null($data->id)): ?><?php echo e(route('voyager.'.$type.'.update', [$data->id, $requests])); ?><?php else: ?><?php echo e(route('voyager.'.$type.'.store', [$requests])); ?><?php endif; ?>" method="POST" enctype="multipart/form-data" autocomplete="off">            
            <?php if(isset($data->id)): ?>
                <?php echo e(method_field("PUT")); ?>

            <?php endif; ?>
            <?php echo e(csrf_field()); ?>


            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-bordered">  
                        <div class="panel-body">                 
                            <?php if(count($errors) > 0): ?>
                                <div class="alert alert-danger">
                                    <ul>
                                        <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <li><?php echo e($error); ?></li>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </ul>
                                </div>
                            <?php endif; ?>                                                    
                            <div class="form-group">
                                <label for="status"><?php echo e(__('voyager::generic.status')); ?></label>
                                <select class="form-control" name="status">        
                                    <option value="1" <?php if(isset($data->status) && $data->status == 1): ?> selected <?php endif; ?>>Active</option>       
                                    <option value="2" <?php if(isset($data->status) && $data->status == 2): ?> selected <?php endif; ?>>Disactive</option>                                      
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="name"><?php echo e(__('voyager::generic.name')); ?></label>
                                <input type="text" class="form-control" id="name" name="name" placeholder="<?php echo e(__('voyager::generic.name')); ?>" value="<?php echo e(old('name', $data->name ?? '')); ?>">
                            </div>                 
                            <div class="form-group">
                                <label for="sort_order"><?php echo e(__('voyager::generic.sort_order')); ?></label>
                                <input type="text" class="form-control" id="sort_order" name="sort_order" placeholder="<?php echo e(__('voyager::generic.sort_order')); ?>" value="<?php echo e(old('sort_order', $data->sort_order ?? 0)); ?>">
                            </div> 
                            <div class="form-group">
                                <label for="parent_id">Parent (leave empty if main)</label>
                                <select class="form-control" name="parent_id">        
                                    <option value="0" <?php if(isset($data->parent_id) && $data->parent_id == 0): ?> selected <?php endif; ?>>---- </option>
                                    <?php $__currentLoopData = $parents; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $parent): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
                                        <option value="<?php echo e($parent->id); ?>" <?php if(isset($data->parent_id) && $data->parent_id == $parent->id): ?> selected <?php endif; ?>><?php echo e($parent->name); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>                 
                                </select>
                            </div>                           
                        </div>
                    </div>
                </div>                
            </div>

            <button type="submit" class="btn btn-primary pull-right save">
                <?php echo e(__('voyager::generic.save')); ?>

            </button>
        </form>

       
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>
    <script>
        $('document').ready(function () {
            $('.toggleswitch').bootstrapToggle();
        });
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('voyager::master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/d2l.sitepark.ua/resources/views/Backend/industries/edit-add.blade.php ENDPATH**/ ?>