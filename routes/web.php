<?php
$backEndPrefix = 'Backend\\';
$frontEndPrefix = 'Frontend\\';

Route::get('/', ['uses' => $frontEndPrefix.'HomeController@index', 'as' => 'home']);
Route::get('/createnotifications', ['uses' => $frontEndPrefix.'NotificationsController@createnotifications', 'as' => 'createnotifications']);
Route::any('login', ['uses' => $frontEndPrefix.'LoginController@index', 'as' => 'login']);
Route::get('logout', ['uses' => $frontEndPrefix.'LoginController@logout', 'as' => 'logout']);
Route::any('forgot', ['uses' => $frontEndPrefix.'LoginController@forgot', 'as' => 'forgot']);
Route::any('registration', ['uses' => $frontEndPrefix.'RegistrationController@index', 'as' => 'registration']);
Route::get('sitemap', ['uses' => $frontEndPrefix.'SitemapController@index', 'as' => 'sitemap']);
Route::get('sitemapxml', ['uses' => $frontEndPrefix.'SitemapController@sitemapxml', 'as' => 'sitemapxml']);
Route::get('article/{slug}', ['uses' => $frontEndPrefix.'ArticleController@index', 'as' => 'article']);
Route::get('catalog', ['uses' => $frontEndPrefix.'CatalogController@index', 'as' => 'catalog']);
Route::get('catalog/{slug}', ['uses' => $frontEndPrefix.'CatalogController@industry', 'as' => 'catalog.industry']);
Route::get('catalog/{slug1}/{slug2}', ['uses' => $frontEndPrefix.'CatalogController@type', 'as' => 'catalog.industry.type']);
Route::post('api/gettoken', ['uses' => $frontEndPrefix.'ApiController@gettoken']);
Route::post('api/{method}', ['uses' => $frontEndPrefix.'ApiController@method']);

Route::group(['middleware' => 'auth'], function() use ($frontEndPrefix) {
    
    Route::get('account', ['uses' => $frontEndPrefix.'AccountController@index', 'as' => 'account']);
    Route::post('account/dashboard_setup', ['uses' => $frontEndPrefix.'AccountController@setup', 'as' => 'dashboard_setup']);
    Route::group(['middleware' => 'accountrolecheck'], function() use ($frontEndPrefix) {
        //account settings
        Route::any('account/settings', ['uses' => $frontEndPrefix.'AccountController@settings', 'as' => 'settings']);
        //account prices  
        Route::get('account/prices', ['uses' => $frontEndPrefix.'PricesController@index', 'as' => 'prices']);
        Route::any('account/prices/create', ['uses' => $frontEndPrefix.'PricesController@create', 'as' => 'prices.create']);
        Route::any('account/prices/edit/{id}', ['uses' => $frontEndPrefix.'PricesController@edit', 'as' => 'prices.edit']);
        Route::any('account/prices/editcustom/{id}', ['uses' => $frontEndPrefix.'PricesController@editcustom', 'as' => 'prices.editcustom']);
        Route::any('account/prices/delete/{id}', ['uses' => $frontEndPrefix.'PricesController@delete', 'as' => 'prices.delete']);  
        //account branches
        Route::get('account/branches', ['uses' => $frontEndPrefix.'BranchesController@index', 'as' => 'branches']);
        Route::any('account/branches/create', ['uses' => $frontEndPrefix.'BranchesController@create', 'as' => 'branches.create']);
        Route::any('account/branches/store', ['uses' => $frontEndPrefix.'BranchesController@store', 'as' => 'branches.store']);
        Route::any('account/branches/edit/{id}', ['uses' => $frontEndPrefix.'BranchesController@edit', 'as' => 'branches.edit']);
        Route::any('account/branches/update/{id}', ['uses' => $frontEndPrefix.'BranchesController@update', 'as' => 'branches.update']);
        Route::any('account/branches/delete/{id}', ['uses' => $frontEndPrefix.'BranchesController@delete', 'as' => 'branches.delete']);
        //account staff
        Route::get('account/staff', ['uses' => $frontEndPrefix.'StaffController@index', 'as' => 'staff']);
        Route::any('account/staff/create', ['uses' => $frontEndPrefix.'StaffController@create', 'as' => 'staff.create']);
        Route::any('account/staff/edit/{id}', ['uses' => $frontEndPrefix.'StaffController@edit', 'as' => 'staff.edit']);
        Route::any('account/staff/delete/{id}', ['uses' => $frontEndPrefix.'StaffController@delete', 'as' => 'staff.delete']);
        //account portfolio
        Route::get('account/portfolios', ['uses' => $frontEndPrefix.'PortfoliosController@index', 'as' => 'portfolios']);
         Route::any('account/portfolios/create', ['uses' => $frontEndPrefix.'PortfoliosController@create', 'as' => 'portfolio.create']);
        Route::any('account/portfolios/edit/{id}', ['uses' => $frontEndPrefix.'PortfoliosController@edit', 'as' => 'portfolio.edit']);
        Route::any('account/portfolios/delete/{id}', ['uses' => $frontEndPrefix.'PortfoliosController@delete', 'as' => 'portfolio.delete']);
        //account reviews
        Route::get('account/reviews', ['uses' => $frontEndPrefix.'ReviewsController@index', 'as' => 'reviews']);
        Route::get('account/reviews/edit/{id}', ['uses' => $frontEndPrefix.'ReviewsController@edit', 'as' => 'reviews.edit']);
        //account orders
        Route::get('account/orders', ['uses' => $frontEndPrefix.'OrdersController@index', 'as' => 'orders']);
        Route::get('account/orders/edit/{id}', ['uses' => $frontEndPrefix.'OrdersController@edit', 'as' => 'orders.edit']);
        Route::post('account/orders/update/{id}', ['uses' => $frontEndPrefix.'OrdersController@update', 'as' => 'orders.update']);
        //account invoices
        Route::get('account/invoices', ['uses' => $frontEndPrefix.'InvoicesController@index', 'as' => 'invoices']);
        Route::get('account/invoices/edit/{id}', ['uses' => $frontEndPrefix.'InvoicesController@edit', 'as' => 'invoices.edit']);
        Route::post('account/invoices/update/{id}', ['uses' => $frontEndPrefix.'InvoicesController@update', 'as' => 'invoices.update']);
        //account reports
        Route::get('account/reports', ['uses' => $frontEndPrefix.'ReportsController@index', 'as' => 'reports']);
        Route::post('account/reports/excel', ['uses' => $frontEndPrefix.'ReportsController@excel', 'as' => 'excel']);
        Route::get('account/reports/{slug}', ['uses' => $frontEndPrefix.'ReportsController@report', 'as' => 'report']);
        //account labs
        Route::get('account/mylabs', ['uses' => $frontEndPrefix.'LabsController@index', 'as' => 'mylabs']);
        //account tickets
        Route::any('account/tickets', ['uses' => $frontEndPrefix.'TicketsController@index', 'as' => 'tickets']);
        //account notifications
        Route::any('account/notifications', ['uses' => $frontEndPrefix.'NotificationsController@index', 'as' => 'notifications']);
    });

    Route::group(['middleware' => 'guestcheck'], function() use ($frontEndPrefix) {
        Route::get('checkout', ['uses' => $frontEndPrefix.'CheckoutController@index', 'as' => 'checkout']);
        Route::post('checkout/create', ['uses' => $frontEndPrefix.'CheckoutController@create', 'as' => 'checkout.create']);
    });
});

Route::group(['middleware' => 'postcheck'], function() use ($frontEndPrefix) {
    Route::any('/ajax/getData', ['uses' => $frontEndPrefix.'AjaxController@getData']);    
});

Route::group(['prefix' => 'admin', 'as' => 'voyager.'], function ()  use ($backEndPrefix) {
    Route::get('login', ['uses' => $backEndPrefix.'VoyagerAuthController@login',     'as' => 'login']);
    Route::post('login', ['uses' => $backEndPrefix.'VoyagerAuthController@postLogin', 'as' => 'postlogin']);

    Route::group(['middleware' => 'admin.user'], function () use ($backEndPrefix) {
        Route::get('/', ['uses' => $backEndPrefix.'VoyagerController@index',   'as' => 'dashboard']);
        Route::post('logout', ['uses' => $backEndPrefix.'VoyagerController@logout',  'as' => 'logout']);
        Route::post('upload', ['uses' => $backEndPrefix.'VoyagerController@upload',  'as' => 'upload']);

        Route::post('industry-services/getservices', ['uses' => $backEndPrefix.'VoyagerIndustryServiceController@getservices',  'as' => 'getservices']);
        Route::post('industry-services/getoptions', ['uses' => $backEndPrefix.'VoyagerIndustryServiceController@getoptions',  'as' => 'getoptions']);
        Route::post('branches/getbranches', ['uses' => $backEndPrefix.'VoyagerBranchController@getbranches',  'as' => 'getbranches']);

        Route::get('profile', ['uses' => $backEndPrefix.'VoyagerUserController@profile', 'as' => 'profile']);

        try {
            foreach (Voyager::model('DataType')::all() as $dataType) {
                $breadController = $dataType->controller
                                 ? Str::start($dataType->controller, '\\')
                                 : $backEndPrefix.'VoyagerBaseController';

                Route::get($dataType->slug.'/order', $breadController.'@order')->name($dataType->slug.'.order');
                Route::post($dataType->slug.'/action', $breadController.'@action')->name($dataType->slug.'.action');
                Route::post($dataType->slug.'/order', $breadController.'@update_order')->name($dataType->slug.'.order');
                Route::get($dataType->slug.'/{id}/restore', $breadController.'@restore')->name($dataType->slug.'.restore');
                Route::get($dataType->slug.'/relation', $breadController.'@relation')->name($dataType->slug.'.relation');
                Route::post($dataType->slug.'/remove', $breadController.'@remove_media')->name($dataType->slug.'.media.remove');
                Route::resource($dataType->slug, $breadController, ['parameters' => [$dataType->slug => 'id']]);
            }
        } catch (\InvalidArgumentException $e) {
            throw new \InvalidArgumentException("Custom routes hasn't been configured because: ".$e->getMessage(), 1);
        } catch (\Exception $e) {
            
        }

        Route::resource('roles', $backEndPrefix.'VoyagerRoleController');

        Route::group([
            'as'     => 'menus.',
            'prefix' => 'menus/{menu}',
        ], function () use ($backEndPrefix) {
            Route::get('builder', ['uses' => $backEndPrefix.'VoyagerMenuController@builder',    'as' => 'builder']);
            Route::post('order', ['uses' => $backEndPrefix.'VoyagerMenuController@order_item', 'as' => 'order_item']);

            Route::group([
                'as'     => 'item.',
                'prefix' => 'item',
            ], function () use ($backEndPrefix) {
                Route::delete('{id}', ['uses' => $backEndPrefix.'VoyagerMenuController@delete_menu', 'as' => 'destroy']);
                Route::post('/', ['uses' => $backEndPrefix.'VoyagerMenuController@add_item',    'as' => 'add']);
                Route::put('/', ['uses' => $backEndPrefix.'VoyagerMenuController@update_item', 'as' => 'update']);
            });
        });


        Route::group([
            'as'     => 'settings.',
            'prefix' => 'settings',
        ], function () use ($backEndPrefix) {
            Route::get('/', ['uses' => $backEndPrefix.'VoyagerSettingsController@index',        'as' => 'index']);
            Route::post('/', ['uses' => $backEndPrefix.'VoyagerSettingsController@store',        'as' => 'store']);
            Route::put('/', ['uses' => $backEndPrefix.'VoyagerSettingsController@update',       'as' => 'update']);
            Route::delete('{id}', ['uses' => $backEndPrefix.'VoyagerSettingsController@delete',       'as' => 'delete']);
            Route::get('{id}/move_up', ['uses' => $backEndPrefix.'VoyagerSettingsController@move_up',      'as' => 'move_up']);
            Route::get('{id}/move_down', ['uses' => $backEndPrefix.'VoyagerSettingsController@move_down',    'as' => 'move_down']);
            Route::put('{id}/delete_value', ['uses' => $backEndPrefix.'VoyagerSettingsController@delete_value', 'as' => 'delete_value']);
        });

        Route::group([
            'as'     => 'media.',
            'prefix' => 'media',
        ], function () use ($backEndPrefix) {
            Route::get('/', ['uses' => $backEndPrefix.'VoyagerMediaController@index',              'as' => 'index']);
            Route::post('files', ['uses' => $backEndPrefix.'VoyagerMediaController@files',              'as' => 'files']);
            Route::post('new_folder', ['uses' => $backEndPrefix.'VoyagerMediaController@new_folder',         'as' => 'new_folder']);
            Route::post('delete_file_folder', ['uses' => $backEndPrefix.'VoyagerMediaController@delete', 'as' => 'delete']);
            Route::post('move_file', ['uses' => $backEndPrefix.'VoyagerMediaController@move',          'as' => 'move']);
            Route::post('rename_file', ['uses' => $backEndPrefix.'VoyagerMediaController@rename',        'as' => 'rename']);
            Route::post('upload', ['uses' => $backEndPrefix.'VoyagerMediaController@upload',             'as' => 'upload']);
            Route::post('crop', ['uses' => $backEndPrefix.'VoyagerMediaController@crop',             'as' => 'crop']);
        });

        Route::group([
            'as'     => 'bread.',
            'prefix' => 'bread',
        ], function () use ($backEndPrefix) {
            Route::get('/', ['uses' => $backEndPrefix.'VoyagerBreadController@index',              'as' => 'index']);
            Route::get('{table}/create', ['uses' => $backEndPrefix.'VoyagerBreadController@create',     'as' => 'create']);
            Route::post('/', ['uses' => $backEndPrefix.'VoyagerBreadController@store',   'as' => 'store']);
            Route::get('{table}/edit', ['uses' => $backEndPrefix.'VoyagerBreadController@edit', 'as' => 'edit']);
            Route::put('{id}', ['uses' => $backEndPrefix.'VoyagerBreadController@update',  'as' => 'update']);
            Route::delete('{id}', ['uses' => $backEndPrefix.'VoyagerBreadController@destroy',  'as' => 'delete']);
            Route::post('relationship', ['uses' => $backEndPrefix.'VoyagerBreadController@addRelationship',  'as' => 'relationship']);
            Route::get('delete_relationship/{id}', ['uses' => $backEndPrefix.'VoyagerBreadController@deleteRelationship',  'as' => 'delete_relationship']);
        });

        Route::resource('database', $backEndPrefix.'VoyagerDatabaseController');

        Route::group([
            'as'     => 'compass.',
            'prefix' => 'compass',
        ], function () use ($backEndPrefix) {
            Route::get('/', ['uses' => $backEndPrefix.'VoyagerCompassController@index',  'as' => 'index']);
            Route::post('/', ['uses' => $backEndPrefix.'VoyagerCompassController@index',  'as' => 'post']);
        });

    });
});

Route::get('/voyager-assets', ['uses' => $backEndPrefix.'VoyagerController@assets', 'as' => 'voyager.voyager_assets']);
Route::get('/{slug}/{id}', ['uses' => $frontEndPrefix.'BranchController@index', 'as' => 'branch']);
